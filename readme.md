# .NET Windows Forms Controls Libraries

Support libraries for Windows Forms Controls.
This is a fork of the [dn.core] repository.

* Project README files:
  * [cc.isr.WinControls.Binding](/src/libs/binding/readme.md) 
  * [cc.isr.WinControls.Explorers](/src/libs/explorers/readme.md) 
  * [cc.isr.WinControls.Extended](/src/libs/extended/readme.md) 
  * [cc.isr.WinControls.Extensions](/src/libs/extensions/readme.md) 
  * [cc.isr.WinControls.Frameless](/src/libs/frameless/readme.md) 
  * [cc.isr.WinControls.Led](/src/libs/led/readme.md) 
  * [cc.isr.WinControls.Loging](/src/libs/loging/readme.md) 
  * [cc.isr.WinControls.Metro](/src/libs/metro/readme.md) 
  * [cc.isr.WinControls.ModelView](/src/libs/model.view/readme.md) 
  * [cc.isr.WinControls.ModelViewLogger](/src/libs/model.view.logger/readme.md) 
  * [cc.isr.WinControls.Progress](/src/libs/progress/readme.md) 
  * [cc.isr.WinControls.RichText](/src/libs/rich.text/readme.md) 
  * [cc.isr.WinControls.RichTextbox](/src/libs/rich.text.box/readme.md) 
  * [cc.isr.WinControls.Rounded](/src/libs/rounded/readme.md) 
  * [cc.isr.WinControls.Selectors](/src/libs/selectors/readme.md) 
  * [cc.isr.WinControls.Shape](/src/libs/shape/readme.md) 
  * [cc.isr.WinControls.Tab](/src/libs/tab/readme.md) 
  * [cc.isr.WinControls.Toggle](/src/libs/toggle/readme.md) 
  * [cc.isr.WinControls.ToolStrips](/src/libs/tool.strips/readme.md) 
  * [cc.isr.WinControls.Trees](/src/libs/trees/readme.md) 
  * [cc.isr.WinControls.Wizard](/src/libs/wisard/readme.md) 
* [Attributions](Attributions.md)
* [Change Log](./CHANGELOG.md)
* [Cloning](Cloning.md)
* [Code License](LICENSE-CODE)
* [Code of Conduct](code_of_conduct.md)
* [Contributing](contributing.md)
* [Legal Notices](#legal-notices)
* [License](LICENSE)
* [Open Source](Open-Source.md)
* [Repository Owner](#Repository-Owner)
* [Security](security.md)

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Authors"></a>
## Authors
* [ATE Coder]  

<a name="legal-notices"></a>
## Legal Notices

Integrated Scientific Resources, Inc., and any contributors grant you a license to the documentation and other content in this repository under the [Creative Commons Attribution 4.0 International Public License] and grant you a license to any code in the repository under the [MIT License].

Integrated Scientific Resources, Inc., and/or other Integrated Scientific Resources, Inc., products and services referenced in the documentation may be either trademarks or registered trademarks of Integrated Scientific Resources, Inc., in the United States and/or other countries. The licenses for this project do not grant you rights to use any Integrated Scientific Resources, Inc., names, logos, or trademarks.

Integrated Scientific Resources, Inc., and any contributors reserve all other rights, whether under their respective copyrights, patents, or trademarks, whether by implication, estoppel or otherwise.

[Creative Commons Attribution 4.0 International Public License]: https://github.com/ATECoder/dn.vi.ivi/blob/main/license
[MIT License]: https://github.com/ATECoder/dn.vi.ivi/blob/main/license-code
 
[ATE Coder]: https://www.IntegratedScientificResources.com
[dn.core]: https://www.bitbucket.org/davidhary/dn.core

