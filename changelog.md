# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [1.1.8956] - 2024-07-09 Preview 202304
* Reference updated core projects.

## [1.1.8949] - 2024-07-02 Preview 202304
* Updated to .Net 8.
* Implemented MS Test SDK project format.
* Incremented libraries versions.
* Applied code analysis rules.
* Tool strip range: implement nullable.
* Generate assembly attributes.

## [1.1.8540] - 2023-05-20 Preview 202304
* Set projects framework to .NET 7.
* Rearrange folders.
* Set MSTest namespace to cc.isr.
* Use Observable object form the community toolkit.
* Implement nullable.
* Fix orphan links in MD files. 
* update project references due to restructuring folders or referenced libraries.
* Update packages. 
* Use cc.isr.Json.AppSettings.ViewModels project.

## [1.1.8538] - 2023-05-15 Preview 202304
* Use cc.isr.Json.AppSettings.ViewModels project for settings I/O.

## [1.1.8518] - 2023-04-28 Preview 202304
* Split README.MD to attribution, cloning, open-source and read me files.
* Add code of conduct, contribution and security documents.
* Increment version.

## [1.0.8189] - 2022-06-03
* Use Tuples to implement GetHashCode().

## [1.0.8125] - 2022-03-31
* Pass tests in project reference mode. 

## [1.0.8122] - 2022-03-28
* Selectors: expose the number of resource names and the 
selected resource name.

## [1.0.8119] - 2022-03-25
* Use the new Json application settings base class.
* Use Messages box from Logging Windows Forms.

## [1.0.8109] - 2022-03-15
* Use the ?. operator, without making a copy of the delegate, 
to check if a delegate is non-null and invoke it in a thread-safe way.

## [1.0.8104] - 2022-03-10
* Forked from [dn.core].

&copy; 2012 Integrated Scientific Resources, Inc. All rights reserved.

[dn.core]: https://www.bitbucket.org/davidhary/dn.core
[1.1.9083]: https://bitbucket.org/davidhary/dn.win.controls/src/main/
