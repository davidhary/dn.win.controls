# Open Source

Support libraries for Windows Forms Controls
This is a fork of the [dn.core] repository.

* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Open-Source"></a>
## Open Source
Open source used by this software is described and licensed at the
following sites:  
[win.controls]  
[enums]  
[json]  
[tracing]  
[std]  
[logging]  
[Exception Extension]  
[Notification Window]  
[Safe Events1]  
[Safe Events2]  
[Safe Events3]  

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
 
[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[win.controls]: https://www.bitbucket.org/davidhary/dn.win.controls
[enums]: https://www.bitbucket.org/davidhary/dn.enums
[json]: https://www.bitbucket.org/davidhary/dn.json
[tracing]: https://www.bitbucket.org/davidhary/dn.tracing
[logging]: https://www.bitbucket.org/davidhary/dn.logging
[std]: https://www.bitbucket.org/davidhary/dn.std
[Exception Extension]: https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
[Notification Window]: http://www.codeproject.com/KB/dialog/notificationwindow.aspx
[Safe Events1]: http://www.CodeProject.com/KB/cs/EventSafeTrigger.aspx
[Safe Events2]: http://www.DreamInCode.net/forums/user/334492-aeonhack
[Safe Events3]: http://www.DreamInCode.net/code/snippet5016.htm

[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
[external repositories]: ExternalReposCommits.csv

