using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace cc.isr.WinControls.Demo;
public partial class TreePanelForm
{
    /// <summary>
	    /// Required designer variable.
	    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
	    /// Clean up any resources being used.
	    /// </summary>
	    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if ( disposing )
        {
            components?.Dispose();
        }

        base.Dispose(disposing);
    }

    /// <summary>
	    /// Required method for Designer support - do not modify
	    /// the contents of this method with the code editor.
	    /// </summary>
    private void InitializeComponent()
    {
        this.treePanel1 = new cc.isr.WinControls.TreePanel();
        ((System.ComponentModel.ISupportInitialize)(this.treePanel1)).BeginInit();
        this.treePanel1.SuspendLayout();
        this.SuspendLayout();
        // 
        // treePanel1
        // 
        this.treePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
        this.treePanel1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
        this.treePanel1.Location = new System.Drawing.Point(0, 0);
        this.treePanel1.MinTreeSize = 25;
        this.treePanel1.Name = "treePanel1";
        this.treePanel1.Size = new System.Drawing.Size(472, 443);
        this.treePanel1.SplitterDistance = 130;
        this.treePanel1.TabIndex = 7;
        this.treePanel1.Title = "Tree Panel";
        // 
        // TreePanelForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(472, 443);
        this.Controls.Add(this.treePanel1);
        this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
        this.Name = "TreePanelForm";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "Micro Timer Demo";
        ((System.ComponentModel.ISupportInitialize)(this.treePanel1)).EndInit();
        this.treePanel1.ResumeLayout(false);
        this.ResumeLayout(false);

    }

    private WinControls.TreePanel treePanel1;
}
