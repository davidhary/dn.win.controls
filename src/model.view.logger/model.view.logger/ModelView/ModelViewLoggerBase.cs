using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Extensions.Logging;

namespace cc.isr.WinControls;

/// <summary>
/// A user control base. Supports property change notification and logger.
/// </summary>
/// <remarks>
/// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-12-26, 2.1.5836. </para>
/// </remarks>
public partial class ModelViewLoggerBase : ModelViewBase
{
    #region " construction and cleanup "

    /// <summary>
    /// A protected constructor for this class making it not publicly creatable. This ensure using
    /// the class as a base class.
    /// </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    protected ModelViewLoggerBase() : base()
    {
        this.InitializingComponents = true;
        this.InitializeComponent();
        this.InitializingComponents = false;
    }

    #region " windows form designer generated code "

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        this.SuspendLayout();
        // 
        // ModelViewLoggerBase
        // 
        this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
        this.AutoScaleMode = AutoScaleMode.Inherit;
        this.Font = new Font( SystemFonts.DefaultFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0 );
        this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
        this.Name = "ModelViewLoggerBase";
        this.Size = new Size( 175, 173 );
        this.ResumeLayout( false );
    }

    #endregion

    #endregion

    #region " text  writer "

    /// <summary>   Adds a display trace event writer. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    /// <param name="textWriter">   The trace Event tWriter. </param>
    public void AddDisplayTextWriter( Tracing.ITraceEventWriter textWriter )
    {
        textWriter.TraceLevel = this.DisplayTraceEventType;
        cc.isr.Tracing.TracingPlatform.Instance.AddTraceEventWriter( textWriter );
    }

    /// <summary>   Removes the display text writer described by <paramref name="textWriter"/>. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    /// <param name="textWriter">   The trace Event tWriter. </param>
    public static void RemoveDisplayTextWriter( Tracing.ITraceEventWriter textWriter )
    {
        cc.isr.Tracing.TracingPlatform.Instance.RemoveTraceEventWriter( textWriter );
    }

    #endregion

    #region " display log level "
#if false
    // log level does not map to trace event types, which filter the display.

    private KeyValuePair<LogLevel, string> _displayLogLevelValueNamePair;

    /// <summary>
    /// Gets or sets the <see cref="Microsoft.Extensions.Logging.LogLevel"/> value name pair for display.
    /// </summary>
    /// <value> The log level value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<LogLevel, string> DisplayLogLevelValueNamePair
    {
        get => this._displayLogLevelValueNamePair;
        set
        {
             if ( !KeyValuePair<LogLevel, string>.Equals( value, this.DisplayLogLevelValueNamePair ) )
            {
                this._displayLogLevelValueNamePair = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    private LogLevel _displayLogLevel;

    /// <summary>
    /// Gets or sets the <see cref="Microsoft.Extensions.Logging.LogLevel"/> for display.
    /// </summary>
    /// <value> The trace event writer log level. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public LogLevel DisplayLogLevel
    {
        get => this._displayLogLevel;
        set
        {
             if ( value != this.DisplayLogLevel )
            {
                this._displayLogLevel = value;
                this.NotifyPropertyChanged();
            }
        }
    }
#endif
    #endregion

    #region " display trace event type "

    private KeyValuePair<TraceEventType, string> _traceEventWriterTraceEventValueNamePair;

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> value name pair for the global trace event
    /// writer. This level determines the level of all the
    /// <see cref="Tracing.ITraceEventWriter"/>s Trace Listeners. Each trace listener can still listen at a
    /// lower level.
    /// </summary>
    /// <value> The trace event writer trace event value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<TraceEventType, string> TraceEventWriterTraceEventValueNamePair
    {
        get => this._traceEventWriterTraceEventValueNamePair;
        set => _ = this.SetProperty( ref this._traceEventWriterTraceEventValueNamePair, value );
    }

    private TraceEventType _traceEventWriterTraceEventType;

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> for the global trace event writer. This level
    /// determines the level of all the
    /// <see cref="Tracing.ITraceEventWriter"/>s Trace Listeners. Each trace listener can still listen at a
    /// lower level.
    /// </summary>
    /// <value> The type of the trace event writer trace event. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public TraceEventType TraceEventWriterTraceEventType
    {
        get => this._traceEventWriterTraceEventType;
        set => _ = this.SetProperty( ref this._traceEventWriterTraceEventType, value );
    }

    private KeyValuePair<TraceEventType, string> _displayTraceEventTypeValueNamePair;

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> value name pair for display.
    /// </summary>
    /// <value> The log level value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<TraceEventType, string> DisplayTraceEventTypeValueNamePair
    {
        get => this._displayTraceEventTypeValueNamePair;
        set => _ = this.SetProperty( ref this._displayTraceEventTypeValueNamePair, value );
    }

    private TraceEventType _displayTraceEventType;

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> for display.
    /// </summary>
    /// <value> The trace event writer log level. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public TraceEventType DisplayTraceEventType
    {
        get => this._displayTraceEventType;
        set => _ = this.SetProperty( ref this._displayTraceEventType, value );
    }

    #endregion

    #region " logging log level "

    private KeyValuePair<LogLevel, string> _loggingLevelValueNamePair;

    /// <summary>
    /// Gets or sets the <see cref="Microsoft.Extensions.Logging.LogLevel"/> value name pair for logging. This level
    /// determines the level of the <see cref="ILogger"/>.
    /// </summary>
    /// <value> The log trace event value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<LogLevel, string> LoggingLevelValueNamePair
    {
        get => this._loggingLevelValueNamePair;
        set => _ = this.SetProperty( ref this._loggingLevelValueNamePair, value );
    }

    private LogLevel _loggingLogLevel;

    /// <summary>   Gets or sets <see cref="Microsoft.Extensions.Logging.LogLevel"/> value for logging. </summary>
    /// <value> The trace event type for logging. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public LogLevel LoggingLogLevel
    {
        get => this._loggingLogLevel;
        set => _ = this.SetProperty( ref this._loggingLogLevel, value );
    }

    #endregion

    #region " log "

    /// <summary>
    /// Trace an information message built using the <see cref="cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage(string, string, string, int)"/>
    /// method.
    /// </summary>
    /// <remarks>   2024-08-19. </remarks>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A string. </returns>
    protected virtual string TraceInformation( string message,
         [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
         [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
         [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        message = cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourceFilePath, sourceLineNumber );
        System.Diagnostics.Trace.TraceInformation( message );
        return message;
    }

    /// <summary>
    /// Trace a warning message built using the <see cref="cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage(string, string, string, int)"/>
    /// method.
    /// </summary>
    /// <remarks>   2024-08-19. </remarks>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A string. </returns>
    protected virtual string TraceWarning( string message,
         [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
         [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
         [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        message = cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourceFilePath, sourceLineNumber );
        System.Diagnostics.Trace.TraceWarning( message );
        return message;
    }

    /// <summary>   Adds an exception data. </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="ex">   The exception. </param>
    /// <returns>   True if exception data was added for this exception; otherwise, false. </returns>
    protected virtual bool AddExceptionData( Exception ex )
    {
        return false;
    }

    /// <summary>
    /// Trace the message built using the <see cref="cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage(System.Exception, string, string, string, int)"/>
    /// method.
    /// </summary>
    /// <remarks>   2024-08-19. </remarks>
    /// <param name="ex">               The ex. </param>
    /// <param name="activity">         The activity. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A string. </returns>
    protected virtual string TraceException( Exception ex, string activity,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        _ = this.AddExceptionData( ex );
        string message = cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( ex, activity, memberName, sourceFilePath, sourceLineNumber );
        System.Diagnostics.Trace.WriteLine( message );
        return message;
    }

    /// <summary>   Log exception. </summary>
    /// <remarks>
    /// Declared as must override so that the calling method could add exception data before
    /// recording this exception.
    /// </remarks>
    /// <param name="ex">               The ex. </param>
    /// <param name="activity">         The activity. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    protected virtual string LogException( Exception ex, string activity,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        _ = this.AddExceptionData( ex );
        string message = cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( ex, activity, memberName, sourceFilePath, sourceLineNumber );
        System.Diagnostics.Trace.WriteLine( message );
        return message;
    }

    #endregion
}
