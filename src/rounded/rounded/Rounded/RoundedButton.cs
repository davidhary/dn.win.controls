using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using cc.isr.WinControls.DrawingExtensions;

namespace cc.isr.WinControls;

/// <summary> A button with rounded option. </summary>
/// <remarks>
/// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-03-26 </para>
/// </remarks>
[DesignerCategory( "code" )]
[Description( "Round Button" )]
public class RoundedButton : Button
{
    #region " contructor "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public RoundedButton() : base() => this.CornerRadii = new CornerRadius();

    #endregion

    #region " border style "

    /// <summary> Size of the border. </summary>
    private int _borderSize;

    /// <summary> Specifies the border size. </summary>
    /// <value> The size of the border. </value>
    [Category( "Appearance" )]
    [DefaultValue( 1 )]
    [Description( "Specifies the border size." )]
    public int BorderSize
    {
        get => this._borderSize;
        set
        {
            this._borderSize = value;
            this.Invalidate();
        }
    }

    /// <summary> The shadow angle. </summary>
    private int _shadowAngle;

    /// <summary> Specifies the shadow angle. </summary>
    /// <value> The shadow angle. </value>
    [Category( "Appearance" )]
    [DefaultValue( 270 )]
    [Description( "Specifies the shadow angle." )]
    public int ShadowAngle
    {
        get => this._shadowAngle;
        set
        {
            this._shadowAngle = value;
            this.Invalidate();
        }
    }

    /// <summary> Specifies the top left radius in percent of height or width. </summary>
    /// <value> The top left radius. </value>
    [Category( "Appearance" )]
    [DefaultValue( 0 )]
    [Description( "Specifies the top left radius in percent of height or width." )]
    public int TopLeftRadius
    {
        get => this.Corners.TopLeft;
        set
        {
            this._corners = new CornerRadius( this.BottomLeftRadius, this.BottomRightRadius, value, this.TopRightRadius );
            this.CornerRadii = new CornerRadius( this.BottomLeftRadius * this.Height / 100, this.BottomRightRadius * this.Height / 100, this.TopLeftRadius * this.Height / 100, this.TopRightRadius * this.Height / 100 );
            this.Invalidate();
        }
    }

    /// <summary> Specifies the top right radius. </summary>
    /// <value> The top right radius. </value>
    [Category( "Appearance" )]
    [DefaultValue( 0 )]
    [Description( "Specifies the top right radius." )]
    public int TopRightRadius
    {
        get => this.Corners.TopRight;
        set
        {
            this._corners = new CornerRadius( this.BottomLeftRadius, this.BottomRightRadius, this.TopLeftRadius, value );
            this.CornerRadii = new CornerRadius( this.BottomLeftRadius * this.Height / 100, this.BottomRightRadius * this.Height / 100, this.TopLeftRadius * this.Height / 100, this.TopRightRadius * this.Height / 100 );
            this.Invalidate();
        }
    }

    /// <summary> Specifies the bottom left radius. </summary>
    /// <value> The bottom left radius. </value>
    [Category( "Appearance" )]
    [DefaultValue( 0 )]
    [Description( "Specifies the bottom left radius." )]
    public int BottomLeftRadius
    {
        get => this.Corners.BottomLeft;
        set
        {
            this._corners = new CornerRadius( value, this.BottomRightRadius, this.TopLeftRadius, this.TopRightRadius );
            this.CornerRadii = new CornerRadius( this.BottomLeftRadius * this.Height / 100, this.BottomRightRadius * this.Height / 100, this.TopLeftRadius * this.Height / 100, this.TopRightRadius * this.Height / 100 );
            this.Invalidate();
        }
    }

    /// <summary> Specifies the bottom right radius. </summary>
    /// <value> The bottom right radius. </value>
    [Category( "Appearance" )]
    [DefaultValue( 0 )]
    [Description( "Specifies the bottom right radius." )]
    public int BottomRightRadius
    {
        get => this.Corners.BottomRight;
        set
        {
            this._corners = new CornerRadius( this.BottomLeftRadius, value, this.TopLeftRadius, this.TopRightRadius );
            this.CornerRadii = new CornerRadius( this.BottomLeftRadius * this.Height / 100, this.BottomRightRadius * this.Height / 100, this.TopLeftRadius * this.Height / 100, this.TopRightRadius * this.Height / 100 );
            this.Invalidate();
        }
    }

    /// <summary> Default corners. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The CornerRadius. </returns>
    private static CornerRadius DefaultCorners()
    {
        return new CornerRadius( 0 );
    }

    /// <summary> The corners. </summary>
    private CornerRadius _corners = DefaultCorners();

    /// <summary> Gets or sets the relative corner radii. </summary>
    /// <value> The relative corner radii. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public CornerRadius Corners
    {
        get => this._corners;
        set
        {
            this._corners = value;
            this.CornerRadii = new CornerRadius( this.BottomLeftRadius * this.Height / 100, this.BottomRightRadius * this.Height / 100, this.TopLeftRadius * this.Height / 100, this.TopRightRadius * this.Height / 100 );
            this.Invalidate();
        }
    }

    /// <summary> Gets or sets the corner radii. </summary>
    /// <value> The corner radii. </value>
    private CornerRadius CornerRadii { get; set; }

    #endregion

    #region " paint "

    /// <summary> Clip region. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="surface">         The surface. </param>
    /// <param name="borderRectangle"> The border rectangle. </param>
    public void ClipRegion( Graphics surface, Rectangle borderRectangle )
    {
        if ( surface is null ) return;

        surface.PixelOffsetMode = PixelOffsetMode.HighQuality;
        surface.InterpolationMode = InterpolationMode.HighQualityBilinear;
        surface.SmoothingMode = SmoothingMode.AntiAlias;
        using GraphicsPath path = new();
        _ = path.BuildRoundClipPath( this.Size, borderRectangle, this.CornerRadii );
        this.Region = new Region( path );
    }

    /// <summary> Renders the border. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="surface">         The surface. </param>
    /// <param name="borderRectangle"> The border rectangle. </param>
    public void RenderBorder( Graphics surface, Rectangle borderRectangle )
    {
        if ( surface is null ) return;

        surface.PixelOffsetMode = PixelOffsetMode.HighQuality;
        surface.InterpolationMode = InterpolationMode.HighQualityBilinear;
        surface.SmoothingMode = SmoothingMode.AntiAlias;
        using ( GraphicsPath path = new() )
        {
            _ = path.BuildRoundClipPath( this.Size, borderRectangle, this.CornerRadii );
            surface.SetClip( path );
            using ( Pen pen = new( ControlPaint.Dark( this.BackColor ), 1f ) )
            {
                surface.SmoothingMode = SmoothingMode.AntiAlias;
                surface.DrawPath( pen, path );
            }

            surface.ResetClip();
        }

        Rectangle innerRectangle = borderRectangle;
        using ( GraphicsPath path = new() )
        {
            _ = path.BuildRoundClipPath( this.Size, innerRectangle, this.CornerRadii );
            surface.SetClip( path );
            // TO_DO: Try transparent color.
            Color color1 = ControlPaint.LightLight( this.BackColor );
            // color1 = Color.Transparent
            Color color2 = ControlPaint.Dark( this.BackColor );
            if ( this.ShadowAngle < 0 )
            {
                using LinearGradientBrush brush = new( innerRectangle, color2, color2, 0f )
                {
                    GammaCorrection = true
                };
                using Pen pen = new( brush, this.BorderSize )
                {
                    Alignment = PenAlignment.Inset
                };
                surface.SmoothingMode = SmoothingMode.AntiAlias;
                surface.DrawPath( pen, path );
            }
            else
            {
                using LinearGradientBrush brush = new( innerRectangle, color1, color2, this.ShadowAngle )
                {
                    GammaCorrection = true
                };
                using Pen pen = new( brush, this.BorderSize )
                {
                    Alignment = PenAlignment.Inset
                };
                surface.SmoothingMode = SmoothingMode.AntiAlias;
                surface.DrawPath( pen, path );
            }

            surface.ResetClip();
        }
    }

    /// <summary> Raises the <see cref="Control.Paint" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="pevent"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs pevent )
    {
        if ( pevent is null ) return;

        pevent.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
        base.OnPaint( pevent );
        if ( this.DisplayRectangle.Width < 4 || this.DisplayRectangle.Height < 4 ) return;

        this.CornerRadii = new CornerRadius( this.BottomLeftRadius * this.Height / 100, this.BottomRightRadius * this.Height / 100, this.TopLeftRadius * this.Height / 100, this.TopRightRadius * this.Height / 100 );
        Rectangle rect = this.DisplayRectangle;
        // this removes the default border.
        rect.Inflate( -3, -3 );
        this.ClipRegion( pevent.Graphics, rect );
        if ( this.BorderSize > 0 )
        {
            this.RenderBorder( pevent.Graphics, rect );
        }
    }

    #endregion

    #region " resize "

    /// <summary> Raises the <see cref="Control.Resize" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnResize( EventArgs e )
    {
        base.OnResize( e );
        using PaintEventArgs paintArgs = new( this.CreateGraphics(), this.DisplayRectangle );
        this.InvokePaint( this, paintArgs );
    }

    #endregion
}
