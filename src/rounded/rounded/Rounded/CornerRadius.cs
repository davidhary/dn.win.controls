using System;
using System.Collections;
using System.ComponentModel;

namespace cc.isr.WinControls;

#pragma warning disable CA2231
/// <summary> A corner radius. </summary>
/// <remarks>
/// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para>
/// </remarks>
public struct CornerRadius
#pragma warning restore CA2231
{
    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="radius"> The radius. </param>
    public CornerRadius( int radius )
    {
        this.BottomLeft = radius;
        this.BottomRight = radius;
        this.TopLeft = radius;
        this.TopRight = radius;
    }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="bottomLeft">  The bottom left. </param>
    /// <param name="bottomRight"> The bottom right. </param>
    /// <param name="topLeft">     The top left. </param>
    /// <param name="topRight">    The top right. </param>
    public CornerRadius( int bottomLeft, int bottomRight, int topLeft, int topRight )
    {
        this.BottomLeft = bottomLeft;
        this.BottomRight = bottomRight;
        this.TopLeft = topLeft;
        this.TopRight = topRight;
    }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    public CornerRadius( CornerRadius value )
    {
        this.BottomLeft = value.BottomLeft;
        this.BottomRight = value.BottomRight;
        this.TopLeft = value.TopLeft;
        this.TopRight = value.TopRight;
    }

    /// <summary> Parses. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> The CornerRadius. </returns>
    public static CornerRadius Parse( string value )
    {
        if ( string.IsNullOrWhiteSpace( value ) )
        {
            return Empty;
        }
        else
        {
            string[] values = value.Split( ',' );
            return value.Length < 4 ? Empty : new CornerRadius( Convert.ToInt32( values[0], System.Globalization.CultureInfo.InvariantCulture ), Convert.ToInt32( values[1], System.Globalization.CultureInfo.InvariantCulture ), Convert.ToInt32( values[2], System.Globalization.CultureInfo.InvariantCulture ), Convert.ToInt32( values[3], System.Globalization.CultureInfo.InvariantCulture ) );
        }
    }

    /// <summary> Gets the unity. </summary>
    /// <value> The unity. </value>
    public static CornerRadius Unity => new( 1, 1, 1, 1 );

    /// <summary> Gets the empty. </summary>
    /// <value> The empty. </value>
    public static CornerRadius Empty => new( 0, 0, 0, 0 );

    /// <summary> Gets the is empty. </summary>
    /// <value> The is empty. </value>
    public readonly bool IsEmpty => this.BottomLeft <= 0 || this.BottomRight <= 0 || this.TopLeft <= 0 || this.TopRight <= 0;

    /// <summary> Set the Radius of the Lower Left Corner. </summary>
    /// <value> The bottom left. </value>
    [Description( "Set the Radius of the Lower Left Corner" )]
    [RefreshProperties( RefreshProperties.Repaint )]
    [NotifyParentProperty( true )]
    [DefaultValue( 0 )]
    public int BottomLeft { get; set; }

    /// <summary> Set the Radius of the Lower Right Corner. </summary>
    /// <value> The bottom right. </value>
    [Description( "Set the Radius of the Lower Right Corner" )]
    [RefreshProperties( RefreshProperties.Repaint )]
    [NotifyParentProperty( true )]
    [DefaultValue( 0 )]
    public int BottomRight { get; set; }

    /// <summary> Set the Radius of the Upper Left Corner. </summary>
    /// <value> The top left. </value>
    [Description( "Set the Radius of the Upper Left Corner" )]
    [RefreshProperties( RefreshProperties.Repaint )]
    [NotifyParentProperty( true )]
    [DefaultValue( 0 )]
    public int TopLeft { get; set; }

    /// <summary> Set the Radius of the Upper Right Corner. </summary>
    /// <value> The top right. </value>
    [Description( "Set the Radius of the Upper Right Corner" )]
    [RefreshProperties( RefreshProperties.Repaint )]
    [NotifyParentProperty( true )]
    [DefaultValue( 0 )]
    public int TopRight { get; set; }

    /// <summary> Returns the fully qualified type name of this instance. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The fully qualified type name. </returns>
    public override readonly string ToString()
    {
        return $"{this.BottomLeft}, {this.BottomRight}, {this.TopLeft}, {this.TopRight}";
    }

    /// <summary> Indicates whether this instance and a specified object are equal. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="obj"> The object to compare with the current instance. </param>
    /// <returns>
    /// true if <paramref name="obj" /> and this instance are the same type and represent the same
    /// value; otherwise, false.
    /// </returns>
    public override readonly bool Equals( object? obj )
    {
        return obj is not null && ReferenceEquals( this.GetType(), obj.GetType() ) && this.Equals( ( CornerRadius ) obj );
    }

    /// <summary> Indicates whether this instance and a specified object are equal. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="other"> The corner radius to compare to this object. </param>
    /// <returns>
    /// true if <paramref name="other" /> and this instance are the same type and represent the same
    /// value; otherwise, false.
    /// </returns>
    public readonly bool Equals( CornerRadius other )
    {
        return this.BottomLeft == other.BottomLeft && this.BottomRight == other.BottomRight && this.TopLeft == other.TopLeft && this.TopRight == other.TopRight;
    }

    /// <summary>   Returns the hash code for this instance. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
    public override readonly int GetHashCode()
    {
#if NET8_0_OR_GREATER
        return System.HashCode.Combine( this.BottomLeft, this.BottomRight, this.TopLeft, this.TopRight );
#else
        return (this.BottomLeft, this.BottomRight, this.TopLeft, this.TopRight).GetHashCode();
#endif
    }
}
/// <summary> A corner radius converter. </summary>
/// <remarks>
/// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-04-04 </para>
/// </remarks>
internal class CornerRadiusConverter : ExpandableObjectConverter
{
    /// <summary>
    /// Returns whether changing a value on this object requires a call to
    /// <see cref="TypeConverter.CreateInstance(IDictionary)" />
    /// to create a new value, using the specified context.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="context"> An <see cref="ITypeDescriptorContext" /> that
    /// provides a format context. </param>
    /// <returns>
    /// true if changing a property on this object requires a call to
    /// <see cref="TypeConverter.CreateInstance(IDictionary)" />
    /// to create a new value; otherwise, false.
    /// </returns>
    public override bool GetCreateInstanceSupported( ITypeDescriptorContext? context )
    {
        return true;
    }

    /// <summary>
    /// Creates an instance of the type that this
    /// <see cref="TypeConverter" /> is associated with, using the specified
    /// context, given a set of property values for the object.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="context">        An <see cref="ITypeDescriptorContext" />
    /// that provides a format context. </param>
    /// <param name="propertyValues"> An <see cref="IDictionary" /> of new
    /// property values. </param>
    /// <returns>
    /// An <see cref="object" /> representing the given
    /// <see cref="IDictionary" />, or null if the object cannot be created.
    /// This method always returns null.
    /// </returns>
    public override object CreateInstance( ITypeDescriptorContext? context, IDictionary propertyValues )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( propertyValues, nameof( propertyValues ) );
#else
        if ( propertyValues is null ) throw new ArgumentNullException( nameof( propertyValues ) );
#endif

        int lL = ( int ) (propertyValues["BottomLeft"] ?? 0);
        int lR = ( int ) (propertyValues["BottomRight"] ?? 0);
        int uL = ( int ) (propertyValues["TopLeft"] ?? 0);
        int uR = ( int ) (propertyValues["TopRight"] ?? 0);
        CornerRadius crn = new( lL, lR, uL, uR );
        return crn;
    }

    /// <summary>
    /// Returns whether this converter can convert an object of the given type to the type of this
    /// converter, using the specified context.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="context">    An <see cref="ITypeDescriptorContext" /> that
    /// provides a format context. </param>
    /// <param name="sourceType"> A <see cref="Type" /> that represents the type you want to
    /// convert from. </param>
    /// <returns> true if this converter can perform the conversion; otherwise, false. </returns>
    public override bool CanConvertFrom( ITypeDescriptorContext? context, Type sourceType )
    {
        return ReferenceEquals( sourceType, typeof( string ) ) || base.CanConvertFrom( context, sourceType );
    }

    /// <summary>
    /// Converts the given object to the type of this converter, using the specified context and
    /// culture information.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="context"> An <see cref="ITypeDescriptorContext" /> that
    /// provides a format context. </param>
    /// <param name="culture"> The <see cref="System.Globalization.CultureInfo" /> to use as the
    /// current culture. </param>
    /// <param name="value">   The <see cref="object" /> to convert. </param>
    /// <returns> An <see cref="object" /> that represents the converted value. </returns>
    public override object? ConvertFrom( ITypeDescriptorContext? context, System.Globalization.CultureInfo? culture, object value )
    {
        if ( value is string @string )
        {
            try
            {
                string s = @string;
                string[]? cornerParts = new string[5];
                cornerParts = s.Split( ',' );
                if ( cornerParts is not null )
                {
                    if ( string.IsNullOrEmpty( cornerParts[0] ) )
                    {
                        cornerParts[0] = "0";
                    }

                    if ( string.IsNullOrEmpty( cornerParts[1] ) )
                    {
                        cornerParts[1] = "0";
                    }

                    if ( string.IsNullOrEmpty( cornerParts[2] ) )
                    {
                        cornerParts[2] = "0";
                    }

                    if ( string.IsNullOrEmpty( cornerParts[3] ) )
                    {
                        cornerParts[3] = "0";
                    }

                    return new CornerRadius( Convert.ToInt32( cornerParts[0].Trim(), System.Globalization.CultureInfo.CurrentCulture ),
                                             Convert.ToInt32( cornerParts[1].Trim(), System.Globalization.CultureInfo.CurrentCulture ),
                                             Convert.ToInt32( cornerParts[2].Trim(), System.Globalization.CultureInfo.CurrentCulture ),
                                             Convert.ToInt32( cornerParts[3].Trim(), System.Globalization.CultureInfo.CurrentCulture ) );
                }
            }
            catch ( Exception ex )
            {
                throw new ArgumentException( FormattableString.Invariant( $"Can not convert '{value}' to type CornerRadius" ), ex );
            }
        }
        else
        {
            return new CornerRadius();
        }

        return base.ConvertFrom( context, culture, value );
    }

    /// <summary>
    /// Converts the given value object to the specified type, using the specified context and
    /// culture information.
    /// </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="context">          An <see cref="ITypeDescriptorContext"/>
    ///                                 that provides a format context. </param>
    /// <param name="culture">          A <see cref="System.Globalization.CultureInfo" />. If
    ///                                 <see langword="null" /> is passed, the current culture is
    ///                                 assumed. </param>
    /// <param name="value">            The <see cref="object" /> to convert. </param>
    /// <param name="destinationType">  The <see cref="Type" /> to convert the
    ///                                 <paramref name="value" /> parameter to. </param>
    /// <returns>   An <see cref="object" /> that represents the converted value. </returns>
    public override object? ConvertTo( ITypeDescriptorContext? context, System.Globalization.CultureInfo? culture, object? value, Type destinationType )
    {
        return ReferenceEquals( destinationType, typeof( string ) ) && value is CornerRadius radius
            ? radius.ToString()
            : base.ConvertTo( context, culture, value, destinationType );
    }
}
