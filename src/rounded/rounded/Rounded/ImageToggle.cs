using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> An image toggle button. </summary>
/// <remarks>
/// (c) 2008 Vartan Simonian. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-03-31 </para>
/// </remarks>
public class ImageToggle : PictureBox, IButtonControl
{
    #region " ibuttoncontrol members "

    /// <summary>
    /// Gets or sets the value returned to the parent form when the button is clicked.
    /// </summary>
    /// <value> One of the <see cref="DialogResult" /> values. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public DialogResult DialogResult { get; set; }

    /// <summary> Gets or sets the is default. </summary>
    /// <value> The is default. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool IsDefault { get; private set; }

    /// <summary>
    /// Notifies a control that it is the default button so that its appearance and behavior is
    /// adjusted accordingly.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> true if the control should behave as a default button; otherwise false. </param>
    public void NotifyDefault( bool value )
    {
        this.IsDefault = value;
    }

    /// <summary>
    /// Generates a <see cref="Control.Click" /> event for the control.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void PerformClick()
    {
        this.OnClick( EventArgs.Empty );
    }

    #endregion

    #region " images "

    /// <summary> True to hover. </summary>
    private bool _hover;

    /// <summary> True to down. </summary>
    private bool _down;

    /// <summary> The checked hover image. </summary>
    private Image? _checkedHoverImage;

    /// <summary> Image to show when the Checked button is hovered over. </summary>
    /// <value> The hover image. </value>
    [Category( "Appearance" )]
    [Description( "Image to show when the button is hovered over." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image? CheckedHoverImage
    {
        get => this._checkedHoverImage;
        set
        {
            this._checkedHoverImage = value;
            if ( this._hover )
            {
                this.Image = value ?? this.Image;
            }
        }
    }

    /// <summary> The checked image. </summary>
    private Image? _checkedImage;

    /// <summary> Image to show when the button is depressed. </summary>
    /// <value> The down image. </value>
    [Category( "Appearance" )]
    [Description( "Image to show when the button is depressed." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image? CheckedImage
    {
        get => this._checkedImage;
        set
        {
            this._checkedImage = value;
            if ( this._down && value is not null )
                this.Image = value;
        }
    }

    /// <summary> The unchecked hover image. </summary>
    private Image? _uncheckedHoverImage;

    /// <summary> Image to show when the unchecked button is hovered over. </summary>
    /// <value> The hover image. </value>
    [Category( "Appearance" )]
    [Description( "Image to show when the button is hovered over." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image? UncheckedHoverImage
    {
        get => this._uncheckedHoverImage;
        set
        {
            this._uncheckedHoverImage = value;
            if ( this._hover && value is not null )
            {
                this.Image = value;
            }
        }
    }

    /// <summary> The unchecked image. </summary>
    private Image? _uncheckedImage;

    /// <summary> Image to show when the button is not in any other state. </summary>
    /// <value> The normal image. </value>
    [Category( "Appearance" )]
    [Description( "Image to show when the button is not in any other state." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image? UncheckedImage
    {
        get => this._uncheckedImage;
        set
        {
            this._uncheckedImage = value;
            if ( value is not null && !(this._hover || this._down) )
            {
                this.Image = value;
            }
        }
    }

    #endregion

    #region " overrides "

    /// <summary>
    /// Gets or sets the text of the <see cref="PictureBox" />.
    /// </summary>
    /// <value> The text of the <see cref="PictureBox" />. </value>
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [Category( "Appearance" )]
    [Description( "The text associated with the control." )]
    [AllowNull]
    public override string Text
    {
        get => base.Text;
        set => base.Text = value;
    }

    /// <summary> Gets or sets the font of the text displayed by the control. </summary>
    /// <value>
    /// The <see cref="Font" /> to apply to the text displayed by the control. The
    /// default is the value of the <see cref="Control.DefaultFont" />
    /// property.
    /// </value>
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [Category( "Appearance" )]
    [Description( "The font used to display text in the control." )]
    [System.Diagnostics.CodeAnalysis.AllowNull]
    public override Font Font
    {
        get => base.Font;
        set => base.Font = value;
    }

    #endregion

    #region " description changes "

    /// <summary>
    /// Controls how the ImageButton will handle image placement and control sizing.
    /// </summary>
    /// <value> The size mode. </value>
    [Description( "Controls how the ImageButton will handle image placement and control sizing." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new PictureBoxSizeMode SizeMode
    {
        get => base.SizeMode;
        set => base.SizeMode = value;
    }

    /// <summary> Controls what type of border the ImageButton should have. </summary>
    /// <value> The border style. </value>
    [Description( "Controls what type of border the ImageButton should have." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new BorderStyle BorderStyle
    {
        get => base.BorderStyle;
        set => base.BorderStyle = value;
    }

    #endregion

    #region " hiding"

    /// <summary> Gets or sets the image. </summary>
    /// <value> The image. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new Image Image
    {
        get => base.Image!;
        set => base.Image = value;
    }

    /// <summary> Gets or sets the background image layout. </summary>
    /// <value> The background image layout. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new ImageLayout BackgroundImageLayout
    {
        get => base.BackgroundImageLayout;
        set => base.BackgroundImageLayout = value;
    }

    /// <summary> Gets or sets the background image. </summary>
    /// <value> The background image. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new Image? BackgroundImage
    {
        get => base.BackgroundImage;
        set => base.BackgroundImage = value;
    }

    /// <summary> Gets or sets the image location. </summary>
    /// <value> The image location. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new string ImageLocation
    {
        get => base.ImageLocation!;
        set => base.ImageLocation = value;
    }

    /// <summary> Gets or sets the error image. </summary>
    /// <value> The error image. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new Image ErrorImage
    {
        get => base.ErrorImage!;
        set => base.ErrorImage = value;
    }

    /// <summary> Gets or sets the initial image. </summary>
    /// <value> The initial image. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new Image InitialImage
    {
        get => base.InitialImage!;
        set => base.InitialImage = value;
    }

    /// <summary> Gets or sets the wait on load. </summary>
    /// <value> The wait on load. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new bool WaitOnLoad
    {
        get => base.WaitOnLoad;
        set => base.WaitOnLoad = value;
    }

    #endregion

    #region " message process "

    /// <summary> The windows message key down. </summary>
    private const int WM_KEYDOWN = 0x100;

    /// <summary> The windows message key up. </summary>
    private const int WM_KEYUP = 0x101;

    /// <summary> True to holding space. </summary>
    private bool _holdingSpace;

    /// <summary>
    /// Preprocesses keyboard or input messages within the message loop before they are dispatched.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="msg"> [in,out] A <see cref="Message" />, passed by
    /// reference, that represents the message to process. The possible values
    /// are WM_KEYDOWN, WM_SYSKEYDOWN, WM_CHAR, and WM_SYSCHAR. </param>
    /// <returns>
    /// <see langword="true" /> if the message was processed by the control; otherwise,
    /// <see langword="false" />.
    /// </returns>
    public override bool PreProcessMessage( ref Message msg )
    {
        if ( msg.Msg == WM_KEYUP )
        {
            if ( this._holdingSpace )
            {
                if ( msg.WParam.ToInt32() is ( int ) Keys.Space )
                {
                    this.OnMouseUp( null );
                    this.PerformClick();
                }
                else if ( msg.WParam.ToInt32() is ( int ) Keys.Escape or ( int ) Keys.Tab )
                {
                    this._holdingSpace = false;
                    this.OnMouseUp( null );
                }
            }

            return true;
        }
        else if ( msg.Msg == WM_KEYDOWN )
        {
            if ( msg.WParam.ToInt32() is ( int ) Keys.Space )
            {
                this._holdingSpace = true;
                this.OnMouseDown( null );
            }
            else if ( msg.WParam.ToInt32() is ( int ) Keys.Enter )
            {
                this.PerformClick();
            }

            return true;
        }
        else
        {
            return base.PreProcessMessage( ref msg );
        }
    }

    #endregion

    #region " mouse events "

    /// <summary> Updates the image on mouse move. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected void UpdateImageOnMouseMove()
    {
        this._hover = true;
        if ( this.ReadOnly )
        {
        }
        else if ( this.Checked )
        {
            if ( this._down )
            {
                if ( this.CheckedImage is not null && !ReferenceEquals( this.Image, this.CheckedImage ) )
                {
                    this.Image = this.CheckedImage;
                }
            }
            else
            {
                this.Image = (this._checkedHoverImage is not null ? this.CheckedHoverImage : this.UncheckedImage) ?? this.Image;
            }
        }
        else
        {
            this.Image = (this._down ? this.CheckedHoverImage ?? (this._uncheckedHoverImage is not null ? this.UncheckedHoverImage : this.UncheckedImage) : this.UncheckedHoverImage ?? this.UncheckedImage) ?? this.Image;
        }
    }

    /// <summary> Raises the <see cref="Control.MouseMove" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseMove( MouseEventArgs e )
    {
        this.UpdateImageOnMouseMove();
        base.OnMouseMove( e );
    }

    /// <summary> Updates the image mouse leave. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected void UpdateImageMouseLeave()
    {
        this._hover = false;
        this.Image = (this.Checked ? this.CheckedImage : this.UncheckedImage) ?? this.Image;
    }

    /// <summary> Raises the <see cref="Control.MouseLeave" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnMouseLeave( EventArgs e )
    {
        this.UpdateImageMouseLeave();
        base.OnMouseLeave( e );
    }

    /// <summary> Raises the <see cref="Control.MouseDown" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseDown( MouseEventArgs? e )
    {
        _ = this.Focus();
        this.OnMouseUp( null );
        this._down = true;
        if ( this.ReadOnly )
        {
        }
        else if ( this.Checked )
        {
            if ( this.CheckedHoverImage is not null )
            {
                this.Image = this.CheckedHoverImage;
            }
        }
        else if ( this.CheckedImage is not null )
        {
            this.Image = this.CheckedImage;
        }
        if ( e is not null ) base.OnMouseDown( e );
    }

    /// <summary> Raises the <see cref="Control.MouseUp" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseUp( MouseEventArgs? e )
    {
        this._down = false;
        if ( this.ReadOnly )
        {
        }
        else if ( this.Checked )
        {
            if ( this._hover )
            {
                if ( this.CheckedHoverImage is not null )
                {
                    this.Image = this.CheckedHoverImage;
                }
            }
            else
            {
                if ( this.CheckedImage is not null )
                    this.Image = this.CheckedImage;
            }
        }
        else if ( this._hover )
        {
            if ( this.UncheckedHoverImage is not null )
            {
                this.Image = this.UncheckedHoverImage;
            }
        }
        else
        {
            if ( this.UncheckedImage is not null )
                this.Image = this.UncheckedImage;
        }
        if ( e is not null ) base.OnMouseUp( e );
    }

    /// <summary> Raises the <see cref="Control.LostFocus" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnLostFocus( EventArgs e )
    {
        this._holdingSpace = false;
        this.OnMouseUp( null );
        if ( e is not null ) base.OnLostFocus( e );
    }

    #endregion

    #region " graphics events "

    /// <summary> Renders the image described by the paint events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="pe"> Paint event information. </param>
    private void RenderImage( PaintEventArgs pe )
    {
        if ( this.Image is not null )
        {
            ColorMatrix matrix = new();
            float value = this.Enabled ? 1.0f : 0.6f;
            matrix.Matrix33 = value;
            Graphics g = pe.Graphics;
            g.Clear( this.BackColor );
            using ImageAttributes attributes = new();
            attributes.SetColorMatrix( matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap );
            using Bitmap bmp = new( this.Image, new Size( this.Width, this.Height ) );
            g.DrawImage( bmp, new Rectangle( 0, 0, bmp.Width, bmp.Height ), 0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel, attributes );
        }
        else
        {
            base.OnPaint( pe );
        }
    }

    /// <summary> Draw text. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="pe"> Paint event information. </param>
    private void DrawText( PaintEventArgs pe )
    {
        if ( !string.IsNullOrEmpty( base.Text ) && pe is not null && base.Font is not null )
        {
            SizeF drawStringSize = pe.Graphics.MeasureString( base.Text, base.Font );
            PointF drawPoint = base.Image is not null ? new PointF( (base.Image.Width / 2) - (( int ) drawStringSize.Width / 2), (base.Image.Height / 2) - (( int ) drawStringSize.Height / 2) ) : new PointF( (this.Width / 2) - (( int ) drawStringSize.Width / 2), (this.Height / 2) - (( int ) drawStringSize.Height / 2) );
            using SolidBrush drawBrush = new( base.ForeColor );
            pe.Graphics.DrawString( base.Text, base.Font, drawBrush, drawPoint );
        }
    }

    /// <summary> Raises the <see cref="Control.Paint" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="pe"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs pe )
    {
        this.RenderImage( pe );
        this.DrawText( pe );
    }

    /// <summary>
    /// Raises the <see cref="Control.TextChanged" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnTextChanged( EventArgs e )
    {
        this.Refresh();
        if ( e is not null ) base.OnTextChanged( e );
    }

    #endregion

    #region " value "

    /// <summary> True to readonly. </summary>
    private bool _readonly;

    /// <summary> Gets or sets the read only value. </summary>
    /// <value> The read only value. </value>
    [Category( "Appearance" )]
    [Description( "Read only value." )]
    [DefaultValue( false )]
    public bool ReadOnly
    {
        get => this._readonly;
        set
        {
            if ( value != this.ReadOnly )
            {
                this._readonly = value;
                this.Refresh();
            }
        }
    }

    /// <summary> True if checked. </summary>
    private bool _checked;

    /// <summary> Gets or sets the checked value. </summary>
    /// <value> The checked value. </value>
    [Category( "Appearance" )]
    [Description( "Checked value." )]
    [DefaultValue( false )]
    public bool Checked
    {
        get => this._checked;
        set
        {
            if ( value != this.Checked )
            {
                this._checked = value;
                this.UpdateImageMouseLeave();
                this.OnCheckChanged( EventArgs.Empty );
            }
        }
    }

    /// <summary> Event queue for all listeners interested in CheckChanged events. </summary>
    public event EventHandler<EventArgs>? CheckChanged;

    /// <summary> Raises the system. event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    protected virtual void OnCheckChanged( EventArgs e )
    {
        this.CheckChanged?.Invoke( this, e );
    }

    /// <summary> Raises the <see cref="Control.Click" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnClick( EventArgs e )
    {
        base.OnClick( e );
        if ( !this.ReadOnly )
        {
            this._checked = !this.Checked;
            this.OnCheckChanged( e );
        }
    }

    #endregion
}
