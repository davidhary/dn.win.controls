using System.Diagnostics;
namespace cc.isr.WinControls.Demo;

/// <summary>   A rounded controls tester. </summary>
/// <remarks>   David, 2021-03-12. </remarks>
public partial class RoundedControlsForm : Form
{
    /// <summary>   Form overrides dispose to clean up the component list. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
    ///                             resources; <see langword="false" /> to release only unmanaged
    ///                             resources. </param>
    [DebuggerNonUserCode()]
    protected override void Dispose(bool disposing)
    {
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose(disposing);
        }
    }

    /// <summary>   Required by the Windows Form Designer. </summary>
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// NOTE: The following procedure is required by the Windows Form Designer It can be modified
    /// using the Windows Form Designer.  
    /// Do not modify it using the code editor.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        _panel = new Panel();
        RoundedPanel1 = new cc.isr.WinControls.RoundedPanel();
        _roundButton = new cc.isr.WinControls.RoundedButton();
        _roundedPanel = new cc.isr.WinControls.RoundedPanel();
        TableLayoutPanel1 = new TableLayoutPanel();
        _roundedLabel = new cc.isr.WinControls.RoundedLabel();
        _roundedPanel.SuspendLayout();
        TableLayoutPanel1.SuspendLayout();
        SuspendLayout();
        // 
        // _panel
        // 
        _panel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        _panel.BorderStyle = BorderStyle.Fixed3D;
        _panel.Location = new Point(45, 145);
        _panel.Name = "_Panel";
        _panel.Size = new Size(251, 51);
        _panel.TabIndex = 2;
        // 
        // RoundedPanel1
        // 
        RoundedPanel1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        RoundedPanel1.BackgroundImageLayout = ImageLayout.None;
        RoundedPanel1.BorderSize = 3;
        RoundedPanel1.BottomLeftRadius = 15;
        RoundedPanel1.BottomRightRadius = 15;
        RoundedPanel1.Location = new Point(46, 205);
        RoundedPanel1.Name = "RoundedPanel1";
        RoundedPanel1.ShadowAngle = 0;
        RoundedPanel1.Size = new Size(251, 49);
        RoundedPanel1.TabIndex = 5;
        RoundedPanel1.TopLeftRadius = 30;
        RoundedPanel1.TopRightRadius = 50;
        // 
        // _roundButton
        // 
        _roundButton.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        _roundButton.BorderSize = 3;
        _roundButton.BottomLeftRadius = 13;
        _roundButton.BottomRightRadius = 15;
        _roundButton.Cursor = Cursors.Default;
        _roundButton.Location = new Point(177, 85);
        _roundButton.Name = "_RoundButton";
        _roundButton.ShadowAngle = 0;
        _roundButton.Size = new Size(126, 45);
        _roundButton.TabIndex = 4;
        _roundButton.Text = "Button";
        _roundButton.TopLeftRadius = 15;
        _roundButton.TopRightRadius = 15;
        _roundButton.UseVisualStyleBackColor = true;
        // 
        // _roundedPanel
        // 
        _roundedPanel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        _roundedPanel.BackgroundImageLayout = ImageLayout.None;
        _roundedPanel.BorderSize = 3;
        _roundedPanel.BottomLeftRadius = 50;
        _roundedPanel.BottomRightRadius = 50;
        _roundedPanel.Controls.Add(TableLayoutPanel1);
        _roundedPanel.Location = new Point(62, 12);
        _roundedPanel.Name = "_RoundedPanel";
        _roundedPanel.Padding = new Padding(6);
        _roundedPanel.ShadowAngle = 0;
        _roundedPanel.Size = new Size(251, 64);
        _roundedPanel.TabIndex = 1;
        _roundedPanel.TopLeftRadius = 30;
        _roundedPanel.TopRightRadius = 50;
        // 
        // TableLayoutPanel1
        // 
        TableLayoutPanel1.BackColor = Color.Transparent;
        TableLayoutPanel1.ColumnCount = 3;
        TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 3.0f));
        TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
        TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 3.0f));
        TableLayoutPanel1.Controls.Add(_roundedLabel, 1, 1);
        TableLayoutPanel1.Dock = DockStyle.Fill;
        TableLayoutPanel1.Location = new Point(6, 6);
        TableLayoutPanel1.Name = "TableLayoutPanel1";
        TableLayoutPanel1.RowCount = 3;
        TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
        TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
        TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
        TableLayoutPanel1.Size = new Size(239, 52);
        TableLayoutPanel1.TabIndex = 0;
        // 
        // _roundedLabel
        // 
        _roundedLabel.BackColor = SystemColors.Control;
        _roundedLabel.BorderSize = 3;
        _roundedLabel.BottomLeftRadius = 23;
        _roundedLabel.BottomRightRadius = 23;
        _roundedLabel.Dock = DockStyle.Fill;
        _roundedLabel.Location = new Point(6, 6);
        _roundedLabel.Margin = new Padding(3);
        _roundedLabel.Name = "_RoundedLabel";
        _roundedLabel.ShadowAngle = 0;
        _roundedLabel.Size = new Size(227, 40);
        _roundedLabel.TabIndex = 0;
        _roundedLabel.Text = "RoundedLabel1";
        _roundedLabel.TextAlign = ContentAlignment.MiddleCenter;
        _roundedLabel.TopLeftRadius = 23;
        _roundedLabel.TopRightRadius = 23;
        // 
        // RoundedControlsTester
        // 
        AutoScaleDimensions = new SizeF(6.0f, 13.0f);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(348, 309);
        Controls.Add(RoundedPanel1);
        Controls.Add(_roundButton);
        Controls.Add(_panel);
        Controls.Add(_roundedPanel);
        Name = "RoundedControlsTester";
        Text = "Rounded Controls Tester";
        _roundedPanel.ResumeLayout(false);
        TableLayoutPanel1.ResumeLayout(false);
        ResumeLayout(false);
    }

    /// <summary>   The panel. </summary>
    private Panel _panel;

    /// <summary>   The rounded panel. </summary>
    private cc.isr.WinControls.RoundedPanel _roundedPanel;

    /// <summary>   The rounded label. </summary>
    private RoundedLabel _roundedLabel;

    /// <summary>   The round button. </summary>
    private RoundedButton _roundButton;

    /// <summary>   The first rounded panel. </summary>
    private cc.isr.WinControls.RoundedPanel RoundedPanel1;

    /// <summary>   The first table layout panel. </summary>
    private TableLayoutPanel TableLayoutPanel1;
}
