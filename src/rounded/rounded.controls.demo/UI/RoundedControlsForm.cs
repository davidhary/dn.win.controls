namespace cc.isr.WinControls.Demo;

/// <summary>   A rounded controls form. </summary>
/// <remarks>   David, 2020-10-25. </remarks>
public partial class RoundedControlsForm
{
    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public RoundedControlsForm()
    {
        // This call is required by the designer.
        this.InitializeComponent();

        // Add any initialization after the InitializeComponent() call.

        // Display the OK close button. 
        this.MinimizeBox = false;
        this.Text = "Controls tester";
    }
}
