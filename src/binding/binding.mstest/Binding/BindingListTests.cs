using cc.isr.Std.BindingLists;

namespace cc.isr.WinControls.MSTest;

/// <summary> Collections Tests. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-02-18. </para>
/// </remarks>
[TestClass]
public class BindingListTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            Console.WriteLine( $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}" );
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets the test context which provides information about and functionality for the current test
    /// run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " sortable binding list "

    /// <summary> A sale. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    private sealed class Sale
    {
        /// <summary> Initializes a new instance of the <see cref="object" /> class. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public Sale() => this.SaleDate = DateTimeOffset.Now;

        /// <summary> Gets the sale details. </summary>
        /// <value> The sale details. </value>
        public SortableBindingList<SaleDetail>? SaleDetails { get; set; }

        /// <summary> Gets the salesman. </summary>
        /// <value> The salesman. </value>
        public string? Salesman { get; set; }

        /// <summary> Gets the client. </summary>
        /// <value> The client. </value>
        public string? Client { get; set; }

        /// <summary> Gets the sale date. </summary>
        /// <value> The sale date. </value>
        public DateTimeOffset SaleDate { get; set; }

        /// <summary> Gets the total number of amount. </summary>
        /// <value> The total number of amount. </value>
        public decimal TotalAmount
        {
            get
            {
                Debug.Assert( this.SaleDetails is not null );
                return this.SaleDetails.Sum( a => a.TotalAmount );
            }
        }
    }

    /// <summary> A sale detail. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    private sealed class SaleDetail
    {
        /// <summary> Gets the product. </summary>
        /// <value> The product. </value>
        public string? Product { get; set; }

        /// <summary> Gets the quantity. </summary>
        /// <value> The quantity. </value>
        public int Quantity { get; set; }

        /// <summary> Gets the unit price. </summary>
        /// <value> The unit price. </value>
        public decimal UnitPrice { get; set; }

        /// <summary> Gets the total number of amount. </summary>
        /// <value> The total number of amount. </value>
        public decimal TotalAmount => this.UnitPrice * this.Quantity;
    }

    /// <summary> Sortable list. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <returns> A list of. </returns>
    private static SortableBindingList<Sale> SortableList()
    {
        Sale[] sales = [
            new Sale()
            {
                Client = "Jane Doe",
                SaleDate = new DateTime(2008, 1, 10),
                Salesman = "John",
                SaleDetails = [new() { Product = "Sportsman", Quantity = 1, UnitPrice = 80m }, new() { Product = "Tusker Malt", Quantity = 2, UnitPrice = 100m }, new() { Product = "Alvaro", Quantity = 1, UnitPrice = 50m }]
            },
            new Sale()
            {
                Client = "Ben Jones",
                SaleDate = new DateTime(2008, 1, 11),
                Salesman = "Danny",
                SaleDetails = [new() { Product = "Embassy Kings", Quantity = 1, UnitPrice = 80m }, new() { Product = "Tusker", Quantity = 5, UnitPrice = 100m }, new() { Product = "Movie", Quantity = 3, UnitPrice = 50m }]
            },
            new Sale()
            {
                Client = "Tim Kim",
                SaleDate = new DateTime(2008, 1, 12),
                Salesman = "Kaplan",
                SaleDetails = [new() { Product = "Citizen Special", Quantity = 10, UnitPrice = 30m }, new() { Product = "Burn", Quantity = 2, UnitPrice = 100m }]
            }
        ];
        return new SortableBindingList<Sale>( [.. sales] );
    }

    /// <summary> (Unit Test Method) tests sortable binding list. </summary>
    /// <remarks> Tested 2019-05-14. </remarks>
    [TestMethod()]
    public void SortableBindingListTest()
    {
        SortableBindingList<Sale> sales = SortableList();
        List<Sale> l = [.. sales.OrderBy( x => x.Client )];
        Assert.IsTrue( string.Compare( l[0].Client, l[1].Client, StringComparison.Ordinal ) < 0, $"{l[0].Client} must come before {l[1].Client}" );
        l = [.. sales.OrderByDescending( x => x.Client )];
        Assert.IsTrue( string.Compare( l[0].Client, l[1].Client, StringComparison.Ordinal ) > 0, $"{l[0].Client} must come after {l[1].Client}" );
        // sales.OrderByDescending(Of String)(Function(x) x.Client)
        string expectedFirstClient = "Abe";
        sales.Add( new Sale()
        {
            Client = expectedFirstClient,
            SaleDate = new DateTime( 2009, 1, 12 ),
            Salesman = "Kaplan",
            SaleDetails = [new() { Product = "Pepsi", Quantity = 4, UnitPrice = 2.8m }, new() { Product = "Perrier", Quantity = 12, UnitPrice = 1.5m }]
        } );
        string expectedLastClient = "Yoda";
        sales.Add( new Sale()
        {
            Client = expectedLastClient,
            SaleDate = new DateTime( 2009, 1, 12 ),
            Salesman = "Kaplan",
            SaleDetails = [new() { Product = "Cola", Quantity = 4, UnitPrice = 3.1m }, new() { Product = "Avian", Quantity = 12, UnitPrice = 1.5m }]
        } );
    }

    /// <summary> (Unit Test Method) tests data grid view sortable binding list. </summary>
    /// <remarks> Tested 2019-05-14. </remarks>
    [TestMethod]
    public void DataGridViewSortableBindingListTest()
    {
        using Form panel = new();
        using DataGridView grid = new();
        // grid.CreateControl()
        panel.Controls.Add( grid );
        // panel.PerformLayout()
        // grid.PerformLayout()
        SortableBindingList<Sale> sales = SortableList();
        grid.DataSource = sales;
        // grid has a header row.
        int expectedItemCount = sales.Count + 1;
        Assert.AreEqual( expectedItemCount, grid.Rows.Count, "Expected row count" );
        string? firstValue = grid.Rows[0].Cells[nameof( Sale.Client )].Value!.ToString();
        string? secondValue = grid.Rows[1].Cells[nameof( Sale.Client )].Value!.ToString();
        Assert.IsTrue( string.Compare( firstValue, secondValue, StringComparison.Ordinal ) > 0, $"Before sorting {firstValue} must come before {secondValue}" );

        // test sort ascending
        System.ComponentModel.ListSortDirection direction = System.ComponentModel.ListSortDirection.Ascending;
        grid.Sort( grid.Columns[nameof( Sale.Client )]!, direction );
        firstValue = grid.Rows[0].Cells[nameof( Sale.Client )].Value!.ToString();
        secondValue = grid.Rows[1].Cells[nameof( Sale.Client )].Value!.ToString();
        Assert.IsTrue( string.Compare( firstValue, secondValue, StringComparison.Ordinal ) < 0, $"After sorting {direction} {firstValue} must come before {secondValue}" );

        // test sort descending
        direction = System.ComponentModel.ListSortDirection.Descending;
        grid.Sort( grid.Columns[nameof( Sale.Client )]!, direction );
        firstValue = grid.Rows[0].Cells[nameof( Sale.Client )].Value!.ToString();
        secondValue = grid.Rows[1].Cells[nameof( Sale.Client )].Value!.ToString();
        Assert.IsTrue( string.Compare( firstValue, secondValue, StringComparison.Ordinal ) > 0, $"After sorting {direction} {firstValue} must come before {secondValue}" );
    }

    #endregion
}
