# About

cc.isr.WinControls.Binding is a .Net library providing biding extensions for Windows Forms controls.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

cc.isr.WinControls.Binding is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Win.Controls Repository].

[Win.Controls Repository]: https://bitbucket.org/davidhary/dn.win.controls

