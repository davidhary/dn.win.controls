namespace cc.isr.WinControls.Demo;
public partial class BlackForm
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if ( disposing )
        {
            components?.Dispose();
        }

        base.Dispose(disposing);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        _topBorderPanel = new Panel();
        _topBorderPanel.MouseDown += new MouseEventHandler(TopBorderPanel_MouseDown);
        _topBorderPanel.MouseMove += new MouseEventHandler(TopBorderPanel_MouseMove);
        _topBorderPanel.MouseUp += new MouseEventHandler(TopBorderPanel_MouseUp);
        _rightBorderPanel = new Panel();
        _rightBorderPanel.MouseDown += new MouseEventHandler(RightPanel_MouseDown);
        _rightBorderPanel.MouseMove += new MouseEventHandler(RightPanel_MouseMove);
        _rightBorderPanel.MouseUp += new MouseEventHandler(RightPanel_MouseUp);
        _leftBorderPanel = new Panel();
        _leftBorderPanel.MouseDown += new MouseEventHandler(LeftPanel_MouseDown);
        _leftBorderPanel.MouseMove += new MouseEventHandler(LeftPanel_MouseMove);
        _leftBorderPanel.MouseUp += new MouseEventHandler(LeftPanel_MouseUp);
        _bottomBorderPanel = new Panel();
        _bottomBorderPanel.MouseDown += new MouseEventHandler(BottomPanel_MouseDown);
        _bottomBorderPanel.MouseMove += new MouseEventHandler(BottomPanel_MouseMove);
        _bottomBorderPanel.MouseUp += new MouseEventHandler(BottomPanel_MouseUp);
        _topPanel = new Panel();
        _topPanel.MouseDown += new MouseEventHandler(TopPanel_MouseDown);
        _topPanel.MouseMove += new MouseEventHandler(TopPanel_MouseMove);
        _topPanel.MouseUp += new MouseEventHandler(TopPanel_MouseUp);
        _minButton = new ZopeButton();
        _minButton.Click += new System.EventHandler(MinButton_Click);
        _maxButton = new MinMaxButton();
        _maxButton.Click += new System.EventHandler(MaxButton_Click);
        _windowTextLabel = new Label();
        _windowTextLabel.MouseDown += new MouseEventHandler(WindowTextLabel_MouseDown);
        _windowTextLabel.MouseMove += new MouseEventHandler(WindowTextLabel_MouseMove);
        _windowTextLabel.MouseUp += new MouseEventHandler(WindowTextLabel_MouseUp);
        _closeButton = new ZopeButton();
        _closeButton.Click += new System.EventHandler(CloseButton_Click);
        _menuStrip = new ZopeMenuStrip();
        _fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _toolStripSeparator1 = new ToolStripSeparator();
        _saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _toolStripSeparator2 = new ToolStripSeparator();
        _closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _closeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _toolStripSeparator3 = new ToolStripSeparator();
        _printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _toolStripSeparator4 = new ToolStripSeparator();
        _closeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        _editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _toolStripSeparator5 = new ToolStripSeparator();
        _undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _toolStripSeparator6 = new ToolStripSeparator();
        _findToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _replaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _helpContentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _onlineHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _buyNowShapedButton = new ShapedButton();
        _bottomBorderRightPanel = new Panel();
        _bottomBorderRightPanel.MouseDown += new MouseEventHandler(RightBottomPanel_1_MouseDown);
        _bottomBorderRightPanel.MouseMove += new MouseEventHandler(RightBottomPanel_1_MouseMove);
        _bottomBorderRightPanel.MouseUp += new MouseEventHandler(RightBottomPanel_1_MouseUp);
        _bottomPanel = new Panel();
        _timesShapedButton = new ShapedButton();
        _testShapedButton = new ShapedButton();
        _addShapedButton = new ShapedButton();
        _toolsShapedButton = new ShapedButton();
        _toolTip = new ToolTip(components);
        _rightBorderBottomPanel = new Panel();
        _rightBorderBottomPanel.MouseDown += new MouseEventHandler(RightBottomPanel_2_MouseDown);
        _rightBorderBottomPanel.MouseMove += new MouseEventHandler(RightBottomPanel_2_MouseMove);
        _rightBorderBottomPanel.MouseUp += new MouseEventHandler(RightBottomPanel_2_MouseUp);
        _bottomBorderLeftPanel = new Panel();
        _bottomBorderLeftPanel.MouseDown += new MouseEventHandler(LeftBottomPanel_1_MouseDown);
        _bottomBorderLeftPanel.MouseMove += new MouseEventHandler(LeftBottomPanel_1_MouseMove);
        _bottomBorderLeftPanel.MouseUp += new MouseEventHandler(LeftBottomPanel_1_MouseUp);
        _leftBorderBottomPanel = new Panel();
        _leftBorderBottomPanel.MouseDown += new MouseEventHandler(LeftBottomPanel_2_MouseDown);
        _leftBorderBottomPanel.MouseMove += new MouseEventHandler(LeftBottomPanel_2_MouseMove);
        _leftBorderBottomPanel.MouseUp += new MouseEventHandler(LeftBottomPanel_2_MouseUp);
        _rightBorderTopPanel = new Panel();
        _rightBorderTopPanel.MouseDown += new MouseEventHandler(RightTopPanel_1_MouseDown);
        _rightBorderTopPanel.MouseMove += new MouseEventHandler(RightTopPanel_1_MouseMove);
        _rightBorderTopPanel.MouseUp += new MouseEventHandler(RightTopPanel_1_MouseUp);
        _topBorderRightPanel = new Panel();
        _topBorderRightPanel.MouseDown += new MouseEventHandler(RightTopPanel_2_MouseDown);
        _topBorderRightPanel.MouseMove += new MouseEventHandler(RightTopPanel_2_MouseMove);
        _topBorderRightPanel.MouseUp += new MouseEventHandler(RightTopPanel_2_MouseUp);
        _topBorderLeftPanel = new Panel();
        _topBorderLeftPanel.MouseDown += new MouseEventHandler(LeftTopPanel_1_MouseDown);
        _topBorderLeftPanel.MouseMove += new MouseEventHandler(LeftTopPanel_1_MouseMove);
        _topBorderLeftPanel.MouseUp += new MouseEventHandler(LeftTopPanel_1_MouseUp);
        _leftBorderTopPanel = new Panel();
        _leftBorderTopPanel.MouseDown += new MouseEventHandler(LeftTopPanel_2_MouseDown);
        _leftBorderTopPanel.MouseMove += new MouseEventHandler(LeftTopPanel_2_MouseMove);
        _leftBorderTopPanel.MouseUp += new MouseEventHandler(LeftTopPanel_2_MouseUp);
        _leftPanel = new Panel();
        _helpButton = new ZopeButton();
        _helpButton.Click += new System.EventHandler(Help_button_Click);
        _runButton = new ZopeButton();
        _runButton.Click += new System.EventHandler(Run_button_Click);
        _viewButton = new ZopeButton();
        _viewButton.Click += new System.EventHandler(View_button_Click);
        _editButton = new ZopeButton();
        _editButton.Click += new System.EventHandler(Edit_button_Click);
        _fileButton = new ZopeButton();
        _fileButton.Click += new System.EventHandler(File_button_Click);
        _separatorPanel = new Panel();
        _informationLabel = new Label();
        _topPanel.SuspendLayout();
        _menuStrip.SuspendLayout();
        _bottomPanel.SuspendLayout();
        _leftPanel.SuspendLayout();
        SuspendLayout();
        // 
        // _topBorderPanel
        // 
        _topBorderPanel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        _topBorderPanel.BackColor = Color.Black;
        _topBorderPanel.Cursor = Cursors.SizeNS;
        _topBorderPanel.Location = new Point(20, 0);
        _topBorderPanel.Name = "_TopBorderPanel";
        _topBorderPanel.Size = new Size(690, 2);
        _topBorderPanel.TabIndex = 0;
        // 
        // _rightBorderPanel
        // 
        _rightBorderPanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
        _rightBorderPanel.BackColor = Color.Black;
        _rightBorderPanel.Cursor = Cursors.SizeWE;
        _rightBorderPanel.Location = new Point(728, 22);
        _rightBorderPanel.Name = "_RightBorderPanel";
        _rightBorderPanel.Size = new Size(2, 430);
        _rightBorderPanel.TabIndex = 1;
        // 
        // _leftBorderPanel
        // 
        _leftBorderPanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
        _leftBorderPanel.BackColor = Color.Black;
        _leftBorderPanel.Cursor = Cursors.SizeWE;
        _leftBorderPanel.Location = new Point(0, 20);
        _leftBorderPanel.Name = "_LeftBorderPanel";
        _leftBorderPanel.Size = new Size(2, 430);
        _leftBorderPanel.TabIndex = 2;
        // 
        // _bottomBorderPanel
        // 
        _bottomBorderPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
        _bottomBorderPanel.BackColor = Color.Black;
        _bottomBorderPanel.Cursor = Cursors.SizeNS;
        _bottomBorderPanel.Location = new Point(15, 471);
        _bottomBorderPanel.Name = "_BottomBorderPanel";
        _bottomBorderPanel.Size = new Size(692, 2);
        _bottomBorderPanel.TabIndex = 3;
        // 
        // _topPanel
        // 
        _topPanel.BackColor = Color.FromArgb(30, 30, 30);
        _topPanel.Controls.Add(_minButton);
        _topPanel.Controls.Add(_maxButton);
        _topPanel.Controls.Add(_windowTextLabel);
        _topPanel.Controls.Add(_closeButton);
        _topPanel.Controls.Add(_menuStrip);
        _topPanel.Dock = DockStyle.Top;
        _topPanel.Location = new Point(0, 0);
        _topPanel.Name = "_TopPanel";
        _topPanel.Size = new Size(730, 76);
        _topPanel.TabIndex = 4;
        // 
        // _minButton
        // 
        _minButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        _minButton.BusyBackColor = Color.FromArgb(30, 30, 30);
        _minButton.DisplayText = "_";
        _minButton.FlatStyle = FlatStyle.Flat;
        _minButton.Font = new Font("Microsoft YaHei UI", 20.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _minButton.ForeColor = Color.White;
        _minButton.Location = new Point(632, 6);
        _minButton.MouseClickColor = Color.FromArgb(60, 60, 160);
        _minButton.MouseColorsEnabled = true;
        _minButton.MouseHoverColor = Color.FromArgb(50, 50, 50);
        _minButton.Name = "_MinButton";
        _minButton.Size = new Size(31, 24);
        _minButton.TabIndex = 4;
        _minButton.Text = "_";
        _minButton.TextLocationLeft = 6;
        _minButton.TextLocationTop = -20;
        _toolTip.SetToolTip(_minButton, "Minimize");
        _minButton.UseVisualStyleBackColor = true;
        // 
        // _maxButton
        // 
        _maxButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        _maxButton.BusyBackColor = Color.FromArgb(30, 30, 30);
        _maxButton.CustomFormState = CustomFormState.Normal;
        _maxButton.DisplayText = "_";
        _maxButton.FlatStyle = FlatStyle.Flat;
        _maxButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _maxButton.ForeColor = Color.White;
        _maxButton.Location = new Point(663, 6);
        _maxButton.MouseClickColor = Color.FromArgb(60, 60, 160);
        _maxButton.MouseHoverColor = Color.FromArgb(50, 50, 50);
        _maxButton.Name = "_MaxButton";
        _maxButton.Size = new Size(31, 24);
        _maxButton.TabIndex = 2;
        _maxButton.Text = "minMaxButton1";
        _maxButton.TextLocation = new Point(8, 6);
        _toolTip.SetToolTip(_maxButton, "Maximize");
        _maxButton.UseVisualStyleBackColor = true;
        // 
        // _windowTextLabel
        // 
        _windowTextLabel.AutoSize = true;
        _windowTextLabel.Font = new Font("Microsoft Sans Serif", 26.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _windowTextLabel.ForeColor = Color.White;
        _windowTextLabel.Location = new Point(28, 21);
        _windowTextLabel.Name = "_WindowTextLabel";
        _windowTextLabel.Size = new Size(134, 39);
        _windowTextLabel.TabIndex = 1;
        _windowTextLabel.Text = "My App";
        // 
        // _closeButton
        // 
        _closeButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        _closeButton.BusyBackColor = Color.FromArgb(30, 30, 30);
        _closeButton.DisplayText = "X";
        _closeButton.FlatStyle = FlatStyle.Flat;
        _closeButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _closeButton.ForeColor = Color.White;
        _closeButton.Location = new Point(694, 6);
        _closeButton.MouseClickColor = Color.FromArgb(60, 60, 160);
        _closeButton.MouseColorsEnabled = true;
        _closeButton.MouseHoverColor = Color.FromArgb(50, 50, 50);
        _closeButton.Name = "_CloseButton";
        _closeButton.Size = new Size(31, 24);
        _closeButton.TabIndex = 0;
        _closeButton.Text = "X";
        _closeButton.TextLocationLeft = 6;
        _closeButton.TextLocationTop = 1;
        _toolTip.SetToolTip(_closeButton, "Close");
        _closeButton.UseVisualStyleBackColor = true;
        // 
        // _menuStrip
        // 
        _menuStrip.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        _menuStrip.Dock = DockStyle.None;
        _menuStrip.Font = new Font("Segoe UI", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _menuStrip.Items.AddRange(new ToolStripItem[] { _fileToolStripMenuItem, _editToolStripMenuItem, _helpToolStripMenuItem });
        _menuStrip.Location = new Point(421, 32);
        _menuStrip.Name = "_MenuStrip";
        _menuStrip.Size = new Size(200, 28);
        _menuStrip.TabIndex = 19;
        _menuStrip.Text = "menuStripZ1";
        // 
        // _fileToolStripMenuItem
        // 
        _fileToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _newToolStripMenuItem, _openToolStripMenuItem, _toolStripSeparator1, _saveToolStripMenuItem, _saveAsToolStripMenuItem, _toolStripSeparator2, _closeToolStripMenuItem, _closeAllToolStripMenuItem, _toolStripSeparator3, _printToolStripMenuItem, _printPreviewToolStripMenuItem, _toolStripSeparator4, _closeToolStripMenuItem1 });
        _fileToolStripMenuItem.ForeColor = Color.White;
        _fileToolStripMenuItem.Name = "_FileToolStripMenuItem";
        _fileToolStripMenuItem.Size = new Size(60, 24);
        _fileToolStripMenuItem.Text = "  File  ";
        // 
        // _newToolStripMenuItem
        // 
        _newToolStripMenuItem.ForeColor = Color.White;
        _newToolStripMenuItem.Name = "_NewToolStripMenuItem";
        _newToolStripMenuItem.Size = new Size(240, 24);
        _newToolStripMenuItem.Text = "New                                 ";
        // 
        // _openToolStripMenuItem
        // 
        _openToolStripMenuItem.ForeColor = Color.White;
        _openToolStripMenuItem.Name = "_OpenToolStripMenuItem";
        _openToolStripMenuItem.Size = new Size(240, 24);
        _openToolStripMenuItem.Text = "Open";
        // 
        // _toolStripSeparator1
        // 
        _toolStripSeparator1.Name = "_ToolStripSeparator1";
        _toolStripSeparator1.Size = new Size(237, 6);
        // 
        // _saveToolStripMenuItem
        // 
        _saveToolStripMenuItem.ForeColor = Color.White;
        _saveToolStripMenuItem.Name = "_SaveToolStripMenuItem";
        _saveToolStripMenuItem.Size = new Size(240, 24);
        _saveToolStripMenuItem.Text = "Save";
        // 
        // _saveAsToolStripMenuItem
        // 
        _saveAsToolStripMenuItem.ForeColor = Color.White;
        _saveAsToolStripMenuItem.Name = "_SaveAsToolStripMenuItem";
        _saveAsToolStripMenuItem.Size = new Size(240, 24);
        _saveAsToolStripMenuItem.Text = "Save As";
        // 
        // _toolStripSeparator2
        // 
        _toolStripSeparator2.Name = "_ToolStripSeparator2";
        _toolStripSeparator2.Size = new Size(237, 6);
        // 
        // _closeToolStripMenuItem
        // 
        _closeToolStripMenuItem.ForeColor = Color.White;
        _closeToolStripMenuItem.Name = "_CloseToolStripMenuItem";
        _closeToolStripMenuItem.Size = new Size(240, 24);
        _closeToolStripMenuItem.Text = "Close";
        // 
        // _closeAllToolStripMenuItem
        // 
        _closeAllToolStripMenuItem.ForeColor = Color.White;
        _closeAllToolStripMenuItem.Name = "_CloseAllToolStripMenuItem";
        _closeAllToolStripMenuItem.Size = new Size(240, 24);
        _closeAllToolStripMenuItem.Text = "Close All";
        // 
        // _toolStripSeparator3
        // 
        _toolStripSeparator3.Name = "_ToolStripSeparator3";
        _toolStripSeparator3.Size = new Size(237, 6);
        // 
        // _printToolStripMenuItem
        // 
        _printToolStripMenuItem.ForeColor = Color.White;
        _printToolStripMenuItem.Name = "_PrintToolStripMenuItem";
        _printToolStripMenuItem.Size = new Size(240, 24);
        _printToolStripMenuItem.Text = "Print";
        // 
        // _printPreviewToolStripMenuItem
        // 
        _printPreviewToolStripMenuItem.ForeColor = Color.White;
        _printPreviewToolStripMenuItem.Name = "_PrintPreviewToolStripMenuItem";
        _printPreviewToolStripMenuItem.Size = new Size(240, 24);
        _printPreviewToolStripMenuItem.Text = "Print Preview";
        // 
        // _toolStripSeparator4
        // 
        _toolStripSeparator4.Name = "_ToolStripSeparator4";
        _toolStripSeparator4.Size = new Size(237, 6);
        // 
        // _closeToolStripMenuItem1
        // 
        _closeToolStripMenuItem1.ForeColor = Color.White;
        _closeToolStripMenuItem1.Name = "_CloseToolStripMenuItem1";
        _closeToolStripMenuItem1.Size = new Size(240, 24);
        _closeToolStripMenuItem1.Text = "Close";
        // 
        // _editToolStripMenuItem
        // 
        _editToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _cutToolStripMenuItem, _copyToolStripMenuItem, _pasteToolStripMenuItem, _toolStripSeparator5, _undoToolStripMenuItem, _redoToolStripMenuItem, _toolStripSeparator6, _findToolStripMenuItem, _replaceToolStripMenuItem, _selectAllToolStripMenuItem });
        _editToolStripMenuItem.ForeColor = Color.White;
        _editToolStripMenuItem.Name = "_EditToolStripMenuItem";
        _editToolStripMenuItem.Size = new Size(63, 24);
        _editToolStripMenuItem.Text = "  Edit  ";
        // 
        // _cutToolStripMenuItem
        // 
        _cutToolStripMenuItem.ForeColor = Color.White;
        _cutToolStripMenuItem.Name = "_CutToolStripMenuItem";
        _cutToolStripMenuItem.Size = new Size(216, 24);
        _cutToolStripMenuItem.Text = "Cut                             ";
        // 
        // _copyToolStripMenuItem
        // 
        _copyToolStripMenuItem.ForeColor = Color.White;
        _copyToolStripMenuItem.Name = "_CopyToolStripMenuItem";
        _copyToolStripMenuItem.Size = new Size(216, 24);
        _copyToolStripMenuItem.Text = "Copy";
        // 
        // _pasteToolStripMenuItem
        // 
        _pasteToolStripMenuItem.ForeColor = Color.White;
        _pasteToolStripMenuItem.Name = "_PasteToolStripMenuItem";
        _pasteToolStripMenuItem.Size = new Size(216, 24);
        _pasteToolStripMenuItem.Text = "Paste";
        // 
        // _toolStripSeparator5
        // 
        _toolStripSeparator5.Name = "_ToolStripSeparator5";
        _toolStripSeparator5.Size = new Size(213, 6);
        // 
        // _undoToolStripMenuItem
        // 
        _undoToolStripMenuItem.ForeColor = Color.White;
        _undoToolStripMenuItem.Name = "_UndoToolStripMenuItem";
        _undoToolStripMenuItem.Size = new Size(216, 24);
        _undoToolStripMenuItem.Text = "Undo";
        // 
        // _redoToolStripMenuItem
        // 
        _redoToolStripMenuItem.ForeColor = Color.White;
        _redoToolStripMenuItem.Name = "_RedoToolStripMenuItem";
        _redoToolStripMenuItem.Size = new Size(216, 24);
        _redoToolStripMenuItem.Text = "Redo";
        // 
        // _toolStripSeparator6
        // 
        _toolStripSeparator6.Name = "_ToolStripSeparator6";
        _toolStripSeparator6.Size = new Size(213, 6);
        // 
        // _findToolStripMenuItem
        // 
        _findToolStripMenuItem.ForeColor = Color.White;
        _findToolStripMenuItem.Name = "_FindToolStripMenuItem";
        _findToolStripMenuItem.Size = new Size(216, 24);
        _findToolStripMenuItem.Text = "Find";
        // 
        // _replaceToolStripMenuItem
        // 
        _replaceToolStripMenuItem.ForeColor = Color.White;
        _replaceToolStripMenuItem.Name = "_ReplaceToolStripMenuItem";
        _replaceToolStripMenuItem.Size = new Size(216, 24);
        _replaceToolStripMenuItem.Text = "Replace";
        // 
        // _selectAllToolStripMenuItem
        // 
        _selectAllToolStripMenuItem.ForeColor = Color.White;
        _selectAllToolStripMenuItem.Name = "_SelectAllToolStripMenuItem";
        _selectAllToolStripMenuItem.Size = new Size(216, 24);
        _selectAllToolStripMenuItem.Text = "Select All";
        // 
        // _helpToolStripMenuItem
        // 
        _helpToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _helpContentsToolStripMenuItem, _onlineHelpToolStripMenuItem, _aboutToolStripMenuItem });
        _helpToolStripMenuItem.ForeColor = Color.White;
        _helpToolStripMenuItem.Name = "_HelpToolStripMenuItem";
        _helpToolStripMenuItem.Size = new Size(69, 24);
        _helpToolStripMenuItem.Text = "  Help  ";
        // 
        // _helpContentsToolStripMenuItem
        // 
        _helpContentsToolStripMenuItem.ForeColor = Color.White;
        _helpContentsToolStripMenuItem.Name = "_HelpContentsToolStripMenuItem";
        _helpContentsToolStripMenuItem.Size = new Size(172, 24);
        _helpContentsToolStripMenuItem.Text = "Help Contents";
        // 
        // _onlineHelpToolStripMenuItem
        // 
        _onlineHelpToolStripMenuItem.ForeColor = Color.White;
        _onlineHelpToolStripMenuItem.Name = "_OnlineHelpToolStripMenuItem";
        _onlineHelpToolStripMenuItem.Size = new Size(172, 24);
        _onlineHelpToolStripMenuItem.Text = "Online Help";
        // 
        // _aboutToolStripMenuItem
        // 
        _aboutToolStripMenuItem.ForeColor = Color.White;
        _aboutToolStripMenuItem.Name = "_aboutToolStripMenuItem";
        _aboutToolStripMenuItem.Size = new Size(172, 24);
        _aboutToolStripMenuItem.Text = "About";
        // 
        // _buyNowShapedButton
        // 
        _buyNowShapedButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        _buyNowShapedButton.BackColor = Color.Transparent;
        _buyNowShapedButton.BorderColor = Color.Transparent;
        _buyNowShapedButton.BorderWidth = 2;
        _buyNowShapedButton.ButtonShape = ButtonShape.RoundRect;
        _buyNowShapedButton.ButtonText = "Buy Now";
        _buyNowShapedButton.EndColor = Color.FromArgb(20, 20, 40);
        _buyNowShapedButton.EndOpacity = 250;
        _buyNowShapedButton.FlatAppearance.BorderSize = 0;
        _buyNowShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
        _buyNowShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
        _buyNowShapedButton.FlatStyle = FlatStyle.Flat;
        _buyNowShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _buyNowShapedButton.ForeColor = Color.White;
        _buyNowShapedButton.GradientAngle = 90;
        _buyNowShapedButton.Location = new Point(427, 10);
        _buyNowShapedButton.MouseClickStartColor = Color.Black;
        _buyNowShapedButton.MouseClickEndColor = Color.Black;
        _buyNowShapedButton.MouseHoverStartColor = Color.FromArgb(10, 10, 30);
        _buyNowShapedButton.MouseHoverEndColor = Color.FromArgb(20, 20, 80);
        _buyNowShapedButton.Name = "_BuyNowShapedButton";
        _buyNowShapedButton.ShowButtonText = true;
        _buyNowShapedButton.Size = new Size(145, 54);
        _buyNowShapedButton.StartColor = Color.FromArgb(30, 70, 190);
        _buyNowShapedButton.StartOpacity = 250;
        _buyNowShapedButton.TabIndex = 3;
        _buyNowShapedButton.Text = "shapedButton2";
        _buyNowShapedButton.TextLocation = new Point(40, 18);
        _buyNowShapedButton.UseVisualStyleBackColor = false;
        // 
        // _bottomBorderRightPanel
        // 
        _bottomBorderRightPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
        _bottomBorderRightPanel.BackColor = Color.Black;
        _bottomBorderRightPanel.Cursor = Cursors.SizeNWSE;
        _bottomBorderRightPanel.Location = new Point(710, 471);
        _bottomBorderRightPanel.Name = "_BottomBorderRightPanel";
        _bottomBorderRightPanel.Size = new Size(19, 2);
        _bottomBorderRightPanel.TabIndex = 5;
        // 
        // _bottomPanel
        // 
        _bottomPanel.BackColor = Color.FromArgb(30, 30, 30);
        _bottomPanel.Controls.Add(_timesShapedButton);
        _bottomPanel.Controls.Add(_buyNowShapedButton);
        _bottomPanel.Controls.Add(_testShapedButton);
        _bottomPanel.Controls.Add(_addShapedButton);
        _bottomPanel.Controls.Add(_toolsShapedButton);
        _bottomPanel.Dock = DockStyle.Bottom;
        _bottomPanel.Location = new Point(0, 402);
        _bottomPanel.Name = "_BottomPanel";
        _bottomPanel.Size = new Size(730, 71);
        _bottomPanel.TabIndex = 8;
        // 
        // _timesShapedButton
        // 
        _timesShapedButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
        _timesShapedButton.BackColor = Color.Transparent;
        _timesShapedButton.BorderColor = Color.Transparent;
        _timesShapedButton.BorderWidth = 2;
        _timesShapedButton.ButtonShape = ButtonShape.Circle;
        _timesShapedButton.ButtonText = "X";
        _timesShapedButton.EndColor = Color.FromArgb(30, 30, 30);
        _timesShapedButton.EndOpacity = 250;
        _timesShapedButton.FlatAppearance.BorderSize = 0;
        _timesShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
        _timesShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
        _timesShapedButton.FlatStyle = FlatStyle.Flat;
        _timesShapedButton.Font = new Font("Microsoft Sans Serif", 15.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _timesShapedButton.ForeColor = Color.White;
        _timesShapedButton.GradientAngle = 90;
        _timesShapedButton.Location = new Point(643, 8);
        _timesShapedButton.MouseClickStartColor = Color.Black;
        _timesShapedButton.MouseClickEndColor = Color.Black;
        _timesShapedButton.MouseHoverStartColor = Color.FromArgb(150, 0, 0);
        _timesShapedButton.MouseHoverEndColor = Color.FromArgb(150, 0, 0);
        _timesShapedButton.Name = "_TimesShapedButton";
        _timesShapedButton.ShowButtonText = true;
        _timesShapedButton.Size = new Size(75, 55);
        _timesShapedButton.StartColor = Color.FromArgb(30, 30, 30);
        _timesShapedButton.StartOpacity = 250;
        _timesShapedButton.TabIndex = 9;
        _timesShapedButton.TextLocation = new Point(28, 16);
        _timesShapedButton.UseVisualStyleBackColor = false;
        // 
        // _testShapedButton
        // 
        _testShapedButton.BackColor = Color.Transparent;
        _testShapedButton.BorderColor = Color.Transparent;
        _testShapedButton.BorderWidth = 2;
        _testShapedButton.ButtonShape = ButtonShape.RoundRect;
        _testShapedButton.ButtonText = "Test";
        _testShapedButton.EndColor = Color.FromArgb(30, 30, 30);
        _testShapedButton.EndOpacity = 250;
        _testShapedButton.FlatAppearance.BorderSize = 0;
        _testShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
        _testShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
        _testShapedButton.FlatStyle = FlatStyle.Flat;
        _testShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _testShapedButton.ForeColor = Color.White;
        _testShapedButton.GradientAngle = 90;
        _testShapedButton.Location = new Point(257, 10);
        _testShapedButton.MouseClickStartColor = Color.Black;
        _testShapedButton.MouseClickEndColor = Color.Black;
        _testShapedButton.MouseHoverStartColor = Color.FromArgb(80, 80, 80);
        _testShapedButton.MouseHoverEndColor = Color.FromArgb(80, 80, 80);
        _testShapedButton.Name = "_TestShapedButton";
        _testShapedButton.ShowButtonText = true;
        _testShapedButton.Size = new Size(136, 55);
        _testShapedButton.StartColor = Color.FromArgb(30, 30, 30);
        _testShapedButton.StartOpacity = 250;
        _testShapedButton.TabIndex = 8;
        _testShapedButton.TextLocation = new Point(46, 18);
        _testShapedButton.UseVisualStyleBackColor = false;
        // 
        // _addShapedButton
        // 
        _addShapedButton.BackColor = Color.Transparent;
        _addShapedButton.BorderColor = Color.Transparent;
        _addShapedButton.BorderWidth = 2;
        _addShapedButton.ButtonShape = ButtonShape.Circle;
        _addShapedButton.ButtonText = "+";
        _addShapedButton.EndColor = Color.FromArgb(30, 30, 30);
        _addShapedButton.EndOpacity = 250;
        _addShapedButton.FlatAppearance.BorderSize = 0;
        _addShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
        _addShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
        _addShapedButton.FlatStyle = FlatStyle.Flat;
        _addShapedButton.Font = new Font("Microsoft Sans Serif", 27.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _addShapedButton.ForeColor = Color.White;
        _addShapedButton.GradientAngle = 90;
        _addShapedButton.Location = new Point(10, 10);
        _addShapedButton.MouseClickStartColor = Color.Black;
        _addShapedButton.MouseClickEndColor = Color.Black;
        _addShapedButton.MouseHoverStartColor = Color.FromArgb(80, 80, 80);
        _addShapedButton.MouseHoverEndColor = Color.FromArgb(80, 80, 80);
        _addShapedButton.Name = "_addShapedButton";
        _addShapedButton.ShowButtonText = true;
        _addShapedButton.Size = new Size(59, 55);
        _addShapedButton.StartColor = Color.FromArgb(30, 30, 30);
        _addShapedButton.StartOpacity = 250;
        _addShapedButton.TabIndex = 6;
        _addShapedButton.TextLocation = new Point(13, 6);
        _addShapedButton.UseVisualStyleBackColor = false;
        // 
        // _toolsShapedButton
        // 
        _toolsShapedButton.BackColor = Color.Transparent;
        _toolsShapedButton.BorderColor = Color.Transparent;
        _toolsShapedButton.BorderWidth = 2;
        _toolsShapedButton.ButtonShape = ButtonShape.RoundRect;
        _toolsShapedButton.ButtonText = "Tools";
        _toolsShapedButton.EndColor = Color.FromArgb(30, 30, 30);
        _toolsShapedButton.EndOpacity = 250;
        _toolsShapedButton.FlatAppearance.BorderSize = 0;
        _toolsShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
        _toolsShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
        _toolsShapedButton.FlatStyle = FlatStyle.Flat;
        _toolsShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _toolsShapedButton.ForeColor = Color.White;
        _toolsShapedButton.GradientAngle = 90;
        _toolsShapedButton.Location = new Point(96, 10);
        _toolsShapedButton.MouseClickStartColor = Color.Black;
        _toolsShapedButton.MouseClickEndColor = Color.Black;
        _toolsShapedButton.MouseHoverStartColor = Color.FromArgb(80, 80, 80);
        _toolsShapedButton.MouseHoverEndColor = Color.FromArgb(80, 80, 80);
        _toolsShapedButton.Name = "_ToolsShapedButton";
        _toolsShapedButton.ShowButtonText = true;
        _toolsShapedButton.Size = new Size(136, 55);
        _toolsShapedButton.StartColor = Color.FromArgb(30, 30, 30);
        _toolsShapedButton.StartOpacity = 250;
        _toolsShapedButton.TabIndex = 7;
        _toolsShapedButton.TextLocation = new Point(45, 18);
        _toolsShapedButton.UseVisualStyleBackColor = false;
        // 
        // _rightBorderBottomPanel
        // 
        _rightBorderBottomPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
        _rightBorderBottomPanel.BackColor = Color.Black;
        _rightBorderBottomPanel.Cursor = Cursors.SizeNWSE;
        _rightBorderBottomPanel.Location = new Point(728, 452);
        _rightBorderBottomPanel.Name = "_RightBorderBottomPanel";
        _rightBorderBottomPanel.Size = new Size(2, 19);
        _rightBorderBottomPanel.TabIndex = 9;
        // 
        // _bottomBorderLeftPanel
        // 
        _bottomBorderLeftPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
        _bottomBorderLeftPanel.BackColor = Color.Black;
        _bottomBorderLeftPanel.Cursor = Cursors.SizeNESW;
        _bottomBorderLeftPanel.Location = new Point(0, 471);
        _bottomBorderLeftPanel.Name = "_BottomBorderLeftPanel";
        _bottomBorderLeftPanel.Size = new Size(15, 2);
        _bottomBorderLeftPanel.TabIndex = 10;
        // 
        // _leftBorderBottomPanel
        // 
        _leftBorderBottomPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
        _leftBorderBottomPanel.BackColor = Color.Black;
        _leftBorderBottomPanel.Cursor = Cursors.SizeNESW;
        _leftBorderBottomPanel.Location = new Point(0, 453);
        _leftBorderBottomPanel.Name = "_LeftBorderBottomPanel";
        _leftBorderBottomPanel.Size = new Size(2, 19);
        _leftBorderBottomPanel.TabIndex = 11;
        // 
        // _rightBorderTopPanel
        // 
        _rightBorderTopPanel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        _rightBorderTopPanel.BackColor = Color.Black;
        _rightBorderTopPanel.Cursor = Cursors.SizeNESW;
        _rightBorderTopPanel.Location = new Point(728, 2);
        _rightBorderTopPanel.Name = "_RightBorderTopPanel";
        _rightBorderTopPanel.Size = new Size(2, 20);
        _rightBorderTopPanel.TabIndex = 12;
        // 
        // _topBorderRightPanel
        // 
        _topBorderRightPanel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        _topBorderRightPanel.BackColor = Color.Black;
        _topBorderRightPanel.Cursor = Cursors.SizeNESW;
        _topBorderRightPanel.Location = new Point(710, 0);
        _topBorderRightPanel.Name = "_TopBorderRightPanel";
        _topBorderRightPanel.Size = new Size(20, 2);
        _topBorderRightPanel.TabIndex = 13;
        // 
        // _topBorderLeftPanel
        // 
        _topBorderLeftPanel.BackColor = Color.Black;
        _topBorderLeftPanel.Cursor = Cursors.SizeNWSE;
        _topBorderLeftPanel.Location = new Point(0, 0);
        _topBorderLeftPanel.Name = "_TopBorderLeftPanel";
        _topBorderLeftPanel.Size = new Size(20, 2);
        _topBorderLeftPanel.TabIndex = 14;
        // 
        // _leftBorderTopPanel
        // 
        _leftBorderTopPanel.BackColor = Color.Black;
        _leftBorderTopPanel.Cursor = Cursors.SizeNWSE;
        _leftBorderTopPanel.Location = new Point(0, 0);
        _leftBorderTopPanel.Name = "_LeftBorderTopPanel";
        _leftBorderTopPanel.Size = new Size(2, 20);
        _leftBorderTopPanel.TabIndex = 15;
        // 
        // _leftPanel
        // 
        _leftPanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
        _leftPanel.BackColor = Color.FromArgb(60, 60, 60);
        _leftPanel.Controls.Add(_helpButton);
        _leftPanel.Controls.Add(_runButton);
        _leftPanel.Controls.Add(_viewButton);
        _leftPanel.Controls.Add(_editButton);
        _leftPanel.Controls.Add(_fileButton);
        _leftPanel.Location = new Point(2, 77);
        _leftPanel.Name = "_LeftPanel";
        _leftPanel.Size = new Size(280, 325);
        _leftPanel.TabIndex = 16;
        // 
        // _helpButton
        // 
        _helpButton.BusyBackColor = Color.FromArgb(60, 60, 60);
        _helpButton.DisplayText = "Help";
        _helpButton.Dock = DockStyle.Top;
        _helpButton.FlatStyle = FlatStyle.Flat;
        _helpButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _helpButton.ForeColor = Color.White;
        _helpButton.Location = new Point(0, 140);
        _helpButton.MouseClickColor = Color.Black;
        _helpButton.MouseColorsEnabled = true;
        _helpButton.MouseHoverColor = Color.FromArgb(30, 30, 30);
        _helpButton.Name = "_HelpButton";
        _helpButton.Size = new Size(280, 35);
        _helpButton.TabIndex = 4;
        _helpButton.Text = "Help";
        _helpButton.TextLocationLeft = 110;
        _helpButton.TextLocationTop = 6;
        _helpButton.UseVisualStyleBackColor = true;
        // 
        // _runButton
        // 
        _runButton.BusyBackColor = Color.FromArgb(60, 60, 60);
        _runButton.DisplayText = "Run";
        _runButton.Dock = DockStyle.Top;
        _runButton.FlatStyle = FlatStyle.Flat;
        _runButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _runButton.ForeColor = Color.White;
        _runButton.Location = new Point(0, 105);
        _runButton.MouseClickColor = Color.Black;
        _runButton.MouseColorsEnabled = true;
        _runButton.MouseHoverColor = Color.FromArgb(30, 30, 30);
        _runButton.Name = "_RunButton";
        _runButton.Size = new Size(280, 35);
        _runButton.TabIndex = 3;
        _runButton.Text = "Run";
        _runButton.TextLocationLeft = 110;
        _runButton.TextLocationTop = 6;
        _runButton.UseVisualStyleBackColor = true;
        // 
        // _viewButton
        // 
        _viewButton.BusyBackColor = Color.FromArgb(60, 60, 60);
        _viewButton.DisplayText = "View";
        _viewButton.Dock = DockStyle.Top;
        _viewButton.FlatStyle = FlatStyle.Flat;
        _viewButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _viewButton.ForeColor = Color.White;
        _viewButton.Location = new Point(0, 70);
        _viewButton.MouseClickColor = Color.Black;
        _viewButton.MouseColorsEnabled = true;
        _viewButton.MouseHoverColor = Color.FromArgb(30, 30, 30);
        _viewButton.Name = "_ViewButton";
        _viewButton.Size = new Size(280, 35);
        _viewButton.TabIndex = 2;
        _viewButton.Text = "View";
        _viewButton.TextLocationLeft = 110;
        _viewButton.TextLocationTop = 6;
        _viewButton.UseVisualStyleBackColor = true;
        // 
        // _editButton
        // 
        _editButton.BusyBackColor = Color.FromArgb(60, 60, 60);
        _editButton.DisplayText = "Edit";
        _editButton.Dock = DockStyle.Top;
        _editButton.FlatStyle = FlatStyle.Flat;
        _editButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _editButton.ForeColor = Color.White;
        _editButton.Location = new Point(0, 35);
        _editButton.MouseClickColor = Color.Black;
        _editButton.MouseColorsEnabled = true;
        _editButton.MouseHoverColor = Color.FromArgb(30, 30, 30);
        _editButton.Name = "_EditButton";
        _editButton.Size = new Size(280, 35);
        _editButton.TabIndex = 1;
        _editButton.Text = "Edit";
        _editButton.TextLocationLeft = 110;
        _editButton.TextLocationTop = 6;
        _editButton.UseVisualStyleBackColor = true;
        // 
        // _fileButton
        // 
        _fileButton.BusyBackColor = Color.FromArgb(60, 60, 60);
        _fileButton.DisplayText = "File";
        _fileButton.Dock = DockStyle.Top;
        _fileButton.FlatStyle = FlatStyle.Flat;
        _fileButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _fileButton.ForeColor = Color.White;
        _fileButton.Location = new Point(0, 0);
        _fileButton.MouseClickColor = Color.Black;
        _fileButton.MouseColorsEnabled = true;
        _fileButton.MouseHoverColor = Color.FromArgb(30, 30, 30);
        _fileButton.Name = "_FileButton";
        _fileButton.Size = new Size(280, 35);
        _fileButton.TabIndex = 0;
        _fileButton.Text = "File";
        _fileButton.TextLocationLeft = 110;
        _fileButton.TextLocationTop = 6;
        _fileButton.UseVisualStyleBackColor = true;
        // 
        // _separatorPanel
        // 
        _separatorPanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
        _separatorPanel.BackColor = Color.Black;
        _separatorPanel.Location = new Point(282, 78);
        _separatorPanel.Name = "_SeparatorPanel";
        _separatorPanel.Size = new Size(2, 324);
        _separatorPanel.TabIndex = 17;
        // 
        // _informationLabel
        // 
        _informationLabel.AutoSize = true;
        _informationLabel.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _informationLabel.ForeColor = Color.White;
        _informationLabel.Location = new Point(309, 204);
        _informationLabel.Name = "_InformationLabel";
        _informationLabel.Size = new Size(378, 24);
        _informationLabel.TabIndex = 18;
        _informationLabel.Text = "Resizable Black colored Custom Form in C#";
        // 
        // BlackForm
        // 
        AutoScaleDimensions = new SizeF(6.0f, 13.0f);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(60, 60, 60);
        ClientSize = new Size(730, 473);
        Controls.Add(_informationLabel);
        Controls.Add(_separatorPanel);
        Controls.Add(_leftPanel);
        Controls.Add(_leftBorderTopPanel);
        Controls.Add(_topBorderLeftPanel);
        Controls.Add(_topBorderRightPanel);
        Controls.Add(_rightBorderTopPanel);
        Controls.Add(_leftBorderBottomPanel);
        Controls.Add(_bottomBorderLeftPanel);
        Controls.Add(_rightBorderBottomPanel);
        Controls.Add(_bottomBorderRightPanel);
        Controls.Add(_bottomBorderPanel);
        Controls.Add(_leftBorderPanel);
        Controls.Add(_rightBorderPanel);
        Controls.Add(_topBorderPanel);
        Controls.Add(_topPanel);
        Controls.Add(_bottomPanel);
        FormBorderStyle = FormBorderStyle.None;
        MainMenuStrip = _menuStrip;
        Name = "BlackForm";
        Text = "My App";
        _topPanel.ResumeLayout(false);
        _topPanel.PerformLayout();
        _menuStrip.ResumeLayout(false);
        _menuStrip.PerformLayout();
        _bottomPanel.ResumeLayout(false);
        _leftPanel.ResumeLayout(false);
        ResumeLayout(false);
        PerformLayout();
    }

    private Panel _topBorderPanel;
    private Panel _rightBorderPanel;
    private Panel _leftBorderPanel;
    private Panel _bottomBorderPanel;
    private Panel _topPanel;
    private ZopeButton _closeButton;
    private Panel _bottomBorderRightPanel;
    private ShapedButton _addShapedButton;
    private Label _windowTextLabel;
    private MinMaxButton _maxButton;
    private ShapedButton _buyNowShapedButton;
    private ShapedButton _toolsShapedButton;
    private Panel _bottomPanel;
    private ZopeButton _minButton;
    private ToolTip _toolTip;
    private Panel _rightBorderBottomPanel;
    private Panel _bottomBorderLeftPanel;
    private Panel _leftBorderBottomPanel;
    private Panel _rightBorderTopPanel;
    private Panel _topBorderRightPanel;
    private Panel _topBorderLeftPanel;
    private Panel _leftBorderTopPanel;
    private Panel _leftPanel;
    private ZopeButton _fileButton;
    private Panel _separatorPanel;
    private ZopeButton _editButton;
    private ZopeButton _viewButton;
    private ZopeButton _helpButton;
    private ZopeButton _runButton;
    private Label _informationLabel;
    private ShapedButton _testShapedButton;
    private ShapedButton _timesShapedButton;
    private ZopeMenuStrip _menuStrip;
    private System.Windows.Forms.ToolStripMenuItem _fileToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _newToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _openToolStripMenuItem;
    private ToolStripSeparator _toolStripSeparator1;
    private System.Windows.Forms.ToolStripMenuItem _saveToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _saveAsToolStripMenuItem;
    private ToolStripSeparator _toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem _closeToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _closeAllToolStripMenuItem;
    private ToolStripSeparator _toolStripSeparator3;
    private System.Windows.Forms.ToolStripMenuItem _printToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _printPreviewToolStripMenuItem;
    private ToolStripSeparator _toolStripSeparator4;
    private System.Windows.Forms.ToolStripMenuItem _closeToolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem _editToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _cutToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _copyToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _pasteToolStripMenuItem;
    private ToolStripSeparator _toolStripSeparator5;
    private System.Windows.Forms.ToolStripMenuItem _undoToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _redoToolStripMenuItem;
    private ToolStripSeparator _toolStripSeparator6;
    private System.Windows.Forms.ToolStripMenuItem _findToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _replaceToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _selectAllToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _helpToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _helpContentsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _onlineHelpToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _aboutToolStripMenuItem;
}
