namespace cc.isr.WinControls.Demo;

public partial class BlueForm
{
    /// <summary>
/// Required designer variable.
/// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
/// Clean up any resources being used.
/// </summary>
/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if ( disposing )
        {
            components?.Dispose();
        }

        base.Dispose(disposing);
    }

    /// <summary>
    /// Required method for Designer support - do not modify the contents of this method with the
    /// code editor.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        _topBorderPanel = new Panel();
        _topBorderPanel.MouseDown += new MouseEventHandler(TopBorderPanel_MouseDown);
        _topBorderPanel.MouseMove += new MouseEventHandler(TopBorderPanel_MouseMove);
        _topBorderPanel.MouseUp += new MouseEventHandler(TopBorderPanel_MouseUp);
        _rightBorderPanel = new Panel();
        _rightBorderPanel.MouseDown += new MouseEventHandler(RightPanel_MouseDown);
        _rightBorderPanel.MouseMove += new MouseEventHandler(RightPanel_MouseMove);
        _rightBorderPanel.MouseUp += new MouseEventHandler(RightPanel_MouseUp);
        _leftBorderPanel = new Panel();
        _leftBorderPanel.MouseDown += new MouseEventHandler(LeftPanel_MouseDown);
        _leftBorderPanel.MouseMove += new MouseEventHandler(LeftPanel_MouseMove);
        _leftBorderPanel.MouseUp += new MouseEventHandler(LeftPanel_MouseUp);
        _bottomBorderPanel = new Panel();
        _bottomBorderPanel.MouseDown += new MouseEventHandler(BottomPanel_MouseDown);
        _bottomBorderPanel.MouseMove += new MouseEventHandler(BottomPanel_MouseMove);
        _bottomBorderPanel.MouseUp += new MouseEventHandler(BottomPanel_MouseUp);
        _topPanel = new Panel();
        _topPanel.MouseDown += new MouseEventHandler(TopPanel_MouseDown);
        _topPanel.MouseMove += new MouseEventHandler(TopPanel_MouseMove);
        _topPanel.MouseUp += new MouseEventHandler(TopPanel_MouseUp);
        _iconPanel = new Panel();
        _windowTextLabel = new Label();
        _maxButton = new MinMaxButton();
        _maxButton.Click += new System.EventHandler(MaxButton_Click);
        _minButton = new ZopeButton();
        _minButton.Click += new System.EventHandler(MinButton_Click);
        _closeButton = new ZopeButton();
        _closeButton.Click += new System.EventHandler(CloseButton_Click);
        _toolTip = new ToolTip(components);
        _topPanel.SuspendLayout();
        SuspendLayout();
        // 
        // _topBorderPanel
        // 
        _topBorderPanel.BackColor = Color.FromArgb(10, 20, 50);
        _topBorderPanel.Cursor = Cursors.SizeNS;
        _topBorderPanel.Dock = DockStyle.Top;
        _topBorderPanel.Location = new Point(0, 0);
        _topBorderPanel.Name = "_TopBorderPanel";
        _topBorderPanel.Size = new Size(684, 2);
        _topBorderPanel.TabIndex = 0;
        // 
        // _rightBorderPanel
        // 
        _rightBorderPanel.BackColor = Color.FromArgb(10, 20, 50);
        _rightBorderPanel.Cursor = Cursors.SizeWE;
        _rightBorderPanel.Dock = DockStyle.Right;
        _rightBorderPanel.Location = new Point(682, 2);
        _rightBorderPanel.Name = "_RightBorderPanel";
        _rightBorderPanel.Size = new Size(2, 459);
        _rightBorderPanel.TabIndex = 1;
        // 
        // _leftBorderPanel
        // 
        _leftBorderPanel.BackColor = Color.FromArgb(10, 20, 50);
        _leftBorderPanel.Cursor = Cursors.SizeWE;
        _leftBorderPanel.Dock = DockStyle.Left;
        _leftBorderPanel.Location = new Point(0, 2);
        _leftBorderPanel.Name = "_LeftBorderPanel";
        _leftBorderPanel.Size = new Size(2, 459);
        _leftBorderPanel.TabIndex = 2;
        // 
        // _bottomBorderPanel
        // 
        _bottomBorderPanel.BackColor = Color.FromArgb(10, 20, 50);
        _bottomBorderPanel.Cursor = Cursors.SizeNS;
        _bottomBorderPanel.Dock = DockStyle.Bottom;
        _bottomBorderPanel.Location = new Point(2, 459);
        _bottomBorderPanel.Name = "_BottomBorderPanel";
        _bottomBorderPanel.Size = new Size(680, 2);
        _bottomBorderPanel.TabIndex = 3;
        // 
        // _topPanel
        // 
        _topPanel.BackColor = Color.FromArgb(20, 40, 80);
        _topPanel.Controls.Add(_iconPanel);
        _topPanel.Controls.Add(_windowTextLabel);
        _topPanel.Controls.Add(_maxButton);
        _topPanel.Controls.Add(_minButton);
        _topPanel.Controls.Add(_closeButton);
        _topPanel.Dock = DockStyle.Top;
        _topPanel.Location = new Point(2, 2);
        _topPanel.Name = "_TopPanel";
        _topPanel.Size = new Size(680, 35);
        _topPanel.TabIndex = 4;
        // 
        // _iconPanel
        // 
        _iconPanel.BackgroundImage = Demo.Properties.Resources.Crazy;
        _iconPanel.Location = new Point(10, 3);
        _iconPanel.Name = "_IconPanel";
        _iconPanel.Size = new Size(26, 26);
        _iconPanel.TabIndex = 5;
        // 
        // _windowTextLabel
        // 
        _windowTextLabel.AutoSize = true;
        _windowTextLabel.Font = new Font("Microsoft Sans Serif", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _windowTextLabel.ForeColor = Color.White;
        _windowTextLabel.Location = new Point(57, 7);
        _windowTextLabel.Name = "_WindowTextLabel";
        _windowTextLabel.Size = new Size(77, 18);
        _windowTextLabel.TabIndex = 6;
        _windowTextLabel.Text = "Blue Form";
        // 
        // _maxButton
        // 
        _maxButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        _maxButton.BusyBackColor = Color.FromArgb(20, 40, 80);
        _maxButton.CustomFormState = CustomFormState.Normal;
        _maxButton.DisplayText = "_";
        _maxButton.FlatStyle = FlatStyle.Flat;
        _maxButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _maxButton.ForeColor = Color.White;
        _maxButton.Location = new Point(604, 3);
        _maxButton.MouseClickColor = Color.FromArgb(10, 20, 60);
        _maxButton.MouseHoverColor = Color.FromArgb(40, 80, 180);
        _maxButton.Name = "_MaxButton";
        _maxButton.Size = new Size(35, 25);
        _maxButton.TabIndex = 5;
        _maxButton.Text = "minMaxButton1";
        _maxButton.TextLocation = new Point(11, 8);
        _toolTip.SetToolTip(_maxButton, "Maximize");
        _maxButton.UseVisualStyleBackColor = true;
        // 
        // _minButton
        // 
        _minButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        _minButton.BusyBackColor = Color.FromArgb(20, 40, 80);
        _minButton.DisplayText = "_";
        _minButton.FlatStyle = FlatStyle.Flat;
        _minButton.Font = new Font("Microsoft YaHei UI", 18.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _minButton.ForeColor = Color.White;
        _minButton.Location = new Point(569, 3);
        _minButton.MouseClickColor = Color.FromArgb(10, 20, 60);
        _minButton.MouseColorsEnabled = true;
        _minButton.MouseHoverColor = Color.FromArgb(40, 80, 180);
        _minButton.Name = "_MinButton";
        _minButton.Size = new Size(35, 25);
        _minButton.TabIndex = 1;
        _minButton.Text = "_";
        _minButton.TextLocationLeft = 8;
        _minButton.TextLocationTop = -14;
        _toolTip.SetToolTip(_minButton, "Minimize");
        _minButton.UseVisualStyleBackColor = true;
        // 
        // _closeButton
        // 
        _closeButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        _closeButton.BusyBackColor = Color.FromArgb(20, 40, 80);
        _closeButton.DisplayText = "X";
        _closeButton.FlatStyle = FlatStyle.Flat;
        _closeButton.Font = new Font("Microsoft YaHei UI", 11.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _closeButton.ForeColor = Color.White;
        _closeButton.Location = new Point(639, 3);
        _closeButton.MouseClickColor = Color.FromArgb(150, 0, 0);
        _closeButton.MouseColorsEnabled = true;
        _closeButton.MouseHoverColor = Color.FromArgb(40, 80, 180);
        _closeButton.Name = "_CloseButton";
        _closeButton.Size = new Size(35, 25);
        _closeButton.TabIndex = 0;
        _closeButton.Text = "X";
        _closeButton.TextLocationLeft = 10;
        _closeButton.TextLocationTop = 4;
        _toolTip.SetToolTip(_closeButton, "Close");
        _closeButton.UseVisualStyleBackColor = true;
        // 
        // BlueForm
        // 
        AutoScaleDimensions = new SizeF(6.0f, 13.0f);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(30, 60, 150);
        ClientSize = new Size(684, 461);
        Controls.Add(_topPanel);
        Controls.Add(_bottomBorderPanel);
        Controls.Add(_leftBorderPanel);
        Controls.Add(_rightBorderPanel);
        Controls.Add(_topBorderPanel);
        FormBorderStyle = FormBorderStyle.None;
        Name = "BlueForm";
        Text = "Blue Form";
        _topPanel.ResumeLayout(false);
        _topPanel.PerformLayout();
        ResumeLayout(false);
    }

    private Panel _topBorderPanel;
    private Panel _rightBorderPanel;
    private Panel _leftBorderPanel;
    private Panel _bottomBorderPanel;
    private Panel _topPanel;
    private ZopeButton _closeButton;
    private MinMaxButton _maxButton;
    private ZopeButton _minButton;
    private ToolTip _toolTip;
    private Label _windowTextLabel;
    private Panel _iconPanel;
}
