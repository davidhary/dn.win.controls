namespace cc.isr.WinControls.Demo;

/// <summary>   Form for viewing the dashboard. </summary>
/// <remarks>   David, 2021-03-12. </remarks>
public partial class DashboardForm : Form
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Form" /> class.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public DashboardForm()
    {
        /// <summary> Dashboard form load. </summary>
        /// <remarks> David, 2021-03-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        base.Load += this.Dashboard_Form_Load;
        this.InitializeComponent();
    }
    /// <summary>   The offset. </summary>

    private Point _offset;

    /// <summary>   True if is top panel dragged, false if not. </summary>
    private bool _isTopPanelDragged;

    /// <summary>   Event handler. Called by Dashboard_Form for load events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Dashboard_Form_Load( object? sender, EventArgs e )
    {
        this.Dashboard_button_Click( sender, e );
    }

    /// <summary>   Top panel mouse down. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void TopPanel_MouseDown( object? sender, MouseEventArgs e )
    {
        if ( e.Button == MouseButtons.Left )
        {
            this._isTopPanelDragged = true;
            Point pointStartPosition = this.PointToScreen( new Point( e.X, e.Y ) );
            this._offset = new Point()
            {
                X = this.Location.X - (pointStartPosition.X + this._leftTopPanel.Size.Width),
                Y = this.Location.Y - pointStartPosition.Y
            };
        }
        else
        {
            this._isTopPanelDragged = false;
        }
    }

    /// <summary>   Top panel mouse move. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void TopPanel_MouseMove( object? sender, MouseEventArgs e )
    {
        if ( this._isTopPanelDragged )
        {
            Point newPoint = this._topPanel.PointToScreen( new Point( e.X, e.Y ) );
            newPoint.Offset( this._offset );
            this.Location = newPoint;
        }
    }

    /// <summary>   Top panel mouse up. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void TopPanel_MouseUp( object? sender, MouseEventArgs e )
    {
        this._isTopPanelDragged = false;
    }

    /// <summary>   Closes button click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void CloseButton_Click( object? sender, EventArgs e )
    {
        this.Close();
    }

    /// <summary>   Minimum button click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void MinButton_Click( object? sender, EventArgs e )
    {
        this.WindowState = FormWindowState.Minimized;
    }

    /// <summary>   Dashboard label mouse down. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void DashboardLabel_mouseDown( object? sender, MouseEventArgs e )
    {
        this.TopPanel_MouseDown( sender, e );
    }

    /// <summary>   Dashboard label mouse move. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void DashboardLabel_mouseMove( object? sender, MouseEventArgs e )
    {
        this.TopPanel_MouseMove( sender, e );
    }

    /// <summary>   Dashboard label mouse up. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void DashboardLabel_mouseUp( object? sender, MouseEventArgs e )
    {
        this.TopPanel_MouseUp( sender, e );
    }

    /// <summary>   Dashboard button click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Dashboard_button_Click( object? sender, EventArgs e )
    {
        this._dashboardButton.BusyBackColor = Color.FromArgb( 20, 20, 20 );
        this._dashboardButton.MouseColorsEnabled = false;
        this._statsButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._pagesButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._layoutButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._themeButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._settingsButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._statsButton.MouseColorsEnabled = true;
        this._pagesButton.MouseColorsEnabled = true;
        this._layoutButton.MouseColorsEnabled = true;
        this._themeButton.MouseColorsEnabled = true;
        this._settingsButton.MouseColorsEnabled = true;
    }

    /// <summary>   Statistics button click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Stats_button_Click( object? sender, EventArgs e )
    {
        this._statsButton.BusyBackColor = Color.FromArgb( 20, 20, 20 );
        this._statsButton.MouseColorsEnabled = false;
        this._dashboardButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._pagesButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._layoutButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._themeButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._settingsButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._dashboardButton.MouseColorsEnabled = true;
        this._pagesButton.MouseColorsEnabled = true;
        this._layoutButton.MouseColorsEnabled = true;
        this._themeButton.MouseColorsEnabled = true;
        this._settingsButton.MouseColorsEnabled = true;
    }

    /// <summary>   Pages button click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Pages_button_Click( object? sender, EventArgs e )
    {
        this._pagesButton.BusyBackColor = Color.FromArgb( 20, 20, 20 );
        this._pagesButton.MouseColorsEnabled = false;
        this._dashboardButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._statsButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._layoutButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._themeButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._settingsButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._dashboardButton.MouseColorsEnabled = true;
        this._statsButton.MouseColorsEnabled = true;
        this._layoutButton.MouseColorsEnabled = true;
        this._themeButton.MouseColorsEnabled = true;
        this._settingsButton.MouseColorsEnabled = true;
    }

    /// <summary>   Layout button click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Layout_button_Click( object? sender, EventArgs e )
    {
        this._layoutButton.BusyBackColor = Color.FromArgb( 20, 20, 20 );
        this._layoutButton.MouseColorsEnabled = false;
        this._dashboardButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._statsButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._pagesButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._themeButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._settingsButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._dashboardButton.MouseColorsEnabled = true;
        this._statsButton.MouseColorsEnabled = true;
        this._pagesButton.MouseColorsEnabled = true;
        this._themeButton.MouseColorsEnabled = true;
        this._settingsButton.MouseColorsEnabled = true;
    }

    /// <summary>   Theme button click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Theme_button_Click( object? sender, EventArgs e )
    {
        this._themeButton.BusyBackColor = Color.FromArgb( 20, 20, 20 );
        this._themeButton.MouseColorsEnabled = false;
        this._dashboardButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._statsButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._pagesButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._layoutButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._settingsButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._dashboardButton.MouseColorsEnabled = true;
        this._statsButton.MouseColorsEnabled = true;
        this._pagesButton.MouseColorsEnabled = true;
        this._layoutButton.MouseColorsEnabled = true;
        this._settingsButton.MouseColorsEnabled = true;
    }

    /// <summary>   Settings button click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Settings_button_Click( object? sender, EventArgs e )
    {
        this._settingsButton.BusyBackColor = Color.FromArgb( 20, 20, 20 );
        this._settingsButton.MouseColorsEnabled = false;
        this._dashboardButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._statsButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._pagesButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._layoutButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._themeButton.BusyBackColor = Color.FromArgb( 30, 30, 40 );
        this._dashboardButton.MouseColorsEnabled = true;
        this._statsButton.MouseColorsEnabled = true;
        this._pagesButton.MouseColorsEnabled = true;
        this._layoutButton.MouseColorsEnabled = true;
        this._themeButton.MouseColorsEnabled = true;
    }
}
