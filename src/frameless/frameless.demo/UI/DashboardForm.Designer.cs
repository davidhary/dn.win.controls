namespace cc.isr.WinControls.Demo;

public partial class DashboardForm
{
    /// <summary>
/// Required designer variable.
/// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
/// Clean up any resources being used.
/// </summary>
/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if ( disposing )
        {
            components?.Dispose();
        }

        base.Dispose(disposing);
    }

    /// <summary>
    /// Required method for Designer support - do not modify the contents of this method with the
    /// code editor.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    private void InitializeComponent()
    {
        _leftTopPanel = new Panel();
        _leftTopPanelLabel = new Label();
        _topPanel = new Panel();
        _topPanel.MouseDown += new MouseEventHandler(TopPanel_MouseDown);
        _topPanel.MouseMove += new MouseEventHandler(TopPanel_MouseMove);
        _topPanel.MouseUp += new MouseEventHandler(TopPanel_MouseUp);
        _dashboardLabel = new Label();
        _dashboardLabel.MouseDown += new MouseEventHandler(DashboardLabel_mouseDown);
        _dashboardLabel.MouseMove += new MouseEventHandler(DashboardLabel_mouseMove);
        _dashboardLabel.MouseUp += new MouseEventHandler(DashboardLabel_mouseUp);
        _iconPanel = new Panel();
        _minButton = new ZopeButton();
        _minButton.Click += new System.EventHandler(MinButton_Click);
        _closeButton = new ZopeButton();
        _closeButton.Click += new System.EventHandler(CloseButton_Click);
        _leftPanel = new Panel();
        _leftBottomPanel = new Panel();
        _leftBottomPanelLabel = new Label();
        _settingsButton = new ZopeButton();
        _settingsButton.Click += new System.EventHandler(Settings_button_Click);
        _themeButton = new ZopeButton();
        _themeButton.Click += new System.EventHandler(Theme_button_Click);
        _layoutButton = new ZopeButton();
        _layoutButton.Click += new System.EventHandler(Layout_button_Click);
        _pagesButton = new ZopeButton();
        _pagesButton.Click += new System.EventHandler(Pages_button_Click);
        _statsButton = new ZopeButton();
        _statsButton.Click += new System.EventHandler(Stats_button_Click);
        _dashboardButton = new ZopeButton();
        _dashboardButton.Click += new System.EventHandler(Dashboard_button_Click);
        _visitorsPanel = new Panel();
        _monthVisitorsCountLabel = new Label();
        _monthVisitorsCountLabelLabel = new Label();
        _todayVisitorsCountLabel = new Label();
        _todayVisitorsCountLabelLabel = new Label();
        _visitorsCountPanel = new Panel();
        _visitorsCountLabel = new Label();
        _visitorsCountLabelLabel = new Label();
        _adminPanel = new Panel();
        _adminNameLabel = new Label();
        _adminLabelLabel = new Label();
        _adminIconPanel = new Panel();
        _storagePanel = new Panel();
        _totalStorageLabel = new Label();
        _totalStorageLabelLabel = new Label();
        _freeStorageLabel = new Label();
        _freeStorageLabelLabel = new Label();
        _usedStorageLabel = new Label();
        _usedStorageLabelLabel = new Label();
        _storageTopPanel = new Panel();
        _storageTitleLabel = new Label();
        _controlPanel = new Panel();
        _controlPanelLabel = new Label();
        _deleteBlogButton = new ZopeButton();
        _viewBlogButton = new ZopeButton();
        _campaignsPanel = new Panel();
        _campaignsLabel = new Label();
        _leftTopPanel.SuspendLayout();
        _topPanel.SuspendLayout();
        _leftPanel.SuspendLayout();
        _leftBottomPanel.SuspendLayout();
        _visitorsPanel.SuspendLayout();
        _visitorsCountPanel.SuspendLayout();
        _adminPanel.SuspendLayout();
        _storagePanel.SuspendLayout();
        _storageTopPanel.SuspendLayout();
        _controlPanel.SuspendLayout();
        _campaignsPanel.SuspendLayout();
        SuspendLayout();
        // 
        // _leftTopPanel
        // 
        _leftTopPanel.BackColor = Color.FromArgb(30, 150, 80);
        _leftTopPanel.Controls.Add(_leftTopPanelLabel);
        _leftTopPanel.Location = new Point(0, 0);
        _leftTopPanel.Name = "_LeftTopPanel";
        _leftTopPanel.Size = new Size(245, 60);
        _leftTopPanel.TabIndex = 0;
        // 
        // _leftTopPanelLabel
        // 
        _leftTopPanelLabel.AutoSize = true;
        _leftTopPanelLabel.Font = new Font("Microsoft Sans Serif", 18.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _leftTopPanelLabel.ForeColor = Color.White;
        _leftTopPanelLabel.Location = new Point(45, 13);
        _leftTopPanelLabel.Name = "_LeftTopPanelLabel";
        _leftTopPanelLabel.Size = new Size(131, 29);
        _leftTopPanelLabel.TabIndex = 0;
        _leftTopPanelLabel.Text = "Extreme UI";
        // 
        // _topPanel
        // 
        _topPanel.BackColor = Color.White;
        _topPanel.Controls.Add(_dashboardLabel);
        _topPanel.Controls.Add(_iconPanel);
        _topPanel.Controls.Add(_minButton);
        _topPanel.Controls.Add(_closeButton);
        _topPanel.Location = new Point(245, 0);
        _topPanel.Name = "_TopPanel";
        _topPanel.Size = new Size(605, 60);
        _topPanel.TabIndex = 1;
        // 
        // _dashboardLabel
        // 
        _dashboardLabel.AutoSize = true;
        _dashboardLabel.Font = new Font("Microsoft Sans Serif", 18.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _dashboardLabel.ForeColor = Color.FromArgb(50, 50, 50);
        _dashboardLabel.Location = new Point(60, 13);
        _dashboardLabel.Name = "_DashboardLabel";
        _dashboardLabel.Size = new Size(131, 29);
        _dashboardLabel.TabIndex = 3;
        _dashboardLabel.Text = "Dashboard";
        // 
        // _iconPanel
        // 
        _iconPanel.BackgroundImage = Demo.Properties.Resources.home;
        _iconPanel.Location = new Point(6, 13);
        _iconPanel.Name = "_IconPanel";
        _iconPanel.Size = new Size(30, 30);
        _iconPanel.TabIndex = 2;
        // 
        // _minButton
        // 
        _minButton.BusyBackColor = Color.White;
        _minButton.DisplayText = "_";
        _minButton.FlatStyle = FlatStyle.Flat;
        _minButton.Font = new Font("Microsoft YaHei UI", 20.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _minButton.ForeColor = Color.Black;
        _minButton.Location = new Point(540, 4);
        _minButton.MouseClickColor = Color.FromArgb(160, 180, 200);
        _minButton.MouseColorsEnabled = true;
        _minButton.MouseHoverColor = Color.FromArgb(100, 150, 220);
        _minButton.Name = "_MinButton";
        _minButton.Size = new Size(31, 24);
        _minButton.TabIndex = 1;
        _minButton.Text = "_";
        _minButton.TextLocationLeft = 6;
        _minButton.TextLocationTop = -20;
        _minButton.UseVisualStyleBackColor = true;
        // 
        // _closeButton
        // 
        _closeButton.BusyBackColor = Color.White;
        _closeButton.DisplayText = "X";
        _closeButton.FlatStyle = FlatStyle.Flat;
        _closeButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _closeButton.ForeColor = Color.Black;
        _closeButton.Location = new Point(571, 4);
        _closeButton.MouseClickColor = Color.FromArgb(100, 150, 220);
        _closeButton.MouseColorsEnabled = true;
        _closeButton.MouseHoverColor = Color.FromArgb(180, 40, 50);
        _closeButton.Name = "_CloseButton";
        _closeButton.Size = new Size(31, 24);
        _closeButton.TabIndex = 0;
        _closeButton.Text = "X";
        _closeButton.TextLocationLeft = 6;
        _closeButton.TextLocationTop = -1;
        _closeButton.UseVisualStyleBackColor = true;
        // 
        // _leftPanel
        // 
        _leftPanel.BackColor = Color.FromArgb(30, 30, 40);
        _leftPanel.Controls.Add(_leftBottomPanel);
        _leftPanel.Controls.Add(_settingsButton);
        _leftPanel.Controls.Add(_themeButton);
        _leftPanel.Controls.Add(_layoutButton);
        _leftPanel.Controls.Add(_pagesButton);
        _leftPanel.Controls.Add(_statsButton);
        _leftPanel.Controls.Add(_dashboardButton);
        _leftPanel.Location = new Point(0, 60);
        _leftPanel.Name = "_LeftPanel";
        _leftPanel.Size = new Size(245, 440);
        _leftPanel.TabIndex = 2;
        // 
        // _leftBottomPanel
        // 
        _leftBottomPanel.BackColor = Color.FromArgb(20, 20, 30);
        _leftBottomPanel.Controls.Add(_leftBottomPanelLabel);
        _leftBottomPanel.Location = new Point(0, 380);
        _leftBottomPanel.Name = "_LeftBottomPanel";
        _leftBottomPanel.Size = new Size(245, 60);
        _leftBottomPanel.TabIndex = 6;
        // 
        // _leftBottomPanelLabel
        // 
        _leftBottomPanelLabel.AutoSize = true;
        _leftBottomPanelLabel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _leftBottomPanelLabel.ForeColor = Color.FromArgb(200, 200, 200);
        _leftBottomPanelLabel.Location = new Point(12, 14);
        _leftBottomPanelLabel.Name = "_LeftBottomPanelLabel";
        _leftBottomPanelLabel.Size = new Size(102, 16);
        _leftBottomPanelLabel.TabIndex = 0;
        _leftBottomPanelLabel.Text = "Copyright   2016";
        // 
        // _settingsButton
        // 
        _settingsButton.BusyBackColor = Color.FromArgb(30, 30, 40);
        _settingsButton.Cursor = Cursors.Hand;
        _settingsButton.DisplayText = "Settings";
        _settingsButton.FlatStyle = FlatStyle.Flat;
        _settingsButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _settingsButton.ForeColor = Color.White;
        _settingsButton.Location = new Point(0, 250);
        _settingsButton.MouseClickColor = Color.FromArgb(20, 20, 20);
        _settingsButton.MouseColorsEnabled = true;
        _settingsButton.MouseHoverColor = Color.FromArgb(20, 20, 20);
        _settingsButton.Name = "_SettingsButton";
        _settingsButton.Size = new Size(245, 50);
        _settingsButton.TabIndex = 5;
        _settingsButton.Text = "Settings";
        _settingsButton.TextLocationLeft = 80;
        _settingsButton.TextLocationTop = 10;
        _settingsButton.UseVisualStyleBackColor = true;
        // 
        // _themeButton
        // 
        _themeButton.BusyBackColor = Color.FromArgb(30, 30, 40);
        _themeButton.Cursor = Cursors.Hand;
        _themeButton.DisplayText = "Theme";
        _themeButton.FlatStyle = FlatStyle.Flat;
        _themeButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _themeButton.ForeColor = Color.White;
        _themeButton.Location = new Point(0, 200);
        _themeButton.MouseClickColor = Color.FromArgb(20, 20, 20);
        _themeButton.MouseColorsEnabled = true;
        _themeButton.MouseHoverColor = Color.FromArgb(20, 20, 20);
        _themeButton.Name = "_ThemeButton";
        _themeButton.Size = new Size(245, 50);
        _themeButton.TabIndex = 4;
        _themeButton.Text = "Theme";
        _themeButton.TextLocationLeft = 82;
        _themeButton.TextLocationTop = 10;
        _themeButton.UseVisualStyleBackColor = true;
        // 
        // _layoutButton
        // 
        _layoutButton.BusyBackColor = Color.FromArgb(30, 30, 40);
        _layoutButton.Cursor = Cursors.Hand;
        _layoutButton.DisplayText = "Layout";
        _layoutButton.FlatStyle = FlatStyle.Flat;
        _layoutButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _layoutButton.ForeColor = Color.White;
        _layoutButton.Location = new Point(0, 150);
        _layoutButton.MouseClickColor = Color.FromArgb(20, 20, 20);
        _layoutButton.MouseColorsEnabled = true;
        _layoutButton.MouseHoverColor = Color.FromArgb(20, 20, 20);
        _layoutButton.Name = "_LayoutButton";
        _layoutButton.Size = new Size(245, 50);
        _layoutButton.TabIndex = 3;
        _layoutButton.Text = "Layout";
        _layoutButton.TextLocationLeft = 80;
        _layoutButton.TextLocationTop = 10;
        _layoutButton.UseVisualStyleBackColor = true;
        // 
        // _pagesButton
        // 
        _pagesButton.BusyBackColor = Color.FromArgb(30, 30, 40);
        _pagesButton.Cursor = Cursors.Hand;
        _pagesButton.DisplayText = "Pages";
        _pagesButton.FlatStyle = FlatStyle.Flat;
        _pagesButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _pagesButton.ForeColor = Color.White;
        _pagesButton.Location = new Point(0, 100);
        _pagesButton.MouseClickColor = Color.FromArgb(20, 20, 20);
        _pagesButton.MouseColorsEnabled = true;
        _pagesButton.MouseHoverColor = Color.FromArgb(20, 20, 20);
        _pagesButton.Name = "_PagesButton";
        _pagesButton.Size = new Size(245, 50);
        _pagesButton.TabIndex = 2;
        _pagesButton.Text = "Pages";
        _pagesButton.TextLocationLeft = 82;
        _pagesButton.TextLocationTop = 10;
        _pagesButton.UseVisualStyleBackColor = true;
        // 
        // _statsButton
        // 
        _statsButton.BusyBackColor = Color.FromArgb(30, 30, 40);
        _statsButton.Cursor = Cursors.Hand;
        _statsButton.DisplayText = "Stats";
        _statsButton.FlatStyle = FlatStyle.Flat;
        _statsButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _statsButton.ForeColor = Color.White;
        _statsButton.Location = new Point(0, 50);
        _statsButton.MouseClickColor = Color.FromArgb(20, 20, 20);
        _statsButton.MouseColorsEnabled = true;
        _statsButton.MouseHoverColor = Color.FromArgb(20, 20, 20);
        _statsButton.Name = "_StatsButton";
        _statsButton.Size = new Size(245, 50);
        _statsButton.TabIndex = 1;
        _statsButton.Text = "Stats";
        _statsButton.TextLocationLeft = 84;
        _statsButton.TextLocationTop = 10;
        _statsButton.UseVisualStyleBackColor = true;
        // 
        // _dashboardButton
        // 
        _dashboardButton.BusyBackColor = Color.FromArgb(30, 30, 40);
        _dashboardButton.Cursor = Cursors.Hand;
        _dashboardButton.DisplayText = "Dashboard";
        _dashboardButton.FlatStyle = FlatStyle.Flat;
        _dashboardButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _dashboardButton.ForeColor = Color.White;
        _dashboardButton.Location = new Point(0, 0);
        _dashboardButton.MouseClickColor = Color.FromArgb(20, 20, 20);
        _dashboardButton.MouseColorsEnabled = true;
        _dashboardButton.MouseHoverColor = Color.FromArgb(20, 20, 20);
        _dashboardButton.Name = "_DashboardButton";
        _dashboardButton.Size = new Size(245, 50);
        _dashboardButton.TabIndex = 0;
        _dashboardButton.Text = "Dashboard";
        _dashboardButton.TextLocationLeft = 60;
        _dashboardButton.TextLocationTop = 10;
        _dashboardButton.UseVisualStyleBackColor = true;
        // 
        // _visitorsPanel
        // 
        _visitorsPanel.BackColor = Color.FromArgb(50, 80, 160);
        _visitorsPanel.Controls.Add(_monthVisitorsCountLabel);
        _visitorsPanel.Controls.Add(_monthVisitorsCountLabelLabel);
        _visitorsPanel.Controls.Add(_todayVisitorsCountLabel);
        _visitorsPanel.Controls.Add(_todayVisitorsCountLabelLabel);
        _visitorsPanel.Controls.Add(_visitorsCountPanel);
        _visitorsPanel.Location = new Point(251, 66);
        _visitorsPanel.Name = "_VisitorsPanel";
        _visitorsPanel.Size = new Size(590, 109);
        _visitorsPanel.TabIndex = 3;
        // 
        // _monthVisitorsCountLabel
        // 
        _monthVisitorsCountLabel.AutoSize = true;
        _monthVisitorsCountLabel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _monthVisitorsCountLabel.ForeColor = Color.Azure;
        _monthVisitorsCountLabel.Location = new Point(457, 61);
        _monthVisitorsCountLabel.Name = "_MonthVisitorsCountLabel";
        _monthVisitorsCountLabel.Size = new Size(36, 16);
        _monthVisitorsCountLabel.TabIndex = 4;
        _monthVisitorsCountLabel.Text = "1035";
        // 
        // _monthVisitorsCountLabelLabel
        // 
        _monthVisitorsCountLabelLabel.AutoSize = true;
        _monthVisitorsCountLabelLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _monthVisitorsCountLabelLabel.ForeColor = Color.White;
        _monthVisitorsCountLabelLabel.Location = new Point(446, 23);
        _monthVisitorsCountLabelLabel.Name = "_MonthVisitorsCountLabelLabel";
        _monthVisitorsCountLabelLabel.Size = new Size(64, 20);
        _monthVisitorsCountLabelLabel.TabIndex = 3;
        _monthVisitorsCountLabelLabel.Text = "Monthly";
        // 
        // _todayVisitorsCountLabel
        // 
        _todayVisitorsCountLabel.AutoSize = true;
        _todayVisitorsCountLabel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _todayVisitorsCountLabel.ForeColor = Color.Azure;
        _todayVisitorsCountLabel.Location = new Point(285, 61);
        _todayVisitorsCountLabel.Name = "_TodayVisitorsCountLabel";
        _todayVisitorsCountLabel.Size = new Size(29, 16);
        _todayVisitorsCountLabel.TabIndex = 2;
        _todayVisitorsCountLabel.Text = "568";
        // 
        // _todayVisitorsCountLabelLabel
        // 
        _todayVisitorsCountLabelLabel.AutoSize = true;
        _todayVisitorsCountLabelLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _todayVisitorsCountLabelLabel.ForeColor = Color.White;
        _todayVisitorsCountLabelLabel.Location = new Point(274, 23);
        _todayVisitorsCountLabelLabel.Name = "_TodayVisitorsCountLabelLabel";
        _todayVisitorsCountLabelLabel.Size = new Size(52, 20);
        _todayVisitorsCountLabelLabel.TabIndex = 1;
        _todayVisitorsCountLabelLabel.Text = "Today";
        // 
        // _visitorsCountPanel
        // 
        _visitorsCountPanel.BackColor = Color.FromArgb(10, 30, 60);
        _visitorsCountPanel.Controls.Add(_visitorsCountLabel);
        _visitorsCountPanel.Controls.Add(_visitorsCountLabelLabel);
        _visitorsCountPanel.Location = new Point(0, 0);
        _visitorsCountPanel.Name = "_VisitorsCountPanel";
        _visitorsCountPanel.Size = new Size(200, 109);
        _visitorsCountPanel.TabIndex = 0;
        // 
        // _visitorsCountLabel
        // 
        _visitorsCountLabel.AutoSize = true;
        _visitorsCountLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _visitorsCountLabel.ForeColor = Color.Cyan;
        _visitorsCountLabel.Location = new Point(67, 61);
        _visitorsCountLabel.Name = "_VisitorsCountLabel";
        _visitorsCountLabel.Size = new Size(49, 20);
        _visitorsCountLabel.TabIndex = 1;
        _visitorsCountLabel.Text = "5,274";
        // 
        // _visitorsCountLabelLabel
        // 
        _visitorsCountLabelLabel.AutoSize = true;
        _visitorsCountLabelLabel.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _visitorsCountLabelLabel.ForeColor = Color.White;
        _visitorsCountLabelLabel.Location = new Point(55, 20);
        _visitorsCountLabelLabel.Name = "_VisitorsCountLabelLabel";
        _visitorsCountLabelLabel.Size = new Size(70, 24);
        _visitorsCountLabelLabel.TabIndex = 0;
        _visitorsCountLabelLabel.Text = "Visitors";
        // 
        // _adminPanel
        // 
        _adminPanel.BackColor = Color.FromArgb(240, 130, 40);
        _adminPanel.Controls.Add(_adminNameLabel);
        _adminPanel.Controls.Add(_adminLabelLabel);
        _adminPanel.Controls.Add(_adminIconPanel);
        _adminPanel.Location = new Point(251, 190);
        _adminPanel.Name = "_adminPanel";
        _adminPanel.Size = new Size(341, 130);
        _adminPanel.TabIndex = 4;
        // 
        // _adminNameLabel
        // 
        _adminNameLabel.AutoSize = true;
        _adminNameLabel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _adminNameLabel.ForeColor = Color.White;
        _adminNameLabel.Location = new Point(195, 69);
        _adminNameLabel.Name = "_adminNameLabel";
        _adminNameLabel.Size = new Size(81, 16);
        _adminNameLabel.TabIndex = 2;
        _adminNameLabel.Text = "Pritam Zope";
        // 
        // _adminLabelLabel
        // 
        _adminLabelLabel.AutoSize = true;
        _adminLabelLabel.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _adminLabelLabel.ForeColor = Color.White;
        _adminLabelLabel.Location = new Point(202, 19);
        _adminLabelLabel.Name = "_adminLabelLabel";
        _adminLabelLabel.Size = new Size(65, 24);
        _adminLabelLabel.TabIndex = 1;
        _adminLabelLabel.Text = "Admin";
        // 
        // _adminIconPanel
        // 
        _adminIconPanel.BackgroundImage = Demo.Properties.Resources.pritam12345;
        _adminIconPanel.Location = new Point(0, 0);
        _adminIconPanel.Name = "_adminIconPanel";
        _adminIconPanel.Size = new Size(152, 130);
        _adminIconPanel.TabIndex = 0;
        // 
        // _storagePanel
        // 
        _storagePanel.BackColor = Color.FromArgb(200, 40, 30);
        _storagePanel.Controls.Add(_totalStorageLabel);
        _storagePanel.Controls.Add(_totalStorageLabelLabel);
        _storagePanel.Controls.Add(_freeStorageLabel);
        _storagePanel.Controls.Add(_freeStorageLabelLabel);
        _storagePanel.Controls.Add(_usedStorageLabel);
        _storagePanel.Controls.Add(_usedStorageLabelLabel);
        _storagePanel.Controls.Add(_storageTopPanel);
        _storagePanel.Location = new Point(251, 337);
        _storagePanel.Name = "_StoragePanel";
        _storagePanel.Size = new Size(341, 151);
        _storagePanel.TabIndex = 7;
        // 
        // _totalStorageLabel
        // 
        _totalStorageLabel.AutoSize = true;
        _totalStorageLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _totalStorageLabel.ForeColor = Color.White;
        _totalStorageLabel.Location = new Point(268, 103);
        _totalStorageLabel.Name = "_TotalStorageLabel";
        _totalStorageLabel.Size = new Size(55, 20);
        _totalStorageLabel.TabIndex = 6;
        _totalStorageLabel.Text = "50 GB";
        // 
        // _totalStorageLabelLabel
        // 
        _totalStorageLabelLabel.AutoSize = true;
        _totalStorageLabelLabel.Font = new Font("Microsoft Sans Serif", 12.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _totalStorageLabelLabel.ForeColor = Color.White;
        _totalStorageLabelLabel.Location = new Point(268, 64);
        _totalStorageLabelLabel.Name = "_TotalStorageLabelLabel";
        _totalStorageLabelLabel.Size = new Size(46, 20);
        _totalStorageLabelLabel.TabIndex = 5;
        _totalStorageLabelLabel.Text = "Total";
        // 
        // _freeStorageLabel
        // 
        _freeStorageLabel.AutoSize = true;
        _freeStorageLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _freeStorageLabel.ForeColor = Color.White;
        _freeStorageLabel.Location = new Point(146, 103);
        _freeStorageLabel.Name = "_FreeStorageLabel";
        _freeStorageLabel.Size = new Size(68, 20);
        _freeStorageLabel.TabIndex = 4;
        _freeStorageLabel.Text = "26.5 GB";
        // 
        // _freeStorageLabelLabel
        // 
        _freeStorageLabelLabel.AutoSize = true;
        _freeStorageLabelLabel.Font = new Font("Microsoft Sans Serif", 12.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _freeStorageLabelLabel.ForeColor = Color.White;
        _freeStorageLabelLabel.Location = new Point(157, 64);
        _freeStorageLabelLabel.Name = "_FreeStorageLabelLabel";
        _freeStorageLabelLabel.Size = new Size(43, 20);
        _freeStorageLabelLabel.TabIndex = 3;
        _freeStorageLabelLabel.Text = "Free";
        // 
        // _usedStorageLabel
        // 
        _usedStorageLabel.AutoSize = true;
        _usedStorageLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _usedStorageLabel.ForeColor = Color.White;
        _usedStorageLabel.Location = new Point(27, 103);
        _usedStorageLabel.Name = "_UsedStorageLabel";
        _usedStorageLabel.Size = new Size(68, 20);
        _usedStorageLabel.TabIndex = 2;
        _usedStorageLabel.Text = "23.5 GB";
        // 
        // _usedStorageLabelLabel
        // 
        _usedStorageLabelLabel.AutoSize = true;
        _usedStorageLabelLabel.Font = new Font("Microsoft Sans Serif", 12.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _usedStorageLabelLabel.ForeColor = Color.White;
        _usedStorageLabelLabel.Location = new Point(35, 64);
        _usedStorageLabelLabel.Name = "_UsedStorageLabelLabel";
        _usedStorageLabelLabel.Size = new Size(48, 20);
        _usedStorageLabelLabel.TabIndex = 1;
        _usedStorageLabelLabel.Text = "Used";
        // 
        // _storageTopPanel
        // 
        _storageTopPanel.BackColor = Color.FromArgb(120, 0, 0);
        _storageTopPanel.Controls.Add(_storageTitleLabel);
        _storageTopPanel.Location = new Point(0, 0);
        _storageTopPanel.Name = "_StorageTopPanel";
        _storageTopPanel.Size = new Size(341, 49);
        _storageTopPanel.TabIndex = 0;
        // 
        // _storageTitleLabel
        // 
        _storageTitleLabel.AutoSize = true;
        _storageTitleLabel.Font = new Font("Microsoft Sans Serif", 15.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _storageTitleLabel.ForeColor = Color.White;
        _storageTitleLabel.Location = new Point(119, 11);
        _storageTitleLabel.Name = "_StorageTitleLabel";
        _storageTitleLabel.Size = new Size(81, 25);
        _storageTitleLabel.TabIndex = 0;
        _storageTitleLabel.Text = "Storage";
        // 
        // _controlPanel
        // 
        _controlPanel.BackColor = Color.FromArgb(100, 30, 120);
        _controlPanel.Controls.Add(_controlPanelLabel);
        _controlPanel.Location = new Point(598, 385);
        _controlPanel.Name = "_ControlPanel";
        _controlPanel.Size = new Size(240, 103);
        _controlPanel.TabIndex = 8;
        // 
        // _controlPanelLabel
        // 
        _controlPanelLabel.AutoSize = true;
        _controlPanelLabel.Font = new Font("Microsoft Sans Serif", 15.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _controlPanelLabel.ForeColor = Color.White;
        _controlPanelLabel.Location = new Point(48, 40);
        _controlPanelLabel.Name = "_ControlPanelLabel";
        _controlPanelLabel.Size = new Size(142, 25);
        _controlPanelLabel.TabIndex = 0;
        _controlPanelLabel.Text = "Control Panel";
        // 
        // _deleteBlogButton
        // 
        _deleteBlogButton.BusyBackColor = Color.FromArgb(180, 50, 60);
        _deleteBlogButton.Cursor = Cursors.Hand;
        _deleteBlogButton.DisplayText = "Delete Blog";
        _deleteBlogButton.FlatStyle = FlatStyle.Flat;
        _deleteBlogButton.Font = new Font("Microsoft YaHei UI", 12.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _deleteBlogButton.ForeColor = Color.White;
        _deleteBlogButton.Location = new Point(598, 260);
        _deleteBlogButton.MouseClickColor = Color.FromArgb(100, 10, 10);
        _deleteBlogButton.MouseColorsEnabled = true;
        _deleteBlogButton.MouseHoverColor = Color.FromArgb(150, 30, 30);
        _deleteBlogButton.Name = "_DeleteBlogButton";
        _deleteBlogButton.Size = new Size(240, 60);
        _deleteBlogButton.TabIndex = 6;
        _deleteBlogButton.Text = "Delete Blog";
        _deleteBlogButton.TextLocationLeft = 65;
        _deleteBlogButton.TextLocationTop = 16;
        _deleteBlogButton.UseVisualStyleBackColor = true;
        // 
        // _viewBlogButton
        // 
        _viewBlogButton.BusyBackColor = Color.FromArgb(30, 130, 110);
        _viewBlogButton.Cursor = Cursors.Hand;
        _viewBlogButton.DisplayText = "View Blog";
        _viewBlogButton.FlatStyle = FlatStyle.Flat;
        _viewBlogButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _viewBlogButton.ForeColor = Color.White;
        _viewBlogButton.Location = new Point(599, 191);
        _viewBlogButton.MouseClickColor = Color.FromArgb(10, 80, 50);
        _viewBlogButton.MouseColorsEnabled = true;
        _viewBlogButton.MouseHoverColor = Color.FromArgb(10, 110, 90);
        _viewBlogButton.Name = "_ViewBlogButton";
        _viewBlogButton.Size = new Size(240, 60);
        _viewBlogButton.TabIndex = 5;
        _viewBlogButton.Text = "View Blog";
        _viewBlogButton.TextLocationLeft = 65;
        _viewBlogButton.TextLocationTop = 16;
        _viewBlogButton.UseVisualStyleBackColor = true;
        // 
        // _campaignsPanel
        // 
        _campaignsPanel.BackColor = Color.FromArgb(10, 40, 80);
        _campaignsPanel.Controls.Add(_campaignsLabel);
        _campaignsPanel.Location = new Point(599, 337);
        _campaignsPanel.Name = "_CampaignsPanel";
        _campaignsPanel.Size = new Size(239, 42);
        _campaignsPanel.TabIndex = 9;
        // 
        // _campaignsLabel
        // 
        _campaignsLabel.AutoSize = true;
        _campaignsLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _campaignsLabel.ForeColor = Color.White;
        _campaignsLabel.Location = new Point(73, 11);
        _campaignsLabel.Name = "_CampaignsLabel";
        _campaignsLabel.Size = new Size(89, 20);
        _campaignsLabel.TabIndex = 0;
        _campaignsLabel.Text = "Campaigns";
        // 
        // DashboardForm
        // 
        AutoScaleDimensions = new SizeF(6.0f, 13.0f);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.Gainsboro;
        ClientSize = new Size(873, 515);
        Controls.Add(_campaignsPanel);
        Controls.Add(_controlPanel);
        Controls.Add(_storagePanel);
        Controls.Add(_deleteBlogButton);
        Controls.Add(_viewBlogButton);
        Controls.Add(_adminPanel);
        Controls.Add(_visitorsPanel);
        Controls.Add(_leftPanel);
        Controls.Add(_topPanel);
        Controls.Add(_leftTopPanel);
        FormBorderStyle = FormBorderStyle.None;
        Name = "DashboardForm";
        Text = "Dashboard";
        _leftTopPanel.ResumeLayout(false);
        _leftTopPanel.PerformLayout();
        _topPanel.ResumeLayout(false);
        _topPanel.PerformLayout();
        _leftPanel.ResumeLayout(false);
        _leftBottomPanel.ResumeLayout(false);
        _leftBottomPanel.PerformLayout();
        _visitorsPanel.ResumeLayout(false);
        _visitorsPanel.PerformLayout();
        _visitorsCountPanel.ResumeLayout(false);
        _visitorsCountPanel.PerformLayout();
        _adminPanel.ResumeLayout(false);
        _adminPanel.PerformLayout();
        _storagePanel.ResumeLayout(false);
        _storagePanel.PerformLayout();
        _storageTopPanel.ResumeLayout(false);
        _storageTopPanel.PerformLayout();
        _controlPanel.ResumeLayout(false);
        _controlPanel.PerformLayout();
        _campaignsPanel.ResumeLayout(false);
        _campaignsPanel.PerformLayout();
        ResumeLayout(false);
    }

    private Panel _leftTopPanel;
    private Panel _topPanel;
    private ZopeButton _closeButton;
    private ZopeButton _minButton;
    private Label _leftTopPanelLabel;
    private Panel _iconPanel;
    private Label _dashboardLabel;
    private Panel _leftPanel;
    private ZopeButton _dashboardButton;
    private ZopeButton _statsButton;
    private ZopeButton _layoutButton;
    private ZopeButton _pagesButton;
    private ZopeButton _settingsButton;
    private ZopeButton _themeButton;
    private Panel _leftBottomPanel;
    private Label _leftBottomPanelLabel;
    private Panel _visitorsPanel;
    private Panel _visitorsCountPanel;
    private Label _visitorsCountLabel;
    private Label _visitorsCountLabelLabel;
    private Label _monthVisitorsCountLabel;
    private Label _monthVisitorsCountLabelLabel;
    private Label _todayVisitorsCountLabel;
    private Label _todayVisitorsCountLabelLabel;
    private Panel _adminPanel;
    private Label _adminNameLabel;
    private Label _adminLabelLabel;
    private Panel _adminIconPanel;
    private ZopeButton _viewBlogButton;
    private ZopeButton _deleteBlogButton;
    private Panel _storagePanel;
    private Label _totalStorageLabel;
    private Label _totalStorageLabelLabel;
    private Label _freeStorageLabel;
    private Label _freeStorageLabelLabel;
    private Label _usedStorageLabel;
    private Label _usedStorageLabelLabel;
    private Panel _storageTopPanel;
    private Label _storageTitleLabel;
    private Panel _controlPanel;
    private Label _controlPanelLabel;
    private Panel _campaignsPanel;
    private Label _campaignsLabel;
}
