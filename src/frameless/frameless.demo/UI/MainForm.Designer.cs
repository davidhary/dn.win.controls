namespace cc.isr.WinControls.Demo;
public partial class MainForm
{
    /// <summary>
/// Required designer variable.
/// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
/// Clean up any resources being used.
/// </summary>
/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if ( disposing )
        {
            components?.Dispose();
        }

        base.Dispose(disposing);
    }

    /// <summary>
    /// Required method for Designer support - do not modify the contents of this method with the
    /// code editor.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    private void InitializeComponent()
    {
        _topPanel = new Panel();
        _topPanel.MouseDown += new MouseEventHandler(TopPanel_MouseDown);
        _topPanel.MouseMove += new MouseEventHandler(TopPanel_MouseMove);
        _topPanel.MouseUp += new MouseEventHandler(TopPanel_MouseUp);
        _windowTextLabel = new Label();
        _windowTextLabel.MouseDown += new MouseEventHandler(WindowTextLabel_MouseDown);
        _windowTextLabel.MouseMove += new MouseEventHandler(WindowTextLabel_MouseMove);
        _windowTextLabel.MouseUp += new MouseEventHandler(WindowTextLabel_MouseUp);
        _minButton = new ZopeButton();
        _minButton.Click += new System.EventHandler(MinButton_Click);
        _closeButton = new ZopeButton();
        _closeButton.Click += new System.EventHandler(CloseButton_Click);
        _rightPanel = new Panel();
        _leftPanel = new Panel();
        _bottomPanel = new Panel();
        _exitButton = new ZopeButton();
        _exitButton.Click += new System.EventHandler(ExitButton_Click);
        _openDashboardShapedButton = new ShapedButton();
        _openDashboardShapedButton.Click += new System.EventHandler(OpenDashboardShapedButton_Click);
        _openDarkFormShapedButton = new ShapedButton();
        _openDarkFormShapedButton.Click += new System.EventHandler(OpenDarkFormShapedButton_Click);
        _openBlueFormShapedButton = new ShapedButton();
        _openBlueFormShapedButton.Click += new System.EventHandler(OpenBlueFormShapedButton_Click);
        _topPanel.SuspendLayout();
        SuspendLayout();
        // 
        // _topPanel
        // 
        _topPanel.BackColor = Color.FromArgb(10, 100, 60);
        _topPanel.Controls.Add(_windowTextLabel);
        _topPanel.Controls.Add(_minButton);
        _topPanel.Controls.Add(_closeButton);
        _topPanel.Dock = DockStyle.Top;
        _topPanel.Location = new Point(0, 0);
        _topPanel.Name = "_TopPanel";
        _topPanel.Size = new Size(355, 52);
        _topPanel.TabIndex = 0;
        // 
        // _windowTextLabel
        // 
        _windowTextLabel.AutoSize = true;
        _windowTextLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _windowTextLabel.ForeColor = Color.White;
        _windowTextLabel.Location = new Point(21, 14);
        _windowTextLabel.Name = "_WindowTextLabel";
        _windowTextLabel.Size = new Size(80, 20);
        _windowTextLabel.TabIndex = 4;
        _windowTextLabel.Text = "Main Form";
        // 
        // _minButton
        // 
        _minButton.BusyBackColor = Color.FromArgb(10, 100, 60);
        _minButton.DisplayText = "_";
        _minButton.FlatStyle = FlatStyle.Flat;
        _minButton.Font = new Font("Microsoft YaHei UI", 20.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _minButton.ForeColor = Color.White;
        _minButton.Location = new Point(289, 3);
        _minButton.MouseClickColor = Color.FromArgb(160, 180, 200);
        _minButton.MouseColorsEnabled = true;
        _minButton.MouseHoverColor = Color.FromArgb(10, 80, 40);
        _minButton.Name = "_MinButton";
        _minButton.Size = new Size(31, 24);
        _minButton.TabIndex = 3;
        _minButton.Text = "_";
        _minButton.TextLocationLeft = 6;
        _minButton.TextLocationTop = -20;
        _minButton.UseVisualStyleBackColor = true;
        // 
        // _closeButton
        // 
        _closeButton.BusyBackColor = Color.FromArgb(10, 100, 60);
        _closeButton.DisplayText = "X";
        _closeButton.FlatStyle = FlatStyle.Flat;
        _closeButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _closeButton.ForeColor = Color.White;
        _closeButton.Location = new Point(320, 3);
        _closeButton.MouseClickColor = Color.FromArgb(100, 150, 220);
        _closeButton.MouseColorsEnabled = true;
        _closeButton.MouseHoverColor = Color.FromArgb(10, 80, 40);
        _closeButton.Name = "_CloseButton";
        _closeButton.Size = new Size(31, 24);
        _closeButton.TabIndex = 2;
        _closeButton.Text = "X";
        _closeButton.TextLocationLeft = 6;
        _closeButton.TextLocationTop = -1;
        _closeButton.UseVisualStyleBackColor = true;
        // 
        // _rightPanel
        // 
        _rightPanel.BackColor = Color.FromArgb(10, 100, 60);
        _rightPanel.Dock = DockStyle.Right;
        _rightPanel.Location = new Point(343, 52);
        _rightPanel.Name = "_RightPanel";
        _rightPanel.Size = new Size(12, 375);
        _rightPanel.TabIndex = 1;
        // 
        // _leftPanel
        // 
        _leftPanel.BackColor = Color.FromArgb(10, 100, 60);
        _leftPanel.Dock = DockStyle.Left;
        _leftPanel.Location = new Point(0, 52);
        _leftPanel.Name = "_LeftPanel";
        _leftPanel.Size = new Size(12, 375);
        _leftPanel.TabIndex = 2;
        // 
        // _bottomPanel
        // 
        _bottomPanel.BackColor = Color.FromArgb(10, 100, 60);
        _bottomPanel.Dock = DockStyle.Bottom;
        _bottomPanel.Location = new Point(12, 415);
        _bottomPanel.Name = "_BottomPanel";
        _bottomPanel.Size = new Size(331, 12);
        _bottomPanel.TabIndex = 3;
        // 
        // _exitButton
        // 
        _exitButton.BusyBackColor = Color.FromArgb(10, 100, 60);
        _exitButton.DisplayText = "Exit";
        _exitButton.FlatStyle = FlatStyle.Flat;
        _exitButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _exitButton.ForeColor = Color.White;
        _exitButton.Location = new Point(227, 367);
        _exitButton.MouseClickColor = Color.FromArgb(100, 0, 0);
        _exitButton.MouseColorsEnabled = true;
        _exitButton.MouseHoverColor = Color.FromArgb(180, 30, 40);
        _exitButton.Name = "_ExitButton";
        _exitButton.Size = new Size(93, 29);
        _exitButton.TabIndex = 7;
        _exitButton.Text = "Exit";
        _exitButton.TextLocationLeft = 26;
        _exitButton.TextLocationTop = 3;
        _exitButton.UseVisualStyleBackColor = true;
        // 
        // _openDashboardShapedButton
        // 
        _openDashboardShapedButton.BackColor = Color.Transparent;
        _openDashboardShapedButton.BorderColor = Color.Transparent;
        _openDashboardShapedButton.BorderWidth = 2;
        _openDashboardShapedButton.ButtonShape = ButtonShape.RoundRect;
        _openDashboardShapedButton.ButtonText = "Dashboard UI Form";
        _openDashboardShapedButton.EndColor = Color.FromArgb(80, 20, 40);
        _openDashboardShapedButton.EndOpacity = 250;
        _openDashboardShapedButton.FlatAppearance.BorderSize = 0;
        _openDashboardShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
        _openDashboardShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
        _openDashboardShapedButton.FlatStyle = FlatStyle.Flat;
        _openDashboardShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _openDashboardShapedButton.ForeColor = Color.White;
        _openDashboardShapedButton.GradientAngle = 90;
        _openDashboardShapedButton.Location = new Point(25, 269);
        _openDashboardShapedButton.MouseClickStartColor = Color.FromArgb(20, 20, 40);
        _openDashboardShapedButton.MouseClickEndColor = Color.FromArgb(10, 100, 60);
        _openDashboardShapedButton.MouseHoverStartColor = Color.FromArgb(250, 100, 180);
        _openDashboardShapedButton.MouseHoverEndColor = Color.FromArgb(100, 40, 60);
        _openDashboardShapedButton.Name = "_OpenDashboardShapedButton";
        _openDashboardShapedButton.ShowButtonText = true;
        _openDashboardShapedButton.Size = new Size(312, 75);
        _openDashboardShapedButton.StartColor = Color.FromArgb(220, 40, 120);
        _openDashboardShapedButton.StartOpacity = 250;
        _openDashboardShapedButton.TabIndex = 6;
        _openDashboardShapedButton.Text = "Dashboard UI Form";
        _openDashboardShapedButton.TextLocation = new Point(90, 25);
        _openDashboardShapedButton.UseVisualStyleBackColor = false;
        // 
        // _openDarkFormShapedButton
        // 
        _openDarkFormShapedButton.BackColor = Color.Transparent;
        _openDarkFormShapedButton.BorderColor = Color.Transparent;
        _openDarkFormShapedButton.BorderWidth = 2;
        _openDarkFormShapedButton.ButtonShape = ButtonShape.RoundRect;
        _openDarkFormShapedButton.ButtonText = "Dark Custom Form";
        _openDarkFormShapedButton.EndColor = Color.FromArgb(10, 10, 10);
        _openDarkFormShapedButton.EndOpacity = 250;
        _openDarkFormShapedButton.FlatAppearance.BorderSize = 0;
        _openDarkFormShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
        _openDarkFormShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
        _openDarkFormShapedButton.FlatStyle = FlatStyle.Flat;
        _openDarkFormShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _openDarkFormShapedButton.ForeColor = Color.White;
        _openDarkFormShapedButton.GradientAngle = 90;
        _openDarkFormShapedButton.Location = new Point(25, 172);
        _openDarkFormShapedButton.MouseClickStartColor = Color.FromArgb(20, 20, 40);
        _openDarkFormShapedButton.MouseClickEndColor = Color.FromArgb(10, 100, 60);
        _openDarkFormShapedButton.MouseHoverStartColor = Color.FromArgb(150, 150, 150);
        _openDarkFormShapedButton.MouseHoverEndColor = Color.FromArgb(50, 50, 50);
        _openDarkFormShapedButton.Name = "_OpenDarkFormShapedButton";
        _openDarkFormShapedButton.ShowButtonText = true;
        _openDarkFormShapedButton.Size = new Size(312, 75);
        _openDarkFormShapedButton.StartColor = Color.FromArgb(100, 100, 100);
        _openDarkFormShapedButton.StartOpacity = 250;
        _openDarkFormShapedButton.TabIndex = 5;
        _openDarkFormShapedButton.Text = "shapedButton2";
        _openDarkFormShapedButton.TextLocation = new Point(90, 25);
        _openDarkFormShapedButton.UseVisualStyleBackColor = false;
        // 
        // _openBlueFormShapedButton
        // 
        _openBlueFormShapedButton.BackColor = Color.Transparent;
        _openBlueFormShapedButton.BorderColor = Color.Transparent;
        _openBlueFormShapedButton.BorderWidth = 2;
        _openBlueFormShapedButton.ButtonShape = ButtonShape.RoundRect;
        _openBlueFormShapedButton.ButtonText = "Simple Blue Form";
        _openBlueFormShapedButton.EndColor = Color.MidnightBlue;
        _openBlueFormShapedButton.EndOpacity = 250;
        _openBlueFormShapedButton.FlatAppearance.BorderSize = 0;
        _openBlueFormShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
        _openBlueFormShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
        _openBlueFormShapedButton.FlatStyle = FlatStyle.Flat;
        _openBlueFormShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _openBlueFormShapedButton.ForeColor = Color.White;
        _openBlueFormShapedButton.GradientAngle = 90;
        _openBlueFormShapedButton.Location = new Point(25, 77);
        _openBlueFormShapedButton.MouseClickStartColor = Color.FromArgb(20, 20, 40);
        _openBlueFormShapedButton.MouseClickEndColor = Color.FromArgb(10, 100, 60);
        _openBlueFormShapedButton.MouseHoverStartColor = Color.Turquoise;
        _openBlueFormShapedButton.MouseHoverEndColor = Color.DarkSlateGray;
        _openBlueFormShapedButton.Name = "_OpenBlueFormShapedButton";
        _openBlueFormShapedButton.ShowButtonText = true;
        _openBlueFormShapedButton.Size = new Size(312, 75);
        _openBlueFormShapedButton.StartColor = Color.DodgerBlue;
        _openBlueFormShapedButton.StartOpacity = 250;
        _openBlueFormShapedButton.TabIndex = 4;
        _openBlueFormShapedButton.Text = "Simple Blue Form";
        _openBlueFormShapedButton.TextLocation = new Point(90, 26);
        _openBlueFormShapedButton.UseVisualStyleBackColor = false;
        // 
        // MainForm
        // 
        AutoScaleDimensions = new SizeF(6.0f, 13.0f);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(40, 180, 100);
        ClientSize = new Size(355, 427);
        Controls.Add(_exitButton);
        Controls.Add(_openDashboardShapedButton);
        Controls.Add(_openDarkFormShapedButton);
        Controls.Add(_openBlueFormShapedButton);
        Controls.Add(_bottomPanel);
        Controls.Add(_leftPanel);
        Controls.Add(_rightPanel);
        Controls.Add(_topPanel);
        FormBorderStyle = FormBorderStyle.None;
        Name = "MainForm";
        StartPosition = FormStartPosition.CenterScreen;
        Text = "Main Form";
        _topPanel.ResumeLayout(false);
        _topPanel.PerformLayout();
        ResumeLayout(false);
    }

    private Panel _topPanel;
    private ZopeButton _minButton;
    private ZopeButton _closeButton;
    private Label _windowTextLabel;
    private Panel _rightPanel;
    private Panel _leftPanel;
    private Panel _bottomPanel;
    private ShapedButton _openBlueFormShapedButton;
    private ShapedButton _openDarkFormShapedButton;
    private ShapedButton _openDashboardShapedButton;
    private ZopeButton _exitButton;
}
