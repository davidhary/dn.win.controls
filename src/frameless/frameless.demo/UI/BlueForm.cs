namespace cc.isr.WinControls.Demo;

/// <summary>   Form for viewing the blue. </summary>
/// <remarks>   David, 2021-03-12. </remarks>
public partial class BlueForm : Form
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Form" /> class.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public BlueForm() => this.InitializeComponent();

    /// <summary>   True if is top panel dragged, false if not. </summary>
    private bool _isTopPanelDragged;

    /// <summary>   True if is left panel dragged, false if not. </summary>
    private bool _isLeftPanelDragged;

    /// <summary>   True if is right panel dragged, false if not. </summary>
    private bool _isRightPanelDragged;

    /// <summary>   True if is bottom panel dragged, false if not. </summary>
    private bool _isBottomPanelDragged;

    /// <summary>   True if is top border panel dragged, false if not. </summary>
    private bool _isTopBorderPanelDragged;

    /// <summary>   True if is window maximized, false if not. </summary>
    private bool _isWindowMaximized;

    /// <summary>   The offset. </summary>
    private Point _offset;

    /// <summary>   Size of the normal window. </summary>
    private Size _normalWindowSize;

    /// <summary>   The normal window location. </summary>
    private Point _normalWindowLocation = Point.Empty;

    /// <summary>   Top border panel mouse down. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void TopBorderPanel_MouseDown( object? sender, MouseEventArgs e )
    {
        this._isTopBorderPanelDragged = e.Button == MouseButtons.Left;
    }

    /// <summary>   Top border panel mouse move. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void TopBorderPanel_MouseMove( object? sender, MouseEventArgs e )
    {
        if ( e.Y < this.Location.Y )
        {
            if ( this._isTopBorderPanelDragged )
            {
                if ( this.Height < 50 )
                {
                    this.Height = 50;
                    this._isTopBorderPanelDragged = false;
                }
                else
                {
                    this.Location = new Point( this.Location.X, this.Location.Y + e.Y );
                    this.Height -= e.Y;
                }
            }
        }
    }

    /// <summary>   Top border panel mouse up. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void TopBorderPanel_MouseUp( object? sender, MouseEventArgs e )
    {
        this._isTopBorderPanelDragged = false;
    }

    /// <summary>   Top panel mouse down. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void TopPanel_MouseDown( object? sender, MouseEventArgs e )
    {
        if ( e.Button == MouseButtons.Left )
        {
            this._isTopPanelDragged = true;
            Point pointStartPosition = this.PointToScreen( new Point( e.X, e.Y ) );
            this._offset = new Point()
            {
                X = this.Location.X - pointStartPosition.X,
                Y = this.Location.Y - pointStartPosition.Y
            };
        }
        else
        {
            this._isTopPanelDragged = false;
        }

        if ( e.Clicks == 2 )
        {
            this._isTopPanelDragged = false;
            this.MaxButton_Click( sender, e );
        }
    }

    /// <summary>   Top panel mouse move. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void TopPanel_MouseMove( object? sender, MouseEventArgs e )
    {
        if ( this._isTopPanelDragged )
        {
            Point newPoint = this._topPanel.PointToScreen( new Point( e.X, e.Y ) );
            newPoint.Offset( this._offset );
            this.Location = newPoint;
            if ( this.Location.X > 2 || this.Location.Y > 2 )
            {
                if ( this.WindowState == FormWindowState.Maximized )
                {
                    this.Location = this._normalWindowLocation;
                    this.Size = this._normalWindowSize;
                    this._toolTip.SetToolTip( this._maxButton, "Maximize" );
                    this._maxButton.CustomFormState = CustomFormState.Normal;
                    this._isWindowMaximized = false;
                }
            }
        }
    }

    /// <summary>   Top panel mouse up. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void TopPanel_MouseUp( object? sender, MouseEventArgs e )
    {
        this._isTopPanelDragged = false;
        if ( this.Location.Y <= 5 )
        {
            if ( !this._isWindowMaximized )
            {
                this._normalWindowSize = this.Size;
                this._normalWindowLocation = this.Location;
                if ( Screen.PrimaryScreen?.WorkingArea is Rectangle rect )
                {
                    this.Location = new Point( 0, 0 );
                    this.Size = new Size( rect.Width, rect.Height );
                    this._toolTip.SetToolTip( this._maxButton, "Restore Down" );
                    this._maxButton.CustomFormState = CustomFormState.Maximize;
                    this._isWindowMaximized = true;
                }
            }
        }
    }

    /// <summary>   Left panel mouse down. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void LeftPanel_MouseDown( object? sender, MouseEventArgs e )
    {
        if ( this.Location.X <= 0 || e.X < 0 )
        {
            this._isLeftPanelDragged = false;
            this.Location = new Point( 10, this.Location.Y );
        }
        else
        {
            this._isLeftPanelDragged = e.Button == MouseButtons.Left;
        }
    }

    /// <summary>   Left panel mouse move. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void LeftPanel_MouseMove( object? sender, MouseEventArgs e )
    {
        if ( e.X < this.Location.X )
        {
            if ( this._isLeftPanelDragged )
            {
                if ( this.Width < 100 )
                {
                    this.Width = 100;
                    this._isLeftPanelDragged = false;
                }
                else
                {
                    this.Location = new Point( this.Location.X + e.X, this.Location.Y );
                    this.Width -= e.X;
                }
            }
        }
    }

    /// <summary>   Left panel mouse up. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void LeftPanel_MouseUp( object? sender, MouseEventArgs e )
    {
        this._isLeftPanelDragged = false;
    }

    /// <summary>   Right panel mouse down. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void RightPanel_MouseDown( object? sender, MouseEventArgs e )
    {
        this._isRightPanelDragged = e.Button == MouseButtons.Left;
    }

    /// <summary>   Right panel mouse move. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void RightPanel_MouseMove( object? sender, MouseEventArgs e )
    {
        if ( this._isRightPanelDragged )
        {
            if ( this.Width < 100 )
            {
                this.Width = 100;
                this._isRightPanelDragged = false;
            }
            else
            {
                this.Width += e.X;
            }
        }
    }

    /// <summary>   Right panel mouse up. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void RightPanel_MouseUp( object? sender, MouseEventArgs e )
    {
        this._isRightPanelDragged = false;
    }

    /// <summary>   Bottom panel mouse down. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void BottomPanel_MouseDown( object? sender, MouseEventArgs e )
    {
        this._isBottomPanelDragged = e.Button == MouseButtons.Left;
    }

    /// <summary>   Bottom panel mouse move. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void BottomPanel_MouseMove( object? sender, MouseEventArgs e )
    {
        if ( this._isBottomPanelDragged )
        {
            if ( this.Height < 50 )
            {
                this.Height = 50;
                this._isBottomPanelDragged = false;
            }
            else
            {
                this.Height += e.Y;
            }
        }
    }

    /// <summary>   Bottom panel mouse up. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void BottomPanel_MouseUp( object? sender, MouseEventArgs e )
    {
        this._isBottomPanelDragged = false;
    }

    /// <summary>   Closes button click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void CloseButton_Click( object? sender, EventArgs e )
    {
        this.Close();
    }

    /// <summary>   Maximum button click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void MaxButton_Click( object? sender, EventArgs e )
    {
        if ( this._isWindowMaximized )
        {
            this.Location = this._normalWindowLocation;
            this.Size = this._normalWindowSize;
            this._toolTip.SetToolTip( this._maxButton, "Maximize" );
            this._maxButton.CustomFormState = CustomFormState.Normal;
            this._isWindowMaximized = false;
        }
        else
        {
            this._normalWindowSize = this.Size;
            this._normalWindowLocation = this.Location;
            if ( Screen.PrimaryScreen?.WorkingArea is Rectangle rect )
            {
                this.Location = new Point( 0, 0 );
                this.Size = new Size( rect.Width, rect.Height );
                this._toolTip.SetToolTip( this._maxButton, "Restore Down" );
                this._maxButton.CustomFormState = CustomFormState.Maximize;
                this._isWindowMaximized = true;
            }
        }
    }

    /// <summary>   Minimum button click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void MinButton_Click( object? sender, EventArgs e )
    {
        this.WindowState = FormWindowState.Minimized;
    }
}
