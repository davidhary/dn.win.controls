using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A Zope button. </summary>
/// <remarks>
/// (c) 2017 Pritam ZOPE, All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-03-08, 3.1.6276 </para><para>
/// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
/// </para>
/// </remarks>
public class ZopeButton : Button
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Button" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public ZopeButton() : base()
    {
        this.Size = new Size( 31, 24 );
        this.ForeColor = Color.White;
        this.FlatStyle = FlatStyle.Flat;
        this.Font = new Font( SystemFonts.DefaultFont.FontFamily, 20.25f, FontStyle.Bold, GraphicsUnit.Point, 0 );
        this.Text = "_";
        this._displayText = this.Text;
    }

    private string _displayText = "_";

    /// <summary> Gets or sets the display text. </summary>
    /// <value> The display text. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string DisplayText
    {
        get => this._displayText;
        set
        {
            this._displayText = value;
            this.Invalidate();
        }
    }

    /// <summary> The busy back color. </summary>
    private Color _busyBackColor = Color.Teal;

    /// <summary> Gets or sets the color of the busy back. </summary>
    /// <value> The color of the busy back. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color BusyBackColor
    {
        get => this._busyBackColor;
        set
        {
            this._busyBackColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The mouse hover color. </summary>
    private Color _mouseHoverColor = Color.FromArgb( 0, 0, 140 );

    /// <summary> Gets or sets the color of the mouse hover. </summary>
    /// <value> The color of the mouse hover. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color MouseHoverColor
    {
        get => this._mouseHoverColor;
        set
        {
            this._mouseHoverColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The mouse click color. </summary>
    private Color _mouseClickColor = Color.FromArgb( 160, 180, 200 );

    /// <summary> Gets or sets the color of the mouse click. </summary>
    /// <value> The color of the mouse click. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color MouseClickColor
    {
        get => this._mouseClickColor;
        set
        {
            this._mouseClickColor = value;
            this.Invalidate();
        }
    }

    /// <summary> True to enable, false to disable the mouse colors. </summary>
    private bool _mouseColorsEnabled = true;

    /// <summary> Gets or sets the mouse colors enabled. </summary>
    /// <value> The mouse colors enabled. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool MouseColorsEnabled
    {
        get => this._mouseColorsEnabled;
        set
        {
            this._mouseColorsEnabled = value;
            this.Invalidate();
        }
    }

    /// <summary> The text location left. </summary>
    private int _textLocationLeft = 6;

    /// <summary> Gets or sets the text location left. </summary>
    /// <value> The text location left. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int TextLocationLeft
    {
        get => this._textLocationLeft;
        set
        {
            this._textLocationLeft = value;
            this.Invalidate();
        }
    }

    /// <summary> The text location top. </summary>
    private int _textLocationTop = -20;

    /// <summary> Gets or sets the text location top. </summary>
    /// <value> The text location top. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int TextLocationTop
    {
        get => this._textLocationTop;
        set
        {
            this._textLocationTop = value;
            this.Invalidate();
        }
    }

    /// <summary> The cached color. </summary>
    private Color _cachedColor;

    /// <summary>
    /// Raises the <see cref="Control.OnMouseEnter(EventArgs)" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnMouseEnter( EventArgs e )
    {
        base.OnMouseEnter( e );
        this._cachedColor = this.BusyBackColor;
        this._busyBackColor = this.MouseHoverColor;
    }

    /// <summary>
    /// Raises the <see cref="Control.OnMouseLeave(EventArgs)" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnMouseLeave( EventArgs e )
    {
        base.OnMouseLeave( e );
        if ( this._mouseColorsEnabled )
        {
            this._busyBackColor = this._cachedColor;
        }
    }

    /// <summary>
    /// Raises the
    /// <see cref="Control.OnMouseDown(MouseEventArgs)" />
    /// event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="mevent"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseDown( MouseEventArgs mevent )
    {
        base.OnMouseDown( mevent );
        if ( this.MouseColorsEnabled )
        {
            this._busyBackColor = this.MouseClickColor;
        }
    }

    /// <summary>
    /// Raises the
    /// <see cref="ButtonBase.OnMouseUp(MouseEventArgs)" />
    /// event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="mevent"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseUp( MouseEventArgs mevent )
    {
        base.OnMouseUp( mevent );
        if ( this.MouseColorsEnabled )
        {
            this._busyBackColor = this._cachedColor;
        }
    }

    /// <summary>
    /// Raises the
    /// <see cref="ButtonBase.OnPaint(PaintEventArgs)" />
    /// event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="pevent"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs pevent )
    {
        if ( pevent is null ) return;

        base.OnPaint( pevent );
        this._displayText = this.Text;
        if ( this._textLocationLeft == 100 && this._textLocationTop == 25 )
        {
            this._textLocationLeft = (this.Width / 3) + 10;
            this._textLocationTop = (this.Height / 2) - 1;
        }

        Point p = new( this.TextLocationLeft, this.TextLocationTop );
        using ( SolidBrush b = new( this._busyBackColor ) )
        {
            pevent.Graphics.FillRectangle( b, this.ClientRectangle );
        }

        using ( SolidBrush b = new( this.ForeColor ) )
        {
            pevent.Graphics.DrawString( this.DisplayText, this.Font, b, p );
        }
    }
}
