using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A minimum maximum button. </summary>
/// <remarks>
/// (c) 2017 Pritam ZOPE, All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-03-08, 3.1.6276 </para><para>
/// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
/// </para>
/// </remarks>
public class MinMaxButton : Button
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Button" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public MinMaxButton() : base()
    {
        this.Size = new Size( 31, 24 );
        this.Font = new Font( SystemFonts.DefaultFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0 );
        this.ForeColor = Color.White;
        this.FlatStyle = FlatStyle.Flat;
        this.Text = "_";
        this._displayText = this.Text;
    }

    private CustomFormState _customFormState;

    /// <summary> Gets or sets the state of the custom form. </summary>
    /// <value> The custom form state. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public CustomFormState CustomFormState
    {
        get => this._customFormState;
        set
        {
            this._customFormState = value;
            this.Invalidate();
        }
    }

    private string _displayText = "_";

    /// <summary> Gets or sets the display text. </summary>
    /// <value> The display text. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string DisplayText
    {
        get => this._displayText;
        set
        {
            this._displayText = value;
            this.Invalidate();
        }
    }

    private Color _busyBackColor = Color.Gray;

    /// <summary> Gets or sets the color of the busy back. </summary>
    /// <value> The color of the busy back. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color BusyBackColor
    {
        get => this._busyBackColor;
        set
        {
            this._busyBackColor = value;
            this.Invalidate();
        }
    }

    private Color _mouseHoverColor = Color.FromArgb( 180, 200, 240 );

    /// <summary> Gets or sets the color of the mouse hover. </summary>
    /// <value> The color of the mouse hover. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color MouseHoverColor
    {
        get => this._mouseHoverColor;
        set
        {
            this._mouseHoverColor = value;
            this.Invalidate();
        }
    }

    private Color _mouseClickColor = Color.FromArgb( 160, 180, 200 );

    /// <summary> Gets or sets the color of the mouse click. </summary>
    /// <value> The color of the mouse click. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color MouseClickColor
    {
        get => this._mouseClickColor;
        set
        {
            this._mouseClickColor = value;
            this.Invalidate();
        }
    }

    private Point _textLocation = new( 6, -20 );

    /// <summary> Gets or sets the text location. </summary>
    /// <value> The text location. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Point TextLocation
    {
        get => this._textLocation;
        set
        {
            this._textLocation = value;
            this.Invalidate();
        }
    }

    /// <summary> The cached back color. </summary>
    private Color _cachedBackColor;

    /// <summary>
    /// Raises the <see cref="Control.OnMouseEnter(EventArgs)" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnMouseEnter( EventArgs e )
    {
        base.OnMouseEnter( e );
        this._cachedBackColor = this._busyBackColor;
        this._busyBackColor = this._mouseHoverColor;
    }

    /// <summary>
    /// Raises the <see cref="Control.OnMouseLeave(EventArgs)" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnMouseLeave( EventArgs e )
    {
        base.OnMouseLeave( e );
        this._busyBackColor = this._cachedBackColor;
    }

    /// <summary>
    /// Raises the
    /// <see cref="Control.OnMouseDown(MouseEventArgs)" />
    /// event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="mevent"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseDown( MouseEventArgs mevent )
    {
        base.OnMouseDown( mevent );
        this._busyBackColor = this._mouseClickColor;
    }

    /// <summary>
    /// Raises the
    /// <see cref="ButtonBase.OnMouseUp(MouseEventArgs)" />
    /// event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="mevent"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseUp( MouseEventArgs mevent )
    {
        base.OnMouseUp( mevent );
        this._busyBackColor = this._cachedBackColor;
    }

    /// <summary>
    /// Raises the
    /// <see cref="ButtonBase.OnPaint(PaintEventArgs)" />
    /// event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="pevent"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs pevent )
    {
        if ( pevent is null ) return;

        base.OnPaint( pevent );
        switch ( this._customFormState )
        {
            case CustomFormState.Normal:
                {
                    using ( SolidBrush brush = new( this.BusyBackColor ) )
                    {
                        pevent.Graphics.FillRectangle( brush, this.ClientRectangle );
                    }

                    // draw and fill the rectangles of maximized window     
                    using Pen p = new( this.ForeColor );
                    using SolidBrush b = new( this.ForeColor );
                    for ( int i = 0; i <= 1; i++ )
                    {
                        pevent.Graphics.DrawRectangle( p, this.TextLocation.X + i + 1, this.TextLocation.Y, 10, 10 );
                        pevent.Graphics.FillRectangle( b, this.TextLocation.X + 1, this.TextLocation.Y - 1, 12, 4 );
                    }

                    break;
                }

            case CustomFormState.Maximize:
                {
                    using ( SolidBrush brush = new( this.BusyBackColor ) )
                    {
                        pevent.Graphics.FillRectangle( brush, this.ClientRectangle );
                    }

                    using Pen p = new( this.ForeColor );
                    using SolidBrush b = new( this.ForeColor );
                    // draw and fill the rectangles of maximized window     
                    for ( int i = 0; i <= 1; i++ )
                    {
                        pevent.Graphics.DrawRectangle( p, this.TextLocation.X + 5, this.TextLocation.Y, 8, 8 );
                        pevent.Graphics.FillRectangle( b, this.TextLocation.X + 5, this.TextLocation.Y - 1, 9, 4 );
                        pevent.Graphics.DrawRectangle( p, this.TextLocation.X + 2, this.TextLocation.Y + 5, 8, 8 );
                        pevent.Graphics.FillRectangle( b, this.TextLocation.X + 2, this.TextLocation.Y + 4, 9, 4 );
                    }

                    break;
                }

            default:
                break;
        }
    }
}
/// <summary> Values that represent custom form states. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum CustomFormState
{
    /// <summary> An enum constant representing the normal option. </summary>
    Normal,

    /// <summary> An enum constant representing the maximize option. </summary>
    Maximize
}
