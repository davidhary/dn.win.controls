using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A shaped button. </summary>
/// <remarks>
/// (c) 2017 Pritam ZOPE, All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-03-08, 3.1.6276 </para><para>
/// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
/// </para>
/// </remarks>
public class ShapedButton : Button
{
    #region " construction "

    /// <summary>
    /// Initializes a new instance of the <see cref="Button" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public ShapedButton()
    {
        this.Size = new Size( 100, 40 );
        this.BackColor = Color.Transparent;
        this.FlatStyle = FlatStyle.Flat;
        this.FlatAppearance.BorderSize = 0;
        this.FlatAppearance.MouseOverBackColor = Color.Transparent;
        this.FlatAppearance.MouseDownBackColor = Color.Transparent;
        this._buttonText = this.Text;
        this.Font = new Font( SystemFonts.DefaultFont.FontFamily, 20.25f, FontStyle.Bold, GraphicsUnit.Point, 0 );
    }

    #endregion

    #region " shape "

    private int _radiusPercent = 25;

    /// <summary> Gets or sets the radius percent. </summary>
    /// <value> The radius percent. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int RadiusPercent
    {
        get => this._radiusPercent;
        set
        {
            this._radiusPercent = value;
            this.Invalidate();
        }
    }

    private ButtonShape _buttonShape;

    /// <summary> Gets or sets the button shape. </summary>
    /// <value> The button shape. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ButtonShape ButtonShape
    {
        get => this._buttonShape;
        set
        {
            this._buttonShape = value;
            this.Invalidate();
        }
    }

    #endregion

    #region " text "

    private string _buttonText = string.Empty;

    /// <summary> Gets or sets the button text. </summary>
    /// <value> The button text. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string ButtonText
    {
        get => this._buttonText;
        set
        {
            this._buttonText = value;
            this.Invalidate();
        }
    }

    private Point _textLocation = new( 100, 25 );

    /// <summary> Gets or sets the text location. </summary>
    /// <value> The text location. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Point TextLocation
    {
        get => this._textLocation;
        set
        {
            this._textLocation = value;
            this.Invalidate();
        }
    }

    private bool _showButtonText = true;

    /// <summary> True to show, false to hide the button text. </summary>
    /// <value> The show button text. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool ShowButtonText
    {
        get => this._showButtonText;
        set
        {
            this._showButtonText = value;
            this.Invalidate();
        }
    }

    #endregion

    #region " border "

    private int _borderWidth = 2;

    /// <summary> Gets or sets the width of the border. </summary>
    /// <value> The width of the border. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int BorderWidth
    {
        get => this._borderWidth;
        set
        {
            this._borderWidth = value;
            this.Invalidate();
        }
    }

    /// <summary> Sets border color. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="bdrColor"> The bdr color. </param>
    private void SetBorderColor( Color bdrColor )
    {
        int red = bdrColor.R - 40;
        int green = bdrColor.G - 40;
        int blue = bdrColor.B - 40;
        if ( red < 0 )
        {
            red = 0;
        }

        if ( green < 0 )
        {
            green = 0;
        }

        if ( blue < 0 )
        {
            blue = 0;
        }

        this._buttonBorderColor = Color.FromArgb( red, green, blue );
    }

    /// <summary> The button border color. </summary>
    private Color _buttonBorderColor = Color.FromArgb( 220, 220, 220 );

    private Color _borderColor = Color.Transparent;

    /// <summary> Gets or sets the color of the border. </summary>
    /// <value> The color of the border. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color BorderColor
    {
        get => this._borderColor;
        set
        {
            this._borderColor = value;
            if ( this._borderColor == Color.Transparent )
            {
                this._buttonBorderColor = Color.FromArgb( 220, 220, 220 );
            }
            else
            {
                this.SetBorderColor( this._borderColor );
            }
        }
    }

    #endregion

    #region " button colors "

    private Color _startColor = Color.DodgerBlue;

    /// <summary> Gets or sets the color of the start. </summary>
    /// <value> The color of the start. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color StartColor
    {
        get => this._startColor;
        set
        {
            this._startColor = value;
            this.Invalidate();
        }
    }

    private Color _endColor = Color.MidnightBlue;

    /// <summary> Gets or sets the color of the end. </summary>
    /// <value> The color of the end. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color EndColor
    {
        get => this._endColor;
        set
        {
            this._endColor = value;
            this.Invalidate();
        }
    }

    private Color _mouseHoverStartColor = Color.Turquoise;

    /// <summary> Gets or sets the color of the mouse hover start. </summary>
    /// <value> The color of the mouse hover start. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color MouseHoverStartColor
    {
        get => this._mouseHoverStartColor;
        set
        {
            this._mouseHoverStartColor = value;
            this.Invalidate();
        }
    }

    private Color _mouseHoverEndColor = Color.DarkSlateGray;

    /// <summary> Gets or sets the color of the mouse hover end. </summary>
    /// <value> The color of the mouse hover end. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color MouseHoverEndColor
    {
        get => this._mouseHoverEndColor;
        set
        {
            this._mouseHoverEndColor = value;
            this.Invalidate();
        }
    }

    private Color _mouseClickStartColor = Color.Yellow;

    /// <summary> Gets or sets the color of the mouse click start. </summary>
    /// <value> The color of the mouse click start. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color MouseClickStartColor
    {
        get => this._mouseClickStartColor;
        set
        {
            this._mouseClickStartColor = value;
            this.Invalidate();
        }
    }

    private Color _mouseClickEndColor = Color.Red;

    /// <summary> Gets or sets the color of the mouse click end. </summary>
    /// <value> The color of the mouse click end. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color MouseClickEndColor
    {
        get => this._mouseClickEndColor;
        set
        {
            this._mouseClickEndColor = value;
            this.Invalidate();
        }
    }

    private int _startOpacity = 250;

    /// <summary> Gets or sets the start opacity. </summary>
    /// <value> The start opacity. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int StartOpacity
    {
        get => this._startOpacity;
        set
        {
            this._startOpacity = value;
            if ( this._startOpacity > 255 )
            {
                this._startOpacity = 255;
                this.Invalidate();
            }
            else
            {
                this.Invalidate();
            }
        }
    }

    private int _endOpacity = 250;

    /// <summary> Gets or sets the end opacity. </summary>
    /// <value> The end opacity. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int EndOpacity
    {
        get => this._endOpacity;
        set
        {
            this._endOpacity = value;
            if ( this._endOpacity > 255 )
            {
                this._endOpacity = 255;
                this.Invalidate();
            }
            else
            {
                this.Invalidate();
            }
        }
    }

    private int _gradientAngle = 90;

    /// <summary> Gets or sets the gradient angle. </summary>
    /// <value> The gradient angle. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int GradientAngle
    {
        get => this._gradientAngle;
        set
        {
            this._gradientAngle = value;
            this.Invalidate();
        }
    }

    /// <summary> The cached start color. </summary>
    private Color _cachedStartColor, _cachedEndColor;

    /// <summary>
    /// Raises the <see cref="Control.OnMouseEnter(EventArgs)" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnMouseEnter( EventArgs e )
    {
        base.OnMouseEnter( e );
        this._cachedStartColor = this._startColor;
        this._cachedEndColor = this._endColor;
        this._startColor = this._mouseHoverStartColor;
        this._endColor = this._mouseHoverEndColor;
    }

    /// <summary>
    /// Raises the <see cref="Control.OnMouseLeave(EventArgs)" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnMouseLeave( EventArgs e )
    {
        base.OnMouseLeave( e );
        this._startColor = this._cachedStartColor;
        this._endColor = this._cachedEndColor;
        this.SetBorderColor( this._borderColor );
    }

    /// <summary>
    /// Raises the
    /// <see cref="Control.OnMouseDown(MouseEventArgs)" />
    /// event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="mevent"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseDown( MouseEventArgs mevent )
    {
        base.OnMouseDown( mevent );
        this._startColor = this._mouseClickStartColor;
        this._endColor = this._mouseClickEndColor;
        this._buttonBorderColor = this._borderColor;
        this.SetBorderColor( this._borderColor );
        this.Invalidate();
    }

    /// <summary>
    /// Raises the
    /// <see cref="ButtonBase.OnMouseUp(MouseEventArgs)" />
    /// event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="mevent"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseUp( MouseEventArgs mevent )
    {
        base.OnMouseUp( mevent );
        this.OnMouseLeave( mevent );
        this._startColor = this._cachedStartColor;
        this._endColor = this._cachedEndColor;
        this.SetBorderColor( this._borderColor );
        this.Invalidate();
    }

    /// <summary>
    /// Raises the <see cref="ButtonBase.OnLostFocus(EventArgs)" />
    /// event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnLostFocus( EventArgs e )
    {
        base.OnLostFocus( e );
        this._startColor = this._cachedStartColor;
        this._endColor = this._cachedEndColor;
        this.Invalidate();
    }

    #endregion

    #region " render "

    /// <summary> Raises the <see cref="Control.Resize" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnResize( EventArgs e )
    {
        base.OnResize( e );
        this.TextLocation = new Point( (this.Width / 3) - 1, (this.Height / 3) + 5 );
    }

    /// <summary> Draw circular button. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="g"> The Graphics to process. </param>
    private void DrawCircularButton( Graphics g )
    {
        Color c1 = Color.FromArgb( this._startOpacity, this._startColor );
        Color c2 = Color.FromArgb( this._endOpacity, this._endColor );
        using ( Brush b = new LinearGradientBrush( this.ClientRectangle, c1, c2, this._gradientAngle ) )
        {
            g.FillEllipse( b, 5, 5, this.Width - 10, this.Height - 10 );
        }

        using ( SolidBrush b = new( this._buttonBorderColor ) )
        {
            using Pen p = new( b );
            for ( int i = 0, loopTo = this._borderWidth - 1; i <= loopTo; i++ )
            {
                g.DrawEllipse( p, 5 + i, 5, this.Width - 10, this.Height - 10 );
            }
        }

        if ( this._showButtonText )
        {
            Point p = new( this.TextLocation.X, this.TextLocation.Y );
            using SolidBrush sb = new( this.ForeColor );
            g.DrawString( this._buttonText, this.Font, sb, p );
        }
    }

    /// <summary> Draw round rectangular button. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="g"> The Graphics to process. </param>
    private void DrawRoundRectangularButton( Graphics g )
    {
        Color c1 = Color.FromArgb( this._startOpacity, this._startColor );
        Color c2 = Color.FromArgb( this._endOpacity, this._endColor );
        Rectangle rec = new( 5, 5, this.Width, this.Height );

        // create the radius variable and set it equal to 20% the height of the rectangle
        // this will determine the amount of bend at the corners
        int radius = ( int ) Math.Truncate( 0.01d * Math.Min( rec.Height, rec.Height ) * this.RadiusPercent );

        // make sure that we have a valid radius, too small and we have a problem
        if ( radius < 1 )
        {
            radius = 2;
        }

        int diameter = radius * 2;

        // create an x and y variable so that we can reduce the length of our code lines
        int x = rec.Left;
        int y = rec.Top;
        int h = rec.Height;
        int w = rec.Width;
        using ( Region region = new( rec ) )
        {
            using ( GraphicsPath path = new() )
            {
                path.AddArc( x, y, diameter, diameter, 180f, 90f );
                path.AddArc( w - diameter - x, y, diameter, diameter, 270f, 90f );
                path.AddArc( w - diameter - x, h - diameter - y, diameter, diameter, 0f, 90f );
                path.AddArc( x, h - diameter - y, diameter, diameter, 90f, 90f );
                path.CloseFigure();
                region.Intersect( path );
            }

            using Brush b = new LinearGradientBrush( this.ClientRectangle, c1, c2, this._gradientAngle );
            g.FillRegion( b, region );
        }

        using ( Pen p = new( this._buttonBorderColor ) )
        {
            for ( int i = 0, loopTo = this._borderWidth - 1; i <= loopTo; i++ )
            {
                g.DrawArc( p, x + i, y + i, diameter, diameter, 180, 90 );
                g.DrawLine( p, x + radius, y + i, this.Width - x - radius, y + i );
                g.DrawArc( p, this.Width - diameter - x - i, y + i, diameter, diameter, 270, 90 );
                g.DrawLine( p, x + i, y + radius, x + i, this.Height - y - radius );
                g.DrawLine( p, this.Width - x - i, y + radius, this.Width - x - i, this.Height - y - radius );
                g.DrawArc( p, this.Width - diameter - x - i, this.Height - diameter - y - i, diameter, diameter, 0, 90 );
                g.DrawLine( p, x + radius, this.Height - y - i, this.Width - x - radius, this.Height - y - i );
                g.DrawArc( p, x + i, this.Height - diameter - y - i, diameter, diameter, 90, 90 );
            }
        }

        if ( this._showButtonText )
        {
            using SolidBrush sb = new( this.ForeColor );
            g.DrawString( this.ButtonText, this.Font, sb, this.TextLocation );
        }
    }

    /// <summary>
    /// Raises the
    /// <see cref="ButtonBase.OnPaint(PaintEventArgs)" />
    /// event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="pevent"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs pevent )
    {
        if ( pevent is null ) return;

        pevent.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
        base.OnPaint( pevent );
        switch ( this._buttonShape )
        {
            case ButtonShape.Circle:
                {
                    this.DrawCircularButton( pevent.Graphics );
                    break;
                }

            case ButtonShape.RoundRect:
                {
                    this.DrawRoundRectangularButton( pevent.Graphics );
                    break;
                }

            default:
                break;
        }
    }

    #endregion
}
/// <summary> Values that represent button shapes. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum ButtonShape
{
    /// <summary> An enum constant representing the round Rectangle option. </summary>
    RoundRect,

    /// <summary> An enum constant representing the circle option. </summary>
    Circle
}
