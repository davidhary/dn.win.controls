using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A zope menu strip. </summary>
/// <remarks>
/// (c) 2017 Pritam ZOPE, All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-03-08, 3.1.6276 </para><para>
/// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
/// </para>
/// </remarks>
public class ZopeMenuStrip : MenuStrip
{
    /// <summary>
    /// Initializes a new instance of the <see cref="MenuStrip" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public ZopeMenuStrip() => this.Renderer = new ZopeDarkMenuRenderer();
}
/// <summary> Zope Dark Menu renderer. </summary>
/// <remarks>
/// (c) 2017 Pritam ZOPE, All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-03-08, 3.1.6276 </para><para>
/// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
/// </para>
/// </remarks>
public class ZopeDarkMenuRenderer : ToolStripRenderer
{
    /// <summary>
    /// Raises the <see cref="ToolStripRenderer.RenderMenuItemBackground" />
    /// event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="ToolStripItemRenderEventArgs" /> that
    /// contains the event data. </param>
    protected override void OnRenderMenuItemBackground( ToolStripItemRenderEventArgs e )
    {
        if ( e is null ) return;

        base.OnRenderMenuItemBackground( e );
        if ( e.Item.Enabled )
        {
            if ( !e.Item.IsOnDropDown && e.Item.Selected )
            {
                Rectangle rect = new( 0, 0, e.Item.Width - 1, e.Item.Height - 1 );
                Rectangle rect2 = new( 0, 0, e.Item.Width - 1, e.Item.Height - 1 );
                using ( SolidBrush b = new( Color.FromArgb( 60, 60, 60 ) ) )
                {
                    e.Graphics.FillRectangle( b, rect );
                }

                using ( SolidBrush b = new( Color.Black ) )
                {
                    using Pen p = new( b );
                    e.Graphics.DrawRectangle( p, rect2 );
                }

                e.Item.ForeColor = Color.White;
            }
            else
            {
                e.Item.ForeColor = Color.White;
            }

            if ( e.Item.IsOnDropDown && e.Item.Selected )
            {
                Rectangle rect = new( 0, 0, e.Item.Width - 1, e.Item.Height - 1 );
                using ( SolidBrush b = new( Color.FromArgb( 60, 60, 60 ) ) )
                {
                    e.Graphics.FillRectangle( b, rect );
                }

                using ( SolidBrush b = new( Color.Black ) )
                {
                    using Pen p = new( b );
                    e.Graphics.DrawRectangle( p, rect );
                }

                e.Item.ForeColor = Color.White;
            }

            if ( (e.Item as ToolStripMenuItem)!.DropDown.Visible && !e.Item.IsOnDropDown )
            {
                Rectangle rect = new( 0, 0, e.Item.Width - 1, e.Item.Height - 1 );
                Rectangle rect2 = new( 0, 0, e.Item.Width - 1, e.Item.Height - 1 );
                using ( SolidBrush b = new( Color.FromArgb( 20, 20, 20 ) ) )
                {
                    e.Graphics.FillRectangle( b, rect );
                }

                using ( SolidBrush b = new( Color.Black ) )
                {
                    using Pen p = new( b );
                    e.Graphics.DrawRectangle( p, rect2 );
                }

                e.Item.ForeColor = Color.White;
            }
        }
    }

    /// <summary>
    /// Raises the <see cref="ToolStripRenderer.RenderSeparator" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="ToolStripSeparatorRenderEventArgs" />
    /// that contains the event data. </param>
    protected override void OnRenderSeparator( ToolStripSeparatorRenderEventArgs e )
    {
        if ( e is null ) return;

        base.OnRenderSeparator( e );
        Rectangle rect = new( 30, 3, e.Item.Width - 30, 1 );
        using SolidBrush darkLine = new( Color.FromArgb( 30, 30, 30 ) );
        e.Graphics.FillRectangle( darkLine, rect );
    }

    /// <summary>
    /// Raises the <see cref="ToolStripRenderer.RenderItemCheck" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="ToolStripItemImageRenderEventArgs" />
    /// that contains the event data. </param>
    protected override void OnRenderItemCheck( ToolStripItemImageRenderEventArgs e )
    {
        if ( e is null ) return;

        base.OnRenderItemCheck( e );
        if ( e.Item.Selected )
        {
            Rectangle rect = new( 4, 2, 18, 18 );
            Rectangle rect2 = new( 5, 3, 16, 16 );
            using ( SolidBrush b = new( Color.Black ) )
            {
                e.Graphics.FillRectangle( b, rect );
            }

            using ( SolidBrush b2 = new( Color.FromArgb( 220, 220, 220 ) ) )
            {
                e.Graphics.FillRectangle( b2, rect2 );
            }

            if ( e.Image is not null )
                e.Graphics.DrawImage( e.Image, new Point( 5, 3 ) );
        }
        else
        {
            Rectangle rect = new( 4, 2, 18, 18 );
            Rectangle rect2 = new( 5, 3, 16, 16 );
            using ( SolidBrush b = new( Color.White ) )
            {
                e.Graphics.FillRectangle( b, rect );
            }

            using ( SolidBrush b2 = new( Color.FromArgb( 255, 80, 90, 90 ) ) )
            {
                e.Graphics.FillRectangle( b2, rect2 );
            }

            if ( e.Image is not null )
                e.Graphics.DrawImage( e.Image, new Point( 5, 3 ) );
        }
    }

    /// <summary> Draws the item background. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="ToolStripRenderEventArgs" /> that
    /// contains the event data. </param>
    protected override void OnRenderImageMargin( ToolStripRenderEventArgs e )
    {
        if ( e is null ) return;

        base.OnRenderImageMargin( e );
        Rectangle rect = new( 0, 0, e.ToolStrip.Width, e.ToolStrip.Height );
        Rectangle rect3 = new( 0, 0, 26, e.AffectedBounds.Height );
        using ( SolidBrush darkLine = new( Color.FromArgb( 20, 20, 20 ) ) )
        {
            e.Graphics.FillRectangle( darkLine, rect );
            e.Graphics.FillRectangle( darkLine, rect3 );
        }

        using ( SolidBrush b = new( Color.FromArgb( 20, 20, 20 ) ) )
        {
            using Pen p = new( b );
            e.Graphics.DrawLine( p, 28, 0, 28, e.AffectedBounds.Height );
        }

        Rectangle rect2 = new( 0, 0, e.ToolStrip.Width - 1, e.ToolStrip.Height - 1 );
        using ( SolidBrush b = new( Color.Black ) )
        {
            using Pen p = new( b );
            e.Graphics.DrawRectangle( p, rect2 );
        }
    }
}
