using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A zope label. </summary>
/// <remarks>
/// (c) 2017 Pritam ZOPE, All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-03-08, 3.1.6276 </para><para>
/// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
/// </para>
/// </remarks>
public class ZopeLabel : Label
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Label" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public ZopeLabel() : base()
    {
        this.ForeColor = Color.Transparent;
        this.Font = new Font( SystemFonts.DefaultFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0 );
    }

    /// <summary> The display text. </summary>
    private string _displayText = "ZopeLabel";

    /// <summary> Gets or sets the display text. </summary>
    /// <value> The display text. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string DisplayText
    {
        get => this._displayText;
        set
        {
            this._displayText = value;
            this.Invalidate();
        }
    }

    /// <summary> The start color. </summary>
    private Color _startColor = Color.LightGreen;

    /// <summary> Gets or sets the color of the start. </summary>
    /// <value> The color of the start. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color StartColor
    {
        get => this._startColor;
        set
        {
            this._startColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The end color. </summary>
    private Color _endColor = Color.DarkBlue;

    /// <summary> Gets or sets the color of the end. </summary>
    /// <value> The color of the end. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color EndColor
    {
        get => this._endColor;
        set
        {
            this._endColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The start opacity. </summary>
    private int _startOpacity = 150;

    /// <summary> Gets or sets the start opacity. </summary>
    /// <value> The start opacity. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int StartOpacity
    {
        get => this._startOpacity;
        set
        {
            this._startOpacity = value;
            if ( this._startOpacity > 255 )
            {
                this._startOpacity = 255;
                this.Invalidate();
            }
            else
            {
                this.Invalidate();
            }
        }
    }

    /// <summary> The end opacity. </summary>
    private int _endOpacity = 150;

    /// <summary> Gets or sets the end opacity. </summary>
    /// <value> The end opacity. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int EndOpacity
    {
        get => this._endOpacity;
        set
        {
            this._endOpacity = value;
            if ( this._endOpacity > 255 )
            {
                this._endOpacity = 255;
                this.Invalidate();
            }
            else
            {
                this.Invalidate();
            }
        }
    }

    /// <summary> The gradient angle. </summary>
    private int _gradientAngle = 90;

    /// <summary> Gets or sets the gradient angle. </summary>
    /// <value> The gradient angle. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int GradientAngle
    {
        get => this._gradientAngle;
        set
        {
            this._gradientAngle = value;
            this.Invalidate();
        }
    }

    /// <summary> Raises the <see cref="Control.Paint" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs e )
    {
        if ( e is null ) return;

        base.OnPaint( e );
        this._displayText = this.DisplayText;
        Color c1 = Color.FromArgb( this.StartOpacity, this.StartColor );
        Color c2 = Color.FromArgb( this.EndOpacity, this.EndColor );
        using Brush b = new System.Drawing.Drawing2D.LinearGradientBrush( new Rectangle( 0, 0, 50, 50 ), c1, c2, this.GradientAngle );
        e.Graphics.DrawString( this.DisplayText, this.Font, b, new Point( 0, 0 ) );
    }
}
