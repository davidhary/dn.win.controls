using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A zope radio button. </summary>
/// <remarks>
/// (c) 2017 Pritam ZOPE, All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-03-08, 3.1.6276 </para><para>
/// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
/// </para>
/// </remarks>
public class ZopeRadioButton : RadioButton
{
    /// <summary>
    /// Initializes a new instance of the <see cref="RadioButton" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public ZopeRadioButton()
    {
        this.ForeColor = Color.White;
        this.Font = new Font( SystemFonts.DefaultFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0 );
    }

    /// <summary> The display text. </summary>
    private string _displayText = string.Empty;

    /// <summary> Gets or sets the display text. </summary>
    /// <value> The display text. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string DisplayText
    {
        get => this._displayText;
        set
        {
            this._displayText = value;
            this.Invalidate();
        }
    }

    /// <summary> The start color. </summary>
    private Color _startColor = Color.SteelBlue;

    /// <summary> Gets or sets the color of the start. </summary>
    /// <value> The color of the start. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color StartColor
    {
        get => this._startColor;
        set
        {
            this._startColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The end color. </summary>
    private Color _endColor = Color.DarkBlue;

    /// <summary> Gets or sets the color of the end. </summary>
    /// <value> The color of the end. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color EndColor
    {
        get => this._endColor;
        set
        {
            this._endColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The mouse hover start color. </summary>
    private Color _mouseHoverStartColor = Color.Yellow;

    /// <summary> Gets or sets the color of the mouse hover start. </summary>
    /// <value> The color of the mouse hover start. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color MouseHoverStartColor
    {
        get => this._mouseHoverStartColor;
        set
        {
            this._mouseHoverStartColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The mouse hover end color. </summary>
    private Color _mouseHoverEndColor = Color.DarkOrange;

    /// <summary> Gets or sets the color of the mouse hover end. </summary>
    /// <value> The color of the mouse hover end. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color MouseHoverEndColor
    {
        get => this._mouseHoverEndColor;
        set
        {
            this._mouseHoverEndColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The start opacity. </summary>
    private int _startOpacity = 150;

    /// <summary> Gets or sets the start opacity. </summary>
    /// <value> The start opacity. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int StartOpacity
    {
        get => this._startOpacity;
        set
        {
            this._startOpacity = value;
            if ( this._startOpacity > 255 )
            {
                this._startOpacity = 255;
                this.Invalidate();
            }
            else
            {
                this.Invalidate();
            }
        }
    }

    /// <summary> The end opacity. </summary>
    private int _endOpacity = 150;

    /// <summary> Gets or sets the end opacity. </summary>
    /// <value> The end opacity. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int EndOpacity
    {
        get => this._endOpacity;
        set
        {
            this._endOpacity = value;
            if ( this._endOpacity > 255 )
            {
                this._endOpacity = 255;
                this.Invalidate();
            }
            else
            {
                this.Invalidate();
            }
        }
    }

    /// <summary> The gradient angle. </summary>
    private int _gradientAngle = 90;

    /// <summary> Gets or sets the gradient angle. </summary>
    /// <value> The gradient angle. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int GradientAngle
    {
        get => this._gradientAngle;
        set
        {
            this._gradientAngle = value;
            this.Invalidate();
        }
    }

    /// <summary> The text location. </summary>
    private Point _textLocation = new( 4, 5 );

    /// <summary> Gets or sets the text location. </summary>
    /// <value> The text location. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Point TextLocation
    {
        get => this._textLocation;
        set
        {
            this._textLocation = value;
            this.Invalidate();
        }
    }

    /// <summary> Size of the box. </summary>
    private Size _boxSize = new( 18, 18 );

    /// <summary> Gets or sets the box size. </summary>
    /// <value> The size of the box. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Size BoxSize
    {
        get => this._boxSize;
        set
        {
            this._boxSize = value;
            this.Invalidate();
        }
    }

    /// <summary> The box location. </summary>
    private Point _boxLocation = new( 0, 0 );

    /// <summary> Gets or sets the box location. </summary>
    /// <value> The box location. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Point BoxLocation
    {
        get => this._boxLocation;
        set
        {
            this._boxLocation = value;
            this.Invalidate();
        }
    }

    /// <summary> The cached start color. </summary>
    private Color _cachedStartColor, _cachedEndColor;

    /// <summary>
    /// Raises the <see cref="Control.OnMouseEnter(EventArgs)" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="eventargs"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnMouseEnter( EventArgs eventargs )
    {
        if ( eventargs is null ) return;

        base.OnMouseEnter( eventargs );
        this._cachedStartColor = this._startColor;
        this._cachedEndColor = this._endColor;
        this._startColor = this._mouseHoverStartColor;
        this._endColor = this._mouseHoverEndColor;
    }

    /// <summary>
    /// Raises the <see cref="Control.OnMouseLeave(EventArgs)" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="eventargs"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnMouseLeave( EventArgs eventargs )
    {
        if ( eventargs is null ) return;

        base.OnMouseLeave( eventargs );
        this._startColor = this._cachedStartColor;
        this._endColor = this._cachedEndColor;
    }

    /// <summary>
    /// Raises the
    /// <see cref="ButtonBase.OnPaint(PaintEventArgs)" />
    /// event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="pevent"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs pevent )
    {
        if ( pevent is null ) return;

        base.OnPaint( pevent );
        this.AutoSize = false;
        // _displayText = _displayText;
        Color c1 = Color.FromArgb( this._startOpacity, this._startColor );
        Color c2 = Color.FromArgb( this._endOpacity, this._endColor );
        using ( Brush b = new System.Drawing.Drawing2D.LinearGradientBrush( this.ClientRectangle, c1, c2, this._gradientAngle ) )
        {
            pevent.Graphics.FillRectangle( b, this.ClientRectangle );
        }

        using ( SolidBrush forwardColor = new( this.ForeColor ) )
        {
            pevent.Graphics.DrawString( this._displayText, this.Font, forwardColor, this.TextLocation );
        }

        Rectangle rc = new( this.BoxLocation, this.BoxSize );
        ControlPaint.DrawRadioButton( pevent.Graphics, rc, this.Checked ? ButtonState.Checked : ButtonState.Normal );
    }
}
