using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A Zope tab control. </summary>
/// <remarks>
/// (c) 2017 Pritam ZOPE, All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-03-08, 3.1.6276 </para><para>
/// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
/// </para>
/// </remarks>
public class ZopeTabControl : TabControl
{
    /// <summary>
    /// Initializes a new instance of the <see cref="TabControl" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public ZopeTabControl() : base()
    {
        this.DrawMode = TabDrawMode.OwnerDrawFixed;
        this.Padding = new Point( 22, 4 );
        this.Font = new Font( SystemFonts.DefaultFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0 );
    }

    /// <summary> The active tab start color. </summary>

    private Color _activeTabStartColor = Color.Yellow;

    /// <summary> Gets or sets the active tab start color. </summary>
    /// <value> The color of the active tab start. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color ActiveTabStartColor
    {
        get => this._activeTabStartColor;
        set
        {
            this._activeTabStartColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The active tab end color. </summary>

    private Color _activeTabEndColor = Color.DarkOrange;

    /// <summary> Gets or sets the active tab end color. </summary>
    /// <value> The color of the active tab end. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color ActiveTabEndColor
    {
        get => this._activeTabEndColor;
        set
        {
            this._activeTabEndColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The inactive tab start color. </summary>

    private Color _inactiveTabStartColor = Color.LightGreen;

    /// <summary> Gets or sets the color of the inactive tab start. </summary>
    /// <value> The color of the inactive tab start. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color InactiveTabStartColor
    {
        get => this._inactiveTabStartColor;
        set
        {
            this._inactiveTabStartColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The inactive tab end color. </summary>
    private Color _inactiveTabEndColor = Color.DarkBlue;

    /// <summary> Gets or sets the color of the inactive tab end. </summary>
    /// <value> The color of the inactive tab end. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color InactiveTabEndColor
    {
        get => this._inactiveTabEndColor;
        set
        {
            this._inactiveTabEndColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The start opacity. </summary>
    private int _startOpacity = 150;

    /// <summary> Gets or sets the start opacity. </summary>
    /// <value> The start opacity. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int StartOpacity
    {
        get => this._startOpacity;
        set
        {
            this._startOpacity = value;
            if ( this._startOpacity > 255 )
            {
                this._startOpacity = 255;
                this.Invalidate();
            }
            else
            {
                this.Invalidate();
            }
        }
    }

    /// <summary> The end opacity. </summary>

    private int _endOpacity = 150;

    /// <summary> Gets or sets the end opacity. </summary>
    /// <value> The end opacity. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int EndOpacity
    {
        get => this._endOpacity;
        set
        {
            this._endOpacity = value;
            if ( this._endOpacity > 255 )
            {
                this._endOpacity = 255;
                this.Invalidate();
            }
            else
            {
                this.Invalidate();
            }
        }
    }

    /// <summary> The gradient angle. </summary>
    private int _gradientAngle = 90;

    /// <summary> Gets or sets the gradient angle. </summary>
    /// <value> The gradient angle. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int GradientAngle
    {
        get => this._gradientAngle;
        set
        {
            this._gradientAngle = value;
            this.Invalidate();
        }
    }

    /// <summary> The text color. </summary>
    private Color _textColor = Color.Navy;

    /// <summary> Gets or sets the color of the text. </summary>
    /// <value> The color of the text. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color TextColor
    {
        get => this._textColor;
        set
        {
            this._textColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The close button color. </summary>
    private Color _closeButtonColor = Color.Red;

    /// <summary> Gets or sets the color of the close button. </summary>
    /// <value> The color of the close button. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color CloseButtonColor
    {
        get => this._closeButtonColor;
        set
        {
            this._closeButtonColor = value;
            this.Invalidate();
        }
    }

    /// <summary> Raises the <see cref="Control.Paint" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs e )
    {
        base.OnPaint( e );
    }

    /// <summary>
    /// Raises the <see cref="TabControl.DrawItem" /> event.
    /// Draws tab items.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="DrawItemEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnDrawItem( DrawItemEventArgs e )
    {
        if ( e is null ) return;

        base.OnDrawItem( e );
        Rectangle rc = this.GetTabRect( e.Index );

        // if tab is selected
        if ( string.Equals( this.SelectedTab?.Text, this.TabPages[e.Index].Text, StringComparison.Ordinal ) )
        {
            Color c1 = Color.FromArgb( this._startOpacity, this._activeTabStartColor );
            Color c2 = Color.FromArgb( this._endOpacity, this._activeTabEndColor );
            using LinearGradientBrush br = new( rc, c1, c2, this._gradientAngle );
            e.Graphics.FillRectangle( br, rc );
        }
        else
        {
            Color c1 = Color.FromArgb( this._startOpacity, this._inactiveTabStartColor );
            Color c2 = Color.FromArgb( this._endOpacity, this._inactiveTabEndColor );
            using LinearGradientBrush br = new( rc, c1, c2, this._gradientAngle );
            e.Graphics.FillRectangle( br, rc );
        }

        this.TabPages[e.Index].BorderStyle = BorderStyle.FixedSingle;
        this.TabPages[e.Index].ForeColor = SystemColors.ControlText;

        // draw close button on tabs

        Rectangle paddedBounds = e.Bounds;
        paddedBounds.Inflate( -5, -4 );
        using ( SolidBrush b = new( this._textColor ) )
        {
            e.Graphics.DrawString( this.TabPages[e.Index].Text, this.Font, b, paddedBounds );
        }

        Point pad = this.Padding;

        // drawing close button to tab items
        using ( SolidBrush b = new( this._closeButtonColor ) )
        {
            using Font f = new( this.Font.FontFamily, 10f, FontStyle.Bold );
            e.Graphics.DrawString( "X", f, b, e.Bounds.Right + 1 - 18, e.Bounds.Top + pad.Y - 2 );
        }

        e.DrawFocusRectangle();
    }

    /// <summary> Raises the <see cref="Control.MouseDown" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseDown( MouseEventArgs e )
    {
        if ( e is null ) return;

        base.OnMouseDown( e );
        int i = 0;
        while ( i < this.TabPages.Count )
        {
            Rectangle r = this.GetTabRect( i );
            Rectangle closeButton = new( r.Right + 1 - 15, r.Top + 4, 12, 12 );
            if ( closeButton.Contains( e.Location ) )
            {
                if ( this.IsTabClosing() )
                {
                    this.TabPages.RemoveAt( i );
                }

                break;
            }

            i += 1;
        }
    }

    /// <summary> Event queue for all listeners interested in TabClosing events. </summary>
    public event EventHandler<System.ComponentModel.CancelEventArgs>? TabClosing;

    /// <summary> Raises the system. component model. cancel event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    protected void OnTabClosing( System.ComponentModel.CancelEventArgs e )
    {
        this.TabClosing?.Invoke( this, e );
    }

    /// <summary> Query if this object is tab closing. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> True if tab closing, false if not. </returns>
    private bool IsTabClosing()
    {
        System.ComponentModel.CancelEventArgs cancelArgs = new();
        this.OnTabClosing( cancelArgs );
        return !cancelArgs.Cancel;
    }
}
