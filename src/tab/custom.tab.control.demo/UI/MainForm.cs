namespace cc.isr.WinControls.Demo;

/// <summary> The application's main window. </summary>
/// <remarks> David, 2021-03-12. </remarks>
public partial class MainForm : Form
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Form" /> class.
    /// </summary>
    /// <remarks> David, 2021-03-12. </remarks>
    public MainForm() => this.InitializeComponent();
}
