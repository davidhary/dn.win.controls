using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A tab style chrome provider. </summary>
/// <remarks>
/// (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-09-26 </para><para>
/// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
/// </remarks>
/// <remarks> Specialized constructor for use only by derived class. </remarks>
/// <remarks> David, 2020-09-24. </remarks>
/// <param name="tabControl"> The tab control. </param>
[System.ComponentModel.ToolboxItem( false )]
public class TabStyleChromeProvider( CustomTabControl tabControl ) : TabStyleProvider( tabControl, 16, 16, true, Color.White, new Point( 7, 5 ) )
{
    /// <summary> Adds a tab border to 'tabBounds'. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="path">      Full pathname of the file. </param>
    /// <param name="tabBounds"> The tab bounds. </param>
    public override void AddTabBorder( GraphicsPath path, Rectangle tabBounds )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( path, nameof( path ) );
#else
        if ( path is null ) throw new ArgumentNullException( nameof( path ) );
#endif

        int spread;
        int eight;
        int sixth;
        int quarter;
        if ( this.TabControl.Alignment <= TabAlignment.Bottom )
        {
            spread = ( int ) Math.Truncate( Math.Floor( tabBounds.Height * 2m / 3m ) );
            eight = ( int ) Math.Truncate( Math.Floor( tabBounds.Height * 1m / 8m ) );
            sixth = ( int ) Math.Truncate( Math.Floor( tabBounds.Height * 1m / 6m ) );
            quarter = ( int ) Math.Truncate( Math.Floor( tabBounds.Height * 1m / 4m ) );
        }
        else
        {
            spread = ( int ) Math.Truncate( Math.Floor( tabBounds.Width * 2m / 3m ) );
            eight = ( int ) Math.Truncate( Math.Floor( tabBounds.Width * 1m / 8m ) );
            sixth = ( int ) Math.Truncate( Math.Floor( tabBounds.Width * 1m / 6m ) );
            quarter = ( int ) Math.Truncate( Math.Floor( tabBounds.Width * 1m / 4m ) );
        }

        switch ( this.TabControl.Alignment )
        {
            case TabAlignment.Top:
                {
                    path.AddCurve( [new( tabBounds.X, tabBounds.Bottom ), new( tabBounds.X + sixth, tabBounds.Bottom - eight ), new( tabBounds.X + spread - quarter, tabBounds.Y + eight ), new( tabBounds.X + spread, tabBounds.Y )] );
                    path.AddLine( tabBounds.X + spread, tabBounds.Y, tabBounds.Right - spread, tabBounds.Y );
                    path.AddCurve( [new( tabBounds.Right - spread, tabBounds.Y ), new( tabBounds.Right - spread + quarter, tabBounds.Y + eight ), new( tabBounds.Right - sixth, tabBounds.Bottom - eight ), new( tabBounds.Right, tabBounds.Bottom )] );
                    break;
                }

            case TabAlignment.Bottom:
                {
                    path.AddCurve( [new( tabBounds.Right, tabBounds.Y ), new( tabBounds.Right - sixth, tabBounds.Y + eight ), new( tabBounds.Right - spread + quarter, tabBounds.Bottom - eight ), new( tabBounds.Right - spread, tabBounds.Bottom )] );
                    path.AddLine( tabBounds.Right - spread, tabBounds.Bottom, tabBounds.X + spread, tabBounds.Bottom );
                    path.AddCurve( [new( tabBounds.X + spread, tabBounds.Bottom ), new( tabBounds.X + spread - quarter, tabBounds.Bottom - eight ), new( tabBounds.X + sixth, tabBounds.Y + eight ), new( tabBounds.X, tabBounds.Y )] );
                    break;
                }

            case TabAlignment.Left:
                {
                    path.AddCurve( [new( tabBounds.Right, tabBounds.Bottom ), new( tabBounds.Right - eight, tabBounds.Bottom - sixth ), new( tabBounds.X + eight, tabBounds.Bottom - spread + quarter ), new( tabBounds.X, tabBounds.Bottom - spread )] );
                    path.AddLine( tabBounds.X, tabBounds.Bottom - spread, tabBounds.X, tabBounds.Y + spread );
                    path.AddCurve( [new( tabBounds.X, tabBounds.Y + spread ), new( tabBounds.X + eight, tabBounds.Y + spread - quarter ), new( tabBounds.Right - eight, tabBounds.Y + sixth ), new( tabBounds.Right, tabBounds.Y )] );
                    break;
                }

            case TabAlignment.Right:
                {
                    path.AddCurve( [new( tabBounds.X, tabBounds.Y ), new( tabBounds.X + eight, tabBounds.Y + sixth ), new( tabBounds.Right - eight, tabBounds.Y + spread - quarter ), new( tabBounds.Right, tabBounds.Y + spread )] );
                    path.AddLine( tabBounds.Right, tabBounds.Y + spread, tabBounds.Right, tabBounds.Bottom - spread );
                    path.AddCurve( [new( tabBounds.Right, tabBounds.Bottom - spread ), new( tabBounds.Right - eight, tabBounds.Bottom - spread + quarter ), new( tabBounds.X + eight, tabBounds.Bottom - sixth ), new( tabBounds.X, tabBounds.Bottom )] );
                    break;
                }

            default:
                break;
        }
    }

    /// <summary> Draw tab closer. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="index">    Zero-based index of the. </param>
    /// <param name="graphics"> The graphics. </param>
    protected override void DrawTabCloser( int index, Graphics graphics )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( graphics, nameof( graphics ) );
#else
        if ( graphics is null ) throw new ArgumentNullException( nameof( graphics ) );
#endif

        if ( this.ShowTabCloser )
        {
            Rectangle closerRect = this.TabControl.GetTabCloserRect( index );
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            if ( closerRect.Contains( this.TabControl.MousePosition ) )
            {
                using ( GraphicsPath closerPath = GetCloserButtonPath( closerRect ) )
                {
                    using SolidBrush closerBrush = new( Color.FromArgb( 193, 53, 53 ) );
                    graphics.FillPath( closerBrush, closerPath );
                }

                using ( GraphicsPath closerPath = GetCloserPath( closerRect ) )
                {
                    using Pen closerPen = new( this.CloserColorActive );
                    graphics.DrawPath( closerPen, closerPath );
                }
            }
            else
            {
                using GraphicsPath closerPath = GetCloserPath( closerRect );
                using Pen closerPen = new( this.CloserColor );
                graphics.DrawPath( closerPen, closerPath );
            }
        }
    }

    /// <summary> Gets closer button path. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="closerRect"> The closer rectangle. </param>
    /// <returns> The closer button path. </returns>
    private static GraphicsPath GetCloserButtonPath( Rectangle closerRect )
    {
        GraphicsPath closerPath = new();
        closerPath.AddEllipse( new Rectangle( closerRect.X - 2, closerRect.Y - 2, closerRect.Width + 4, closerRect.Height + 4 ) );
        closerPath.CloseFigure();
        return closerPath;
    }
}
