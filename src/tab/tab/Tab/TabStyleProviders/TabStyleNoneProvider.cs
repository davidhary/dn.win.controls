using System.Drawing;

namespace cc.isr.WinControls;

/// <summary> A tab style none provider. </summary>
/// <remarks>
/// (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-09-26 </para><para>
/// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
/// </remarks>
/// <remarks> Specialized constructor for use only by derived class. </remarks>
/// <remarks> David, 2020-09-24. </remarks>
/// <param name="tabControl"> The tab control. </param>
[System.ComponentModel.ToolboxItem( false )]
public class TabStyleNoneProvider( CustomTabControl tabControl ) : TabStyleProvider( tabControl )
{
    /// <summary> Adds a tab border to 'tabBounds'. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="path">      Full pathname of the file. </param>
    /// <param name="tabBounds"> The tab bounds. </param>
    public override void AddTabBorder( System.Drawing.Drawing2D.GraphicsPath path, Rectangle tabBounds )
    {
    }
}
