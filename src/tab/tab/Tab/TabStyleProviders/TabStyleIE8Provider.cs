using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A tab style IE 8 provider. </summary>
/// <remarks>
/// (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-09-26 </para><para>
/// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
/// </remarks>
/// <remarks> Specialized constructor for use only by derived class. </remarks>
/// <remarks> David, 2020-09-24. </remarks>
/// <param name="tabControl"> The tab control. </param>
[System.ComponentModel.ToolboxItem( false )]
public class TabStyleIE8Provider( CustomTabControl tabControl ) : TabStyleRoundedProvider( tabControl, 3, true, Color.Red, new Point( 6, 5 ) )
{
    /// <summary> Gets tab rectangle. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="index"> Zero-based index of the. </param>
    /// <returns> The tab rectangle. </returns>
    public override Rectangle GetTabRect( int index )
    {
        if ( index < 0 )
        {
            return new Rectangle();
        }

        Rectangle tabBounds = base.GetTabRect( index );
        bool firstTabInRow = this.TabControl.IsFirstTabInRow( index );

        // Make non-SelectedTabs smaller and selected tab bigger
        if ( index != this.TabControl.SelectedIndex )
        {
            switch ( this.TabControl.Alignment )
            {
                case TabAlignment.Top:
                    {
                        tabBounds.Y += 1;
                        tabBounds.Height -= 1;
                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        tabBounds.Height -= 1;
                        break;
                    }

                case TabAlignment.Left:
                    {
                        tabBounds.X += 1;
                        tabBounds.Width -= 1;
                        break;
                    }

                case TabAlignment.Right:
                    {
                        tabBounds.Width -= 1;
                        break;
                    }

                default:
                    break;
            }
        }
        else
        {
            switch ( this.TabControl.Alignment )
            {
                case TabAlignment.Top:
                    {
                        tabBounds.Y -= 1;
                        tabBounds.Height += 1;
                        if ( firstTabInRow )
                        {
                            tabBounds.Width += 1;
                        }
                        else
                        {
                            tabBounds.X -= 1;
                            tabBounds.Width += 2;
                        }

                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        tabBounds.Height += 1;
                        if ( firstTabInRow )
                        {
                            tabBounds.Width += 1;
                        }
                        else
                        {
                            tabBounds.X -= 1;
                            tabBounds.Width += 2;
                        }

                        break;
                    }

                case TabAlignment.Left:
                    {
                        tabBounds.X -= 1;
                        tabBounds.Width += 1;
                        if ( firstTabInRow )
                        {
                            tabBounds.Height += 1;
                        }
                        else
                        {
                            tabBounds.Y -= 1;
                            tabBounds.Height += 2;
                        }

                        break;
                    }

                case TabAlignment.Right:
                    {
                        tabBounds.Width += 1;
                        if ( firstTabInRow )
                        {
                            tabBounds.Height += 1;
                        }
                        else
                        {
                            tabBounds.Y -= 1;
                            tabBounds.Height += 2;
                        }

                        break;
                    }

                default:
                    break;
            }
        }

        // Adjust first tab in the row to align with tab page
        this.EnsureFirstTabIsInView( ref tabBounds, index );
        return tabBounds;
    }

    /// <summary> Gets tab background brush. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="index"> Zero-based index of the. </param>
    /// <returns> The tab background brush. </returns>
    protected override Brush GetTabBackgroundBrush( int index )
    {
        // Capture the colors dependent on selection state of the tab
        Color dark = Color.FromArgb( 227, 238, 251 );
        Color light = Color.FromArgb( 227, 238, 251 );
        if ( this.TabControl.SelectedIndex == index )
        {
            dark = Color.FromArgb( 196, 222, 251 );
            light = SystemColors.Window;
        }
        else if ( !this.TabControl.TabPages[index].Enabled )
        {
            light = dark;
        }
        else if ( this.HotTrack && index == this.TabControl.ActiveIndex )
        {
            // Enable hot tracking
            light = SystemColors.Window;
            dark = Color.FromArgb( 166, 203, 248 );
        }

        // Get the correctly aligned gradient
        Rectangle tabBounds = this.GetTabRect( index );
        tabBounds.Inflate( 3, 3 );
        tabBounds.X -= 1;
        tabBounds.Y -= 1;
        LinearGradientBrush fillBrush = new( tabBounds, dark, light, LinearGradientMode.Vertical );
        switch ( this.TabControl.Alignment )
        {
            case TabAlignment.Top:
                {
                    fillBrush = new LinearGradientBrush( tabBounds, dark, light, LinearGradientMode.Vertical );
                    break;
                }

            case TabAlignment.Bottom:
                {
                    fillBrush = new LinearGradientBrush( tabBounds, light, dark, LinearGradientMode.Vertical );
                    break;
                }

            case TabAlignment.Left:
                {
                    fillBrush = new LinearGradientBrush( tabBounds, dark, light, LinearGradientMode.Horizontal );
                    break;
                }

            case TabAlignment.Right:
                {
                    fillBrush = new LinearGradientBrush( tabBounds, light, dark, LinearGradientMode.Horizontal );
                    break;
                }

            default:
                break;
        }

        // Add the blend
        fillBrush.Blend = this.GetBackgroundBlend( index );
        return fillBrush;
    }

    /// <summary> Gets background blend. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="index"> Zero-based index of the. </param>
    /// <returns> The background blend. </returns>
    private Blend GetBackgroundBlend( int index )
    {
        float[] relativeIntensities = [0f, 0.7f, 1.0f];
        float[] relativePositions = [0f, 0.8f, 1.0f];
        if ( this.TabControl.SelectedIndex != index )
        {
            relativeIntensities = [0f, 0.3f, 1.0f];
            relativePositions = [0f, 0.2f, 1.0f];
        }

        Blend blend = new() { Factors = relativeIntensities, Positions = relativePositions };
        return blend;
    }

    /// <summary> Draw tab closer. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="index">    Zero-based index of the. </param>
    /// <param name="graphics"> The graphics. </param>
    protected override void DrawTabCloser( int index, Graphics graphics )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( graphics, nameof( graphics ) );
#else
        if ( graphics is null ) throw new ArgumentNullException( nameof( graphics ) );
#endif

        if ( this.ShowTabCloser )
        {
            Rectangle closerRect = this.TabControl.GetTabCloserRect( index );
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            if ( closerRect.Contains( this.TabControl.MousePosition ) )
            {
                using ( GraphicsPath closerPath = GetCloserButtonPath( closerRect ) )
                {
                    graphics.FillPath( Brushes.White, closerPath );
                    using Pen closerPen = new( this.BorderColor );
                    graphics.DrawPath( closerPen, closerPath );
                }

                using ( GraphicsPath closerPath = GetCloserPath( closerRect ) )
                {
                    using Pen closerPen = new( this.CloserColorActive )
                    {
                        Width = 2f
                    };
                    graphics.DrawPath( closerPen, closerPath );
                }
            }
            else
            {
                using GraphicsPath closerPath = GetCloserPath( closerRect );
                using Pen closerPen = new( this.CloserColor )
                {
                    Width = 2f
                };
                graphics.DrawPath( closerPen, closerPath );
            }
        }
    }

    /// <summary> Gets closer button path. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="closerRect"> The closer rectangle. </param>
    /// <returns> The closer button path. </returns>
    private static GraphicsPath GetCloserButtonPath( Rectangle closerRect )
    {
        GraphicsPath closerPath = new();
        closerPath.AddLine( closerRect.X - 1, closerRect.Y - 2, closerRect.Right + 1, closerRect.Y - 2 );
        closerPath.AddLine( closerRect.Right + 2, closerRect.Y - 1, closerRect.Right + 2, closerRect.Bottom + 1 );
        closerPath.AddLine( closerRect.Right + 1, closerRect.Bottom + 2, closerRect.X - 1, closerRect.Bottom + 2 );
        closerPath.AddLine( closerRect.X - 2, closerRect.Bottom + 1, closerRect.X - 2, closerRect.Y - 1 );
        closerPath.CloseFigure();
        return closerPath;
    }

    /// <summary> Gets page background brush. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="index"> Zero-based index of the. </param>
    /// <returns> The page background brush. </returns>
    public override Brush GetPageBackgroundBrush( int index )
    {
        // Capture the colors dependent on selection state of the tab
        Color light = Color.FromArgb( 227, 238, 251 );
        if ( this.TabControl.SelectedIndex == index )
        {
            light = SystemColors.Window;
        }
        else if ( !this.TabControl.TabPages[index].Enabled )
        {
            light = Color.FromArgb( 207, 207, 207 );
        }
        else if ( this.HotTrack && index == this.TabControl.ActiveIndex )
        {
            // Enable hot tracking
            light = Color.FromArgb( 234, 246, 253 );
        }

        return new SolidBrush( light );
    }
}
