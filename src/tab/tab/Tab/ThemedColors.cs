using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace cc.isr.WinControls;

/// <summary> A themed colors. </summary>
/// <remarks> (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-09-26,Created.
/// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para></remarks>
internal sealed class ThemedColors
{
    #region "   variables and constants "

    /// <summary> The normal color. </summary>
    private const string NORMAL_COLOR = "NormalColor";

    /// <summary> The home stead. </summary>
    private const string HOME_STEAD = "HomeStead";

    /// <summary> The metallic. </summary>
    private const string METALLIC = "Metallic";

    // Private Constant NoTheme As String = "NoTheme"

    #endregion

    #region "   properties "

    /// <summary> Gets the current theme index. </summary>
    /// <value> The current theme index. </value>
    public static ColorScheme CurrentThemeIndex => GetCurrentThemeIndex();

    /// <summary> The tool border. </summary>
    private static readonly Color[] _toolBorderInternal = [Color.FromArgb( 127, 157, 185 ), Color.FromArgb( 164, 185, 127 ), Color.FromArgb( 165, 172, 178 ), Color.FromArgb( 132, 130, 132 )];

    /// <summary> Gets the tool border. </summary>
    /// <value> The tool border. </value>
    public static Color ToolBorder => _toolBorderInternal[( int ) CurrentThemeIndex];

    #endregion

    #region "   constructors "

    /// <summary> Initializes a new instance of the <see cref="object" /> class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private ThemedColors()
    {
    }

    #endregion

    /// <summary> Gets the zero-based index of the current theme. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The current theme index. </returns>
    private static ColorScheme GetCurrentThemeIndex()
    {
        ColorScheme theme = ColorScheme.NoTheme;
        if ( VisualStyleInformation.IsSupportedByOS && VisualStyleInformation.IsEnabledByUser && Application.RenderWithVisualStyles )
        {
            switch ( VisualStyleInformation.ColorScheme ?? "" )
            {
                case NORMAL_COLOR:
                    {
                        theme = ColorScheme.NormalColor;
                        break;
                    }

                case HOME_STEAD:
                    {
                        theme = ColorScheme.HomeStead;
                        break;
                    }

                case METALLIC:
                    {
                        theme = ColorScheme.Metallic;
                        break;
                    }

                default:
                    {
                        theme = ColorScheme.NoTheme;
                        break;
                    }
            }
        }

        return theme;
    }

    /// <summary> Values that represent color schemes. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum ColorScheme
    {
        /// <summary> An enum constant representing the normal color option. </summary>
        NormalColor = 0,

        /// <summary> An enum constant representing the home stead option. </summary>
        HomeStead = 1,

        /// <summary> An enum constant representing the metallic option. </summary>
        Metallic = 2,

        /// <summary> An enum constant representing the no theme option. </summary>
        NoTheme = 3
    }
}
