using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary>   A native methods. This class cannot be inherited. </summary>
/// <remarks>   David, 2021-03-17. </remarks>
internal sealed partial class NativeMethods
{
    #region " send message "

    /// <summary> Sends a message. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="hWnd">   The window. </param>
    /// <param name="msg">    The message. </param>
    /// <param name="wParam"> The parameter. </param>
    /// <param name="lParam"> The parameter. </param>
    /// <returns> An IntPtr. </returns>
    public static IntPtr SendMessage( IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam )
    {
        // This Method replaces the User32 method SendMessage, but will only work for sending
        // messages to Managed controls.
        Control? control__1 = Control.FromHandle( hWnd );
        if ( control__1 is null )
        {
            return IntPtr.Zero;
        }

        Message message = new() { HWnd = hWnd, LParam = lParam, WParam = wParam, Msg = msg };

        MethodInfo? methodInfo = control__1.GetType().GetMethod( "WndProc",
            BindingFlags.NonPublic | BindingFlags.InvokeMethod | BindingFlags.FlattenHierarchy | BindingFlags.IgnoreCase | BindingFlags.Instance );
        object[] args = [message];
        _ = methodInfo?.Invoke( control__1, args );
        return (( Message ) args[0]).Result;
    }

    #endregion

    #region " misc functions "

    /// <summary> Lower word. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="dWord"> The word. </param>
    /// <returns> An Integer. </returns>
    public static int LoWord( IntPtr dWord )
    {
        return dWord.ToInt32() & 0xFFFF;
    }

    /// <summary> Higher word. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="dWord"> The word. </param>
    /// <returns> An Integer. </returns>
    public static int HiWord( IntPtr dWord )
    {
        return (( ulong ) dWord.ToInt32() & 0x80000000UL) == 0x80000000UL ? dWord.ToInt32() >> 16 : (dWord.ToInt32() >> 16) & 0xFFFF;
    }

    /// <summary> Converts a Structure Object to an int pointer. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="structureObject"> The structure object. </param>
    /// <returns> Structure Object as an IntPtr. </returns>
    public static IntPtr ToIntPtr( object structureObject )
    {
        IntPtr pointParam = Marshal.AllocCoTaskMem( Marshal.SizeOf( structureObject ) );
        Marshal.StructureToPtr( structureObject, pointParam, false );
        return pointParam;
    }

    #endregion
}
