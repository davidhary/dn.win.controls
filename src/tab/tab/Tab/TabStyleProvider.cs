using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A tab style provider. </summary>
/// <remarks>
/// (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-09-26 </para><para>
/// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
/// </remarks>
[ToolboxItem( false )]
public abstract class TabStyleProvider : Component
{
    #region " constructor"

    /// <summary> Specialized constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="tabControl">          The tab control. </param>
    /// <param name="imageAlignment">      The image alignment. </param>
    /// <param name="borderColor">         The border color. </param>
    /// <param name="borderColorSelected"> The border color selected. </param>
    /// <param name="borderColorHot">      The border color hot. </param>
    /// <param name="focusColor">          The focus color. </param>
    /// <param name="textColor">           The text color. </param>
    /// <param name="textColorDisabled">   The text color disabled. </param>
    /// <param name="hotTrack">            True to hot track. </param>
    /// <param name="padding">             The padding. </param>
    /// <param name="radius">              The radius. </param>
    /// <param name="showTabCloser">       True to show, false to hide the tab closer. </param>
    /// <param name="closerColorActive">   The closer color active. </param>
    /// <param name="closerColor">         The closer color. </param>
    protected TabStyleProvider( CustomTabControl tabControl, ContentAlignment imageAlignment, Color borderColor, Color borderColorSelected, Color borderColorHot, Color focusColor, Color textColor, Color textColorDisabled, bool hotTrack, Point padding, int radius, bool showTabCloser, Color closerColorActive, Color closerColor ) : base()
    {
        this.TabControl = tabControl;
        this._borderColor = borderColor;
        this._borderColorSelected = borderColorSelected;
        this._borderColorHot = borderColorHot;
        this._focusColor = focusColor;
        this._imageAlign = imageAlignment;
        this._hotTrack = hotTrack;
        this._radius = radius;
        this._showTabCloser = showTabCloser;
        this._closerColorActive = closerColorActive;
        this._closerColor = closerColor;
        this._textColor = textColor;
        this._textColorDisabled = textColorDisabled;
        this._opacity = 1f;
        this._padding = padding;
        this.AdjustPadding( padding );
    }

    /// <summary> Specialized constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="tabControl">          The tab control. </param>
    /// <param name="borderColor">         The border color. </param>
    /// <param name="borderColorSelected"> The border color selected. </param>
    /// <param name="borderColorHot">      The border color hot. </param>
    /// <param name="focusColor">          The focus color. </param>
    /// <param name="textColor">           The text color. </param>
    /// <param name="textColorDisabled">   The text color disabled. </param>
    /// <param name="hotTrack">            True to hot track. </param>
    /// <param name="padding">             The padding. </param>
    /// <param name="radius">              The radius. </param>
    /// <param name="showTabCloser">       True to show, false to hide the tab closer. </param>
    /// <param name="closerColorActive">   The closer color active. </param>
    /// <param name="closerColor">         The closer color. </param>
    protected TabStyleProvider( CustomTabControl tabControl, Color borderColor, Color borderColorSelected, Color borderColorHot, Color focusColor, Color textColor, Color textColorDisabled, bool hotTrack, Point padding, int radius, bool showTabCloser, Color closerColorActive, Color closerColor ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft, borderColor, borderColorSelected, borderColorHot, focusColor, textColor, textColorDisabled, hotTrack, padding, radius, showTabCloser, closerColorActive, closerColor )
    {
    }

    /// <summary> Specialized constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="tabControl">        The tab control. </param>
    /// <param name="borderColor">       The border color. </param>
    /// <param name="borderColorHot">    The border color hot. </param>
    /// <param name="textColor">         The text color. </param>
    /// <param name="textColorDisabled"> The text color disabled. </param>
    /// <param name="padding">           The padding. </param>
    /// <param name="radius">            The radius. </param>
    /// <param name="showTabCloser">     True to show, false to hide the tab closer. </param>
    /// <param name="closerColorActive"> The closer color active. </param>
    /// <param name="closerColor">       The closer color. </param>
    protected TabStyleProvider( CustomTabControl tabControl, Color borderColor, Color borderColorHot, Color textColor, Color textColorDisabled, Point padding, int radius, bool showTabCloser, Color closerColorActive, Color closerColor ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft, borderColor, Color.Empty, borderColorHot, Color.Orange, textColor, textColorDisabled, true, padding, radius, showTabCloser, closerColorActive, closerColor )
    {
    }

    /// <summary> Specialized constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="tabControl">     The tab control. </param>
    /// <param name="imageAlignment"> The image alignment. </param>
    protected TabStyleProvider( CustomTabControl tabControl, ContentAlignment imageAlignment ) : base()
    {
        this.TabControl = tabControl;
        this._borderColor = Color.Empty;
        this._borderColorSelected = Color.Empty;
        this._borderColorHot = Color.Empty;
        this._focusColor = Color.Orange;
        this._imageAlign = imageAlignment;
        this._hotTrack = true;
        this._radius = 1;
        this._closerColorActive = Color.Black;
        this._closerColor = Color.DarkGray;
        this._textColor = Color.Empty;
        this._textColorDisabled = Color.Empty;
        this._opacity = 1f;
        this._padding = new Point( 6, 3 );
        this.AdjustPadding( this.Padding );
    }

    /// <summary> Specialized constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="tabControl"> The tab control. </param>
    protected TabStyleProvider( CustomTabControl tabControl ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft )
    {
    }

    /// <summary> Specialized constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="tabControl"> The tab control. </param>
    /// <param name="radius">     The radius. </param>
    /// <param name="focusTrack"> True to focus track. </param>
    protected TabStyleProvider( CustomTabControl tabControl, int radius, bool focusTrack ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft )
    {
        this._radius = radius;
        this._focusTrack = focusTrack;
        this._padding = new Point( 6, 3 );
        this.AdjustPadding( this.Padding );
    }

    /// <summary> Specialized constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="tabControl"> The tab control. </param>
    /// <param name="radius">     The radius. </param>
    protected TabStyleProvider( CustomTabControl tabControl, int radius ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft )
    {
        this._radius = radius;
        this._padding = new Point( 6, 3 );
        this.AdjustPadding( this.Padding );
    }

    /// <summary> Specialized constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="tabControl">        The tab control. </param>
    /// <param name="radius">            The radius. </param>
    /// <param name="showTabCloser">     True to show, false to hide the tab closer. </param>
    /// <param name="closerColorActive"> The closer color active. </param>
    /// <param name="padding">           The padding. </param>
    protected TabStyleProvider( CustomTabControl tabControl, int radius, bool showTabCloser, Color closerColorActive, Point padding ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft )
    {
        this._showTabCloser = showTabCloser;
        this._closerColorActive = closerColorActive;
        this._radius = radius;
        this._padding = padding;
        this.AdjustPadding( padding );
    }

    /// <summary> Specialized constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="tabControl">        The tab control. </param>
    /// <param name="radius">            The radius. </param>
    /// <param name="overlap">           The overlap. </param>
    /// <param name="showTabCloser">     True to show, false to hide the tab closer. </param>
    /// <param name="closerColorActive"> The closer color active. </param>
    /// <param name="padding">           The padding. </param>
    protected TabStyleProvider( CustomTabControl tabControl, int radius, int overlap, bool showTabCloser, Color closerColorActive, Point padding ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft )
    {
        this._showTabCloser = showTabCloser;
        this._closerColorActive = closerColorActive;
        this._overlap = overlap;
        this._radius = radius;
        this._padding = padding;
        this.AdjustPadding( padding );
    }

    /// <summary> Specialized constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="tabControl">     The tab control. </param>
    /// <param name="imageAlignment"> The image alignment. </param>
    /// <param name="radius">         The radius. </param>
    /// <param name="overlap">        The overlap. </param>
    /// <param name="padding">        The padding. </param>
    protected TabStyleProvider( CustomTabControl tabControl, ContentAlignment imageAlignment, int radius, int overlap, Point padding ) : this( tabControl, imageAlignment )
    {
        this._overlap = overlap;
        this._radius = radius;
        this._padding = padding;
        this.AdjustPadding( padding );
    }

    #endregion

    #region " factory methods"

    /// <summary> Creates a provider. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="tabControl"> The tab control. </param>
    /// <returns> The new provider. </returns>
    public static TabStyleProvider CreateProvider( CustomTabControl tabControl )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( tabControl, nameof( tabControl ) );
#else
        if ( tabControl is null ) throw new ArgumentNullException( nameof( tabControl ) );
#endif

        TabStyleProvider provider;

        // Depending on the display style of the tabControl generate an appropriate provider.
        switch ( tabControl.DisplayStyle )
        {
            case TabStyle.None:
                {
                    provider = new TabStyleNoneProvider( tabControl );
                    break;
                }

            case TabStyle.Default:
                {
                    provider = new TabStyleDefaultProvider( tabControl );
                    break;
                }

            case TabStyle.Angled:
                {
                    provider = new TabStyleAngledProvider( tabControl );
                    break;
                }

            case TabStyle.Rounded:
                {
                    provider = new TabStyleRoundedProvider( tabControl );
                    break;
                }

            case TabStyle.VisualStudio:
                {
                    provider = new TabStyleVisualStudioProvider( tabControl );
                    break;
                }

            case TabStyle.Chrome:
                {
                    provider = new TabStyleChromeProvider( tabControl );
                    break;
                }

            case TabStyle.IE8:
                {
                    provider = new TabStyleIE8Provider( tabControl );
                    break;
                }

            case TabStyle.VS2010:
                {
                    provider = new TabStyleVS2010Provider( tabControl );
                    break;
                }

            default:
                {
                    provider = new TabStyleDefaultProvider( tabControl );
                    break;
                }
        }

        provider.DisplayStyle = tabControl.DisplayStyle;
        return provider;
    }

    #endregion

    #region " overridable methods"

    /// <summary> Adds a tab border to 'tabBounds'. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="path">      Full pathname of the file. </param>
    /// <param name="tabBounds"> The tab bounds. </param>
    public abstract void AddTabBorder( GraphicsPath path, Rectangle tabBounds );

    /// <summary> Gets tab rectangle. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="index"> Zero-based index of the. </param>
    /// <returns> The tab rectangle. </returns>
    public virtual Rectangle GetTabRect( int index )
    {
        if ( index < 0 )
        {
            return new Rectangle();
        }

        Rectangle tabBounds = this.TabControl.GetTabRect( index );
        if ( this.TabControl.RightToLeftLayout )
        {
            tabBounds.X = this.TabControl.Width - tabBounds.Right;
        }

        bool firstTabInRow = this.TabControl.IsFirstTabInRow( index );

        // Expand to overlap the tab page
        switch ( this.TabControl.Alignment )
        {
            case TabAlignment.Top:
                {
                    tabBounds.Height += 2;
                    break;
                }

            case TabAlignment.Bottom:
                {
                    tabBounds.Height += 2;
                    tabBounds.Y -= 2;
                    break;
                }

            case TabAlignment.Left:
                {
                    tabBounds.Width += 2;
                    break;
                }

            case TabAlignment.Right:
                {
                    tabBounds.X -= 2;
                    tabBounds.Width += 2;
                    break;
                }

            default:
                break;
        }


        // Create Overlap unless first tab in the row to align with tab page
        if ( (!firstTabInRow || this.TabControl.RightToLeftLayout) && this.Overlap > 0 )
        {
            if ( this.TabControl.Alignment <= TabAlignment.Bottom )
            {
                tabBounds.X -= this.Overlap;
                tabBounds.Width += this.Overlap;
            }
            else
            {
                tabBounds.Y -= this.Overlap;
                tabBounds.Height += this.Overlap;
            }
        }

        // Adjust first tab in the row to align with tab page
        this.EnsureFirstTabIsInView( ref tabBounds, index );
        return tabBounds;
    }

    /// <summary> Ensures that first tab is in view. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="tabBounds"> [in,out] The tab bounds. </param>
    /// <param name="index">     Zero-based index of the. </param>
    protected virtual void EnsureFirstTabIsInView( ref Rectangle tabBounds, int index )
    {
        // Adjust first tab in the row to align with tab page
        // Make sure we only reposition visible tabs, as we may have scrolled out of view.

        bool firstTabInRow = this.TabControl.IsFirstTabInRow( index );
        if ( firstTabInRow )
        {
            if ( this.TabControl.Alignment <= TabAlignment.Bottom )
            {
                if ( this.TabControl.RightToLeftLayout )
                {
                    if ( tabBounds.Left < this.TabControl.Right )
                    {
                        int tabPageRight = this.TabControl.GetPageBounds( index ).Right;
                        if ( tabBounds.Right > tabPageRight )
                        {
                            tabBounds.Width -= tabBounds.Right - tabPageRight;
                        }
                    }
                }
                else if ( tabBounds.Right > 0 )
                {
                    int tabPageX = this.TabControl.GetPageBounds( index ).X;
                    if ( tabBounds.X < tabPageX )
                    {
                        tabBounds.Width -= tabPageX - tabBounds.X;
                        tabBounds.X = tabPageX;
                    }
                }
            }
            else if ( this.TabControl.RightToLeftLayout )
            {
                if ( tabBounds.Top < this.TabControl.Bottom )
                {
                    int tabPageBottom = this.TabControl.GetPageBounds( index ).Bottom;
                    if ( tabBounds.Bottom > tabPageBottom )
                    {
                        tabBounds.Height -= tabBounds.Bottom - tabPageBottom;
                    }
                }
            }
            else if ( tabBounds.Bottom > 0 )
            {
                int tabPageY = this.TabControl.GetPageBounds( index ).Location.Y;
                if ( tabBounds.Y < tabPageY )
                {
                    tabBounds.Height -= tabPageY - tabBounds.Y;
                    tabBounds.Y = tabPageY;
                }
            }
        }
    }

    /// <summary> Gets tab background brush. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="index"> Zero-based index of the. </param>
    /// <returns> The tab background brush. </returns>
    protected virtual Brush GetTabBackgroundBrush( int index )
    {
        // Capture the colors dependent on selection state of the tab
        Color dark = Color.FromArgb( 207, 207, 207 );
        Color light = Color.FromArgb( 242, 242, 242 );
        if ( this.TabControl.SelectedIndex == index )
        {
            dark = SystemColors.ControlLight;
            light = SystemColors.Window;
        }
        else if ( !this.TabControl.TabPages[index].Enabled )
        {
            light = dark;
        }
        else if ( this.HotTrack && index == this.TabControl.ActiveIndex )
        {
            // Enable hot tracking
            light = Color.FromArgb( 234, 246, 253 );
            dark = Color.FromArgb( 167, 217, 245 );
        }

        // Get the correctly aligned gradient
        Rectangle tabBounds = this.GetTabRect( index );
        tabBounds.Inflate( 3, 3 );
        tabBounds.X -= 1;
        tabBounds.Y -= 1;
        LinearGradientBrush fillBrush = new( tabBounds, light, dark, LinearGradientMode.Vertical );
        switch ( this.TabControl.Alignment )
        {
            case TabAlignment.Top:
                {
                    if ( this.TabControl.SelectedIndex == index )
                    {
                        dark = light;
                    }

                    fillBrush = new( tabBounds, light, dark, LinearGradientMode.Vertical );
                    break;
                }

            case TabAlignment.Bottom:
                {
                    fillBrush = new( tabBounds, light, dark, LinearGradientMode.Vertical );
                    break;
                }

            case TabAlignment.Left:
                {
                    fillBrush = new( tabBounds, dark, light, LinearGradientMode.Horizontal );
                    break;
                }

            case TabAlignment.Right:
                {
                    fillBrush = new LinearGradientBrush( tabBounds, light, dark, LinearGradientMode.Horizontal );
                    break;
                }

            default:
                break;
        }

        // Add the blend
        fillBrush.Blend = this.GetBackgroundBlend();
        return fillBrush;
    }

    #endregion

    #region " base properties"

    /// <summary> Gets or sets the tab control. </summary>
    /// <value> The tab control. </value>
    protected CustomTabControl TabControl { get; set; }

    /// <summary> Gets or sets the display style. </summary>
    /// <value> The display style. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public TabStyle DisplayStyle { get; set; }

    /// <summary> The image align. </summary>
    private ContentAlignment _imageAlign;

    /// <summary> Gets or sets the image align. </summary>
    /// <value> The image align. </value>
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ContentAlignment ImageAlign
    {
        get => this._imageAlign;
        set
        {
            this._imageAlign = value;
            this.TabControl.Invalidate();
        }
    }

    /// <summary> The padding. </summary>
    private Point _padding;

    /// <summary> Adjust padding. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    private void AdjustPadding( Point value )
    {
        // This line will trigger the handle to recreate, therefore invalidating the control
        (( TabControl ) this.TabControl).Padding = this.ShowTabCloser ? value.X + (this.Radius / 2) < -6 ? new Point( 0, value.Y ) : new Point( value.X + (this.Radius / 2) + 6, value.Y ) : value.X + (this.Radius / 2) < 1 ? new Point( 0, value.Y ) : new Point( value.X + (this.Radius / 2) - 1, value.Y );
    }

    /// <summary> Gets or sets the padding. </summary>
    /// <value> The padding. </value>
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Point Padding
    {
        get => this._padding;
        set
        {
            this._padding = value;
            this.AdjustPadding( value );
        }
    }

    /// <summary> The radius. </summary>

    private int _radius;

    /// <summary> Gets or sets the radius. </summary>
    /// <value> The radius. </value>
    [Category( "Appearance" )]
    [DefaultValue( 1 )]
    [Browsable( true )]
    public int Radius
    {
        get => this._radius;
        set
        {
            if ( value < 1 )
            {
                value = 1;
            }

            this._radius = value;
            this.AdjustPadding( this._padding );
        }
    }

    /// <summary> The overlap. </summary>
    private int _overlap;

    /// <summary> Gets or sets the overlap. </summary>
    /// <value> The overlap. </value>
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int Overlap
    {
        get => this._overlap;
        set
        {
            if ( value < 0 )
            {
                value = 0;
            }

            this._overlap = value;
        }
    }

    /// <summary> True to focus track. </summary>
    private bool _focusTrack;

    /// <summary> Gets or sets the focus track. </summary>
    /// <value> The focus track. </value>
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool FocusTrack
    {
        get => this._focusTrack;
        set
        {
            this._focusTrack = value;
            this.TabControl.Invalidate();
        }
    }

    /// <summary> True to hot track. </summary>
    private bool _hotTrack;

    /// <summary> Gets or sets the hot track. </summary>
    /// <value> The hot track. </value>
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool HotTrack
    {
        get => this._hotTrack;
        set
        {
            this._hotTrack = value;
            (( TabControl ) this.TabControl).HotTrack = value;
        }
    }

    /// <summary> True to show, false to hide the tab closer. </summary>
    private bool _showTabCloser;

    /// <summary> Gets or sets the show tab closer. </summary>
    /// <value> The show tab closer. </value>
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool ShowTabCloser
    {
        get => this._showTabCloser;
        set
        {
            this._showTabCloser = value;
            this.AdjustPadding( this._padding );
        }
    }

    /// <summary> The opacity. </summary>
    private float _opacity = 1f;

    /// <summary> Gets or sets the opacity. </summary>
    /// <value> The opacity. </value>
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public float Opacity
    {
        get => this._opacity;
        set
        {
            if ( value < 0f )
            {
                value = 0f;
            }
            else if ( value > 1f )
            {
                value = 1f;
            }

            this._opacity = value;
            this.TabControl.Invalidate();
        }
    }

    /// <summary> The border color selected. </summary>
    private Color _borderColorSelected;

    /// <summary> Gets or sets the border color selected. </summary>
    /// <value> The border color selected. </value>
    [Category( "Appearance" )]
    [DefaultValue( typeof( Color ), "" )]
    public Color BorderColorSelected
    {
        get => this._borderColorSelected.IsEmpty ? ThemedColors.ToolBorder : this._borderColorSelected;
        set
        {
            this._borderColorSelected = value.Equals( ThemedColors.ToolBorder ) ? Color.Empty : value;
            this.TabControl.Invalidate();
        }
    }

    /// <summary> The border color hot. </summary>
    private Color _borderColorHot;

    /// <summary> Gets or sets the border color hot. </summary>
    /// <value> The border color hot. </value>
    [Category( "Appearance" )]
    [DefaultValue( typeof( Color ), "" )]
    public Color BorderColorHot
    {
        get => this._borderColorHot.IsEmpty ? SystemColors.ControlDark : this._borderColorHot;
        set
        {
            this._borderColorHot = value.Equals( SystemColors.ControlDark ) ? Color.Empty : value;
            this.TabControl.Invalidate();
        }
    }

    /// <summary> The border color. </summary>
    private Color _borderColor;

    /// <summary> Gets or sets the color of the border. </summary>
    /// <value> The color of the border. </value>
    [Category( "Appearance" )]
    [DefaultValue( typeof( Color ), "" )]
    public Color BorderColor
    {
        get => this._borderColor.IsEmpty ? SystemColors.ControlDark : this._borderColor;
        set
        {
            this._borderColor = value.Equals( SystemColors.ControlDark ) ? Color.Empty : value;
            this.TabControl.Invalidate();
        }
    }

    /// <summary> The text color. </summary>
    private Color _textColor;

    /// <summary> Gets or sets the color of the text. </summary>
    /// <value> The color of the text. </value>
    [Category( "Appearance" )]
    [DefaultValue( typeof( Color ), "" )]
    public Color TextColor
    {
        get => this._textColor.IsEmpty ? SystemColors.ControlText : this._textColor;
        set
        {
            this._textColor = value.Equals( SystemColors.ControlText ) ? Color.Empty : value;
            this.TabControl.Invalidate();
        }
    }

    /// <summary> The text color selected. </summary>
    private Color _textColorSelected = Color.Empty;

    /// <summary> Gets or sets the text color selected. </summary>
    /// <value> The text color selected. </value>
    [Category( "Appearance" )]
    [DefaultValue( typeof( Color ), "" )]
    public Color TextColorSelected
    {
        get => this._textColorSelected.IsEmpty ? SystemColors.ControlText : this._textColorSelected;
        set
        {
            this._textColorSelected = value.Equals( SystemColors.ControlText ) ? Color.Empty : value;
            this.TabControl.Invalidate();
        }
    }

    /// <summary> The text color disabled. </summary>
    private Color _textColorDisabled;

    /// <summary> Gets or sets the text color disabled. </summary>
    /// <value> The text color disabled. </value>
    [Category( "Appearance" )]
    [DefaultValue( typeof( Color ), "" )]
    public Color TextColorDisabled
    {
        get => this._textColorDisabled.IsEmpty ? SystemColors.ControlDark : this._textColorDisabled;
        set
        {
            this._textColorDisabled = value.Equals( SystemColors.ControlDark ) ? Color.Empty : value;
            this.TabControl.Invalidate();
        }
    }

    /// <summary> The focus color. </summary>
    private Color _focusColor;

    /// <summary> Gets or sets the color of the focus. </summary>
    /// <value> The color of the focus. </value>
    [Category( "Appearance" )]
    [DefaultValue( typeof( Color ), "Orange" )]
    public Color FocusColor
    {
        get => this._focusColor;
        set
        {
            this._focusColor = value;
            this.TabControl.Invalidate();
        }
    }

    /// <summary> The closer color active. </summary>
    private Color _closerColorActive;

    /// <summary> Gets or sets the closer color active. </summary>
    /// <value> The closer color active. </value>
    [Category( "Appearance" )]
    [DefaultValue( typeof( Color ), "Black" )]
    public Color CloserColorActive
    {
        get => this._closerColorActive;
        set
        {
            this._closerColorActive = value;
            this.TabControl.Invalidate();
        }
    }

    /// <summary> The closer color. </summary>
    private Color _closerColor;

    /// <summary> Gets or sets the color of the closer. </summary>
    /// <value> The color of the closer. </value>
    [Category( "Appearance" )]
    [DefaultValue( typeof( Color ), "DarkGrey" )]
    public Color CloserColor
    {
        get => this._closerColor;
        set
        {
            this._closerColor = value;
            this.TabControl.Invalidate();
        }
    }

    #endregion

    #region " painting"

    /// <summary> Paints the tab. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="index">    Zero-based index of the. </param>
    /// <param name="graphics"> The graphics. </param>
    public void PaintTab( int index, Graphics graphics )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( graphics, nameof( graphics ) );
#else
        if ( graphics is null ) throw new ArgumentNullException( nameof( graphics ) );
#endif

        using GraphicsPath tabGraphicsPath = this.GetTabBorder( index );
        using Brush fillBrush = this.GetTabBackgroundBrush( index );
        // Paint the background
        graphics.FillPath( fillBrush, tabGraphicsPath );
        // Paint a focus indication
        if ( this.TabControl.Focused )
        {
            this.DrawTabFocusIndicator( tabGraphicsPath, index, graphics );
        }
        // Paint the closer
        this.DrawTabCloser( index, graphics );
    }

    /// <summary> Draw tab closer. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="index">    Zero-based index of the. </param>
    /// <param name="graphics"> The graphics. </param>
    protected virtual void DrawTabCloser( int index, Graphics graphics )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( graphics, nameof( graphics ) );
#else
        if ( graphics is null ) throw new ArgumentNullException( nameof( graphics ) );
#endif

        if ( this.ShowTabCloser )
        {
            Rectangle closerRect = this.TabControl.GetTabCloserRect( index );
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            using GraphicsPath closerPath = GetCloserPath( closerRect );
            if ( closerRect.Contains( this.TabControl.MousePosition ) )
            {
                using Pen closerPen = new( this.CloserColorActive );
                graphics.DrawPath( closerPen, closerPath );
            }
            else
            {
                using Pen closerPen = new( this.CloserColor );
                graphics.DrawPath( closerPen, closerPath );
            }
        }
    }

    /// <summary> Gets closer path. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="closerRect"> The closer rectangle. </param>
    /// <returns> The closer path. </returns>
    protected static GraphicsPath GetCloserPath( Rectangle closerRect )
    {
        GraphicsPath closerPath = new();
        closerPath.AddLine( closerRect.X, closerRect.Y, closerRect.Right, closerRect.Bottom );
        closerPath.CloseFigure();
        closerPath.AddLine( closerRect.Right, closerRect.Y, closerRect.X, closerRect.Bottom );
        closerPath.CloseFigure();
        return closerPath;
    }

    /// <summary> Draw tab focus indicator. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="tabPath">  The tab graphics path. </param>
    /// <param name="index">    Zero-based index of the. </param>
    /// <param name="graphics"> The graphics. </param>
    private void DrawTabFocusIndicator( GraphicsPath tabPath, int index, Graphics graphics )
    {
        if ( this.FocusTrack && this.TabControl.Focused && index == this.TabControl.SelectedIndex )
        {
            RectangleF pathRect = tabPath.GetBounds();
            Rectangle focusRect;
            switch ( this.TabControl.Alignment )
            {
                case TabAlignment.Top:
                    {
                        focusRect = new Rectangle( ( int ) Math.Truncate( pathRect.X ), ( int ) Math.Truncate( pathRect.Y ), ( int ) Math.Truncate( pathRect.Width ), 4 );
                        using Brush focusBrush = new LinearGradientBrush( focusRect, this.FocusColor, SystemColors.Window, LinearGradientMode.Vertical );
                        using Region focusRegion = new( focusRect );
                        focusRegion.Intersect( tabPath );
                        graphics.FillRegion( focusBrush, focusRegion );

                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        focusRect = new Rectangle( ( int ) Math.Truncate( pathRect.X ), ( int ) Math.Truncate( pathRect.Bottom ) - 4, ( int ) Math.Truncate( pathRect.Width ), 4 );
                        using Brush focusBrush = new LinearGradientBrush( focusRect, SystemColors.ControlLight, this.FocusColor, LinearGradientMode.Vertical );
                        using Region focusRegion = new( focusRect );
                        focusRegion.Intersect( tabPath );
                        graphics.FillRegion( focusBrush, focusRegion );

                        break;
                    }

                case TabAlignment.Left:
                    {
                        focusRect = new Rectangle( ( int ) Math.Truncate( pathRect.X ), ( int ) Math.Truncate( pathRect.Y ), 4, ( int ) Math.Truncate( pathRect.Height ) );
                        using Brush focusBrush = new LinearGradientBrush( focusRect, this.FocusColor, SystemColors.ControlLight, LinearGradientMode.Horizontal );
                        using Region focusRegion = new( focusRect );
                        focusRegion.Intersect( tabPath );
                        graphics.FillRegion( focusBrush, focusRegion );

                        break;
                    }

                case TabAlignment.Right:
                    {
                        focusRect = new Rectangle( ( int ) Math.Truncate( pathRect.Right ) - 4, ( int ) Math.Truncate( pathRect.Y ), 4, ( int ) Math.Truncate( pathRect.Height ) );
                        using Brush focusBrush = new LinearGradientBrush( focusRect, SystemColors.ControlLight, this.FocusColor, LinearGradientMode.Horizontal );
                        using Region focusRegion = new( focusRect );
                        focusRegion.Intersect( tabPath );
                        graphics.FillRegion( focusBrush, focusRegion );

                        break;
                    }

                default:
                    break;
            }
        }
    }

    #endregion

    #region " background brushes"

    /// <summary> Gets background blend. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The background blend. </returns>
    private Blend GetBackgroundBlend()
    {
        float[] relativeIntensities = [0f, 0.7f, 1.0f];
        float[] relativePositions = [0f, 0.6f, 1.0f];

        // Glass look to top aligned tabs
        if ( this.TabControl.Alignment == TabAlignment.Top )
        {
            relativeIntensities = [0f, 0.5f, 1.0f, 1.0f];
            relativePositions = [0f, 0.5f, 0.51f, 1.0f];
        }

        Blend blend = new() { Factors = relativeIntensities, Positions = relativePositions };
        return blend;
    }

    /// <summary> Gets page background brush. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="index"> Zero-based index of the. </param>
    /// <returns> The page background brush. </returns>
    public virtual Brush GetPageBackgroundBrush( int index )
    {
        // Capture the colors dependent on selection state of the tab
        Color light = Color.FromArgb( 242, 242, 242 );
        if ( this.TabControl.Alignment == TabAlignment.Top )
        {
            light = Color.FromArgb( 207, 207, 207 );
        }

        if ( this.TabControl.SelectedIndex == index )
        {
            light = SystemColors.Window;
        }
        else if ( !this.TabControl.TabPages[index].Enabled )
        {
            light = Color.FromArgb( 207, 207, 207 );
        }
        else if ( this.HotTrack && index == this.TabControl.ActiveIndex )
        {
            // Enable hot tracking
            light = Color.FromArgb( 234, 246, 253 );
        }

        return new SolidBrush( light );
    }

    #endregion

    #region " tab border and rectangle"

    /// <summary> Gets tab border. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="index"> Zero-based index of the. </param>
    /// <returns> The tab border. </returns>
    public GraphicsPath GetTabBorder( int index )
    {
        GraphicsPath path = new();
        Rectangle tabBounds = this.GetTabRect( index );
        this.AddTabBorder( path, tabBounds );
        path.CloseFigure();
        return path;
    }

    #endregion
}
/// <summary> Values that represent tab styles. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum TabStyle
{
    /// <summary> An enum constant representing the none option. </summary>
    None,

    /// <summary> An enum constant representing the default] option. </summary>
    Default,

    /// <summary> An enum constant representing the visual studio option. </summary>
    VisualStudio,

    /// <summary> An enum constant representing the rounded option. </summary>
    Rounded,

    /// <summary> An enum constant representing the angled option. </summary>
    Angled,

    /// <summary> An enum constant representing the chrome option. </summary>
    Chrome,

    /// <summary> An enum constant representing the IE 8 option. </summary>
    IE8,

    /// <summary> An enum constant representing the vs 2010 option. </summary>
    VS2010
}
