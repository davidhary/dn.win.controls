# About

cc.isr.WinControls.Metro is a .Net library supporting Metro like Windows controls.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

cc.isr.WinControls.Metro is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Win.Controls Repository].

[Win.Controls Repository]: https://bitbucket.org/davidhary/dn.win.controls

