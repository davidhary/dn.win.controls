using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;
public partial class MetroLoading
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    #region " component designer generated code"

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        var resources = new System.ComponentModel.ComponentResourceManager(typeof(MetroLoading));
        _pictureBox = new PictureBox();
        ((System.ComponentModel.ISupportInitialize)_pictureBox).BeginInit();
        SuspendLayout();
        // 
        // _pictureBox
        // 
        _pictureBox.BackColor = Color.Transparent;
        _pictureBox.Dock = DockStyle.Fill;
        _pictureBox.Image = (Image)resources.GetObject("_pictureBox.Image");
        _pictureBox.Location = new Point(0, 0);
        _pictureBox.Name = "_pictureBox";
        _pictureBox.Size = new Size(76, 76);
        _pictureBox.TabIndex = 0;
        _pictureBox.TabStop = false;
        // 
        // MetroLoading
        // 
        AutoScaleDimensions = new SizeF(6.0f, 13.0f);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.Transparent;
        Controls.Add(_pictureBox);
        Name = "MetroLoading";
        Size = new Size(76, 76);
        ((System.ComponentModel.ISupportInitialize)_pictureBox).EndInit();
        ResumeLayout(false);
    }

    #endregion

    private PictureBox _pictureBox;
}
