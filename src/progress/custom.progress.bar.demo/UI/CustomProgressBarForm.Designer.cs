namespace cc.isr.WinControls.Demo;

public partial class CustomProgressBarForm
{
    /// <summary>
/// Required designer variable.
/// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
/// Clean up any resources being used.
/// </summary>
/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if ( disposing )
        {
            components?.Dispose();
        }

        base.Dispose(disposing);
    }

    /// <summary>
    /// Required method for Designer support - do not modify the contents of this method with the
    /// code editor.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    private void InitializeComponent()
    {
        StandardProgressBar = new ProgressBar();
        FadeTrackBar = new TrackBar();
        FadeTrackBar.Scroll += new EventHandler(FadeTrackBar_Scroll);
        StandardProgressBarLabel = new Label();
        CustomProgressBarLabel = new Label();
        FadeTrackBarLabel = new Label();
        CustomProgressBar = new WinControls.CustomProgressBar();
        StatusStrip1 = new StatusStrip();
        ToolStripProgressBar = new ToolStripProgressBar();
        _statusStripCustomProgressBar = new WinControls.StatusStripCustomProgressBar();
        ((System.ComponentModel.ISupportInitialize)FadeTrackBar).BeginInit();
        StatusStrip1.SuspendLayout();
        SuspendLayout();
        // 
        // StandardProgressBar
        // 
        StandardProgressBar.Location = new Point(133, 24);
        StandardProgressBar.Name = "StandardProgressBar";
        StandardProgressBar.Size = new Size(321, 23);
        StandardProgressBar.TabIndex = 1;
        // 
        // FadeTrackBar
        // 
        FadeTrackBar.Location = new Point(125, 124);
        FadeTrackBar.Maximum = 200;
        FadeTrackBar.Minimum = 100;
        FadeTrackBar.Name = "FadeTrackBar";
        FadeTrackBar.Size = new Size(336, 45);
        FadeTrackBar.TabIndex = 5;
        FadeTrackBar.Value = 150;
        // 
        // StandardProgressBarLabel
        // 
        StandardProgressBarLabel.AutoSize = true;
        StandardProgressBarLabel.Location = new Point(19, 29);
        StandardProgressBarLabel.Name = "StandardProgressBarLabel";
        StandardProgressBarLabel.Size = new Size(110, 13);
        StandardProgressBarLabel.TabIndex = 0;
        StandardProgressBarLabel.Text = "Standard Progress Bar";
        // 
        // CustomProgressBarLabel
        // 
        CustomProgressBarLabel.AutoSize = true;
        CustomProgressBarLabel.Location = new Point(19, 76);
        CustomProgressBarLabel.Name = "CustomProgressBarLabel";
        CustomProgressBarLabel.Size = new Size(102, 13);
        CustomProgressBarLabel.TabIndex = 2;
        CustomProgressBarLabel.Text = "Custom Progress Bar";
        // 
        // FadeTrackBarLabel
        // 
        FadeTrackBarLabel.AutoSize = true;
        FadeTrackBarLabel.Location = new Point(19, 123);
        FadeTrackBarLabel.Name = "FadeTrackBarLabel";
        FadeTrackBarLabel.Size = new Size(85, 13);
        FadeTrackBarLabel.TabIndex = 4;
        FadeTrackBarLabel.Text = "Fade Alpha: 150";
        // 
        // CustomProgressBar
        // 
        CustomProgressBar.Font = new Font("Microsoft Sans Serif", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        CustomProgressBar.Location = new Point(133, 70);
        CustomProgressBar.Name = "CustomProgressBar";
        CustomProgressBar.Size = new Size(321, 23);
        CustomProgressBar.TabIndex = 3;
        CustomProgressBar.Text = "0 %";
        // 
        // StatusStrip1
        // 
        StatusStrip1.Items.AddRange(new ToolStripItem[] { ToolStripProgressBar, _statusStripCustomProgressBar });
        StatusStrip1.Location = new Point(0, 150);
        StatusStrip1.Name = "StatusStrip1";
        StatusStrip1.Size = new Size(484, 29);
        StatusStrip1.TabIndex = 6;
        StatusStrip1.Text = "Status Strip";
        // 
        // ToolStripProgressBar
        // 
        ToolStripProgressBar.Name = "ToolStripProgressBar";
        ToolStripProgressBar.Size = new Size(100, 23);
        // 
        // _statusStripCustomProgressBar
        // 
        _statusStripCustomProgressBar.ForeColor = SystemColors.ControlText;
        _statusStripCustomProgressBar.Margin = new Padding(1, 3, 1, 3);
        _statusStripCustomProgressBar.Name = "_StatusStripCustomProgressBar";
        _statusStripCustomProgressBar.Size = new Size(100, 23);
        _statusStripCustomProgressBar.Text = "0 %";
        // 
        // Form1
        // 
        AutoScaleDimensions = new SizeF(6.0f, 13.0f);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(484, 179);
        Controls.Add(StatusStrip1);
        Controls.Add(CustomProgressBar);
        Controls.Add(FadeTrackBarLabel);
        Controls.Add(CustomProgressBarLabel);
        Controls.Add(StandardProgressBarLabel);
        Controls.Add(FadeTrackBar);
        Controls.Add(StandardProgressBar);
        Name = "Form1";
        Text = "Custom Bar Demo";
        ((System.ComponentModel.ISupportInitialize)FadeTrackBar).EndInit();
        StatusStrip1.ResumeLayout(false);
        StatusStrip1.PerformLayout();
        ResumeLayout(false);
        PerformLayout();
    }

    private Label StandardProgressBarLabel;
    private ProgressBar StandardProgressBar;
    private Label CustomProgressBarLabel;
    private WinControls.CustomProgressBar CustomProgressBar;
    private Label FadeTrackBarLabel;
    private TrackBar FadeTrackBar;
    private StatusStrip StatusStrip1;
    private StatusStripCustomProgressBar _statusStripCustomProgressBar;
    private ToolStripProgressBar ToolStripProgressBar;
}
