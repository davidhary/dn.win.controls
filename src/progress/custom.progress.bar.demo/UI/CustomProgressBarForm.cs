namespace cc.isr.WinControls.Demo;
public partial class CustomProgressBarForm : Form
{
    public CustomProgressBarForm()
    {
        base.Load += this.Form1_Load;
        this.Shown += this.Form1_Shown;
        this.InitializeComponent();
    }

    private void FadeTrackBar_Scroll( object? sender, EventArgs e )
    {
        int fadeAlpha = this.FadeTrackBar.Value;
        this.CustomProgressBar.Fade = fadeAlpha;
        this._statusStripCustomProgressBar.Fade = fadeAlpha;
        this.FadeTrackBarLabel.Text = "Fade Alpha: " + fadeAlpha.ToString( System.Globalization.CultureInfo.CurrentCulture );
    }

    private void Form1_Load( object? sender, EventArgs e )
    {
    }

    private void Form1_Shown( object? sender, EventArgs e )
    {
        this.CustomProgressBar.Value = 75;
        this.StandardProgressBar.Value = 75;
        this._statusStripCustomProgressBar.Value = 75;
        this.ToolStripProgressBar.Value = 75;
    }
}
