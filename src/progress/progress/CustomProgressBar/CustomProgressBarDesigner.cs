using System;
using System.Collections;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Windows.Forms.Design.Behavior;

namespace cc.isr.WinControls;
internal class CustomProgressBarDesigner : System.Windows.Forms.Design.ControlDesigner
{
    /// <summary>
    /// Initializes a new instance of the
    /// <see cref="System.Windows.Forms.Design.ControlDesigner" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public CustomProgressBarDesigner()
    {
    }

    /// <summary>
    /// Gets a list of <see cref="System.Windows.Forms.Design.ControlDesigner.SnapLines" /> objects
    /// representing significant alignment points for this control.
    /// </summary>
    /// <remarks>
    /// A list of <see cref="System.Windows.Forms.Design.ControlDesigner.SnapLines" /> objects representing
    /// significant alignment points for this control.
    /// </remarks>
    [System.Diagnostics.CodeAnalysis.AllowNull]
    public override IList SnapLines
    {
        get
        {
            // Get the SnapLines collection from the base class
            ArrayList? snapList = base.SnapLines as ArrayList;

            // Calculate the Baseline for the Font used by the Control and add it to the SnapLines
            int textBaseline = GetBaseline( base.Control, ContentAlignment.MiddleCenter );
            if ( textBaseline > 0 )
                _ = snapList?.Add( new SnapLine( SnapLineType.Baseline, textBaseline, SnapLinePriority.Medium ) );
            return snapList ?? base.SnapLines;
        }
    }

    /// <summary> Gets a baseline. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="ctrl">      The control. </param>
    /// <param name="alignment"> The alignment. </param>
    /// <returns> The baseline. </returns>
    private static int GetBaseline( Control ctrl, ContentAlignment alignment )
    {
        int textAscent = 0;
        int textHeight = 0;

        // Retrieve the ClientRect of the Control
        Rectangle clientRect = ctrl.ClientRectangle;

        // Create a Graphics object for the Control
        using ( Graphics graphics = ctrl.CreateGraphics() )
        {
            // Retrieve the device context Handle
            IntPtr hDC = graphics.GetHdc();

            // Create a wrapper for the Font of the Control
            Font controlFont = ctrl.Font;
            HandleRef tempFontHandle = new( controlFont, controlFont.ToHfont() );
            try
            {
                // Create a wrapper for the device context
                HandleRef deviceContextHandle = new( ctrl, hDC );

                // Select the Font into the device context
                IntPtr originalFont = NativeMethods.SelectObject( deviceContextHandle, tempFontHandle );

                // Create a TEXTMETRIC and calculate metrics for the selected Font
                NativeMethods.TEXTMETRIC tEXTMETRIC = new();
                if ( NativeMethods.GetTextMetrics( deviceContextHandle, ref tEXTMETRIC ) != 0 )
                {
                    textAscent = tEXTMETRIC.tmAscent + 1;
                    textHeight = tEXTMETRIC.tmHeight;
                }

                // Restore original Font
                HandleRef originalFontHandle = new( ctrl, originalFont );
                _ = NativeMethods.SelectObject( deviceContextHandle, originalFontHandle );
            }
            finally
            {
                // Cleanup tempFont
                _ = NativeMethods.DeleteObject( tempFontHandle );

                // Release device context
                graphics.ReleaseHdc( hDC );
            }
        }

        // Calculate return value based on the specified alignment; first check top alignment
        if ( (alignment & (ContentAlignment.TopLeft | ContentAlignment.TopCenter | ContentAlignment.TopRight)) != 0 )
        {
            return clientRect.Top + textAscent;
        }

        // Check middle alignment
        if ( (alignment & (ContentAlignment.MiddleLeft | ContentAlignment.MiddleCenter | ContentAlignment.MiddleRight)) == 0 )
        {
            return clientRect.Bottom - textHeight + textAscent;
        }

        // Assume bottom alignment
        return ( int ) Math.Truncate( Math.Round( clientRect.Top + (clientRect.Height / 2d) - (textHeight / 2d) + textAscent ) );
    }
}
