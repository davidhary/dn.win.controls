using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A custom progress bar.  Represents a Windows progress bar control which displays
/// its <see cref="ProgressBar.Value" /> as text on a faded background.
/// </summary>
/// <remarks> David, 2020-09-24.
/// CustomProgressBar is a specialized type of <see cref="ProgressBar" />, which it extends
/// to fade its background colors and to display its <see cref="Text" />.
/// 
/// <para>You can manipulate the background fading intensity by changing the value of
/// property <see cref="Fade" /> which accepts values between 0 and 255.
/// Lower values make the background darker; higher values make the background lighter.</para>
/// 
/// <para>The current <see cref="ProgressBar.Text" /> is displayed using the values of properties
/// <see cref="Font" /> and <see cref="ForeColor" />.</para>
/// 
/// <para><note type="inherit">When you derive from CustomProgressBar, adding new functionality to the
/// derived class, if your derived class references objects that must be disposed of before an instance of
/// your class is destroyed, you must override the <see cref="Dispose(bool)" />
/// method, and call <see cref="Component.Dispose()">Dispose()</see> on all objects
/// that are referenced in your class, before calling <c>Dispose(disposing)</c> on the base class.</note></para> <para>
/// (c) 2016 HISKE BEKKERING. All rights reserved.</para><para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-03-01 </para>
/// </remarks>
[Description( "Provides a ProgressBar which displays its Value as text on a faded background." )]
[Designer( typeof( CustomProgressBarDesigner ) )]
[ToolboxBitmap( typeof( ProgressBar ) )]
public class CustomProgressBar : ProgressBar
{
    #region " construction & destruction "

    /// <summary>
    /// Initializes a new instance of the
    /// <see cref="CustomProgressBar" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public CustomProgressBar() : base()
    {
        base.ForeColor = SystemColors.ControlText;
        this._fadeBrush = new SolidBrush( Color.FromArgb( this.Fade, Color.White ) );
    }

    /// <summary> Creates a new CustomProgressBar. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> A CustomProgressBar. </returns>
    public static CustomProgressBar Create()
    {
        CustomProgressBar? result = null;
        try
        {
            result = new CustomProgressBar();
        }
        catch
        {
            result?.Dispose();
            throw;
        }

        return result;
    }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="CustomProgressBar" />
    /// and optionally releases the managed resources.
    /// </summary>
    /// <remarks>
    /// This method is called by the public <see cref="Control.Dispose" /> method and the
    /// <see cref="object.Finalize" /> method. Dispose invokes Dispose with the
    /// <i>disposing</i> parameter set to <b>true</b>. Finalize invokes Dispose with
    /// <i>disposing</i> set to <b>false</b>.
    /// 
    /// <para><note type="inherit">Dispose might be called multiple times by other objects. When
    /// overriding <i>Dispose(Boolean)</i>, be careful not to reference objects that have been
    /// previously disposed of in an earlier call to Dispose.
    /// 
    /// <para>If your derived class references objects that must be disposed of before an instance of
    /// your class is destroyed, you must call <see cref="Control.Dispose" /> on all objects that are
    /// referenced in your class, before calling <c>Dispose(disposing)</c>
    /// on the base class.</para></note></para>
    /// </remarks>
    /// <param name="disposing"> <b>True</b> to release both managed and unmanaged resources;
    /// <b>false</b> to release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( disposing )
        {
            this._fadeBrush?.Dispose();
        }

        base.Dispose( disposing );
    }

    #endregion

    #region " public & protected properties "

    /// <summary>
    /// Returns the parameters used to create the window for the <see cref="CustomProgressBar" />
    /// control.
    /// </summary>
    /// <remarks>
    /// The information returned by the CreateParams property is used to pass information about the
    /// initial state and appearance of this control, at the time an instance of this class is being
    /// created.
    /// 
    /// <para><note type="inherit">When overriding the CreateParams property in a derived class, use
    /// the base class's CreateParams property to extend the base implementation. Otherwise, you must
    /// provide all the implementation.</note></para>
    /// </remarks>
    /// <value>
    /// A <see cref="CreateParams" /> object that contains the required creation
    /// parameters for the <see cref="CustomProgressBar" /> control.
    /// </value>
    protected override CreateParams CreateParams
    {
        get
        {
            CreateParams myParams = base.CreateParams;

            // Make the control use double buffering
            myParams.ExStyle |= NativeMethods.WS_EX_COMPOSITED;
            return myParams;
        }
    }

    /// <summary> The fade. </summary>
    private int _fade = 150;

    /// <summary> The fade brush. </summary>
    private SolidBrush _fadeBrush;

    /// <summary>
    /// Gets or sets the opacity of the white overlay brush which fades the background colors of the
    /// <see cref="CustomProgressBar" />.
    /// </summary>
    /// <remarks>
    /// You can use this property to manipulate the density of the background coloring of this
    /// control, to allow for better readability of any text within the
    /// <see cref="CustomProgressBar" />. You can use the <see cref="Font" /> and
    /// <see cref="ForeColor" /> properties to further optimize the display of text.
    /// 
    /// <para>Acceptable values for this property are between 0 and 255 inclusive. The default is 150;
    /// lower values make the background darker; higher values make the background lighter.</para>
    /// </remarks>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <value>
    /// An <see cref="int" /> representing the alpha value of the overlay color. The default
    /// is <b>150</b>.
    /// </value>
    [Category( "Appearance" )]
    [DefaultValue( 150 )]
    [Description( "Specifies the opacity of the white overlay brush which fades the background colors of the CustomProgressBar." )]
    public int Fade
    {
        get => this._fade;
        set
        {
            if ( value is < 0 or > 255 )
            {
                throw new ArgumentOutOfRangeException( nameof( value ),
                            $"A value of '{value}' is not valid for '{nameof( CustomProgressBar.Fade )}'; must be between 0 and 255." );
            }

            this._fade = value;

            // Clean up previous brush
            this._fadeBrush?.Dispose();

            this._fadeBrush = new SolidBrush( Color.FromArgb( value, Color.White ) );
            this.Invalidate();
        }
    }

    /// <summary>
    /// Gets or sets the <see cref="Font" /> of the text displayed by the
    /// <see cref="CustomProgressBar" />.
    /// </summary>
    /// <remarks>
    /// You can use the Font property to change the <see cref="Font" />
    /// to use when drawing text. To change the text <see cref="Color" />, use the
    /// <see cref="ForeColor" /> property.
    /// 
    /// <para>The Font property is an ambient property. An ambient property is a control property
    /// that, if not set, is retrieved from the parent control.</para>
    /// 
    /// <para>Because the <see cref="Font" /> class is immutable (meaning that you
    /// cannot adjust any of its properties), you can only assign the Font property a new Font.
    /// However, you can base the new font on the existing font.</para>
    /// 
    /// <para><note type="inherit">When overriding the Font property in a derived class, use the base
    /// class's Font property to extend the base implementation. Otherwise, you must provide all the
    /// implementation.</note></para>
    /// </remarks>
    /// <value>
    /// The <see cref="Font" /> to apply to the text displayed by the control.
    /// </value>
    [Browsable( true )]
    [EditorBrowsable( EditorBrowsableState.Always )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public override Font Font
    {
        get => base.Font;

#pragma warning disable CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).
        set => base.Font = value;
#pragma warning restore CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).
    }

    /// <summary>
    /// Gets or sets the color of the text displayed by <see cref="CustomProgressBar" />.
    /// </summary>
    /// <remarks>
    /// You can use the ForeColor property to change the color of the text within the
    /// <see cref="CustomProgressBar" /> to match the text of other controls on your form.
    /// To change the <see cref="Font" /> to use when drawing text, use the
    /// <see cref="Font">CustomProgressBar.Font</see> property.
    /// 
    /// <para><note type="inherit">When overriding the ForeColor property in a derived class, use the
    /// base class's ForeColor property to extend the base implementation. Otherwise, you must
    /// provide all the implementation.</note></para>
    /// </remarks>
    /// <value>
    /// A <see cref="Color" /> that represents the control's foreground color. The default is
    /// <b>ControlText</b>.
    /// </value>
    [DefaultValue( typeof( Color ), "ControlText" )]
    public override Color ForeColor
    {
        get => base.ForeColor;
        set => base.ForeColor = value;
    }

    /// <summary> The caption format. </summary>
    private string _captionFormat = "{0} %";

    /// <summary> Specifies the format of the overlay. </summary>
    /// <value> The caption format. </value>
    [Category( "Appearance" )]
    [DefaultValue( "{0} %" )]
    [Description( "Specifies the format of the overlay." )]
    public string CaptionFormat
    {
        get => string.IsNullOrEmpty( this._captionFormat ) ? "{0} %" : this._captionFormat;
        set => this._captionFormat = value;
    }

    /// <summary>
    /// Gets or sets the text associated with this <see cref="CustomProgressBar" />.
    /// </summary>
    /// <remarks>
    /// The <see cref="CustomProgressBar" /> control supports display of a single line of text,
    /// consisting of the <see cref="ProgressBar.Value" /> followed by a percent sign.
    /// 
    /// <para>The text is displayed using the values of properties
    /// <see cref="Font" /> and <see cref="ForeColor" />.</para>
    /// </remarks>
    /// <value> A <see cref="string" /> representing the text displayed in the control. </value>
    [Browsable( false )]
    [EditorBrowsable( EditorBrowsableState.Always )]
    [Bindable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public override string Text
    {
        get
        {
            string format = this.CaptionFormat;
            if ( string.IsNullOrEmpty( format ) )
            {
                format = "{0} %";
            }
            return string.Format( CultureInfo.CurrentCulture, format, this.Value );
        }

#pragma warning disable CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).
        set => base.Text = value;
#pragma warning restore CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).
    }

    #endregion

    #region " public & protected methods "

    /// <summary> Processes Windows messages. </summary>
    /// <remarks>
    /// All messages are sent to the WndProc method after getting filtered through the
    /// PreProcessMessage method. The WndProc method corresponds exactly to the Windows WindowProc
    /// function.
    /// 
    /// <para><note type="inherit">Inheriting controls should call the base class's WndProc method to
    /// process any messages that they do not handle.</note></para>
    /// </remarks>
    /// <param name="m"> [in,out] The Windows Message to process. </param>
    protected override void WndProc( ref Message m )
    {
        int message = m.Msg;
        if ( message == NativeMethods.WM_PAINT )
        {
            this.WmPaint( ref m );
            return;
        }

        if ( message == NativeMethods.WM_PRINTCLIENT )
        {
            this.WmPrintClient( ref m );
            return;
        }

        base.WndProc( ref m );
    }

    /// <summary> Returns a string representation for this <see cref="CustomProgressBar" />. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> A <see cref="string" /> that describes this control. </returns>
    public override string ToString()
    {
        System.Text.StringBuilder builder = new();
        _ = builder.Append( this.GetType().FullName );
        _ = builder.Append( ", Minimum: " );
        _ = builder.Append( this.Minimum.ToString( CultureInfo.CurrentCulture ) );
        _ = builder.Append( ", Maximum: " );
        _ = builder.Append( this.Maximum.ToString( CultureInfo.CurrentCulture ) );
        _ = builder.Append( ", Value: " );
        _ = builder.Append( this.Value.ToString( CultureInfo.CurrentCulture ) );
        return builder.ToString();
    }

    #endregion

    #region " private members "

    /// <summary> Paints the private described by device. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="device"> The device. </param>
    private void PaintPrivate( IntPtr device )
    {
        // Create a Graphics object for the device context
        using Graphics graphics = Graphics.FromHdc( device );
        Rectangle rect = this.ClientRectangle;
        if ( this._fadeBrush is not null )
        {
            // Paint a translucent white layer on top, to fade the colors a bit
            graphics.FillRectangle( this._fadeBrush, rect );
        }

        TextRenderer.DrawText( graphics, this.Text, this.Font, rect, this.ForeColor );
    }

    /// <summary> Windows message paint. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="m"> [in,out] The Windows Message to process. </param>
    private void WmPaint( ref Message m )
    {
        // Create a wrapper for the Handle
        HandleRef myHandle = new( this, this.Handle );

        // Prepare the window for painting and retrieve a device context
        NativeMethods.PAINTSTRUCT pAINTSTRUCT = new();

        IntPtr hDC = NativeMethods.BeginPaint( myHandle, ref pAINTSTRUCT );
        try
        {
            // Apply hDC to message
            m.WParam = hDC;

            // Let Windows paint
            base.WndProc( ref m );

            // Custom painting
            this.PaintPrivate( hDC );
        }
        finally
        {
            // Release the device context that BeginPaint retrieved
            _ = NativeMethods.EndPaint( myHandle, ref pAINTSTRUCT );
        }
    }

    /// <summary> Windows message print client. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="m"> [in,out] The Windows Message to process. </param>
    private void WmPrintClient( ref Message m )
    {
        // Retrieve the device context
        IntPtr hDC = m.WParam;

        // Let Windows paint
        base.WndProc( ref m );

        // Custom painting
        this.PaintPrivate( hDC );
    }

    #endregion
}
