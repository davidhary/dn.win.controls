using System.Runtime.InteropServices;
using System;
using System.Drawing;

namespace cc.isr.WinControls;


internal sealed partial class NativeMethods
{
    #region " parameters "

    /// <summary> The windows message print client. </summary>
    /// <remarks> Sent to a window to request that it draw its client area
    /// in the specified device context, most commonly in a printer device context.
    /// </remarks>
    public const int WM_PRINTCLIENT = 0x318;

    /// <summary> The WS EX composited. </summary>
    /// <remarks> Paints all descendants of a window in bottom-to-top painting order using double-buffering. </remarks>
    public const int WS_EX_COMPOSITED = 0x2000000;

    /// <summary> The windows message paint. Sent when the system makes a request to paint (a portion of) a window
    /// </summary>
    public const int WM_PAINT = 0xF;

    #endregion

    #region " structures "

    /// <summary> Contains basic information about a physical font. This is the Unicode version of the structure. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
    public struct TEXTMETRIC
    {
        /// <summary> The height (ascent + descent) of characters. . </summary>
        public int tmHeight;

        /// <summary> The ascent (units above the base line) of characters. </summary>
        public int tmAscent;

        /// <summary> The descent (units below the base line) of characters. . </summary>
        public int tmDescent;

        /// <summary> The amount of leading (space) inside the bounds set by the tmHeight member. . </summary>
        public int tmInternalLeading;

        /// <summary> The amount of extra leading (space) that the application adds between rows. </summary>
        public int tmExternalLeading;

        /// <summary> The average width of characters in the font (generally defined as the width of the letter x). </summary>
        public int tmAveCharWidth;

        /// <summary> The width of the widest character in the font.  </summary>
        public int tmMaxCharWidth;

        /// <summary> The weight of the font.  </summary>
        public int tmWeight;

        /// <summary> The extra width per string that may be added to some synthesized fonts.  </summary>
        public int tmOverhang;

        /// <summary> The horizontal aspect of the device for which the font was designed. </summary>
        public int tmDigitizedAspectX;

        /// <summary> The vertical aspect of the device for which the font was designed. </summary>
        public int tmDigitizedAspectY;

        /// <summary> The value of the first character defined in the font. </summary>
        public char tmFirstChar;

        /// <summary> The value of the last character defined in the font. </summary>
        public char tmLastChar;

        /// <summary> The value of the character to be substituted for characters not in the font. </summary>
        public char tmDefaultChar;

        /// <summary> The value of the character that will be used to define word breaks for text justification. </summary>
        public char tmBreakChar;

        /// <summary> Specifies an italic font if it is nonzero. </summary>
        public byte tmItalic;

        /// <summary> Specifies an underlined font if it is nonzero. </summary>
        public byte tmUnderlined;

        /// <summary> A strikeout font if it is nonzero. </summary>
        public byte tmStruckOut;

        /// <summary> Specifies information about the pitch, the technology, and the family of a physical font. </summary>
        public byte tmPitchAndFamily;

        /// <summary> The character set of the font. </summary>
        public byte tmCharSet;
    }

    /// <summary> Contains basic information about a physical font. This is the ANSI version of the structure. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet = System.Runtime.InteropServices.CharSet.Ansi)]
    public struct TEXTMETRICA
    {
        /// <summary> The height (ascent + descent) of characters. </summary>
        public int tmHeight;

        /// <summary> The ascent (units above the base line) of characters. </summary>
        public int tmAscent;
        /// <summary> The descent (units below the base line) of characters. </summary>
        public int tmDescent;

        /// <summary> The amount of leading (space) inside the bounds set by the tmHeight member. </summary>
        public int tmInternalLeading;

        /// <summary> The amount of extra leading (space) that the application adds between rows. </summary>
        public int tmExternalLeading;

        /// <summary> The average width of characters in the font (generally defined as the width of the letter x). </summary>
        public int tmAveCharWidth;

        /// <summary> The width of the widest character in the font. </summary>
        public int tmMaxCharWidth;

        /// <summary> The weight of the font. </summary>
        public int tmWeight;

        /// <summary> The extra width per string that may be added to some synthesized fonts. </summary>
        public int tmOverhang;

        /// <summary> The horizontal aspect of the device for which the font was designed. </summary>
        public int tmDigitizedAspectX;

        /// <summary> The vertical aspect of the device for which the font was designed. </summary>
        public int tmDigitizedAspectY;

        /// <summary> The value of the first character defined in the font. </summary>
        public byte tmFirstChar;

        /// <summary> The value of the last character defined in the font. </summary>
        public byte tmLastChar;

        /// <summary> The value of the character to be substituted for characters not in the font. </summary>
        public byte tmDefaultChar;

        /// <summary> The value of the character that will be used to define word breaks for text justification. </summary>
        public byte tmBreakChar;

        /// <summary> Specifies an italic font if it is nonzero. </summary>
        public byte tmItalic;

        /// <summary> Specifies an underlined font if it is nonzero. </summary>
        public byte tmUnderlined;

        /// <summary> A strikeout font if it is nonzero. </summary>
        public byte tmStruckOut;

        /// <summary> Specifies information about the pitch, the technology, and the family of a physical font. </summary>
        public byte tmPitchAndFamily;

        /// <summary> The character set of the font. </summary>
        public byte tmCharSet;
    }

    /// <summary> A paint structure. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    [StructLayout( LayoutKind.Sequential, Pack = 4 )]
    internal struct PAINTSTRUCT
    {
        /// <summary> A handle to the display DC to use for painting. </summary>
        public IntPtr hdc;

        /// <summary> Indicates whether the background should be erased. </summary>
        public int fErase;

        /// <summary> A RECT structure that specifies the upper left and lower right
        /// corners of the rectangle in which the painting is requested, </summary>
        public RECT rcPaint;

        /// <summary> Reserved; used internally by the system.  The restore. </summary>
        public int fRestore;

        /// <summary> Reserved; used internally by the system.  The restore. </summary>
        public int fIncUpdate;

        /// <summary> The RGB reserved. </summary>
        [MarshalAs( UnmanagedType.ByValArray, SizeConst = 32 )]
        public byte[] rgbReserved;
    }

    /// <summary> A rectangle. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    [StructLayout( LayoutKind.Sequential )]
    public struct RECT
    {
        /// <summary> The x-coordinate of the upper-left corner of the rectangle.  The left. </summary>
        public int left;

        /// <summary> The y-coordinate of the upper-left corner of the rectangle.  The left. </summary>
        public int top;

        /// <summary> The x-coordinate of the lower-right corner of the rectangle.  The left. </summary>
        public int right;

        /// <summary> The y-coordinate of the lower-right corner of the rectangle.  The left. </summary>
        public int bottom;

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="left">   The left. </param>
        /// <param name="top">    The top. </param>
        /// <param name="right">  The right. </param>
        /// <param name="bottom"> The bottom. </param>
        public RECT( int left, int top, int right, int bottom )
        {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="r"> A Rectangle to process. </param>
        public RECT( Rectangle r )
        {
            left = r.Left;
            top = r.Top;
            right = r.Right;
            bottom = r.Bottom;
        }

        /// <summary> From X, Y, W, H. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="x">      The x coordinate. </param>
        /// <param name="y">      The y coordinate. </param>
        /// <param name="width">  The width. </param>
        /// <param name="height"> The height. </param>
        /// <returns> A RECT. </returns>
        public static RECT FromXYWH( int x, int y, int width, int height )
        {
            return new RECT( x, y, x + width, y + height );
        }

        /// <summary> Initializes this object from the given int pointer. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="ptr"> The pointer. </param>
        /// <returns> A RECT. </returns>
        public static RECT FromIntPtr( IntPtr ptr )
        {
            RECT rect = ( RECT ) Marshal.PtrToStructure( ptr, typeof( RECT ) );
            return rect;
        }

        /// <summary> Gets the size. </summary>
        /// <value> The size. </value>
        public Size Size
        {
            get {
                return new Size( right - left, bottom - top );
            }
        }
    }

    #endregion
}
