using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace cc.isr.WinControls.ControlExtensions;

/// <summary> Includes extensions for controls. </summary>
/// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2010-11-19, 1.2.3975 </para></remarks>
internal static partial class ControlExtensionMethods
{
    #region " copy properties "

    /// <summary> Copies properties from a source to a destination controls. </summary>
    /// <remarks> Use this method to copy the properties of a control to a new control. </remarks>
    /// <param name="source">      specifies an instance of the control which properties are copied
    /// from. </param>
    /// <param name="destination"> specifies an instance of the control which properties are copied
    /// to. </param>
    /// <example>
    /// This example adds a button to a form.
    /// <code>
    /// Private buttons As New ArrayList()
    /// Private Sub AddButton(ByVal sourceButton As Button)
    /// Dim newButton As New Button()
    /// WinFormsSupport.CopyControlProperties(sourceButton, newButton)
    /// Me.Controls.Add(newButton)
    /// AddHandler newButton.Click, AddressOf Me.ExitButton_Click
    /// newButton.Name = $"newButton {buttons.count}"
    /// newButton.Text = newButton.Name
    /// newButton.Top = newButton.Top + newButton.Height * (buttons.Count + 1)
    /// buttons.Add(newButton)
    /// End Sub
    /// </code>
    /// To run this example, paste the code fragment into the method region of a Visual Basic form.
    /// Run the program by pressing F5.
    /// </example>
    public static void CopyControlProperties( this Control source, Control destination )
    {
        if ( source is null || destination is null ) return;

        string[] doNotCopyNames = ["WindowTarget"];
        System.Reflection.PropertyInfo[] propertyData;
        if ( !(source is null || destination is null) )
        {
            propertyData = source.GetType().GetProperties();
            foreach ( System.Reflection.PropertyInfo propertyDatum in propertyData )
            {
                // copy only those properties without parameters - you'll rarely need indexed properties
                if ( propertyDatum.GetIndexParameters().Length == 0 )
                {
                    // skip properties that appear in the list of disallowed names.
                    if ( Array.IndexOf( doNotCopyNames, propertyDatum.Name ) < 0 )
                    {
                        // can only copy those properties that can be read and written.  
                        if ( propertyDatum.CanRead & propertyDatum.CanWrite )
                        {
                            // set the destination based on the source.
                            propertyDatum.SetValue( destination, propertyDatum.GetValue( source, null ), null );
                        }
                    }
                }
            }
        }
    }

    #endregion

    #region " read only tab stops "

    /// <summary> Disables tab stops on read only text box. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="controls"> The controls. </param>
    public static void DisableReadOnlyTabStop( this Control.ControlCollection controls )
    {
        if ( controls is not null )
        {
            foreach ( TextBoxBase tb in controls.OfType<TextBoxBase>() )
            {
                tb.TabStop = !tb.ReadOnly;
            }
        }
    }

    /// <summary> Disables tab stops on read only text box. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="control"> The control to enable or disable. </param>
    public static void DisableReadOnlyTabStop( this Control control )
    {
        if ( control is not null && control.Controls is not null )
        {
            control.Controls.DisableReadOnlyTabStop();
            foreach ( Control c in control.Controls )
            {
                c.DisableReadOnlyTabStop();
            }
        }
    }

    #endregion

    #region " enable controls "

    /// <summary> Recursively enables or disabled the control and all its children . </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="control"> The control to enable or disable. </param>
    /// <param name="value">   Specifies a value to set. </param>
    public static void RecursivelyEnable( this Control control, bool value )
    {
        if ( control is not null )
        {
            control.Enabled = value;
            control.Controls.RecursivelyEnable( value );
        }
    }

    /// <summary> Recursively enables or disabled the control and all its children . </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="controls"> The controls. </param>
    /// <param name="value">    Specifies a value to set. </param>
    public static void RecursivelyEnable( this Control.ControlCollection controls, bool value )
    {
        if ( controls is not null )
        {
            foreach ( Control c in controls )
            {
                c.RecursivelyEnable( value );
            }
        }
    }

    #endregion

    #region " measure "

    /// <summary> Measure text. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="control"> The control to enable or disable. </param>
    /// <param name="text">    The text. </param>
    /// <returns> A Drawing.SizeF. </returns>
    public static SizeF MeasureText( this Control control, string text )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( control, nameof( control ) );
#else
        if ( control is null )
        {
            throw new ArgumentNullException( nameof( control ) );
        }
#endif

        using Graphics graphics = control.CreateGraphics();
        return graphics.MeasureString( text, control.Font );
    }

    /// <summary> Measure text. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="font"> The font. </param>
    /// <param name="text"> The text. </param>
    /// <returns> A Drawing.SizeF. </returns>
    public static SizeF MeasureText( this Font font, string text )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( font, nameof( font ) );
#else
        if ( font is null )
        {
            throw new ArgumentNullException( nameof( font ) );
        }
#endif

        using Control ctrl = new();
        using Graphics graphics = ctrl.CreateGraphics();
        return graphics.MeasureString( text, font );
    }

    /// <summary>   Select font to fit withing the specified width starting with the largest size. </summary>
    /// <remarks>   David, 2021-03-02. </remarks>
    /// <param name="largestSize">  Size of the largest. </param>
    /// <param name="width">        The width. </param>
    /// <param name="font">         The font. </param>
    /// <param name="text">         The text. </param>
    /// <returns>   A Font. </returns>
    public static Font SelectFont( this Font font, float largestSize, float width, string text )
    {
        float increment = 1;
        float size = largestSize + increment;
        do
        {
            size -= increment;
            font = new Font( font.FontFamily, size, font.Style );
        }
        while ( MeasureText( font, text ).Width >= width );

        // this is required to fit the font inside.
        font = new Font( font.FontFamily, ( float ) (size - 0.5), font.Style );

        return font;
    }

    #endregion

    #region " text "

    /// <summary>
    /// Sets the <see cref="TextBox">control</see> text to the
    /// <paramref name="value">value</paramref>.
    /// The control is disabled when set so that the handling of the changed event can be skipped.
    /// </summary>
    /// <remarks> The value is set to empty if null or empty. </remarks>
    /// <param name="control"> The control. </param>
    /// <param name="value">   The value. </param>
    /// <returns> value. </returns>
    public static string SilentTextSetter( this Control control, string value )
    {
        if ( control is not null )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                value = string.Empty;
            }

            bool enabled = control.Enabled;
            control.Enabled = false;
            control.Text = value;
            control.Enabled = enabled;
        }

        return value;
    }

    /// <summary>
    /// Sets the <see cref="TextBox">control</see> text to the formatted text. The control is
    /// disabled when set so that the handling of the changed event can be skipped.
    /// </summary>
    /// <remarks> The value is set to empty if null or empty. </remarks>
    /// <param name="control"> The control. </param>
    /// <param name="format">  The text format. </param>
    /// <param name="args">    The format arguments. </param>
    /// <returns> value. </returns>
    public static string SilentTextSetter( this Control control, string format, params object[] args )
    {
        return control.SilentTextSetter( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    #endregion

    #region " draw text "

    /// <summary>
    /// Draws text into a  <see cref="Control">windows control</see>
    /// center middle using the control font.
    /// </summary>
    /// <remarks>
    /// Call this method after changing the progress bar's value. If for some reason, the changing of
    /// the progress bar's value doesn't refresh it and clear the previously drawn text, call the
    /// Refresh method of the progress bar before calling this method.
    /// </remarks>
    /// <param name="control"> The target progress bar to add text into. </param>
    /// <param name="value">   The text to add into the progress bar. Leave null or empty to
    /// automatically add the percent. </param>
    public static void DrawText( this Control control, string value )
    {
        control?.DrawText( value, ContentAlignment.MiddleCenter, control.Font );
    }

    /// <summary>
    /// Draws text into a  <see cref="Control">windows control</see>.
    /// </summary>
    /// <remarks>
    /// Call this method after changing the progress bar's value. If for some reason, the changing of
    /// the progress bar's value doesn't refresh it and clear the previously drawn text, call the
    /// Refresh method of the progress bar before calling this method.
    /// </remarks>
    /// <param name="control">   The target progress bar to add text into. </param>
    /// <param name="value">     The text to add into the progress bar. Leave null or empty to
    /// automatically add the percent. </param>
    /// <param name="textAlign"> Where the text is to be placed. </param>
    public static void DrawText( this Control control, string value, ContentAlignment textAlign )
    {
        control?.DrawText( value, textAlign, control.Font );
    }

    /// <summary>
    /// Draws text into a  <see cref="Control">windows control</see>.
    /// </summary>
    /// <remarks>
    /// Call this method after changing the progress bar's value. If for some reason, the changing of
    /// the progress bar's value doesn't refresh it and clear the previously drawn text, call the
    /// Refresh method of the progress bar before calling this method.
    /// </remarks>
    /// <param name="control">   The target progress bar to add text into. </param>
    /// <param name="value">     The text to add into the progress bar. Leave null or empty to
    /// automatically add the percent. </param>
    /// <param name="textAlign"> Where the text is to be placed. </param>
    /// <param name="textFont">  The font the text should be drawn in. </param>
    public static void DrawText( this Control control, string value, ContentAlignment textAlign, Font textFont )
    {
        if ( control is not null )
        {
            if ( control.InvokeRequired )
            {
                _ = control.BeginInvoke( new Action<Control, string, ContentAlignment, Font>( DrawText ), [control, value, textAlign, textFont] );
            }
            else
            {
                using Graphics gr = control.CreateGraphics();
                const float MARGIN = 0f;
                float x = MARGIN;
                float y = (control.Height - gr.MeasureString( value, textFont ).Height) / 2.0f;
                if ( textAlign is ContentAlignment.BottomCenter or ContentAlignment.MiddleCenter or ContentAlignment.TopCenter )
                {
                    x = (control.Width - gr.MeasureString( value, textFont ).Width) / 2.0f;
                }
                else if ( textAlign is ContentAlignment.BottomRight or ContentAlignment.MiddleRight or ContentAlignment.TopRight )
                {
                    x = control.Width - gr.MeasureString( value, textFont ).Width - MARGIN;
                }

                if ( textAlign is ContentAlignment.BottomCenter or ContentAlignment.BottomLeft or ContentAlignment.BottomRight )
                {
                    y = control.Height - gr.MeasureString( value, textFont ).Height - MARGIN;
                }
                else if ( textAlign is ContentAlignment.TopCenter or ContentAlignment.TopLeft or ContentAlignment.TopRight )
                {
                    y = MARGIN;
                }

                y = Math.Max( y, MARGIN );
                x = Math.Max( x, MARGIN );
                using SolidBrush sb = new( control.ForeColor );
                gr.DrawString( value, textFont, sb, new PointF( x, y ) );
            }
        }
    }

    #endregion

    #region " image "

    /// <summary> Take screen shot. </summary>
    /// <remarks> David, 2020-09-01. </remarks>
    /// <param name="control"> The control to enable or disable. </param>
    /// <returns> A Bitmap. </returns>
    public static Bitmap TakeScreenShot( this Control control )
    {
        Bitmap tmpImg = new( control.Width, control.Height );
        using ( Graphics g = Graphics.FromImage( tmpImg ) )
        {
            g.CopyFromScreen( control.PointToScreen( new Point( 0, 0 ) ), new Point( 0, 0 ), new Size( control.Width, control.Height ) );
        }

        return tmpImg;
    }

    /// <summary> Saves a screen shot. </summary>
    /// <remarks> David, 2020-09-01. </remarks>
    /// <param name="control">     The control to enable or disable. </param>
    /// <param name="filename">    Filename of the file. </param>
    /// <param name="imageFormat"> The image format. </param>
    public static void SaveScreenShot( this Control control, string filename, System.Drawing.Imaging.ImageFormat imageFormat )
    {
        Bitmap tmpImg = new( control.Width, control.Height );
        using ( Graphics g = Graphics.FromImage( tmpImg ) )
        {
            g.CopyFromScreen( control.PointToScreen( new Point( 0, 0 ) ), new Point( 0, 0 ), new Size( control.Width, control.Height ) );
        }

        tmpImg.Save( filename, imageFormat );
    }

    #endregion
}
