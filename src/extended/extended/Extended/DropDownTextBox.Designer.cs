using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

public partial class DropDownTextBox
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        _dropDownToggle = new System.Windows.Forms.CheckBox();
        _dropDownToggle.CheckedChanged += new EventHandler(DropDownToggle_CheckedChanged);
        _textBox = new TextBox();
        SuspendLayout();
        // 
        // _dropDownToggle
        // 
        _dropDownToggle.Appearance = Appearance.Button;
        _dropDownToggle.Cursor = Cursors.Arrow;
        _dropDownToggle.Dock = DockStyle.Right;
        _dropDownToggle.Image = Properties.Resources.go_down_8;
        _dropDownToggle.Location = new Point(514, 0);
        _dropDownToggle.Margin = new Padding(0);
        _dropDownToggle.MaximumSize = new Size(24, 25);
        _dropDownToggle.Name = "_dropDownToggle";
        _dropDownToggle.Size = new Size(24, 25);
        _dropDownToggle.TabIndex = 0;
        _dropDownToggle.TextAlign = ContentAlignment.TopCenter;
        _dropDownToggle.UseVisualStyleBackColor = true;
        // 
        // _textBox
        // 
        _textBox.BackColor = SystemColors.Window;
        _textBox.Dock = DockStyle.Fill;
        _textBox.ForeColor = SystemColors.ControlText;
        _textBox.Location = new Point(0, 0);
        _textBox.Margin = new Padding(0);
        _textBox.Name = "_TextBox";
        _textBox.ReadOnlyBackColor = SystemColors.Control;
        _textBox.ReadOnlyForeColor = SystemColors.WindowText;
        _textBox.ReadWriteBackColor = SystemColors.Window;
        _textBox.ReadWriteForeColor = SystemColors.ControlText;
        _textBox.Size = new Size(514, 25);
        _textBox.TabIndex = 1;
        // 
        // DropDownTextBox
        // 
        Controls.Add(_textBox);
        Controls.Add(_dropDownToggle);
        Margin = new Padding(0);
        Name = "DropDownTextBox";
        Size = new Size(538, 25);
        ResumeLayout(false);
        PerformLayout();
    }

    private System.Windows.Forms.CheckBox _dropDownToggle;
    internal TextBox _textBox;
}
