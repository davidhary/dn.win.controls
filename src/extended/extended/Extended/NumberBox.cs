using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary>   Number box control. </summary>
/// <remarks>
/// (c) 2020 Silicon Video. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
///  David, 2020-04-08 </para><para>
/// https://www.codeproject.com/Articles/5264392/NumberBox-Class-for-Number-Entry-Display
/// [Silicon Video](https://www.codeproject.com/script/Membership/View.aspx?mid=3452838)  </para>
/// </remarks>
[DesignerCategory( "code" )]
[Description( "Number Box" )]
public class NumberBox : TextBox
{
    #region " constructtion "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    public NumberBox()
    {
        Leave += this.NumberBox_Leave;
        KeyPress += this.NumberBox_KeyPress;
        KeyDown += this.NumberBox_KeyDown;
        MouseDoubleClick += this.NumberBox_MouseDoubleClick;
        Enter += this.NumberBox_Enter;
        this.AssignStdNoFormatString( "D" );
    }

    #endregion

    #region " properties "

    /// <summary>   The number format information. </summary>
    private readonly NumberFormatInfo _nfi = new CultureInfo( CultureInfo.CurrentCulture.Name, false ).NumberFormat;

    /// <summary>   Specifies the pattern. </summary>
    private string? _pattern;

    /// <summary>   True if Standard numeric format string is empty. </summary>
    private bool _formatStringIsEmpty;

    private string _numberFormatString = "D";

    /// <summary>   Standard Numeric Format String D,E,F,N,X, B for binary. </summary>
    /// <value> The Standard numeric format string </value>
    [Category( "NumberBox" ),
        Description( "Standard Numeric Format String D,E,F,N,X, B for binary" ), DefaultValue( "D" )]
    public string NumberFormatString
    {
        get => this._numberFormatString;
        set => this.AssignStdNoFormatString( value );
    }

    /// <summary>   Assign <see cref="NumberFormatString"/>. </summary>
    /// <remarks>   David, 2021-09-24. </remarks>
    /// <param name="value">    The value. </param>
    private void AssignStdNoFormatString( string? value )
    {
        this._numberFormatString = value ?? string.Empty;
        this._formatStringIsEmpty = string.IsNullOrEmpty( this._numberFormatString );
        this._pattern = this.BuildRegularExpression();
        this.LeaveBox();
    }

    private bool _prefixEmpty = true;

    private string? _prefix;

    /// <summary>   Prefix for X and B formats. </summary>
    /// <value> The prefix. </value>
    [Category( "NumberBox" ), Description( "Prefix for X and B formats" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string? Prefix
    {
        get => this._prefix;
        set
        {
            this._prefix = value;
            this._prefixEmpty = string.IsNullOrEmpty( this._prefix );
            this._pattern = this.BuildRegularExpression();
            this.LeaveBox();
        }
    }

    /// <summary>   True to suffix empty. </summary>
    private bool _suffixEmpty = true;

    private string? _suffix;

    /// <summary>   Suffix for X and B formats. </summary>
    /// <value> The suffix. </value>
    [Category( "NumberBox" ), Description( "Suffix for X and B formats" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string? Suffix
    {
        get => this._suffix;
        set
        {
            this._suffix = value;
            this._suffixEmpty = string.IsNullOrEmpty( this._suffix );
            this._pattern = this.BuildRegularExpression();
            this.LeaveBox();
        }
    }

    private double _maxValue = double.MaxValue;

    /// <summary>   Gets/sets Max Value...double. </summary>
    /// <value> The maximum value. </value>
    [Category( "NumberBox" ), Description( "gets/sets Max Value...double" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double MaxValue
    {
        get => this._maxValue;
        set
        {
            this._maxValue = value;
            this.LeaveBox();
        }
    }

    private double _minValue = double.MinValue;

    /// <summary>   Gets/sets Min Value...double. </summary>
    /// <value> The minimum value. </value>
    [Category( "NumberBox" ), Description( "gets/sets Min Value...double" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double MinValue
    {
        get => this._minValue;
        set
        {
            this._minValue = value;
            this.LeaveBox();
        }
    }

    /// <summary>   Gets/sets Value As Double. </summary>
    /// <value> The value as double. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As Double" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double ValueAsDouble
    {
        get => this.GetValue();
        set => this.SetValue( value );
    }

    /// <summary>   Gets/sets Value As Float. </summary>
    /// <value> The value as float. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As Float" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public float ValueAsFloat
    {
        get => ( float ) this.GetValue();
        set => this.SetValue( value );
    }

    /// <summary>   Gets/sets Value As Byte. </summary>
    /// <value> The value as byte. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As Byte" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public byte ValueAsByte
    {
        get => ( byte ) Math.Min( byte.MaxValue, Math.Max( byte.MinValue, this.GetValue() ) );
        set => this.SetValue( value );
    }

    /// <summary>   Gets/sets Value As SByte. </summary>
    /// <value> The value as s byte. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As SByte" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public sbyte ValueAsSByte
    {
        get => ( sbyte ) Math.Min( sbyte.MaxValue, Math.Max( sbyte.MinValue, this.GetValue() ) );
        set => this.SetValue( value );
    }

    /// <summary>   Gets/sets Value As UInt64. </summary>
    /// <value> The value as u int 64. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As UInt64" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ulong ValueAsUInt64
    {
        get => ( ulong ) Math.Min( ulong.MaxValue, Math.Max( ulong.MinValue, this.GetValue() ) );
        set => this.SetValue( value );
    }

    /// <summary>   Gets/sets Value As UInt32. </summary>
    /// <value> The value as u int 32. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As UInt32" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public uint ValueAsUInt32
    {
        get => ( uint ) Math.Min( uint.MaxValue, Math.Max( uint.MinValue, this.GetValue() ) );
        set => this.SetValue( value );
    }

    /// <summary>   Gets/sets Value As UInt16. </summary>
    /// <value> The value as u int 16. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As UInt16" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ushort ValueAsUInt16
    {
        get => ( ushort ) Math.Min( ushort.MaxValue, Math.Max( ushort.MinValue, this.GetValue() ) );
        set => this.SetValue( value );
    }

    /// <summary>   Gets/sets Value As Int64. </summary>
    /// <value> The value as int 64. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As Int64" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public long ValueAsInt64
    {
        get => ( long ) Math.Min( long.MaxValue, Math.Max( long.MinValue, this.GetValue() ) );
        set => this.SetValue( value );
    }

    /// <summary>   Gets/sets Value As Int32. </summary>
    /// <value> The value as int 32. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As Int32" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int ValueAsInt32
    {
        get => ( int ) Math.Min( int.MaxValue, Math.Max( int.MinValue, this.GetValue() ) );
        set => this.SetValue( value );
    }

    /// <summary>   Gets/sets Value As Int16. </summary>
    /// <value> The value as int 16. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As Int16" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public short ValueAsInt16
    {
        get => ( short ) Math.Min( short.MaxValue, Math.Max( short.MinValue, this.GetValue() ) );
        set => this.SetValue( value );
    }

    #endregion Fields

    #region " methods "

    /// <summary>   Leave box. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    public void LeaveBox()
    {
        this.SetValue( Math.Max( this._minValue, Math.Min( this._maxValue, this.GetValue() ) ) );
        this.OnValidated( System.EventArgs.Empty );
    }

    /// <summary>   Gets the value. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    /// <returns>   The value. </returns>
    private double GetValue()
    {
        if ( this._formatStringIsEmpty )
        {
            return 0;
        }

        double x;
        string text = this.RemovePrefixSuffix( this.Text );
        if ( string.IsNullOrEmpty( text ) )
        {
            text = "0";
        }

        try
        {
#if NET8_0_OR_GREATER
            x = string.IsNullOrEmpty( this._numberFormatString )
                ? Convert.ToDouble( text, System.Globalization.CultureInfo.CurrentCulture )
                : this._numberFormatString!.StartsWith( 'X' )
                  ? Convert.ToUInt64( text, 16 )
                  : this._numberFormatString.StartsWith( 'B' )
                    ? Convert.ToUInt64( text, 2 )
                    : Convert.ToDouble( text, System.Globalization.CultureInfo.CurrentCulture );
#else
            x = string.IsNullOrEmpty( this._numberFormatString )
                ? Convert.ToDouble( text, System.Globalization.CultureInfo.CurrentCulture )
                : this._numberFormatString!.StartsWith( "X", StringComparison.Ordinal )
                  ? Convert.ToUInt64( text, 16 )
                  : this._numberFormatString.StartsWith( "B", StringComparison.Ordinal )
                    ? Convert.ToUInt64( text, 2 )
                    : Convert.ToDouble( text, System.Globalization.CultureInfo.CurrentCulture );
#endif
        }
        catch ( Exception )
        {
            x = 0;
        }
        return x;
    }

    /// <summary>   Removes the prefix suffix described by text. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    /// <param name="text"> The text. </param>
    /// <returns>   A <see cref="string" />. </returns>
    private string RemovePrefixSuffix( string text )
    {
        if ( !this._prefixEmpty && text.StartsWith( this._prefix!, StringComparison.Ordinal ) )
        {
            text = text[this._prefix!.Length..];
        }

        if ( !this._suffixEmpty && text.EndsWith( this._suffix!, StringComparison.Ordinal ) )
        {
            text = text[..^this._suffix!.Length];
        }

        return text;
    }

    /// <summary>   Sets a value. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    /// <param name="x">    The x coordinate. </param>
    private void SetValue( double x )
    {
        if ( this._formatStringIsEmpty ) return;

        x = Math.Max( this._minValue, Math.Min( this._maxValue, x ) );
#if NET8_0_OR_GREATER
        if ( this._numberFormatString.StartsWith( 'D' ) )
#else
        if ( this._numberFormatString.StartsWith( "D", StringComparison.Ordinal ) )
#endif
        {
            if ( x >= 0 )
            {
                this.Text = (( uint ) x).ToString( this._numberFormatString, this._nfi );
            }
            else
            {
                x = -x;
                this.Text = @"-" + (( uint ) x).ToString( this._numberFormatString, this._nfi );
            }
        }
#if NET8_0_OR_GREATER
        else if ( this._numberFormatString.StartsWith( 'X' ) )
#else
        else if ( this._numberFormatString.StartsWith( "X", StringComparison.Ordinal ) )
#endif
        {
            this.Text = (( int ) x).ToString( this._numberFormatString, System.Globalization.CultureInfo.CurrentCulture );
        }
#if NET8_0_OR_GREATER
        else if ( this._numberFormatString.StartsWith( 'B' ) )
#else
        else if ( this._numberFormatString.StartsWith( "B", StringComparison.Ordinal ) )
#endif
        {
            string str = Convert.ToString( ( long ) x, 2 );
            int len = LengthParameter( this.NumberFormatString );
            if ( len == 0 )
            {
                len = 1;
            }

            while ( str.Length < len )
            {
                str = "0" + str;
            }

            this.Text = str;
        }
        else
        {
            this.Text = x.ToString( this._numberFormatString, this._nfi );
        }

        this.Text = this.AddPrefixSuffix( this.Text );
    }

    /// <summary>   Adds a prefix suffix. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    /// <param name="text"> The text. </param>
    /// <returns>   A <see cref="string" />. </returns>
    private string AddPrefixSuffix( string text )
    {
        if ( !this._prefixEmpty )
        {
            text = this._prefix + text;
        }

        if ( !this._suffixEmpty )
        {
            text += this._suffix;
        }

        return text;
    }

    /// <summary>   Builds regular expression. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    /// <returns>   A <see cref="string" />. </returns>
    private string BuildRegularExpression()
    {
        if ( this._formatStringIsEmpty )
        {
            return string.Empty;
        }

        // start of line anchor
        string str = "^";
        int len = LengthParameter( this._numberFormatString );

        // add in prefix
        if ( !this._prefixEmpty )
        {
            str = this.Prefix!.Aggregate( str, ( current, c ) => current + "[" + c + "]" );
        }

        // D4 => -?\d\d?\d?\d?\d?
#if NET8_0_OR_GREATER
        if ( this._numberFormatString.StartsWith( 'D' ) )
#else
        if ( this._numberFormatString.StartsWith( "D", StringComparison.Ordinal ) )
#endif
        {
            str += "-?";
            if ( len == 0 )
            {
                len = 4;
            }

            for ( int i = 0; i < len; ++i )
            {
                str += @"\d?";
            }
        }
        // E => ^-?\d*[.,]?\d?\d?\d?\d?\d?\d?[Ee]?[+-]?\d?\d?\d?
#if NET8_0_OR_GREATER
        if ( this._numberFormatString.StartsWith( 'E' ) )
#else
        if ( this._numberFormatString.StartsWith( "E", StringComparison.Ordinal ) )
#endif
        {
            str += @"-?\d*[.,]?";
            if ( len == 0 )
            {
                len = 6;  // SNFS default length
            }

            for ( int i = 0; i < len; ++i )
            {
                str += @"\d?";
            }

            str += @"[Ee]?[+-]?\d?\d?\d?";
        }
#if NET8_0_OR_GREATER
        if ( this._numberFormatString.StartsWith( 'F' ) )
#else
        if ( this._numberFormatString.StartsWith( "F", StringComparison.Ordinal ) )
#endif
        {
            str += @"-?\d*[.,]?";
            if ( len == 0 )
            {
                len = 2;  //SNFS default length
            }

            for ( int i = 0; i < len; ++i )
            {
                str += @"\d?";
            }
        }

#if NET8_0_OR_GREATER
        if ( this._numberFormatString.StartsWith( 'N' ) )
#else
        if ( this._numberFormatString.StartsWith( "N", StringComparison.Ordinal ) )
#endif
        {
            str += @"-?\d*[., ]?";
            if ( len == 0 )
            {
                len = 2;  //SNFS default length
            }

            for ( int i = 0; i < len; ++i )
            {
                str += @"\d?";
            }
        }

#if NET8_0_OR_GREATER
        if ( this._numberFormatString.StartsWith( 'X' ) )
#else
        if ( this._numberFormatString.StartsWith( "X", StringComparison.Ordinal ) )
#endif
        {
            if ( len == 0 )
            {
                len = 2;  //SNFS default length
            }

            for ( int i = 0; i < len; ++i )
            {
                str += @"[0-9A-Fa-f]?";
            }
        }

#if NET8_0_OR_GREATER
        if ( this._numberFormatString.StartsWith( 'B' ) )
#else
        if ( this._numberFormatString.StartsWith( "B", StringComparison.Ordinal ) )
#endif
        {
            if ( len == 0 )
            {
                len = 1;  //SNFS default length
            }

            for ( int i = 0; i < len; ++i )
            {
                str += @"[0-1]?";
            }
        }

        // add suffix
        if ( !this._suffixEmpty )
        {
            str = this._suffix!.Aggregate( str, ( current, c ) => current + "[" + c + "]" );
        }

        // and the end of line anchor
        str += "$";

        return str;
    }

    /// <summary>   Select editable region. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    private void SelectEditableRegion()
    {
        this.SelectionStart = this._prefixEmpty ? 0 : this._prefix!.Length;
        this.SelectionLength = this.Text.Length - this.SelectionStart - (this._suffixEmpty ? 0 : this._suffix!.Length);
    }

    /// <summary>   Length parameter. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    /// <param name="formatString"> The number format string. </param>
    /// <returns>   An int. </returns>
    private static int LengthParameter( string formatString )
    {
        try
        {
            return formatString.Length == 1 ? 0 : Convert.ToInt16( formatString[1..], System.Globalization.CultureInfo.CurrentCulture );
        }
        catch ( Exception )
        {
            return 0;
        }
    }

    #endregion Methods

    #region " events "

    /// <summary>   Event handler. Called by NumberBox for enter events. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void NumberBox_Enter( object? sender, EventArgs e )
    {
        this.SelectEditableRegion();
    }

    /// <summary>   Event handler. Called by NumberBox for mouse double click events. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void NumberBox_MouseDoubleClick( object? sender, MouseEventArgs e )
    {
        this.SelectEditableRegion();
    }

    /// <summary>   Event handler. Called by NumberBox for leave events. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void NumberBox_Leave( object? sender, EventArgs e )
    {
        this.LeaveBox();
    }

    /// <summary>   Event handler. Called by NumberBox for key down events. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Key event information. </param>
    private void NumberBox_KeyDown( object? sender, KeyEventArgs e )
    {
        // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
        switch ( e.KeyCode )
        {
            case Keys.Delete:
                if ( !this._prefixEmpty && this.SelectionStart < this._prefix!.Length )
                {
                    e.Handled = true;
                }

                if ( !this._suffixEmpty && this.SelectionStart > this.Text.Length - this._suffix!.Length - 1 )
                {
                    e.Handled = true;
                }

                this.SelectionLength = 0;
                break;
            case Keys.Back:
                if ( this.SelectionLength > 0 )
                {
                    this.Text = this.Text[..this.SelectionStart] + this.Text[(this.SelectionStart + this.SelectionLength)..];
                    this.SelectionStart = (!this._prefixEmpty) ? this._prefix!.Length : 0;
                    this.SelectionLength = 0;
                    e.Handled = true;
                }
                else if ( this.SelectionLength == 0 )
                {
                    if ( !this._prefixEmpty && this.SelectionStart > this._prefix!.Length )
                    {
                        int pos = this.SelectionStart - 1;
                        this.Text = this.Text[..(this.SelectionStart - 1)] + this.Text[this.SelectionStart..];
                        this.SelectionStart = pos;
                        e.Handled = true;
                    }
                    else if ( this._prefixEmpty && this.SelectionStart > 0 )
                    {
                        int pos = this.SelectionStart - 1;
                        this.Text = this.Text[..(this.SelectionStart - 1)] + this.Text[this.SelectionStart..];
                        this.SelectionStart = pos;
                        e.Handled = true;
                    }
                }
                break;
            case Keys.Left:
                if ( !this._prefixEmpty && this.SelectionStart < this._prefix!.Length + 1 )
                {
                    e.Handled = true;
                }

                this.SelectionLength = 0;
                break;
            case Keys.Right:
                if ( !this._suffixEmpty && this.SelectionStart > this.Text.Length - this._suffix!.Length - 1 )
                {
                    e.Handled = true;
                }

                this.SelectionLength = 0;
                break;
            case Keys.End:
                this.SelectionStart = this.Text.Length - (this._suffixEmpty ? 0 : this._suffix!.Length);
                e.Handled = true;
                this.SelectionLength = 0;
                break;
            case Keys.Home:
                this.SelectionStart = this._prefixEmpty ? 0 : this._prefix!.Length;
                e.Handled = true;
                this.SelectionLength = 0;
                break;
            case Keys.KeyCode:
                break;
            case Keys.Modifiers:
                break;
            case Keys.None:
                break;
            case Keys.LButton:
                break;
            case Keys.RButton:
                break;
            case Keys.Cancel:
                break;
            case Keys.MButton:
                break;
            case Keys.XButton1:
                break;
            case Keys.XButton2:
                break;
            case Keys.Tab:
                break;
            case Keys.LineFeed:
                break;
            case Keys.Clear:
                break;
            case Keys.Return:
                break;
            case Keys.ShiftKey:
                break;
            case Keys.ControlKey:
                break;
            case Keys.Menu:
                break;
            case Keys.Pause:
                break;
            case Keys.Capital:
                break;
            case Keys.KanaMode:
                break;
            case Keys.JunjaMode:
                break;
            case Keys.FinalMode:
                break;
            case Keys.HanjaMode:
                break;
            case Keys.Escape:
                break;
            case Keys.IMEConvert:
                break;
            case Keys.IMENonconvert:
                break;
            case Keys.IMEAccept:
                break;
            case Keys.IMEModeChange:
                break;
            case Keys.Space:
                break;
            case Keys.Prior:
                break;
            case Keys.Next:
                break;
            case Keys.Up:
                break;
            case Keys.Down:
                break;
            case Keys.Select:
                break;
            case Keys.Print:
                break;
            case Keys.Execute:
                break;
            case Keys.Snapshot:
                break;
            case Keys.Insert:
                break;
            case Keys.Help:
                break;
            case Keys.D0:
                break;
            case Keys.D1:
                break;
            case Keys.D2:
                break;
            case Keys.D3:
                break;
            case Keys.D4:
                break;
            case Keys.D5:
                break;
            case Keys.D6:
                break;
            case Keys.D7:
                break;
            case Keys.D8:
                break;
            case Keys.D9:
                break;
            case Keys.A:
                break;
            case Keys.B:
                break;
            case Keys.C:
                break;
            case Keys.D:
                break;
            case Keys.E:
                break;
            case Keys.F:
                break;
            case Keys.G:
                break;
            case Keys.H:
                break;
            case Keys.I:
                break;
            case Keys.J:
                break;
            case Keys.K:
                break;
            case Keys.L:
                break;
            case Keys.M:
                break;
            case Keys.N:
                break;
            case Keys.O:
                break;
            case Keys.P:
                break;
            case Keys.Q:
                break;
            case Keys.R:
                break;
            case Keys.S:
                break;
            case Keys.T:
                break;
            case Keys.U:
                break;
            case Keys.V:
                break;
            case Keys.W:
                break;
            case Keys.X:
                break;
            case Keys.Y:
                break;
            case Keys.Z:
                break;
            case Keys.LWin:
                break;
            case Keys.RWin:
                break;
            case Keys.Apps:
                break;
            case Keys.Sleep:
                break;
            case Keys.NumPad0:
                break;
            case Keys.NumPad1:
                break;
            case Keys.NumPad2:
                break;
            case Keys.NumPad3:
                break;
            case Keys.NumPad4:
                break;
            case Keys.NumPad5:
                break;
            case Keys.NumPad6:
                break;
            case Keys.NumPad7:
                break;
            case Keys.NumPad8:
                break;
            case Keys.NumPad9:
                break;
            case Keys.Multiply:
                break;
            case Keys.Add:
                break;
            case Keys.Separator:
                break;
            case Keys.Subtract:
                break;
            case Keys.Decimal:
                break;
            case Keys.Divide:
                break;
            case Keys.F1:
                break;
            case Keys.F2:
                break;
            case Keys.F3:
                break;
            case Keys.F4:
                break;
            case Keys.F5:
                break;
            case Keys.F6:
                break;
            case Keys.F7:
                break;
            case Keys.F8:
                break;
            case Keys.F9:
                break;
            case Keys.F10:
                break;
            case Keys.F11:
                break;
            case Keys.F12:
                break;
            case Keys.F13:
                break;
            case Keys.F14:
                break;
            case Keys.F15:
                break;
            case Keys.F16:
                break;
            case Keys.F17:
                break;
            case Keys.F18:
                break;
            case Keys.F19:
                break;
            case Keys.F20:
                break;
            case Keys.F21:
                break;
            case Keys.F22:
                break;
            case Keys.F23:
                break;
            case Keys.F24:
                break;
            case Keys.NumLock:
                break;
            case Keys.Scroll:
                break;
            case Keys.LShiftKey:
                break;
            case Keys.RShiftKey:
                break;
            case Keys.LControlKey:
                break;
            case Keys.RControlKey:
                break;
            case Keys.LMenu:
                break;
            case Keys.RMenu:
                break;
            case Keys.BrowserBack:
                break;
            case Keys.BrowserForward:
                break;
            case Keys.BrowserRefresh:
                break;
            case Keys.BrowserStop:
                break;
            case Keys.BrowserSearch:
                break;
            case Keys.BrowserFavorites:
                break;
            case Keys.BrowserHome:
                break;
            case Keys.VolumeMute:
                break;
            case Keys.VolumeDown:
                break;
            case Keys.VolumeUp:
                break;
            case Keys.MediaNextTrack:
                break;
            case Keys.MediaPreviousTrack:
                break;
            case Keys.MediaStop:
                break;
            case Keys.MediaPlayPause:
                break;
            case Keys.LaunchMail:
                break;
            case Keys.SelectMedia:
                break;
            case Keys.LaunchApplication1:
                break;
            case Keys.LaunchApplication2:
                break;
            case Keys.OemSemicolon:
                break;
            case Keys.Oemplus:
                break;
            case Keys.Oemcomma:
                break;
            case Keys.OemMinus:
                break;
            case Keys.OemPeriod:
                break;
            case Keys.OemQuestion:
                break;
            case Keys.Oemtilde:
                break;
            case Keys.OemOpenBrackets:
                break;
            case Keys.OemPipe:
                break;
            case Keys.OemCloseBrackets:
                break;
            case Keys.OemQuotes:
                break;
            case Keys.Oem8:
                break;
            case Keys.OemBackslash:
                break;
            case Keys.ProcessKey:
                break;
            case Keys.Packet:
                break;
            case Keys.Attn:
                break;
            case Keys.Crsel:
                break;
            case Keys.Exsel:
                break;
            case Keys.EraseEof:
                break;
            case Keys.Play:
                break;
            case Keys.Zoom:
                break;
            case Keys.NoName:
                break;
            case Keys.Pa1:
                break;
            case Keys.OemClear:
                break;
            case Keys.Shift:
                break;
            case Keys.Control:
                break;
            case Keys.Alt:
                break;
            default:
                break;
        }
    }

    /// <summary>   Event handler. Called by NumberBox for key press events. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Key press event information. </param>
    private void NumberBox_KeyPress( object? sender, KeyPressEventArgs e )
    {
        if ( e.Handled ) return;

        if ( string.IsNullOrEmpty( this.Text ) || this.SelectionLength == this.Text.Length )
        {
            this.Text = this._prefix + this._suffix;
            this.SelectionStart = !this._prefixEmpty ? this._prefix!.Length : 0;
        }

        if ( !this._suffixEmpty && !this.Text.EndsWith( this._suffix!, StringComparison.Ordinal ) )
        {
            this.Text += this._suffix;
            this.SelectionStart = this.Text.Length - this._suffix!.Length;
        }

        // insert key into temp string to see if it fits the pattern
        string text = this.SelectionLength > 0
            ? this.Text[..this.SelectionStart] + e.KeyChar + this.Text[(this.SelectionStart + this.SelectionLength)..]
            : this.Text.Insert( this.SelectionStart, e.KeyChar.ToString() );

        if ( !string.IsNullOrEmpty( this._pattern ) )
        {
            Regex regex = new( this._pattern, RegexOptions.None );
            // if doesn't match pattern then reject the key by returning e.Handled = true
            e.Handled = !regex.IsMatch( text );
        }
    }

    #endregion Events

    #region " binding "

    /// <summary>   Displays a value. </summary>
    /// <remarks>   David, 2021-09-24. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Convert event information. </param>
    public void DisplayValue( object? sender, ConvertEventArgs e )
    {
        this.DisplayValue( e );
    }

    /// <summary>   Displays a value. </summary>
    /// <remarks>   David, 2021-09-24. </remarks>
    /// <param name="convertEventArgs"> The convertEventArgs to act on. </param>
    private void DisplayValue( ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs is not null ) // && ReferenceEquals( convertEventArgs.DesiredType, typeof( string ) ) )
        {
            this.SetValue( Convert.ToDouble( convertEventArgs.Value, System.Globalization.CultureInfo.CurrentCulture ) );
            convertEventArgs.Value = this.Text;
        }
    }

    /// <summary>   The ConvertEventArgs extension method that parse value. </summary>
    /// <remarks>   David, 2021-09-24. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Convert event information. </param>
    public void ParseValue( object? sender, ConvertEventArgs e )
    {
        this.ParseValue( e );
    }

    /// <summary>   The ConvertEventArgs extension method that parse value. </summary>
    /// <remarks>   David, 2021-09-24. </remarks>
    /// <param name="convertEventArgs"> The convertEventArgs to act on. </param>
    private void ParseValue( ConvertEventArgs convertEventArgs )
    {
        if ( convertEventArgs is not null ) //  && ReferenceEquals( convertEventArgs.DesiredType, typeof( double ) ) )
        {
            double value = this.GetValue();
            convertEventArgs.Value = ( int ) value; // this.GetValue();
        }
    }

    #endregion " BINDING "
}
