using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A drop down box with two fields. </summary>
/// <remarks>
/// (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License. </para><para>
/// David, 2011-03-23, 1.02.4099 </para>
/// </remarks>
[Description( "Dual Field Combo Box" )]
[DefaultBindingProperty( "Text" )]
public partial class DualFieldComboBox : UserControl
{
    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    public DualFieldComboBox()
    {
        this.InitializeComponent();
        this.Text = string.Empty;
    }

    /// <summary>
    /// Releases the unmanaged resources used by the cc.isr.WinControls.UserControlBase and optionally
    /// releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    /// <summary> Gets the combo box. </summary>
    /// <value> The combo box. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public System.Windows.Forms.ComboBox ComboBox { get; private set; }

    /// <summary> Parse record. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    private void ParseRecord( string? value )
    {
        if ( string.IsNullOrWhiteSpace( value ) ) // OrElse String.Is NullOrWhiteSpace(value.Trim) Then
        {
            value = string.Empty;
            this._firstFieldTextBox.Text = value;
            this._secondFieldTextBox.Text = value;
        }
        else
        {
            value = value!.Trim();
            string[] names = value.Split( ' ' );
            if ( names.Length == 1 )
            {
                this._secondFieldTextBox.Text = value;
                this._firstFieldTextBox.Text = string.Empty;
            }
            else
            {
                this._secondFieldTextBox.Text = names[^1];
                this._firstFieldTextBox.Text = value[..^this.SecondField.Length].Trim();
            }
        }

        base.Text = value;
    }

    /// <summary> Gets or sets the text, which includes the two fields. </summary>
    /// <value> The full name. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public override string Text
    {
        get => base.Text;

#pragma warning disable CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).
        set
        {
            if ( string.IsNullOrWhiteSpace( value ) )
                value = string.Empty;

            if ( !string.Equals( value, this.Text, StringComparison.Ordinal ) )
                this.ParseRecord( value );
        }
#pragma warning restore CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).
    }

    /// <summary> Gets the first field. </summary>
    /// <value> The first field. </value>
    public string FirstField => this._firstFieldTextBox.Text;

    /// <summary> Gets the second field. </summary>
    /// <value> The second field. </value>
    public string SecondField => this._secondFieldTextBox.Text;

    /// <summary> Event handler. Called by _valueComboBox for selected index changed events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void TextComboBox_SelectedIndexChanged( object? sender, EventArgs e )
    {
        this.ParseRecord( this.ComboBox.Text );
    }

    /// <summary>
    /// Builds a value based on the <see cref="FirstField">first</see> and
    /// <see cref="SecondField">second </see> fields.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The built value. </returns>
    public string BuildValue()
    {
        return $"{this.FirstField} {this.SecondField}";
    }

    /// <summary> Event handler. Called by _firstFieldTextBox for validated events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FirstFieldTextBox_Validated( object? sender, EventArgs e )
    {
        // Me.parseRecord(Me.BuildValue)
    }

    /// <summary> Event handler. Called by _secondFieldTextBox for validated events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void SecondFieldTextBox_Validated( object? sender, EventArgs e )
    {
        // Me.parseRecord(Me.BuildValue)
    }

    /// <summary> Event handler. Called by DualFieldComboBox for resize events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void DualFieldComboBox_Resize( object? sender, EventArgs e )
    {
        this.ResizeMe();
    }

    /// <summary> Event handler. Called by  for  events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void ResizeMe()
    {
        const int BUTTON_WIDTH = 17;

        this._layout.Width = this.Width - BUTTON_WIDTH - this.Padding.Left - this.Padding.Right;
        this.ComboBox.Width = this._layout.Width + 17 - this._layout.Padding.Left;
        this.ComboBox.Left = this._layout.Left + this._layout.Padding.Left;
        this.ComboBox.Top = this.Height - this.Padding.Top - this.Padding.Bottom - this.ComboBox.Height;
    }

    /// <summary> Event handler. Called by _textComboBox for layout events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Layout event information. </param>
    private void TextComboBox_Layout( object? sender, LayoutEventArgs e )
    {
        this.ResizeMe();
    }

    /// <summary> Event handler. Called by DualFieldComboBox for validated events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void DualFieldComboBox_Validated( object? sender, EventArgs e )
    {
        this.ParseRecord( this.BuildValue() );
    }
}
