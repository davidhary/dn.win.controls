using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary>   A range numeric. </summary>
/// <remarks>   David, 2021-09-15. </remarks>
public partial class RangeNumeric : UserControlBase
{
    #region " construction and cleanup "

    /// <summary> Clears internal properties. </summary>
    /// <remarks> David, 2021-09-15. </remarks>
    public RangeNumeric() : base()
    {
        this.InitializeComponent();
        this._lowerNumericHorizontalLocation = HorizontalLocation.Left;
        this._lowerNumericVerticalLocation = VerticalLocation.Center;
        this._upperNumericHorizontalLocation = HorizontalLocation.Right;
        this._upperNumericVerticalLocation = VerticalLocation.Center;
        if ( this.LowerNumeric is not null ) this.LowerNumeric.ReadOnly = false;
        if ( this.UpperNumeric is not null ) this.UpperNumeric.ReadOnly = false;
        this.ArrangeControls();
    }

    /// <summary>
    /// Releases the unmanaged resources used by the cc.isr.WinControls.UserControlBase and optionally
    /// releases the managed resources.
    /// </summary>
    /// <remarks> David, 2021-09-15. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
                this.RemoveRangeChangedEventHandler( this.RangeChanged );
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " behavior "

    /// <summary> Gets or sets the read only property. </summary>
    /// <value> The read only. </value>
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Indicates whether the numeric is read only." )]
    public bool ReadOnly
    {
        get => this.LowerNumeric?.ReadOnly ?? true;
        set
        {
            if ( this.ReadOnly != value )
            {
                if ( this.LowerNumeric is not null ) this.LowerNumeric.ReadOnly = value;
                if ( this.UpperNumeric is not null ) this.UpperNumeric.ReadOnly = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary>   Gets or sets the minimum. </summary>
    /// <value> The minimum value. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public decimal Minimum
    {
        get => this.UpperNumeric?.Minimum ?? 0;
        set
        {
            if ( !decimal.Equals( value, this.Minimum ) )
            {
                if ( this.LowerNumeric is not null ) this.LowerNumeric.Minimum = value;
                if ( this.UpperNumeric is not null ) this.UpperNumeric.Minimum = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary>   Gets or sets the maximum. </summary>
    /// <value> The maximum value. </value>
    [DefaultValue( 100 )]
    [Category( "Behavior" )]
    [Description( "The maximum value." )]
    public decimal Maximum
    {
        get => this.UpperNumeric?.Maximum ?? 0;
        set
        {
            if ( !decimal.Equals( value, this.Maximum ) )
            {
                if ( this.LowerNumeric is not null ) this.LowerNumeric.Maximum = value;
                if ( this.UpperNumeric is not null ) this.UpperNumeric.Maximum = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary>   The lower value. </summary>
    /// <value> The lower. </value>
    [DefaultValue( 0 )]
    [Category( "Behavior" )]
    [Description( "The lower value." )]
    public decimal Lower
    {
        get => this.LowerNumeric?.Value ?? 0;
        set
        {
            if ( !decimal.Equals( value, this.Lower ) )
            {
                if ( this.LowerNumeric is not null ) this.LowerNumeric.Value = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary>   The upper value. </summary>
    /// <value> The upper. </value>
    [DefaultValue( 100 )]
    [Category( "Behavior" )]
    [Description( "The upper value." )]
    public decimal Upper
    {
        get => this.UpperNumeric?.Value ?? 0;
        set
        {
            if ( !decimal.Equals( value, this.Upper ) )
            {
                if ( this.UpperNumeric is not null ) this.UpperNumeric.Value = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary>   Gets or sets the decimal places. </summary>
    /// <value> The decimal places. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int DecimalPlaces
    {
        get => this.UpperNumeric?.DecimalPlaces ?? 0;
        set
        {
            if ( !int.Equals( value, this.DecimalPlaces ) )
            {
                if ( this.LowerNumeric is not null ) this.LowerNumeric.DecimalPlaces = value;
                if ( this.UpperNumeric is not null ) this.UpperNumeric.DecimalPlaces = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    #endregion

    #region " lower numeric "

    /// <summary>   Gets the lower numeric. </summary>
    /// <value> The lower numeric. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public NumericUpDown? LowerNumeric { get; private set; }

    private HorizontalLocation _lowerNumericHorizontalLocation;

    /// <summary> Gets or sets the Lower Numeric horizontal location. </summary>
    /// <value> The Lower Numeric horizontal location. </value>
    [DefaultValue( typeof( HorizontalLocation ), "Left" )]
    [Category( "Behavior" )]
    [Description( "Horizontal location of the Lower Numeric." )]
    public HorizontalLocation LowerNumericHorizontalLocation
    {
        get => this._lowerNumericHorizontalLocation;
        set
        {
            if ( value != this.LowerNumericHorizontalLocation )
            {
                this._lowerNumericHorizontalLocation = value;
                this.NotifyPropertyChanged();
                this.ArrangeControls();
            }
        }
    }

    private VerticalLocation _lowerNumericVerticalLocation;

    /// <summary> Gets or sets the Lower Numeric vertical location. </summary>
    /// <value> The Lower Numeric vertical location. </value>
    [DefaultValue( typeof( VerticalLocation ), "Center" )]
    [Category( "Behavior" )]
    [Description( "Vertical location of the Lower Numeric." )]
    public VerticalLocation LowerNumericVerticalLocation
    {
        get => this._lowerNumericVerticalLocation;
        set
        {
            if ( value != this.LowerNumericVerticalLocation )
            {
                this._lowerNumericVerticalLocation = value;
                this.NotifyPropertyChanged();
                this.ArrangeControls();
            }
        }
    }

    private string? _lowerNumericToolTip;

    /// <summary> Gets or sets the Lower Numeric toolTip. </summary>
    /// <value> The Lower Numeric toolTip. </value>
    [Category( "Appearance" )]
    [Description( "Lower Numeric Tool Tip" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "Lower numeric" )]
    public string? LowerNumericToolTip
    {
        get => this._lowerNumericToolTip;
        set
        {
            if ( !string.Equals( this.LowerNumericToolTip, value, StringComparison.Ordinal ) )
            {
                this._lowerNumericToolTip = value;
                if ( this.LowerNumeric is not null ) this.ToolTip?.SetToolTip( this.LowerNumeric, value );
                this.NotifyPropertyChanged();
            }
        }
    }

    #endregion

    #region " upper numeric "

    /// <summary>   Gets the lower numeric. </summary>
    /// <value> The lower numeric. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public NumericUpDown? UpperNumeric { get; private set; }

    private HorizontalLocation _upperNumericHorizontalLocation;

    /// <summary> Gets or sets the Upper Numeric horizontal location. </summary>
    /// <value> The Upper Numeric horizontal location. </value>
    [DefaultValue( typeof( HorizontalLocation ), "Right" )]
    [Category( "Behavior" )]
    [Description( "Horizontal location of the Upper Numeric." )]
    public HorizontalLocation UpperNumericHorizontalLocation
    {
        get => this._upperNumericHorizontalLocation;
        set
        {
            if ( value != this.UpperNumericHorizontalLocation )
            {
                this._upperNumericHorizontalLocation = value;
                this.NotifyPropertyChanged();
                this.ArrangeControls();
            }
        }
    }

    private VerticalLocation _upperNumericVerticalLocation;

    /// <summary> Gets or sets the Upper Numeric vertical location. </summary>
    /// <value> The Upper Numeric vertical location. </value>
    [DefaultValue( typeof( VerticalLocation ), "Center" )]
    [Category( "Behavior" )]
    [Description( "Vertical location of the Upper Numeric." )]
    public VerticalLocation UpperNumericVerticalLocation
    {
        get => this._upperNumericVerticalLocation;
        set
        {
            if ( value != this.UpperNumericVerticalLocation )
            {
                this._upperNumericVerticalLocation = value;
                this.NotifyPropertyChanged();
                this.ArrangeControls();
            }
        }
    }

    private string? _upperNumericToolTip;

    /// <summary> Gets or sets the Upper Numeric toolTip. </summary>
    /// <value> The Upper Numeric toolTip. </value>
    [Category( "Appearance" )]
    [Description( "Upper Numeric Tool Tip" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "Upper numeric" )]
    public string? UpperNumericToolTip
    {
        get => this._upperNumericToolTip;
        set
        {
            if ( !string.Equals( this.UpperNumericToolTip, value, StringComparison.Ordinal ) )
            {
                this._upperNumericToolTip = value;
                if ( this.UpperNumeric is not null ) this.ToolTip?.SetToolTip( this.UpperNumeric, value );
                this.NotifyPropertyChanged();
            }
        }
    }

    #endregion

    #region " arrange control "

    /// <summary> Gets the row and column for the control within the control layout. </summary>
    /// <remarks> David, 2021-09-15. </remarks>
    /// <param name="verticalLocation">   The vertical location. </param>
    /// <param name="horizontalLocation"> The horizontal location. </param>
    /// <returns> A (Column As Integer, Row As Integer) </returns>
    private static (int Column, int Row) LocateControl( VerticalLocation verticalLocation, HorizontalLocation horizontalLocation )
    {
        int column = 1;
        int row = 1;
        switch ( verticalLocation )
        {
            case VerticalLocation.Bottom:
                {
                    row = 2;
                    break;
                }

            case VerticalLocation.Center:
                {
                    row = 1;
                    break;
                }

            case VerticalLocation.Top:
                {
                    row = 0;
                    break;
                }

            default:
                break;
        }

        switch ( horizontalLocation )
        {
            case HorizontalLocation.Center:
                {
                    column = 1;
                    break;
                }

            case HorizontalLocation.Left:
                {
                    column = 0;
                    break;
                }

            case HorizontalLocation.Right:
                {
                    column = 2;
                    break;
                }

            default:
                break;
        }

        return (column, row);
    }

    /// <summary> Arrange the controls within the control layout. </summary>
    /// <remarks> David, 2021-09-15. </remarks>
    private void ArrangeControls()
    {
        (int column, int row) = LocateControl( this.LowerNumericVerticalLocation, this.LowerNumericHorizontalLocation );
        (int column, int row) secondaryLocation = LocateControl( this.UpperNumericVerticalLocation, this.UpperNumericHorizontalLocation );
        float hundredPercent = 100.0f;

        // clear the styles
        foreach ( ColumnStyle colStyle in this._layout.ColumnStyles )
        {
            colStyle.SizeType = SizeType.AutoSize;
        }

        foreach ( RowStyle rowStyle in this._layout.RowStyles )
        {
            rowStyle.SizeType = SizeType.AutoSize;
        }

        this._layout.RowStyles[row].SizeType = SizeType.Percent;
        this._layout.RowStyles[secondaryLocation.row].SizeType = SizeType.Percent;
        this._layout.ColumnStyles[column].SizeType = SizeType.Percent;
        this._layout.ColumnStyles[secondaryLocation.column].SizeType = SizeType.Percent;
        int percentCount = 0;
        foreach ( RowStyle rowStyle in this._layout.RowStyles )
        {
            if ( rowStyle.SizeType == SizeType.Percent )
            {
                percentCount += 1;
            }
        }

        foreach ( RowStyle rowStyle in this._layout.RowStyles )
        {
            if ( rowStyle.SizeType == SizeType.Percent )
            {
                rowStyle.Height = hundredPercent / percentCount;
            }
        }

        percentCount = 0;
        foreach ( ColumnStyle columnStyle in this._layout.ColumnStyles )
        {
            if ( columnStyle.SizeType == SizeType.Percent )
            {
                percentCount += 1;
            }
        }

        foreach ( ColumnStyle columnStyle in this._layout.ColumnStyles )
        {
            if ( columnStyle.SizeType == SizeType.Percent )
            {
                columnStyle.Width = hundredPercent / percentCount;
            }
        }

        this._layout.Controls.Clear();
        if ( this.LowerNumeric is not null ) this.LowerNumeric.Dock = DockStyle.None;
        if ( this.UpperNumeric is not null ) this.UpperNumeric.Dock = DockStyle.None;
        if ( this.LowerNumeric is not null ) this._layout.Controls.Add( this.LowerNumeric, column, row );
        if ( this.UpperNumeric is not null ) this._layout.Controls.Add( this.UpperNumeric, secondaryLocation.column, secondaryLocation.row );
        if ( this.LowerNumeric is not null ) this.LowerNumeric.Dock = DockStyle.Fill;
        if ( this.UpperNumeric is not null ) this.UpperNumeric.Dock = DockStyle.Fill;
    }

    #endregion

    #region " events "

    /// <summary>Occurs when the range value changed. </summary>
    public event EventHandler<EventArgs>? RangeChanged;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2021-09-15. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveRangeChangedEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                this.RangeChanged -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary>   Raises the Range Changed event. </summary>
    /// <remarks>   David, 2021-09-15. </remarks>
    protected virtual void OnRangeChanged()
    {
        this.RangeChanged?.Invoke( this, EventArgs.Empty );
    }

    #endregion
}
