using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace cc.isr.WinControls;

public partial class DualFieldComboBox
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    [MemberNotNull( nameof( ComboBox ))]
    private void InitializeComponent()
    {
        _layout = new TableLayoutPanel();
        _secondFieldTextBox = new System.Windows.Forms.TextBox();
        _secondFieldTextBox.Validated += new EventHandler(SecondFieldTextBox_Validated);
        _firstFieldTextBox = new System.Windows.Forms.TextBox();
        _firstFieldTextBox.Validated += new EventHandler(FirstFieldTextBox_Validated);
        _secondFieldTextBoxLabel = new Label();
        _firstFieldTextBoxLabel = new Label();
        ComboBox = new System.Windows.Forms.ComboBox();
        ComboBox.SelectedIndexChanged += new EventHandler(TextComboBox_SelectedIndexChanged);
        ComboBox.Layout += new LayoutEventHandler(TextComboBox_Layout);
        _layout.SuspendLayout();
        SuspendLayout();
        // 
        // _layout
        // 
        _layout.ColumnCount = 2;
        _layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
        _layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
        _layout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
        _layout.Controls.Add(_secondFieldTextBox, 1, 1);
        _layout.Controls.Add(_firstFieldTextBox, 0, 1);
        _layout.Controls.Add(_secondFieldTextBoxLabel, 1, 0);
        _layout.Controls.Add(_firstFieldTextBoxLabel, 0, 0);
        _layout.Dock = DockStyle.Left;
        _layout.Location = new Point(0, 0);
        _layout.Name = "_Layout";
        _layout.RowCount = 2;
        _layout.RowStyles.Add(new RowStyle());
        _layout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
        _layout.Size = new Size(339, 41);
        _layout.TabIndex = 0;
        // 
        // _secondFieldTextBox
        // 
        _secondFieldTextBox.Dock = DockStyle.Top;
        _secondFieldTextBox.Location = new Point(172, 16);
        _secondFieldTextBox.Name = "_SecondFieldTextBox";
        _secondFieldTextBox.Size = new Size(164, 20);
        _secondFieldTextBox.TabIndex = 3;
        // 
        // _firstFieldTextBox
        // 
        _firstFieldTextBox.Dock = DockStyle.Top;
        _firstFieldTextBox.Location = new Point(3, 16);
        _firstFieldTextBox.Name = "_FirstFieldTextBox";
        _firstFieldTextBox.Size = new Size(163, 20);
        _firstFieldTextBox.TabIndex = 1;
        // 
        // _secondFieldTextBoxLabel
        // 
        _secondFieldTextBoxLabel.AutoSize = true;
        _secondFieldTextBoxLabel.Dock = DockStyle.Top;
        _secondFieldTextBoxLabel.Location = new Point(172, 0);
        _secondFieldTextBoxLabel.Name = "_SecondFieldTextBoxLabel";
        _secondFieldTextBoxLabel.Size = new Size(164, 13);
        _secondFieldTextBoxLabel.TabIndex = 2;
        _secondFieldTextBoxLabel.Text = "LAST NAME";
        // 
        // _firstFieldTextBoxLabel
        // 
        _firstFieldTextBoxLabel.AutoSize = true;
        _firstFieldTextBoxLabel.Dock = DockStyle.Top;
        _firstFieldTextBoxLabel.Location = new Point(3, 0);
        _firstFieldTextBoxLabel.Name = "_FirstFieldTextBoxLabel";
        _firstFieldTextBoxLabel.Size = new Size(163, 13);
        _firstFieldTextBoxLabel.TabIndex = 0;
        _firstFieldTextBoxLabel.Text = "FIRST NAME";
        // 
        // _textComboBox
        // 
        ComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        ComboBox.FormattingEnabled = true;
        ComboBox.Location = new Point(0, 20);
        ComboBox.Name = "_TextComboBox";
        ComboBox.Size = new Size(360, 21);
        ComboBox.TabIndex = 0;
        // 
        // DualFieldComboBox
        // 
        AutoScaleDimensions = new SizeF(6.0f, 13.0f);
        AutoScaleMode = AutoScaleMode.Font;
        Controls.Add(_layout);
        Controls.Add(ComboBox);
        Name = "DualFieldComboBox";
        Size = new Size(360, 41);
        _layout.ResumeLayout(false);
        _layout.PerformLayout();
        Resize += new EventHandler(DualFieldComboBox_Resize);
        Validated += new EventHandler(DualFieldComboBox_Validated);
        ResumeLayout(false);
    }

    private TableLayoutPanel _layout;
    private System.Windows.Forms.TextBox _secondFieldTextBox;
    private System.Windows.Forms.TextBox _firstFieldTextBox;
    private Label _secondFieldTextBoxLabel;
    private Label _firstFieldTextBoxLabel;
}
