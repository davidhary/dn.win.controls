using System;
using System.ComponentModel;
using System.Windows.Forms;
using cc.isr.Std.EventContext;

namespace cc.isr.WinControls;

/// <summary>   A range number box. </summary>
/// <remarks>   David, 2021-09-15. </remarks>
public partial class RangeNumberBox : UserControlBase
{
    #region " construction and cleanup "

    /// <summary> Clears internal properties. </summary>
    /// <remarks> David, 2021-09-15. </remarks>
    public RangeNumberBox() : base()
    {
        this.InitializeComponent();
        this._lowerNumberBoxHorizontalLocation = HorizontalLocation.Left;
        this._lowerNumberBoxVerticalLocation = VerticalLocation.Center;
        this._upperNumberBoxHorizontalLocation = HorizontalLocation.Right;
        this._upperNumberBoxVerticalLocation = VerticalLocation.Center;
        if ( this.LowerNumberBox is not null ) this.LowerNumberBox.ReadOnly = false;
        if ( this.UpperNumberBox is not null ) this.UpperNumberBox.ReadOnly = false;
        this.ArrangeControls();
    }

    /// <summary>
    /// Releases the unmanaged resources used by the cc.isr.WinControls.UserControlBase and optionally
    /// releases the managed resources.
    /// </summary>
    /// <remarks> David, 2021-09-15. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
                this._rangeChangedEventHandlers?.RemoveAll();
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " range number box "

    /// <summary> Gets or sets the read only property. </summary>
    /// <value> The read only. </value>
    [DefaultValue( false )]
    [Category( "NumberBox" )]
    [Description( "Indicates whether the NumberBox is read only." )]
    public bool ReadOnly
    {
        get => this.LowerNumberBox?.ReadOnly ?? true;
        set
        {
            if ( this.ReadOnly != value )
            {
                if ( this.LowerNumberBox is not null ) this.LowerNumberBox.ReadOnly = value;
                if ( this.UpperNumberBox is not null ) this.UpperNumberBox.ReadOnly = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary>   Gets or sets the minimum. </summary>
    /// <value> The minimum value. </value>
    [DefaultValue( 100 )]
    [Category( "NumberBox" )]
    [Description( "The minimum value." )]
    public double Minimum
    {
        get => this.UpperNumberBox?.MinValue ?? 0;
        set
        {
            if ( !double.Equals( value, this.Minimum ) )
            {
                if ( this.LowerNumberBox is not null ) this.LowerNumberBox.MinValue = value;
                if ( this.UpperNumberBox is not null ) this.UpperNumberBox.MinValue = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary>   Gets or sets the maximum. </summary>
    /// <value> The maximum value. </value>
    [DefaultValue( 100 )]
    [Category( "NumberBox" )]
    [Description( "The maximum value." )]
    public double Maximum
    {
        get => this.UpperNumberBox?.MaxValue ?? 0;
        set
        {
            if ( !double.Equals( value, this.Maximum ) )
            {
                if ( this.LowerNumberBox is not null ) this.LowerNumberBox.MaxValue = value;
                if ( this.UpperNumberBox is not null ) this.UpperNumberBox.MaxValue = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary> Standard Numeric Format String D,E,F,N,X, B for binary. </summary>
    /// <value> The Standard numeric format string </value>
    [Category( "NumberBox" ), Description( "Standard Numeric Format String D,E,F,N,X, B for binary" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string NumberFormatString
    {
        get => this.LowerNumberBox?.NumberFormatString ?? string.Empty;
        set
        {
            if ( !string.Equals( this.NumberFormatString, value, StringComparison.Ordinal ) )
            {
                if ( this.LowerNumberBox is not null ) this.LowerNumberBox.NumberFormatString = value;
                if ( this.UpperNumberBox is not null ) this.UpperNumberBox.NumberFormatString = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary>   Prefix for X and B formats. </summary>
    /// <value> The prefix. </value>
    [Category( "NumberBox" ), Description( "Prefix for X and B formats" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string? Prefix
    {
        get => this.LowerNumberBox?.Prefix ?? string.Empty;
        set
        {
            if ( !string.Equals( this.Prefix, value, StringComparison.Ordinal ) )
            {
                if ( this.LowerNumberBox is not null ) this.LowerNumberBox.Prefix = value;
                if ( this.UpperNumberBox is not null ) this.UpperNumberBox.Prefix = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary>   Suffix for X and B formats. </summary>
    /// <value> The suffix. </value>
    [Category( "NumberBox" ), Description( "Suffix for X and B formats" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string? Suffix
    {
        get => this.LowerNumberBox?.Suffix ?? string.Empty;
        set
        {
            if ( !string.Equals( this.Suffix, value, StringComparison.Ordinal ) )
            {
                if ( this.LowerNumberBox is not null ) this.LowerNumberBox.Suffix = value;
                if ( this.UpperNumberBox is not null ) this.UpperNumberBox.Suffix = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    #endregion

    #region " lower number box "

    /// <summary>   Gets the lower number box. </summary>
    /// <value> The lower number box. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public NumberBox? LowerNumberBox { get; private set; }

    private HorizontalLocation _lowerNumberBoxHorizontalLocation;

    /// <summary> Gets or sets the Lower NumberBox horizontal location. </summary>
    /// <value> The Lower NumberBox horizontal location. </value>
    [DefaultValue( typeof( HorizontalLocation ), "Left" )]
    [Category( "Behavior" )]
    [Description( "Horizontal location of the Lower NumberBox." )]
    public HorizontalLocation LowerNumberBoxHorizontalLocation
    {
        get => this._lowerNumberBoxHorizontalLocation;
        set
        {
            if ( value != this.LowerNumberBoxHorizontalLocation )
            {
                this._lowerNumberBoxHorizontalLocation = value;
                this.NotifyPropertyChanged();
                this.ArrangeControls();
            }
        }
    }

    private VerticalLocation _lowerNumberBoxVerticalLocation;

    /// <summary> Gets or sets the Lower NumberBox vertical location. </summary>
    /// <value> The Lower NumberBox vertical location. </value>
    [DefaultValue( typeof( VerticalLocation ), "Center" )]
    [Category( "Behavior" )]
    [Description( "Vertical location of the Lower NumberBox." )]
    public VerticalLocation LowerNumberBoxVerticalLocation
    {
        get => this._lowerNumberBoxVerticalLocation;
        set
        {
            if ( value != this.LowerNumberBoxVerticalLocation )
            {
                this._lowerNumberBoxVerticalLocation = value;
                this.NotifyPropertyChanged();
                this.ArrangeControls();
            }
        }
    }

    private string? _lowerNumberBoxToolTip;

    /// <summary> Gets or sets the Lower NumberBox toolTip. </summary>
    /// <value> The Lower NumberBox toolTip. </value>
    [Category( "Appearance" )]
    [Description( "Lower NumberBox Tool Tip" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "Lower NumberBox" )]
    public string? LowerNumberBoxToolTip
    {
        get => this._lowerNumberBoxToolTip;
        set
        {
            if ( !string.Equals( this.LowerNumberBoxToolTip, value, StringComparison.Ordinal ) )
            {
                this._lowerNumberBoxToolTip = value;
                if ( this.LowerNumberBox is not null ) this.ToolTip?.SetToolTip( this.LowerNumberBox, value );
                this.NotifyPropertyChanged();
            }
        }
    }

    #endregion

    #region " upper number box "

    /// <summary>   Gets the Upper number box. </summary>
    /// <value> The Upper number box. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public NumberBox? UpperNumberBox { get; private set; }

    private HorizontalLocation _upperNumberBoxHorizontalLocation;

    /// <summary> Gets or sets the Upper NumberBox horizontal location. </summary>
    /// <value> The Upper NumberBox horizontal location. </value>
    [DefaultValue( typeof( HorizontalLocation ), "Right" )]
    [Category( "Behavior" )]
    [Description( "Horizontal location of the Upper NumberBox." )]
    public HorizontalLocation UpperNumberBoxHorizontalLocation
    {
        get => this._upperNumberBoxHorizontalLocation;
        set
        {
            if ( value != this.UpperNumberBoxHorizontalLocation )
            {
                this._upperNumberBoxHorizontalLocation = value;
                this.NotifyPropertyChanged();
                this.ArrangeControls();
            }
        }
    }

    private VerticalLocation _upperNumberBoxVerticalLocation;

    /// <summary> Gets or sets the Upper NumberBox vertical location. </summary>
    /// <value> The Upper NumberBox vertical location. </value>
    [DefaultValue( typeof( VerticalLocation ), "Center" )]
    [Category( "Behavior" )]
    [Description( "Vertical location of the Upper NumberBox." )]
    public VerticalLocation UpperNumberBoxVerticalLocation
    {
        get => this._upperNumberBoxVerticalLocation;
        set
        {
            if ( value != this.UpperNumberBoxVerticalLocation )
            {
                this._upperNumberBoxVerticalLocation = value;
                this.NotifyPropertyChanged();
                this.ArrangeControls();
            }
        }
    }

    private string? _upperNumberBoxToolTip;

    /// <summary> Gets or sets the Upper NumberBox toolTip. </summary>
    /// <value> The Upper NumberBox toolTip. </value>
    [Category( "Appearance" )]
    [Description( "Upper NumberBox Tool Tip" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "Upper NumberBox" )]
    public string? UpperNumberBoxToolTip
    {
        get => this._upperNumberBoxToolTip;
        set
        {
            if ( !string.Equals( this.UpperNumberBoxToolTip, value, StringComparison.Ordinal ) )
            {
                this._upperNumberBoxToolTip = value;
                if ( this.UpperNumberBox is not null ) this.ToolTip?.SetToolTip( this.UpperNumberBox, value );
                this.NotifyPropertyChanged();
            }
        }
    }

    #endregion

    #region " arrange control "

    /// <summary> Gets the row and column for the control within the control layout. </summary>
    /// <remarks> David, 2021-09-15. </remarks>
    /// <param name="verticalLocation">   The vertical location. </param>
    /// <param name="horizontalLocation"> The horizontal location. </param>
    /// <returns> A (Column As Integer, Row As Integer) </returns>
    private static (int Column, int Row) LocateControl( VerticalLocation verticalLocation, HorizontalLocation horizontalLocation )
    {
        int column = 1;
        int row = 1;
        switch ( verticalLocation )
        {
            case VerticalLocation.Bottom:
                {
                    row = 2;
                    break;
                }

            case VerticalLocation.Center:
                {
                    row = 1;
                    break;
                }

            case VerticalLocation.Top:
                {
                    row = 0;
                    break;
                }

            default:
                break;
        }

        switch ( horizontalLocation )
        {
            case HorizontalLocation.Center:
                {
                    column = 1;
                    break;
                }

            case HorizontalLocation.Left:
                {
                    column = 0;
                    break;
                }

            case HorizontalLocation.Right:
                {
                    column = 2;
                    break;
                }

            default:
                break;
        }

        return (column, row);
    }

    /// <summary> Arrange the controls within the control layout. </summary>
    /// <remarks> David, 2021-09-15. </remarks>
    private void ArrangeControls()
    {
        (int column, int row) = LocateControl( this.LowerNumberBoxVerticalLocation, this.LowerNumberBoxHorizontalLocation );
        (int column, int row) secondaryLocation = LocateControl( this.UpperNumberBoxVerticalLocation, this.UpperNumberBoxHorizontalLocation );
        float hundredPercent = 100.0f;

        // clear the styles
        foreach ( ColumnStyle colStyle in this._layout.ColumnStyles )
        {
            colStyle.SizeType = SizeType.AutoSize;
        }

        foreach ( RowStyle rowStyle in this._layout.RowStyles )
        {
            rowStyle.SizeType = SizeType.AutoSize;
        }

        this._layout.RowStyles[row].SizeType = SizeType.Percent;
        this._layout.RowStyles[secondaryLocation.row].SizeType = SizeType.Percent;
        this._layout.ColumnStyles[column].SizeType = SizeType.Percent;
        this._layout.ColumnStyles[secondaryLocation.column].SizeType = SizeType.Percent;
        int percentCount = 0;
        foreach ( RowStyle rowStyle in this._layout.RowStyles )
        {
            if ( rowStyle.SizeType == SizeType.Percent )
            {
                percentCount += 1;
            }
        }

        foreach ( RowStyle rowStyle in this._layout.RowStyles )
        {
            if ( rowStyle.SizeType == SizeType.Percent )
            {
                rowStyle.Height = hundredPercent / percentCount;
            }
        }

        percentCount = 0;
        foreach ( ColumnStyle columnStyle in this._layout.ColumnStyles )
        {
            if ( columnStyle.SizeType == SizeType.Percent )
            {
                percentCount += 1;
            }
        }

        foreach ( ColumnStyle columnStyle in this._layout.ColumnStyles )
        {
            if ( columnStyle.SizeType == SizeType.Percent )
            {
                columnStyle.Width = hundredPercent / percentCount;
            }
        }

        this._layout.Controls.Clear();
        if ( this.LowerNumberBox is not null ) this.LowerNumberBox.Dock = DockStyle.None;
        if ( this.UpperNumberBox is not null ) this.UpperNumberBox.Dock = DockStyle.None;
        if ( this.LowerNumberBox is not null ) this._layout.Controls.Add( this.LowerNumberBox, column, row );
        if ( this.UpperNumberBox is not null ) this._layout.Controls.Add( this.UpperNumberBox, secondaryLocation.column, secondaryLocation.row );
        if ( this.LowerNumberBox is not null ) this.LowerNumberBox.Dock = DockStyle.Fill;
        if ( this.UpperNumberBox is not null ) this.UpperNumberBox.Dock = DockStyle.Fill;
    }

    #endregion

    #region " events "

    /// <summary>   Raises the Range Changed event. </summary>
    /// <remarks>   David, 2021-09-15. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information to send to registered event handlers. </param>
    private void OnRangeChanged( object? sender, EventArgs e )
    {
        this.OnRangeChanged();
    }

    /// <summary> The Opening event handlers. </summary>
    private readonly EventHandlerContextCollection<EventArgs> _rangeChangedEventHandlers = [];

    /// <summary>Occurs when the range value changed. </summary>
    public event EventHandler<EventArgs> RangeChanged
    {
        add
        {
            this._rangeChangedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            if ( this.LowerNumberBox is not null ) this.LowerNumberBox.TextChanged += new EventHandler( value );
            if ( this.UpperNumberBox is not null ) this.UpperNumberBox.TextChanged += new EventHandler( value );
        }
        remove
        {
            this._rangeChangedEventHandlers.RemoveValue( value );
            if ( this.LowerNumberBox is not null ) this.LowerNumberBox.TextChanged -= new EventHandler( value );
            if ( this.UpperNumberBox is not null ) this.UpperNumberBox.TextChanged -= new EventHandler( value );
        }
    }

    /// <summary>   Raises the Range Changed event. </summary>
    /// <remarks>   David, 2021-09-15. </remarks>
    protected virtual void OnRangeChanged()
    {
        this._rangeChangedEventHandlers?.Send( this, EventArgs.Empty );
    }

    #endregion
}

