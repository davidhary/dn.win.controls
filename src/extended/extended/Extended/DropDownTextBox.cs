using System;
using System.ComponentModel;

namespace cc.isr.WinControls;

/// <summary> Drop down text box. </summary>
/// <remarks>
/// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-12-06 </para>
/// </remarks>
public partial class DropDownTextBox : System.Windows.Forms.UserControl
{
    /// <summary>
    /// A private constructor for this class making it not publicly creatable. This ensure using the
    /// class as a singleton.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public DropDownTextBox()
    {
        // This call is required by the designer.
        this.InitializeComponent();

        // Add any initialization after the InitializeComponent() call.
        this.InitialHeight = this.Height;
        if ( this.DropDownHeight <= this.InitialHeight )
        {
            this.DropDownHeight = 300;
        }

        this._dropDownToggle.Name = "_dropDownToggle";
    }

    /// <summary>
    /// Releases the unmanaged resources used by the cc.isr.WinControls.DropDownTextBox and optionally
    /// releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    /// <summary> Gets the text box. </summary>
    /// <value> The text box. </value>
    public TextBox TextBox => this._textBox;

    /// <summary> Toggle multi line. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="turnOn"> true to enable, false to disable the turn. </param>
    private void ToggleMultiLine( bool turnOn )
    {
        if ( this._textBox is not null )
        {
            if ( turnOn && !this._textBox.Multiline )
            {
                if ( this.DropDownHeight > this.InitialHeight )
                {
                    this._textBox.Multiline = true;
                    this.Height = this.DropDownHeight;
                }
            }
            else if ( !turnOn && this._textBox.Multiline )
            {
                this._textBox.Multiline = false;
                this.Height = this.InitialHeight;
            }
        }
    }

    /// <summary> Drop down toggle checked changed. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void DropDownToggle_CheckedChanged( object? sender, EventArgs e )
    {
        if ( sender is System.Windows.Forms.CheckBox checkBox )
        {
            this.ToggleMultiLine( checkBox.Checked );
            if ( this._textBox is not null )
            {
                checkBox.Image = this._textBox.Multiline ? Properties.Resources.go_up_8 : Properties.Resources.go_down_8;
            }
        }
    }

    /// <summary> Drop Down Height. </summary>
    /// <value> The height of the drop down. </value>
    [DefaultValue( 300 )]
    [Description( "Drop Down Height" )]
    [Category( "Appearance" )]
    public int DropDownHeight { get; set; }

    /// <summary> Gets or sets the height of the initial. </summary>
    /// <value> The height of the initial. </value>
    private int InitialHeight { get; set; }
}
