using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> Text box with read only non-validation. </summary>
/// <remarks>
/// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2013-09-27 </para>
/// </remarks>
[DesignerCategory( "code" )]
[Description( "Text Box" )]
public class TextBox : System.Windows.Forms.TextBox
{
    #region " construction "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public TextBox() : base()
    {
        base.ReadOnly = false;
        this.ApplyReadOnly();
    }

    #endregion

    #region " read only "

    private Color _readOnlyBackColor;

    /// <summary> Gets or sets the color of the read only back. </summary>
    /// <value> The color of the read only back. </value>
    [DefaultValue( typeof( Color ), "SystemColors.Control" )]
    [Description( "Back color when read only" )]
    [Category( "Appearance" )]
    public Color ReadOnlyBackColor
    {
        get
        {
            if ( this._readOnlyBackColor.IsEmpty )
            {
                this._readOnlyBackColor = SystemColors.Control;
            }

            return this._readOnlyBackColor;
        }

        set => this._readOnlyBackColor = value;
    }

    private Color _readOnlyForeColor;

    /// <summary> Gets or sets the color of the read only foreground. </summary>
    /// <value> The color of the read only foreground. </value>
    [DefaultValue( typeof( Color ), "SystemColors.WindowText" )]
    [Description( "Fore color when read only" )]
    [Category( "Appearance" )]
    public Color ReadOnlyForeColor
    {
        get
        {
            if ( this._readOnlyForeColor.IsEmpty )
            {
                this._readOnlyForeColor = SystemColors.WindowText;
            }

            return this._readOnlyForeColor;
        }

        set => this._readOnlyForeColor = value;
    }

    private Color _readWriteBackColor;

    /// <summary> Gets or sets the color of the read write back. </summary>
    /// <value> The color of the read write back. </value>
    [DefaultValue( typeof( Color ), "SystemColors.Window" )]
    [Description( "Back color when control is read/write" )]
    [Category( "Appearance" )]
    public Color ReadWriteBackColor
    {
        get
        {
            if ( this._readWriteBackColor.IsEmpty )
            {
                this._readWriteBackColor = SystemColors.Window;
            }

            return this._readWriteBackColor;
        }

        set => this._readWriteBackColor = value;
    }

    private Color _readWriteForeColor;

    /// <summary> Gets or sets the color of the read write foreground. </summary>
    /// <value> The color of the read write foreground. </value>
    [DefaultValue( typeof( Color ), "System.Drawing.SystemColors.ControlText" )]
    [Description( "Fore color when control is read/write" )]
    [Category( "Appearance" )]
    public Color ReadWriteForeColor
    {
        get
        {
            if ( this._readWriteForeColor.IsEmpty )
            {
                this._readWriteForeColor = SystemColors.ControlText;
            }

            return this._readWriteForeColor;
        }

        set => this._readWriteForeColor = value;
    }

    private ContextMenuStrip? _readWriteContextMenu;

    /// <summary> Applies the read only. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void ApplyReadOnly()
    {
        if ( base.ReadOnly )
        {
            this._readWriteContextMenu = base.ContextMenuStrip;
            this.ContextMenuStrip = new ContextMenuStrip();
            this.BackColor = this.ReadOnlyBackColor;
            this.ForeColor = this.ReadOnlyForeColor;
        }
        else
        {
            this.ContextMenuStrip = this._readWriteContextMenu;
            this.BackColor = this.ReadWriteBackColor;
            this.ForeColor = this.ReadWriteForeColor;
        }

        this.CausesValidation = !base.ReadOnly;
    }

    /// <summary> Gets or sets the read only. </summary>
    /// <value> The read only. </value>
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Indicates whether the combo box is read only." )]
    public new bool ReadOnly
    {
        get => base.ReadOnly;
        set
        {
            if ( this.ReadOnly != value )
            {
                base.ReadOnly = value;
                this.ApplyReadOnly();
            }
        }
    }

    #endregion
}
