using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> Combo box with read only capability. </summary>
/// <remarks>
/// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2013-09-27. </para><para>
/// David, 2018-01-07. Added implementation of binding update on item changed. </para><para>
/// Dan.A. http://www.CodeProject.com/KB/ComboBox/CSReadOnlyComboBox.aspx </para>
/// </remarks>
[DesignerCategory( "code" )]
[Description( "Extended Combo Box" )]
public class ComboBox : System.Windows.Forms.ComboBox
{
    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public ComboBox() : base() => this._dropDownStyle = ComboBoxStyle.DropDown;

    #region " read only implementation "

    /// <summary> The read write context menu. </summary>
    private ContextMenuStrip? _readWriteContextMenu;

    private bool _readOnly;

    /// <summary> Gets or sets the read only. </summary>
    /// <value> The read only. </value>
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Indicates whether the combo box is read only." )]
    public bool ReadOnly
    {
        get => this._readOnly;
        set
        {
            if ( this._readOnly != value )
            {
                this._readOnly = value;
                if ( value )
                {
                    this._readWriteContextMenu = base.ContextMenuStrip;
                    this.ContextMenuStrip = new ContextMenuStrip();
                    int h = this.Height;
                    base.DropDownStyle = ComboBoxStyle.Simple;
                    this.Height = h + 3;
                    this.BackColor = this.ReadOnlyBackColor;
                    this.ForeColor = this.ReadOnlyForeColor;
                }
                else
                {
                    this.ContextMenuStrip = this._readWriteContextMenu;
                    base.DropDownStyle = this._dropDownStyle;
                    this.BackColor = this.ReadWriteBackColor;
                    this.ForeColor = this.ReadWriteForeColor;
                }
            }
        }
    }

    private Color _readOnlyBackColor;

    /// <summary> Gets or sets the color of the read only back. </summary>
    /// <value> The color of the read only back. </value>
    [DefaultValue( typeof( Color ), "SystemColors.Control" )]
    [Description( "Back color when read only" )]
    [Category( "Appearance" )]
    public Color ReadOnlyBackColor
    {
        get
        {
            if ( this._readOnlyBackColor.IsEmpty )
            {
                this._readOnlyBackColor = SystemColors.Control;
            }

            return this._readOnlyBackColor;
        }

        set => this._readOnlyBackColor = value;
    }

    private Color _readOnlyForeColor;

    /// <summary> Gets or sets the color of the read only foreground. </summary>
    /// <value> The color of the read only foreground. </value>
    [DefaultValue( typeof( Color ), "SystemColors.WindowText" )]
    [Description( "Fore color when read only" )]
    [Category( "Appearance" )]
    public Color ReadOnlyForeColor
    {
        get
        {
            if ( this._readOnlyForeColor.IsEmpty )
            {
                this._readOnlyForeColor = SystemColors.WindowText;
            }

            return this._readOnlyForeColor;
        }

        set => this._readOnlyForeColor = value;
    }

    private Color _readWriteBackColor;

    /// <summary> Gets or sets the color of the read write back. </summary>
    /// <value> The color of the read write back. </value>
    [DefaultValue( typeof( Color ), "SystemColors.Window" )]
    [Description( "Back color when control is read/write" )]
    [Category( "Appearance" )]
    public Color ReadWriteBackColor
    {
        get
        {
            if ( this._readWriteBackColor.IsEmpty )
            {
                this._readWriteBackColor = SystemColors.Window;
            }

            return this._readWriteBackColor;
        }

        set => this._readWriteBackColor = value;
    }

    private Color _readWriteForeColor;

    /// <summary> Gets or sets the color of the read write foreground. </summary>
    /// <value> The color of the read write foreground. </value>
    [DefaultValue( typeof( Color ), "System.Drawing.SystemColors.ControlText" )]
    [Description( "Fore color when control is read/write" )]
    [Category( "Appearance" )]
    public Color ReadWriteForeColor
    {
        get
        {
            if ( this._readWriteForeColor.IsEmpty )
            {
                this._readWriteForeColor = SystemColors.ControlText;
            }

            return this._readWriteForeColor;
        }

        set => this._readWriteForeColor = value;
    }

    private ComboBoxStyle _dropDownStyle;

    /// <summary> Gets or sets the drop down style. </summary>
    /// <value> The drop down style. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new ComboBoxStyle DropDownStyle
    {
        get => this._dropDownStyle;
        set
        {
            if ( this._dropDownStyle != value )
            {
                this._dropDownStyle = value;
                if ( !this.ReadOnly )
                {
                    base.DropDownStyle = value;
                }
            }
        }
    }

    /// <summary> Raises the <see cref="Control.KeyDown" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="KeyEventArgs" /> that contains the event
    /// data. </param>
    protected override void OnKeyDown( KeyEventArgs e )
    {
        if ( e is null ) return;

        if ( this.ReadOnly && (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down || e.KeyCode == Keys.Delete || e.KeyCode == Keys.F4 || e.KeyCode == Keys.PageDown || e.KeyCode == Keys.PageUp) )
        {
            e.Handled = true;
        }
        else
        {
            base.OnKeyDown( e );
        }
    }

    /// <summary> Raises the <see cref="Control.KeyPress" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="KeyPressEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnKeyPress( KeyPressEventArgs e )
    {
        if ( e is null ) return;

        if ( this.ReadOnly )
        {
            e.Handled = true;
        }
        else
        {
            base.OnKeyPress( e );
        }
    }

    #endregion

    #region " selected item change change binding "

    /// <summary>   Searches for the first binding. </summary>
    /// <remarks>   David, 2021-03-11. </remarks>
    /// <param name="bindings">     The bindings. </param>
    /// <param name="propertyName"> Name of the property. </param>
    /// <returns>   The found binding. </returns>
    private static Binding? FindBinding( BindingsCollection bindings, string propertyName )
    {
        Binding? result = null;
        if ( bindings is not null )
        {
            foreach ( Binding b in bindings )
            {
                if ( string.Equals( b.PropertyName, propertyName, StringComparison.Ordinal ) )
                {
                    result = b;
                    break;
                }
            }
        }

        return result;
    }

    /// <summary>
    /// Overrides raising the <see cref="DomainUpDown.SelectedItemChanged" />
    /// event and updates the data source if <see cref="DataSourceUpdateMode"/> =.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    /// <seealso cref="DataSourceUpdateMode.OnPropertyChanged "/>
    protected override void OnSelectedItemChanged( EventArgs e )
    {
        base.OnSelectedItemChanged( e );
        Binding? binding = FindBinding( this.DataBindings, nameof( ComboBox.SelectedItem ) );
        if ( binding is not null && binding.DataSourceUpdateMode == DataSourceUpdateMode.OnPropertyChanged )
        {
            binding.WriteValue();
        }
    }

    #endregion
}
