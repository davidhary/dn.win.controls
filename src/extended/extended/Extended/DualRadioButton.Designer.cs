using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

public partial class DualRadioButton
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        _layout = new TableLayoutPanel();
        _primaryRadioButton = new RadioButton();
        _primaryRadioButton.CheckedChanged += new EventHandler(PrimaryRadioButtonCheckedChanged);
        _secondaryRadioButton = new RadioButton();
        _secondaryRadioButton.CheckedChanged += new EventHandler(SecondaryRadioButtonCheckedChanged);
        _layout.SuspendLayout();
        SuspendLayout();
        // 
        // _layout
        // 
        _layout.BackColor = Color.Transparent;
        _layout.ColumnCount = 3;
        _layout.ColumnStyles.Add(new ColumnStyle());
        _layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
        _layout.ColumnStyles.Add(new ColumnStyle());
        _layout.Controls.Add(_secondaryRadioButton, 2, 1);
        _layout.Controls.Add(_primaryRadioButton, 0, 1);
        _layout.Dock = DockStyle.Fill;
        _layout.Location = new Point(0, 0);
        _layout.Name = "_Layout";
        _layout.RowCount = 3;
        _layout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
        _layout.RowStyles.Add(new RowStyle());
        _layout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
        _layout.Size = new Size(107, 34);
        _layout.TabIndex = 0;
        // 
        // _primaryRadioButton
        // 
        _primaryRadioButton.AutoSize = true;
        _primaryRadioButton.Checked = true;
        _primaryRadioButton.Dock = DockStyle.Top;
        _primaryRadioButton.Location = new Point(3, 8);
        _primaryRadioButton.Name = "_PrimaryRadioButton";
        _primaryRadioButton.Size = new Size(41, 17);
        _primaryRadioButton.TabIndex = 1;
        _primaryRadioButton.TabStop = true;
        _primaryRadioButton.Text = "ON";
        _primaryRadioButton.UseVisualStyleBackColor = true;
        // 
        // _secondaryRadioButton
        // 
        _secondaryRadioButton.AutoSize = true;
        _secondaryRadioButton.Dock = DockStyle.Top;
        _secondaryRadioButton.Location = new Point(59, 8);
        _secondaryRadioButton.Name = "_SecondaryRadioButton";
        _secondaryRadioButton.Size = new Size(45, 17);
        _secondaryRadioButton.TabIndex = 1;
        _secondaryRadioButton.Text = "OFF";
        _secondaryRadioButton.UseVisualStyleBackColor = true;
        // 
        // DualRadioButton
        // 
        AutoScaleDimensions = new SizeF(6.0f, 13.0f);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.Transparent;
        Controls.Add(_layout);
        Name = "DualRadioButton";
        Size = new Size(107, 34);
        _layout.ResumeLayout(false);
        _layout.PerformLayout();
        ResumeLayout(false);
    }

    private TableLayoutPanel _layout;
    private RadioButton _primaryRadioButton;
    private RadioButton _secondaryRadioButton;
}
