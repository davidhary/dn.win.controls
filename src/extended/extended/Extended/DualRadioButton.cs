using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;
/// <summary>
/// A dual radio button control functioning as a check box control displaying both its states.
/// </summary>
/// <remarks>
/// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License. </para><para>
/// David, 2010-12-09, 1.02.3995 </para>
/// </remarks>
[Description( "Dual Radio Button" )]
[DefaultEvent( "CheckToggled" )]
[DefaultBindingProperty( "Checked" )]
public partial class DualRadioButton : UserControlBase
{
    #region " construction "

    /// <summary> Clears internal properties. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public DualRadioButton() : base()
    {
        this.InitializeComponent();
        this._primaryButtonHorizontalLocation = HorizontalLocation.Left;
        this._primaryButtonVerticalLocation = VerticalLocation.Center;
        this._secondaryButtonHorizontalLocation = HorizontalLocation.Right;
        this._secondaryButtonVerticalLocation = VerticalLocation.Center;
        this._primaryRadioButton.ReadOnly = false;
        this._secondaryRadioButton.ReadOnly = false;
        this._checked = false;
        this._primaryRadioButton.Checked = false;
        this._secondaryRadioButton.Checked = false;
        this.ArrangeButtons();
    }

    /// <summary>
    /// Releases the unmanaged resources used by the cc.isr.WinControls.UserControlBase and optionally
    /// releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;

                this.RemoveCheckToggledEventHandler( this.CheckToggled );
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " behavior "

    /// <summary> Gets or sets the read only property. </summary>
    /// <value> The read only. </value>
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Indicates whether the dual radio button is read only." )]
    public bool ReadOnly
    {
        get => this._primaryRadioButton.ReadOnly;
        set
        {
            if ( this.ReadOnly != value )
            {
                this._primaryRadioButton.ReadOnly = value;
                this._secondaryRadioButton.ReadOnly = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    private bool _checked;

    /// <summary> Gets or sets the checked property. </summary>
    /// <value> The checked. </value>
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Indicates whether the radio button is checked or not." )]
    public bool Checked
    {
        get => this._checked;
        set
        {
            if ( this.Checked != value )
            {
                this._checked = value;
                this._primaryRadioButton.Checked = value;
                this._secondaryRadioButton.Checked = !value;
                this.NotifyPropertyChanged();
                this.OnCheckToggled();
            }
        }
    }

    #endregion

    #region " appearance "

    private Color _checkedColor;

    /// <summary> Checked background color. </summary>
    /// <value> The color of the checked. </value>
    [Category( "Appearance" )]
    [Description( "Checked background color" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( typeof( Color ), "LightGreen" )]
    public Color CheckedColor
    {
        get => this._checkedColor;
        set
        {
            if ( !Equals( this.CheckedColor, value ) )
            {
                this._checkedColor = value;
                this.NotifyPropertyChanged();
                this.RadioButtonCheckedChanged( this._primaryRadioButton );
                this.RadioButtonCheckedChanged( this._secondaryRadioButton );
            }
        }
    }

    private Color _uncheckedColor;

    /// <summary> Unchecked background color. </summary>
    /// <value> The color of the unchecked. </value>
    [Category( "Appearance" )]
    [Description( "Unchecked background color" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( typeof( Color ), "DarkSeaGreen" )]
    public Color UncheckedColor
    {
        get => this._uncheckedColor;
        set
        {
            if ( !Equals( this.UncheckedColor, value ) )
            {
                this._uncheckedColor = value;
                this.NotifyPropertyChanged();
                this.RadioButtonCheckedChanged( this._primaryRadioButton );
                this.RadioButtonCheckedChanged( this._secondaryRadioButton );
            }
        }
    }

    #endregion

    #region " primary button "

    /// <summary> Primary Button appearance. </summary>
    /// <value> The Primary Button appearance. </value>
    [Category( "Appearance" )]
    [Description( "Primary Button appearance" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( typeof( Appearance ), "Button" )]
    public Appearance PrimaryButtonAppearance
    {
        get => this._primaryRadioButton.Appearance;
        set
        {
            if ( this.PrimaryButtonAppearance != value )
            {
                this._primaryRadioButton.Appearance = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary> Primary Button font. </summary>
    /// <value> The Primary Button font. </value>
    [Category( "Appearance" )]
    [Description( "Primary Button font" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    public Font PrimaryButtonFont
    {
        get => this._primaryRadioButton.Font;
        set
        {
            if ( !Equals( this.PrimaryButtonFont, value ) )
            {
                this._primaryRadioButton.Font = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    private HorizontalLocation _primaryButtonHorizontalLocation;

    /// <summary> Gets or sets the primary button horizontal location. </summary>
    /// <value> The primary button horizontal location. </value>
    [DefaultValue( typeof( HorizontalLocation ), "Left" )]
    [Category( "Behavior" )]
    [Description( "Horizontal location of the primary button." )]
    public HorizontalLocation PrimaryButtonHorizontalLocation
    {
        get => this._primaryButtonHorizontalLocation;
        set
        {
            if ( value != this.PrimaryButtonHorizontalLocation )
            {
                this._primaryButtonHorizontalLocation = value;
                this.NotifyPropertyChanged();
                this.ArrangeButtons();
            }
        }
    }

    private VerticalLocation _primaryButtonVerticalLocation;

    /// <summary> Gets or sets the primary button vertical location. </summary>
    /// <value> The primary button vertical location. </value>
    [DefaultValue( typeof( VerticalLocation ), "Center" )]
    [Category( "Behavior" )]
    [Description( "Vertical location of the primary button." )]
    public VerticalLocation PrimaryButtonVerticalLocation
    {
        get => this._primaryButtonVerticalLocation;
        set
        {
            if ( value != this.PrimaryButtonVerticalLocation )
            {
                this._primaryButtonVerticalLocation = value;
                this.NotifyPropertyChanged();
                this.ArrangeButtons();
            }
        }
    }

    /// <summary> Primary Button Text. </summary>
    /// <value> The Primary Button text. </value>
    [Category( "Appearance" )]
    [Description( "Primary Button Text" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "O&N" )]
    public string PrimaryButtonText
    {
        get => this._primaryRadioButton.Text;
        set
        {
            if ( !string.Equals( this.PrimaryButtonText, value, StringComparison.Ordinal ) )
            {
                this._primaryRadioButton.Text = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary> Gets or sets the Primary Button toolTip. </summary>
    /// <value> The Primary Button toolTip. </value>
    [Category( "Appearance" )]
    [Description( "Primary Button text alignment" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( typeof( ContentAlignment ), "MiddleCenter" )]
    public ContentAlignment PrimaryButtonTextAlign
    {
        get => this._primaryRadioButton.TextAlign;
        set
        {
            if ( this.PrimaryButtonTextAlign != value )
            {
                this._primaryRadioButton.TextAlign = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    private string? _primaryButtonToolTip;

    /// <summary> Gets or sets the Primary Button toolTip. </summary>
    /// <value> The Primary Button toolTip. </value>
    [Category( "Appearance" )]
    [Description( "Primary Button Tool Tip" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "Press to turn on" )]
    public string? PrimaryButtonToolTip
    {
        get => this._primaryButtonToolTip;
        set
        {
            if ( !string.Equals( this.PrimaryButtonToolTip, value, StringComparison.Ordinal ) )
            {
                this._primaryButtonToolTip = value;
                this.ToolTip?.SetToolTip( this._primaryRadioButton, value );
                this.NotifyPropertyChanged();
            }
        }
    }

    #endregion

    #region " secondary button "

    /// <summary> Secondary Button appearance. </summary>
    /// <value> The Secondary Button appearance. </value>
    [Category( "Appearance" )]
    [Description( "Secondary Button appearance" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( typeof( Appearance ), "Button" )]
    public Appearance SecondaryButtonAppearance
    {
        get => this._secondaryRadioButton.Appearance;
        set
        {
            if ( this.SecondaryButtonAppearance != value )
            {
                this._secondaryRadioButton.Appearance = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary> Secondary Button font. </summary>
    /// <value> The Secondary Button font. </value>
    [Category( "Appearance" )]
    [Description( "Secondary Button font" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    public Font SecondaryButtonFont
    {
        get => this._secondaryRadioButton.Font;
        set
        {
            if ( !Equals( this.SecondaryButtonFont, value ) )
            {
                this._secondaryRadioButton.Font = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    private HorizontalLocation _secondaryButtonHorizontalLocation;

    /// <summary> Gets or sets the secondary button horizontal location. </summary>
    /// <value> The secondary button horizontal location. </value>
    [DefaultValue( typeof( HorizontalLocation ), "Right" )]
    [Category( "Behavior" )]
    [Description( "Horizontal location of the Secondary button." )]
    public HorizontalLocation SecondaryButtonHorizontalLocation
    {
        get => this._secondaryButtonHorizontalLocation;
        set
        {
            if ( value != this.SecondaryButtonHorizontalLocation )
            {
                this._secondaryButtonHorizontalLocation = value;
                this.NotifyPropertyChanged();
                this.ArrangeButtons();
            }
        }
    }

    /// <summary> Gets or sets the text associated with the secondary button. </summary>
    /// <value> The secondary button text. </value>
    [Category( "Appearance" )]
    [Description( "The text of the Secondary button" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "O&FF" )]
    public string SecondaryButtonText
    {
        get => this._secondaryRadioButton.Text;
        set
        {
            if ( !string.Equals( this.SecondaryButtonText, value, StringComparison.Ordinal ) )
            {
                this._secondaryRadioButton.Text = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    private VerticalLocation _secondaryButtonVerticalLocation;

    /// <summary> Gets or sets the secondary button vertical location. </summary>
    /// <value> The secondary button vertical location. </value>
    [DefaultValue( typeof( VerticalLocation ), "Center" )]
    [Category( "Behavior" )]
    [Description( "Vertical location of the Secondary button." )]
    public VerticalLocation SecondaryButtonVerticalLocation
    {
        get => this._secondaryButtonVerticalLocation;
        set
        {
            if ( value != this.SecondaryButtonVerticalLocation )
            {
                this._secondaryButtonVerticalLocation = value;
                this.NotifyPropertyChanged();
                this.ArrangeButtons();
            }
        }
    }

    /// <summary> Gets or sets the Secondary Button toolTip. </summary>
    /// <value> The Secondary Button toolTip. </value>
    [Category( "Appearance" )]
    [Description( "Secondary Button text alignment" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( typeof( ContentAlignment ), "MiddleCenter" )]
    public ContentAlignment SecondaryButtonTextAlign
    {
        get => this._secondaryRadioButton.TextAlign;
        set
        {
            if ( this.SecondaryButtonTextAlign != value )
            {
                this._secondaryRadioButton.TextAlign = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    private string? _secondaryButtonToolTip;

    /// <summary> Gets or sets the Secondary Button toolTip. </summary>
    /// <value> The Secondary Button toolTip. </value>
    [Category( "Appearance" )]
    [Description( "Secondary Button Tool Tip" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "Press to turn off" )]
    public string? SecondaryButtonToolTip
    {
        get => this._secondaryButtonToolTip;
        set
        {
            if ( !string.Equals( this.SecondaryButtonToolTip, value, StringComparison.Ordinal ) )
            {
                this._secondaryButtonToolTip = value;
                this.ToolTip?.SetToolTip( this._primaryRadioButton, value );
                this.NotifyPropertyChanged();
            }
        }
    }

    #endregion

    #region " event handlers "

    /// <summary> Handles the control checked changed described by control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="control"> The control. </param>
    private void RadioButtonCheckedChanged( System.Windows.Forms.RadioButton control )
    {
        control.BackColor = control.Checked ? this.CheckedColor : this.UncheckedColor;
    }

    /// <summary> Handles the primary radio button checked changed described by sender. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    private void PrimaryRadioButtonCheckedChanged( object? sender )
    {
        if ( sender is RadioButton control )
        {
            this.Checked = control.Checked;
            this._secondaryRadioButton.Checked = !control.Checked;
            this.RadioButtonCheckedChanged( control );
        }
    }

    /// <summary> Handles the secondary radio button checked changed described by sender. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    private void SecondaryRadioButtonCheckedChanged( object? sender )
    {
        if ( sender is RadioButton control )
        {
            this.Checked = !control.Checked;
            control.BackColor = control.Checked ? this.CheckedColor : this.UncheckedColor;
            this.RadioButtonCheckedChanged( control );
        }
    }

    /// <summary> Raises the radio button checked changed event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void PrimaryRadioButtonCheckedChanged( object? sender, EventArgs e )
    {
        this.PrimaryRadioButtonCheckedChanged( sender );
    }

    /// <summary> Off radio button checked changed. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void SecondaryRadioButtonCheckedChanged( object? sender, EventArgs e )
    {
        this.SecondaryRadioButtonCheckedChanged( sender );
    }

    /// <summary> Gets the row and column for the button withing the control layout. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="verticalLocation">   The vertical location. </param>
    /// <param name="horizontalLocation"> The horizontal location. </param>
    /// <returns> A (Column As Integer, Row As Integer) </returns>
    private static (int Column, int Row) LocateButton( VerticalLocation verticalLocation, HorizontalLocation horizontalLocation )
    {
        int column = 1;
        int row = 1;
        switch ( verticalLocation )
        {
            case VerticalLocation.Bottom:
                {
                    row = 2;
                    break;
                }

            case VerticalLocation.Center:
                {
                    row = 1;
                    break;
                }

            case VerticalLocation.Top:
                {
                    row = 0;
                    break;
                }

            default:
                break;
        }

        switch ( horizontalLocation )
        {
            case HorizontalLocation.Center:
                {
                    column = 1;
                    break;
                }

            case HorizontalLocation.Left:
                {
                    column = 0;
                    break;
                }

            case HorizontalLocation.Right:
                {
                    column = 2;
                    break;
                }

            default:
                break;
        }

        return (column, row);
    }

    /// <summary> Arrange buttons within the control layout. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void ArrangeButtons()
    {
        (int column, int row) = LocateButton( this.PrimaryButtonVerticalLocation, this.PrimaryButtonHorizontalLocation );
        (int column, int row) secondaryLocation = LocateButton( this.SecondaryButtonVerticalLocation, this.SecondaryButtonHorizontalLocation );
        float hundredPercent = 100.0f;

        // clear the styles
        foreach ( ColumnStyle colStyle in this._layout.ColumnStyles )
        {
            colStyle.SizeType = SizeType.AutoSize;
        }

        foreach ( RowStyle rowStyle in this._layout.RowStyles )
        {
            rowStyle.SizeType = SizeType.AutoSize;
        }

        this._layout.RowStyles[row].SizeType = SizeType.Percent;
        this._layout.RowStyles[secondaryLocation.row].SizeType = SizeType.Percent;
        this._layout.ColumnStyles[column].SizeType = SizeType.Percent;
        this._layout.ColumnStyles[secondaryLocation.column].SizeType = SizeType.Percent;
        int percentCount = 0;
        foreach ( RowStyle rowStyle in this._layout.RowStyles )
        {
            if ( rowStyle.SizeType == SizeType.Percent )
            {
                percentCount += 1;
            }
        }

        foreach ( RowStyle rowStyle in this._layout.RowStyles )
        {
            if ( rowStyle.SizeType == SizeType.Percent )
            {
                rowStyle.Height = hundredPercent / percentCount;
            }
        }

        percentCount = 0;
        foreach ( ColumnStyle columnStyle in this._layout.ColumnStyles )
        {
            if ( columnStyle.SizeType == SizeType.Percent )
            {
                percentCount += 1;
            }
        }

        foreach ( ColumnStyle columnStyle in this._layout.ColumnStyles )
        {
            if ( columnStyle.SizeType == SizeType.Percent )
            {
                columnStyle.Width = hundredPercent / percentCount;
            }
        }

        this._layout.Controls.Clear();
        this._primaryRadioButton.Dock = DockStyle.None;
        this._secondaryRadioButton.Dock = DockStyle.None;
        this._layout.Controls.Add( this._primaryRadioButton, column, row );
        this._layout.Controls.Add( this._secondaryRadioButton, secondaryLocation.column, secondaryLocation.row );
        this._primaryRadioButton.Dock = DockStyle.Fill;
        this._secondaryRadioButton.Dock = DockStyle.Fill;
    }

    #endregion

    #region " events "

    /// <summary>Occurs when the checked value changed. </summary>
    public event EventHandler<EventArgs>? CheckToggled;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveCheckToggledEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                CheckToggled -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Raises the Check changed event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected virtual void OnCheckToggled()
    {
        this.CheckToggled?.Invoke( this, EventArgs.Empty );
    }

    #endregion
}
/// <summary> Values that represent HorizontalLocation. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum HorizontalLocation
{
    /// <summary>Button located on the left side of the control (default behavior)</summary>
    Left,
    /// <summary>Button located in the center of the control</summary>
    Center,
    /// <summary>Button located at the right side of the control</summary>
    Right
}
/// <summary> Values that represent VerticalLocation. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum VerticalLocation
{
    /// <summary>Button located on the top side of the control (default behavior)</summary>
    Top,
    /// <summary>Button located in the center of the control</summary>
    Center,
    /// <summary>Button located at the bottom of the control</summary>
    Bottom
}
