namespace cc.isr.WinControls;

partial class RangeNumberBox
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    #region " component designer generated code "

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this._layout = new System.Windows.Forms.TableLayoutPanel();
        this.LowerNumberBox = new cc.isr.WinControls.NumberBox();
        this._upperNumberBox = new cc.isr.WinControls.NumberBox();
        this._layout.SuspendLayout();
        this.SuspendLayout();
        // 
        // _layout
        // 
        this._layout.BackColor = System.Drawing.Color.Transparent;
        this._layout.ColumnCount = 3;
        this._layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
        this._layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this._layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
        this._layout.Controls.Add(this.LowerNumberBox, 0, 1);
        this._layout.Controls.Add(this._upperNumberBox, 2, 1);
        this._layout.Dock = System.Windows.Forms.DockStyle.Fill;
        this._layout.Location = new System.Drawing.Point(0, 0);
        this._layout.Name = "_Layout";
        this._layout.RowCount = 3;
        this._layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
        this._layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
        this._layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
        this._layout.Size = new System.Drawing.Size(222, 42);
        this._layout.TabIndex = 1;
        // 
        // _lowerNumberBox
        // 
        this.LowerNumberBox.BackColor = System.Drawing.SystemColors.Window;
        this.LowerNumberBox.ForeColor = System.Drawing.SystemColors.ControlText;
        this.LowerNumberBox.Location = new System.Drawing.Point(3, 12);
        this.LowerNumberBox.MaxValue = 1.7976931348623157E+308D;
        this.LowerNumberBox.MinValue = -1.7976931348623157E+308D;
        this.LowerNumberBox.Name = "_lowerNumberBox";
        this.LowerNumberBox.Prefix = null;
        this.LowerNumberBox.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
        this.LowerNumberBox.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
        this.LowerNumberBox.ReadWriteBackColor = System.Drawing.SystemColors.Window;
        this.LowerNumberBox.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
        this.LowerNumberBox.Size = new System.Drawing.Size(100, 20);
        this.LowerNumberBox.NumberFormatString = null;
        this.LowerNumberBox.Suffix = null;
        this.LowerNumberBox.TabIndex = 2;
        this.LowerNumberBox.ValueAsByte = ((byte)(0));
        this.LowerNumberBox.ValueAsDouble = 0D;
        this.LowerNumberBox.ValueAsFloat = 0F;
        this.LowerNumberBox.ValueAsInt16 = ((short)(0));
        this.LowerNumberBox.ValueAsInt32 = 0;
        this.LowerNumberBox.ValueAsInt64 = ((long)(0));
        this.LowerNumberBox.ValueAsSByte = ((sbyte)(0));
        this.LowerNumberBox.ValueAsUInt16 = ((ushort)(0));
        this.LowerNumberBox.ValueAsUInt32 = ((uint)(0u));
        this.LowerNumberBox.ValueAsUInt64 = ((ulong)(0ul));
        // 
        // _upperNumberBox
        // 
        this._upperNumberBox.BackColor = System.Drawing.SystemColors.Window;
        this._upperNumberBox.ForeColor = System.Drawing.SystemColors.ControlText;
        this._upperNumberBox.Location = new System.Drawing.Point(119, 12);
        this._upperNumberBox.MaxValue = 1.7976931348623157E+308D;
        this._upperNumberBox.MinValue = -1.7976931348623157E+308D;
        this._upperNumberBox.Name = "_UpperNumberBox";
        this._upperNumberBox.Prefix = null;
        this._upperNumberBox.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
        this._upperNumberBox.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
        this._upperNumberBox.ReadWriteBackColor = System.Drawing.SystemColors.Window;
        this._upperNumberBox.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
        this._upperNumberBox.Size = new System.Drawing.Size(100, 20);
        this._upperNumberBox.NumberFormatString = null;
        this._upperNumberBox.Suffix = null;
        this._upperNumberBox.TabIndex = 3;
        this._upperNumberBox.ValueAsByte = ((byte)(0));
        this._upperNumberBox.ValueAsDouble = 0D;
        this._upperNumberBox.ValueAsFloat = 0F;
        this._upperNumberBox.ValueAsInt16 = ((short)(0));
        this._upperNumberBox.ValueAsInt32 = 0;
        this._upperNumberBox.ValueAsInt64 = ((long)(0));
        this._upperNumberBox.ValueAsSByte = ((sbyte)(0));
        this._upperNumberBox.ValueAsUInt16 = ((ushort)(0));
        this._upperNumberBox.ValueAsUInt32 = ((uint)(0u));
        this._upperNumberBox.ValueAsUInt64 = ((ulong)(0ul));
        // 
        // RangeNumberBox
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this._layout);
        this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        this.Name = "RangeNumberBox";
        this.Size = new System.Drawing.Size(222, 42);
        this._layout.ResumeLayout(false);
        this._layout.PerformLayout();
        this.ResumeLayout(false);

    }

    #endregion
    private System.Windows.Forms.TableLayoutPanel _layout;
    private NumberBox _upperNumberBox;
}
