using System;
using System.ComponentModel;

namespace cc.isr.WinControls;

/// <summary> Radio button. </summary>
/// <remarks>
/// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2013-09-27 </para>
/// </remarks>
[DesignerCategory( "code" )]
[Description( "Extended Radio Button" )]
public class RadioButton : System.Windows.Forms.RadioButton
{
    private bool _readonly;

    /// <summary> Gets or sets the read only property. </summary>
    /// <value> The read only. </value>
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Indicates whether the radio button is read only." )]
    public bool ReadOnly
    {
        get => this._readonly;
        set
        {
            if ( this._readonly != value )
            {
                this._readonly = value;
            }
        }
    }

    /// <summary> Raises the <see cref="System.Windows.Forms.RadioButton.OnClick" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnClick( EventArgs e )
    {
        if ( !this._readonly )
        {
            base.OnClick( e );
        }
    }
}
