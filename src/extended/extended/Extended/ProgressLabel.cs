using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A progress label. </summary>
/// <remarks>
/// (c) 2007 HyperCubed. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-09-19. http://www.codeproject.com/script/Membership/View.aspx?mid=722189
/// http://www.codeproject.com/Articles/21419/Label-with-ProgressBar-in-a-StatusStrip.
/// </para>
/// </remarks>
[DesignerCategory( "code" )]
[Description( "Label with progress bar" )]
public class ProgressLabel : Label
{
    /// <summary>
    /// Raises the <see cref="Control.Paint" />
    /// event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="PaintEventArgs" />
    /// that contains the event data. </param>
    protected override void OnPaint( PaintEventArgs e )
    {
        if ( e is null ) return;

        double percent = this.Value / 100d;
        Rectangle rect = e.ClipRectangle;
        int height = this.BarHeight;
        if ( height == 0 )
        {
            height = rect.Height;
        }

        rect.Width = ( int ) (rect.Width * percent);
        rect.Y = ( int ) (0.5d * (rect.Height - height));
        rect.Height = height;
        using ( SolidBrush brush = new( this.BarColor ) )
        {
            // Draw bar
            Graphics g = e.Graphics;
            g.FillRectangle( brush, rect );
        }

        base.OnPaint( e );
    }

    private int _value;

    /// <summary> Progress Value. </summary>
    /// <value> The value. </value>
    [Category( "Behavior" )]
    [Description( "Progress Value" )]
    [DefaultValue( 0 )]
    public int Value
    {
        get => this._value;
        set
        {
            // Make sure that the value does not stray outside the valid range.
            switch ( value )
            {
                case var @case when @case < 0:
                    {
                        this._value = 0;
                        break;
                    }

                case var case1 when case1 > 100:
                    {
                        this._value = 100;
                        break;
                    }

                default:
                    {
                        this._value = value;
                        break;
                    }
            }

            // Invalidate the control to get a repaint.
            this.Invalidate();
        }
    }

    private Color _barColor = Color.Blue;   // Color of bar

    /// <summary> Progress Color. </summary>
    /// <value> The color of the bar. </value>
    [Category( "Behavior" )]
    [Description( "Progress Color" )]
    [DefaultValue( typeof( Color ), "Blue" )]
    public Color BarColor
    {
        get => this._barColor;
        set
        {
            this._barColor = value;

            // Invalidate the control to get a repaint.
            this.Invalidate();
        }
    }

    private int _barHeight;

    /// <summary> Progress Height. </summary>
    /// <value> The height of the bar. </value>
    [Category( "Behavior" )]
    [Description( "Progress Height" )]
    [DefaultValue( 0 )]
    public int BarHeight
    {
        get => this._barHeight;
        set
        {
            switch ( value )
            {
                case var @case when @case > this.Size.Height:
                case var case1 when case1 < 0:
                    {
                        this._barHeight = this.Size.Height;
                        break;
                    }

                default:
                    {
                        this._barHeight = value;
                        break;
                    }
            }

            // Invalidate the control to get a repaint.
            this.Invalidate();
        }
    }

    private string? _captionFormat;

    /// <summary> Specifies the format of the overlay. </summary>
    /// <value> The caption format. </value>
    [Category( "Appearance" )]
    [DefaultValue( "{0} %" )]
    [Description( "Specifies the format of the overlay." )]
    public string CaptionFormat
    {
        get => string.IsNullOrEmpty( this._captionFormat ) ? "{0} %" : this._captionFormat!;
        set => this._captionFormat = value;
    }

    /// <summary> The default caption format. </summary>
#pragma warning disable IDE0079
#pragma warning disable CA1707
    public const string DEFAULT_CAPTION_FORMAT = "{0} %";
#pragma warning restore CA1707
#pragma warning restore IDE0079

    /// <summary> Updates the progress described by value. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    public void UpdateProgress( int value )
    {
        string format = this.CaptionFormat;
        if ( string.IsNullOrEmpty( format ) )
        {
            format = DEFAULT_CAPTION_FORMAT;
        }

        this.UpdateProgress( value, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, value ) );
    }

    /// <summary> Updates the progress described by arguments. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value">   The value. </param>
    /// <param name="caption"> The caption. </param>
    public void UpdateProgress( int value, string caption )
    {
        if ( value >= 0 ^ this.Visible )
        {
            this.Visible = value >= 0;
        }

        if ( this.Visible )
        {
            this.Text = caption;
            this.Value = value;
        }
    }
}
