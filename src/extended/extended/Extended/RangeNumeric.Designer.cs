namespace cc.isr.WinControls;

partial class RangeNumeric
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    #region " component designer generated code "

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this._layout = new System.Windows.Forms.TableLayoutPanel();
        this.LowerNumeric = new cc.isr.WinControls.NumericUpDown();
        this.UpperNumeric = new cc.isr.WinControls.NumericUpDown();
        this._layout.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.LowerNumeric)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.UpperNumeric)).BeginInit();
        this.SuspendLayout();
        // 
        // _layout
        // 
        this._layout.BackColor = System.Drawing.Color.Transparent;
        this._layout.ColumnCount = 3;
        this._layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
        this._layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this._layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
        this._layout.Controls.Add(this.LowerNumeric, 0, 1);
        this._layout.Controls.Add(this.UpperNumeric, 2, 1);
        this._layout.Dock = System.Windows.Forms.DockStyle.Fill;
        this._layout.Location = new System.Drawing.Point(0, 0);
        this._layout.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
        this._layout.Name = "_Layout";
        this._layout.RowCount = 3;
        this._layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
        this._layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
        this._layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
        this._layout.Size = new System.Drawing.Size(92, 46);
        this._layout.TabIndex = 1;
        // 
        // _lowerNumeric
        // 
        this.LowerNumeric.Location = new System.Drawing.Point(3, 10);
        this.LowerNumeric.Name = "_LowerNumeric";
        this.LowerNumeric.NullValue = new decimal(new int[] {
        0,
        0,
        0,
        0});
        this.LowerNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
        this.LowerNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
        this.LowerNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
        this.LowerNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
        this.LowerNumeric.Size = new System.Drawing.Size(37, 25);
        this.LowerNumeric.TabIndex = 0;
        this.LowerNumeric.Value = new decimal(new int[] {
        0,
        0,
        0,
        0});
        // 
        // UpperNumeric
        // 
        this.UpperNumeric.Location = new System.Drawing.Point(50, 10);
        this.UpperNumeric.Name = "_UpperNumeric";
        this.UpperNumeric.NullValue = new decimal(new int[] {
        0,
        0,
        0,
        0});
        this.UpperNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
        this.UpperNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
        this.UpperNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
        this.UpperNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
        this.UpperNumeric.Size = new System.Drawing.Size(39, 25);
        this.UpperNumeric.TabIndex = 1;
        this.UpperNumeric.Value = new decimal(new int[] {
        0,
        0,
        0,
        0});
        // 
        // RangeNumeric
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this._layout);
        this.Name = "RangeNumeric";
        this.Size = new System.Drawing.Size(92, 46);
        this._layout.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.LowerNumeric)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.UpperNumeric)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion
    private System.Windows.Forms.TableLayoutPanel _layout;
}
