using System.ComponentModel;

namespace cc.isr.WinControls;

/// <summary> Text box with read only non-validation. </summary>
/// <remarks>
/// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2013-09-27 </para>
/// </remarks>
[DesignerCategory( "code" )]
[Description( "Label Text Box" )]
public class LabelTextBox : System.Windows.Forms.TextBox
{
    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public LabelTextBox() : base()
    {
        base.ReadOnly = true;
        this.CausesValidation = false;
    }

    /// <summary> Gets or sets a value indicating whether text in the text box is read-only. </summary>
    /// <value> <c>true</c> if [read only]; otherwise, <c>false</c>. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new bool ReadOnly
    {
        get => base.ReadOnly;
        set
        {
            base.ReadOnly = value;
            this.CausesValidation = !base.ReadOnly;
        }
    }
}
