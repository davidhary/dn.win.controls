using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace cc.isr.WinControls;

/// <summary> Engineering up down. </summary>
/// <remarks>
/// Features: <para>
/// Disables up/down events when read only.</para><para>
/// Adds up/down cursor.</para><para>
/// Adds engineering scaling. </para> (c) 2014 Integrated Scientific Resources, Inc. All rights
/// reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-04-05 </para>
/// </remarks>
[Description( "Engineering Up Down" )]
public class EngineeringUpDown : NumericUpDownBase
{
    #region " construction "

    /// <summary> object creator. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public EngineeringUpDown() : base()
    {
        this._unit = string.Empty;
        this.UpdateScalingExponent( 0 );
        this.Postfix = string.Empty;
    }

    #endregion

    #region " text box "

    /// <summary> Strips post fix. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> A <see cref="string" />. </returns>
    private string StripedText()
    {
        return this.TextBox is not null && !string.IsNullOrWhiteSpace( this.Postfix ) && this.TextBox.Text.EndsWith( this.Postfix, StringComparison.Ordinal )
            ? this.TextBox.Text[..this.TextBox.Text.IndexOf( this.Postfix, StringComparison.Ordinal )]
            : this.Value.ToString( System.Globalization.CultureInfo.CurrentCulture );
    }

    /// <summary> Strips post fix. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void StripPostFix()
    {
        if ( this.TextBox is not null && !string.IsNullOrWhiteSpace( this.Postfix ) && this.TextBox.Text.EndsWith( this.Postfix, StringComparison.Ordinal ) )
        {
            this.TextBox.Text = this.TextBox.Text[..this.TextBox.Text.IndexOf( this.Postfix, StringComparison.Ordinal )];
        }
    }

    /// <summary> Raises the <see cref="System.Windows.Forms.Control.LostFocus" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnLostFocus( EventArgs e )
    {
        this.StripPostFix();
        base.OnLostFocus( e );
        this.UpdateEditText();
    }

    /// <summary> Raises the <see cref="System.Windows.Forms.Control.Validating" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="CancelEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnValidating( CancelEventArgs e )
    {
        this.StripPostFix();
        base.OnValidating( e );
        this.UpdateEditText();
    }

    /// <summary>
    /// Validates and updates the text displayed in the spin box (also known as an up-down control).
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected override void ValidateEditText()
    {
        this.StripPostFix();
        base.ValidateEditText();
    }

    /// <summary>
    /// Displays the current value of the spin box (also known as an up-down control) in the
    /// appropriate format.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected override void UpdateEditText()
    {
        base.UpdateEditText();
        if ( this.TextBox is not null && !string.IsNullOrWhiteSpace( this.Postfix ) )
        {
            this.TextBox.AppendText( this.Postfix );
        }
    }

    #endregion

    #region " engineering "

    /// <summary> Builds the engineering scale to scale exponent hash. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> A Dictionary for translating trace events to trace levels. </returns>
    private static Dictionary<int, EngineeringScale> BuildEngineeringUnitHash()
    {
        Dictionary<int, EngineeringScale> dix2 = [];
        Dictionary<int, EngineeringScale> dix3 = dix2;
        dix3.Add( -18, EngineeringScale.Atto );
        dix3.Add( 18, EngineeringScale.Exa );
        dix3.Add( 15, EngineeringScale.Femto );
        dix3.Add( -9, EngineeringScale.Giga );
        dix3.Add( -3, EngineeringScale.Kilo );
        dix3.Add( -2, EngineeringScale.Deci );
        dix3.Add( -6, EngineeringScale.Mega );
        dix3.Add( 6, EngineeringScale.Micro );
        dix3.Add( 2, EngineeringScale.Percent );
        dix3.Add( 3, EngineeringScale.Milli );
        dix3.Add( 9, EngineeringScale.Nano );
        dix3.Add( -14, EngineeringScale.Peta );
        dix3.Add( 12, EngineeringScale.Pico );
        dix3.Add( -12, EngineeringScale.Tera );
        dix3.Add( 0, EngineeringScale.Unity );
        return dix2;
    }

    /// <summary> Builds the engineering scale to scale exponent hash. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> A Dictionary for translating trace events to trace levels. </returns>
    private static Dictionary<EngineeringScale, int> BuildScaleExponentHash()
    {
        Dictionary<EngineeringScale, int> dix2 = [];
        Dictionary<EngineeringScale, int> dix3 = dix2;
        dix3.Add( EngineeringScale.Atto, -18 );
        dix3.Add( EngineeringScale.Exa, 18 );
        dix3.Add( EngineeringScale.Femto, 15 );
        dix3.Add( EngineeringScale.Giga, -9 );
        dix3.Add( EngineeringScale.Deci, -2 );
        dix3.Add( EngineeringScale.Kilo, -3 );
        dix3.Add( EngineeringScale.Mega, -6 );
        dix3.Add( EngineeringScale.Micro, 6 );
        dix3.Add( EngineeringScale.Milli, 3 );
        dix3.Add( EngineeringScale.Nano, 9 );
        dix3.Add( EngineeringScale.Percent, 2 );
        dix3.Add( EngineeringScale.Peta, -15 );
        dix3.Add( EngineeringScale.Pico, 12 );
        dix3.Add( EngineeringScale.Tera, -12 );
        dix3.Add( EngineeringScale.Unity, 0 );
        return dix2;
    }

    private EngineeringScale _engineeringScale;

    /// <summary> Gets or sets the engineering scale. </summary>
    /// <value> The engineering scale. </value>
    [DefaultValue( 0 )]
    [Category( "Behavior" )]
    [Description( "The engineering scale." )]
    public EngineeringScale EngineeringScale
    {
        get => this._engineeringScale;
        set => this.UpdateEngineeringScale( value );
    }

    private static IDictionary<EngineeringScale, int>? _scaleExponentHash;

    /// <summary> Gets the scale exponent hash. </summary>
    /// <value> The scale exponent hash. </value>
    private static IDictionary<EngineeringScale, int> ScaleExponentHash
    {
        get
        {
            _scaleExponentHash ??= BuildScaleExponentHash();
            return _scaleExponentHash;
        }
    }

    /// <summary> Updates the engineering scale described by value. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    private void UpdateEngineeringScale( EngineeringScale value )
    {
        this.UpdateScalingExponent( ScaleExponentHash[value] );
    }

    private static IDictionary<int, string>? _scaleValuePrefixHash;

    /// <summary> Gets the scale value prefix hash. </summary>
    /// <value> The scale value prefix hash. </value>
    private static IDictionary<int, string> ScaleValuePrefixHash
    {
        get
        {
            _scaleValuePrefixHash ??= BuildScaleValuePrefixHash();
            return _scaleValuePrefixHash;
        }
    }

    private static IDictionary<int, EngineeringScale>? _engineeringScalesHash;

    /// <summary> Gets the engineering scales hash. </summary>
    /// <value> The engineering scales hash. </value>
    private static IDictionary<int, EngineeringScale> EngineeringScalesHash
    {
        get
        {
            _engineeringScalesHash ??= BuildEngineeringUnitHash();
            return _engineeringScalesHash;
        }
    }

    /// <summary> Initializes the scale properties. </summary>
    /// <remarks> Percent units are treated as a special case. </remarks>
    /// <param name="value"> The value. </param>
    private void UpdateScalingExponent( int value )
    {
        // treat percent units as a special case
        if ( value is 2 or (-2) )
            this._scalingExponent = 2;
        else
            // must be in powers of 3.
            this._scalingExponent = 3 * (value / 3);

        this.ScaleFactor = ( decimal ) Math.Pow( 10d, -this._scalingExponent );
        this.Postfix = value == 0 && string.IsNullOrWhiteSpace( this.Unit ) ? string.Empty : $" {ScaleValuePrefixHash[value]}{this.Unit}";
        this._engineeringScale = EngineeringScalesHash[this.ScalingExponent];
        // update the display to show the change in units, if any.
        this.UpdateEditText();
        this.OnValueChanged( EventArgs.Empty );
    }

    /// <summary> Builds a scale exponent to unit prefix hash. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> A Dictionary for translating exponents to units. </returns>
    private static Dictionary<int, string> BuildScaleValuePrefixHash()
    {
        Dictionary<int, string> dix2 = [];
        Dictionary<int, string> dix3 = dix2;
        dix3.Add( -18, "E" ); // Exa
        dix3.Add( -15, "P" ); // Peta
        dix3.Add( -12, "T" ); // Tera
        dix3.Add( -9, "G" );  // Giga
        dix3.Add( -6, "M" );  // Mega
        dix3.Add( -2, "D" );  // Deci
        dix3.Add( -3, "K" );  // Kilo
        dix3.Add( 0, "" );
        dix3.Add( 2, "%" );  // Percent
        dix3.Add( 3, "m" );  // Milli
        dix3.Add( 6, "u" );  // Micro
        dix3.Add( 9, "n" );  // Nano
        dix3.Add( 12, "p" ); // Pico
        dix3.Add( 15, "f" ); // Femto
        dix3.Add( 18, "a" ); // Atto
        return dix2;
    }

    private string _unit;

    /// <summary> The engineering scale to display, e.g., V, A. </summary>
    /// <value> The unit. </value>
    [DefaultValue( "" )]
    [Category( "Appearance" )]
    [Description( "The engineering scale to display, e.g., V, A." )]
    public string Unit
    {
        get => this._unit;
        set
        {
            this._unit = value;
            // update the post fix.
            this.UpdateScalingExponent( this.ScalingExponent );
        }
    }

    /// <summary> Gets or sets the unscaled value. </summary>
    /// <value> The unscaled value. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public decimal UnscaledValue
    {
        get
        {
            if ( !decimal.TryParse( this.StripedText(), out decimal value ) )
            {
                value = this.Value;
            }

            return value;
        }

        set => this.Value = value;
    }

    /// <summary> Gets the scaled sentinel. </summary>
    /// <value> <c>true</c> if scaled. </value>
    private bool IsScaled => this.ScaleFactor is not 1m and not 0m;

    /// <summary> Gets or sets the scaled value. </summary>
    /// <value> The scaled value. </value>
    [DefaultValue( 0 )]
    [Category( "Behavior" )]
    [Description( "The scaled value." )]
    public decimal ScaledValue
    {
        get => this.IsScaled ? this.ScaleFactor * this.UnscaledValue : this.UnscaledValue;
        set
        {
            if ( this.IsScaled )
            {
                value /= this.ScaleFactor;
            }

            if ( this.TextBox is not null && !string.IsNullOrWhiteSpace( this.Postfix ) )
            {
                this.TextBox.Text = value.ToString( System.Globalization.CultureInfo.CurrentCulture );
            }

            this.UnscaledValue = value;
            this.TextBox?.AppendText( this.Postfix );
        }
    }

    private int _scalingExponent;

    /// <summary> Gets or sets the scaling exponent. </summary>
    /// <value> The scaling exponent. </value>
    protected int ScalingExponent
    {
        get => this._scalingExponent;
        set => this.UpdateScalingExponent( value );
    }

    /// <summary> Gets or sets the scale factor. </summary>
    /// <value> The scale factor. </value>
    protected decimal ScaleFactor { get; set; }

    /// <summary> Gets or sets the text to append to the display value. </summary>
    /// <value> The post fix. </value>
    protected string Postfix { get; set; }

    #endregion
}
/// <summary> Values that represent engineering scale. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum EngineeringScale
{
    /// <summary> An enum constant representing the unity option. </summary>
    [Description( "Unity (0)" )]
    Unity = 0,

    /// <summary> An enum constant representing the Atto option. </summary>
    [Description( "Atto (a:-18)" )]
    Atto,

    /// <summary> An enum constant representing the Femto option. </summary>
    [Description( "Femto (f:-15)" )]
    Femto,

    /// <summary> An enum constant representing the Pico option. </summary>
    [Description( "Pico (p:-12)" )]
    Pico,

    /// <summary> An enum constant representing the nano option. </summary>
    [Description( "Nano (n:-9)" )]
    Nano,

    /// <summary> An enum constant representing the micro option. </summary>
    [Description( "Micro (u:-6)" )]
    Micro,

    /// <summary> An enum constant representing the milli option. </summary>
    [Description( "Milli (m:-3)" )]
    Milli,

    /// <summary> An enum constant representing the Deci option. </summary>
    [Description( "Deci (D:2)" )]
    Deci,

    /// <summary> An enum constant representing the percent option. </summary>
    [Description( "Percent (D:-2)" )]
    Percent,

    /// <summary> An enum constant representing the kilo option. </summary>
    [Description( "Kilo (K:3)" )]
    Kilo,

    /// <summary> An enum constant representing the mega option. </summary>
    [Description( "Mega (M:6)" )]
    Mega,

    /// <summary> An enum constant representing the giga option. </summary>
    [Description( "Giga (G:9)" )]
    Giga,

    /// <summary> An enum constant representing the tera option. </summary>
    [Description( "Tera (T:12)" )]
    Tera,

    /// <summary> An enum constant representing the Peta option. </summary>
    [Description( "Peta (P:15)" )]
    Peta,

    /// <summary> An enum constant representing the exa option. </summary>
    [Description( "Exa (E:18)" )]
    Exa
}
