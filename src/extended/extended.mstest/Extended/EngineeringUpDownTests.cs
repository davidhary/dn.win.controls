namespace cc.isr.WinControls.Extended.MSTest;

/// <summary>   (Unit Test Class) an engineering up down tests. </summary>
/// <remarks>   David, 2020-09-23. </remarks>
[TestClass]
public class EngineeringUpDownTests
{
    /// <summary>   (Unit Test Method) engineering up down scale default should be unity. </summary>
    /// <remarks>   David, 2021-03-11. </remarks>
    [TestMethod()]
    public void EngineeringUpDownScaleDefaultShouldBeUnity()
    {
        using EngineeringUpDown target = new();
        EngineeringScale expected = EngineeringScale.Unity;
        EngineeringScale actual = target.EngineeringScale;
        Assert.AreEqual( expected, actual, $"{nameof( EngineeringUpDown.EngineeringScale )} should match" );
    }
}
