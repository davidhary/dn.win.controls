namespace cc.isr.WinControls.Demo;

/// <summary>   Form for viewing the nullable numeric up down. </summary>
/// <remarks>   David, 2020-10-25. </remarks>
public partial class NullableNumericUpDownForm
{
    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public NullableNumericUpDownForm() => this.InitializeComponent();

    /// <summary>   Event handler. Called by NullableNumericUpDownForm for shown events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void NullableNumericUpDownForm_Shown( object? sender, EventArgs e )
    {
        this._timer.Interval = 1000;
        this._timer.Enabled = true;
    }

    /// <summary>   Nullable numeric up down validated. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void NullableNumericUpDown_Validated( object? sender, EventArgs e )
    {
#pragma warning disable CA1305
        this._validatedNullableValueTextBox.Text = this._nullableNumericUpDown.NullableValue.ToString();
#pragma warning restore CA1305
        this._validatedValueTextBox.Text = this._nullableNumericUpDown.Value.ToString( System.Globalization.CultureInfo.CurrentCulture );
        this._nullableTextTextBox.Text = this._nullableNumericUpDown.Text;
    }

    /// <summary>   Nullable numeric up down value changed. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void NullableNumericUpDown_ValueChanged( object? sender, EventArgs e )
    {
#pragma warning disable CA1305
        this._changedNullableValueTextBox.Text = this._nullableNumericUpDown.NullableValue.ToString();
#pragma warning restore CA1305
        this._changedValueTextBox.Text = this._nullableNumericUpDown.Value.ToString( System.Globalization.CultureInfo.CurrentCulture );
        this._nullableTextTextBox.Text = this._nullableNumericUpDown.Text;
    }

    /// <summary>   Nullable numeric up down value decrementing. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Cancel event information. </param>
    private void NullableNumericUpDown_ValueDecrementing( object? sender, System.ComponentModel.CancelEventArgs e )
    {
#pragma warning disable CA1305
        this._spinningNullableValueTextBox.Text = this._nullableNumericUpDown.NullableValue.ToString();
#pragma warning restore CA1305
        this._spinningValueTextBox.Text = this._nullableNumericUpDown.Value.ToString( System.Globalization.CultureInfo.CurrentCulture );
        this._nullableTextTextBox.Text = this._nullableNumericUpDown.Text;
    }

    /// <summary>   Nullable numeric up down value incrementing. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Cancel event information. </param>
    private void NullableNumericUpDown_ValueIncrementing( object? sender, System.ComponentModel.CancelEventArgs e )
    {
#pragma warning disable CA1305
        this._spinningNullableValueTextBox.Text = this._nullableNumericUpDown.NullableValue.ToString();
#pragma warning restore CA1305
        this._spinningValueTextBox.Text = this._nullableNumericUpDown.Value.ToString( System.Globalization.CultureInfo.CurrentCulture );
        this._nullableTextTextBox.Text = this._nullableNumericUpDown.Text;
    }

    /// <summary>   Nullify button click. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void NullifyButton_Click( object? sender, EventArgs e )
    {
        this._nullableNumericUpDown.NullableValue = new decimal?();
        _ = this._nullableNumericUpDown.Validate();
        Application.DoEvents();
    }

    /// <summary>   Timer tick. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Timer_Tick( object? sender, EventArgs e )
    {
        this._nullableValueTextBox.Text = $"{this._nullableNumericUpDown.NullableValue}";
    }
}
