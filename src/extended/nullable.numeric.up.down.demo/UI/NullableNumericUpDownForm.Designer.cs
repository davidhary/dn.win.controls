using System.Diagnostics;

namespace cc.isr.WinControls.Demo;

public partial class NullableNumericUpDownForm : Form
{
    /// <summary>   Form overrides dispose to clean up the component list. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
    ///                             resources; <see langword="false" /> to release only unmanaged
    ///                             resources. </param>
    [DebuggerNonUserCode()]
    protected override void Dispose(bool disposing)
    {
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose(disposing);
        }
    }

    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        _valueTextBoxLabel = new Label();
        _spinningValueTextBox = new TextBox();
        _changedValueTextBoxLabel = new Label();
        _changedValueTextBox = new TextBox();
        _validatedValueTextBoxLabel = new Label();
        _validatedValueTextBox = new TextBox();
        _spinningNullableValueTextBox = new TextBox();
        _validatedNullableValueTextBox = new TextBox();
        _changedNullableValueTextBox = new TextBox();
        Label1 = new Label();
        _nullableNumericUpDownLabel = new Label();
        _nullableNumericUpDown = new cc.isr.WinControls.NullableNumericUpDown();
        _nullableTextTextBoxLabel = new Label();
        _nullableTextTextBox = new TextBox();
        _nullifyButton = new Button();
        _nullifyButton.Click += new EventHandler(NullifyButton_Click);
        _nullableValueTextBox = new TextBox();
        _timer = new System.Windows.Forms.Timer(components);
        _timer.Tick += new EventHandler(Timer_Tick);
        ((System.ComponentModel.ISupportInitialize)_nullableNumericUpDown).BeginInit();
        SuspendLayout();
        // 
        // _valueTextBoxLabel
        // 
        _valueTextBoxLabel.AutoSize = true;
        _valueTextBoxLabel.Location = new Point(40, 75);
        _valueTextBoxLabel.Name = "_ValueTextBoxLabel";
        _valueTextBoxLabel.Size = new Size(51, 13);
        _valueTextBoxLabel.TabIndex = 4;
        _valueTextBoxLabel.Text = "Spinning:";
        _valueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
        // 
        // _spinningValueTextBox
        // 
        _spinningValueTextBox.Location = new Point(93, 72);
        _spinningValueTextBox.Name = "_SpinningValueTextBox";
        _spinningValueTextBox.Size = new Size(100, 20);
        _spinningValueTextBox.TabIndex = 5;
        // 
        // _changedValueTextBoxLabel
        // 
        _changedValueTextBoxLabel.AutoSize = true;
        _changedValueTextBoxLabel.Location = new Point(38, 113);
        _changedValueTextBoxLabel.Name = "_ChangedValueTextBoxLabel";
        _changedValueTextBoxLabel.Size = new Size(53, 13);
        _changedValueTextBoxLabel.TabIndex = 7;
        _changedValueTextBoxLabel.Text = "Changed:";
        _changedValueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
        // 
        // _changedValueTextBox
        // 
        _changedValueTextBox.Location = new Point(93, 110);
        _changedValueTextBox.Name = "_ChangedValueTextBox";
        _changedValueTextBox.Size = new Size(100, 20);
        _changedValueTextBox.TabIndex = 8;
        // 
        // _validatedValueTextBoxLabel
        // 
        _validatedValueTextBoxLabel.AutoSize = true;
        _validatedValueTextBoxLabel.Location = new Point(37, 152);
        _validatedValueTextBoxLabel.Name = "_ValidatedValueTextBoxLabel";
        _validatedValueTextBoxLabel.Size = new Size(54, 13);
        _validatedValueTextBoxLabel.TabIndex = 10;
        _validatedValueTextBoxLabel.Text = "Validated:";
        _validatedValueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
        // 
        // _validatedValueTextBox
        // 
        _validatedValueTextBox.Location = new Point(93, 149);
        _validatedValueTextBox.Name = "_ValidatedValueTextBox";
        _validatedValueTextBox.Size = new Size(100, 20);
        _validatedValueTextBox.TabIndex = 11;
        // 
        // _spinningNullableValueTextBox
        // 
        _spinningNullableValueTextBox.Location = new Point(199, 72);
        _spinningNullableValueTextBox.Name = "_SpinningNullableValueTextBox";
        _spinningNullableValueTextBox.Size = new Size(100, 20);
        _spinningNullableValueTextBox.TabIndex = 6;
        // 
        // _validatedNullableValueTextBox
        // 
        _validatedNullableValueTextBox.Location = new Point(199, 149);
        _validatedNullableValueTextBox.Name = "_ValidatedNullableValueTextBox";
        _validatedNullableValueTextBox.Size = new Size(100, 20);
        _validatedNullableValueTextBox.TabIndex = 12;
        // 
        // _changedNullableValueTextBox
        // 
        _changedNullableValueTextBox.Location = new Point(199, 110);
        _changedNullableValueTextBox.Name = "_ChangedNullableValueTextBox";
        _changedNullableValueTextBox.Size = new Size(100, 20);
        _changedNullableValueTextBox.TabIndex = 9;
        // 
        // Label1
        // 
        Label1.AutoSize = true;
        Label1.Location = new Point(97, 56);
        Label1.Name = "Label1";
        Label1.Size = new Size(37, 13);
        Label1.TabIndex = 2;
        Label1.Text = "Value:";
        Label1.TextAlign = ContentAlignment.MiddleLeft;
        // 
        // _nullableNumericUpDownLabel
        // 
        _nullableNumericUpDownLabel.AutoSize = true;
        _nullableNumericUpDownLabel.Location = new Point(196, 56);
        _nullableNumericUpDownLabel.Name = "_NullableNumericUpDownLabel";
        _nullableNumericUpDownLabel.Size = new Size(78, 13);
        _nullableNumericUpDownLabel.TabIndex = 3;
        _nullableNumericUpDownLabel.Text = "Nullable Value:";
        _nullableNumericUpDownLabel.TextAlign = ContentAlignment.MiddleLeft;
        // 
        // _nullableNumericUpDown
        // 
        _nullableNumericUpDown.Location = new Point(49, 16);
        _nullableNumericUpDown.Maximum = new decimal(new int[] { 100000, 0, 0, 0 });
        _nullableNumericUpDown.Name = "_NullableNumericUpDown";
        _nullableNumericUpDown.ReadOnlyBackColor = Color.Empty;
        _nullableNumericUpDown.ReadOnlyForeColor = Color.Empty;
        _nullableNumericUpDown.ReadWriteBackColor = Color.Empty;
        _nullableNumericUpDown.ReadWriteForeColor = Color.Empty;
        _nullableNumericUpDown.Size = new Size(145, 20);
        _nullableNumericUpDown.TabIndex = 0;
        _nullableNumericUpDown.Validated += new EventHandler( NullableNumericUpDown_Validated );
        _nullableNumericUpDown.ValueChanged += new EventHandler( NullableNumericUpDown_ValueChanged );
        _nullableNumericUpDown.ValueDecrementing += new EventHandler<System.ComponentModel.CancelEventArgs>( NullableNumericUpDown_ValueDecrementing );
        _nullableNumericUpDown.ValueIncrementing += new EventHandler<System.ComponentModel.CancelEventArgs>( NullableNumericUpDown_ValueIncrementing );
        // 
        // _nullableTextTextBoxLabel
        // 
        _nullableTextTextBoxLabel.AutoSize = true;
        _nullableTextTextBoxLabel.Location = new Point(164, 186);
        _nullableTextTextBoxLabel.Name = "_NullableTextTextBoxLabel";
        _nullableTextTextBoxLabel.Size = new Size(31, 13);
        _nullableTextTextBoxLabel.TabIndex = 13;
        _nullableTextTextBoxLabel.Text = "Text:";
        _nullableTextTextBoxLabel.TextAlign = ContentAlignment.TopRight;
        // 
        // _nullableTextTextBox
        // 
        _nullableTextTextBox.Location = new Point(198, 183);
        _nullableTextTextBox.Name = "_NullableTextTextBox";
        _nullableTextTextBox.Size = new Size(100, 20);
        _nullableTextTextBox.TabIndex = 14;
        // 
        // _nullifyButton
        // 
        _nullifyButton.Location = new Point(223, 16);
        _nullifyButton.Name = "_NullifyButton";
        _nullifyButton.Size = new Size(75, 23);
        _nullifyButton.TabIndex = 1;
        _nullifyButton.Text = "Nullify";
        _nullifyButton.UseVisualStyleBackColor = true;
        // 
        // _nullableValueTextBox
        // 
        _nullableValueTextBox.Location = new Point(302, 18);
        _nullableValueTextBox.Name = "_NullableValueTextBox";
        _nullableValueTextBox.Size = new Size(57, 20);
        _nullableValueTextBox.TabIndex = 14;
        // 
        // _timer
        // 
        // 
        // NullableNumericUpDownForm
        // 
        AutoScaleDimensions = new SizeF(6.0f, 13.0f);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(371, 261);
        Controls.Add(_nullifyButton);
        Controls.Add(_nullableValueTextBox);
        Controls.Add(_nullableTextTextBox);
        Controls.Add(_validatedValueTextBox);
        Controls.Add(_nullableTextTextBoxLabel);
        Controls.Add(_validatedValueTextBoxLabel);
        Controls.Add(_changedNullableValueTextBox);
        Controls.Add(_changedValueTextBox);
        Controls.Add(_changedValueTextBoxLabel);
        Controls.Add(_validatedNullableValueTextBox);
        Controls.Add(_spinningNullableValueTextBox);
        Controls.Add(_spinningValueTextBox);
        Controls.Add(_nullableNumericUpDownLabel);
        Controls.Add(Label1);
        Controls.Add(_valueTextBoxLabel);
        Controls.Add(_nullableNumericUpDown);
        Name = "NullableNumericUpDownForm";
        Text = "Nullable Numeric Up Down Form";
        ((System.ComponentModel.ISupportInitialize)_nullableNumericUpDown).EndInit();
        Shown += new EventHandler(NullableNumericUpDownForm_Shown);
        ResumeLayout(false);
        PerformLayout();
    }

    private NullableNumericUpDown _nullableNumericUpDown;
    private Label _valueTextBoxLabel;
    private TextBox _spinningValueTextBox;
    private Label _changedValueTextBoxLabel;
    private TextBox _changedValueTextBox;
    private Label _validatedValueTextBoxLabel;
    private TextBox _validatedValueTextBox;
    private TextBox _spinningNullableValueTextBox;
    private TextBox _validatedNullableValueTextBox;
    private TextBox _changedNullableValueTextBox;
    private Label Label1;
    private Label _nullableNumericUpDownLabel;
    private Label _nullableTextTextBoxLabel;
    private TextBox _nullableTextTextBox;
    private Button _nullifyButton;
    private TextBox _nullableValueTextBox;
    private System.Windows.Forms.Timer _timer;
}
