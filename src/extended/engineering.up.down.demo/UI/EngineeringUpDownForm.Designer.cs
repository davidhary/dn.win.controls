using System.Diagnostics;

namespace cc.isr.WinControls.Demo;

public partial class EngineeringUpDownForm : Form
{
    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="Form" />.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
    ///                             resources; <see langword="false" /> to release only unmanaged
    ///                             resources. </param>
    [DebuggerNonUserCode()]
    protected override void Dispose(bool disposing)
    {
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose(disposing);
        }
    }

    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        _decimalPlacesNumericLabel = new Label();
        _decimalPlacesNumeric = new NumericUpDown();
        _decimalPlacesNumeric.ValueChanged += new EventHandler(DecimalPlacesNumeric_ValueChanged);
        _engineeringScaleComboBox = new ComboBox();
        _engineeringScaleComboBox.SelectionChangeCommitted += new EventHandler(EngineeringScaleComboBox_SelectionChangeCommitted);
        _engineeringScaleComboBoxLabel = new Label();
        _unitTextBoxLabel = new Label();
        _unitTextBox = new TextBox();
        _unitTextBox.TextChanged += new EventHandler(UnitTextBox_TextChanged);
        _toolTip = new ToolTip(components);
        _scaledValueTextBox = new TextBox();
        _enterValueNumeric = new cc.isr.WinControls.NumericUpDown();
        _enterValueNumeric.ValueChanged += new EventHandler(EnterValueNumeric_ValueChanged);
        _readOnlyCheckBox = new CheckBox();
        _readOnlyCheckBox.CheckedChanged += new EventHandler(ReadOnlyCheckBox_CheckedChanged);
        _editedEngineeringUpDown = new cc.isr.WinControls.EngineeringUpDown();
        _editedEngineeringUpDown.ValueChanged += new EventHandler(EditedEngineeringUpDown_ValueChanged);
        _enterValueNumericLabel = new Label();
        _editedEngineeringUpDownLabel = new Label();
        _scaledValueTextBoxLabel = new Label();
        _numericUpDown = new cc.isr.WinControls.NumericUpDown();
        _numericUpDown.ValueChanged += new EventHandler(EnterValueNumeric_ValueChanged);
        ((System.ComponentModel.ISupportInitialize)_decimalPlacesNumeric).BeginInit();
        ((System.ComponentModel.ISupportInitialize)_enterValueNumeric).BeginInit();
        ((System.ComponentModel.ISupportInitialize)_editedEngineeringUpDown).BeginInit();
        ((System.ComponentModel.ISupportInitialize)_numericUpDown).BeginInit();
        SuspendLayout();
        // 
        // _decimalPlacesNumericLabel
        // 
        _decimalPlacesNumericLabel.AutoSize = true;
        _decimalPlacesNumericLabel.Location = new Point(48, 191);
        _decimalPlacesNumericLabel.Name = "_DecimalPlacesNumericLabel";
        _decimalPlacesNumericLabel.Size = new Size(97, 17);
        _decimalPlacesNumericLabel.TabIndex = 1;
        _decimalPlacesNumericLabel.Text = "Decimal Places:";
        _decimalPlacesNumericLabel.TextAlign = ContentAlignment.TopRight;
        // 
        // _decimalPlacesNumeric
        // 
        _decimalPlacesNumeric.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _decimalPlacesNumeric.Location = new Point(146, 188);
        _decimalPlacesNumeric.Maximum = new decimal(new int[] { 6, 0, 0, 0 });
        _decimalPlacesNumeric.Name = "_DecimalPlacesNumeric";
        _decimalPlacesNumeric.Size = new Size(58, 25);
        _decimalPlacesNumeric.TabIndex = 2;
        _toolTip.SetToolTip(_decimalPlacesNumeric, "Enter decimal places");
        // 
        // _engineeringScaleComboBox
        // 
        _engineeringScaleComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        _engineeringScaleComboBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _engineeringScaleComboBox.FormattingEnabled = true;
        _engineeringScaleComboBox.Location = new Point(147, 150);
        _engineeringScaleComboBox.Name = "_EngineeringScaleComboBox";
        _engineeringScaleComboBox.Size = new Size(121, 25);
        _engineeringScaleComboBox.TabIndex = 3;
        _toolTip.SetToolTip(_engineeringScaleComboBox, "Select engineering scale");
        // 
        // _engineeringScaleComboBoxLabel
        // 
        _engineeringScaleComboBoxLabel.AutoSize = true;
        _engineeringScaleComboBoxLabel.Location = new Point(31, 153);
        _engineeringScaleComboBoxLabel.Name = "_EngineeringScaleComboBoxLabel";
        _engineeringScaleComboBoxLabel.Size = new Size(114, 17);
        _engineeringScaleComboBoxLabel.TabIndex = 1;
        _engineeringScaleComboBoxLabel.Text = "Engineering Scale:";
        _engineeringScaleComboBoxLabel.TextAlign = ContentAlignment.TopRight;
        // 
        // _unitTextBoxLabel
        // 
        _unitTextBoxLabel.AutoSize = true;
        _unitTextBoxLabel.Location = new Point(111, 118);
        _unitTextBoxLabel.Name = "_UnitTextBoxLabel";
        _unitTextBoxLabel.Size = new Size(34, 17);
        _unitTextBoxLabel.TabIndex = 1;
        _unitTextBoxLabel.Text = "Unit:";
        _unitTextBoxLabel.TextAlign = ContentAlignment.TopRight;
        // 
        // _unitTextBox
        // 
        _unitTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _unitTextBox.Location = new Point(147, 114);
        _unitTextBox.Name = "_UnitTextBox";
        _unitTextBox.Size = new Size(58, 25);
        _unitTextBox.TabIndex = 5;
        _toolTip.SetToolTip(_unitTextBox, "Enter the unit, e.g., A, V, Ohm.");
        // 
        // _scaledValueTextBox
        // 
        _scaledValueTextBox.Location = new Point(147, 73);
        _scaledValueTextBox.Name = "_ScaledValueTextBox";
        _scaledValueTextBox.Size = new Size(121, 25);
        _scaledValueTextBox.TabIndex = 4;
        _toolTip.SetToolTip(_scaledValueTextBox, "Scaled value");
        // 
        // _enterValueNumeric
        // 
        _enterValueNumeric.Location = new Point(148, 12);
        _enterValueNumeric.Maximum = new decimal(new int[] { 2000000000, 0, 0, 0 });
        _enterValueNumeric.Minimum = new decimal(new int[] { 2000000000, 0, 0, (int)-2147483648L });
        _enterValueNumeric.Name = "_EnterValueNumeric";
        _enterValueNumeric.ReadOnlyBackColor = Color.Empty;
        _enterValueNumeric.ReadOnlyForeColor = Color.Empty;
        _enterValueNumeric.ReadWriteBackColor = Color.Empty;
        _enterValueNumeric.ReadWriteForeColor = Color.Empty;
        _enterValueNumeric.Size = new Size(120, 25);
        _enterValueNumeric.TabIndex = 9;
        _toolTip.SetToolTip(_enterValueNumeric, "Set this value");
        // 
        // _readOnlyCheckBox
        // 
        _readOnlyCheckBox.AutoSize = true;
        _readOnlyCheckBox.Location = new Point(146, 228);
        _readOnlyCheckBox.Name = "_ReadOnlyCheckBox";
        _readOnlyCheckBox.Size = new Size(87, 21);
        _readOnlyCheckBox.TabIndex = 8;
        _readOnlyCheckBox.Text = "Read Only";
        _readOnlyCheckBox.UseVisualStyleBackColor = true;
        // 
        // _editedEngineeringUpDown
        // 
        _editedEngineeringUpDown.ForeColor = Color.Black;
        _editedEngineeringUpDown.Location = new Point(16, 73);
        _editedEngineeringUpDown.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
        _editedEngineeringUpDown.Minimum = new decimal(new int[] { 1000, 0, 0, (int)-2147483648L });
        _editedEngineeringUpDown.Name = "_EditedEngineeringUpDown";
        _editedEngineeringUpDown.ReadOnlyBackColor = Color.FromArgb(224, 224, 224);
        _editedEngineeringUpDown.ReadOnlyForeColor = Color.Black;
        _editedEngineeringUpDown.ReadWriteBackColor = Color.Empty;
        _editedEngineeringUpDown.ReadWriteForeColor = Color.Black;
        _editedEngineeringUpDown.ScaledValue = new decimal(new int[] { 0, 0, 0, 0 });
        _editedEngineeringUpDown.Size = new Size(120, 25);
        _editedEngineeringUpDown.TabIndex = 6;
        _editedEngineeringUpDown.UnscaledValue = new decimal(new int[] { 0, 0, 0, 0 });
        _editedEngineeringUpDown.UpDownCursor = Cursors.Hand;
        _editedEngineeringUpDown.UpDownDisplayMode = cc.isr.WinControls.UpDownButtonsDisplayMode.WhenMouseOver;
        // 
        // _enterValueNumericLabel
        // 
        _enterValueNumericLabel.AutoSize = true;
        _enterValueNumericLabel.Location = new Point(10, 16);
        _enterValueNumericLabel.Name = "_EnterValueNumericLabel";
        _enterValueNumericLabel.Size = new Size(112, 17);
        _enterValueNumericLabel.TabIndex = 1;
        _enterValueNumericLabel.Text = "Enter value to set:";
        _enterValueNumericLabel.TextAlign = ContentAlignment.TopRight;
        // 
        // _editedEngineeringUpDownLabel
        // 
        _editedEngineeringUpDownLabel.AutoSize = true;
        _editedEngineeringUpDownLabel.Location = new Point(13, 53);
        _editedEngineeringUpDownLabel.Name = "_EditedEngineeringUpDownLabel";
        _editedEngineeringUpDownLabel.Size = new Size(69, 17);
        _editedEngineeringUpDownLabel.TabIndex = 1;
        _editedEngineeringUpDownLabel.Text = "Edit Value:";
        _editedEngineeringUpDownLabel.TextAlign = ContentAlignment.TopRight;
        // 
        // _scaledValueTextBoxLabel
        // 
        _scaledValueTextBoxLabel.AutoSize = true;
        _scaledValueTextBoxLabel.Location = new Point(145, 53);
        _scaledValueTextBoxLabel.Name = "_ScaledValueTextBoxLabel";
        _scaledValueTextBoxLabel.Size = new Size(104, 17);
        _scaledValueTextBoxLabel.TabIndex = 1;
        _scaledValueTextBoxLabel.Text = "Observed Value:";
        _scaledValueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
        // 
        // NumericUpDown1
        // 
        _numericUpDown.Location = new Point(12, 116);
        _numericUpDown.Maximum = new decimal(new int[] { 2000000000, 0, 0, 0 });
        _numericUpDown.Minimum = new decimal(new int[] { 2000000000, 0, 0, (int)-2147483648L });
        _numericUpDown.Name = "_NumericUpDown1";
        _numericUpDown.ReadOnlyBackColor = Color.Empty;
        _numericUpDown.ReadOnlyForeColor = Color.Empty;
        _numericUpDown.ReadWriteBackColor = Color.Empty;
        _numericUpDown.ReadWriteForeColor = Color.Empty;
        _numericUpDown.Size = new Size(80, 25);
        _numericUpDown.TabIndex = 9;
        // 
        // EngineeringUpDownForm
        // 
        AutoScaleDimensions = new SizeF(7.0f, 17.0f);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(291, 257);
        Controls.Add(_numericUpDown);
        Controls.Add(_enterValueNumeric);
        Controls.Add(_readOnlyCheckBox);
        Controls.Add(_editedEngineeringUpDown);
        Controls.Add(_scaledValueTextBox);
        Controls.Add(_scaledValueTextBoxLabel);
        Controls.Add(_editedEngineeringUpDownLabel);
        Controls.Add(_enterValueNumericLabel);
        Controls.Add(_engineeringScaleComboBoxLabel);
        Controls.Add(_engineeringScaleComboBox);
        Controls.Add(_decimalPlacesNumericLabel);
        Controls.Add(_decimalPlacesNumeric);
        Controls.Add(_unitTextBoxLabel);
        Controls.Add(_unitTextBox);
        Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        FormBorderStyle = FormBorderStyle.FixedDialog;
        Margin = new Padding(3, 4, 3, 4);
        Name = "EngineeringUpDownForm";
        Text = "Engineering Up Down Form";
        ((System.ComponentModel.ISupportInitialize)_decimalPlacesNumeric).EndInit();
        ((System.ComponentModel.ISupportInitialize)_enterValueNumeric).EndInit();
        ((System.ComponentModel.ISupportInitialize)_editedEngineeringUpDown).EndInit();
        ((System.ComponentModel.ISupportInitialize)_numericUpDown).EndInit();
        ResumeLayout(false);
        PerformLayout();
    }

    private Label _decimalPlacesNumericLabel;
    private NumericUpDown _decimalPlacesNumeric;
    private ComboBox _engineeringScaleComboBox;
    private Label _engineeringScaleComboBoxLabel;
    private ToolTip _toolTip;
    private Label _unitTextBoxLabel;
    private TextBox _unitTextBox;
    private EngineeringUpDown _editedEngineeringUpDown;
    private TextBox _scaledValueTextBox;
    private CheckBox _readOnlyCheckBox;
    private cc.isr.WinControls.NumericUpDown _enterValueNumeric;
    private Label _enterValueNumericLabel;
    private Label _editedEngineeringUpDownLabel;
    private Label _scaledValueTextBoxLabel;
    private cc.isr.WinControls.NumericUpDown _numericUpDown;
}
