using cc.isr.Enums;

namespace cc.isr.WinControls.Demo;

/// <summary>   Form for viewing the engineering up down. </summary>
/// <remarks>
/// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-04-05 </para>
/// </remarks>
public partial class EngineeringUpDownForm
{
    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public EngineeringUpDownForm() => this.InitializeComponent();

    /// <summary>   Raises the <see cref="Form.Load" /> event. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="e">    An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnLoad( EventArgs e )
    {
        try
        {
            this._engineeringScaleComboBox.DataSource = null;
            this._engineeringScaleComboBox.Items.Clear();
            this._engineeringScaleComboBox.DataSource = typeof( EngineeringScale ).ValueDescriptionPairs().ToBindingList();
            this._engineeringScaleComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._engineeringScaleComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
        }
        catch
        {
            throw;
        }
        finally
        {
            base.OnLoad( e );
        }
    }

    /// <summary>   Enter value numeric value changed. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void EnterValueNumeric_ValueChanged( object? sender, EventArgs e )
    {
        this._editedEngineeringUpDown.ScaledValue = this._enterValueNumeric.Value;
        this._numericUpDown.Value = this._enterValueNumeric.Value;
    }

    /// <summary>   Engineering scale combo box selection change committed. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void EngineeringScaleComboBox_SelectionChangeCommitted( object? sender, EventArgs e )
    {
        if ( this._engineeringScaleComboBox.SelectedItem is not null )
        {
            this._editedEngineeringUpDown.EngineeringScale = ( EngineeringScale ) Convert.ToInt32( (( KeyValuePair<Enum, string> ) this._engineeringScaleComboBox.SelectedItem).Key, System.Globalization.CultureInfo.CurrentCulture );
        }
    }

    /// <summary>   Edited engineering up down value changed. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void EditedEngineeringUpDown_ValueChanged( object? sender, EventArgs e )
    {
        this._scaledValueTextBox.Text = this._editedEngineeringUpDown.ScaledValue.ToString( "0.#########E-00 " + this._unitTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
    }

    /// <summary>   Decimal places numeric value changed. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void DecimalPlacesNumeric_ValueChanged( object? sender, EventArgs e )
    {
        this._editedEngineeringUpDown.DecimalPlaces = ( int ) Math.Round( this._decimalPlacesNumeric.Value );
    }

    /// <summary>   Unit text box text changed. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void UnitTextBox_TextChanged( object? sender, EventArgs e )
    {
        this._editedEngineeringUpDown.Unit = this._unitTextBox.Text;
    }

    /// <summary>   Reads only check box checked changed. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ReadOnlyCheckBox_CheckedChanged( object? sender, EventArgs e )
    {
        this._editedEngineeringUpDown.ReadOnly = this._readOnlyCheckBox.Checked;
    }

}
