namespace cc.isr.WinControls.Demo;

/// <summary>   Form for viewing the number box. </summary>
/// <remarks>   David, 2020-04-08. </remarks>
public partial class NumberBoxForm : Form
{
    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    public NumberBoxForm() => this.InitializeComponent();

    /// <summary>   Event handler. Called by NumberBox for leave events. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void NumberBox_Leave( object? sender, EventArgs e )
    {
        if ( sender is not NumberBox nb ) return;

        foreach ( object? c in this.panel2.Controls )
        {
            if ( c.GetType() == typeof( NumberBox ) )
            {
                NumberBox box = ( NumberBox ) c;
                if ( box.NumberFormatString.StartsWith( "D", StringComparison.Ordinal ) )
                {
                    box.ValueAsInt64 = nb.ValueAsInt64;
                }

                if ( box.NumberFormatString.StartsWith( "F", StringComparison.Ordinal ) )
                {
                    box.ValueAsFloat = nb.ValueAsFloat;
                }

                if ( box.NumberFormatString.StartsWith( "E", StringComparison.Ordinal ) )
                {
                    box.ValueAsDouble = nb.ValueAsDouble;
                }

                if ( box.NumberFormatString.StartsWith( "N", StringComparison.Ordinal ) )
                {
                    box.ValueAsDouble = nb.ValueAsDouble;
                }

                if ( box.NumberFormatString.StartsWith( "X", StringComparison.Ordinal ) )
                {
                    box.ValueAsInt64 = nb.ValueAsInt64;
                }

                if ( box.NumberFormatString.StartsWith( "B", StringComparison.Ordinal ) )
                {
                    box.ValueAsByte = nb.ValueAsByte;
                }
            }
        }
    }
}
