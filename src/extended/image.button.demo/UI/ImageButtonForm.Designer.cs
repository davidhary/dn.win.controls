namespace cc.isr.WinControls.Demo;

public partial class ImageButtonForm
{
    /// <summary>
/// Required designer variable.
/// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
/// Clean up any resources being used.
/// </summary>
/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if ( disposing )
        {
            components?.Dispose();
        }

        base.Dispose(disposing);
    }

    /// <summary>
    /// Required method for Designer support - do not modify the contents of this method with the
    /// code editor.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    private void InitializeComponent()
    {
        ImageButton1 = new WinControls.ImageButton();
        ImageButton1.Click += new EventHandler(ImageButton1_Click);
        ImageButton3 = new WinControls.ImageButton();
        ImageButton3.Click += new EventHandler(ImageButton3_Click);
        ImageButton2 = new WinControls.ImageButton();
        ImageButton2.Click += new EventHandler(ImageButton2_Click);
        label1 = new Label();
        ImageButton4 = new WinControls.ImageButton();
        ImageButton4.Click += new EventHandler(ImageButton4_Click);
        ImageButton5 = new WinControls.ImageButton();
        ImageButton5.Click += new EventHandler(ImageButton5_Click);
        ImageButton6 = new WinControls.ImageButton();
        ImageButton6.Click += new EventHandler(ImageButton6_Click);
        ImageButton7 = new WinControls.ImageButton();
        ImageButton7.Click += new EventHandler(ImageButton7_Click);
        label2 = new Label();
        TraceMessageToolStrip1 = new TextBox();
        ((System.ComponentModel.ISupportInitialize)ImageButton1).BeginInit();
        ((System.ComponentModel.ISupportInitialize)ImageButton3).BeginInit();
        ((System.ComponentModel.ISupportInitialize)ImageButton2).BeginInit();
        ((System.ComponentModel.ISupportInitialize)ImageButton4).BeginInit();
        ((System.ComponentModel.ISupportInitialize)ImageButton5).BeginInit();
        ((System.ComponentModel.ISupportInitialize)ImageButton6).BeginInit();
        ((System.ComponentModel.ISupportInitialize)ImageButton7).BeginInit();
        SuspendLayout();
        // 
        // ImageButton1
        // 
        ImageButton1.DialogResult = DialogResult.None;
        ImageButton1.DownImage = Properties.Resources.ExampleButtonDown;
        ImageButton1.HoverImage = Properties.Resources.ExampleButtonHover;
        ImageButton1.Location = new Point(61, 61);
        ImageButton1.Margin = new Padding(3, 4, 3, 4);
        ImageButton1.Name = "ImageButton1";
        ImageButton1.NormalImage = Properties.Resources.ExampleButton;
        ImageButton1.Size = new Size(100, 50);
        ImageButton1.SizeMode = PictureBoxSizeMode.AutoSize;
        ImageButton1.TabIndex = 0;
        ImageButton1.TabStop = false;
        // 
        // ImageButton3
        // 
        ImageButton3.DialogResult = DialogResult.None;
        ImageButton3.DownImage = Properties.Resources.CUncheckedDown;
        ImageButton3.HoverImage = Properties.Resources.CUncheckedHover;
        ImageButton3.Location = new Point(105, 154);
        ImageButton3.Margin = new Padding(3, 4, 3, 4);
        ImageButton3.Name = "ImageButton3";
        ImageButton3.NormalImage = Properties.Resources.CUncheckedNormal;
        ImageButton3.Size = new Size(20, 20);
        ImageButton3.SizeMode = PictureBoxSizeMode.AutoSize;
        ImageButton3.TabIndex = 2;
        ImageButton3.TabStop = false;
        // 
        // ImageButton2
        // 
        ImageButton2.DialogResult = DialogResult.None;
        ImageButton2.DownImage = Properties.Resources.ExampleButtonDownA;
        ImageButton2.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        ImageButton2.HoverImage = Properties.Resources.ExampleButtonHoverA;
        ImageButton2.Location = new Point(184, 61);
        ImageButton2.Margin = new Padding(3, 4, 3, 4);
        ImageButton2.Name = "ImageButton2";
        ImageButton2.NormalImage = Properties.Resources.ExampleButtonA;
        ImageButton2.Size = new Size(100, 50);
        ImageButton2.SizeMode = PictureBoxSizeMode.AutoSize;
        ImageButton2.TabIndex = 1;
        ImageButton2.TabStop = false;
        ImageButton2.Text = "Example B";
        // 
        // label1
        // 
        label1.AutoSize = true;
        label1.BackColor = Color.Transparent;
        label1.Location = new Point(126, 160);
        label1.Name = "label1";
        label1.Size = new Size(115, 17);
        label1.TabIndex = 3;
        label1.Text = "Disable click alerts";
        // 
        // ImageButton4
        // 
        ImageButton4.DialogResult = DialogResult.None;
        ImageButton4.DownImage = Properties.Resources.ExampleButtonDownA;
        ImageButton4.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        ImageButton4.HoverImage = null;
        ImageButton4.Location = new Point(308, 61);
        ImageButton4.Margin = new Padding(3, 4, 3, 4);
        ImageButton4.Name = "ImageButton4";
        ImageButton4.NormalImage = Properties.Resources.ExampleButtonA;
        ImageButton4.Size = new Size(100, 50);
        ImageButton4.SizeMode = PictureBoxSizeMode.AutoSize;
        ImageButton4.TabIndex = 4;
        ImageButton4.TabStop = false;
        ImageButton4.Text = "Example C";
        // 
        // ImageButton5
        // 
        ImageButton5.DialogResult = DialogResult.None;
        ImageButton5.DownImage = null;
        ImageButton5.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        ImageButton5.HoverImage = Properties.Resources.ExampleButtonHoverA;
        ImageButton5.Location = new Point(432, 61);
        ImageButton5.Margin = new Padding(3, 4, 3, 4);
        ImageButton5.Name = "ImageButton5";
        ImageButton5.NormalImage = Properties.Resources.ExampleButtonA;
        ImageButton5.Size = new Size(100, 50);
        ImageButton5.SizeMode = PictureBoxSizeMode.AutoSize;
        ImageButton5.TabIndex = 5;
        ImageButton5.TabStop = false;
        ImageButton5.Text = "Example D";
        // 
        // ImageButton6
        // 
        ImageButton6.DialogResult = DialogResult.None;
        ImageButton6.DownImage = null;
        ImageButton6.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        ImageButton6.HoverImage = null;
        ImageButton6.Location = new Point(308, 135);
        ImageButton6.Margin = new Padding(3, 4, 3, 4);
        ImageButton6.Name = "ImageButton6";
        ImageButton6.NormalImage = Properties.Resources.ExampleButtonA;
        ImageButton6.Size = new Size(100, 50);
        ImageButton6.SizeMode = PictureBoxSizeMode.AutoSize;
        ImageButton6.TabIndex = 6;
        ImageButton6.TabStop = false;
        ImageButton6.Text = "Example E";
        // 
        // ImageButton7
        // 
        ImageButton7.DialogResult = DialogResult.None;
        ImageButton7.DownImage = Properties.Resources.ExampleButtonDownA;
        ImageButton7.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
        ImageButton7.HoverImage = Properties.Resources.ExampleButtonHoverA;
        ImageButton7.Location = new Point(432, 135);
        ImageButton7.Margin = new Padding(3, 4, 3, 4);
        ImageButton7.Name = "ImageButton7";
        ImageButton7.NormalImage = null;
        ImageButton7.Size = new Size(100, 50);
        ImageButton7.SizeMode = PictureBoxSizeMode.AutoSize;
        ImageButton7.TabIndex = 7;
        ImageButton7.TabStop = false;
        ImageButton7.Text = "Example F";
        // 
        // label2
        // 
        label2.AutoSize = true;
        label2.Location = new Point(66, 126);
        label2.Name = "label2";
        label2.Size = new Size(104, 17);
        label2.TabIndex = 8;
        label2.Text = "Default button ^";
        // 
        // TraceMessageToolStrip1
        // 
        TraceMessageToolStrip1.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        TraceMessageToolStrip1.Location = new Point(0, 0);
        TraceMessageToolStrip1.Name = "TraceMessageToolStrip1";
        TraceMessageToolStrip1.Size = new Size(649, 29);
        TraceMessageToolStrip1.TabIndex = 9;
        TraceMessageToolStrip1.Text = "TraceMessageToolStrip1";
        // 
        // Dashboard
        // 
        AcceptButton = ImageButton1;
        AutoScaleDimensions = new SizeF(7.0f, 17.0f);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(649, 251);
        Controls.Add(TraceMessageToolStrip1);
        Controls.Add(ImageButton7);
        Controls.Add(ImageButton6);
        Controls.Add(ImageButton5);
        Controls.Add(ImageButton4);
        Controls.Add(ImageButton3);
        Controls.Add(label1);
        Controls.Add(ImageButton2);
        Controls.Add(ImageButton1);
        Controls.Add(label2);
        Margin = new Padding(3, 4, 3, 4);
        MaximizeBox = false;
        Name = "Dashboard";
        ShowIcon = false;
        Text = "Image Button Demo";
        ((System.ComponentModel.ISupportInitialize)ImageButton1).EndInit();
        ((System.ComponentModel.ISupportInitialize)ImageButton3).EndInit();
        ((System.ComponentModel.ISupportInitialize)ImageButton2).EndInit();
        ((System.ComponentModel.ISupportInitialize)ImageButton4).EndInit();
        ((System.ComponentModel.ISupportInitialize)ImageButton5).EndInit();
        ((System.ComponentModel.ISupportInitialize)ImageButton6).EndInit();
        ((System.ComponentModel.ISupportInitialize)ImageButton7).EndInit();
        ResumeLayout(false);
        PerformLayout();
    }

    private ImageButton ImageButton1;
    private ImageButton ImageButton2;
    private ImageButton ImageButton3;
    private Label label1;
    private ImageButton ImageButton4;
    private ImageButton ImageButton5;
    private ImageButton ImageButton6;
    private ImageButton ImageButton7;
    private Label label2;
    private TextBox TraceMessageToolStrip1;
}
