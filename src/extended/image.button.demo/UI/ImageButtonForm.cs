using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace cc.isr.WinControls.Demo;

/// <summary>   A dashboard. </summary>
/// <remarks>   David, 2021-03-12. </remarks>
public partial class ImageButtonForm : Form
{
    /// <summary>   True to hide, false to show the alerts. </summary>

    private bool _hideAlerts;

    /// <summary>   Specialized default constructor for use only by derived classes. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public ImageButtonForm()
    {
        this.InitializeComponent();
        this.TraceMessages = new Stack<string>();
    }

    /// <summary>   Gets or sets the trace messages. </summary>
    /// <value> The trace messages. </value>
    private Stack<string> TraceMessages { get; set; }

    /// <summary>   Image button 1 click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ImageButton1_Click( object? sender, EventArgs e )
    {
        if ( !this._hideAlerts )
        {
            // MessageBox.Show("Clicked default button.")
            this.TraceMessages.Push( "Clicked default button." );
            this.TraceMessageToolStrip1.Text = this.TraceMessages.Pop();
        }
    }

    /// <summary>   Image button 2 click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ImageButton2_Click( object? sender, EventArgs e )
    {
        if ( !this._hideAlerts )
        {
            // MessageBox.Show("Clicked button B.")
            this.TraceMessages.Push( "Clicked button B" );
            this.TraceMessageToolStrip1.Text = this.TraceMessages.Pop();
        }
    }

    /// <summary>   Image button 3 click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ImageButton3_Click( object? sender, EventArgs e )
    {
        this._hideAlerts = !this._hideAlerts;
        if ( this._hideAlerts )
        {
            this.ImageButton3.NormalImage = Properties.Resources.CCheckedNormal;
            this.ImageButton3.HoverImage = Properties.Resources.CCheckedHover;
            this.ImageButton3.DownImage = Properties.Resources.CCheckedDown;
        }
        else
        {
            this.ImageButton3.NormalImage = Properties.Resources.CUncheckedNormal;
            this.ImageButton3.HoverImage = Properties.Resources.CUncheckedHover;
            this.ImageButton3.DownImage = Properties.Resources.CUncheckedDown;
        }
    }

    /// <summary>   Image button 4 click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ImageButton4_Click( object? sender, EventArgs e )
    {
        if ( !this._hideAlerts )
        {
            // MessageBox.Show("Clicked button C.")
            this.TraceMessages.Push( "Clicked button C" );
            this.TraceMessageToolStrip1.Text = this.TraceMessages.Pop();
        }
    }

    /// <summary>   Image button 5 click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ImageButton5_Click( object? sender, EventArgs e )
    {
        if ( !this._hideAlerts )
        {
            // MessageBox.Show("Clicked button D.")
            this.TraceMessages.Push( "Clicked button D" );
            this.TraceMessageToolStrip1.Text = this.TraceMessages.Pop();
        }
    }

    /// <summary>   Image button 6 click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ImageButton6_Click( object? sender, EventArgs e )
    {
        if ( !this._hideAlerts )
        {
            // MessageBox.Show("Clicked button E.")
            this.TraceMessages.Push( "Clicked button E" );
            this.TraceMessageToolStrip1.Text = this.TraceMessages.Pop();
        }
    }

    /// <summary>   Image button 7 click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ImageButton7_Click( object? sender, EventArgs e )
    {
        if ( !this._hideAlerts )
        {
            // MessageBox.Show("Clicked button F.")
            this.TraceMessages.Push( "Clicked button F" );
            this.TraceMessageToolStrip1.Text = this.TraceMessages.Pop();
        }
    }

    /// <summary>   Gets or sets the shadow distance. </summary>
    /// <value> The shadow distance. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int ShadowDistance { get; set; } = 4;

    /// <summary>   Draws and Fills a Rounded Rectangle and it's accompanying shadow. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="picCanvas">    The picture canvas. </param>
    /// <param name="e">            [in,out] PaintEventArgs object passed in from the Picture box. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
    private void DrawRndRect( Control picCanvas, ref PaintEventArgs e )
    {
        // I like clean lines so set the smoothing mode to Anti-Alias
        e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

        // lets create a rectangle that will be centered in the picture box and
        // just under half the size
        Rectangle rectangle = new( ( int ) Math.Round( Math.Truncate( picCanvas.Width * 0.3d ) ), ( int ) Math.Round( Math.Truncate( picCanvas.Height * 0.3d ) ), ( int ) Math.Round( Math.Truncate( picCanvas.Width * 0.4d ) ), ( int ) Math.Round( Math.Truncate( picCanvas.Height * 0.4d ) ) );

        // create the radius variable and set it equal to 20% the height of the rectangle
        // this will determine the amount of bend at the corners
        float radius = ( int ) Math.Round( Math.Truncate( rectangle.Height * 0.2d ) );

        // create an x and y variable so that we can reduce the length of our code lines
        float x = rectangle.Left;
        float y = rectangle.Top;

        // make sure that we have a valid radius, too small and we have a problem
        if ( radius < 1f )
        {
            radius = 1f;
        }

        try
        {
            // Create a graphics path object with the using operator so the framework
            // can clean up the resources for us
            using GraphicsPath path = new();
            // build the rounded rectangle starting at the top line and going around
            // until the line meets itself again
            path.AddLine( x + radius, y, x + rectangle.Width - (radius * 2f), y );
            path.AddArc( x + rectangle.Width - (radius * 2f), y, radius * 2f, radius * 2f, 270f, 90f );
            path.AddLine( x + rectangle.Width, y + radius, x + rectangle.Width, y + rectangle.Height - (radius * 2f) );
            path.AddArc( x + rectangle.Width - (radius * 2f), y + rectangle.Height - (radius * 2f), radius * 2f, radius * 2f, 0f, 90f );
            path.AddLine( x + rectangle.Width - (radius * 2f), y + rectangle.Height, x + radius, y + rectangle.Height );
            path.AddArc( x, y + rectangle.Height - (radius * 2f), radius * 2f, radius * 2f, 90f, 90f );
            path.AddLine( x, y + rectangle.Height - (radius * 2f), x, y + radius );
            path.AddArc( x, y, radius * 2f, radius * 2f, 180f, 90f );

            // this is where we create the shadow effect, so we will use a 
            // path gradient brush
            using ( PathGradientBrush brush = new( path ) )
            {
                // set the wrap mode so that the colors will layer themselves
                // from the outer edge in
                brush.WrapMode = WrapMode.Clamp;

                // Create a color blend to manage our colors and positions and
                // since we need 3 colors set the default length to 3

                // here is the important part of the shadow making process, remember
                // the clamp mode on the color blend object layers the colors from
                // the outside to the center so we want our transparent color first
                // followed by the actual shadow color. Set the shadow color to a 
                // slightly transparent DimGray, I find that it works best.

                // our color blend will control the distance of each color layer
                // we want to set our transparent color to 0 indicating that the 
                // transparent color should be the outer most color drawn, then
                // our gray color at about 10% of the distance from the edge

                ColorBlend colorBlend = new( 3 )
                {
                    Colors = [Color.Transparent, Color.FromArgb( 180, Color.DimGray ), Color.FromArgb( 180, Color.DimGray )],
                    Positions = [0f, 0.1f, 1.0f]
                };

                // assign the color blend to the path gradient brush
                brush.InterpolationColors = colorBlend;

                // fill the shadow with our path gradient brush
                e.Graphics.FillPath( brush, path );
            }

            // since the shadow was drawn first we need to move the actual path
            // up and back a little so that we can show the shadow underneath
            // the object. To accomplish this we will create a Matrix Object
            Matrix matrix = new();

            // tell the matrix to move the path up and back the designated distance
            matrix.Translate( this.ShadowDistance, this.ShadowDistance );

            // assign the matrix to the graphics path of the rounded rectangle
            path.Transform( matrix );

            // fill the graphics path first
            using ( LinearGradientBrush brush = new( picCanvas.ClientRectangle, Color.Tomato, Color.MistyRose, LinearGradientMode.Vertical ) )
            {
                e.Graphics.FillPath( brush, path );
            }

            // Draw the Graphics path last so that we have cleaner borders
            using Pen pen = new( Color.DimGray, 1.0f );
            e.Graphics.DrawPath( pen, path );
        }
        catch ( Exception ex )
        {
            Debug.WriteLine( this.GetType().Name + ".DrawRndRect() Error: " + ex.Message );
        }
    }

}
