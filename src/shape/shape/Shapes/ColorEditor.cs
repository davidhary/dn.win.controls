using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace cc.isr.WinControls;

/// <summary> A color editor control. </summary>
/// <remarks>
/// (c) 2016 Yang Kok Wah. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-05-14 </para><para>
/// http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
/// </remarks>
internal class ColorEditorControl : UserControl
{
    #region " construction and cleanup "

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="initial_color"> The initial color. </param>
    public ColorEditorControl( Color initial_color )
    {
        this.NewColor = initial_color;
        this.OldColor = initial_color;
        this._trackBarAlpha = new TrackBar();
        this._trackBarRed = new TrackBar();
        this._trackBarGreen = new TrackBar();
        this._trackBarBlue = new TrackBar();
        this._label1 = new Label();
        this._label2 = new Label();
        this._label3 = new Label();
        this._label4 = new Label();
        this._panelColor = new Panel();
        this._labelColor = new Label();
        this.InitializeComponent();
    }

    #endregion

    #region " designer "

    private TrackBar _trackBarAlpha;
    private TrackBar _trackBarRed;
    private TrackBar _trackBarGreen;
    private TrackBar _trackBarBlue;
    private Label _label1;
    private Label _label2;
    private Label _label3;
    private Label _label4;

    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color NewColor { get; set; }

    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color OldColor { get; set; }

    private Label _labelColor;
    private Panel _panelColor;

    /// <summary> Initializes the component. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void InitializeComponent()
    {
        this._trackBarAlpha = new TrackBar();
        this._trackBarRed = new TrackBar();
        this._trackBarGreen = new TrackBar();
        this._trackBarBlue = new TrackBar();
        this._label1 = new Label();
        this._label2 = new Label();
        this._label3 = new Label();
        this._label4 = new Label();
        this._panelColor = new Panel();
        this._labelColor = new Label();
        (( System.ComponentModel.ISupportInitialize ) this._trackBarAlpha).BeginInit();
        (( System.ComponentModel.ISupportInitialize ) this._trackBarRed).BeginInit();
        (( System.ComponentModel.ISupportInitialize ) this._trackBarGreen).BeginInit();
        (( System.ComponentModel.ISupportInitialize ) this._trackBarBlue).BeginInit();
        this._panelColor.SuspendLayout();
        this.SuspendLayout();
        // 
        // trackBarAlpha
        // 
        this._trackBarAlpha.Location = new Point( 40, 3 );
        this._trackBarAlpha.Maximum = 255;
        this._trackBarAlpha.Name = "trackBarAlpha";
        this._trackBarAlpha.Size = new Size( 94, 45 );
        this._trackBarAlpha.TabIndex = 0;
        this._trackBarAlpha.TickFrequency = 20;
        this._trackBarAlpha.ValueChanged += new EventHandler( this.TrackBar_ValueChanged );
        // 
        // trackBarRed
        // 
        this._trackBarRed.Location = new Point( 40, 33 );
        this._trackBarRed.Maximum = 255;
        this._trackBarRed.Name = "trackBarRed";
        this._trackBarRed.Size = new Size( 94, 45 );
        this._trackBarRed.TabIndex = 1;
        this._trackBarRed.TickFrequency = 20;
        this._trackBarRed.ValueChanged += new EventHandler( this.TrackBar_ValueChanged );
        // 
        // trackBarGreen
        // 
        this._trackBarGreen.Location = new Point( 40, 65 );
        this._trackBarGreen.Maximum = 255;
        this._trackBarGreen.Name = "trackBarGreen";
        this._trackBarGreen.Size = new Size( 94, 45 );
        this._trackBarGreen.TabIndex = 2;
        this._trackBarGreen.TickFrequency = 20;
        this._trackBarGreen.ValueChanged += new EventHandler( this.TrackBar_ValueChanged );
        // 
        // trackBarBlue
        // 
        this._trackBarBlue.Location = new Point( 40, 97 );
        this._trackBarBlue.Maximum = 255;
        this._trackBarBlue.Name = "trackBarBlue";
        this._trackBarBlue.Size = new Size( 94, 45 );
        this._trackBarBlue.TabIndex = 3;
        this._trackBarBlue.TickFrequency = 20;
        this._trackBarBlue.ValueChanged += new EventHandler( this.TrackBar_ValueChanged );
        // 
        // label1
        // 
        this._label1.Location = new Point( 8, 6 );
        this._label1.Name = "label1";
        this._label1.Size = new Size( 40, 24 );
        this._label1.TabIndex = 5;
        this._label1.Text = "Alpha";
        // 
        // label2
        // 
        this._label2.Location = new Point( 8, 38 );
        this._label2.Name = "label2";
        this._label2.Size = new Size( 48, 24 );
        this._label2.TabIndex = 6;
        this._label2.Text = "Red";
        // 
        // label3
        // 
        this._label3.Location = new Point( 8, 70 );
        this._label3.Name = "label3";
        this._label3.Size = new Size( 48, 24 );
        this._label3.TabIndex = 7;
        this._label3.Text = "Green";
        // 
        // label4
        // 
        this._label4.Location = new Point( 8, 104 );
        this._label4.Name = "label4";
        this._label4.Size = new Size( 48, 24 );
        this._label4.TabIndex = 8;
        this._label4.Text = "Blue";
        // 
        // panelColor
        // 
        this._panelColor.BorderStyle = BorderStyle.FixedSingle;
        this._panelColor.Controls.Add( this._labelColor );
        this._panelColor.Location = new Point( 136, 11 );
        this._panelColor.Name = "panelColor";
        this._panelColor.Size = new Size( 31, 106 );
        this._panelColor.TabIndex = 9;
        // 
        // labelColor
        // 
        this._labelColor.Location = new Point( -10, -3 );
        this._labelColor.Name = "labelColor";
        this._labelColor.Size = new Size( 56, 160 );
        this._labelColor.TabIndex = 10;
        // 
        // ColorEditorControl
        // 
        this.BackColor = Color.LightGray;
        this.Controls.Add( this._trackBarBlue );
        this.Controls.Add( this._trackBarGreen );
        this.Controls.Add( this._trackBarRed );
        this.Controls.Add( this._panelColor );
        this.Controls.Add( this._trackBarAlpha );
        this.Controls.Add( this._label4 );
        this.Controls.Add( this._label3 );
        this.Controls.Add( this._label2 );
        this.Controls.Add( this._label1 );
        this.Name = "ColorEditorControl";
        this.Size = new Size( 171, 135 );
        KeyPress += new KeyPressEventHandler( this.ColorEditorControl_KeyPress );
        Load += new EventHandler( this.ColorEditorControl_Load );
        (( System.ComponentModel.ISupportInitialize ) this._trackBarAlpha).EndInit();
        (( System.ComponentModel.ISupportInitialize ) this._trackBarRed).EndInit();
        (( System.ComponentModel.ISupportInitialize ) this._trackBarGreen).EndInit();
        (( System.ComponentModel.ISupportInitialize ) this._trackBarBlue).EndInit();
        this._panelColor.ResumeLayout( false );
        this.ResumeLayout( false );
    }

    #endregion

    #region " event handlers "

    /// <summary> Event handler. Called by ColorEditorControl for load events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void ColorEditorControl_Load( object? sender, EventArgs e )
    {
        int argb = this.NewColor.ToArgb();
        // the colors are store in the argb value as 4 byte : AARRGGBB
        byte alpha = ( byte ) (argb >> 24);
        byte red = ( byte ) (argb >> 16);
        byte green = ( byte ) (argb >> 8);
        byte blue = ( byte ) argb;
        this._trackBarAlpha.Value = alpha;
        this._trackBarRed.Value = red;
        this._trackBarGreen.Value = green;
        this._trackBarBlue.Value = blue;

        // Foreground Label on the Color Panel
        this._labelColor.BackColor = Color.FromArgb( alpha, red, green, blue );

        // Create the Background Image for Color Panel 
        // The Color Panel is to allow the user to check on Alpha transparency
        Bitmap bm = new( this._panelColor.Width, this._panelColor.Height );
        this._panelColor.BackgroundImage = bm;
        Graphics g = Graphics.FromImage( this._panelColor.BackgroundImage );
        g.FillRectangle( Brushes.White, 0, 0, this._panelColor.Width, this._panelColor.Height );

        // For formatting the string
        using StringFormat sf = new()
        {
            Alignment = StringAlignment.Center,
            LineAlignment = StringAlignment.Center
        };

        // For rotating the string 
        // If you want the text to be rotated uncomment the 5 lines below and comment off the last line

        using Font font = new( "Arial", 16f, FontStyle.Bold );
        // Matrix m=new Matrix ();
        // m.Rotate(90);
        // m.Translate(this.panelColor.Width ,0 ,MatrixOrder.Append);
        // g.Transform=m;
        // g.DrawString("TEST",new Font("Arial",16,FontStyle.Bold),Brushes.Black,new Rectangle(0,0,panelColor.Height ,panelColor.Width  ),sf);
        g.DrawString( "TEST", font, Brushes.Black, new Rectangle( 0, 0, this._panelColor.Width, this._panelColor.Height ), sf );
    }

    /// <summary> Updates the color. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="a"> An Integer to process. </param>
    /// <param name="r"> An Integer to process. </param>
    /// <param name="g"> An Integer to process. </param>
    /// <param name="b"> An Integer to process. </param>
    private void UpdateColor( int a, int r, int g, int b )
    {
        this.NewColor = Color.FromArgb( a, r, g, b );
        this._labelColor.BackColor = this.NewColor;
    }

    /// <summary> Event handler. Called by TrackBar for value changed events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void TrackBar_ValueChanged( object? sender, EventArgs e )
    {
        this.UpdateColor( this._trackBarAlpha.Value, this._trackBarRed.Value, this._trackBarGreen.Value, this._trackBarBlue.Value );
    }

    /// <summary> Event handler. Called by ColorEditorControl for key press events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Key press event information. </param>
    private void ColorEditorControl_KeyPress( object? sender, KeyPressEventArgs e )
    {
        // If the user hit the escape key we restore back the old color
        if ( e.KeyChar.Equals( Keys.Escape ) )
        {
            this.NewColor = this.OldColor;
        }
    }
    #endregion
}
/// <summary> Editor for color. </summary>
/// <remarks>
/// (c) 2016 Yang Kok Wah. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-05-14 </para><para>
/// http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
/// </remarks>
public class ColorEditor : UITypeEditor
{
    /// <summary>
    /// Initializes a new instance of the <see cref="UITypeEditor" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public ColorEditor()
    {
    }

    /// <summary>
    /// Gets the editor style used by the
    /// <see cref="UITypeEditor.EditValue(IServiceProvider,object)" />
    /// method.
    /// </summary>
    /// <remarks>
    /// David, 2020-09-24. Indicates whether the UITypeEditor provides a form-based (modal) dialog,
    /// drop down dialog, or no UI outside of the properties window.
    /// </remarks>
    /// <param name="context"> An <see cref="System.ComponentModel.ITypeDescriptorContext" /> that
    /// can be used to gain additional context information. </param>
    /// <returns>
    /// A <see cref="UITypeEditorEditStyle" /> value that indicates the style
    /// of editor used by the
    /// <see cref="UITypeEditor.EditValue(IServiceProvider,object)" />
    /// method. If the <see cref="UITypeEditor" /> does not support this
    /// method, then <see cref="UITypeEditor" />.GetEditStyle will return
    /// <see cref="UITypeEditorEditStyle.None" />.
    /// </returns>
    public override UITypeEditorEditStyle GetEditStyle( System.ComponentModel.ITypeDescriptorContext? context )
    {
        return UITypeEditorEditStyle.DropDown;
    }

    /// <summary>
    /// Edits the specified object's value using the editor style indicated by the
    /// <see cref="UITypeEditor" />.GetEditStyle method.
    /// </summary>
    /// <remarks> David, 2020-09-24. Displays the UI for value selection. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="context">  An <see cref="System.ComponentModel.ITypeDescriptorContext" /> that
    /// can be used to gain additional context information. </param>
    /// <param name="provider"> An <see cref="IServiceProvider" /> that this editor can use
    /// to obtain services. </param>
    /// <param name="value">    The object to edit. </param>
    /// <returns>
    /// The new value of the object. If the value of the object has not changed, this should return
    /// the same object it was passed.
    /// </returns>
    public override object EditValue( System.ComponentModel.ITypeDescriptorContext? context, IServiceProvider provider, object? value )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( value, nameof( value ) );
        ArgumentNullException.ThrowIfNull( provider, nameof( provider ) );
#else
        if ( value is null ) throw new ArgumentNullException( nameof( value ) );
        if ( provider is null ) throw new ArgumentNullException( nameof( provider ) );
#endif

        if ( !ReferenceEquals( value.GetType(), typeof( Color ) ) )
            return value;

        // Uses the IWindowsFormsEditorService to display a 
        // drop-down UI in the Properties window.
        object? svc = provider.GetService( typeof( IWindowsFormsEditorService ) );
        if ( svc is IWindowsFormsEditorService service )
        {
            using ColorEditorControl editor = new( ( Color ) value );
            service.DropDownControl( editor );
            // Return the value in the appropriate data format.
            if ( ReferenceEquals( value.GetType(), typeof( Color ) ) )
            {
                Color result = editor.NewColor;
                return result;
            }
        }

        return value;
    }

    /// <summary>
    /// Paints a representation of the value of an object using the specified
    /// <see cref="PaintValueEventArgs" />.
    /// </summary>
    /// <remarks> David, 2020-09-24. Draws a representation of the property's value. </remarks>
    /// <param name="e"> A <see cref="PaintValueEventArgs" /> that indicates
    /// what to paint and where to paint it. </param>
    public override void PaintValue( PaintValueEventArgs e )
    {
        if ( e?.Value is Color )
        {
            using SolidBrush br = new( ( Color ) e.Value );
            e.Graphics.FillRectangle( br, 1, 1, e.Bounds.Width, e.Bounds.Height );
        }
    }

    /// <summary>
    /// Indicates whether the specified context supports painting a representation of an object's
    /// value within the specified context.
    /// </summary>
    /// <remarks>
    /// David, 2020-09-24. Indicates whether the UITypeEditor supports painting a representation of a
    /// property's value.
    /// </remarks>
    /// <param name="context"> An <see cref="System.ComponentModel.ITypeDescriptorContext" /> that
    /// can be used to gain additional context information. </param>
    /// <returns>
    /// <see langword="true" /> if
    /// <see cref="UITypeEditor.PaintValue(object,Graphics,Rectangle)" />
    /// is implemented; otherwise, <see langword="false" />.
    /// </returns>
    public override bool GetPaintValueSupported( System.ComponentModel.ITypeDescriptorContext? context )
    {
        return true;
    }
}
