using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> An outline trace. </summary>
/// <remarks>
/// (c) 2016 Yang Kok Wah. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-05-14 </para><para>
/// http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
/// </remarks>
internal class OutlineTrace
{
    /// <summary> Initializes a new instance of the <see cref="object" /> class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public OutlineTrace() : base()
    {
    }

    /// <summary> Gets or sets the color threshold. </summary>
    /// <value> The color threshold. </value>
    public int ColorThreshold { get; set; } = 200;

    /// <summary> Gets or sets the use red. </summary>
    /// <value> The use red. </value>
    public bool UseRed { get; set; } = true;

    /// <summary> Gets or sets the use green. </summary>
    /// <value> The use green. </value>
    public bool UseGreen { get; set; } = true;

    /// <summary> Gets or sets the use blue. </summary>
    /// <value> The use blue. </value>
    public bool UseBlue { get; set; } = true;

    /// <summary> Coordinates to index for ARGB color pixel format. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="x">      The x coordinate. </param>
    /// <param name="y">      The y coordinate. </param>
    /// <param name="stride"> The stride. </param>
    /// <returns> An Integer. </returns>
    private static int CoordinatesToIndex( int x, int y, int stride )
    {
        return (stride * y) + (x * 4);
    }

    /// <summary> Gets gray scale color. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="c"> A Color to process. </param>
    /// <returns> The gray scale color. </returns>
    public int GetGrayScaleColor( Color c )
    {
        int numColorPlane = 3;
        numColorPlane = !this.UseBlue ? numColorPlane - 1 : numColorPlane;
        numColorPlane = !this.UseGreen ? numColorPlane - 1 : numColorPlane;
        numColorPlane = !this.UseRed ? numColorPlane - 1 : numColorPlane;
        if ( numColorPlane == 0 )
        {
            return (c.B + c.G + c.R) / 3;
        }

        int accValue = 0;
        accValue = this.UseBlue ? accValue + c.B : accValue;
        accValue = this.UseGreen ? accValue + c.G : accValue;
        accValue = this.UseRed ? accValue + c.R : accValue;
        return accValue / numColorPlane;
    }

    /// <summary> Gets mono color. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="c"> A Color to process. </param>
    /// <returns> The mono color. </returns>
    private int GetMonoColor( Color c )
    {
        int i = this.GetGrayScaleColor( c );
        return i < this.ColorThreshold ? 0 : 1;
    }

    /// <summary> String outline 2 polygon. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="outline"> The outline. </param>
    /// <returns> A Point() </returns>
    public static Point[] StringOutline2Polygon( string outline )
    {
        string[] s = outline.Split( ';' );
        if ( s.Length < 5 )
        {
            return [];
        }

        Point[] p = new Point[s.Length - 2 + 1];
        string[] s1; // = s(0).Split(","c)
        for ( int i = 0, loopTo = s.Length - 2; i <= loopTo; i++ )
        {
            s1 = s[i].Split( ',' );
            p[i].X = int.Parse( s1[0], System.Globalization.CultureInfo.CurrentCulture );
            p[i].Y = int.Parse( s1[1], System.Globalization.CultureInfo.CurrentCulture );
        }

        return p;
    }

    /// <summary> Trace outline n. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="bm">              The bitmap. </param>
    /// <param name="x0">              The x coordinate 0. </param>
    /// <param name="y0">              The y coordinate 0. </param>
    /// <param name="probeWidth">     Width of the probe. </param>
    /// <param name="fg">              The foreground. </param>
    /// <param name="bg">              The background. </param>
    /// <param name="isAutoThreshold"> True to auto threshold. </param>
    /// <param name="n">               An Integer to process. </param>
    /// <returns> A <see cref="string" />. </returns>
    public string TraceOutlineN( Bitmap bm, int x0, int y0, int probeWidth, Color fg, Color bg, bool isAutoThreshold, int n )
    {
        string s = string.Empty;
        int y = y0;
        int x1 = 0;
        int y1 = 0;
        Color c1;
        Color c2;
        int start_direction = 0;
        int current_direction = 0;
        bool hitBorder = false;
        bool hitStart = false;
        int max_width = bm.Width;
        int max_height = bm.Height;

        // direct bit manipulation
        Rectangle rect = new( 0, 0, bm.Width, bm.Height );
        BitmapData bmpData = bm.LockBits( rect, ImageLockMode.ReadOnly, bm.PixelFormat );

        IntPtr ptr = bmpData.Scan0;
        int bytes = bm.Width * bm.Height * 4;
        byte[] rgbValues = new byte[bytes];

        // Copy the RGB values into the array.
        System.Runtime.InteropServices.Marshal.Copy( ptr, rgbValues, 0, bytes );
        bm.UnlockBits( bmpData );
        int x;
        int gc1;
        int gc2;
        if ( isAutoThreshold )
        {
            // get max pix value difference
            int maxPixDiff = 0;
            int maxPixValue = 0;
            for ( int i = 0, loopTo = (probeWidth * 2) - 1; i <= loopTo; i++ )
            {
                try
                {
                    x = i % 2 == 1 ? x0 + (i / 2) : x0 - (i / 2);
                    if ( x < 0 )
                    {
                        continue;
                    }

                    if ( x >= max_width )
                    {
                        continue;
                    }

                    int index = CoordinatesToIndex( x, y0, bmpData.Stride );
                    if ( index < 0 || index > bytes - 1 )
                    {
                        break;
                    }

                    c1 = Color.FromArgb( rgbValues[index + 2], rgbValues[index + 1], rgbValues[index] );
                    gc1 = this.GetGrayScaleColor( c1 );
                    index = CoordinatesToIndex( x + 1, y0, bmpData.Stride );
                    if ( index < 0 || index > bytes - 1 )
                    {
                        break;
                    }

                    c2 = Color.FromArgb( rgbValues[index + 2], rgbValues[index + 1], rgbValues[index] );
                    gc2 = this.GetGrayScaleColor( c2 );
                    if ( maxPixDiff < Math.Abs( gc1 - gc2 ) )
                    {
                        maxPixDiff = Math.Abs( gc1 - gc2 );
                    }

                    if ( gc1 > maxPixValue )
                    {
                        maxPixValue = gc1;
                    }

                    if ( gc2 > maxPixValue )
                    {
                        maxPixValue = gc2;
                    }
                }
                catch ( Exception )
                {
                    break;
                }
            }

            if ( maxPixDiff > 0 )
            {
                this.ColorThreshold = maxPixValue - ( int ) Math.Truncate( 0.3d * maxPixDiff );
            }

            if ( this.ColorThreshold < 0 )
            {
                this.ColorThreshold = 0;
            }
        }

        int gfg = this.GetMonoColor( fg );
        int gbg = this.GetMonoColor( bg );
        for ( int i = 0, loopTo1 = (probeWidth * 2) - 1; i <= loopTo1; i++ )
        {
            try
            {
                x = i % 2 == 1 ? x0 + (i / 2) : x0 - (i / 2);
                if ( x < 0 )
                {
                    continue;
                }

                if ( x >= max_width )
                {
                    continue;
                }

                int index = CoordinatesToIndex( x, y0, bmpData.Stride );
                if ( index < 0 || index > bytes - 1 )
                {
                    break;
                }

                c1 = Color.FromArgb( rgbValues[index + 2], rgbValues[index + 1], rgbValues[index] );
                gc1 = this.GetMonoColor( c1 );
                index = CoordinatesToIndex( x + 1, y0, bmpData.Stride );
                if ( index < 0 || index > bytes - 1 )
                {
                    break;
                }

                c2 = Color.FromArgb( rgbValues[index + 2], rgbValues[index + 1], rgbValues[index] );
                gc2 = this.GetMonoColor( c2 );
                if ( (gc1 == gfg && gc2 == gbg) || (gc1 == gbg && gc2 == gfg) )
                {
                    if ( gc1 == gfg && gc2 == gbg )
                    {
                        start_direction = 4;
                    }

                    if ( gc1 == gbg && gc2 == gfg )
                    {
                        start_direction = 0;
                    }

                    hitBorder = true;
                    x1 = x;
                    y1 = y;
                    break;
                }
            }
            catch ( Exception )
            {
                break;
            }
        }

        if ( !hitBorder )
        {
            return string.Empty;
        }

        Color[] cn = new Color[(8 * n)];
        int count = 0;
        int countLimit = 10000;
        x = x1;
        y = y1;
        while ( !hitStart )
        {
            count += 1;

            // fall back to prevent infinite loop
            if ( count > countLimit )
            {
                return string.Empty;
            }

            int diffX;
            int diffY;
            // getting all the neighbors' pixel color
            try
            {
                int index1;

                // processing top neighbors left to right
                for ( int i = 0, loopTo2 = 2 * n; i <= loopTo2; i++ )
                {
                    diffX = i - n;
                    index1 = CoordinatesToIndex( x + diffX, y - n, bmpData.Stride );
                    cn[i] = x + diffX >= 0 && x + diffX < max_width && y - n >= 0 && y - n < max_height ? Color.FromArgb( rgbValues[index1 + 2], rgbValues[index1 + 1], rgbValues[index1] ) : Color.Empty;
                }

                // processing right neighbors top to bottom
                for ( int i = (2 * n) + 1, loopTo3 = (4 * n) - 1; i <= loopTo3; i++ )
                {
                    diffY = i - (3 * n);
                    index1 = CoordinatesToIndex( x + n, y + diffY, bmpData.Stride );
                    cn[i] = x + n >= 0 && x + n < max_width && y + diffY >= 0 && y + diffY < max_height ? Color.FromArgb( rgbValues[index1 + 2], rgbValues[index1 + 1], rgbValues[index1] ) : Color.Empty;
                }

                // processing bottom neighbors right to left
                for ( int i = 4 * n, loopTo4 = 6 * n; i <= loopTo4; i++ )
                {
                    diffX = i - (5 * n);
                    index1 = CoordinatesToIndex( x - diffX, y + n, bmpData.Stride );
                    cn[i] = x - diffX >= 0 && x - diffX < max_width && y + n >= 0 && y + n < max_height ? Color.FromArgb( rgbValues[index1 + 2], rgbValues[index1 + 1], rgbValues[index1] ) : Color.Empty;
                }

                // processing left neighbors bottom to top
                for ( int i = (6 * n) + 1, loopTo5 = (8 * n) - 1; i <= loopTo5; i++ )
                {
                    diffY = i - (7 * n);
                    index1 = CoordinatesToIndex( x - n, y - diffY, bmpData.Stride );
                    cn[i] = x - n >= 0 && x - n < max_width && y - diffY >= 0 && y - diffY < max_height ? Color.FromArgb( rgbValues[index1 + 2], rgbValues[index1 + 1], rgbValues[index1] ) : Color.Empty;
                }
            }
            catch ( Exception e )
            {
                _ = MessageBox.Show( e.ToString() );
                return string.Empty;
            }

            int index = 0;
            bool dir_found = false;

            // find the first valid foreground pixel				
            for ( int i = start_direction, loopTo6 = start_direction + ((8 * n) - 1); i <= loopTo6; i++ )
            {
                index = i % (8 * n);
                if ( !cn[index].Equals( Color.Empty ) )
                {
                    if ( this.GetMonoColor( cn[index] ) == gfg )
                    {
                        current_direction = index;
                        dir_found = true;
                        break;
                    }
                }
            }


            // if no foreground pixel found, just find the next valid pixel 

            if ( !dir_found )
            {
                for ( int i = start_direction, loopTo7 = start_direction + ((8 * n) - 1); i <= loopTo7; i++ )
                {
                    index = i % (8 * n);
                    if ( !cn[index].Equals( Color.Empty ) )
                    {
                        current_direction = index;
                        break;
                    }
                }
            }


            // find the next direction to look for foreground pixels
            if ( index >= 0 && index <= 2 * n )
            {
                diffX = index - n;
                x += diffX;
                y -= n;
            }

            if ( index > 2 * n && index < 4 * n )
            {
                diffY = index - (3 * n);
                x += n;
                y += diffY;
            }

            if ( index >= 4 * n && index <= 6 * n )
            {
                diffX = index - (5 * n);
                x -= diffX;
                y += n;
            }

            if ( index > 6 * n && index < 8 * n )
            {
                diffY = index - (7 * n);
                x -= n;
                y -= diffY;
            }



            // store the found outline
            string tests = x + "," + y + ";";
            s += tests;
            start_direction = (current_direction + (4 * n) + 1) % (8 * n);

            // adaptive stop condition
            bool bMinCountOK = n > 1 ? count > max_height / 5 : count > 10;
            if ( bMinCountOK && Math.Abs( x - x1 ) < n + 1 && Math.Abs( y - y1 ) < n + 1 )
            {
                hitStart = true;
            }
        }

        return s;
    }
}
