using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace cc.isr.WinControls;

/// <summary> A shape type editor control. </summary>
/// <remarks>
/// (c) 2016 Yang Kok Wah. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-05-14 </para><para>
/// http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
/// </remarks>
internal class ShapeTypeEditorControl : UserControl
{
    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="initial_shape"> The initial shape. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    public ShapeTypeEditorControl( ShapeType initial_shape ) : base()
    {
        this.InitializeComponent();
        this.Shape = initial_shape;

        // Find the number of shapes in the enumeration
#pragma warning disable CA2263
        int shapesCount = Enum.GetValues( typeof( ShapeType ) ).GetLength( 0 );
#pragma warning restore CA2263
        int rowCount = ( int ) Math.Truncate( Math.Sqrt( shapesCount ) );
        // Find the number of rows and columns to accommodate the shapes
        int columnCount = shapesCount / rowCount;
        if ( shapesCount % columnCount > 0 )
        {
            columnCount += 1;
        }

        // Record the specifications
        this._rowCount = rowCount;
        this._columnCount = columnCount;
        this._valid_width = (this._columnCount * this._width) + ((this._columnCount - 1) * 6) + (2 * 4);
        this._valid_height = (this._rowCount * this._height) + ((this._rowCount - 1) * 6) + (2 * 4);
    }

    /// <summary> Initializes the component. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void InitializeComponent()
    {
        // 
        // ShapeTypeEditorControl
        // 
        this.BackColor = Color.LightGray;
        this.Name = "ShapeTypeEditorControl";
    }

    /// <summary> Gets or sets the shape type. </summary>
    /// <value> The shape. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ShapeType Shape { get; set; }

    // Specification for the UI

    // number of shapes
    // Private _shapesCount As Integer

#pragma warning disable IDE0044 // Add readonly modifier
    // number of rows
    private int _rowCount;
    // number of columns
    private int _columnCount;
    // width of each shape
    private int _valid_width;
    // height of each shape
    private int _valid_height;
    private int _width = 20;
    private int _height = 20;
#pragma warning restore IDE0044 // Add readonly modifier

    /// <summary> Raises the <see cref="Control.Paint" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    protected override void OnPaint( PaintEventArgs e )
    {
        if ( e is null ) return;

        using Bitmap bm = new( this.Width, this.Height, e.Graphics );
        Graphics g = Graphics.FromImage( bm );
        g.FillRectangle( Brushes.LightGray, new Rectangle( 0, 0, bm.Width, bm.Height ) );
        e.Graphics.FillRectangle( Brushes.LightGray, new Rectangle( 0, 0, this.Width, this.Height ) );
        int x = 4;
        int y = 4;
        int n = 0;
#pragma warning disable CA2263
        foreach ( ShapeType shape in Enum.GetValues( typeof( ShapeType ) ) )
#pragma warning restore CA2263
        {
            using GraphicsPath path = new();
            GraphicsPath outlinePath = path;
            ShapeControl.UpdateOutline( ref outlinePath, shape, this._width, this._height );
            g.FillRectangle( Brushes.LightGray, 0, 0, bm.Width, bm.Height );
            g.FillPath( Brushes.Yellow, path );
            g.DrawPath( Pens.Red, path );
            e.Graphics.DrawImage( bm, x, y, new Rectangle( new Point( 0, 0 ), new Size( this._width + 1, this._height + 1 ) ), GraphicsUnit.Pixel );
            if ( this.Shape.Equals( shape ) )
            {
                e.Graphics.DrawRectangle( Pens.Red, new Rectangle( new Point( x - 2, y - 2 ), new Size( this._width + 4, this._height + 4 ) ) );
            }

            n += 1;
            x = n % this._columnCount * this._width;
            x = x + (n % this._columnCount * 6) + 4;
            y = n / this._columnCount * this._height;
            y = y + (n / this._columnCount * 6) + 4;
        }
    }

    /// <summary> Raises the <see cref="Control.MouseDown" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>" )]
    protected override void OnMouseDown( MouseEventArgs e )
    {
        if ( e is null ) return;

        if ( e.Button.Equals( MouseButtons.Left ) )
        {
            if ( e.X > this._valid_width )
            {
                return;
            }

            if ( e.Y > this._valid_height )
            {
                return;
            }

            int x;
            int y;
            int n;
            x = e.X;
            y = e.Y;
            n = (y / (this._valid_height / this._rowCount) * this._columnCount) + (x / (this._valid_width / this._columnCount) % this._columnCount);
            int count = 0;
#pragma warning disable CA2263
            foreach ( ShapeType shape in Enum.GetValues( typeof( ShapeType ) ) )
#pragma warning restore CA2263
            {
                if ( count == n )
                {
                    this.Shape = shape;
                    // close the editor immediately
                    SendKeys.Send( "{ENTER}" );
                }

                count += 1;
            }
        }
    }
}
/// <summary> Editor for shape type. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public class ShapeTypeEditor : UITypeEditor
{
    /// <summary>
    /// Initializes a new instance of the <see cref="UITypeEditor" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public ShapeTypeEditor()
    {
    }

    /// <summary>
    /// Gets the editor style used by the
    /// <see cref="UITypeEditor.EditValue(IServiceProvider,object)" />
    /// method.
    /// </summary>
    /// <remarks>
    /// David, 2020-09-24. Indicates whether the UITypeEditor provides a form-based (modal) dialog,
    /// drop down dialog, or no UI outside of the properties window.
    /// </remarks>
    /// <param name="context"> An <see cref="System.ComponentModel.ITypeDescriptorContext" /> that
    /// can be used to gain additional context information. </param>
    /// <returns>
    /// A <see cref="UITypeEditorEditStyle" /> value that indicates the style
    /// of editor used by the
    /// <see cref="UITypeEditor.EditValue(IServiceProvider,object)" />
    /// method. If the <see cref="UITypeEditor" /> does not support this
    /// method, then <see cref="UITypeEditor" />.GetEditStyle will return
    /// <see cref="UITypeEditorEditStyle.None" />.
    /// </returns>
    public override UITypeEditorEditStyle GetEditStyle( System.ComponentModel.ITypeDescriptorContext? context )
    {
        return UITypeEditorEditStyle.DropDown;
    }

    /// <summary>
    /// Edits the specified object's value using the editor style indicated by the
    /// <see cref="UITypeEditor" />.GetEditStyle method.
    /// </summary>
    /// <remarks> David, 2020-09-24. Displays the UI for value selection. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="context">  An <see cref="System.ComponentModel.ITypeDescriptorContext" /> that
    /// can be used to gain additional context information. </param>
    /// <param name="provider"> An <see cref="IServiceProvider" /> that this editor can use
    /// to obtain services. </param>
    /// <param name="value">    The object to edit. </param>
    /// <returns>
    /// The new value of the object. If the value of the object has not changed, this should return
    /// the same object it was passed.
    /// </returns>
    public override object? EditValue( System.ComponentModel.ITypeDescriptorContext? context, IServiceProvider? provider, object? value )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( provider, nameof( provider ) );
#else
        if ( provider is null ) throw new ArgumentNullException( nameof( provider ) );
#endif

        // Return the value if the value is not of type ShapeType
        if ( value is null || !ReferenceEquals( value.GetType(), typeof( ShapeType ) ) )
        {
            return value;
        }

        // Uses the IWindowsFormsEditorService to display a 
        // drop-down UI in the Properties window.
        object? edSvc = provider.GetService( typeof( IWindowsFormsEditorService ) );
        if ( edSvc is IWindowsFormsEditorService service )
        {
            // Display an Shape Type Editor Control and retrieve the value.
            using ShapeTypeEditorControl editor = new( ( ShapeType ) ( int ) value );
            service.DropDownControl( editor );
            // Return the value in the appropriate data format.
            if ( ReferenceEquals( value.GetType(), typeof( ShapeType ) ) )
            {
                ShapeType result = editor.Shape;
                return result;
            }
        }

        return value;
    }

    /// <summary>
    /// Paints a representation of the value of an object using the specified
    /// <see cref="PaintValueEventArgs" />.
    /// </summary>
    /// <remarks> David, 2020-09-24. Draws a representation of the property's value. </remarks>
    /// <param name="e"> A <see cref="PaintValueEventArgs" /> that indicates
    /// what to paint and where to paint it. </param>
    public override void PaintValue( PaintValueEventArgs e )
    {
        if ( e?.Value is ShapeType )
        {
            using Bitmap bm = new( e.Bounds.Width + 4, e.Bounds.Height + 4, e.Graphics );
            using Graphics g = Graphics.FromImage( bm );
            ShapeType shape = ( ShapeType ) ( int ) e.Value;
            using GraphicsPath path = new();
            GraphicsPath argOutline = path;
            ShapeControl.UpdateOutline( ref argOutline, shape, e.Bounds.Width - 5, e.Bounds.Height - 5 );
            g.FillPath( Brushes.Yellow, path );
            g.DrawPath( Pens.Red, path );
            e.Graphics.DrawImage( bm, 3, 3, new Rectangle( new Point( 0, 0 ), new Size( e.Bounds.Width, e.Bounds.Height ) ), GraphicsUnit.Pixel );
        }

    }

    /// <summary>
    /// Indicates whether the specified context supports painting a representation of an object's
    /// value within the specified context.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="context"> An <see cref="System.ComponentModel.ITypeDescriptorContext" /> that
    /// can be used to gain additional context information. </param>
    /// <returns>
    /// <see langword="true" /> if
    /// <see cref="UITypeEditor.PaintValue(object,Graphics,Rectangle)" />
    /// is implemented; otherwise, <see langword="false" />.
    /// </returns>
    public override bool GetPaintValueSupported( System.ComponentModel.ITypeDescriptorContext? context )
    {
        return true;
    }
}
