using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> Shape. </summary>
/// <remarks>
/// David, 2014-10-13 <para>
/// David, 2014-10-13, 2.1.5399. </para>
/// </remarks>
public partial class Shape : UserControl
{
    #region " construction "

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public Shape() : base()
    {
        this.InitializeComponent();
        this.SetStyle( ControlStyles.UserPaint, true );
        this.BackColor = Color.Transparent;
        this._fillBrush = new HatchBrush( HatchStyle.Horizontal, Color.Transparent, Color.Transparent );
        this._borderPen = new Pen( SystemColors.WindowText );
        this._borderColor = SystemColors.WindowText;
        this._shapeStyle = ShapeStyle.Rectangle;
        this._backStyle = ShapeBackStyle.Transparent;
        this._fillStyle = ShapeFillStyle.Solid;
        this._borderStyle = ShapeBorderStyle.Solid;
        this._borderWidth = 1;
        this._fillColor = Color.Black;
        this._roundPercent = 0.15d;
        this.SelectBrush();
        this.SelectPen();
    }

    /// <summary> Cleans up any resources being used. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> <c>true</c> if managed resources should be disposed; otherwise,
    /// false. </param>
    [System.Diagnostics.DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;

                this._borderPen?.Dispose();

                this._fillBrush?.Dispose();
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " shape properties "

    /// <summary>
    /// Brush used to paint the Shape control.
    /// </summary>
    private HatchBrush _fillBrush;

    // Private _backColor As Color

    /// <summary> Background Color to display text and graphics. </summary>
    /// <value> The color of the back. </value>
    [Description( "Returns/sets the background color used to display text and graphics in an object." )]
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public override Color BackColor
    {
        get => base.BackColor;
        set => base.BackColor = value;// Me.SelectBrush()// Me.Refresh()
    }

    /// <summary>
    /// Raises the <see cref="Control.BackColorChanged" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnBackColorChanged( EventArgs e )
    {
        base.OnBackColorChanged( e );
        this.SelectBrush();
        this.Refresh();
    }

    /// <summary>
    /// Stores the Back Style property.
    /// </summary>
    private ShapeBackStyle _backStyle;

    /// <summary>
    /// Indicates whether a Label or the background of a Shape is transparent or opaque.
    /// </summary>
    /// <value> The back style. </value>
    [Description( "Indicates whether a Label or the background of a Shape is transparent or opaque." )]
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ShapeBackStyle BackStyle
    {
        get => this._backStyle;
        set
        {
            this._backStyle = value;
            this.SelectBrush();
            this.Refresh();
        }
    }

    /// <summary>
    /// Pen used to paint the border of the Shape Control.
    /// </summary>
    private readonly Pen _borderPen;

    /// <summary> Stores the BorderColor property. </summary>
    private Color _borderColor;

    /// <summary> Color of the Shape border. </summary>
    /// <value> The color of the border. </value>
    [Description( "Returns/sets the color of an object's border." )]
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color BorderColor
    {
        get => this._borderColor;
        set
        {
            this._borderColor = value;
            this.SelectPen();
            this.Refresh();
        }
    }

    /// <summary> Select pen. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void SelectPen()
    {
        this._borderPen.Width = this.BorderWidth;
        this._borderPen.DashOffset = 1500f;
        switch ( this.BorderStyle )
        {
            case ShapeBorderStyle.None:
                {
                    this._borderPen.Color = Color.Transparent;
                    break;
                }

            case ShapeBorderStyle.Solid:
                {
                    this._borderPen.Color = this.BorderColor;
                    this._borderPen.DashStyle = DashStyle.Solid;
                    break;
                }

            case ShapeBorderStyle.Dash:
                {
                    this._borderPen.Color = this.BorderColor;
                    this._borderPen.DashStyle = DashStyle.Dash;
                    break;
                }

            case ShapeBorderStyle.Dot:
                {
                    this._borderPen.Color = this.BorderColor;
                    this._borderPen.DashStyle = DashStyle.Dot;
                    break;
                }

            case ShapeBorderStyle.DashDot:
                {
                    this._borderPen.Color = this.BorderColor;
                    this._borderPen.DashStyle = DashStyle.DashDot;
                    break;
                }

            case ShapeBorderStyle.DashDotDot:
                {
                    this._borderPen.Color = this.BorderColor;
                    this._borderPen.DashStyle = DashStyle.DashDotDot;
                    break;
                }

            default:
                {
                    this._borderStyle = ShapeBorderStyle.Solid;
                    this._borderPen.Color = this.BorderColor;
                    this._borderPen.DashStyle = DashStyle.Solid;
                    break;
                }
        }
    }

    /// <summary> Stores the Border Style property. </summary>
    private ShapeBorderStyle _borderStyle;

    /// <summary> Border style of the Shape control. </summary>
    /// <value> The border style. </value>
    [Description( "Returns/sets the border style for an object." )]
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new ShapeBorderStyle BorderStyle
    {
        get => this._borderStyle;
        set
        {
            this._borderStyle = value;
            this.SelectPen();
            this.Refresh();
        }
    }

    /// <summary> Stores the BorderWidth property. </summary>
    private int _borderWidth;

    /// <summary> Width of the Shape border. </summary>
    /// <value> The width of the border. </value>
    [Description( "Returns or sets the width of a control's border." )]
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int BorderWidth
    {
        get => this._borderWidth;
        set
        {
            this._borderWidth = value;
            this._borderPen.Width = this.BorderWidth;
            this.Refresh();
        }
    }

    /// <summary>
    /// Stores FillColor property.
    /// </summary>
    private Color _fillColor;

    /// <summary> Color to fill in Shape control. </summary>
    /// <value> The color of the fill. </value>
    [Description( "Returns/sets the color used to fill in shapes, circles, and boxes" )]
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color FillColor
    {
        get => this._fillColor;
        set
        {
            this._fillColor = value;
            this.SelectBrush();
            this.Refresh();
        }
    }

    /// <summary> Select brush. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void SelectBrush()
    {
        switch ( this.FillStyle )
        {
            case ShapeFillStyle.Solid:
                {
                    this._fillBrush = new HatchBrush( HatchStyle.Horizontal, this.FillColor, this.FillColor );
                    break;
                }

            case ShapeFillStyle.HorizontalLine:
                {
                    this._fillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.Horizontal, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.Horizontal, this.FillColor, Color.Transparent );
                    break;
                }

            case ShapeFillStyle.VerticalLine:
                {
                    this._fillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.Vertical, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.Vertical, this.FillColor, Color.Transparent );
                    break;
                }

            case ShapeFillStyle.DownwardDiagonal:
                {
                    this._fillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.WideDownwardDiagonal, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.WideDownwardDiagonal, this.FillColor, Color.Transparent );
                    break;
                }

            case ShapeFillStyle.UpwardDiagonal:
                {
                    this._fillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.WideUpwardDiagonal, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.WideUpwardDiagonal, this.FillColor, Color.Transparent );
                    break;
                }

            case ShapeFillStyle.Cross:
                {
                    this._fillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.Cross, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.Cross, this.FillColor, Color.Transparent );
                    break;
                }

            case ShapeFillStyle.DiagonalCross:
                {
                    this._fillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.DiagonalCross, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.DiagonalCross, this.FillColor, Color.Transparent );
                    break;
                }

            case ShapeFillStyle.Transparent:
                {
                    this._fillBrush = this.BackStyle == ShapeBackStyle.Transparent ? new HatchBrush( HatchStyle.Horizontal, Color.Transparent, Color.Transparent ) : new HatchBrush( HatchStyle.Horizontal, this.BackColor, this.BackColor );
                    break;
                }

            default:
                {
                    this._fillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.Horizontal, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.DiagonalCross, this.FillColor, Color.Transparent );
                    break;
                }
        }
    }

    /// <summary>
    /// Stores FillStyle property.
    /// </summary>
    private ShapeFillStyle _fillStyle;

    /// <summary> FillStyle in Shape control. </summary>
    /// <value> The fill style. </value>
    [Description( "Returns/sets the fill style of a shape" )]
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ShapeFillStyle FillStyle
    {
        get => this._fillStyle;
        set
        {
            this._fillStyle = value;
            this.SelectBrush();
            this.Refresh();
        }
    }

    /// <summary>
    /// Stores the Shape style property.
    /// </summary>
    private ShapeStyle _shapeStyle;

    /// <summary> The kind of Shape. </summary>
    /// <value> The shape. </value>
    [Description( "Returns/sets a value indicating the appearance of a control" )]
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ShapeStyle ShapeStyle
    {
        get => this._shapeStyle;
        set
        {
            this._shapeStyle = value;
            this.Refresh();
        }
    }

    /// <summary>
    /// Stores the RoundPercent property.
    /// </summary>
    private double _roundPercent;

    /// <summary>
    /// Adds a property to specify the percent used to round the corners in round rectangles and
    /// round squares.
    /// </summary>
    /// <value> The round percent. </value>
    [Description( "Allows to specify the percent used to round the corners of round rectangles and round squares" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int RoundPercent
    {
        get => ( int ) Math.Floor( this._roundPercent * 100d );
        set
        {
            value = value < 1 ? 1 : value > 50 ? 50 : value;
            this._roundPercent = value / 100d;
            this.Refresh();
        }
    }

    #endregion

    #region " paint "

    /// <summary> Raises the <see cref="Control.Paint" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs e )
    {
        base.OnPaint( e );
        if ( e is not null )
        {
            Rectangle clientRectangle = new( 0, 0, this.ClientRectangle.Width - 1, this.ClientRectangle.Height - 1 );
            switch ( this.ShapeStyle )
            {
                case ShapeStyle.Rectangle:
                    {
                        this.DrawRectangle( clientRectangle, e.Graphics );
                        break;
                    }

                case ShapeStyle.Square:
                    {
                        this.DrawSquare( clientRectangle, e.Graphics );
                        break;
                    }

                case ShapeStyle.Oval:
                    {
                        this.DrawOval( clientRectangle, e.Graphics );
                        break;
                    }

                case ShapeStyle.Circle:
                    {
                        this.DrawCircle( clientRectangle, e.Graphics );
                        break;
                    }

                case ShapeStyle.RoundRectangle:
                    {
                        this.DrawRoundRectangle( clientRectangle, e.Graphics );
                        break;
                    }

                case ShapeStyle.RoundSquare:
                    {
                        this.DrawRoundSquare( clientRectangle, e.Graphics );
                        break;
                    }

                default:
                    break;
            }
        }
    }

    /// <summary> Raises the system. event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnResize( EventArgs e )
    {
        base.OnResize( e );
        // Me.Refresh()
    }

    /// <summary> Draws a round square. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="clientRectangle"> . </param>
    /// <param name="g">               . </param>
    private void DrawRoundSquare( Rectangle clientRectangle, Graphics g )
    {
        int maxDiameter = Math.Min( clientRectangle.Height, clientRectangle.Width );
        Rectangle newClientRectangle = new( clientRectangle.Location.X + ((clientRectangle.Width - maxDiameter) / 2), clientRectangle.Location.Y + ((clientRectangle.Height - maxDiameter) / 2), maxDiameter, maxDiameter );
        this.DrawRoundRectangle( newClientRectangle, g );
    }

    /// <summary> Draws a round rectangle. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="clientRectangle"> The region where to draw. </param>
    /// <param name="g">               The GDI used to draw the rectangle. </param>
    private void DrawRoundRectangle( Rectangle clientRectangle, Graphics g )
    {
        double percentX = clientRectangle.Width * this.RoundPercent;
        double percentY = clientRectangle.Height * this.RoundPercent;
        double minPercent = Math.Min( percentX, percentY );
        double halfPercentX = percentX / 2d;
        double halfPercentY = percentY / 2d;
        double minHalfPercent = Math.Min( halfPercentX, halfPercentY );
        PointF pUp1 = new( ( float ) (clientRectangle.X + minPercent), clientRectangle.Y );
        PointF pUp2 = new( ( float ) (clientRectangle.X + clientRectangle.Width - minPercent), clientRectangle.Y );
        PointF pDown1 = new( ( float ) (clientRectangle.X + clientRectangle.Width - minPercent), clientRectangle.Y + clientRectangle.Height );
        PointF pDown2 = new( ( float ) (clientRectangle.X + minPercent), clientRectangle.Y + clientRectangle.Height );
        PointF pLeft1 = new( clientRectangle.X, ( float ) (clientRectangle.Y + clientRectangle.Height - minPercent) );
        PointF pLeft2 = new( clientRectangle.X, ( float ) (clientRectangle.Y + minPercent) );
        PointF pRight1 = new( clientRectangle.X + clientRectangle.Width, ( float ) (clientRectangle.Y + minPercent) );
        PointF pRight2 = new( clientRectangle.X + clientRectangle.Width, ( float ) (clientRectangle.Y + clientRectangle.Height - minPercent) );
        PointF pCornerA1 = new( clientRectangle.X, ( float ) (clientRectangle.Y + minHalfPercent) );
        PointF pCornerA2 = new( ( float ) (clientRectangle.X + minHalfPercent), clientRectangle.Y );
        PointF pCornerB1 = new( ( float ) (clientRectangle.X + clientRectangle.Width - minHalfPercent), clientRectangle.Y );
        PointF pCornerB2 = new( clientRectangle.X + clientRectangle.Width, ( float ) (clientRectangle.Y + minHalfPercent) );
        PointF pCornerC1 = new( clientRectangle.X + clientRectangle.Width, ( float ) (clientRectangle.Y + clientRectangle.Height - minHalfPercent) );
        PointF pCornerC2 = new( ( float ) (clientRectangle.X + clientRectangle.Width - minHalfPercent), clientRectangle.Y + clientRectangle.Height );
        PointF pCornerD1 = new( ( float ) (clientRectangle.X + minHalfPercent), clientRectangle.Y + clientRectangle.Height );
        PointF pCornerD2 = new( clientRectangle.X, ( float ) (clientRectangle.Y + clientRectangle.Height - minHalfPercent) );
        if ( this._backStyle != ShapeBackStyle.Transparent || this._fillStyle != ShapeFillStyle.Transparent )
        {
            using GraphicsPath gPath = new();
            gPath.AddLine( pUp1, pUp2 );
            gPath.AddBezier( pUp2, pCornerB1, pCornerB2, pRight1 );
            gPath.AddLine( pRight1, pRight2 );
            gPath.AddBezier( pRight2, pCornerC1, pCornerC2, pDown1 );
            gPath.AddLine( pDown1, pDown2 );
            gPath.AddBezier( pDown2, pCornerD1, pCornerD2, pLeft1 );
            gPath.AddLine( pLeft1, pLeft2 );
            gPath.AddBezier( pLeft2, pCornerA1, pCornerA2, pUp1 );
            using Region region = new( gPath );
            g.FillRegion( this._fillBrush, region );
        }

        g.DrawLine( this._borderPen, pUp1, pUp2 );
        g.DrawLine( this._borderPen, pDown1, pDown2 );
        g.DrawLine( this._borderPen, pLeft1, pLeft2 );
        g.DrawLine( this._borderPen, pRight1, pRight2 );
        g.DrawBezier( this._borderPen, pLeft2, pCornerA1, pCornerA2, pUp1 );
        g.DrawBezier( this._borderPen, pUp2, pCornerB1, pCornerB2, pRight1 );
        g.DrawBezier( this._borderPen, pRight2, pCornerC1, pCornerC2, pDown1 );
        g.DrawBezier( this._borderPen, pDown2, pCornerD1, pCornerD2, pLeft1 );
    }

    /// <summary> Draws a circle. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="clientRectangle"> The region where to draw. </param>
    /// <param name="g">               The GDI used to draw the rectangle. </param>
    private void DrawCircle( Rectangle clientRectangle, Graphics g )
    {
        int maxDiameter = Math.Min( clientRectangle.Height, clientRectangle.Width );
        Rectangle newClientRectangle = new( clientRectangle.Location.X + ((clientRectangle.Width - maxDiameter) / 2), clientRectangle.Location.Y + ((clientRectangle.Height - maxDiameter) / 2), maxDiameter, maxDiameter );
        if ( this._backStyle != ShapeBackStyle.Transparent || this._fillStyle != ShapeFillStyle.Transparent )
        {
            g.FillEllipse( this._fillBrush, newClientRectangle );
        }

        g.DrawEllipse( this._borderPen, newClientRectangle );
    }

    /// <summary> Draws an oval. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="clientRectangle"> The region where to draw. </param>
    /// <param name="g">               The GDI used to draw the rectangle. </param>
    private void DrawOval( Rectangle clientRectangle, Graphics g )
    {
        if ( this.BackStyle != ShapeBackStyle.Transparent || this.FillStyle != ShapeFillStyle.Transparent )
        {
            g.FillEllipse( this._fillBrush, clientRectangle );
        }

        g.DrawEllipse( this._borderPen, clientRectangle );
    }

    /// <summary> Draws a square. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="clientRectangle"> The region where to draw. </param>
    /// <param name="g">               The GDI used to draw the rectangle. </param>
    private void DrawSquare( Rectangle clientRectangle, Graphics g )
    {
        int maxDiameter = Math.Min( clientRectangle.Height, clientRectangle.Width );
        Rectangle newClientRectangle = new( clientRectangle.Location.X + ((clientRectangle.Width - maxDiameter) / 2), clientRectangle.Location.Y + ((clientRectangle.Height - maxDiameter) / 2), maxDiameter, maxDiameter );
        if ( this._backStyle != ShapeBackStyle.Transparent || this._fillStyle != ShapeFillStyle.Transparent )
        {
            g.FillRectangle( this._fillBrush, newClientRectangle );
        }

        g.DrawRectangle( this._borderPen, newClientRectangle );
    }

    /// <summary> Draws a rectangle. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="clientRectangle"> The region where to draw. </param>
    /// <param name="g">               The GDI used to draw the rectangle. </param>
    private void DrawRectangle( Rectangle clientRectangle, Graphics g )
    {
        if ( this.BackStyle != ShapeBackStyle.Transparent || this.FillStyle != ShapeFillStyle.Transparent )
        {
            g.FillRectangle( this._fillBrush, clientRectangle );
        }

        g.DrawRectangle( this._borderPen, clientRectangle );
    }

    /// <summary> Overriding <see cref="CreateParams"/> method from UserControl. </summary>
    /// <value> Options that control the create. </value>
    protected override CreateParams CreateParams
    {
        get
        {
            CreateParams cp = base.CreateParams;
            cp.ExStyle |= 0x20; // WS_EX_TRANSPARENT
            return cp;
        }
    }

    /// <summary> Overriding OnPaintBackground method from UserControl. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaintBackground( PaintEventArgs e )
    {
        // do not allow the background to be painted  
    }
    #endregion
}
/// <summary> Values that represent Shape Style. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum ShapeStyle
{
    /// <summary> An enum constant representing the rectangle option. </summary>
    Rectangle = 0,

    /// <summary> An enum constant representing the square option. </summary>
    Square = 1,

    /// <summary> An enum constant representing the oval option. </summary>
    Oval = 2,

    /// <summary> An enum constant representing the circle option. </summary>
    Circle = 3,

    /// <summary> An enum constant representing the round rectangle option. </summary>
    RoundRectangle = 4,

    /// <summary> An enum constant representing the round square option. </summary>
    RoundSquare = 5
}
/// <summary> Values that represent BackStyle. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum ShapeBackStyle
{
    /// <summary> An enum constant representing the transparent option. </summary>
    Transparent = 0,

    /// <summary> An enum constant representing the opaque option. </summary>
    Opaque = 1
}
/// <summary> Values that represent ShapeBorderStyle. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum ShapeBorderStyle
{
    /// <summary> An enum constant representing the none option. </summary>
    None,

    /// <summary> An enum constant representing the solid option. </summary>
    Solid,

    /// <summary> An enum constant representing the dash option. </summary>
    Dash,

    /// <summary> An enum constant representing the dot option. </summary>
    Dot,

    /// <summary> An enum constant representing the dash dot option. </summary>
    DashDot,

    /// <summary> An enum constant representing the dash dot option. </summary>
    DashDotDot
}
/// <summary> Values that represent ShapeFillStyle. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum ShapeFillStyle
{
    /// <summary> An enum constant representing the solid option. </summary>
    Solid = 0,

    /// <summary> An enum constant representing the transparent option. </summary>
    Transparent = 1,

    /// <summary> An enum constant representing the horizontal line option. </summary>
    HorizontalLine = 2,

    /// <summary> An enum constant representing the vertical line option. </summary>
    VerticalLine = 3,

    /// <summary> An enum constant representing the downward diagonal option. </summary>
    DownwardDiagonal = 4,

    /// <summary> An enum constant representing the upward diagonal option. </summary>
    UpwardDiagonal = 5,

    /// <summary> An enum constant representing the cross option. </summary>
    Cross = 6,

    /// <summary> An enum constant representing the diagonal cross option. </summary>
    DiagonalCross = 7
}
