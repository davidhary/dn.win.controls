using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A shape control. </summary>
/// <remarks>
/// (c) 2016 Yang Kok Wah. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-05-14 </para><para>
/// http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
/// </remarks>
public class ShapeControl : Control
{
    #region " construction and cleanup "

    /// <summary>
    /// Initializes a new instance of the <see cref="Control" /> class with
    /// default settings.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public ShapeControl() : base()
    {
        // This call is required by the Windows.Forms Form Designer.
        this.InitializeComponent();
        this.DoubleBuffered = true;
        // Using Double Buffer allow for smooth rendering 
        // minimizing flickering
        this.SetStyle( ControlStyles.SupportsTransparentBackColor | ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true );

        // set the default back color and font
        this.BackColor = Color.FromArgb( 0, 255, 255, 255 );
        this.Font = new Font( "Arial", 12f, FontStyle.Bold );
        this.Font = new Font( "Arial", 12f, FontStyle.Bold );
    }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="Control" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    /// resources; <see langword="false" /> to release only unmanaged
    /// resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( disposing )
        {
            this._outline?.Dispose();
            this._components?.Dispose();
        }

        base.Dispose( disposing );
    }

    #endregion

    #region " component designer generated code"

    /// <summary> Initializes the component. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    [MemberNotNull( nameof( _components ) )]
    [MemberNotNull( nameof( _timer1 ) )]
    [MemberNotNull( nameof( _timer2 ) )]
    private void InitializeComponent()
    {
        // Me.DoubleBuffered = True
        this._components = new Container();
        this._timer1 = new Timer( this._components );
        this._timer2 = new Timer( this._components );
        this.SuspendLayout();
        // 
        // timer1
        // 
        this._timer1.Interval = 200;
        this._timer1.Tick += new EventHandler( this.Timer1_Tick );
        // 
        // timer2
        // 
        this._timer2.Interval = 200;
        this._timer2.Tick += new EventHandler( this.Timer2_Tick );

        MouseEnter += new EventHandler( this.ShapeControl_MouseEnter );
        MouseLeave += new EventHandler( this.ShapeControl_MouseLeave );

        // 
        // CustomControl1
        // 
        TextChanged += new EventHandler( this.ShapeControl_TextChanged );
        this.ResumeLayout( false );


    }
    private Container _components;
    private Timer _timer1;
    private Timer _timer2;

    #endregion

    /// <summary> Additional user-defined data. </summary>
    /// <value> The tag 2. </value>
    [Category( "Shape" )]
    [Description( "Additional user-defined data" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string Tag2 { get; set; } = string.Empty;

    private bool _blink;

    /// <summary> Causes the control to blink. </summary>
    /// <value> The blink. </value>
    [Category( "Shape" )]
    [Description( "Causes the control to blink" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool Blink
    {
        get => this._blink;
        set
        {
            this._blink = value;
            this._timer1.Enabled = this._blink;
            if ( !this._blink )
            {
                this.Visible = true;
            }
        }
    }

    private bool _verticallyOffset;
    private bool _vibrate;

    /// <summary> Causes the control to vibrate. </summary>
    /// <value> The vibrate. </value>
    [Category( "Shape" )]
    [Description( "Causes the control to vibrate" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool Vibrate
    {
        get => this._vibrate;
        set
        {
            this._vibrate = value;
            this._timer2.Enabled = this._vibrate;
            if ( !this._vibrate )
            {
                if ( this._verticallyOffset )
                {
                    this.Top += 5;
                    this._verticallyOffset = false;
                }
            }
        }
    }

    private Bitmap? _shapeImage;

    /// <summary> Background Image to define outline. </summary>
    /// <value> The shape image. </value>
    [Category( "Shape" )]
    [Description( "Background Image to define outline" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image? ShapeImage
    {
        get => this._shapeImage;
        set
        {
            if ( value is not null )
            {
                this._shapeImage = ( Bitmap ) value.Clone();
                this.Width = 150;
                this.Height = 150;
                this.OnResize( null );
            }
            else
            {
                if ( this._shapeImage is not null )
                {
                    this._shapeImage = null;
                }

                this.OnResize( null );
            }
        }
    }

    private bool _isTextSet;

    /// <summary> Gets or sets the text associated with this control. </summary>
    /// <value> The text associated with this control. </value>
    [Category( "Shape" )]
    [Description( "Text to display" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public override string Text
    {
        get => base.Text;

#pragma warning disable IDE0079
#pragma warning disable CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).
        set
        {
            base.Text = value;

            // When Visual Studio first create a new control, text=name
            // we do not want any default text, thus we override it with blank
            if ( !this._isTextSet && base.Text.Equals( this.Name, StringComparison.Ordinal ) )
            {
                base.Text = string.Empty;
            }

            this._isTextSet = true;
        }
#pragma warning restore CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).
#pragma warning restore IDE0079
    }

    /// <summary> Gets or sets the background color for the control. </summary>
    /// <value>
    /// A <see cref="Color" /> that represents the background color of the control.
    /// The default is the value of the
    /// <see cref="Control.DefaultBackColor" /> property.
    /// </value>
    [Category( "Shape" )]
    [Description( "Back Color" )]
    [Browsable( true )]
    [Editor( typeof( ColorEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public override Color BackColor
    {
        get => base.BackColor;
        set
        {
            base.BackColor = value;
            this.Refresh();
        }
    }


    private bool _useMouseEventColors;

    /// <summary>   Use Mouse Enter and Leave colors. </summary>
    /// <value> True if use mouse event colors, false if not. </value>
    [Category( "Shape" )]
    [Description( "Use Mouse event to change background colors" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool UseMouseEventColors
    {
        get => this._useMouseEventColors;
        set
        {
            this._useMouseEventColors = value;
            this.Refresh();
        }
    }

    /// <summary>   Event handler. Called by ShapeControl for mouse enter events. </summary>
    /// <remarks>   David, 2021-03-17. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ShapeControl_MouseEnter( object? sender, EventArgs e )
    {
        if ( this.UseMouseEventColors )
            this.BackColor = this.MouseEnterBackColor;
    }

    /// <summary>   Event handler. Called by ShapeControl for mouse leave events. </summary>
    /// <remarks>   David, 2021-03-17. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ShapeControl_MouseLeave( object? sender, EventArgs e )
    {
        if ( this.UseMouseEventColors )
            this.BackColor = this.MouseLeaveBackColor;
    }

    private Color _mouseEnterBackColor = SystemColors.Control;

    /// <summary>   MouseEnter Back Color. </summary>
    /// <value> The color of the MouseEnter back. </value>
    [Category( "Shape" )]
    [Description( "Mouse Enter Back Color" )]
    [Browsable( true )]
    [Editor( typeof( ColorEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color MouseEnterBackColor
    {
        get => this._mouseEnterBackColor;
        set
        {
            this._mouseEnterBackColor = value;
            this.Refresh();
        }
    }

    private Color _mouseLeaveBackColor = SystemColors.Control;

    /// <summary>   MouseLeave Back Color. </summary>
    /// <value> The color of the MouseLeave back. </value>
    [Category( "Shape" )]
    [Description( "Mouse Leave Back Color" )]
    [Browsable( true )]
    [Editor( typeof( ColorEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color MouseLeaveBackColor
    {
        get => this._mouseLeaveBackColor;
        set
        {
            this._mouseLeaveBackColor = value;
            this.Refresh();
        }
    }

    private bool _useGradient;

    /// <summary> Using Gradient to fill Shape. </summary>
    /// <value> The use gradient. </value>
    [Category( "Shape" )]
    [Description( "Using Gradient to fill Shape" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool UseGradient
    {
        get => this._useGradient;
        set
        {
            this._useGradient = value;
            this.Refresh();
        }
    }

    private Color _centerColor = Color.FromArgb( 100, 255, 0, 0 );

    /// <summary> Color at center. </summary>
    /// <value> The color of the center. </value>
    /// <remarks> For Gradient Rendering, this is the color at the center of the shape </remarks>
    [Category( "Shape" )]
    [Description( "Color at center" )]
    [Browsable( true )]
    [Editor( typeof( ColorEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color CenterColor
    {
        get => this._centerColor;
        set
        {
            this._centerColor = value;
            this.Refresh();
        }
    }

    private Color _surroundColor = Color.FromArgb( 100, 0, 255, 255 );

    /// <summary> Color at the edges of the Shape. </summary>
    /// <value> The color of the surround. </value>
    /// <remarks> For Gradient Rendering, this is the color at the edges of the shape</remarks>
    [Category( "Shape" )]
    [Description( "Color at the edges of the Shape" )]
    [Browsable( true )]
    [Editor( typeof( ColorEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color SurroundColor
    {
        get => this._surroundColor;
        set
        {
            this._surroundColor = value;
            this.Refresh();
        }
    }

    private int _borderWidth = 3;

    /// <summary> Border Width. </summary>
    /// <value> The width of the border. </value>
    [Category( "Shape" )]
    [Description( "Border Width" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int BorderWidth
    {
        get => this._borderWidth;
        set
        {
            this._borderWidth = value;
            if ( this._borderWidth < 0 )
            {
                this._borderWidth = 0;
            }
            this.Refresh();
        }
    }

    private Color _borderColor = Color.FromArgb( 255, 255, 0, 0 );

    /// <summary> Border Color. </summary>
    /// <value> The color of the border. </value>
    [Category( "Shape" )]
    [Description( "Border Color" )]
    [Browsable( true )]
    [Editor( typeof( ColorEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color BorderColor
    {
        get => this._borderColor;
        set
        {
            this._borderColor = value;
            this.Refresh();
        }
    }

    private DashStyle _borderStyle = DashStyle.Solid;

    /// <summary> Border Style. </summary>
    /// <value> The border style. </value>
    [Category( "Shape" )]
    [Description( "Border Style" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public DashStyle BorderStyle
    {
        get => this._borderStyle;
        set
        {
            this._borderStyle = value;
            this.Refresh();
        }
    }

    private ShapeType _shape = ShapeType.Rectangle;

    /// <summary> Select Shape. </summary>
    /// <value> The shape. </value>
    [Category( "Shape" )]
    [Description( "Select Shape" )]
    [Browsable( true )]
    [Editor( typeof( ShapeTypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ShapeType Shape
    {
        get => this._shape;
        set
        {
            this._shape = value;
            this.OnResize( null );
        }
    }

    /// <summary> Updates the outline. </summary>
    /// <remarks> David, 2020-09-24.
    /// This function creates the path for each shape It is also being used by the
    /// ShapeTypeEditor to create the various shapes for the Shape property editor UI.
    /// </remarks>
    /// <param name="outline"> [in,out] The outline. </param>
    /// <param name="shape">   The shape. </param>
    /// <param name="width">   The width. </param>
    /// <param name="height">  The height. </param>
    internal static void UpdateOutline( ref GraphicsPath outline, ShapeType shape, int width, int height )
    {
        switch ( shape )
        {
            case ShapeType.CustomPie:
                {
                    outline.AddPie( 0, 0, width, height, 180f, 270f );
                    break;
                }

            case ShapeType.CustomPolygon:
                {
                    outline.AddPolygon( [new( 0, 0 ), new( width / 2, height / 4 ), new( width, 0 ), new( width * 3 / 4, height / 2 ), new( width, height ), new( width / 2, height * 3 / 4 ), new( 0, height ), new( width / 4, height / 2 )] );
                    break;
                }

            case ShapeType.Diamond:
                {
                    outline.AddPolygon( [new( 0, height / 2 ), new( width / 2, 0 ), new( width, height / 2 ), new( width / 2, height )] );
                    break;
                }

            case ShapeType.Rectangle:
                {
                    outline.AddRectangle( new Rectangle( 0, 0, width, height ) );
                    break;
                }

            case ShapeType.Ellipse:
                {
                    outline.AddEllipse( 0, 0, width, height );
                    break;
                }

            case ShapeType.TriangleUp:
                {
                    outline.AddPolygon( [new( 0, height ), new( width, height ), new( width / 2, 0 )] );
                    break;
                }

            case ShapeType.TriangleDown:
                {
                    outline.AddPolygon( [new( 0, 0 ), new( width, 0 ), new( width / 2, height )] );
                    break;
                }

            case ShapeType.TriangleLeft:
                {
                    outline.AddPolygon( [new( width, 0 ), new( 0, height / 2 ), new( width, height )] );
                    break;
                }

            case ShapeType.TriangleRight:
                {
                    outline.AddPolygon( [new( 0, 0 ), new( width, height / 2 ), new( 0, height )] );
                    break;
                }

            case ShapeType.RoundedRectangle:
                {
                    outline.AddArc( 0, 0, width / 4, width / 4, 180f, 90f );
                    outline.AddLine( width / 8, 0, width - (width / 8), 0 );
                    outline.AddArc( width - (width / 4), 0, width / 4, width / 4, 270f, 90f );
                    outline.AddLine( width, width / 8, width, height - (width / 8) );
                    outline.AddArc( width - (width / 4), height - (width / 4), width / 4, width / 4, 0f, 90f );
                    outline.AddLine( width - (width / 8), height, width / 8, height );
                    outline.AddArc( 0, height - (width / 4), width / 4, width / 4, 90f, 90f );
                    outline.AddLine( 0, height - (width / 8), 0, width / 8 );
                    break;
                }

            case ShapeType.BalloonSW:
                {
                    outline.AddArc( 0, 0, width / 4, width / 4, 180f, 90f );
                    outline.AddLine( width / 8, 0, width - (width / 8), 0 );
                    outline.AddArc( width - (width / 4), 0, width / 4, width / 4, 270f, 90f );
                    outline.AddLine( width, width / 8, width, (height * 0.75f) - (width / 8) );
                    outline.AddArc( width - (width / 4), (height * 0.75f) - (width / 4), width / 4, width / 4, 0f, 90f );
                    outline.AddLine( width - (width / 8), height * 0.75f, (width / 8) + (width / 4), height * 0.75f );
                    outline.AddLine( (width / 8) + (width / 4), height * 0.75f, (width / 8) + (width / 8), height );
                    outline.AddLine( (width / 8) + (width / 8), height, (width / 8) + (width / 8), height * 0.75f );
                    outline.AddLine( (width / 8) + (width / 8), height * 0.75f, width / 8, height * 0.75f );
                    outline.AddArc( 0f, (height * 0.75f) - (width / 4), width / 4, width / 4, 90f, 90f );
                    outline.AddLine( 0f, (height * 0.75f) - (width / 8), 0f, width / 8 );
                    break;
                }

            case ShapeType.BalloonSE:
                {
                    outline.AddArc( 0, 0, width / 4, width / 4, 180f, 90f );
                    outline.AddLine( width / 8, 0, width - (width / 8), 0 );
                    outline.AddArc( width - (width / 4), 0, width / 4, width / 4, 270f, 90f );
                    outline.AddLine( width, width / 8, width, (height * 0.75f) - (width / 8) );
                    outline.AddArc( width - (width / 4), (height * 0.75f) - (width / 4), width / 4, width / 4, 0f, 90f );
                    outline.AddLine( width - (width / 8), height * 0.75f, width - (width / 4), height * 0.75f );
                    outline.AddLine( width - (width / 4), height * 0.75f, width - (width / 4), height );
                    outline.AddLine( width - (width / 4), height, width - (3 * width / 8), height * 0.75f );
                    outline.AddLine( width - (3 * width / 8), height * 0.75f, width / 8, height * 0.75f );
                    outline.AddArc( 0f, (height * 0.75f) - (width / 4), width / 4, width / 4, 90f, 90f );
                    outline.AddLine( 0f, (height * 0.75f) - (width / 8), 0f, width / 8 );
                    break;
                }

            case ShapeType.BalloonNW:
                {
                    outline.AddArc( width - (width / 4), height - (width / 4), width / 4, width / 4, 0f, 90f );
                    outline.AddLine( width - (width / 8), height, width - (width / 4), height );
                    outline.AddArc( 0, height - (width / 4), width / 4, width / 4, 90f, 90f );
                    outline.AddLine( 0f, height - (width / 8), 0f, (height * 0.25f) + (width / 8) );
                    outline.AddArc( 0f, height * 0.25f, width / 4, width / 4, 180f, 90f );
                    outline.AddLine( width / 8, height * 0.25f, width / 4, height * 0.25f );
                    outline.AddLine( width / 4, height * 0.25f, width / 4, 0f );
                    outline.AddLine( width / 4, 0f, 3 * width / 8, height * 0.25f );
                    outline.AddLine( 3 * width / 8, height * 0.25f, width - (width / 8), height * 0.25f );
                    outline.AddArc( width - (width / 4), height * 0.25f, width / 4, width / 4, 270f, 90f );
                    outline.AddLine( width, (width / 8) + (height * 0.25f), width, height - (width / 8) );
                    break;
                }

            case ShapeType.BalloonNE:
                {
                    outline.AddArc( width - (width / 4), height - (width / 4), width / 4, width / 4, 0f, 90f );
                    outline.AddLine( width - (width / 8), height, width - (width / 4), height );
                    outline.AddArc( 0, height - (width / 4), width / 4, width / 4, 90f, 90f );
                    outline.AddLine( 0f, height - (width / 8), 0f, (height * 0.25f) + (width / 8) );
                    outline.AddArc( 0f, height * 0.25f, width / 4, width / 4, 180f, 90f );
                    outline.AddLine( width / 8, height * 0.25f, 5 * width / 8, height * 0.25f );
                    outline.AddLine( 5 * width / 8, height * 0.25f, 3 * width / 4, 0f );
                    outline.AddLine( 3 * width / 4, 0f, 3 * width / 4, height * 0.25f );
                    outline.AddLine( 3 * width / 4, height * 0.25f, width - (width / 8), height * 0.25f );
                    outline.AddArc( width - (width / 4), height * 0.25f, width / 4, width / 4, 270f, 90f );
                    outline.AddLine( width, (width / 8) + (height * 0.25f), width, height - (width / 8) );
                    break;
                }

            default:
                {
                    break;
                }
        }
    }

    private GraphicsPath _outline = new();

    /// <summary> Raises the <see cref="Control.Resize" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnResize( EventArgs? e )
    {
        if ( this.Width < 0 || this.Height <= 0 ) return;

        if ( this._shapeImage is null )
        {
            this._outline = new GraphicsPath();
            UpdateOutline( ref this._outline, this._shape, this.Width, this.Height );
        }
        else
        {
            using Bitmap bm = ( Bitmap ) this._shapeImage.Clone();
            using Bitmap bm2 = new( this.Width, this.Height );
            Debug.WriteLine( bm2.Width + "," + bm2.Height );
            Graphics.FromImage( bm2 ).DrawImage( bm, new RectangleF( 0f, 0f, bm2.Width, bm2.Height ), new RectangleF( 0f, 0f, bm.Width, bm.Height ), GraphicsUnit.Pixel );
            OutlineTrace trace = new();
            string s = trace.TraceOutlineN( bm2, 0, bm2.Height / 2, bm2.Width / 2, Color.Black, Color.White, true, 1 );
            Point[] p = OutlineTrace.StringOutline2Polygon( s );
            this._outline = new GraphicsPath();
            this._outline.AddPolygon( p );
        }

        if ( this._outline is not null )
        {
            this.Region = new Region( this._outline );
        }

        this.Refresh();
        if ( e is not null )
            base.OnResize( e );
    }

    /// <summary> Raises the <see cref="Control.Paint" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs e )
    {
        if ( e is null ) return;
        // Rendering with Gradient
        if ( this._useGradient )
        {
            using PathGradientBrush br = new( this._outline )
            {
                CenterColor = this._centerColor,
                SurroundColors = [this._surroundColor]
            };
            e.Graphics.FillPath( br, this._outline );
        }

        // Rendering with Border
        if ( this._borderWidth > 0 )
        {
            using Pen p = new( this._borderColor, this._borderWidth * 2 )
            {
                DashStyle = this._borderStyle
            };
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            e.Graphics.DrawPath( p, this._outline );
        }

        // Rendering the text to be at the center of the shape
        using ( StringFormat sf = new() )
        {
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;
            switch ( this._shape )
            {
                case ShapeType.BalloonNE:
                case ShapeType.BalloonNW:
                    {
                        using SolidBrush br = new( this.ForeColor );
                        e.Graphics.DrawString( this.Text, this.Font, br, new RectangleF( 0f, this.Height * 0.25f, this.Width, this.Height * 0.75f ), sf );

                        break;
                    }

                case ShapeType.BalloonSE:
                case ShapeType.BalloonSW:
                    {
                        using SolidBrush br = new( this.ForeColor );
                        e.Graphics.DrawString( this.Text, this.Font, br, new RectangleF( 0f, 0f, this.Width, this.Height * 0.75f ), sf );

                        break;
                    }

                case ShapeType.Rectangle:
                    break;
                case ShapeType.RoundedRectangle:
                    break;
                case ShapeType.Diamond:
                    break;
                case ShapeType.Ellipse:
                    break;
                case ShapeType.TriangleUp:
                    break;
                case ShapeType.TriangleDown:
                    break;
                case ShapeType.TriangleLeft:
                    break;
                case ShapeType.TriangleRight:
                    break;
                case ShapeType.CustomPolygon:
                    break;
                case ShapeType.CustomPie:
                    break;
                default:
                    {
                        using SolidBrush br = new( this.ForeColor );
                        e.Graphics.DrawString( this.Text, this.Font, br, new Rectangle( 0, 0, this.Width, this.Height ), sf );

                        break;
                    }
            }
        }
        // Calling the base class OnPaint
        base.OnPaint( e );
    }

    /// <summary> Event handler. Called by ShapeControl for text changed events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void ShapeControl_TextChanged( object? sender, EventArgs e )
    {
        this.Refresh();
    }

    /// <summary> Event handler. Called by Timer1 for tick events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Timer1_Tick( object? sender, EventArgs e )
    {
        this.Visible = !this.Visible;
    }

    /// <summary> Event handler. Called by Timer2 for tick events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void Timer2_Tick( object? sender, EventArgs e )
    {
        if ( !this._vibrate ) return;

        this._verticallyOffset = !this._verticallyOffset;
        this.Top = this._verticallyOffset ? this.Top - 5 : this.Top + 5;
    }
}
/// <summary> Values that represent shape types. </summary>
/// <remarks> David, 2020-09-24. All the defined shape type </remarks>
public enum ShapeType
{
    /// <summary> An enum constant representing the rectangle option. </summary>
    Rectangle,

    /// <summary> An enum constant representing the rounded rectangle option. </summary>
    RoundedRectangle,

    /// <summary> An enum constant representing the diamond option. </summary>
    Diamond,

    /// <summary> An enum constant representing the ellipse option. </summary>
    Ellipse,

    /// <summary> An enum constant representing the triangle up option. </summary>
    TriangleUp,

    /// <summary> An enum constant representing the triangle down option. </summary>
    TriangleDown,

    /// <summary> An enum constant representing the triangle left option. </summary>
    TriangleLeft,

    /// <summary> An enum constant representing the triangle right option. </summary>
    TriangleRight,

    /// <summary> An enum constant representing the balloon NW option. </summary>
    BalloonNE,

    /// <summary> An enum constant representing the balloon nw option. </summary>
    BalloonNW,

    /// <summary> An enum constant representing the balloon Software option. </summary>
    BalloonSW,

    /// <summary> An enum constant representing the balloon se option. </summary>
    BalloonSE,

    /// <summary> An enum constant representing the custom polygon option. </summary>
    CustomPolygon,

    /// <summary> An enum constant representing the custom pie option. </summary>
    CustomPie
}
