using System;
using System.Runtime.CompilerServices;

namespace cc.isr.WinControls.Demo;
public partial class Form3
{
    /// <summary>
/// Required designer variable.
/// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
/// Clean up any resources being used.
/// </summary>
/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if ( disposing )
        {
            components?.Dispose();
        }

        base.Dispose(disposing);
    }

    /// <summary>
    /// Required method for Designer support - do not modify the contents of this method with the
    /// code editor.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        Panel1 = new System.Windows.Forms.Panel();
        BtnImportMap = new System.Windows.Forms.Button();
        BtnAddCam = new System.Windows.Forms.Button();
        openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
        Button1 = new System.Windows.Forms.Button();
        Button2 = new System.Windows.Forms.Button();
        label1 = new System.Windows.Forms.Label();
        label2 = new System.Windows.Forms.Label();
        label3 = new System.Windows.Forms.Label();
        toolTip1 = new System.Windows.Forms.ToolTip(components);
        SuspendLayout();
        // 
        // panel1
        // 
        Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
        Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        Panel1.Location = new System.Drawing.Point(24, 98);
        Panel1.Name = "_panel1";
        Panel1.Size = new System.Drawing.Size(652, 430);
        Panel1.TabIndex = 0;
        Panel1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler( Panel1_MouseDoubleClick );
        // 
        // btnImportMap
        // 
        BtnImportMap.Location = new System.Drawing.Point(24, 12);
        BtnImportMap.Name = "_btnImportMap";
        BtnImportMap.Size = new System.Drawing.Size(57, 27);
        BtnImportMap.TabIndex = 1;
        BtnImportMap.Text = "Load";
        BtnImportMap.UseVisualStyleBackColor = true;
        BtnImportMap.Click += new EventHandler( BtnImportMap_Click );
        // 
        // btnAddCam
        // 
        BtnAddCam.Location = new System.Drawing.Point(107, 12);
        BtnAddCam.Name = "_btnAddCam";
        BtnAddCam.Size = new System.Drawing.Size(61, 26);
        BtnAddCam.TabIndex = 2;
        BtnAddCam.Text = "Add Cam";
        BtnAddCam.UseVisualStyleBackColor = true;
        BtnAddCam.Click += new EventHandler( BtnAddCam_Click );
        // 
        // openFileDialog1
        // 
        openFileDialog1.FileName = "openFileDialog1";
        // 
        // button1
        // 
        Button1.Location = new System.Drawing.Point(513, 11);
        Button1.Name = "_button1";
        Button1.Size = new System.Drawing.Size(56, 27);
        Button1.TabIndex = 3;
        Button1.Text = "Save";
        Button1.UseVisualStyleBackColor = true;
        Button1.Click += new EventHandler( Button1_Click );
        // 
        // button2
        // 
        Button2.Location = new System.Drawing.Point(455, 11);
        Button2.Name = "_button2";
        Button2.Size = new System.Drawing.Size(52, 27);
        Button2.TabIndex = 4;
        Button2.Text = "New";
        Button2.UseVisualStyleBackColor = true;
        Button2.Click += new EventHandler( Button2_Click );
        // 
        // label1
        // 
        label1.AutoSize = true;
        label1.Location = new System.Drawing.Point(87, 51);
        label1.MaximumSize = new System.Drawing.Size(550, 30);
        label1.Name = "label1";
        label1.Size = new System.Drawing.Size(35, 13);
        label1.TabIndex = 5;
        label1.Text = "label1";
        // 
        // label2
        // 
        label2.AutoSize = true;
        label2.Location = new System.Drawing.Point(24, 542);
        label2.MaximumSize = new System.Drawing.Size(600, 30);
        label2.Name = "label2";
        label2.Size = new System.Drawing.Size(35, 13);
        label2.TabIndex = 6;
        label2.Text = "label2";
        // 
        // label3
        // 
        label3.AutoSize = true;
        label3.Location = new System.Drawing.Point(24, 51);
        label3.Name = "label3";
        label3.Size = new System.Drawing.Size(57, 13);
        label3.TabIndex = 7;
        label3.Text = "File Name:";
        // 
        // Form3
        // 
        AutoScaleDimensions = new System.Drawing.SizeF(6.0f, 13.0f);
        AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        ClientSize = new System.Drawing.Size(705, 580);
        Controls.Add(label3);
        Controls.Add(label2);
        Controls.Add(label1);
        Controls.Add(Button2);
        Controls.Add(Button1);
        Controls.Add(BtnAddCam);
        Controls.Add(BtnImportMap);
        Controls.Add(Panel1);
        KeyPreview = true;
        MinimumSize = new System.Drawing.Size(640, 480);
        Name = "Form3";
        Text = "Cam Map Designer";
        ResumeLayout(false);
        PerformLayout();
    }

    private System.Windows.Forms.Panel Panel1;
    private System.Windows.Forms.Button BtnImportMap;
    private System.Windows.Forms.Button BtnAddCam;
    private System.Windows.Forms.OpenFileDialog openFileDialog1;
    private System.Windows.Forms.Button Button1;
    private System.Windows.Forms.Button Button2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.ToolTip toolTip1;
}
