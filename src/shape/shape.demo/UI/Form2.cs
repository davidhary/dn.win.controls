namespace cc.isr.WinControls.Demo;

/// <summary>   A form 2. </summary>
/// <remarks>   David, 2021-03-12. </remarks>
public partial class Form2 : Form
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Form" /> class.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public Form2() => this.InitializeComponent();

    /// <summary>   Check box b checked changed. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void CheckBoxB_CheckedChanged( object? sender, EventArgs e )
    {
        if ( sender is Control ctrl )
        {
            string tag = ctrl.Tag?.ToString() ?? string.Empty;
            Control[] controls = this.panel1.Controls.Find( "customControl1" + tag, false );
            if ( controls.Length > 0 )
            {
                ShapeControl cam = ( ShapeControl ) controls[0];
                cam.Blink = (( CheckBox ) sender).Checked;
            }
        }
    }

    /// <summary>   Check box v checked changed. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void CheckBoxV_CheckedChanged( object? sender, EventArgs e )
    {
        if ( sender is Control ctrl )
        {
            string tag = ctrl.Tag?.ToString() ?? string.Empty;
            Control[] controls = this.panel1.Controls.Find( "customControl1" + tag, false );
            if ( controls.Length > 0 )
            {
                ShapeControl cam = ( ShapeControl ) controls[0];
                cam.Vibrate = (( CheckBox ) sender).Checked;
            }
        }
    }
}
