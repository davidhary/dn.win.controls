using System.Reflection;

namespace cc.isr.WinControls.Demo;

/// <summary>   Summary description for Form1. </summary>
/// <remarks>   David, 2021-03-12. </remarks>
public partial class Form1 : Form
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Form" /> class.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public Form1()
    {
        base.Load += this.Form1_Load;

        // 
        // Required for Windows Form Designer support
        // 
        this.InitializeComponent();

        // 
        // TODO: Add any constructor code after InitializeComponent call
        // 
        this.bm = new Bitmap( this.panel1.Width, this.panel1.Height );
        Graphics g = Graphics.FromImage( this.bm );
        g.FillRectangle( Brushes.LightBlue, new Rectangle( 0, 0, this.panel1.Width, this.panel1.Height ) );
        using ( StringFormat sf = new() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center } )
        {
            using Font arialFont = new( "Arial", 18f, FontStyle.Bold );
            g.DrawString( "THIS IS THE TEST BACKGROUND", arialFont, Brushes.Brown, new Rectangle( new Point( 0, 0 ), new Size( this.panel1.Width, this.panel1.Height ) ), sf );
        }

        this.panel1.BackgroundImage = this.bm;
    }

    /// <summary>   Clean up any resources being used. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
    ///                             resources; <see langword="false" /> to release only unmanaged
    ///                             resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( disposing )
        {
            this.components?.Dispose();
            this.bm?.Dispose();
        }

        base.Dispose( disposing );
    }

    /// <summary>   Shape control 1 mouse down. </ummary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void ShapeControl1_MouseDown( object? sender, MouseEventArgs e )
    {
        if ( e.Button.Equals( MouseButtons.Left ) )
        {
            this.sx = e.X;
            // temp region = ((ShapeControl)sender).Region.Clone();
            // ((ShapeControl)sender).Region = null;
            this.sy = e.Y;
        }
    }

    /// <summary>   Shape control 1 mouse move. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void ShapeControl1_MouseMove( object? sender, MouseEventArgs e )
    {
        if ( e.Button.Equals( MouseButtons.Left ) )
        {
            if ( sender is Control ctrl )
            {
                ctrl.Left += e.X - this.sx;
                ctrl.Top += e.Y - this.sy;
            }
        }
    }

    /// <summary>   Event handler. Called by Form1 for load events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Form1_Load( object? sender, EventArgs e )
    {
        this.DoubleBuffered = true;
        _ = typeof( Panel ).InvokeMember( nameof( this.DoubleBuffered ), BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty, null, this.panel1, [true], System.Globalization.CultureInfo.InvariantCulture );
    }

    /// <summary>   Shape control 4 click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ShapeControl4_Click( object? sender, EventArgs e )
    {
        this.shapeControl5.Visible = true;
        this.Refresh();
        System.Threading.Thread.Sleep( 1000 );
        this.shapeControl5.Visible = false;
        this.Refresh();
    }

    /// <summary>   Shape control 3 click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ShapeControl3_Click( object? sender, EventArgs e )
    {
        this.Close();
    }

}
