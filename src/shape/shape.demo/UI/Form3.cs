using System.Reflection;

namespace cc.isr.WinControls.Demo;

/// <summary>   A form 3. </summary>
/// <remarks>   David, 2021-03-12. </remarks>
public partial class Form3 : Form
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Form" /> class.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public Form3()
    {
        /// <summary> Form 3 key down. </summary>
        /// <remarks> David, 2021-03-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Key event information. </param>
        base.KeyDown += this.Form3_KeyDown;

        /// <summary> Form 3 key up. </summary>
        /// <remarks> David, 2021-03-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Key event information. </param>
        base.KeyUp += this.Form3_KeyUp;
        base.Load += this.Form3_Load;

        /// <summary> Form 3 resize. </summary>
        /// <remarks> David, 2021-03-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        base.Resize += this.Form3_Resize;
        this.InitializeComponent();
    }
    /// <summary>   List of controls. </summary>

    private List<ShapeControl> _controlList = [];

    /// <summary>   The sx. </summary>
    private int _sx;

    /// <summary>   The sy. </summary>
    private int _sy;

    /// <summary>   The static i. </summary>
    private int _static_i;

    /// <summary>   True to control key. </summary>
    private bool _ctrlKey;

    /// <summary>   True to alternate key. </summary>
    private bool _altKey;

    /// <summary>   True to plus key. </summary>
    private bool _plusKey;

    /// <summary>   True to minus key. </summary>
    private bool _minusKey;

    /// <summary>   Button add camera click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void BtnAddCam_Click( object? sender, EventArgs e )
    {
        this.AddCam( "" );
    }

    /// <summary>   Gets the zero-based index of the next camera. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <returns>   The next camera index. </returns>
    private int GetNextCamIndex()
    {
        if ( this._controlList.Count == 0 )
        {
            return 1;
        }

        object tempVar;
        tempVar = this._controlList.OrderBy( x => x.Name ).ToList();
        this._controlList = ( List<ShapeControl> ) tempVar;
        this._controlList = [.. this._controlList.OrderBy( x => x.Name.Length )];
        List<ShapeControl> tempList = [.. this._controlList];
        int count = tempList.Count;
        int result = count + 1;

        // find missing index
        for ( int i = 0, loopTo = count - 1; i <= loopTo; i++ )
        {
            string ctrlName = tempList[i].Name;
            string ctrlIndex = ctrlName[3..];
            if ( i + 1 != int.Parse( ctrlIndex, System.Globalization.CultureInfo.CurrentCulture ) )
            {
                result = i + 1;
                break;
            }
        }

        return result;
    }

    /// <summary>   Adds a camera. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="camInfo">  The camInfo. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity" )]
    private void AddCam( string camInfo )
    {
        bool bNew = string.IsNullOrEmpty( camInfo );
        string name = string.Empty;
        string tag = string.Empty;
        string tag2 = string.Empty;
        int x = 0;
        int y = 0;
        int w = 0;
        int h = 0;
        int c = 0;
        if ( !string.IsNullOrEmpty( camInfo ) )
        {
            string[] info = camInfo.Split( '|' );
            for ( int i = 0, loopTo = info.Length - 1; i <= loopTo; i++ )
            {
                string[] details = info[i].Split( '=' );
                switch ( details[0] ?? "" )
                {
                    case "name":
                        {
                            name = details[1];
                            break;
                        }

                    case "x":
                        {
                            x = int.Parse( details[1], System.Globalization.CultureInfo.CurrentCulture );
                            break;
                        }

                    case "y":
                        {
                            y = int.Parse( details[1], System.Globalization.CultureInfo.CurrentCulture );
                            break;
                        }

                    case "w":
                        {
                            w = int.Parse( details[1], System.Globalization.CultureInfo.CurrentCulture );
                            break;
                        }

                    case "h":
                        {
                            h = int.Parse( details[1], System.Globalization.CultureInfo.CurrentCulture );
                            break;
                        }

                    case "c":
                        {
                            c = int.Parse( details[1], System.Globalization.CultureInfo.CurrentCulture );
                            break;
                        }

                    case "tag":
                        {
                            tag = details[1];
                            break;
                        }

                    case "tag2":
                        {
                            tag2 = details[1];
                            break;
                        }

                    default:
                        break;
                }
            }
        }

        // ctrlList.Add(ctrl1);

        ShapeControl ctrl1 = new()
        {
            BackColor = bNew ? Color.FromArgb( 126, Color.Red ) : Color.FromArgb( c ),
            Blink = false,
            BorderColor = Color.FromArgb( 0, 255, 255, 255 ),
            BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid,
            BorderWidth = 3,
            Font = new Font( "Arial", 8.0f, FontStyle.Bold ),
            Name = bNew ? "cam" + this.GetNextCamIndex() : name,
            Shape = ShapeType.Ellipse,
            ShapeImage = Properties.Resources.camshape,
            Size = bNew ? new Size( 40, 40 ) : new Size( w, h ),
            TabIndex = 0,
            UseGradient = false,
            Vibrate = false,
            Visible = true
        };
        // ctrl1.CenterColor = System.Drawing.Color.FromARGB(((int)(((byte)(100)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        // ctrlList.Count;
        // ctrl1.SurroundColor = System.Drawing.Color.FromARGB(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));

        ctrl1.MouseDown += new MouseEventHandler( this.Ctrl1_MouseDown );
        ctrl1.MouseMove += new MouseEventHandler( this.Ctrl1_MouseMove );
        ctrl1.MouseDoubleClick += new MouseEventHandler( this.Ctrl1_MouseDoubleClick );
        ctrl1.MouseHover += new EventHandler( this.Ctrl1_MouseHover );
        this._controlList.Add( ctrl1 );
        int yPos = 50 * this._controlList.Count % this.Panel1.Height;
        int xPos = 50 * this._controlList.Count / this.Panel1.Height * 50;
        ctrl1.Location = bNew ? new Point( 50 + xPos, yPos - 40 ) : new Point( 50, 50 );
        this.Panel1.Controls.Add( ctrl1 );
        ctrl1.Text = "cam";
        ctrl1.Text = bNew ? ( string ) ctrl1.Name.ToString().Clone() : name;
        ctrl1.BringToFront();
        ctrl1.Tag2 = bNew ? "127.0.0.1:New cam" : tag2;
        // set the color
        if ( bNew )
        {
            this.Ctrl1_MouseDoubleClick( ctrl1, new MouseEventArgs( MouseButtons.Left, 2, 0, 0, 0 ) );
        }

        float dy = ctrl1.Top + (ctrl1.Height / 2) - (this.Panel1.Height / 2f);
        float dx = ctrl1.Left + (ctrl1.Width / 2) - (this.Panel1.Width / 2f);
        ctrl1.Tag = bNew ? dx + "," + dy + "," + this.GetNumPixelForImageDisplayed() : tag;
    }

    /// <summary>   Event handler. Called by Ctrl1 for mouse hover events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Ctrl1_MouseHover( object? sender, EventArgs e )
    {
        if ( sender is not ShapeControl ctrl )
            return;
        this.toolTip1.Show( ctrl.Tag2 + ",(" + ctrl.Left + "," + ctrl.Top + ")", ctrl, 2000 );
    }

    /// <summary>   Event handler. Called by Ctrl1 for mouse double click events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void Ctrl1_MouseDoubleClick( object? sender, MouseEventArgs e )
    {
        if ( e.Clicks < 2 ) return;

        if ( sender is not ShapeControl ctrl )
            return;
        if ( e.Button.Equals( MouseButtons.Left ) )
        {
            if ( this._plusKey && !this._minusKey )
            {
                if ( ctrl.Width < 80 )
                {
                    ctrl.Size = new Size( ctrl.Width + 5, ctrl.Height + 5 );
                }

                this._plusKey = false;
                return;
            }

            if ( this._minusKey && !this._plusKey )
            {
                if ( ctrl.Width > 20 )
                {
                    ctrl.Size = new Size( ctrl.Width - 5, ctrl.Height - 5 );
                }

                this._minusKey = false;
                return;
            }

            if ( this._ctrlKey && !this._altKey )
            {
                DialogResult dr = MessageBox.Show( this, "Delete cam?", "Delete", MessageBoxButtons.OKCancel );
                if ( dr == DialogResult.OK )
                {
                    _ = this._controlList.Remove( ctrl );
                    this.Panel1.Controls.Remove( ctrl );
                }

                this._ctrlKey = false;
                return;
            }

            if ( this._altKey && !this._ctrlKey )
            {
                ctrl.Vibrate = !ctrl.Vibrate;
                this._altKey = false;
                return;
            }

            if ( this._static_i >= 6 )
            {
                this._static_i = 0;
            }

            switch ( this._static_i )
            {
                case 0:
                    {
                        ctrl.BackColor = Color.FromArgb( 126, Color.Red );
                        break;
                    }

                case 1:
                    {
                        ctrl.BackColor = Color.FromArgb( 126, Color.Blue );
                        break;
                    }

                case 2:
                    {
                        ctrl.BackColor = Color.FromArgb( 126, Color.Green );
                        break;
                    }

                case 3:
                    {
                        ctrl.BackColor = Color.FromArgb( 126, Color.Wheat );
                        break;
                    }

                case 4:
                    {
                        ctrl.BackColor = Color.FromArgb( 126, Color.GreenYellow );
                        break;
                    }

                case 5:
                    {
                        ctrl.BackColor = Color.FromArgb( 126, Color.Cyan );
                        break;
                    }

                default:
                    break;
            }

            this._static_i += 1;
        }
    }

    /// <summary>   Event handler. Called by Ctrl1 for mouse down events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void Ctrl1_MouseDown( object? sender, MouseEventArgs e )
    {
        if ( e.Button.Equals( MouseButtons.Left ) )
        {
            this._sx = e.X;
            this._sy = e.Y;
        }

        if ( sender is ShapeControl && e.Button.Equals( MouseButtons.Right ) )
        {
            FormProperty frm = new() { Caller = (sender as ShapeControl)! };
            _ = frm.ShowDialog();
        }
    }

    /// <summary>   Event handler. Called by Ctrl1 for mouse move events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void Ctrl1_MouseMove( object? sender, MouseEventArgs e )
    {
        if ( sender is not Control ctrl )
            return;
        if ( e.Button.Equals( MouseButtons.Left ) )
        {
            ctrl.Left += e.X - this._sx;
            ctrl.Top += e.Y - this._sy;
            float dy = ctrl.Top + ( float ) (ctrl.Height / 2) - (this.Panel1.Height / 2f);
            float dx = ctrl.Left + ( float ) (ctrl.Width / 2) - (this.Panel1.Width / 2f);
            ctrl.Tag = dx + "," + dy + "," + this.GetNumPixelForImageDisplayed();
        }
    }

    /// <summary>   Event handler. Called by Form3 for key down events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Key event information. </param>
    private void Form3_KeyDown( object? sender, KeyEventArgs e )
    {
        this._ctrlKey = e.Control;
        this._altKey = e.Alt;
        if ( e.KeyCode == Keys.OemMinus )
        {
            this._minusKey = true;
        }

        if ( e.KeyCode == Keys.Oemplus )
        {
            this._plusKey = true;
        }
    }

    /// <summary>   Event handler. Called by Form3 for key up events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Key event information. </param>
    private void Form3_KeyUp( object? sender, KeyEventArgs e )
    {
        this._ctrlKey = false;
        this._altKey = false;
        this._minusKey = false;
        this._plusKey = false;
    }

    /// <summary>   Panel 1 mouse double click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Mouse event information. </param>
    private void Panel1_MouseDoubleClick( object? sender, MouseEventArgs e )
    {
        this.openFileDialog1.Filter = "Image files (*.bmp;*.jpg;*.gif)|*.bmp;*.jpg;*.gif|All files (*.*)|*.*";
        DialogResult dr = this.openFileDialog1.ShowDialog();
        if ( dr == DialogResult.OK )
        {
            try
            {
                Bitmap tempImage = new( this.openFileDialog1.FileName );
                this.Panel1.BackgroundImage = new Bitmap( tempImage );
            }
            catch
            {
            }
        }
    }

    /// <summary>   Gets number pixel for image displayed. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <returns>   The number pixel for image displayed. </returns>
    private int GetNumPixelForImageDisplayed()
    {
        if ( this.Panel1.BackgroundImage is null )
            return default;
        float panelRatio = this.Panel1.Width / ( float ) this.Panel1.Height;
        float imgRatio = this.Panel1.BackgroundImage.Width / ( float ) this.Panel1.BackgroundImage.Height;
        float displayWidth;
        float displayHeight;
        if ( panelRatio > imgRatio )
        {
            // height limiting
            displayHeight = this.Panel1.Height;
            displayWidth = imgRatio * displayHeight;
        }
        else
        {
            displayWidth = this.Panel1.Width;
            displayHeight = displayWidth / imgRatio;
        }

        // System.Diagnostics.Debug.Print(imgRatio +"," + displayWidth + "," + displayHeight);

        return ( int ) Math.Round( Math.Truncate( displayWidth * displayHeight ) );
    }

    /// <summary>   Button 2 click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Button2_Click( object? sender, EventArgs e )
    {
        this.label1.Text = "NewMap_" + Guid.NewGuid().ToString() + ".map";
        this.Panel1.BackgroundImage = new Bitmap( this.Panel1.Width, this.Panel1.Height );
        Graphics.FromImage( this.Panel1.BackgroundImage ).FillRectangle( Brushes.White, new Rectangle( 0, 0, this.Panel1.Width, this.Panel1.Height ) );
        Graphics.FromImage( this.Panel1.BackgroundImage ).DrawString( "Dbl Click here to insert floor plan..", new Font( FontFamily.GenericSansSerif, 12f ), Brushes.Black, 50f, 50f );
        this._controlList.Clear();
        this.Panel1.Controls.Clear();
    }

    /// <summary>   Button 1 click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Button1_Click( object? sender, EventArgs e )
    {
        using ( StreamWriter writer = File.CreateText( this.label1.Text ) )
        {
            this._controlList = [.. this._controlList.OrderBy( x => x.Name )];
            this._controlList = [.. this._controlList.OrderBy( x => x.Name.Length )];
            List<ShapeControl> tempList = [.. this._controlList];
            writer.WriteLine( "CAM_COUNT=" + tempList.Count );
            for ( int i = 0, loopTo = this._controlList.Count - 1; i <= loopTo; i++ )
                writer.WriteLine( "name=" + tempList[i].Name + "|" + "x=" + tempList[i].Left + "|" + "y=" + tempList[i].Top + "|" + "w=" + tempList[i].Width + "|" + "h=" + tempList[i].Height + "|" + "c=" + tempList[i].BackColor.ToArgb() + "|" + "tag=" + tempList[i].Tag?.ToString() + "|" + "tag2=" + tempList[i].Tag2.ToString() );
        }

        this.Panel1.BackgroundImage?.Save( this.label1.Text + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg );

        _ = MessageBox.Show( this.label1.Text + " is saved" );
    }

    /// <summary>   Form 3 load. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.Type.InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[])" )]
    private void Form3_Load( object? sender, EventArgs e )
    {
        this.DoubleBuffered = true;

        // invoke double buffer 
        _ = typeof( Panel ).InvokeMember( "DoubleBuffered", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty, null, this.Panel1, [true] );
        this.label2.Text = "On Cam> Right Click:Set Properties, Dbl_Click:Change Color, Ctl+Dbl_Click:Del, Alt+Dbl_Click:Vibrate, Minus+Dbl_Click:Smaller, Plus+Dbl_Click:Larger";
        this.Button2_Click( this.Button2, EventArgs.Empty );
    }

    /// <summary>   Button import map click. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void BtnImportMap_Click( object? sender, EventArgs e )
    {
        this.openFileDialog1.Filter = "Map files (*.map)|*.map";
        DialogResult dr = this.openFileDialog1.ShowDialog();
        if ( dr == DialogResult.OK )
        {
            this.Button2_Click( this.Button2, EventArgs.Empty );
            this.label1.Text = this.openFileDialog1.FileName;
            try
            {
                using ( StreamReader reader = File.OpenText( this.label1.Text ) )
                {
                    string s = reader.ReadLine() ?? string.Empty;
                    string[] info = s.Split( '=' );
                    for ( int i = 0, loopTo = int.Parse( info[1], System.Globalization.CultureInfo.CurrentCulture ) - 1; i <= loopTo; i++ )
                    {
                        s = reader.ReadLine() ?? string.Empty;
                        this.AddCam( s );
                    }
                }

                if ( File.Exists( this.openFileDialog1.FileName + ".jpg" ) )
                {
                    using Bitmap tempImage = new( this.openFileDialog1.FileName + ".jpg" );
                    this.Panel1.BackgroundImage = new Bitmap( tempImage );
                }


                // resize


                this.UpdateCamPosAfterResize();
            }
            catch
            {
            }
        }
    }

    /// <summary>   Updates the camera position after resize. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    private void UpdateCamPosAfterResize()
    {
        int newArea = this.GetNumPixelForImageDisplayed();
        for ( int i = 0, loopTo = this._controlList.Count - 1; i <= loopTo; i++ )
        {
            string[] info = (this._controlList[i].Tag?.ToString() ?? string.Empty).Split( ',' );
            float dx = float.Parse( info[0], System.Globalization.CultureInfo.CurrentCulture );
            float dy = float.Parse( info[1], System.Globalization.CultureInfo.CurrentCulture );
            int area = int.Parse( info[2], System.Globalization.CultureInfo.CurrentCulture );

            // square root of area ratio = linear ratio
            float ratio = ( float ) Math.Sqrt( newArea / ( float ) area );
            // get the new offset using the calculated linear ratio
            float newDX = ratio * dx;
            float newDY = ratio * dy;


            // update the new pos for the cam 
            this._controlList[i].Left = ( int ) Math.Round( Math.Truncate( (this.Panel1.Width / 2) + newDX - (this._controlList[i].Width / 2) ) );
            this._controlList[i].Top = ( int ) Math.Round( Math.Truncate( (this.Panel1.Height / 2) + newDY - (this._controlList[i].Height / 2) ) );
        }
    }

    /// <summary>   Event handler. Called by Form3 for resize events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Form3_Resize( object? sender, EventArgs e )
    {
        this.Panel1.Visible = false;
        this.Panel1.Height = this.ClientSize.Height - (3 * this.Panel1.Top / 2);
        this.Panel1.Width = this.ClientSize.Width - (2 * this.Panel1.Left);
        this.label2.Top = this.Panel1.Top + this.Panel1.Height + 10;
        this.UpdateCamPosAfterResize();
        this.Panel1.Visible = true;
    }
}
