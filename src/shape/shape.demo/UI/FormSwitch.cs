namespace cc.isr.WinControls.Demo;

/// <summary>   A form switch. </summary>
/// <remarks>   David, 2021-03-12. </remarks>
public partial class FormSwitch : Form
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Form" /> class.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public FormSwitch() => this.InitializeComponent();

    /// <summary>   Event handler. Called by Button1 for click events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Button1_Click( object? sender, EventArgs e )
    {
        Form1 f1 = new();
        _ = f1.ShowDialog();
    }

    /// <summary>   Event handler. Called by Button2 for click events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Button2_Click( object? sender, EventArgs e )
    {
        Form2 f2 = new();
        _ = f2.ShowDialog();
    }

    /// <summary>   Event handler. Called by Button3 for click events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void Button3_Click( object? sender, EventArgs e )
    {
        Form3 f3 = new();
        _ = f3.ShowDialog();
    }
}
