using System;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace cc.isr.WinControls.Demo;
public partial class Form1
{
    /// <summary>   Required designer variable. </summary>
    private System.ComponentModel.Container components = null;

    private ShapeControl ShapeControl1;
    private System.Windows.Forms.Panel panel1;
    private Bitmap bm;
    private int sx;
    private int sy;
    private ShapeControl ShapeControl3;
    private ShapeControl ShapeControl4;
    private ShapeControl shapeControl5;
    private ShapeControl shapeControl7;
    private ShapeControl shapeControl6;
    private ShapeControl customControl11;
    private ShapeControl shapeControl2;

    /// <summary>
/// Required method for Designer support - do not modify
/// the contents of this method with the code editor.
/// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
        this.panel1 = new System.Windows.Forms.Panel();
        this.ShapeControl1 = new cc.isr.WinControls.ShapeControl();
        this.shapeControl5 = new cc.isr.WinControls.ShapeControl();
        this.customControl11 = new cc.isr.WinControls.ShapeControl();
        this.shapeControl7 = new cc.isr.WinControls.ShapeControl();
        this.ShapeControl4 = new cc.isr.WinControls.ShapeControl();
        this.ShapeControl3 = new cc.isr.WinControls.ShapeControl();
        this.shapeControl2 = new cc.isr.WinControls.ShapeControl();
        this.shapeControl6 = new cc.isr.WinControls.ShapeControl();
        this.panel1.SuspendLayout();
        this.SuspendLayout();
        // 
        // panel1
        // 
        this.panel1.Controls.Add(this.ShapeControl1);
        this.panel1.Location = new System.Drawing.Point(10, 20);
        this.panel1.Name = "panel1";
        this.panel1.Size = new System.Drawing.Size(464, 394);
        this.panel1.TabIndex = 2;
        // 
        // _shapeControl1
        // 
        this.ShapeControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
        this.ShapeControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.ShapeControl1.Blink = false;
        this.ShapeControl1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        this.ShapeControl1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
        this.ShapeControl1.BorderWidth = 0;
        this.ShapeControl1.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
        this.ShapeControl1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
        this.ShapeControl1.ForeColor = System.Drawing.Color.Yellow;
        this.ShapeControl1.Location = new System.Drawing.Point(66, 48);
        this.ShapeControl1.MouseEnterBackColor = System.Drawing.SystemColors.Control;
        this.ShapeControl1.MouseLeaveBackColor = System.Drawing.SystemColors.Control;
        this.ShapeControl1.Name = "_ShapeControl1";
        this.ShapeControl1.Shape = ShapeType.Diamond;
        this.ShapeControl1.ShapeImage = null;
        this.ShapeControl1.Size = new System.Drawing.Size(157, 176);
        this.ShapeControl1.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        this.ShapeControl1.TabIndex = 0;
        this.ShapeControl1.Tag2 = "";
        this.ShapeControl1.Text = "Transparency Test Drag Me Around";
        this.ShapeControl1.UseGradient = true;
        this.ShapeControl1.UseMouseEventColors = false;
        this.ShapeControl1.Vibrate = false;
        this.ShapeControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ShapeControl1_MouseDown);
        this.ShapeControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ShapeControl1_MouseMove);
        // 
        // shapeControl5
        // 
        this.shapeControl5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(227)))), ((int)(((byte)(251)))), ((int)(((byte)(72)))));
        this.shapeControl5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.shapeControl5.Blink = false;
        this.shapeControl5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        this.shapeControl5.BorderStyle = System.Drawing.Drawing2D.DashStyle.Dot;
        this.shapeControl5.BorderWidth = 3;
        this.shapeControl5.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        this.shapeControl5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
        this.shapeControl5.Location = new System.Drawing.Point(503, 79);
        this.shapeControl5.MouseEnterBackColor = System.Drawing.SystemColors.Control;
        this.shapeControl5.MouseLeaveBackColor = System.Drawing.SystemColors.Control;
        this.shapeControl5.Name = "shapeControl5";
        this.shapeControl5.Shape = ShapeType.BalloonSE;
        this.shapeControl5.ShapeImage = null;
        this.shapeControl5.Size = new System.Drawing.Size(173, 130);
        this.shapeControl5.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
        this.shapeControl5.TabIndex = 6;
        this.shapeControl5.Tag2 = "";
        this.shapeControl5.Text = "Hello, I am Tommy";
        this.shapeControl5.UseGradient = false;
        this.shapeControl5.UseMouseEventColors = false;
        this.shapeControl5.Vibrate = false;
        this.shapeControl5.Visible = false;
        // 
        // customControl11
        // 
        this.customControl11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
        this.customControl11.Blink = false;
        this.customControl11.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        this.customControl11.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
        this.customControl11.BorderWidth = 0;
        this.customControl11.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        this.customControl11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
        this.customControl11.Location = new System.Drawing.Point(666, 169);
        this.customControl11.MouseEnterBackColor = System.Drawing.SystemColors.Control;
        this.customControl11.MouseLeaveBackColor = System.Drawing.SystemColors.Control;
        this.customControl11.Name = "customControl11";
        this.customControl11.Shape = ShapeType.Ellipse;
        this.customControl11.ShapeImage = null;
        this.customControl11.Size = new System.Drawing.Size(112, 113);
        this.customControl11.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(11)))));
        this.customControl11.TabIndex = 9;
        this.customControl11.Tag2 = "";
        this.customControl11.UseGradient = true;
        this.customControl11.UseMouseEventColors = false;
        this.customControl11.Vibrate = false;
        // 
        // shapeControl7
        // 
        this.shapeControl7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
        this.shapeControl7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.shapeControl7.Blink = false;
        this.shapeControl7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        this.shapeControl7.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
        this.shapeControl7.BorderWidth = 1;
        this.shapeControl7.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        this.shapeControl7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
        this.shapeControl7.Location = new System.Drawing.Point(702, 123);
        this.shapeControl7.MouseEnterBackColor = System.Drawing.SystemColors.Control;
        this.shapeControl7.MouseLeaveBackColor = System.Drawing.SystemColors.Control;
        this.shapeControl7.Name = "shapeControl7";
        this.shapeControl7.Shape = ShapeType.CustomPolygon;
        this.shapeControl7.ShapeImage = null;
        this.shapeControl7.Size = new System.Drawing.Size(24, 27);
        this.shapeControl7.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
        this.shapeControl7.TabIndex = 8;
        this.shapeControl7.Tag2 = "";
        this.shapeControl7.UseGradient = false;
        this.shapeControl7.UseMouseEventColors = false;
        this.shapeControl7.Vibrate = false;
        // 
        // _shapeControl4
        // 
        this.ShapeControl4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(92)))), ((int)(((byte)(159)))), ((int)(((byte)(83)))));
        this.ShapeControl4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.ShapeControl4.Blink = false;
        this.ShapeControl4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(131)))), ((int)(((byte)(255)))), ((int)(((byte)(4)))));
        this.ShapeControl4.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
        this.ShapeControl4.BorderWidth = 3;
        this.ShapeControl4.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        this.ShapeControl4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
        this.ShapeControl4.Location = new System.Drawing.Point(661, 314);
        this.ShapeControl4.MouseEnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(92)))), ((int)(((byte)(159)))), ((int)(((byte)(83)))));
        this.ShapeControl4.MouseLeaveBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(92)))), ((int)(((byte)(159)))), ((int)(((byte)(83)))));
        this.ShapeControl4.Name = "_ShapeControl4";
        this.ShapeControl4.Shape = ShapeType.RoundedRectangle;
        this.ShapeControl4.ShapeImage = null;
        this.ShapeControl4.Size = new System.Drawing.Size(117, 43);
        this.ShapeControl4.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
        this.ShapeControl4.TabIndex = 5;
        this.ShapeControl4.Tag2 = "";
        this.ShapeControl4.Text = "Click Me!";
        this.ShapeControl4.UseGradient = false;
        this.ShapeControl4.UseMouseEventColors = true;
        this.ShapeControl4.Vibrate = false;
        this.ShapeControl4.Click += new System.EventHandler(this.ShapeControl4_Click);
        // 
        // ShapeControl3
        // 
        this.ShapeControl3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(92)))), ((int)(((byte)(159)))), ((int)(((byte)(83)))));
        this.ShapeControl3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.ShapeControl3.Blink = false;
        this.ShapeControl3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(131)))), ((int)(((byte)(255)))), ((int)(((byte)(4)))));
        this.ShapeControl3.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
        this.ShapeControl3.BorderWidth = 3;
        this.ShapeControl3.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        this.ShapeControl3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
        this.ShapeControl3.Location = new System.Drawing.Point(661, 377);
        this.ShapeControl3.MouseEnterBackColor = System.Drawing.SystemColors.Control;
        this.ShapeControl3.MouseLeaveBackColor = System.Drawing.SystemColors.Control;
        this.ShapeControl3.Name = "ShapeControl3";
        this.ShapeControl3.Shape = ShapeType.RoundedRectangle;
        this.ShapeControl3.ShapeImage = null;
        this.ShapeControl3.Size = new System.Drawing.Size(117, 43);
        this.ShapeControl3.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
        this.ShapeControl3.TabIndex = 4;
        this.ShapeControl3.Tag2 = "";
        this.ShapeControl3.Text = "Close";
        this.ShapeControl3.UseGradient = false;
        this.ShapeControl3.UseMouseEventColors = false;
        this.ShapeControl3.Vibrate = false;
        this.ShapeControl3.Click += new System.EventHandler(this.ShapeControl3_Click);
        // 
        // shapeControl2
        // 
        this.shapeControl2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
        this.shapeControl2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.shapeControl2.Blink = false;
        this.shapeControl2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(133)))), ((int)(((byte)(4)))), ((int)(((byte)(9)))));
        this.shapeControl2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
        this.shapeControl2.BorderWidth = 0;
        this.shapeControl2.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(163)))), ((int)(((byte)(126)))), ((int)(((byte)(59)))));
        this.shapeControl2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
        this.shapeControl2.Location = new System.Drawing.Point(623, 111);
        this.shapeControl2.MouseEnterBackColor = System.Drawing.SystemColors.Control;
        this.shapeControl2.MouseLeaveBackColor = System.Drawing.SystemColors.Control;
        this.shapeControl2.Name = "shapeControl2";
        this.shapeControl2.Shape = ShapeType.TriangleUp;
        this.shapeControl2.ShapeImage = null;
        this.shapeControl2.Size = new System.Drawing.Size(182, 39);
        this.shapeControl2.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
        this.shapeControl2.TabIndex = 3;
        this.shapeControl2.Tag2 = "";
        this.shapeControl2.UseGradient = true;
        this.shapeControl2.UseMouseEventColors = false;
        this.shapeControl2.Vibrate = false;
        // 
        // shapeControl6
        // 
        this.shapeControl6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(221)))), ((int)(((byte)(152)))), ((int)(((byte)(53)))));
        this.shapeControl6.Blink = false;
        this.shapeControl6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        this.shapeControl6.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
        this.shapeControl6.BorderWidth = 0;
        this.shapeControl6.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(202)))), ((int)(((byte)(91)))), ((int)(((byte)(171)))));
        this.shapeControl6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
        this.shapeControl6.Location = new System.Drawing.Point(818, 79);
        this.shapeControl6.MouseEnterBackColor = System.Drawing.SystemColors.Control;
        this.shapeControl6.MouseLeaveBackColor = System.Drawing.SystemColors.Control;
        this.shapeControl6.Name = "shapeControl6";
        this.shapeControl6.Shape = ShapeType.Ellipse;
        this.shapeControl6.ShapeImage = ((System.Drawing.Image)(resources.GetObject("shapeControl6.ShapeImage")));
        this.shapeControl6.Size = new System.Drawing.Size(122, 101);
        this.shapeControl6.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(198)))), ((int)(((byte)(74)))), ((int)(((byte)(0)))));
        this.shapeControl6.TabIndex = 9;
        this.shapeControl6.Tag2 = "";
        this.shapeControl6.UseGradient = true;
        this.shapeControl6.UseMouseEventColors = false;
        this.shapeControl6.Vibrate = false;
        // 
        // Form1
        // 
        this.AutoScaleBaseSize = new System.Drawing.Size(6, 16);
        this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.ClientSize = new System.Drawing.Size(845, 459);
        this.ControlBox = false;
        this.Controls.Add(this.shapeControl5);
        this.Controls.Add(this.customControl11);
        this.Controls.Add(this.shapeControl6);
        this.Controls.Add(this.shapeControl7);
        this.Controls.Add(this.ShapeControl4);
        this.Controls.Add(this.ShapeControl3);
        this.Controls.Add(this.shapeControl2);
        this.Controls.Add(this.panel1);
        this.DoubleBuffered = true;
        this.Name = "Form1";
        this.Text = "Test Shape Control";
        this.panel1.ResumeLayout(false);
        this.ResumeLayout(false);

    }
}
