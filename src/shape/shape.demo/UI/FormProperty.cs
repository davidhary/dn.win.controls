using System.ComponentModel;

namespace cc.isr.WinControls.Demo;

/// <summary>   A form property. </summary>
/// <remarks>   David, 2021-03-12. </remarks>
public partial class FormProperty : Form
{
    /// <summary>   The caller. </summary>

    private ShapeControl _caller;

    /// <summary>   Gets or sets the caller. </summary>
    /// <value> The caller. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ShapeControl Caller
    {
        get => this._caller;
        set
        {
            this._caller = value;
            if ( value is not null )
            {
                string s = this._caller.Tag2;
                string[] info = s.Split( ':' );
                this.textBoxIP.Text = info[0];
                this.textBoxNotes.Text = info[1];
                this.Text = this._caller.Text + " properties";
            }
        }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Form" /> class.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public FormProperty()
    {
        this.InitializeComponent();
        this._caller = new ShapeControl();
    }

    /// <summary>   Event handler. Called by FormProperty for form closing events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Form closing event information. </param>
    private void FormProperty_FormClosing( object? sender, FormClosingEventArgs e )
    {
        // should validate first
        this._caller.Tag2 = this.textBoxIP.Text + ":" + this.textBoxNotes.Text;
    }

    /// <summary>   Event handler. Called by FormProperty for activated events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void FormProperty_activated( object? sender, EventArgs e )
    {
        this.Location = this._caller.Location;
    }

    /// <summary>   Event handler. Called by TextBoxIP for enter events. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void TextBoxIP_Enter( object? sender, EventArgs e )
    {
        this.textBoxIP.SelectionStart = this.textBoxIP.Text.Length;
    }
}
