// ------------------------------------------------------------------- 
//                            CRISTI POTLOG                            
//                  Copyright ©2005 - All Rights reserved              
//                                                                     
// THIS SOURCE CODE IS PROVIDED "AS IS" WITH NO WARRANTIES OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE       
// WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR  
// PURPOSE, NONINFRINGEMENT, OR ARISING FROM A COURSE OF DEALING,      
// USAGE OR TRADE PRACTICE.                                            
//                                                                     
// THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.            
// ------------------------------------------------------------------- 
using System.Diagnostics;

namespace cc.isr.WinControls.Demo;

/// <summary>   An example for using the CP wizard control. </summary>
/// <remarks>
/// (c) 2012 Integrated Scientific Resources, Inc., All Rights Reserved <para>
/// (c) 2005 CRISTI POTLOG - All Rights Reserved </para><para>
/// Licensed under the MIT License. </para><para>
/// David, 2012-09-19, 1.05.4645.  </para><para>
/// Based on http://www.CodeProject.com/Articles/10808/Cristi-POTLOG-s-Wizard-Control-for-NET
/// </para>
/// </remarks>
public partial class SimpleWizard : Form
{
    /// <summary>   Creates a new instance of the <see cref="SimpleWizard"/> class. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    public SimpleWizard() =>
        // required for designer support
        this.InitializeComponent();

    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="Form" />.
    /// </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
    ///                             resources; <see langword="false" /> to release only unmanaged
    ///                             resources. </param>
    [DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    /// <summary>   Starts a sample task simulation. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    private void StartTask()
    {
        // setup wizard page
        this._sampleWizard.BackEnabled = false;
        this._sampleWizard.NextEnabled = false;
        this._longTaskProgressBar.Value = this._longTaskProgressBar.Minimum;

        // start timer to simulate a long running task
        this._longTaskTimer.Enabled = true;
    }

    /// <summary>   Handles the AfterSwitchPages event of the wizard form. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Page changed event information. </param>
    private void SampleWizard_afterSwitchPages( object? sender, PageChangedEventArgs e )
    {
        // check if license page
        if ( ReferenceEquals( this._sampleWizard.NewPage, this._licenseWizardPage ) )
        {
            // sync next button's state with check box
            this._sampleWizard.NextEnabled = this._agreeCheckBox.Checked;
        }
        // check if progress page
        else if ( ReferenceEquals( this._sampleWizard.NewPage, this._progressWizardPage ) )
        {
            // start the sample task
            this.StartTask();
        }
    }

    /// <summary>   Handles the BeforeSwitchPages event of the wizard form. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Page changing event information. </param>
    private void SampleWizard_BeforeSwitchPages( object? sender, PageChangingEventArgs e )
    {
        // check if we're going forward from options page
        if ( ReferenceEquals( this._sampleWizard.OldPage, this._optionsWizardPage ) && e.NewIndex > e.OldIndex )
        {
            // check if user selected one option
            if ( !this._checkOptionRadioButton.Checked && !this._skipOptionRadioButton.Checked )
            {
                // display hint & cancel step
                _ = MessageBox.Show( "Please chose one of the options presented.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information );
                e.Cancel = true;
            }
            // check if user chose to skip validation
            else if ( this._skipOptionRadioButton.Checked )
            {
                // skip the validation page
                e.NewIndex += 1;
            }
        }
    }

    /// <summary>   Handles the Cancel event of the wizard form. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Cancel event information. </param>
    private void SampleWizard_Cancel( object? sender, System.ComponentModel.CancelEventArgs e )
    {
        // check if task is running
        bool isTaskRunning = this._longTaskTimer.Enabled;
        // stop the task
        this._longTaskTimer.Enabled = false;

        // ask user to confirm
        if ( MessageBox.Show( "Are you sure you wand to exit the Sample Wizard?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question ) != DialogResult.Yes )
        {
            // cancel closing
            e.Cancel = true;
            // restart the task
            this._longTaskTimer.Enabled = isTaskRunning;
        }
        else
        {
            // ensure parent form is closed (even when ShowDialog is not used)
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }

    /// <summary>   Handles the Finish event of the wizard form. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void SampleWizard_Finish( object? sender, EventArgs e )
    {
        _ = MessageBox.Show( "The Sample Wizard finished successfully.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information );
        // ensure parent form is closed (even when ShowDialog is not used)
        this.Close();
    }

    /// <summary>   Handles the Help event of the wizard form. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void SampleWizard_Help( object? sender, EventArgs e )
    {
        _ = MessageBox.Show( "This is a really cool wizard control!" + Environment.NewLine + ":-)", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information );
    }

    /// <summary>   Handles the CheckedChanged event of checkIAgree. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void CheckIAgree_CheckedChanged( object? sender, EventArgs e )
    {
        // sync next button's state with check box
        this._sampleWizard.NextEnabled = this._agreeCheckBox.Checked;
    }

    /// <summary>   Handles the Tick event of timerTask. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void TimerTask_Tick( object? sender, EventArgs e )
    {
        // check if task completed
        if ( this._longTaskProgressBar.Value == this._longTaskProgressBar.Maximum )
        {
            // stop the timer & switch to next page
            this._longTaskTimer.Enabled = false;
            this._sampleWizard.Next();
        }
        else
        {
            // update progress bar
            this._longTaskProgressBar.PerformStep();
        }
    }

}
