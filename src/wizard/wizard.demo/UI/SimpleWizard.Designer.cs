using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace cc.isr.WinControls.Demo;

public partial class SimpleWizard
{

    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        this._licenseWizardPage = new cc.isr.WinControls.WizardPage();
        this._agreeCheckBox = new System.Windows.Forms.CheckBox();
        this._licenseTextBox = new System.Windows.Forms.TextBox();
        this._optionsWizardPage = new cc.isr.WinControls.WizardPage();
        this._optionsGroupBox = new System.Windows.Forms.GroupBox();
        this._skipOptionRadioButton = new System.Windows.Forms.RadioButton();
        this._checkOptionRadioButton = new System.Windows.Forms.RadioButton();
        this._finishWizardPage = new cc.isr.WinControls.WizardPage();
        this._sampleWizard = new cc.isr.WinControls.Wizard();
        this._welcomeWizardPage = new cc.isr.WinControls.WizardPage();
        this._progressWizardPage = new cc.isr.WinControls.WizardPage();
        this._progressLabel = new System.Windows.Forms.Label();
        this._longTaskProgressBar = new System.Windows.Forms.ProgressBar();
        this._checkWizardPage = new cc.isr.WinControls.WizardPage();
        this._placeholderLabel = new System.Windows.Forms.Label();
        this._longTaskTimer = new System.Windows.Forms.Timer(this.components);
        this._licenseWizardPage.SuspendLayout();
        this._optionsWizardPage.SuspendLayout();
        this._optionsGroupBox.SuspendLayout();
        this._sampleWizard.SuspendLayout();
        this._progressWizardPage.SuspendLayout();
        this._checkWizardPage.SuspendLayout();
        this.SuspendLayout();
        // 
        // _licenseWizardPage
        // 
        this._licenseWizardPage.Controls.Add(this._agreeCheckBox);
        this._licenseWizardPage.Controls.Add(this._licenseTextBox);
        this._licenseWizardPage.Description = "Please read the following license agreement and confirm that you agree with all terms and conditions.";
        this._licenseWizardPage.Location = new System.Drawing.Point(0, 0);
        this._licenseWizardPage.Name = "_LicenseWizardPage";
        this._licenseWizardPage.Size = new System.Drawing.Size(428, 208);
        this._licenseWizardPage.TabIndex = 10;
        this._licenseWizardPage.Title = "License Agreement";
        // 
        // _agreeCheckBox
        // 
        this._agreeCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
        this._agreeCheckBox.Location = new System.Drawing.Point(14, 335);
        this._agreeCheckBox.Name = "_agreeCheckBox";
        this._agreeCheckBox.Size = new System.Drawing.Size(346, 19);
        this._agreeCheckBox.TabIndex = 0;
        this._agreeCheckBox.Text = "I agree with this license\'s terms and conditions.";
        this._agreeCheckBox.CheckedChanged += new System.EventHandler(this.CheckIAgree_CheckedChanged);
        // 
        // _licenseTextBox
        // 
        this._licenseTextBox.BackColor = System.Drawing.SystemColors.Window;
        this._licenseTextBox.Location = new System.Drawing.Point(14, 94);
        this._licenseTextBox.Multiline = true;
        this._licenseTextBox.Name = "_LicenseTextBox";
        this._licenseTextBox.ReadOnly = true;
        this._licenseTextBox.Size = new System.Drawing.Size(528, 231);
        this._licenseTextBox.TabIndex = 1;
        this._licenseTextBox.Text = "Some long and boring license text...";
        // 
        // _optionsWizardPage
        // 
        this._optionsWizardPage.Controls.Add(this._optionsGroupBox);
        this._optionsWizardPage.Description = "Please select an option from the available list.";
        this._optionsWizardPage.Location = new System.Drawing.Point(0, 0);
        this._optionsWizardPage.Name = "_OptionsWizardPage";
        this._optionsWizardPage.Size = new System.Drawing.Size(428, 208);
        this._optionsWizardPage.TabIndex = 11;
        this._optionsWizardPage.Title = "Task Options";
        // 
        // _optionsGroupBox
        // 
        this._optionsGroupBox.Controls.Add(this._skipOptionRadioButton);
        this._optionsGroupBox.Controls.Add(this._checkOptionRadioButton);
        this._optionsGroupBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
        this._optionsGroupBox.Location = new System.Drawing.Point(19, 103);
        this._optionsGroupBox.Name = "_OptionsGroupBox";
        this._optionsGroupBox.Size = new System.Drawing.Size(523, 138);
        this._optionsGroupBox.TabIndex = 0;
        this._optionsGroupBox.TabStop = false;
        this._optionsGroupBox.Text = "Available Options";
        // 
        // _skipOptionRadioButton
        // 
        this._skipOptionRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
        this._skipOptionRadioButton.Location = new System.Drawing.Point(24, 44);
        this._skipOptionRadioButton.Name = "_SkipOptionRadioButton";
        this._skipOptionRadioButton.Size = new System.Drawing.Size(312, 25);
        this._skipOptionRadioButton.TabIndex = 0;
        this._skipOptionRadioButton.Text = "Skip any checks and proceed.";
        // 
        // _checkOptionRadioButton
        // 
        this._checkOptionRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
        this._checkOptionRadioButton.Location = new System.Drawing.Point(24, 89);
        this._checkOptionRadioButton.Name = "_CheckOptionRadioButton";
        this._checkOptionRadioButton.Size = new System.Drawing.Size(312, 24);
        this._checkOptionRadioButton.TabIndex = 1;
        this._checkOptionRadioButton.Text = "Check for something first.";
        // 
        // _finishWizardPage
        // 
        this._finishWizardPage.Description = "Thank you for using the Sample Wizard.\nPress OK to exit.";
        this._finishWizardPage.Location = new System.Drawing.Point(0, 0);
        this._finishWizardPage.Name = "_FinishWizardPage";
        this._finishWizardPage.Size = new System.Drawing.Size(559, 375);
        this._finishWizardPage.TabIndex = 12;
        this._finishWizardPage.Title = "Sample Wizard has finished";
        this._finishWizardPage.WizardPageStyle = WizardPageStyle.Finish;
        // 
        // _sampleWizard
        // 
        this._sampleWizard.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
        this._sampleWizard.Controls.Add(this._welcomeWizardPage);
        this._sampleWizard.Controls.Add(this._finishWizardPage);
        this._sampleWizard.Controls.Add(this._progressWizardPage);
        this._sampleWizard.Controls.Add(this._checkWizardPage);
        this._sampleWizard.Controls.Add(this._optionsWizardPage);
        this._sampleWizard.Controls.Add(this._licenseWizardPage);
        this._sampleWizard.Dock = System.Windows.Forms.DockStyle.None;
        this._sampleWizard.FinishText = null;
        this._sampleWizard.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
        this._sampleWizard.HeaderImage = global::cc.isr.WinControls.Demo.Properties.Resources.HeaderIcon;
        this._sampleWizard.HelpVisible = true;
        this._sampleWizard.Location = new System.Drawing.Point(0, 0);
        this._sampleWizard.Name = "_SampleWizard";
        this._sampleWizard.Pages.AddRange(new cc.isr.WinControls.WizardPage[] {
        this._welcomeWizardPage,
        this._licenseWizardPage,
        this._optionsWizardPage,
        this._checkWizardPage,
        this._progressWizardPage,
        this._finishWizardPage});
        this._sampleWizard.Size = new System.Drawing.Size(456, 326);
        this._sampleWizard.TabIndex = 0;
        this._sampleWizard.WelcomeImage = global::cc.isr.WinControls.Demo.Properties.Resources.WelcomeImage;
        this._sampleWizard.PageChanging += new System.EventHandler<PageChangingEventArgs>( this.SampleWizard_BeforeSwitchPages );
        this._sampleWizard.PageChanged += new System.EventHandler<PageChangedEventArgs>(this.SampleWizard_afterSwitchPages);
        this._sampleWizard.Cancel += new System.ComponentModel.CancelEventHandler(this.SampleWizard_Cancel);
        this._sampleWizard.Finish += new System.EventHandler(this.SampleWizard_Finish);
        this._sampleWizard.Help += new System.EventHandler(this.SampleWizard_Help);
        // 
        // _welcomeWizardPage
        // 
        this._welcomeWizardPage.Description = "This wizard will guide you through the steps of performing a sample task.";
        this._welcomeWizardPage.Location = new System.Drawing.Point(0, 0);
        this._welcomeWizardPage.Name = "_WelcomeWizardPage";
        this._welcomeWizardPage.Size = new System.Drawing.Size(413, 254);
        this._welcomeWizardPage.TabIndex = 9;
        this._welcomeWizardPage.Title = "Welcome to the Sample  Wizard";
        this._welcomeWizardPage.WizardPageStyle = WizardPageStyle.Welcome;
        // 
        // _progressWizardPage
        // 
        this._progressWizardPage.Controls.Add(this._progressLabel);
        this._progressWizardPage.Controls.Add(this._longTaskProgressBar);
        this._progressWizardPage.Description = "This simulates a long running sample task.";
        this._progressWizardPage.Location = new System.Drawing.Point(0, 0);
        this._progressWizardPage.Name = "_ProgressWizardPage";
        this._progressWizardPage.Size = new System.Drawing.Size(428, 208);
        this._progressWizardPage.TabIndex = 10;
        this._progressWizardPage.Title = "Task Running";
        // 
        // _progressLabel
        // 
        this._progressLabel.Location = new System.Drawing.Point(24, 103);
        this._progressLabel.Name = "_ProgressLabel";
        this._progressLabel.Size = new System.Drawing.Size(302, 20);
        this._progressLabel.TabIndex = 1;
        this._progressLabel.Text = "Please wait while the wizard does a long task...";
        // 
        // _longTaskProgressBar
        // 
        this._longTaskProgressBar.Location = new System.Drawing.Point(19, 128);
        this._longTaskProgressBar.Name = "_LongTaskProgressBar";
        this._longTaskProgressBar.Size = new System.Drawing.Size(523, 25);
        this._longTaskProgressBar.TabIndex = 0;
        // 
        // _checkWizardPage
        // 
        this._checkWizardPage.Controls.Add(this._placeholderLabel);
        this._checkWizardPage.Description = "Please enter required information.";
        this._checkWizardPage.Location = new System.Drawing.Point(0, 0);
        this._checkWizardPage.Name = "_CheckWizardPage";
        this._checkWizardPage.Size = new System.Drawing.Size(428, 208);
        this._checkWizardPage.TabIndex = 13;
        this._checkWizardPage.Title = "Check Something";
        // 
        // _placeholderLabel
        // 
        this._placeholderLabel.ForeColor = System.Drawing.Color.Red;
        this._placeholderLabel.Location = new System.Drawing.Point(34, 123);
        this._placeholderLabel.Name = "_PlaceholderLabel";
        this._placeholderLabel.Size = new System.Drawing.Size(460, 20);
        this._placeholderLabel.TabIndex = 0;
        this._placeholderLabel.Text = "Place some validation controls here.";
        // 
        // _longTaskTimer
        // 
        this._longTaskTimer.Tick += new System.EventHandler(this.TimerTask_Tick);
        // 
        // SimpleWizard
        // 
        this.AutoScaleBaseSize = new System.Drawing.Size(6, 16);
        this.ClientSize = new System.Drawing.Size(468, 338);
        this.Controls.Add(this._sampleWizard);
        this.MaximizeBox = false;
        this.MinimizeBox = false;
        this.Name = "SimpleWizard";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "Simple Wizard";
        this._licenseWizardPage.ResumeLayout(false);
        this._licenseWizardPage.PerformLayout();
        this._optionsWizardPage.ResumeLayout(false);
        this._optionsGroupBox.ResumeLayout(false);
        this._sampleWizard.ResumeLayout(false);
        this._progressWizardPage.ResumeLayout(false);
        this._checkWizardPage.ResumeLayout(false);
        this.ResumeLayout(false);

    }

    private CheckBox _agreeCheckBox;
    private TextBox _licenseTextBox;
    private cc.isr.WinControls.WizardPage _licenseWizardPage;
    private cc.isr.WinControls.WizardPage _optionsWizardPage;
    private cc.isr.WinControls.WizardPage _finishWizardPage;
    private cc.isr.WinControls.WizardPage _welcomeWizardPage;
    private cc.isr.WinControls.WizardPage _progressWizardPage;
    private cc.isr.WinControls.Wizard _sampleWizard;
    private ProgressBar _longTaskProgressBar;
    private Label _progressLabel;
    private System.Windows.Forms.Timer _longTaskTimer;
    private GroupBox _optionsGroupBox;
    private RadioButton _skipOptionRadioButton;
    private RadioButton _checkOptionRadioButton;
    private cc.isr.WinControls.WizardPage _checkWizardPage;
    private Label _placeholderLabel;
}
