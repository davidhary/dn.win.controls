using System.ComponentModel;

namespace cc.isr.WinControls;

[DefaultEvent("Click")]
[Designer(typeof(WizardPageDesigner))]
public partial class WizardPage
{ }
