using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;
/// <summary>
/// Represents an extensible wizard control with basic page navigation functionality.
/// </summary>
/// <remarks>
/// (c) 2012 Integrated Scientific Resources, Inc., All Rights Reserved <para>
/// (c) 2005 CRISTI POTLOG - All Rights Reserved </para><para>
/// Licensed under the MIT License. </para><para>
/// David, 2012-09-19, 1.05.4645 </para><para>
/// Based on http://www.CodeProject.com/Articles/10808/Cristi-Potlog-s-Wizard-Control-for-NET
/// </para>
/// </remarks>
[Designer( typeof( WizardDesigner ) )]
public partial class Wizard : UserControl
{
    #region " construction and cleanup "

    /// <summary> Creates a new instance of the <see cref="Wizard"/> class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public Wizard()
    {
        // call required by designer
        this.InitializeComponent();

        // reset control style to improve rendering (reduce flicker)
        this.SetStyle( ControlStyles.AllPaintingInWmPaint, true );
        this.SetStyle( ControlStyles.DoubleBuffer, true );
        this.SetStyle( ControlStyles.ResizeRedraw, true );
        this.SetStyle( ControlStyles.UserPaint, true );

        // reset dock style
        base.Dock = DockStyle.Fill;

        // initialize pages collection
        this.Pages = new WizardPagesCollection( this );
        this.CancelText = "&Cancel";
        this.FinishText = string.Empty;
    }

    #endregion

    #region " constants "

    /// <summary> Height of the footer area. </summary>
    private const int FOOTER_AREA_HEIGHT = 48;

    /// <summary> The offset cancel. </summary>
    private readonly Point _offsetCancel = new( 84, 36 );

    /// <summary> The offset next. </summary>
    private readonly Point _offsetNext = new( 168, 36 );

    /// <summary> The offset back. </summary>
    private readonly Point _offsetBack = new( 244, 36 );

    #endregion

    #region " properties "

    /// <summary> Gets or sets which edge of the parent container a control is docked to. </summary>
    /// <value> The dock. </value>
    [DefaultValue( DockStyle.Fill )]
    [Category( "Layout" )]
    [Description( "Gets or sets which edge of the parent container a control is docked to." )]
    public new DockStyle Dock
    {
        get => base.Dock;
        set => base.Dock = value;
    }

    /// <summary> Gets the collection of wizard pages in this tab control. </summary>
    /// <value> The pages. </value>
    [Category( "Wizard" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
    [Description( "Gets the collection of wizard pages in this tab control." )]
    public WizardPagesCollection Pages { get; private set; }

    /// <summary>
    /// Gets the New page -- the page to be displayed. Selected when the <see cref="PageChanged">page
    /// changed event</see> occurs but before it is invoked.
    /// </summary>
    /// <value> The new page. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public WizardPage? NewPage { get; private set; }

    /// <summary>
    /// Gets the old page -- the page already displayed. Selected when the
    /// <see cref="PageChanged">page changed event</see> occurs but before it is invoked.
    /// </summary>
    /// <value> The old page. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public WizardPage? OldPage { get; private set; }

    private WizardPage? _selectedPage;

    /// <summary> Gets or sets the currently-selected wizard page. </summary>
    /// <value> The selected page. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public WizardPage? SelectedPage
    {
        get => this._selectedPage;
        set =>
            // select new page
            this.ActivatePage( value ?? new WizardPage() );
    }

    /// <summary> Gets the currently-selected wizard page by index. </summary>
    /// <value> The selected index. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal int SelectedIndex
    {
        get => this._selectedPage is null ? -1 : this.Pages.IndexOf( this._selectedPage );
        set
        {
            // check if there are any pages or index out of range.
            if ( this.Pages.Count == 0 || value < -1 || value >= this.Pages.Count )
            {
                // reset invalid index
                this.ActivatePage( -1 );
            }
            else
            {
                // select new page
                this.ActivatePage( value );
            }
        }
    }

    private Image? _headerImage;

    /// <summary> Gets or sets the image displayed on the header of the standard pages. </summary>
    /// <value> The header image. </value>
    [Category( "Wizard" )]
    [Description( "Gets or sets the image displayed on the header of the standard pages." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image? HeaderImage
    {
        get => this._headerImage;
        set
        {
            if ( !ReferenceEquals( this._headerImage, value ) )
            {
                this._headerImage = value;
                this.Invalidate();
            }
        }
    }

    private Image? _welcomeImage;

    /// <summary> Gets or sets the image displayed on the welcome and finish pages. </summary>
    /// <value> The welcome image. </value>
    [Category( "Wizard" )]
    [Description( "Gets or sets the image displayed on the welcome and finish pages." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image? WelcomeImage
    {
        get => this._welcomeImage;
        set
        {
            if ( !ReferenceEquals( this._welcomeImage, value ) )
            {
                this._welcomeImage = value;
                this.Invalidate();
            }
        }
    }

    private Font _headerFont = new( "Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0 );

    /// <summary> Gets or sets the font used to display the description of a standard page. </summary>
    /// <value> The header font. </value>
    [Category( "Appearance" )]
    [Description( "Gets or sets the font used to display the description of a standard page." )]
    public Font HeaderFont
    {
        get => this._headerFont ?? this.Font;
        set
        {
            if ( !ReferenceEquals( this._headerFont, value ) )
            {
                this._headerFont = value;
                this.Invalidate();
            }
        }
    }

    /// <summary> Determine if we should serialize header font. </summary>
    /// <remarks> David, 2020-03-07. </remarks>
    /// <returns> True if it succeeds; otherwise, false. </returns>
    protected bool ShouldSerializeHeaderFont()
    {
        return this._headerFont is not null;
    }

    private Font _headerTitleFont = new( "Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0 );

    /// <summary> Gets or sets the font used to display the title of a standard page. </summary>
    /// <value> The header title font. </value>
    [Category( "Appearance" )]
    [Description( "Gets or sets the font used to display the title of a standard page." )]
    public Font HeaderTitleFont
    {
        get => this._headerTitleFont ?? new Font( this.Font.FontFamily, this.Font.Size + 2f, FontStyle.Bold );
        set
        {
            if ( !ReferenceEquals( this._headerTitleFont, value ) )
            {
                this._headerTitleFont = value;
                this.Invalidate();
            }
        }
    }

    /// <summary> Returns the sentinel indicating if header title font should be serialized. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> True if it succeeds; otherwise, false. </returns>
    protected bool ShouldSerializeHeaderTitleFont()
    {
        return this._headerTitleFont is not null;
    }

    private Font _welcomeFont = new( "Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0 );

    /// <summary>
    /// Gets or sets the font used to display the description of a welcome of finish page.
    /// </summary>
    /// <value> The welcome font. </value>
    [Category( "Appearance" )]
    [Description( "Gets or sets the font used to display the description of a welcome of finish page." )]
    public Font WelcomeFont
    {
        get => this._welcomeFont ?? this.Font;
        set
        {
            if ( !ReferenceEquals( this._welcomeFont, value ) )
            {
                this._welcomeFont = value;
                this.Invalidate();
            }
        }
    }

    /// <summary> Returns the sentinel indicating if welcome font should be serialized. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> True if it succeeds; otherwise, false. </returns>
    protected bool ShouldSerializeWelcomeFont()
    {
        return this._welcomeFont is not null;
    }

    private Font _welcomeTitleFont = new( "Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0 );

    /// <summary>
    /// Gets or sets the font used to display the title of a welcome of finish page.
    /// </summary>
    /// <value> The welcome title font. </value>
    [Category( "Appearance" )]
    [Description( "Gets or sets the font used to display the title of a welcome of finish page." )]
    public Font WelcomeTitleFont
    {
        get => this._welcomeTitleFont ?? new Font( this.Font.FontFamily, this.Font.Size + 10f, FontStyle.Bold );
        set
        {
            if ( !ReferenceEquals( this._welcomeTitleFont, value ) )
            {
                this._welcomeTitleFont = value;
                this.Invalidate();
            }
        }
    }

    /// <summary> Determine if we should serialize welcome title font. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> True if it succeeds; otherwise, false. </returns>
    protected bool ShouldSerializeWelcomeTitleFont()
    {
        return this._welcomeTitleFont is not null;
    }

    /// <summary> Gets or sets the enabled state of the Next button. </summary>
    /// <value> The next enabled. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool NextEnabled
    {
        get => this._nextButton.Enabled;
        set => this._nextButton.Enabled = value;
    }

    /// <summary> Gets or sets the enabled state of the back button. </summary>
    /// <value> The back enabled. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool BackEnabled
    {
        get => this._backButton.Enabled;
        set => this._backButton.Enabled = value;
    }

    /// <summary> Gets or sets the enabled state of the cancel button. </summary>
    /// <value> The cancel enabled. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool CancelEnabled
    {
        get => this._cancelButton.Enabled;
        set => this._cancelButton.Enabled = value;
    }

    /// <summary> Gets or sets the visible state of the help button. </summary>
    /// <value> The help visible. </value>
    [Category( "Behavior" )]
    [DefaultValue( false )]
    [Description( "Gets or sets the visible state of the help button." )]
    public bool HelpVisible
    {
        get => this._helpButton.Visible;
        set => this._helpButton.Visible = value;
    }

    /// <summary> Gets or sets the text displayed by the Next button. </summary>
    /// <value> The next text. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string NextText
    {
        get => this._nextButton.Text;
        set => this._nextButton.Text = value;
    }

    /// <summary> Gets or sets the text displayed by the back button. </summary>
    /// <value> The back text. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string BackText
    {
        get => this._backButton.Text;
        set => this._backButton.Text = value;
    }

    /// <summary> Gets or sets the text displayed by the cancel button. </summary>
    /// <value> The cancel text. </value>
    [Browsable( true )]
    [Category( "Appearance" )]
    [DefaultValue( "&Cancel" )]
    [Description( "Gets or sets the test to display on the title of the Cancel button." )]
    public string CancelText { get; set; }

    /// <summary> Gets or sets the text displayed by the cancel button. </summary>
    /// <value> The help text. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string HelpText
    {
        get => this._helpButton.Text;
        set => this._helpButton.Text = value;
    }

    /// <summary> Gets or sets the text displayed by the Finish button. </summary>
    /// <value> The finish text. </value>
    [Browsable( true )]
    [Category( "Appearance" )]
    [DefaultValue( "&Finish" )]
    [Description( "Gets or sets the test to display on the title of Finish button on the finish page." )]
    public string FinishText { get; set; }

    #endregion

    #region " methods "

    /// <summary>
    /// Advances to next wizard page.
    /// </summary>
    public void Next()
    {
        // check if we're on the last page (finish)
        if ( this.SelectedIndex == this.Pages.Count - 1 )
        {
            this._nextButton.Enabled = false;
        }
        else
        {
            // handle page change
            this.OnPageChanging( new PageChangingEventArgs( this.SelectedIndex, this.SelectedIndex + 1 ) );
        }
    }

    /// <summary> Retreats to the previous wizard page. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void Back()
    {
        if ( this.SelectedIndex == 0 )
        {
            this._backButton.Enabled = false;
        }
        else
        {
            // handle page change
            this.OnPageChanging( new PageChangingEventArgs( this.SelectedIndex, this.SelectedIndex - 1 ) );
        }
    }

    /// <summary> Activates the specified wizard page. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="index"> An Integer value representing the zero-based index of the page to be
    /// activated. </param>
    private void ActivatePage( int index )
    {
        // check if new page is invalid
        if ( index < 0 || index >= this.Pages.Count )
        {
            // filter out
            return;
        }

        // get new page
        WizardPage? page = this.Pages[index];

        // activate page
        if ( page is not null )
            this.ActivatePage( page );
    }

    /// <summary> Activates the specified wizard page. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="page"> A WizardPage object representing the page to be activated. </param>
    private void ActivatePage( WizardPage page )
    {
        // validate given page
        if ( !this.Pages.Contains( page ) )
        {
            // filter out
            return;
        }

        // deactivate current page
        if ( this._selectedPage is not null )
        {
            this._selectedPage.Visible = false;
        }

        // activate new page
        this._selectedPage = page;
        if ( this._selectedPage is not null )
        {
            // Ensure that this panel displays inside the wizard
            this._selectedPage.Parent = this;
            if ( !this.Contains( this._selectedPage ) )
            {
                this.Container?.Add( this._selectedPage );
            }

            if ( this._selectedPage.WizardPageStyle == WizardPageStyle.Finish )
            {
                this._cancelButton.Text = this.FinishText;
                this._cancelButton.DialogResult = DialogResult.OK;
            }
            else
            {
                this._cancelButton.Text = this.CancelText; // "Cancel"
                this._cancelButton.DialogResult = DialogResult.Cancel;
            }

            // Make it fill the space
            this._selectedPage.SetBounds( 0, 0, this.Width, this.Height - FOOTER_AREA_HEIGHT );
            this._selectedPage.Visible = true;
            this._selectedPage.BringToFront();
            FocusFirstTabIndex( this._selectedPage );
        }

        // What should the back button say
        this._backButton.Enabled = this.SelectedIndex > 0;

        // What should the Next button say
        if ( this.SelectedIndex < this.Pages.Count - 1 )
        {
            this._nextButton.Enabled = true;
        }
        else
        {
            if ( !this.DesignMode )
            {
                // at runtime disable back button (we finished; there's no point going back)
                this._backButton.Enabled = false;
            }

            this._nextButton.Enabled = false;
        }

        // refresh
        if ( this._selectedPage is not null )
        {
            this._selectedPage.Invalidate();
        }
        else
        {
            this.Invalidate();
        }
    }

    /// <summary> Focus the control with a lowest tab index in the given container. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="container"> A Control object to process. </param>
    private static void FocusFirstTabIndex( Control container )
    {
        // initialize search result variable
        Control? searchResult = null;

        // find the control with the lowest tab index
        foreach ( Control control in container.Controls )
        {
            if ( control.CanFocus && (searchResult is null || control.TabIndex < searchResult.TabIndex) )
            {
                searchResult = control;
            }
        }

        // check if anything searchResult
        if ( searchResult is not null )
        {
            // focus found control
            _ = searchResult.Focus();
        }
        else
        {
            // focus the container
            _ = container.Focus();
        }
    }

    /// <summary> Raises the <see cref="PageChanging">page changing event</see>. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A WizardPageEventArgs object that holds event data. </param>
    protected virtual void OnPageChanging( PageChangingEventArgs e )
    {
        if ( e is null ) return;

        // get wizard page already displayed
        this.OldPage = this.Pages[e.OldIndex];

        // get wizard page to be displayed
        this.NewPage = this.Pages[e.NewIndex];

        // check if there are subscribers and raise changing event
        PageChanging?.Invoke( this, e );

        // check if user canceled
        if ( e.Cancel )
        {
            // filter
            return;
        }

        // activate new page
        this.ActivatePage( e.NewIndex );

        // raise the changed
        this.OnPageChanged( e );
    }

    /// <summary> Raises the <see cref="PageChanged">page changed event</see>. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A WizardPageEventArgs object that holds event data. </param>
    protected virtual void OnPageChanged( PageChangedEventArgs e )
    {
        if ( e is null ) return;

        // get wizard page already displayed
        this.OldPage = this.Pages[e.OldIndex];

        // get wizard page to be displayed
        this.NewPage = this.Pages[e.NewIndex];

        // check if there are subscribers and raise the changed event
        PageChanged?.Invoke( this, e );
    }

    /// <summary> Raises the Cancel event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A CancelEventArgs object that holds event data. </param>
    protected virtual void OnCancel( CancelEventArgs e )
    {
        if ( e is null ) return;
        // check if there are subscribers and raise Cancel event
        Cancel?.Invoke( this, e );
    }

    /// <summary> Raises the Finish event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A EventArgs object that holds event data. </param>
    protected virtual void OnFinish( EventArgs e )
    {
        if ( e is null ) return;
        // check if there are subscribers and raise Finish event
        Finish?.Invoke( this, e );
    }

    /// <summary> Raises the Help event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A EventArgs object that holds event data. </param>
    protected virtual void OnHelp( EventArgs e )
    {
        if ( e is null ) return;
        // check if there are subscribers and raise the Help event
        Help?.Invoke( this, e );
    }

    /// <summary> Raises the Load event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnLoad( EventArgs e )
    {
        // raise the Load event
        base.OnLoad( e );

        // activate first page, if exists
        if ( this.Pages.Count > 0 )
        {
            this.ActivatePage( 0 );
        }
    }

    /// <summary> Raises the Resize event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnResize( EventArgs e )
    {
        // raise the Resize event
        base.OnResize( e );

        // resize the selected page to fit the wizard
        this._selectedPage?.SetBounds( 0, 0, this.Width, this.Height - FOOTER_AREA_HEIGHT );

        // position navigation buttons
        this._cancelButton.Location = new Point( this.Width - this._offsetCancel.X, this.Height - this._offsetCancel.Y );
        this._nextButton.Location = new Point( this.Width - this._offsetNext.X, this.Height - this._offsetNext.Y );
        this._backButton.Location = new Point( this.Width - this._offsetBack.X, this.Height - this._offsetBack.Y );
        this._helpButton.Location = new Point( this._helpButton.Location.X, this.Height - this._offsetBack.Y );
    }

    /// <summary> Raises the Paint event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs e )
    {
        // raise the Paint event
        base.OnPaint( e );
        if ( e is null ) return;

        Rectangle bottomRect = this.ClientRectangle;
        bottomRect.Y = this.Height - FOOTER_AREA_HEIGHT;
        bottomRect.Height = FOOTER_AREA_HEIGHT;
        ControlPaint.DrawBorder3D( e.Graphics, bottomRect, Border3DStyle.Etched, Border3DSide.Top );
    }

    /// <summary> Raises the ControlAdded event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="ControlEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnControlAdded( ControlEventArgs e )
    {
        // prevent other controls from being added directly to the wizard
        if ( e is null ) return;

        if ( e.Control is not WizardPage && !ReferenceEquals( e.Control, this._cancelButton ) && !ReferenceEquals( e.Control, this._nextButton ) && !ReferenceEquals( e.Control, this._backButton ) )
        {
            // add the control to the selected page
            this._selectedPage?.Controls.Add( e.Control );
        }
        else
        {
            // raise the ControlAdded event
            base.OnControlAdded( e );
        }
    }

    #endregion

    #region " events "

    /// <summary> Event queue for all listeners interested in PageChanging events. </summary>
    /// <remarks>
    /// Occurs before the wizard page changes, giving the user a chance to validate.
    /// </remarks>
    [Category( "Wizard" )]
    [Description( "Occurs before the wizard page changes, giving the user a chance to validate." )]
    public event EventHandler<PageChangingEventArgs>? PageChanging;

    /// <summary> Event queue for all listeners interested in PageChanged events. </summary>
    /// <remarks>
    /// Occurs after the wizard page changed, giving the user a chance to setup the new page.
    /// </remarks>
    [Category( "Wizard" )]
    [Description( "Occurs after the wizard page changed, giving the user a chance to setup the new page." )]
    public event EventHandler<PageChangedEventArgs>? PageChanged;

    /// <summary> Event queue for all listeners interested in Cancel events. </summary>
    /// <remarks>
    /// Occurs when wizard is canceled, giving the user a chance to validate.
    /// </remarks>
    [Category( "Wizard" )]
    [Description( "Occurs when wizard is canceled, giving the user a chance to validate." )]
    public event CancelEventHandler? Cancel;

    /// <summary> Event queue for all listeners interested in Finish events. </summary>
    /// <remarks>
    /// Occurs when wizard is finished, giving the user a chance to do extra stuff.
    /// </remarks>
    [Category( "Wizard" )]
    [Description( "Occurs when wizard is finished, giving the user a chance to do extra stuff." )]
    public event EventHandler? Finish;

    /// <summary> Event queue for all listeners interested in Help events. </summary>
    /// <remarks>
    /// Occurs when the user clicks the help button.
    /// </remarks>
    [Category( "Wizard" )]
    [Description( "Occurs when the user clicks the help button." )]
    public event EventHandler? Help;

    #endregion

    #region " events handlers "

    /// <summary> Handles the Click event of the Next button. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void NextButtonClick( object? sender, EventArgs e )
    {
        this.Next();
    }

    /// <summary> Handles the Click event of the Back button. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void BackButtonClick( object? sender, EventArgs e )
    {
        this.Back();
    }

    /// <summary> Handles the Click event of the Cancel button. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void CancelButtonClick( object? sender, EventArgs e )
    {
        // check if button is cancel mode
        if ( this._cancelButton.DialogResult == DialogResult.Cancel )
        {
            this.OnCancel( new CancelEventArgs() );
        }
        // check if button is finish mode
        else if ( this._cancelButton.DialogResult == DialogResult.OK )
        {
            this.OnFinish( EventArgs.Empty );
        }
    }

    /// <summary> Handles the Click event of the Help button. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void HelpButtonClick( object? sender, EventArgs e )
    {
        this.OnHelp( EventArgs.Empty );
    }

    #endregion

    #region " inner classes "

    /// <summary> Represents a designer for the wizard control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    internal class WizardDesigner : System.Windows.Forms.Design.ParentControlDesigner
    {
        #region " methods "

        /// <summary>
        /// Overrides the handling of Mouse clicks to allow back-next to work in the designer.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="msg"> [in,out] A Message value. </param>
        protected override void WndProc( ref Message msg )
        {
            // declare PInvoke constants
            const int WM_LBUTTONDOWN = 0x201;
            const int WM_LBUTTONDBLCLK = 0x203;

            // check message
            if ( msg.Msg is WM_LBUTTONDOWN or WM_LBUTTONDBLCLK )
            {
                // get the control under the mouse
                ISelectionService? ss = ( ISelectionService? ) this.GetService( typeof( ISelectionService ) );
                if ( ss is not null && ss.PrimarySelection is Wizard wizard )
                {
                    // extract the mouse position
                    int xPos = ( short ) (( uint ) msg.LParam & 0xFFFFL);
                    int yPos = ( short ) ((( uint ) msg.LParam & 0xFFFF0000L) >> 16);
                    Point mousePos = new( xPos, yPos );
                    if ( msg.HWnd == wizard._nextButton.Handle )
                    {
                        if ( wizard._nextButton.Enabled && wizard._nextButton.ClientRectangle.Contains( mousePos ) )
                        {
                            // Press the button
                            wizard.Next();
                        }
                    }
                    else if ( msg.HWnd == wizard._backButton.Handle )
                    {
                        if ( wizard._backButton.Enabled && wizard._backButton.ClientRectangle.Contains( mousePos ) )
                        {
                            // Press the button
                            wizard.Back();
                        }
                    }

                    // filter message
                    return;
                }
            }

            // forward message
            base.WndProc( ref msg );
        }

        /// <summary> Prevents the grid from being drawn on the Wizard. </summary>
        /// <value>
        /// <see langword="true" /> if a grid should be drawn on the control in the designer; otherwise,
        /// <see langword="false" />.
        /// </value>
        protected override bool DrawGrid
        {
            get => false;

            set => base.DrawGrid = value;
        }

        #endregion

    }

    #endregion
}
