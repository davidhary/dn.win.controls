#region " copyright �2005, CRISTI POTLOG - all rights reserved"
// ------------------------------------------------------------------- *
// *                            CRISTI POTLOG                             *
// *                  Copyright ©2005 - All Rights reserved               *
// *                                                                      *
// * THIS SOURCE CODE IS PROVIDED "AS IS" WITH NO WARRANTIES OF ANY KIND, *
// * EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE        *
// * WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR   *
// * PURPOSE, NONINFRINGEMENT, OR ARISING FROM A COURSE OF DEALING,       *
// * USAGE OR TRADE PRACTICE.                                             *
// *                                                                      *
// * THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.             *
// * ------------------------------------------------------------------- 
#endregion

using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace cc.isr.WinControls;

public partial class Wizard : UserControl
{
    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="Control" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
    ///                             resources; <see langword="false" /> to release only unmanaged
    ///                             resources. </param>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    protected override void Dispose(bool disposing)
    {
        try
        {
            if (disposing)
            {
                components?.Dispose();                }
        }
        finally
        {
            base.Dispose(disposing);
        }
    }

    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        _cancelButton = new Button();
        _nextButton = new Button();
        _backButton = new Button();
        _helpButton = new Button();
        SuspendLayout();
        // 
        // _cancelButton
        // 
        _cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
        _cancelButton.DialogResult = DialogResult.Cancel;
        _cancelButton.FlatStyle = FlatStyle.System;
        _cancelButton.Location = new Point(344, 224);
        _cancelButton.Name = "_CancelButton";
        _cancelButton.TabIndex = 8;
        _cancelButton.Text = "Cancel";
        _cancelButton.Click += new EventHandler( CancelButtonClick );
        // 
        // _nextButton
        // 
        _nextButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
        _nextButton.FlatStyle = FlatStyle.System;
        _nextButton.Location = new Point(260, 224);
        _nextButton.Name = "_NextButton";
        _nextButton.TabIndex = 7;
        _nextButton.Text = "&Next >";
        _nextButton.Click += new EventHandler( NextButtonClick );
        // 
        // _backButton
        // 
        _backButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
        _backButton.FlatStyle = FlatStyle.System;
        _backButton.Location = new Point(184, 224);
        _backButton.Name = "_BackButton";
        _backButton.TabIndex = 6;
        _backButton.Text = "< &Back";
        _backButton.Click += new EventHandler( BackButtonClick );
        // 
        // _helpButton
        // 
        _helpButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
        _helpButton.FlatStyle = FlatStyle.System;
        _helpButton.Location = new Point(8, 224);
        _helpButton.Name = "_HelpButton";
        _helpButton.TabIndex = 9;
        _helpButton.Text = "&Help";
        _helpButton.Visible = false;
        _helpButton.Click += new EventHandler( HelpButtonClick );
        // 
        // Wizard
        // 
        Controls.Add(_helpButton);
        Controls.Add(_cancelButton);
        Controls.Add(_nextButton);
        Controls.Add(_backButton);
        AutoScaleDimensions = new SizeF(7.0f, 17.0f);
        AutoScaleMode = AutoScaleMode.Inherit;
        Font = new Font(SystemFonts.DefaultFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        AutoSizeMode = AutoSizeMode.GrowAndShrink;
        Name = "Wizard";
        Size = new Size(428, 256);
        ResumeLayout(false);
    }

    private Button _cancelButton;
    private Button _nextButton;
    private Button _backButton;
    private Button _helpButton;
}
