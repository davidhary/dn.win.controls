using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace cc.isr.WinControls;

/// <summary> Represents a wizard page control with basic layout functionality. </summary>
/// <remarks>
/// (c) 2012 Integrated Scientific Resources, Inc., All Rights Reserved <para>
/// (c) 2005 CRISTI POTLOG - All Rights Reserved </para><para>
/// Licensed under the MIT License. </para><para>
/// David, 2012-09-19, 1.05.4645 </para><para>
/// Based on http://www.CodeProject.com/Articles/10808/Cristi-Potlog-s-Wizard-Control-for-NET
/// </para>
/// </remarks>
public partial class WizardPage : Panel
{
    #region " constants "

    /// <summary> Height of the header area. </summary>
    private const int HEADER_AREA_HEIGHT = 64;

    /// <summary> Size of the header glyph. </summary>
    private const int HEADER_GLYPH_SIZE = 48;

    /// <summary> The header text padding. </summary>
    private const int HEADER_TEXT_PADDING = 8;

    /// <summary> Width of the welcome glyph. </summary>
    private const int WELCOME_GLYPH_WIDTH = 164;

    #endregion

    #region " construction and cleanup "

    /// <summary> Creates a new instance of the <see cref="WizardPage"/> class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public WizardPage() : base()
    {
        this._description = string.Empty;
        this._title = string.Empty;
        this._wizardPageStyle = WizardPageStyle.Standard;

        // reset control style to improve rendering (reduce flicker)
        this.SetStyle( ControlStyles.AllPaintingInWmPaint, true );
        this.SetStyle( ControlStyles.DoubleBuffer, true );
        this.SetStyle( ControlStyles.ResizeRedraw, true );
        this.SetStyle( ControlStyles.UserPaint, true );
    }

    #endregion

    #region " properties "

    private WizardPageStyle _wizardPageStyle;

    /// <summary> Gets or sets the style of the wizard page. </summary>
    /// <value> The wizard page style. </value>
    [DefaultValue( WizardPageStyle.Standard )]
    [Category( "Wizard" )]
    [Description( "Gets or sets the style of the wizard page." )]
    public WizardPageStyle WizardPageStyle
    {
        get => this._wizardPageStyle;
        set
        {
            if ( this._wizardPageStyle != value )
            {
                this._wizardPageStyle = value;
                // get the parent wizard control
                if ( this.Parent is not null and Wizard wizard )
                {
                    Wizard parentWizard = wizard;
                    // check if page is selected
                    if ( ReferenceEquals( parentWizard.SelectedPage, this ) )
                    {
                        // reactivate the selected page (performs redraw too)
                        parentWizard.SelectedPage = this;
                    }
                }
                else
                {
                    // just redraw the page
                    this.Invalidate();
                }
            }
        }
    }

    private string _title;

    /// <summary> Gets or sets the title of the wizard page. </summary>
    /// <value> The title. </value>
    [DefaultValue( "" )]
    [Category( "Wizard" )]
    [Description( "Gets or sets the title of the wizard page." )]
    public string Title
    {
        get => this._title;
        set
        {
            value ??= string.Empty;

            if ( !string.Equals( this._title, value, StringComparison.Ordinal ) )
            {
                this._title = value;
                this.Invalidate();
            }
        }
    }

    private string _description;

    /// <summary> Gets or sets the description of the wizard page. </summary>
    /// <value> The description. </value>
    [DefaultValue( "" )]
    [Category( "Wizard" )]
    [Description( "Gets or sets the description of the wizard page." )]
    public string Description
    {
        get => this._description;
        set
        {
            value ??= string.Empty;

            if ( !string.Equals( this._description, value, StringComparison.Ordinal ) )
            {
                this._description = value;
                this.Invalidate();
            }
        }
    }
    #endregion

    #region " methods "

    /// <summary> Provides custom drawing to the wizard page. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs e )
    {
        // raise paint event
        base.OnPaint( e );
        if ( e is null ) return;

        // check if custom style
        if ( this._wizardPageStyle == WizardPageStyle.Custom )
        {
            // filter out
            return;
        }

        // initialize graphic resources
        Rectangle headerRect = this.ClientRectangle;
        Rectangle glyphRect = Rectangle.Empty;
        Rectangle titleRect = Rectangle.Empty;
        Rectangle descriptionRect = Rectangle.Empty;

        // determine text format
        StringFormat textFormat = StringFormat.GenericDefault;
        textFormat.LineAlignment = StringAlignment.Near;
        textFormat.Alignment = StringAlignment.Near;
        textFormat.Trimming = StringTrimming.EllipsisCharacter;
        switch ( this._wizardPageStyle )
        {
            case WizardPageStyle.Standard:
                {
                    // adjust height for header
                    headerRect.Height = HEADER_AREA_HEIGHT;
                    // draw header border
                    ControlPaint.DrawBorder3D( e.Graphics, headerRect, Border3DStyle.Etched, Border3DSide.Bottom );
                    // adjust header rect not to overwrite the border
                    headerRect.Height -= SystemInformation.Border3DSize.Height;
                    // fill header with window color
                    e.Graphics.FillRectangle( SystemBrushes.Window, headerRect );

                    // determine header image rectangle
                    int headerPadding = ( int ) Math.Floor( (HEADER_AREA_HEIGHT - HEADER_GLYPH_SIZE) / 2.0d );
                    glyphRect.Location = new Point( this.Width - HEADER_GLYPH_SIZE - headerPadding, headerPadding );
                    glyphRect.Size = new Size( HEADER_GLYPH_SIZE, HEADER_GLYPH_SIZE );

                    // determine the header content
                    Image? headerImage = null;
                    Font headerFont = this.Font;
                    Font headerTitleFont = this.Font;
                    if ( this.Parent is not null and Wizard wizard )
                    {
                        // get content from parent wizard, if exists
                        Wizard parentWizard = wizard;
                        headerImage = parentWizard.HeaderImage;
                        headerFont = parentWizard.HeaderFont;
                        headerTitleFont = parentWizard.HeaderTitleFont;
                    }

                    // check if we have an image
                    if ( headerImage is null )
                    {
                        // display a focus rect as a place holder
                        ControlPaint.DrawFocusRectangle( e.Graphics, glyphRect );
                    }
                    else
                    {
                        // draw header image
                        e.Graphics.DrawImage( headerImage, glyphRect );
                    }

                    // determine title height
                    int headerTitleHeight = ( int ) Math.Ceiling( e.Graphics.MeasureString( this._title, headerTitleFont, 0, textFormat ).Height );

                    // calculate text sizes
                    titleRect.Location = new Point( HEADER_TEXT_PADDING, HEADER_TEXT_PADDING );
                    titleRect.Size = new Size( glyphRect.Left - HEADER_TEXT_PADDING, headerTitleHeight );
                    descriptionRect.Location = titleRect.Location;
                    descriptionRect.Y += headerTitleHeight + (HEADER_TEXT_PADDING / 2);
                    descriptionRect.Size = new Size( titleRect.Width, HEADER_AREA_HEIGHT - descriptionRect.Y );

                    // draw title text (single line, truncated with ellipsis)
                    e.Graphics.DrawString( this._title, headerTitleFont, SystemBrushes.WindowText, titleRect, textFormat );
                    // draw description text (multiple lines if needed)
                    e.Graphics.DrawString( this._description, headerFont, SystemBrushes.WindowText, descriptionRect, textFormat );
                    break;
                }

            case WizardPageStyle.Welcome:
            case WizardPageStyle.Finish:
                {
                    // fill whole page with window color
                    e.Graphics.FillRectangle( SystemBrushes.Window, headerRect );

                    // determine welcome image rectangle
                    glyphRect.Location = Point.Empty;
                    glyphRect.Size = new Size( WELCOME_GLYPH_WIDTH, this.Height );

                    // determine the icon that should appear on the welcome page
                    Image? welcomeImage = null;
                    Font welcomeFont = this.Font;
                    Font welcomeTitleFont = this.Font;
                    if ( this.Parent is not null and Wizard wizard )
                    {
                        // get content from parent wizard, if exists
                        Wizard parentWizard = wizard;
                        welcomeImage = parentWizard.WelcomeImage;
                        welcomeFont = parentWizard.WelcomeFont;
                        welcomeTitleFont = parentWizard.WelcomeTitleFont;
                    }

                    // check if we have an image
                    if ( welcomeImage is null )
                    {
                        // display a focus rect as a place holder
                        ControlPaint.DrawFocusRectangle( e.Graphics, glyphRect );
                    }
                    else
                    {
                        // draw welcome page image
                        e.Graphics.DrawImage( welcomeImage, glyphRect );
                    }

                    // calculate text sizes
                    titleRect.Location = new Point( WELCOME_GLYPH_WIDTH + HEADER_TEXT_PADDING, HEADER_TEXT_PADDING );
                    titleRect.Width = this.Width - titleRect.Left - HEADER_TEXT_PADDING;
                    // determine title height
                    int welcomeTitleHeight = ( int ) Math.Ceiling( e.Graphics.MeasureString( this._title, welcomeTitleFont, titleRect.Width, textFormat ).Height );
                    descriptionRect.Location = titleRect.Location;
                    descriptionRect.Y += welcomeTitleHeight + HEADER_TEXT_PADDING;
                    descriptionRect.Size = new Size( this.Width - descriptionRect.Left - HEADER_TEXT_PADDING, this.Height - descriptionRect.Y );

                    // draw title text (multiple lines if needed)
                    e.Graphics.DrawString( this._title, welcomeTitleFont, SystemBrushes.WindowText, titleRect, textFormat );
                    // draw description text (multiple lines if needed)
                    e.Graphics.DrawString( this._description, welcomeFont, SystemBrushes.WindowText, descriptionRect, textFormat );
                    break;
                }

            case WizardPageStyle.Custom:
                break;
            default:
                break;
        }
    }
    #endregion

    #region " inner classes "

    /// <summary>
    /// This is a designer for the Banner. This designer locks the control vertical sizing.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    internal class WizardPageDesigner : ParentControlDesigner
    {
        /// <summary>
        /// Gets the selection rules that indicate the movement capabilities of a component.
        /// </summary>
        /// <value>
        /// A bitwise combination of <see cref="System.Windows.Forms.Design.SelectionRules" /> values.
        /// </value>
        public override SelectionRules SelectionRules =>
                // lock the control
                SelectionRules.Visible | SelectionRules.Locked;
    }

    #endregion
}
