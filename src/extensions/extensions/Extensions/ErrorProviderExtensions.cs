using System.Windows.Forms;

namespace cc.isr.WinControls.ErrorProviderExtensions;

/// <summary> Includes extensions for <see cref="ErrorProvider">Error Provider</see>. </summary>
/// <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-05-26, 1.0.5624 </para></remarks>
public static class ErrorProviderExtensionMethods
{
    #region " clear "

    /// <summary> Clears this object to its blank/initial state. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="provider"> The provider. </param>
    /// <param name="sender">   The event sender. </param>
    public static void Clear( this ErrorProvider provider, object sender )
    {
        if ( sender is Control control )
        {
            provider?.Clear( control );
        }
        else
        {
            if ( sender is ToolStripItem toolStripItem )
            {
                provider?.Clear( toolStripItem );
            }
        }
    }

    /// <summary> Clears this object to its blank/initial state. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="provider"> The provider. </param>
    /// <param name="sender">   The event sender. </param>
    public static void Clear( this ErrorProvider provider, Control sender )
    {
        if ( sender is not null && sender.Container is not null )
        {
            provider?.SetError( sender, "" );
            if ( sender.Container is ToolStripItem or ToolStripMenuItem )
            {
                provider?.Clear( sender.Container );
            }
            else
            {
                provider?.SetError( sender, "" );
            }
        }
    }

    /// <summary> Clears this object to its blank/initial state. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="provider"> The provider. </param>
    /// <param name="sender">   The event sender. </param>
    public static void Clear( this ErrorProvider provider, ToolStripItem sender )
    {
        if ( sender is not null && sender.Owner is not null )
        {
            provider?.SetError( sender.Owner, "" );
        }
    }

    #endregion

    #region " annunciate - object "

    /// <summary> Annunciates error. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="provider"> The provider. </param>
    /// <param name="sender">   The event sender. </param>
    /// <param name="details">  The details. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string Annunciate( this ErrorProvider provider, object sender, string details )
    {
        if ( sender is Control control )
        {
            _ = (provider?.Annunciate( control, details ));
        }
        else
        {
            if ( sender is ToolStripMenuItem toolStripMenuItem )
            {
                _ = (provider?.Annunciate( toolStripMenuItem, details ));
            }
            else
            {
                if ( sender is ToolStripItem toolStripItem )
                {
                    _ = (provider?.Annunciate( toolStripItem, details ));
                }
            }
        }

        return details;
    }

    /// <summary> Annunciates error. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="provider"> The provider. </param>
    /// <param name="sender">   The event sender. </param>
    /// <param name="format">   Describes the format to use. </param>
    /// <param name="args">     A variable-length parameters list containing arguments. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string? Annunciate( this ErrorProvider provider, object sender, string format, params object[] args )
    {
        return provider?.Annunciate( sender, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    #endregion

    #region " annunciate - control "

    /// <summary> Annunciates error. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="provider"> The provider. </param>
    /// <param name="sender">   The event sender. </param>
    /// <param name="details">  The details. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string Annunciate( this ErrorProvider provider, Control sender, string details )
    {
        if ( provider is not null && sender is not null )
        {
            if ( sender.Container is ToolStripItem or ToolStripMenuItem )
            {
                _ = provider.Annunciate( sender.Container, details );
            }
            else
            {
                provider.SetError( sender, details );
            }
        }

        return details;
    }

    /// <summary> Annunciates error. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="provider"> The provider. </param>
    /// <param name="sender">   The event sender. </param>
    /// <param name="format">   Describes the format to use. </param>
    /// <param name="args">     A variable-length parameters list containing arguments. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string? Annunciate( this ErrorProvider provider, Control sender, string format, params object[] args )
    {
        return provider?.Annunciate( sender, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    #endregion

    #region " annunciate -- tool strip "

    /// <summary> Annunciates error. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="provider"> The provider. </param>
    /// <param name="sender">   The sender. </param>
    /// <param name="details">  The details. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string Annunciate( this ErrorProvider provider, ToolStripItem sender, string details )
    {
        if ( provider is not null && sender is not null && sender.Owner is not null )
        {
            provider.SetIconAlignment( sender.Owner, ErrorIconAlignment.BottomLeft );
            provider.SetIconPadding( sender.Owner, -sender.Bounds.X );
            provider.SetError( sender.Owner, details );
        }

        return details;
    }

    /// <summary> Annunciates error. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="provider"> The provider. </param>
    /// <param name="sender">   The sender. </param>
    /// <param name="format">   Describes the format to use. </param>
    /// <param name="args">     A variable-length parameters list containing arguments. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string? Annunciate( this ErrorProvider provider, ToolStripItem sender, string format, params object[] args )
    {
        return provider?.Annunciate( sender, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    #endregion
}
