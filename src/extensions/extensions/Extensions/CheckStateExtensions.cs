using System.Windows.Forms;

namespace cc.isr.WinControls.CheckStateExtensions;

/// <summary> Extensions methods for <see cref="CheckState"/>. </summary>
/// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para> </remarks>
public static class CheckStateExtensionMethods
{
    #region " check state "

    /// <summary> Converts a value to a check state. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> Value as a CheckState. </returns>
    public static CheckState ToCheckState( this bool? value )
    {
        return value.HasValue ? value.Value.ToCheckState() : CheckState.Indeterminate;
    }

    /// <summary>   Converts a value to a check state. </summary>
    /// <remarks>   2024-08-02. </remarks>
    /// <param name="value">    The value. </param>
    /// <returns>   Value as a CheckState. </returns>
    public static CheckState ToCheckState( this bool value )
    {
        return value ? CheckState.Checked : CheckState.Unchecked;
    }

    /// <summary> Converts a check state to nullable Boolean. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> A Boolean? </returns>
    public static bool? FromCheckState( this CheckState value )
    {
        return value == CheckState.Indeterminate ? new bool?() : value == CheckState.Checked;
    }

    /// <summary> Converts this object to a check state caption. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="value">              The value. </param>
    /// <param name="checkedValue">       The checked value. </param>
    /// <param name="uncheckedValue">     The unchecked value. </param>
    /// <param name="indeterminateValue"> The indeterminate value. </param>
    /// <returns> The given data converted to a <see cref="string" />. </returns>
    public static string ToCheckStateCaption( this CheckState value, string checkedValue, string uncheckedValue, string indeterminateValue )
    {
        return value == CheckState.Checked ? checkedValue : value == CheckState.Unchecked ? uncheckedValue : indeterminateValue;
    }

    #endregion
}
