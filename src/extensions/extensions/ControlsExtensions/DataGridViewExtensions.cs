using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace cc.isr.WinControls.DataGridViewExtensions;

/// <summary> Includes extensions for <see cref="DataGridView">Data Grid View</see>. </summary>
/// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2010-11-19, 1.2.3975 </para></remarks>
public static class DataGridViewExtensionMethods
{
    #region " display "

    /// <summary> Set grid rendering as double buffered to reduce flickering. </summary>
    /// <remarks>
    /// From http://www.CodeProject.com/Tips/390496/Reducing-flicker-blinking-in- DataGridView.
    /// </remarks>
    /// <param name="grid">    The grid. </param>
    /// <param name="enabled"> Enabled if set to <c>true</c>. </param>
    public static void DoubleBuffered( this DataGridView grid, bool enabled )
    {
        if ( grid is not null )
        {
            Type dgvType = grid.GetType();

            System.Reflection.PropertyInfo? pi = dgvType.GetProperty( "DoubleBuffered", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic );
            pi?.SetValue( grid, enabled, null );
        }
    }

    /// <summary> Hides all columns. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="grid"> The <see cref="DataGridView">grid</see> </param>
    public static void HideColumns( this DataGridView grid )
    {
        if ( grid is not null )
        {
            foreach ( DataGridViewColumn column in grid.Columns )
            {
                column.Visible = false;
            }
        }
    }

    /// <summary>
    /// Hides the columns by the <paramref name="columnNames">column names</paramref>.
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="grid">        The grid. </param>
    /// <param name="columnNames"> The column names. </param>
    public static void HideColumns( this DataGridView grid, string[] columnNames )
    {
        if ( grid is not null && columnNames is not null )
        {
            foreach ( DataGridViewColumn column in grid.Columns )
            {
                if ( !columnNames.Contains( column.Name ) )
                {
                    column.Visible = false;
                }
            }
        }
    }

    /// <summary> Displays a row numbers described by grid. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid"> The grid. </param>
    public static void DisplayRowNumbers( this DataGridView grid )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        foreach ( DataGridViewRow row in grid.Rows )
        {
            row.HeaderCell.Value = (row.Index + 1).ToString( System.Globalization.CultureInfo.CurrentCulture );
        }
    }

    /// <summary> Visible column count. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="grid"> The grid. </param>
    /// <returns> The number of visible columns. </returns>
    public static int VisibleColumnCount( this DataGridView grid )
    {
        int count = 0;
        if ( grid is not null )
        {
            foreach ( DataGridViewColumn column in grid.Columns )
            {
                if ( column.Visible )
                {
                    count += 1;
                }
            }
        }

        return count;
    }

    /// <summary> Scrolls while keeping the displayed section centered. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid"> The <see cref="DataGridView">grid</see> </param>
    public static void ScrollCenterRow( this DataGridView grid )
    {
#if NET5_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        int halfWay = grid.DisplayedRowCount( false ) / 2;
        if ( grid.SelectedRows.Count == 0 )
        {
            if ( grid.CurrentRow is null )
                throw new InvalidOperationException( $"Unable to select the grid {nameof( grid.CurrentRow )}." );
            else
                grid.CurrentRow.Selected = true;
        }

        if ( grid.FirstDisplayedScrollingRowIndex + halfWay > grid.SelectedRows[0].Index
            || grid.FirstDisplayedScrollingRowIndex + grid.DisplayedRowCount( false ) - halfWay <= grid.SelectedRows[0].Index )
        {
            int targetRow = grid.SelectedRows[0].Index;
            targetRow = Math.Max( targetRow - halfWay, 0 );
            grid.FirstDisplayedScrollingRowIndex = targetRow;
            grid.PerformLayout();
        }
    }

    #endregion

    #region " column header management "

    /// <summary>
    /// Splits the string to words by adding spaces between lower and upper case characters.
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="value"> The <c>String</c> value to split. </param>
    /// <returns> A <c>String</c> of words separated by spaces. </returns>
    private static string SplitWords( string value )
    {
        if ( string.IsNullOrWhiteSpace( value ) )
        {
            return string.Empty;
        }
        else
        {
            bool isSpace = false;
            bool isLowerCase = false;
            System.Text.StringBuilder newValue = new();
            foreach ( char c in value )
            {
                if ( !isSpace && isLowerCase && char.IsUpper( c ) )
                {
                    _ = newValue.Append( ' ' );
                }

                isSpace = c.Equals( ' ' );
                isLowerCase = !char.IsUpper( c );
                _ = newValue.Append( c );
            }

            return newValue.ToString();
        }
    }

    /// <summary>
    /// Replaces the column header text with one that has spaces between lower and upper case
    /// characters.
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="column"> The column. </param>
    public static void ParseHeaderText( this DataGridViewColumn column )
    {
        if ( column is not null && !string.IsNullOrWhiteSpace( column.HeaderText ) )
        {
            column.HeaderText = SplitWords( column.HeaderText );
        }
    }

    /// <summary>
    /// Replaces the column headers text with one that has spaces between lower and upper case
    /// characters.
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="grid"> The <see cref="DataGridView">grid</see> </param>
    public static void ParseHeaderText( this DataGridView grid )
    {
        if ( grid is not null )
        {
            foreach ( DataGridViewColumn column in grid.Columns )
            {
                column.ParseHeaderText();
            }
        }
    }

    /// <summary>
    /// Replaces the column headers text with one that has spaces between lower and upper case
    /// characters. Operates only on visible headers.
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="grid"> The <see cref="DataGridView">grid</see> </param>
    public static void ParseVisibleHeaderText( this DataGridView grid )
    {
        if ( grid is not null )
        {
            foreach ( DataGridViewColumn column in grid.Columns )
            {
                if ( column.Visible )
                {
                    column.ParseHeaderText();
                }
            }
        }
    }

    #endregion

    #region " column management: duplicate, replace, move, "

    /// <summary>
    /// Replaces an existing column with a combo box column and returns reference to this column.
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid">       The grid. </param>
    /// <param name="columnName"> Name of the column. </param>
    /// <returns> Replaced column. </returns>
    public static DataGridViewComboBoxColumn ReplaceWithComboColumn( this DataGridView grid, string columnName )
    {
#if NET5_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        if ( string.IsNullOrWhiteSpace( columnName ) )
            throw new ArgumentNullException( nameof( columnName ) );

        DataGridViewColumn? column = grid.Columns[columnName] ?? throw new ArgumentException( $"'{columnName}' not found" );
        DataGridViewComboBoxColumn gridColumn = new();
        try
        {
            gridColumn.Name = column.Name;
            gridColumn.DataPropertyName = column.DataPropertyName;
            gridColumn.HeaderText = column.HeaderText;
            // gridColumn.IsDataBound = column.IsDataBound
            grid.Columns.Remove( column );
            // this does not work right. Me._dataGridView.Columns.Insert(fieldColumn.Ordinal, gridColumn)
            grid.Columns.Insert( column.Index, gridColumn );
            return gridColumn;
        }
        catch
        {
            gridColumn?.Dispose();
            throw;
        }
    }

    /// <summary> Duplicates an existing column returns reference to this column. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid">       The grid. </param>
    /// <param name="columnName"> Name of the column. </param>
    /// <param name="newName">    Name of the new. </param>
    /// <returns> Replaced column. </returns>
    public static DataGridViewColumn DuplicateColumn( this DataGridView grid, string columnName, string newName )
    {
#if NET5_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        if ( string.IsNullOrWhiteSpace( columnName ) )
            throw new ArgumentNullException( nameof( columnName ) );

        DataGridViewColumn? column = grid.Columns[columnName] ?? throw new ArgumentException( $"'{columnName}' not found" );
        DataGridViewColumn gridColumn = new();
        try
        {
            gridColumn.Name = newName;
            gridColumn.DataPropertyName = column.DataPropertyName;
            gridColumn.HeaderText = column.HeaderText;
            grid.Columns.Insert( column.Index, gridColumn );
            return gridColumn;
        }
        catch
        {
            gridColumn?.Dispose();
            throw;
        }
    }

    /// <summary> Duplicate combo column. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid">       The grid. </param>
    /// <param name="columnName"> Name of the column. </param>
    /// <param name="newName">    Name of the new. </param>
    /// <returns> A  DataGridViewCheckBoxColumn. </returns>
    public static DataGridViewComboBoxColumn DuplicateComboColumn( this DataGridView grid, string columnName, string newName )
    {
#if NET5_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        if ( string.IsNullOrWhiteSpace( columnName ) )
            throw new ArgumentNullException( nameof( columnName ) );

        DataGridViewColumn? column = grid.Columns[columnName] ?? throw new ArgumentException( $"'{columnName}' not found" );
        DataGridViewComboBoxColumn gridColumn = new();
        try
        {
            gridColumn.Name = newName;
            gridColumn.DataPropertyName = column.DataPropertyName;
            gridColumn.HeaderText = column.HeaderText;
            grid.Columns.Insert( column.Index, gridColumn );
            return gridColumn;
        }
        catch
        {
            gridColumn?.Dispose();
            throw;
        }
    }

    /// <summary>
    /// Replaces an existing column with a check box column and returns reference to this column.
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid">       The grid. </param>
    /// <param name="columnName"> Name of the column. </param>
    /// <returns> Replaced column. </returns>
    public static DataGridViewCheckBoxColumn ReplaceWithCheckColumn( this DataGridView grid, string columnName )
    {
#if NET5_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        if ( string.IsNullOrWhiteSpace( columnName ) )
            throw new ArgumentNullException( nameof( columnName ) );

        DataGridViewColumn? column = grid.Columns[columnName] ?? throw new ArgumentException( $"'{columnName}' not found" );
        DataGridViewCheckBoxColumn gridColumn = new();
        try
        {
            gridColumn.Name = column.Name;
            gridColumn.DataPropertyName = column.DataPropertyName;
            gridColumn.HeaderText = column.HeaderText;
            gridColumn.ThreeState = true;
            grid.Columns.Remove( column );
            grid.Columns.Insert( column.Index, gridColumn );
            return gridColumn;
        }
        catch
        {
            gridColumn?.Dispose();
            throw;
        }
    }

    /// <summary>
    /// Replaces an existing column with a text box column and returns reference to this column.
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid">       The grid. </param>
    /// <param name="columnName"> Name of the column. </param>
    /// <returns> Replaced column. </returns>
    public static DataGridViewTextBoxColumn ReplaceWithTextColumn( this DataGridView grid, string columnName )
    {
#if NET5_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        if ( string.IsNullOrWhiteSpace( columnName ) )
            throw new ArgumentNullException( nameof( columnName ) );

        DataGridViewColumn? column = grid.Columns[columnName] ?? throw new ArgumentException( $"'{columnName}' not found" );
        DataGridViewTextBoxColumn gridColumn = new();
        try
        {
            gridColumn.Name = column.Name;
            gridColumn.DataPropertyName = column.DataPropertyName;
            gridColumn.HeaderText = column.HeaderText;
            // gridColumn.IsDataBound = column.IsDataBound
            grid.Columns.Remove( column );
            // this does not work right. Me._dataGridView.Columns.Insert(fieldColumn.Ordinal, gridColumn)
            grid.Columns.Insert( column.Index, gridColumn );
            return gridColumn;
        }
        catch
        {
            gridColumn?.Dispose();
            throw;
        }
    }

    /// <summary>
    /// Replaces an existing column with a text box column and returns reference to this column.
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid">       The grid. </param>
    /// <param name="columnName"> Name of the column. </param>
    /// <param name="newName">    Name of the new. </param>
    /// <returns> Replaced column. </returns>
    public static DataGridViewTextBoxColumn DuplicateTextColumn( this DataGridView grid, string columnName, string newName )
    {
#if NET5_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        if ( string.IsNullOrWhiteSpace( columnName ) )
            throw new ArgumentNullException( nameof( columnName ) );

        DataGridViewColumn? column = grid.Columns[columnName] ?? throw new ArgumentException( $"'{columnName}' not found" );
        DataGridViewTextBoxColumn gridColumn = new();
        try
        {
            gridColumn.Name = newName;
            gridColumn.DataPropertyName = column.DataPropertyName;
            gridColumn.HeaderText = column.HeaderText;
            grid.Columns.Insert( column.Index, gridColumn );
            return gridColumn;
        }
        catch
        {
            gridColumn?.Dispose();
            throw;
        }
    }

    /// <summary> Moves an existing column to the specified location. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid">       The grid. </param>
    /// <param name="columnName"> Name of the column. </param>
    /// <param name="location">   The location. </param>
    /// <returns> The moved column. </returns>
    public static DataGridViewColumn MoveTo( this DataGridView grid, string columnName, int location )
    {
#if NET5_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        if ( string.IsNullOrWhiteSpace( columnName ) )
            throw new ArgumentNullException( nameof( columnName ) );

        DataGridViewColumn? column = grid.Columns[columnName] ?? throw new ArgumentException( $"'{columnName}' not found" );
        grid.Columns.Remove( column );
        grid.Columns.Insert( location, column );
        return column;
    }

    #endregion

    #region " row management: find, select "

    /// <summary> Returns the row selected based on the column value. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid">        The grid. </param>
    /// <param name="columnName">  Name of the column. </param>
    /// <param name="columnValue"> The column value. </param>
    /// <returns> The found row. </returns>
    public static DataGridViewRow? FindRow<T>( this DataGridView grid, string columnName, T? columnValue )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
        ArgumentNullException.ThrowIfNull( columnValue, nameof( columnValue ) );
#else
#if NET5_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        if ( columnValue is null ) throw new ArgumentNullException( nameof( columnValue ) );
#endif

        if ( string.IsNullOrWhiteSpace( columnName ) ) throw new ArgumentNullException( nameof( columnName ) );


        DataGridViewRow? foundRow = null;
        foreach ( DataGridViewRow r in grid.Rows )
        {
            if ( columnValue.Equals( r.Cells[columnName].Value ) )
            {
                foundRow = r;
                break;
            }
        }

        return foundRow;
    }

    /// <summary> Selects the row. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid">        The grid. </param>
    /// <param name="columnName">  Name of the column. </param>
    /// <param name="columnValue"> The column value. </param>
    /// <returns> <c>true</c> if the row was selected. </returns>
    public static bool SelectRow<T>( this DataGridView grid, string columnName, T? columnValue )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
        ArgumentNullException.ThrowIfNull( columnValue, nameof( columnValue ) );
#else
#if NET5_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        if ( columnValue is null ) throw new ArgumentNullException( nameof( columnValue ) );
#endif

        if ( string.IsNullOrWhiteSpace( columnName ) ) throw new ArgumentNullException( nameof( columnName ) );

        DataGridViewRow? foundRow = grid.FindRow( columnName, columnValue );
        if ( foundRow is null )
            return false;
        else
        {
            foundRow.Selected = true;
            return true;
        }
    }

    /// <summary>
    /// Silently selects a grid row. The control is disabled when set so that the handling of the
    /// changed event can be skipped.
    /// </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid">        The grid. </param>
    /// <param name="columnName">  Name of the column. </param>
    /// <param name="columnValue"> The column value. </param>
    /// <returns> <c>true</c> if the row was selected. </returns>
    public static bool SilentSelectRow<T>( this DataGridView grid, string columnName, T? columnValue )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
        ArgumentNullException.ThrowIfNull( columnValue, nameof( columnValue ) );
#else
#if NET5_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        if ( columnValue is null ) throw new ArgumentNullException( nameof( columnValue ) );
#endif
        if ( string.IsNullOrWhiteSpace( columnName ) ) throw new ArgumentNullException( nameof( columnName ) );

        bool enabled = grid.Enabled;
        try
        {
            grid.Enabled = false;
            return grid.SelectRow( columnName, columnValue );
        }
        catch
        {
            throw;
        }
        finally
        {
            grid.Enabled = enabled;
        }
    }

    #endregion

    #region " store "

    /// <summary> Stores the grid to file. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="grid">     The grid. </param>
    /// <param name="fileName"> Filename of the file. </param>
    /// <returns> An Integer. </returns>
    public static int Store( this DataGridView grid, string fileName )
    {
        if ( grid is null )
        {
            return 0;
        }

        string[] headers = (from header in grid.Columns.Cast<DataGridViewColumn>()
                            select header.HeaderText).ToArray();
        System.Collections.Generic.IEnumerable<string[]> rows = from row in grid.Rows.Cast<DataGridViewRow>()
                                                                where !row.IsNewRow
                                                                select Array.ConvertAll( row.Cells.Cast<DataGridViewCell>().ToArray(), c => c.Value is not null ? c.Value.ToString() : "" );
        using ( System.IO.StreamWriter sw = new( fileName ) )
        {
            sw.WriteLine( string.Join( ",", headers ) );
            foreach ( string[] r in rows )
            {
                sw.WriteLine( string.Join( ",", r ) );
            }
        }

        return rows.Count();
    }

    /// <summary> Stores the grid to file. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="grid">           The grid. </param>
    /// <param name="fileName">       Filename of the file. </param>
    /// <param name="newLineReplace"> The new line replace string. </param>
    /// <returns> An Integer. </returns>
    public static int Store( this DataGridView grid, string fileName, string newLineReplace )
    {
        if ( grid is null )
        {
            return 0;
        }

        string[] headers = (from header in grid.Columns.Cast<DataGridViewColumn>()
                            select header.HeaderText).ToArray();
        System.Collections.Generic.IEnumerable<string[]> rows = from row in grid.Rows.Cast<DataGridViewRow>()
                                                                where !row.IsNewRow
                                                                select Array.ConvertAll( row.Cells.Cast<DataGridViewCell>().ToArray(), c => c.Value is not null ? c.Value.ToString() : "" );
        using ( System.IO.StreamWriter sw = new( fileName ) )
        {
            string s = string.Join( ",", headers );
            if ( !string.IsNullOrWhiteSpace( s ) )
            {
                sw.WriteLine( s.Replace( Environment.NewLine, newLineReplace ) );
            }

            foreach ( string[] r in rows )
            {
                s = string.Join( ",", r );
                if ( !string.IsNullOrWhiteSpace( s ) )
                {
                    sw.WriteLine( s.Replace( Environment.NewLine, newLineReplace ) );
                }
            }
        }

        return rows.Count();
    }

    #endregion

    #region " copy "

    /// <summary> Copies to clipboard described by grid. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="grid"> The <see cref=" DataGridView">grid</see> </param>
    public static void CopyToClipboard( this DataGridView grid )
    {
        if ( grid is not null && (grid.SelectedRows.Count > 0 || grid.SelectedColumns.Count > 0 || grid.SelectedCells.Count > 0) )
        {
            grid.SuspendLayout();
            // grid.RowHeadersVisible = False
            DataObject? dataObject = grid.GetClipboardContent();
            if ( dataObject is not null ) Clipboard.SetDataObject( dataObject );
            // grid.ClearSelection()
            // grid.RowHeadersVisible = True
            grid.ResumeLayout();
        }
    }

    #endregion

    #region " sort "

    /// <summary>
    /// Sort on a number of columns when handling the on column header mouse click.
    /// </summary>
    /// <remarks> https://www.codeproject.com/Tips/1233057/Datagridview-with-Multisort. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid">       The <see cref="DataGridView">grid</see> </param>
    /// <param name="dataSource"> The data source. </param>
    /// <param name="e">          Data grid view cell mouse event information. </param>
    public static void MultipleSort( this DataGridView grid, BindingSource dataSource, DataGridViewCellMouseEventArgs e )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
        ArgumentNullException.ThrowIfNull( dataSource, nameof( dataSource ) );
        ArgumentNullException.ThrowIfNull( e, nameof( e ) );
#else
#if NET5_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        if ( dataSource is null ) throw new ArgumentNullException( nameof( dataSource ) );

        if ( e is null ) throw new ArgumentNullException( nameof( e ) );
#endif

        if ( dataSource.Sort is null ) throw new InvalidOperationException( $"{nameof( dataSource.Sort )} is null." );

        if ( Control.ModifierKeys == Keys.Control )
        {
            if ( dataSource.Sort.Contains( grid.Columns[e.ColumnIndex].Name ) )
            {
                dataSource.Sort = dataSource.Sort.Contains( grid.Columns[e.ColumnIndex].Name + " ASC" ) ? dataSource.Sort.Replace( grid.Columns[e.ColumnIndex].Name + " ASC", grid.Columns[e.ColumnIndex].Name + " DESC" ) : dataSource.Sort.Replace( grid.Columns[e.ColumnIndex].Name + " DESC", grid.Columns[e.ColumnIndex].Name + " ASC" );
            }
            else
            {
                dataSource.Sort += ", " + grid.Columns[e.ColumnIndex].Name + " ASC";
            }

            string[] allSort = dataSource.Sort.Split( ", ".ToCharArray() );
            foreach ( string s in allSort )
            {
                string[] iSortOrder = s.Split( ' ' );
                string cName = s.Replace( " " + iSortOrder[^1], "" );
                DataGridViewColumn? column = grid.Columns[cName];
                if ( column is not null )
                {
                    switch ( iSortOrder[^1] ?? "" )
                    {
                        case "ASC":
                            {
                                column.HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                                break;
                            }

                        case "DESC":
                            {
                                column.HeaderCell.SortGlyphDirection = SortOrder.Descending;
                                break;
                            }

                        default:
                            break;
                    }
                }
            }
        }
        else
        {
            dataSource.Sort = dataSource.Sort.Contains( grid.Columns[e.ColumnIndex].Name + " DESC" ) ? grid.Columns[e.ColumnIndex].Name + " ASC" : grid.Columns[e.ColumnIndex].Name + " DESC";
        }
    }

    #endregion

    #region " print "

    /// <summary> Prints grid. </summary>
    /// <remarks>
    /// From https://StackOverflow.com/questions/24654790/how-to-print-datagridview-has-a- table-in-
    /// vb.
    /// </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="grid">         The <see cref="DataGridView">grid</see> </param>
    /// <param name="e">            Print page event information. </param>
    /// <param name="rowIndex">     [in,out] Zero-based index of the row. </param>
    /// <param name="isNewPage">    [in,out] The is new page. </param>
    /// <param name="bottomMargin"> [in,out] The bottom margin. </param>
    public static void Print( this DataGridView grid, System.Drawing.Printing.PrintPageEventArgs e, ref int rowIndex, ref bool isNewPage, ref float bottomMargin )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
        ArgumentNullException.ThrowIfNull( e, nameof( e ) );
#else
#if NET5_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( grid, nameof( grid ) );
#else
        if ( grid is null ) throw new ArgumentNullException( nameof( grid ) );
#endif

        if ( e is null ) throw new ArgumentNullException( nameof( e ) );
#endif

        using StringFormat fmt = new( StringFormatFlags.LineLimit )
        {
            LineAlignment = StringAlignment.Center,
            Trimming = StringTrimming.EllipsisCharacter
        };
        float y = e.MarginBounds.Top;
        if ( bottomMargin > 0f )
        {
            y = bottomMargin;
        }

        // print column headers
        if ( isNewPage && rowIndex == 0 )
        {
            isNewPage = false;
            float h = 0f;
            float x = e.MarginBounds.Left;
            foreach ( DataGridViewColumn column in grid.Columns )
            {
                RectangleF rc = new( x, y, column.HeaderCell.Size.Width, column.HeaderCell.Size.Height );
                h = Math.Max( h, rc.Height );
                if ( y + h > e.MarginBounds.Bottom )
                {
                    e.HasMorePages = true;
                    break;
                }

                e.Graphics?.DrawRectangle( Pens.Black, rc.Left, rc.Top, rc.Width, rc.Height );
                e.Graphics?.DrawString( column.HeaderText, grid.Font, Brushes.Black, rc, fmt );
                x += rc.Width;
            }

            y += h;
            bottomMargin = y;
        }

        // print the row cells
        while ( rowIndex < grid.RowCount && !e.HasMorePages )
        {
            DataGridViewRow row = grid.Rows[rowIndex];
            float x = e.MarginBounds.Left;
            float h = 0f;
            foreach ( DataGridViewCell cell in row.Cells )
            {
                RectangleF rc = new( x, y, cell.Size.Width, cell.Size.Height );
                h = Math.Max( h, rc.Height );
                if ( y + h > e.MarginBounds.Bottom )
                {
                    e.HasMorePages = true;
                    break;
                }

                e.Graphics?.DrawRectangle( Pens.Black, rc.Left, rc.Top, rc.Width, rc.Height );
                if ( cell is not null )
                {
                    DataGridViewRow? cellRow = grid.Rows[cell.RowIndex];
                    if ( cellRow is not null && cellRow.Cells is not null )
                    {
                        DataGridViewCell? rowCell = cellRow.Cells[cell.ColumnIndex];
                        if ( rowCell is not null && rowCell.FormattedValue is not null )
                        {
                            e.Graphics?.DrawString( rowCell.FormattedValue.ToString(), grid.Font, Brushes.Black, rc, fmt );
                        }
                    }
                }
                x += rc.Width;
            }

            if ( !e.HasMorePages )
            {
                rowIndex += 1;
                y += h;
                bottomMargin = y;
            }
        }

        if ( e.HasMorePages )
        {
            isNewPage = true;
        }
    }

    /// <summary> Prints grid. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="grid">                The <see cref="DataGridView">grid</see> </param>
    /// <param name="selectedCellIndexes"> The selected cell indexes. </param>
    /// <param name="e">                   Print page event information. </param>
    /// <param name="rowIndex">            [in,out] Zero-based index of the row. </param>
    /// <param name="isNewPage">           [in,out] The is new page. </param>
    public static void Print( this DataGridView grid, int[] selectedCellIndexes, System.Drawing.Printing.PrintPageEventArgs e, ref int rowIndex, ref bool isNewPage )
    {
        if ( grid is null || e is null || selectedCellIndexes is null ) return;

        using ( StringFormat fmt = new( StringFormatFlags.LineLimit ) )
        {
            fmt.LineAlignment = StringAlignment.Center;
            fmt.Trimming = StringTrimming.EllipsisCharacter;
            float y = e.MarginBounds.Top;
            while ( rowIndex < grid.RowCount )
            {
                float h = 0f;
                DataGridViewRow row = grid.Rows[rowIndex];
                if ( row is not null )
                {
                    float x = e.MarginBounds.Left;
                    foreach ( int cell in selectedCellIndexes )
                    {
                        RectangleF rc = new( x, y, row.Cells[cell].Size.Width, row.Cells[cell].Size.Height );
                        e.Graphics?.DrawRectangle( Pens.Black, rc.Left, rc.Top, rc.Width, rc.Height );
                        if ( isNewPage )
                        {
                            e.Graphics?.DrawString( grid.Columns[cell].HeaderText, grid.Font, Brushes.Black, rc, fmt );
                        }
                        else
                        {
                            DataGridViewCell? rowCell = row.Cells[cell];
                            if ( rowCell is not null )
                            {
                                DataGridViewRow? cellRow = grid.Rows[rowCell.RowIndex];

                                if ( cellRow is not null && cellRow.Cells is not null )
                                {
                                    DataGridViewCell? rowCell1 = cellRow.Cells[cell];
                                    if ( rowCell1 is not null && rowCell1.FormattedValue is not null )
                                    {
                                        e.Graphics?.DrawString( rowCell1.FormattedValue.ToString(), grid.Font, Brushes.Black, rc, fmt );
                                    }
                                }

                            }
                            // e.Graphics?.DrawString( grid.Rows[row.Cells[cell].RowIndex].Cells[cell].FormattedValue.ToString(), grid.Font, Brushes.Black, rc, fmt );
                        }

                        x += rc.Width;
                        h = Math.Max( h, rc.Height );
                    }
                }

                if ( !isNewPage )
                {
                    rowIndex += 1;
                }

                isNewPage = false;
                y += h;
                if ( y + h > e.MarginBounds.Bottom )
                {
                    e.HasMorePages = true;
                    rowIndex -= 1;
                    isNewPage = true;
                    return;
                }
            }
        }

        rowIndex = 0;
    }

    #endregion
}
