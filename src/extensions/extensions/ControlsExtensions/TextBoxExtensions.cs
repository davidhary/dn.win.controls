using System;
using System.Windows.Forms;

namespace cc.isr.WinControls.TextBoxExtensions;

/// <summary> Includes extensions for <see cref="TextBox">Text Box</see>. </summary>
/// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2010-11-19, 1.2.3975 </para></remarks>
public static class TextBoxExtensionMethods
{
    #region " append and prepend "

    /// <summary>
    /// Appends <paramref name="value">text</paramref> to the
    /// <see cref="TextBoxBase">control</see>.
    /// This setter is thread safe.
    /// </summary>
    /// <remarks> The value is set to empty if null or empty. </remarks>
    /// <param name="control"> The <see cref="TextBoxBase"/> control. </param>
    /// <param name="value">   The value. </param>
    /// <returns> value. </returns>
    public static string Append( this TextBoxBase control, string value )
    {
        if ( control is not null )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                value = string.Empty;
            }
            else if ( control.InvokeRequired )
            {
                _ = control.BeginInvoke( new Func<TextBoxBase, string, string>( TextBoxExtensionMethods.Append ), [control, value] );
            }
            else
            {
                control.SelectionStart = control.Text.Length;
                control.SelectionLength = 0;
                control.SelectedText = control.SelectionStart == 0 ? value : value;
                control.SelectionStart = control.Text.Length;
            }
        }

        return value;
    }

    /// <summary>
    /// Appends formatted text of the <see cref="TextBoxBase">control</see>. This setter is thread
    /// safe.
    /// </summary>
    /// <remarks> The value is set to empty if null or empty. </remarks>
    /// <param name="control"> The <see cref="TextBoxBase"/> control. </param>
    /// <param name="format">  The text format. </param>
    /// <param name="args">    The format arguments. </param>
    /// <returns> value. </returns>
    public static string Append( this TextBoxBase control, string format, params object[] args )
    {
        return control.Append( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    /// <summary>
    /// Prepends <paramref name="value">text</paramref> to the
    /// <see cref="TextBoxBase">control</see>.
    /// This setter is thread safe.
    /// </summary>
    /// <remarks> The value is set to empty if null or empty. </remarks>
    /// <param name="control"> The <see cref="TextBoxBase"/> control. </param>
    /// <param name="value">   The value. </param>
    /// <returns> value. </returns>
    public static string Prepend( this TextBoxBase control, string value )
    {
        if ( control is not null )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                value = string.Empty;
            }
            else if ( control.InvokeRequired )
            {
                _ = control.BeginInvoke( new Func<TextBoxBase, string, string>( TextBoxExtensionMethods.Prepend ), [control, value] );
            }
            else
            {
                control.SelectionStart = 0;
                control.SelectionLength = 0;
                control.SelectedText = value;
                control.SelectionLength = 0;
            }
        }

        return value;
    }

    /// <summary> Prepends the formatted text. This setter is thread safe. </summary>
    /// <remarks> The value is set to empty if null or empty. </remarks>
    /// <param name="control"> The <see cref="TextBoxBase"/> control. </param>
    /// <param name="format">  The text format. </param>
    /// <param name="args">    The format arguments. </param>
    /// <returns> value. </returns>
    public static string Prepend( this TextBoxBase control, string format, params object[] args )
    {
        return control.Prepend( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    #endregion

    #region " special characters "

    /// <summary> Insert special characters based on the provided keystrokes. </summary>
    /// <param name="textBox"> The control. </param>
    /// <param name="e">  Key event information. </param>
    public static void InsertSpecialCharacters( this TextBox textBox, KeyEventArgs e )
    {
        string specialText = e.ObtainSpecialCharacters();
        // insert special character if one was given
        if ( !string.IsNullOrEmpty( specialText ) )
        {
            textBox.SelectedText = specialText;
            e.SuppressKeyPress = true;
        }
    }

    /// <summary> Obtain special characters based on the provided keystrokes. </summary>
    /// <param name="e">       Key event information. </param>
    /// <returns> A <see cref="string" />. </returns>
    /// <remakrs>
    /// <list type="bullet">Special Character codes<item>
    /// [Ctrl] + [Alt] + [-]: em dash ("—") </item><item>
    /// [Ctrl] + [-]: ChrW(173) ¡ optional hyphen (display only when breaking line)</item><item>
    /// [Ctrl] + [Shift] + [~]: ChrW(8220) left double-quote</item><item>
    /// [Ctrl] + [Shift] + [~]: ChrW(8216) left single-quote</item><item>
    /// [Ctrl] + [Shift] + ["]: ChrW(8221) right double-quote</item><item>
    /// [Ctrl] + [']: : ChrW(8217) right single-quote</item><item>
    /// [Ctrl] + [Alt] + [R]: registered trademark ("®")</item><item>
    /// [Ctrl] + [Alt] + [C]: copyright ("©")</item><item>
    /// [Ctrl] + [Alt] + [T]: trademark ("™")</item></list>
    /// </remakrs>
    public static string ObtainSpecialCharacters( this KeyEventArgs e )
    {
        string specialText = string.Empty;
        // [Ctrl] not pressed
        if ( e is null || !e.Control )
        {
            return specialText;
        }

        switch ( e.KeyCode )
        {
            case Keys.OemMinus:
            case Keys.Subtract:
                {
                    // hyphens/dashes
                    if ( e.Alt )
                    {
                        // em dash ("—")
                        // -- [Ctrl] + [Alt] + [-]
                        specialText = "—";
                    }
                    else
                    {
                        // optional hyphen (display only when breaking line)
                        // -- [Ctrl] + [-]
                        specialText = string.Empty;
                    }

                    break;
                }

            case Keys.Oemtilde:
                {
                    // left quotes
                    if ( e.Shift )
                    {
                        // left double-quote
                        // -- [Ctrl] + [Shift] + [~]
                        specialText = "“";
                    }
                    else
                    {
                        // left single-quote
                        // -- [Ctrl] + [`]
                        specialText = "‘";
                    }

                    break;
                }

            case Keys.OemQuotes:
                {
                    // right quotes
                    if ( e.Shift )
                    {
                        // right double-quote
                        // -- [Ctrl] + [Shift] + ["]
                        specialText = "”";
                    }
                    else
                    {
                        // right single-quote
                        // -- [Ctrl] + [']
                        specialText = "’";
                    }

                    break;
                }

            case Keys.C:
                {
                    // copyright ("©")
                    // -- [Ctrl] + [Alt] + [C]
                    if ( e.Alt )
                    {
                        specialText = "©";
                    }

                    break;
                }

            case Keys.R:
                {
                    // registered trademark ("®")
                    // -- [Ctrl] + [Alt] + [R]
                    if ( e.Alt )
                    {
                        specialText = "®";
                    }

                    break;
                }

            case Keys.T:
                {
                    // trademark ("™")
                    // -- [Ctrl] + [Alt] + [T]
                    if ( e.Alt )
                    {
                        specialText = "™";
                    }

                    break;
                }

            case Keys.KeyCode:
                break;
            case Keys.Modifiers:
                break;
            case Keys.None:
                break;
            case Keys.LButton:
                break;
            case Keys.RButton:
                break;
            case Keys.Cancel:
                break;
            case Keys.MButton:
                break;
            case Keys.XButton1:
                break;
            case Keys.XButton2:
                break;
            case Keys.Back:
                break;
            case Keys.Tab:
                break;
            case Keys.LineFeed:
                break;
            case Keys.Clear:
                break;
            case Keys.Return:
                break;
            case Keys.ShiftKey:
                break;
            case Keys.ControlKey:
                break;
            case Keys.Menu:
                break;
            case Keys.Pause:
                break;
            case Keys.Capital:
                break;
            case Keys.KanaMode:
                break;
            case Keys.JunjaMode:
                break;
            case Keys.FinalMode:
                break;
            case Keys.HanjaMode:
                break;
            case Keys.Escape:
                break;
            case Keys.IMEConvert:
                break;
            case Keys.IMENonconvert:
                break;
            case Keys.IMEAccept:
                break;
            case Keys.IMEModeChange:
                break;
            case Keys.Space:
                break;
            case Keys.Prior:
                break;
            case Keys.Next:
                break;
            case Keys.End:
                break;
            case Keys.Home:
                break;
            case Keys.Left:
                break;
            case Keys.Up:
                break;
            case Keys.Right:
                break;
            case Keys.Down:
                break;
            case Keys.Select:
                break;
            case Keys.Print:
                break;
            case Keys.Execute:
                break;
            case Keys.Snapshot:
                break;
            case Keys.Insert:
                break;
            case Keys.Delete:
                break;
            case Keys.Help:
                break;
            case Keys.D0:
                break;
            case Keys.D1:
                break;
            case Keys.D2:
                break;
            case Keys.D3:
                break;
            case Keys.D4:
                break;
            case Keys.D5:
                break;
            case Keys.D6:
                break;
            case Keys.D7:
                break;
            case Keys.D8:
                break;
            case Keys.D9:
                break;
            case Keys.A:
                break;
            case Keys.B:
                break;
            case Keys.D:
                break;
            case Keys.E:
                break;
            case Keys.F:
                break;
            case Keys.G:
                break;
            case Keys.H:
                break;
            case Keys.I:
                break;
            case Keys.J:
                break;
            case Keys.K:
                break;
            case Keys.L:
                break;
            case Keys.M:
                break;
            case Keys.N:
                break;
            case Keys.O:
                break;
            case Keys.P:
                break;
            case Keys.Q:
                break;
            case Keys.S:
                break;
            case Keys.U:
                break;
            case Keys.V:
                break;
            case Keys.W:
                break;
            case Keys.X:
                break;
            case Keys.Y:
                break;
            case Keys.Z:
                break;
            case Keys.LWin:
                break;
            case Keys.RWin:
                break;
            case Keys.Apps:
                break;
            case Keys.Sleep:
                break;
            case Keys.NumPad0:
                break;
            case Keys.NumPad1:
                break;
            case Keys.NumPad2:
                break;
            case Keys.NumPad3:
                break;
            case Keys.NumPad4:
                break;
            case Keys.NumPad5:
                break;
            case Keys.NumPad6:
                break;
            case Keys.NumPad7:
                break;
            case Keys.NumPad8:
                break;
            case Keys.NumPad9:
                break;
            case Keys.Multiply:
                break;
            case Keys.Add:
                break;
            case Keys.Separator:
                break;
            case Keys.Decimal:
                break;
            case Keys.Divide:
                break;
            case Keys.F1:
                break;
            case Keys.F2:
                break;
            case Keys.F3:
                break;
            case Keys.F4:
                break;
            case Keys.F5:
                break;
            case Keys.F6:
                break;
            case Keys.F7:
                break;
            case Keys.F8:
                break;
            case Keys.F9:
                break;
            case Keys.F10:
                break;
            case Keys.F11:
                break;
            case Keys.F12:
                break;
            case Keys.F13:
                break;
            case Keys.F14:
                break;
            case Keys.F15:
                break;
            case Keys.F16:
                break;
            case Keys.F17:
                break;
            case Keys.F18:
                break;
            case Keys.F19:
                break;
            case Keys.F20:
                break;
            case Keys.F21:
                break;
            case Keys.F22:
                break;
            case Keys.F23:
                break;
            case Keys.F24:
                break;
            case Keys.NumLock:
                break;
            case Keys.Scroll:
                break;
            case Keys.LShiftKey:
                break;
            case Keys.RShiftKey:
                break;
            case Keys.LControlKey:
                break;
            case Keys.RControlKey:
                break;
            case Keys.LMenu:
                break;
            case Keys.RMenu:
                break;
            case Keys.BrowserBack:
                break;
            case Keys.BrowserForward:
                break;
            case Keys.BrowserRefresh:
                break;
            case Keys.BrowserStop:
                break;
            case Keys.BrowserSearch:
                break;
            case Keys.BrowserFavorites:
                break;
            case Keys.BrowserHome:
                break;
            case Keys.VolumeMute:
                break;
            case Keys.VolumeDown:
                break;
            case Keys.VolumeUp:
                break;
            case Keys.MediaNextTrack:
                break;
            case Keys.MediaPreviousTrack:
                break;
            case Keys.MediaStop:
                break;
            case Keys.MediaPlayPause:
                break;
            case Keys.LaunchMail:
                break;
            case Keys.SelectMedia:
                break;
            case Keys.LaunchApplication1:
                break;
            case Keys.LaunchApplication2:
                break;
            case Keys.OemSemicolon:
                break;
            case Keys.Oemplus:
                break;
            case Keys.Oemcomma:
                break;
            case Keys.OemPeriod:
                break;
            case Keys.OemQuestion:
                break;
            case Keys.OemOpenBrackets:
                break;
            case Keys.OemPipe:
                break;
            case Keys.OemCloseBrackets:
                break;
            case Keys.Oem8:
                break;
            case Keys.OemBackslash:
                break;
            case Keys.ProcessKey:
                break;
            case Keys.Packet:
                break;
            case Keys.Attn:
                break;
            case Keys.Crsel:
                break;
            case Keys.Exsel:
                break;
            case Keys.EraseEof:
                break;
            case Keys.Play:
                break;
            case Keys.Zoom:
                break;
            case Keys.NoName:
                break;
            case Keys.Pa1:
                break;
            case Keys.OemClear:
                break;
            case Keys.Shift:
                break;
            case Keys.Control:
                break;
            case Keys.Alt:
                break;
            default:
                break;
        }

        return specialText;
    }

    #endregion
}
