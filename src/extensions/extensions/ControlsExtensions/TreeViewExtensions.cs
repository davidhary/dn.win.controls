using System.Windows.Forms;

namespace cc.isr.WinControls.TreeViewExtensions;

/// <summary> Includes extensions for <see cref="TreeView">Tree View</see> control. </summary>
/// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2010-11-19, 1.2.3975 </para></remarks>
public static class TreeViewExtensionMethods
{
    #region " tree view "

    /// <summary> Selects a node. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="control">  The <see cref="TreeView"/> control. </param>
    /// <param name="nodeName"> Name of the node. </param>
    public static void SelectNode( this TreeView control, string nodeName )
    {
        if ( control is not null )
        {
            control.SelectedNode = control.Nodes[nodeName];
        }
    }

    /// <summary> Selects a node. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    /// <param name="control">       The <see cref="TreeView"/> control. </param>
    /// <param name="nodeHierarchy"> The hierarchy of node names. </param>
    public static void SelectNode( this TreeView control, string[] nodeHierarchy )
    {
        if ( !(control is null || nodeHierarchy is null || (nodeHierarchy.Length == 0)) )
        {
            TreeNode? parentTreeNode = null;
            TreeNode? childTreeNode;
            foreach ( string nodeName in nodeHierarchy )
            {
                if ( parentTreeNode is null || parentTreeNode.Nodes is null )
                {
                    parentTreeNode = control.Nodes[nodeName];
                    childTreeNode = parentTreeNode;
                }
                else
                {
                    childTreeNode = parentTreeNode.Nodes.Count > 0 ? parentTreeNode.Nodes[nodeName] : null;
                }

                if ( childTreeNode is null )
                {
                    break;
                }
                else
                {
                    parentTreeNode = childTreeNode;
                }
            }

            control.SelectedNode = parentTreeNode;
        }
    }

    #endregion
}
