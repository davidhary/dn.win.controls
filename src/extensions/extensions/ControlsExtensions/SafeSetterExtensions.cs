using System;
using System.Windows.Forms;

namespace cc.isr.WinControls.ControlExtensions;
public static partial class ControlExtensionMethods
{
    /// <summary>
    /// A <see cref="Control"/> extension method that safely invokes the specified
    /// <paramref name="actionMethod"/> asynchronously.
    /// </summary>
    /// <remarks>   David, 2020-07-27. </remarks>
    /// <param name="control">      The control which method to invoke. </param>
    /// <param name="actionMethod"> The action method. </param>
    public static void SafelyBeginInvoke( this Control control, Action actionMethod )
    {
        if ( control is not null )
        {
            if ( control.InvokeRequired )
            {
                _ = control.BeginInvoke( new Action<Control, Action>( SafelyBeginInvoke ), [control, actionMethod] );
            }
            else if ( control.IsHandleCreated )
            {
                actionMethod.Invoke();
            }
        }
    }

    /// <summary>   A <see cref="Control"/> extension method that safe text writer. </summary>
    /// <remarks>   David, 2020-09-25. </remarks>
    /// <param name="control">  The control which text property to set. </param>
    /// <param name="value">    The value. </param>
    public static void SafeTextWriter( this Control control, string value )
    {
        control.SafelyBeginInvoke( new Action( () => control.Text = value ) );
    }

    /// <summary>
    /// A <see cref="Control"/> extension method that safely invokes the specified
    /// <paramref name="functionMethod"/> asynchronously.
    /// </summary>
    /// <remarks>   David, 2020-09-25. </remarks>
    /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
    ///                                                 are null. </exception>
    /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
    ///                                                 invalid. </exception>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="control">          The control which method to invoke. </param>
    /// <param name="functionMethod">   The function method. </param>
    /// <returns>   A <typeparamref name="T"/>. </returns>
    public static T SafelyBeginInvoke<T>( this Control control, Func<T> functionMethod )
    {
#if false
        // left this code for debugging.
        if ( control.InvokeRequired )
        {
            return ( T ) control.BeginInvoke( new Func<Control, Func<T>, T>( SafeSetter ), new object[] { control, setter } );
        }
        else
        {
            return ( T ) setter.Invoke();
        }
#else
        return control is not not null ? throw new ArgumentNullException( nameof( control ) )
            : control.InvokeRequired
            ? ( T ) control.BeginInvoke( new Func<Control, Func<T>, T>( SafelyBeginInvoke ), [control, functionMethod] )
            : control.IsHandleCreated
                ? functionMethod.Invoke()
                : throw new InvalidOperationException( $"{nameof( control )} handle was not created" );
#endif
    }

    /// <summary>   A <see cref="Control"/> extension method that safely sets a text value. </summary>
    /// <remarks>   David, 2020-09-25. </remarks>
    /// <param name="control">  The control which text to set. </param>
    /// <param name="value">    The value. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string SafeTextSetter( this Control control, string value )
    {
        return control.SafelyBeginInvoke( new Func<string>( () => { control.Text = value; return control.Text; } ) );
    }

    /// <summary>
    /// A <see cref="StatusStrip"/> extension method that safely invokes the specified
    /// <paramref name="actionMethod"/> asynchronously.
    /// </summary>
    /// <remarks>   David, 2020-07-27. </remarks>
    /// <param name="control">      The control which methods to invoke. </param>
    /// <param name="actionMethod"> The action method. </param>
    public static void SafelyBeginInvoke( this StatusStrip control, Action actionMethod )
    {
        if ( control is not null && control.Parent is not null )
        {
            if ( control.Parent.InvokeRequired )
            {
                _ = control.Parent.BeginInvoke( new Action<Control, Action>( SafelyBeginInvoke ), [control, actionMethod] );
            }
            else if ( control.Parent.IsHandleCreated )
            {
                actionMethod.Invoke();
            }
        }
    }

    /// <summary>   A <see cref="ToolStripItem"/> extension method that safely invokes the specified <paramref name="actionMethod"/>. </summary>
    /// <remarks>   David, 2020-07-27. </remarks>
    /// <param name="control">      The control to enable or disable. </param>
    /// <param name="actionMethod"> . </param>
    public static void SafelyBeginInvoke( this ToolStripItem control, Action actionMethod )
    {
        if ( control is not null && control.Owner is not null )
        {
            if ( control.Owner.InvokeRequired )
            {
                _ = control.Owner.BeginInvoke( new Action<Control, Action>( SafelyBeginInvoke ), [control, actionMethod] );
            }
            else if ( control.Owner.IsHandleCreated )
            {
                actionMethod.Invoke();
            }
        }
    }
}
