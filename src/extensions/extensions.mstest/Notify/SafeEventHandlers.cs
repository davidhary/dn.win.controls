using System.ComponentModel;

namespace cc.isr.WinControls.MSTest;
internal static class SafeEventHandlers
{
    /// <summary>
    /// Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled entity</see>)
    /// or (Dynamically Invokes) a change
    /// <see cref="PropertyChangedEventHandler">Event</see> in a property value.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="handler"> The handler. </param>
    /// <param name="sender">  Source of the event. </param>
    /// <param name="e">       The <see cref="PropertyChangedEventArgs" /> instance containing the
    /// event data. </param>
    public static void SafeInvoke( PropertyChangedEventHandler handler, object sender, PropertyChangedEventArgs e )
    {
        foreach ( Delegate d in handler is null ? ([]) : handler.GetInvocationList() )
        {
            _ = d.Target is null
                ? d.DynamicInvoke( [sender, e] )
                : d.Target is not ISynchronizeInvoke target ? d.DynamicInvoke( [sender, e] ) : target.Invoke( d, [sender, e] );
        }
    }
}
