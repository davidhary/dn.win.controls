using cc.isr.WinControls.CompactExtensions;

namespace cc.isr.WinControls.MSTest;

/// <summary> String extensions tests. </summary>
/// <remarks>
/// <para>
/// David, 2018-03-14 </para>
/// </remarks>
[TestClass]
public class StringExtensionsTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            Console.WriteLine( $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}" );
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets the test context which provides information about and functionality for the current test
    /// run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " compact tests "

    /// <summary> (Unit Test Method) tests compact string. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod]
    public void CompactStringTest()
    {
        string candidate = "Compact this string";
        string compactedValue = string.Empty;
        string expectedValue;
        using ( TextBox textBox = new() { Width = 100, Font = new Font( System.Drawing.FontFamily.GenericSansSerif, 10f ), AutoSize = false } )
        {
            compactedValue = candidate.Compact( textBox, System.Windows.Forms.TextFormatFlags.EndEllipsis );
            expectedValue = "Compact thi...";
            Assert.AreEqual( expectedValue, compactedValue, $"Failed compacting in a text box with {System.Windows.Forms.TextFormatFlags.EndEllipsis}" );
            compactedValue = candidate.Compact( textBox, System.Windows.Forms.TextFormatFlags.PathEllipsis );
            expectedValue = "pact t...his str";
            Assert.AreEqual( expectedValue, compactedValue, $"Failed compacting in a text box with {System.Windows.Forms.TextFormatFlags.PathEllipsis}" );
            compactedValue = candidate.Compact( textBox, System.Windows.Forms.TextFormatFlags.WordEllipsis );
            expectedValue = "...ct this string";
            Assert.AreEqual( expectedValue, compactedValue, $"Failed compacting in a text box with {System.Windows.Forms.TextFormatFlags.WordEllipsis}" );
        }

        using ( ToolStripStatusLabel label = new() { Width = 100, Font = new Font( System.Drawing.FontFamily.GenericSansSerif, 10f ), AutoSize = false } )
        {
            compactedValue = candidate.Compact( label, System.Windows.Forms.TextFormatFlags.PathEllipsis );
        }

        expectedValue = "pact t...his str";
        Assert.AreEqual( expectedValue, compactedValue, $"Failed compacting in a tool strip label with {System.Windows.Forms.TextFormatFlags.PathEllipsis}" );
    }

    #endregion
}
