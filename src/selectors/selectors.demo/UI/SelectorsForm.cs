namespace cc.isr.WinControls.Demo;

/// <summary>   Form for viewing the selector. </summary>
/// <remarks>
/// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-03-06 </para>
/// </remarks>
public partial class SelectorsForm
{
    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public SelectorsForm() => this.InitializeComponent();

    /// <summary>   Reads only check box checked changed. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ReadOnlyCheckBox_CheckedChanged( object? sender, EventArgs e )
    {
        this._selectorComboBox.WriteProtected = this._readOnlyCheckBox.Checked;
        this._selectorNumeric.WriteProtected = this._readOnlyCheckBox.Checked;
        this._toolStripSelectorComboBox.ReadOnly = this._readOnlyCheckBox.Checked;
        this._toolStripSelectorNumeric.ReadOnly = this._readOnlyCheckBox.Checked;
    }

    /// <summary>   Applies the button click. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ApplyButton_Click( object? sender, EventArgs e )
    {
        this._selectorComboBox.Text = this._enterNumeric.Text;
        this._selectorNumeric.Value = this._enterNumeric.Value;
        this._toolStripSelectorComboBox.Text = this._enterNumeric.Text;
        this._toolStripSelectorNumeric.Value = this._enterNumeric.Value;
    }

    /// <summary>   Select button click. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void SelectButton_Click( object? sender, EventArgs e )
    {
        this._selectorComboBox.SelectValue();
        this._selectorNumeric.SelectValue();
        this._toolStripSelectorComboBox.SelectValue();
        this._toolStripSelectorNumeric.SelectValue();
    }

    /// <summary>   Selector combo box value selected. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void SelectorComboBox_ValueSelected( object? sender, EventArgs e )
    {
        this._selectedTextBox.Text = this._selectorComboBox.SelectedText;
    }

    /// <summary>   Selector numeric value selected. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void SelectorNumeric_ValueSelected( object? sender, EventArgs e )
    {
        this._selectedTextBox.Text = this._selectorNumeric.SelectedText;
    }

    /// <summary>   Tool strip selector combo box value selected. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ToolStripSelectorComboBox_ValueSelected( object? sender, EventArgs e )
    {
        this._selectedTextBox.Text = this._toolStripSelectorComboBox.SelectedText;
    }

    /// <summary>   Tool strip selector numeric value selected. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ToolStripSelectorNumeric_ValueSelected( object? sender, EventArgs e )
    {
        this._selectedTextBox.Text = this._toolStripSelectorNumeric.SelectedText;
    }

}
