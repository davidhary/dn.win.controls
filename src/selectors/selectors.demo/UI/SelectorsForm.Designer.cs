using System.Diagnostics;

namespace cc.isr.WinControls.Demo;

/// <summary>   Form for viewing the selector. </summary>
/// <remarks>   David, 2021-03-12. </remarks>
public partial class SelectorsForm : Form
{
    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="Form" />.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
    ///                                                         resources; <see langword="false" /> to
    ///                                                         release only unmanaged resources. </param>
    [DebuggerNonUserCode()]
    protected override void Dispose(bool disposing)
    {
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose(disposing);
        }
    }

    /// <summary>   Required by the Windows Form Designer. </summary>
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// NOTE: The following procedure is required by the Windows Form Designer It can be modified
    /// using the Windows Form Designer.  
    /// Do not modify it using the code editor.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        var resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectorsForm));
        _toolTip = new ToolTip(components);
        _selectedTextBox = new TextBox();
        _readOnlyCheckBox = new CheckBox();
        _readOnlyCheckBox.CheckedChanged += new EventHandler(ReadOnlyCheckBox_CheckedChanged);
        _enterValueNumericLabel = new Label();
        _editedEngineeringUpDownLabel = new Label();
        _scaledValueTextBoxLabel = new Label();
        _applyButton = new Button();
        _applyButton.Click += new EventHandler(ApplyButton_Click);
        _enterValueLabel = new ToolStripTextBox();
        _selectedValueLabel = new ToolStripTextBox();
        _selectorComboBox = new cc.isr.WinControls.SelectorComboBox();
        _selectorComboBox.ValueSelected += new EventHandler<EventArgs>(SelectorComboBox_ValueSelected);
        _selectButton = new Button();
        _selectButton.Click += new EventHandler(SelectButton_Click);
        _enterNumeric = new cc.isr.WinControls.EngineeringUpDown();
        _selectorNumeric = new cc.isr.WinControls.SelectorNumeric();
        _selectorNumeric.ValueSelected += new EventHandler<EventArgs>(SelectorNumeric_ValueSelected);
        _toolStrip = new ToolStrip();
        _toolStripSelectorComboBox = new cc.isr.WinControls.ToolStripSelectorComboBox();
        _toolStripSelectorComboBox.ValueSelected += new EventHandler<EventArgs>(ToolStripSelectorComboBox_ValueSelected);
        _toolStripSelectorNumeric = new cc.isr.WinControls.ToolStripSelectorNumeric();
        _toolStripSelectorNumeric.ValueSelected += new EventHandler<EventArgs>(ToolStripSelectorNumeric_ValueSelected);
        ((System.ComponentModel.ISupportInitialize)_enterNumeric).BeginInit();
        _toolStrip.SuspendLayout();
        SuspendLayout();
        // 
        // _selectedTextBox
        // 
        _selectedTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _selectedTextBox.ForeColor = SystemColors.ActiveCaption;
        _selectedTextBox.Location = new Point(147, 122);
        _selectedTextBox.Name = "_SelectedTextBox";
        _selectedTextBox.Size = new Size(121, 25);
        _selectedTextBox.TabIndex = 4;
        _selectedTextBox.Text = "ABC";
        _toolTip.SetToolTip(_selectedTextBox, "Scaled value");
        // 
        // _readOnlyCheckBox
        // 
        _readOnlyCheckBox.AutoSize = true;
        _readOnlyCheckBox.Location = new Point(16, 171);
        _readOnlyCheckBox.Name = "_ReadOnlyCheckBox";
        _readOnlyCheckBox.Size = new Size(87, 21);
        _readOnlyCheckBox.TabIndex = 8;
        _readOnlyCheckBox.Text = "Read Only";
        _readOnlyCheckBox.UseVisualStyleBackColor = true;
        // 
        // _enterValueNumericLabel
        // 
        _enterValueNumericLabel.AutoSize = true;
        _enterValueNumericLabel.Location = new Point(10, 65);
        _enterValueNumericLabel.Name = "_EnterValueNumericLabel";
        _enterValueNumericLabel.Size = new Size(112, 17);
        _enterValueNumericLabel.TabIndex = 1;
        _enterValueNumericLabel.Text = "Enter value to set:";
        _enterValueNumericLabel.TextAlign = ContentAlignment.TopRight;
        // 
        // _editedEngineeringUpDownLabel
        // 
        _editedEngineeringUpDownLabel.AutoSize = true;
        _editedEngineeringUpDownLabel.Location = new Point(13, 102);
        _editedEngineeringUpDownLabel.Name = "_EditedEngineeringUpDownLabel";
        _editedEngineeringUpDownLabel.Size = new Size(69, 17);
        _editedEngineeringUpDownLabel.TabIndex = 1;
        _editedEngineeringUpDownLabel.Text = "Edit Value:";
        _editedEngineeringUpDownLabel.TextAlign = ContentAlignment.TopRight;
        // 
        // _scaledValueTextBoxLabel
        // 
        _scaledValueTextBoxLabel.AutoSize = true;
        _scaledValueTextBoxLabel.Location = new Point(145, 102);
        _scaledValueTextBoxLabel.Name = "_ScaledValueTextBoxLabel";
        _scaledValueTextBoxLabel.Size = new Size(104, 17);
        _scaledValueTextBoxLabel.TabIndex = 1;
        _scaledValueTextBoxLabel.Text = "Observed Value:";
        _scaledValueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
        // 
        // _applyButton
        // 
        _applyButton.Location = new Point(146, 163);
        _applyButton.Name = "_applyButton";
        _applyButton.Size = new Size(122, 35);
        _applyButton.TabIndex = 10;
        _applyButton.Text = "Apply Edit Value";
        _applyButton.UseVisualStyleBackColor = true;
        // 
        // _enterValueLabel
        // 
        _enterValueLabel.Name = "_EnterValueLabel";
        _enterValueLabel.Size = new Size(47, 28);
        _enterValueLabel.Text = "Entered";
        // 
        // _selectedValueLabel
        // 
        _selectedValueLabel.Name = "_SelectedValueLabel";
        _selectedValueLabel.Size = new Size(51, 28);
        _selectedValueLabel.Text = "Selected";
        // 
        // _selectorComboBox
        // 
        _selectorComboBox.AutoSizeMode = AutoSizeMode.GrowAndShrink;
        _selectorComboBox.DirtyBackColor = Color.Orange;
        _selectorComboBox.DirtyForeColor = SystemColors.ActiveCaption;
        _selectorComboBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _selectorComboBox.Location = new Point(311, 122);
        _selectorComboBox.Margin = new Padding(3, 4, 3, 4);
        _selectorComboBox.Name = "_SelectorComboBox";
        _selectorComboBox.SelectedText = null;
        _selectorComboBox.SelectorIcon = (Image)resources.GetObject("_SelectorComboBox.SelectorIcon");
        _selectorComboBox.Size = new Size(99, 25);
        _selectorComboBox.TabIndex = 11;
        // 
        // _selectButton
        // 
        _selectButton.Location = new Point(311, 162);
        _selectButton.Name = "_SelectButton";
        _selectButton.Size = new Size(75, 36);
        _selectButton.TabIndex = 12;
        _selectButton.Text = "Select";
        _selectButton.UseVisualStyleBackColor = true;
        // 
        // _enterNumeric
        // 
        _enterNumeric.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _enterNumeric.ForeColor = SystemColors.WindowText;
        _enterNumeric.Location = new Point(16, 122);
        _enterNumeric.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
        _enterNumeric.Minimum = new decimal(new int[] { 1000, 0, 0, (int)-2147483648L });
        _enterNumeric.Name = "_EnterNumeric";
        _enterNumeric.NullValue = new decimal(new int[] { 0, 0, 0, 0 });
        _enterNumeric.ReadOnlyBackColor = Color.FromArgb(224, 224, 224);
        _enterNumeric.ReadOnlyForeColor = Color.Black;
        _enterNumeric.ReadWriteBackColor = SystemColors.Window;
        _enterNumeric.ReadWriteForeColor = Color.Black;
        _enterNumeric.ScaledValue = new decimal(new int[] { 0, 0, 0, 0 });
        _enterNumeric.Size = new Size(120, 25);
        _enterNumeric.TabIndex = 6;
        _enterNumeric.UnscaledValue = new decimal(new int[] { 0, 0, 0, 0 });
        _enterNumeric.UpDownDisplayMode = cc.isr.WinControls.UpDownButtonsDisplayMode.WhenMouseOver;
        _enterNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
        // 
        // _selectorNumeric
        // 
        _selectorNumeric.AutoSizeMode = AutoSizeMode.GrowAndShrink;
        _selectorNumeric.DirtyBackColor = Color.Orange;
        _selectorNumeric.DirtyForeColor = SystemColors.ActiveCaption;
        _selectorNumeric.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _selectorNumeric.Location = new Point(442, 122);
        _selectorNumeric.Margin = new Padding(3, 4, 3, 4);
        _selectorNumeric.Name = "_SelectorNumeric";
        _selectorNumeric.SelectedText = null;
        _selectorNumeric.SelectedValue = default;
        _selectorNumeric.SelectorIcon = (Image)resources.GetObject("_SelectorNumeric.SelectorIcon");
        _selectorNumeric.Size = new Size(76, 25);
        _selectorNumeric.TabIndex = 13;
        _selectorNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
        // 
        // ToolStrip1
        // 
        _toolStrip.Items.AddRange(new ToolStripItem[] { _toolStripSelectorComboBox, _toolStripSelectorNumeric });
        _toolStrip.Location = new Point(0, 0);
        _toolStrip.Name = "ToolStrip1";
        _toolStrip.Size = new Size(622, 28);
        _toolStrip.TabIndex = 14;
        _toolStrip.Text = "ToolStrip1";
        // 
        // _toolStripSelectorComboBox
        // 
        _toolStripSelectorComboBox.AutoSize = false;
        _toolStripSelectorComboBox.DirtyBackColor = Color.Orange;
        _toolStripSelectorComboBox.DirtyForeColor = SystemColors.ActiveCaption;
        _toolStripSelectorComboBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _toolStripSelectorComboBox.Name = "_ToolStripSelectorComboBox";
        _toolStripSelectorComboBox.SelectedText = null;
        _toolStripSelectorComboBox.SelectorIcon = (Image)resources.GetObject("_ToolStripSelectorComboBox.SelectorIcon");
        _toolStripSelectorComboBox.Size = new Size(91, 25);
        // 
        // _toolStripSelectorNumeric
        // 
        _toolStripSelectorNumeric.AutoSize = false;
        _toolStripSelectorNumeric.DirtyBackColor = Color.Orange;
        _toolStripSelectorNumeric.DirtyForeColor = SystemColors.ActiveCaption;
        _toolStripSelectorNumeric.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _toolStripSelectorNumeric.Name = "_ToolStripSelectorNumeric";
        _toolStripSelectorNumeric.SelectedText = null;
        _toolStripSelectorNumeric.SelectorIcon = (Image)resources.GetObject("_ToolStripSelectorNumeric.SelectorIcon");
        _toolStripSelectorNumeric.Size = new Size(74, 25);
        _toolStripSelectorNumeric.Text = "0";
        _toolStripSelectorNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
        // 
        // SelectorForm
        // 
        AutoScaleDimensions = new SizeF(7.0f, 17.0f);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(622, 263);
        Controls.Add(_toolStrip);
        Controls.Add(_selectorNumeric);
        Controls.Add(_selectButton);
        Controls.Add(_selectorComboBox);
        Controls.Add(_applyButton);
        Controls.Add(_readOnlyCheckBox);
        Controls.Add(_enterNumeric);
        Controls.Add(_selectedTextBox);
        Controls.Add(_scaledValueTextBoxLabel);
        Controls.Add(_editedEngineeringUpDownLabel);
        Controls.Add(_enterValueNumericLabel);
        Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        FormBorderStyle = FormBorderStyle.FixedDialog;
        Margin = new Padding(3, 4, 3, 4);
        Name = "SelectorsForm";
        Text = "Selectors Form";
        ((System.ComponentModel.ISupportInitialize)_enterNumeric).EndInit();
        _toolStrip.ResumeLayout(false);
        _toolStrip.PerformLayout();
        ResumeLayout(false);
        PerformLayout();
    }

    private ToolTip _toolTip;
    private EngineeringUpDown _enterNumeric;
    private TextBox _selectedTextBox;
    private CheckBox _readOnlyCheckBox;
    private Label _enterValueNumericLabel;
    private Label _editedEngineeringUpDownLabel;
    private Label _scaledValueTextBoxLabel;
    private Button _applyButton;
    private ToolStripTextBox _enterValueLabel;
    private ToolStripTextBox _selectedValueLabel;
    private cc.isr.WinControls.SelectorComboBox _selectorComboBox;
    private Button _selectButton;
    private cc.isr.WinControls.SelectorNumeric _selectorNumeric;
    private ToolStrip _toolStrip;
    private ToolStripSelectorComboBox _toolStripSelectorComboBox;
    private ToolStripSelectorNumeric _toolStripSelectorNumeric;
}
