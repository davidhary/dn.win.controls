using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace cc.isr.WinControls;

/// <summary> Tool strip selector base. </summary>
/// <remarks>
/// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-03-02 </para>
/// </remarks>
[ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
public class ToolStripSelectorBase : ToolStripControlHost
{
    /// <summary> Default constructor. </summary>
    /// <remarks> Call the base constructor passing in a base selector instance. </remarks>
    /// <param name="selectorControl"> The selector control. </param>
    public ToolStripSelectorBase( SelectorControlBase selectorControl ) : base( selectorControl ) => this.AutoSize = false;

    /// <summary>
    /// Releases the unmanaged resources used by the
    /// <see cref="ToolStripControlHost" /> and optionally releases the
    /// managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    /// resources; <see langword="false" /> to release only unmanaged
    /// resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.RemoveDirtyChangedEventHandler( this.DirtyChanged );
                this.RemoveValueSelectedEventHandler( this.ValueSelected );
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #region " colors "

    /// <summary> Gets or sets the color of the read only back. </summary>
    /// <value> The color of the read only back. </value>
    [DefaultValue( typeof( Color ), "SystemColors.AppWorkspace" )]
    [Description( "Back color when dirty" )]
    [Category( "Appearance" )]
    public Color DirtyBackColor
    {
        get => this.SelectorControlBase?.DirtyBackColor ?? SystemColors.AppWorkspace;
        set
        {
            if ( this.SelectorControlBase is not null )
                this.SelectorControlBase.DirtyBackColor = value;
        }
    }

    /// <summary> Gets or sets the color of the read only foreground. </summary>
    /// <value> The color of the read only foreground. </value>
    [DefaultValue( typeof( Color ), "SystemColors.InactiveCaption" )]
    [Description( "Fore color when dirty" )]
    [Category( "Appearance" )]
    public Color DirtyForeColor
    {
        get => this.SelectorControlBase?.DirtyForeColor ?? SystemColors.InactiveCaption;
        set
        {
            if ( this.SelectorControlBase is not null )
                this.SelectorControlBase.DirtyForeColor = value;
        }
    }

    #endregion

    #region " underlying control properties "

    /// <summary> Gets the numeric up down control. </summary>
    /// <value> The numeric up down control. </value>
    public SelectorControlBase? SelectorControlBase => this.Control as SelectorControlBase;

    /// <summary> Gets a value indicating whether this object has value. </summary>
    /// <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public bool HasValue => this.SelectorControlBase?.HasValue ?? false;

    /// <summary> The watermark text. </summary>
    /// <value> The water mark text with this control. </value>
    [DefaultValue( typeof( string ), "Watermark" )]
    [Description( "Watermark Text" )]
    [Category( "Appearance" )]
    public string? Watermark
    {
        get => this.SelectorControlBase?.Watermark;
        set
        {
            if ( this.SelectorControlBase is not null )
                this.SelectorControlBase.Watermark = value;
        }

    }

    /// <summary> Gets or sets the selected text. </summary>
    /// <value> The selected text. </value>
    [DefaultValue( "" )]
    [Description( "Selected text" )]
    [Category( "Appearance" )]
    public string? SelectedText
    {
        get => this.SelectorControlBase?.SelectedText;
        set
        {
            if ( this.SelectorControlBase is not null )
                this.SelectorControlBase.SelectedText = value;
        }
    }

    /// <summary> Gets or sets the selected text. </summary>
    /// <value> The selected text. </value>
    [DefaultValue( "" )]
    [Description( "text" )]
    [Category( "Appearance" )]
    public override string Text
    {
        get => this.SelectorControlBase?.Text ?? string.Empty;

#pragma warning disable IDE0079
#pragma warning disable CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).
        set
        {
            if ( this.SelectorControlBase is not null )
                this.SelectorControlBase.Text = value ?? string.Empty;
        }
#pragma warning restore CS8765
#pragma warning restore IDE0079
    }

    /// <summary> Selector icon image. </summary>
    /// <value> The selector icon. </value>
    [Description( "Selector icon image" )]
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image? SelectorIcon
    {
        get => this.SelectorControlBase?.SelectorIcon;
        set
        {
            if ( this.SelectorControlBase is not null )
                this.SelectorControlBase.SelectorIcon = value;
        }
    }

    /// <summary> Gets or sets the read only property. </summary>
    /// <value> The read only. </value>
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Indicates whether the check box is read only." )]
    public bool ReadOnly
    {
        get => this.SelectorControlBase?.WriteProtected ?? false;
        set
        {
            if ( this.SelectorControlBase is not null )
                this.SelectorControlBase.WriteProtected = value;
        }
    }

    #endregion

    #region " underlying control methods "

    /// <summary> Select value. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void SelectValue()
    {
        this.SelectorControlBase?.SelectValue();
    }

    #endregion

    #region " event handlers "

    /// <summary> Subscribes events from the hosted control. </summary>
    /// <remarks> Subscribe the control events to expose. </remarks>
    /// <param name="control"> The control from which to subscribe events. </param>
    protected override void OnSubscribeControlEvents( Control? control )
    {
        if ( control is not null )
        {
            // Call the base so the base events are connected.
            base.OnSubscribeControlEvents( control );

            // Cast the control to a Selector control base.
            if ( control is SelectorControlBase ctrl )
            {
                // Add the event.
                ctrl.DirtyChanged += this.OnDirtyChanged;
                ctrl.ValueSelected += this.OnValueSelected;
            }
        }
    }

    /// <summary> Unsubscribes events from the hosted control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="control"> The control from which to unsubscribe events. </param>
    protected override void OnUnsubscribeControlEvents( Control? control )
    {
        // Call the base method so the basic events are unsubscribed.
        base.OnUnsubscribeControlEvents( control );

        // Cast the control to a Selector control base.
        if ( control is SelectorControlBase ctrl )
        {
            // Remove the events.
            ctrl.DirtyChanged -= this.OnDirtyChanged;
            ctrl.ValueSelected -= this.OnValueSelected;
        }
    }

    /// <summary> Event queue for all listeners interested in DirtyChanged events. </summary>
    public event EventHandler<EventArgs>? DirtyChanged;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveDirtyChangedEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                DirtyChanged -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Raises the value selected event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information to send to registered event handlers. </param>
    private void OnDirtyChanged( object? sender, EventArgs e )
    {
        this.DirtyChanged?.Invoke( this, e );
    }

    /// <summary> Event queue for all listeners interested in ValueSelected events. </summary>
    public event EventHandler<EventArgs>? ValueSelected;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveValueSelectedEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                ValueSelected -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Raises the value selected event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information to send to registered event handlers. </param>
    private void OnValueSelected( object? sender, EventArgs e )
    {
        this.ValueSelected?.Invoke( this, e );
    }

    #endregion
}
