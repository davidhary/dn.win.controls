using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

public partial class SelectorOpener
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        _errorProvider = new ErrorProvider(components);
        _toolStrip = new ToolStrip();
        _toolStrip.DoubleClick += new EventHandler(ToolStripDoubleClick);
        _clearButton = new ToolStripButton();
        _clearButton.Click += new EventHandler(ClearButtonClick);
        _resourceNamesComboBox = new ToolStripSpringComboBox();
        _resourceNamesComboBox.DoubleClick += new EventHandler(ToolStripDoubleClick);
        _toggleOpenButton = new ToolStripButton();
        _toggleOpenButton.Click += new EventHandler(ToggleOpenButtonClick);
        _enumerateButton = new ToolStripButton();
        _enumerateButton.Click += new EventHandler(EnumerateButtonClick);
        _overflowLabel = new System.Windows.Forms.ToolStripLabel();
        ((System.ComponentModel.ISupportInitialize)_errorProvider).BeginInit();
        _toolStrip.SuspendLayout();
        SuspendLayout();
        // 
        // _errorProvider
        // 
        _errorProvider.ContainerControl = this;
        // 
        // _toolStrip
        // 
        _toolStrip.GripStyle = ToolStripGripStyle.Hidden;
        _toolStrip.Items.AddRange(new ToolStripItem[] { _clearButton, _resourceNamesComboBox, _toggleOpenButton, _enumerateButton, _overflowLabel });
        _toolStrip.Location = new Point(0, 0);
        _toolStrip.Name = "_ToolStrip";
        _toolStrip.Size = new Size(474, 29);
        _toolStrip.TabIndex = 0;
        _toolStrip.Text = "Selector Opener";
        // 
        // _clearButton
        // 
        _clearButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _clearButton.Image = Properties.Resources.Clear_22x22;
        _clearButton.ImageScaling = ToolStripItemImageScaling.None;
        _clearButton.ImageTransparentColor = Color.Magenta;
        _clearButton.Name = "_ClearButton";
        _clearButton.Overflow = ToolStripItemOverflow.Never;
        _clearButton.Size = new Size(26, 26);
        _clearButton.Text = "Clear known state";
        // 
        // _resourceNamesComboBox
        // 
        _resourceNamesComboBox.Name = "_ResourceNamesComboBox";
        _resourceNamesComboBox.Overflow = ToolStripItemOverflow.Never;
        _resourceNamesComboBox.Size = new Size(362, 29);
        _resourceNamesComboBox.ToolTipText = "Available resource names";
        // 
        // _toggleOpenButton
        // 
        _toggleOpenButton.Alignment = ToolStripItemAlignment.Right;
        _toggleOpenButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _toggleOpenButton.Image = Properties.Resources.Disconnected_22x22;
        _toggleOpenButton.ImageScaling = ToolStripItemImageScaling.None;
        _toggleOpenButton.ImageTransparentColor = Color.Magenta;
        _toggleOpenButton.Name = "_ToggleOpenButton";
        _toggleOpenButton.Overflow = ToolStripItemOverflow.Never;
        _toggleOpenButton.Size = new Size(26, 26);
        _toggleOpenButton.ToolTipText = "Click to open";
        // 
        // _enumerateButton
        // 
        _enumerateButton.Alignment = ToolStripItemAlignment.Right;
        _enumerateButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _enumerateButton.Image = Properties.Resources.Find_22x22;
        _enumerateButton.ImageScaling = ToolStripItemImageScaling.None;
        _enumerateButton.ImageTransparentColor = Color.Magenta;
        _enumerateButton.Name = "_EnumerateButton";
        _enumerateButton.Overflow = ToolStripItemOverflow.Never;
        _enumerateButton.Size = new Size(26, 26);
        _enumerateButton.ToolTipText = "Enumerate resources";
        // 
        // _overflowLabel
        // 
        _overflowLabel.Name = "_OverflowLabel";
        _overflowLabel.Size = new Size(0, 26);
        // 
        // SelectorOpener
        // 
        BackColor = Color.Transparent;
        Controls.Add(_toolStrip);
        Margin = new Padding(0);
        Name = "SelectorOpener";
        Size = new Size(474, 29);
        ((System.ComponentModel.ISupportInitialize)_errorProvider).EndInit();
        _toolStrip.ResumeLayout(false);
        _toolStrip.PerformLayout();
        ResumeLayout(false);
        PerformLayout();
    }

    private ErrorProvider _errorProvider;
    private ToolStripButton _clearButton;
    private ToolStripSpringComboBox _resourceNamesComboBox;
    private ToolStripButton _enumerateButton;
    private ToolStripButton _toggleOpenButton;
    private ToolStrip _toolStrip;
    private System.Windows.Forms.ToolStripLabel _overflowLabel;
}
