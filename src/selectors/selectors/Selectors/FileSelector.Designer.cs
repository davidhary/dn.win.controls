using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace cc.isr.WinControls;

public partial class FileSelector
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;
    internal ToolTip _toolTip;
    private System.Windows.Forms.TextBox _filePathTextBox;
    private Button _browseButton;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        _toolTip = new ToolTip(components);
        _browseButton = new Button();
        _browseButton.Click += new EventHandler(BrowseButton_Click);
        _filePathTextBox = new System.Windows.Forms.TextBox();
        _filePathTextBox.Validating += new System.ComponentModel.CancelEventHandler(FilePathTextBox_Validating);
        SuspendLayout();
        // 
        // _browseButton
        // 
        _browseButton.BackColor = SystemColors.Control;
        _browseButton.Cursor = Cursors.Default;
        _browseButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
        _browseButton.ForeColor = SystemColors.ControlText;
        _browseButton.Location = new Point(516, 1);
        _browseButton.Name = "_BrowseButton";
        _browseButton.RightToLeft = RightToLeft.No;
        _browseButton.Size = new Size(29, 22);
        _browseButton.TabIndex = 0;
        _browseButton.Text = "...";
        _browseButton.TextAlign = ContentAlignment.TopCenter;
        _toolTip.SetToolTip(_browseButton, "Browses for a file");
        _browseButton.UseMnemonic = false;
        _browseButton.UseVisualStyleBackColor = true;
        // 
        // _filePathTextBox
        // 
        _filePathTextBox.AcceptsReturn = true;
        _filePathTextBox.BackColor = SystemColors.Window;
        _filePathTextBox.Cursor = Cursors.IBeam;
        _filePathTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
        _filePathTextBox.ForeColor = SystemColors.WindowText;
        _filePathTextBox.Location = new Point(0, 0);
        _filePathTextBox.MaxLength = 0;
        _filePathTextBox.Name = "_FilePathTextBox";
        _filePathTextBox.RightToLeft = RightToLeft.No;
        _filePathTextBox.Size = new Size(510, 25);
        _filePathTextBox.TabIndex = 1;
        // 
        // FileSelector
        // 
        BackColor = Color.Transparent;
        Controls.Add(_filePathTextBox);
        Controls.Add(_browseButton);
        Name = "FileSelector";
        Size = new Size(547, 25);
        Resize += new EventHandler(UserControl_Resize);
        ResumeLayout(false);
        PerformLayout();
    }
}
