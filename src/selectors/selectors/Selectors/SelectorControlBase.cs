using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> Selector control base. </summary>
/// <remarks>
/// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-03-06 </para>
/// </remarks>
[DefaultEvent( "ValueSelected" )]
public partial class SelectorControlBase : ModelViewBase
{
    #region " construction "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public SelectorControlBase()
    {
        // This call is required by the designer.
        this.InitializeComponent();
        this.SelectorButtonInternal = new Button();
        this.SelectorTextBoxInternal = new Control();
    }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="Control" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;

                this.SelectorButtonInternal?.Dispose();
                this.SelectorButtonInternal = null;
                this.SelectorTextBoxInternal?.Dispose();
                this.SelectorTextBoxInternal = null;
                this.RemoveDirtyChangedEventHandler( this.DirtyChanged );
                this.RemoveValueSelectedEventHandler( this.ValueSelected );
                this.RemoveValueSelectingEventHandler( this.ValueSelecting );
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " base controls "

    private Button? _selectorButtonInternal;

    /// <summary>   Gets or sets the selector button internal. </summary>
    /// <value> The selector button internal. </value>
    private Button? SelectorButtonInternal
    {
        [System.Runtime.CompilerServices.MethodImpl( System.Runtime.CompilerServices.MethodImplOptions.Synchronized )]
        get => this._selectorButtonInternal;

        [System.Runtime.CompilerServices.MethodImpl( System.Runtime.CompilerServices.MethodImplOptions.Synchronized )]
        set
        {
            if ( this._selectorButtonInternal is not null )
            {
                this._selectorButtonInternal.Click -= this.SelectorButton_Click;
            }

            this._selectorButtonInternal = value;
            if ( this._selectorButtonInternal is not null )
            {
                this._selectorButtonInternal.Click += this.SelectorButton_Click;
            }
        }
    }

    /// <summary> Gets or sets the selector button. </summary>
    /// <value> The selector button. </value>
    protected Button? SelectorButton
    {
        get => this.SelectorButtonInternal;
        set => this.SelectorButtonInternal = value;
    }

    private Control? _selectorTextBoxInternal;

    private Control? SelectorTextBoxInternal
    {
        [System.Runtime.CompilerServices.MethodImpl( System.Runtime.CompilerServices.MethodImplOptions.Synchronized )]
        get => this._selectorTextBoxInternal;

        [System.Runtime.CompilerServices.MethodImpl( System.Runtime.CompilerServices.MethodImplOptions.Synchronized )]
        set
        {
            if ( this._selectorTextBoxInternal is not null )
            {
                this._selectorTextBoxInternal.TextChanged -= this.SelectorTextBox_TextChanged;
            }

            this._selectorTextBoxInternal = value;
            if ( this._selectorTextBoxInternal is not null )
            {
                this._selectorTextBoxInternal.TextChanged += this.SelectorTextBox_TextChanged;
            }
        }
    }

    /// <summary> Gets or sets the selector text box. </summary>
    /// <value> The selector text box. </value>
    protected Control? SelectorTextBox
    {
        get => this.SelectorTextBoxInternal;
        set
        {
            this.SelectorTextBoxInternal = value;
            this._isDirty = !this.IsDirty;
        }
    }

    #endregion

    #region " exposed properties "

    private string? _watermark;

    /// <summary> The watermark text. </summary>
    /// <value> The water mark text with this control. </value>
    [DefaultValue( typeof( string ), "Watermark" )]
    [Description( "Watermark Text" )]
    [Category( "Appearance" )]
    public virtual string? Watermark
    {
        get => this._watermark;
        set
        {
            if ( !string.Equals( value, this.Watermark, StringComparison.Ordinal ) )
            {
                this._watermark = value;
                this.OnDirtyChangedThis();
            }
        }
    }

    /// <summary> Combo box text. </summary>
    /// <value> The text associated with this control. </value>
    [DefaultValue( typeof( string ), "" )]
    [Description( "Combo box text" )]
    [Category( "Appearance" )]
    public override string Text
    {
        get => this.SelectorTextBox?.Text ?? string.Empty;

#pragma warning disable CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).
        set
        {
            if ( this.SelectorTextBox is not null )
                this.SelectorTextBox.Text = value;
        }
#pragma warning restore CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).
    }

    /// <summary> Selector icon image. </summary>
    /// <value> The selector icon. </value>
    [Description( "Selector icon image" )]
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image? SelectorIcon
    {
        get => this.SelectorButton?.Image;
        set
        {
            if ( this.SelectorButton is not null )
                this.SelectorButton.Image = value;
        }
    }

    /// <summary> Gets or sets the read only property. </summary>
    /// <value> The read only. </value>
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Indicates whether the selector is read only." )]
    public virtual bool WriteProtected { get; set; }

    /// <summary> Indicates whether an empty value can be selected. </summary>
    /// <value> The can select empty value. </value>
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Indicates whether an empty value can be selected." )]
    public bool CanSelectEmptyValue { get; set; }

    #endregion

    #region " caption handlers "

    /// <summary> Gets the selected text. </summary>
    /// <value> The selected text. </value>
    [DefaultValue( "" )]
    [Description( "Selected text" )]
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public string? SelectedText { get; set; }

    /// <summary> Gets the previous selected text. </summary>
    /// <value> The previous selected text. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public string? PreviousSelectedText { get; private set; }

    /// <summary> Gets a value indicating whether a new value was selected. </summary>
    /// <value> <c>true</c> if a new value was selected; otherwise <c>false</c> </value>
    public bool IsSelectedNew => !string.Equals( this.PreviousSelectedText, this.SelectedText, StringComparison.Ordinal );

    /// <summary> Gets a value indicating whether the selector text is empty. </summary>
    /// <value> <c>true</c> if the selector test is empty; otherwise <c>false</c> </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public virtual bool IsEmpty => string.IsNullOrWhiteSpace( this.Text );

    /// <summary> Gets a value indicating whether this object has value. </summary>
    /// <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public virtual bool HasValue => !string.IsNullOrWhiteSpace( this.Text );

    private bool _isDirty;

    /// <summary> Gets a value indicating whether this object is dirty. </summary>
    /// <value> <c>true</c> if this object is dirty; otherwise <c>false</c> </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public bool IsDirty => !string.Equals( this.SelectedText, this.Text, StringComparison.Ordinal );

    /// <summary> Executes the dirty changed action. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void OnDirtyChangedThis()
    {
        if ( string.IsNullOrEmpty( this.Text ) && !string.IsNullOrWhiteSpace( this.Watermark ) )
        {
            this.Text = this.Watermark ?? string.Empty;
        }


        if ( !string.IsNullOrEmpty( this.Text ) && !string.IsNullOrWhiteSpace( this.Watermark ) && string.Equals( this.Text, this.Watermark, StringComparison.Ordinal ) )
        {
            if ( this.SelectorTextBox is not null )
                this.SelectorTextBox.ForeColor = SystemColors.InactiveCaption;
        }
        else if ( this.IsDirty != this._isDirty )
        {
            this._isDirty = this.IsDirty;
            this.DirtyChanged?.Invoke( this, EventArgs.Empty );
        }

        if ( !string.IsNullOrEmpty( this.Text ) && !string.IsNullOrWhiteSpace( this.Watermark ) && string.Equals( this.Text, this.Watermark, StringComparison.Ordinal ) )
        {
            if ( this.SelectorTextBox is not null )
                this.SelectorTextBox.ForeColor = SystemColors.InactiveCaption;
        }
        else if ( !this.Enabled )
        {
            if ( this.SelectorButton is not null )
                this.SelectorButton.BackColor = this.BackColor;
            if ( this.SelectorTextBox is not null )
                this.SelectorTextBox.ForeColor = SystemColors.InactiveCaption;
        }
        else if ( !this.IsDirty && this.SelectorTextBox is not null && this.SelectorTextBox.ForeColor != this.ForeColor )
        {
            if ( this.SelectorButton is not null )
                this.SelectorButton.BackColor = this.BackColor;
            this.SelectorTextBox.ForeColor = this.ForeColor;
        }
        else if ( this.IsDirty && this.SelectorTextBox is not null && this.SelectorTextBox.ForeColor != this._dirtyForeColor )
        {
            if ( this.SelectorButton is not null )
                this.SelectorButton.BackColor = this._dirtyBackColor;
            this.SelectorTextBox.ForeColor = this._dirtyForeColor;
        }

        this.OnContentChanged();
    }

    /// <summary> Executes the content changed action. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected virtual void OnContentChanged()
    {
        if ( this.SelectorButton is not null && this.SelectorButton.Enabled != this.CanSelectItem() )
        {
            this.SelectorButton.Enabled = this.CanSelectItem();
            if ( !this.SelectorButton.Enabled )
            {
                this.SelectorButton.BackColor = this.BackColor;
                if ( this.SelectorTextBox is not null )
                    this.SelectorTextBox.ForeColor = SystemColors.InactiveCaption;
            }
        }
    }

    /// <summary> Determine if we can select item. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> <c>true</c> if we can select item; otherwise <c>false</c> </returns>
    protected virtual bool CanSelectItem()
    {
        return this.CanSelectEmptyValue || this.HasValue;
    }

    /// <summary> Executes the dirty changed action. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public virtual void OnDirtyChanged()
    {
        this.OnDirtyChangedThis();
    }

    #endregion

    #region " colors "

    private Color _dirtyBackColor;

    /// <summary> Gets or sets the color of the read only back. </summary>
    /// <value> The color of the read only back. </value>
    [DefaultValue( typeof( Color ), "SystemColors.orange" )]
    [Description( "Back color when dirty" )]
    [Category( "Appearance" )]
    public Color DirtyBackColor
    {
        get
        {
            if ( this._dirtyBackColor.IsEmpty || this._dirtyBackColor == this.BackColor )
            {
                this._dirtyBackColor = Color.Orange;
            }

            return this._dirtyBackColor;
        }

        set => this._dirtyBackColor = value;
    }

    private Color _dirtyForeColor;

    /// <summary> Gets or sets the color of the read only foreground. </summary>
    /// <value> The color of the read only foreground. </value>
    [DefaultValue( typeof( Color ), "SystemColors.ActiveCaption" )]
    [Description( "Fore color when dirty" )]
    [Category( "Appearance" )]
    public Color DirtyForeColor
    {
        get
        {
            if ( this._dirtyForeColor.IsEmpty || this._dirtyForeColor == this.ForeColor )
            {
                this._dirtyForeColor = SystemColors.ActiveCaption;
            }

            return this._dirtyForeColor;
        }

        set => this._dirtyForeColor = value;
    }

    #endregion

    #region " select function "

    /// <summary> Select text. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void SelectTextThis()
    {
        this.PreviousSelectedText = this.SelectedText;
        this.SelectedText = this.SelectorTextBox?.Text;
        this.OnDirtyChanged();
    }

    /// <summary> Select text. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public virtual void SelectText()
    {
        this.SelectTextThis();
    }

    /// <summary> Select value. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void SelectValueThis()
    {
        CancelEventArgs e = new();
        this.OnValueSelecting( this, e );
        if ( e is not null && !e.Cancel )
        {
            this.SelectTextThis();
            this.OnValueSelected( this, EventArgs.Empty );
        }
    }

    /// <summary> Select value. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public virtual void SelectValue()
    {
        this.SelectValueThis();
    }

    private void SelectorTextBox_TextChanged( object? sender, EventArgs e )
    {
        this.OnDirtyChanged();
    }

    /// <summary> Select value. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    private void SelectValue( Control sender )
    {
        Cursor cursor = sender.Cursor;
        try
        {
            sender.Cursor = Cursors.WaitCursor;
            this.SelectValue();
        }
        finally
        {
            sender.Cursor = cursor;
        }
    }

    private void SelectorButton_Click( object? sender, EventArgs e )
    {
        if ( sender is not Control ctrl )
        {
            this.SelectValue();
        }
        else
        {
            this.SelectValue( ctrl );
        }
    }

    /// <summary> Event queue for all listeners interested in Value-Selected events. </summary>
    public event EventHandler<EventArgs>? DirtyChanged;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveDirtyChangedEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                DirtyChanged -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Event queue for all listeners interested in Value-Selecting events. </summary>
    public event EventHandler<CancelEventArgs>? ValueSelecting;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveValueSelectingEventHandler( EventHandler<CancelEventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                ValueSelecting -= ( EventHandler<CancelEventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Raises the Value-Selecting event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information to send to registered event handlers. </param>
    public virtual void OnValueSelecting( object? sender, CancelEventArgs e )
    {
        this.ValueSelecting?.Invoke( this, e );
    }

    /// <summary> Event queue for all listeners interested in Value-Selected events. </summary>
    public event EventHandler<EventArgs>? ValueSelected;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveValueSelectedEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                ValueSelected -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Raises the Value-Selected event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information to send to registered event handlers. </param>
    public virtual void OnValueSelected( object? sender, EventArgs e )
    {
        this.ValueSelected?.Invoke( this, e );
    }

    #endregion
}
