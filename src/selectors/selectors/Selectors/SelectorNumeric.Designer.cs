using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

public partial class SelectorNumeric : SelectorControlBase
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        Button = new Button();
        NumericUpDown = new NumericUpDown();
        NumericUpDown.ValueChanged += new EventHandler(NumericUpDown_ValueChanged);
        NumericUpDown.NumericTextChanged += new EventHandler<EventArgs>(NumericUpDown_NumericTextChanged);
        ((System.ComponentModel.ISupportInitialize)NumericUpDown).BeginInit();
        SuspendLayout();
        // 
        // Button
        // 
        Button.Dock = DockStyle.Right;
        Button.Image = Properties.Resources.dialog_ok_4;
        Button.Location = new Point(49, 0);
        Button.MaximumSize = new Size(25, 25);
        Button.Name = "Button";
        Button.Size = new Size(25, 25);
        Button.TabIndex = 1;
        Button.UseVisualStyleBackColor = true;
        // 
        // NumericUpDown
        // 
        NumericUpDown.Dock = DockStyle.Bottom;
        NumericUpDown.Location = new Point(0, 0);
        NumericUpDown.Name = "NumericUpDown";
        NumericUpDown.NullValue = new decimal(new int[] { 0, 0, 0, 0 });
        NumericUpDown.ReadOnlyBackColor = SystemColors.Control;
        NumericUpDown.ReadOnlyForeColor = SystemColors.WindowText;
        NumericUpDown.ReadWriteBackColor = SystemColors.Window;
        NumericUpDown.ReadWriteForeColor = SystemColors.ControlText;
        NumericUpDown.Size = new Size(49, 25);
        NumericUpDown.TabIndex = 0;
        NumericUpDown.Value = new decimal(new int[] { 0, 0, 0, 0 });
        // 
        // SelectorNumeric
        // 
        AutoScaleDimensions = new SizeF(7.0f, 17.0f);
        AutoScaleMode = AutoScaleMode.Font;
        Controls.Add(NumericUpDown);
        Controls.Add(Button);
        Name = "SelectorNumeric";
        Size = new Size(74, 25);
        ((System.ComponentModel.ISupportInitialize)NumericUpDown).EndInit();
        ResumeLayout(false);
    }

    /// <summary>   The button control. </summary>
    public Button Button { get; private set; }

    /// <summary>   Gets or sets the numeric up down. </summary>
    /// <value> The numeric up down. </value>
    public NumericUpDown NumericUpDown { get; private set; }
}
