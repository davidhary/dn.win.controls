using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;
public partial class SelectorComboBox
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        ComboBox = new ComboBox();
        Button = new Button();
        SuspendLayout();
        // 
        // ComboBox
        // 
        ComboBox.Dock = DockStyle.Top;
        ComboBox.FormattingEnabled = true;
        ComboBox.Location = new Point(0, 0);
        ComboBox.Name = "ComboBox";
        ComboBox.ReadOnlyBackColor = SystemColors.Control;
        ComboBox.ReadOnlyForeColor = SystemColors.WindowText;
        ComboBox.ReadWriteBackColor = SystemColors.Window;
        ComboBox.ReadWriteForeColor = SystemColors.ControlText;
        ComboBox.Size = new Size(66, 25);
        ComboBox.TabIndex = 0;
        ComboBox.DataSourceChanged += new EventHandler( ComboBox_DataSourceChanged );
        ComboBox.SelectedValueChanged += new EventHandler( ComboBox_SelectedValueChanged );
        // 
        // Button
        // 
        Button.Dock = DockStyle.Right;
        Button.Image = Properties.Resources.dialog_ok_4;
        Button.Location = new Point(66, 0);
        Button.MaximumSize = new Size(25, 25);
        Button.Name = "Button";
        Button.Size = new Size(25, 25);
        Button.TabIndex = 1;
        Button.UseVisualStyleBackColor = true;
        // 
        // SelectorComboBox
        // 
        AutoScaleDimensions = new SizeF(7.0f, 17.0f);
        AutoScaleMode = AutoScaleMode.Font;
        Controls.Add(ComboBox);
        Controls.Add(Button);
        Name = "SelectorComboBox";
        Size = new Size(91, 25);
        ResumeLayout(false);
    }

    /// <summary>   Gets or sets the combo box. </summary>
    /// <value> The combo box. </value>
    public ComboBox ComboBox { get; private set; }

    /// <summary>   The button control. </summary>
    public Button Button { get; private set; }
}
