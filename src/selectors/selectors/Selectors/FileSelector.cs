using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using cc.isr.WinControls.CompactExtensions;

namespace cc.isr.WinControls;

/// <summary> A simple text box and browse button file selector. </summary>
/// <remarks>
/// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License. </para><para>
/// David, 2007-03-14, 1.2.3405 </para><para>  
/// Add default event and description and move bitmap setting
/// from the designer file. </para>
/// </remarks>
[Description( "File Selector" )]
[DefaultEvent( "SelectedChanged" )]
public partial class FileSelector : UserControl, INotifyPropertyChanged
{
    #region " construction "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public FileSelector() : base()
    {
        // This call is required by the Windows Form Designer.
        this.InitializeComponent();
        this._textBoxContents = string.Empty;
        this._filePath = string.Empty;
        this._defaultExtension = ".TXT";
        this._dialogFilter = "Text files (*.txt; *.lst)|*.txt;*.lst|Batch files (*.bat)|*.bat|All Files (*.*)|*.*";
        this._dialogTitle = "SELECT A TEXT FILE";
        this._fileExtension = string.Empty;
        this._fileName = string.Empty;
        this.FileTitle = string.Empty;
    }

    /// <summary>
    /// Releases the unmanaged resources used by the cc.isr.WinControls.ModelViewBase and optionally
    /// releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.RemoveSelectedChangedEventHandler( this.SelectedChanged );
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <param name="propertyName"> Name of the property. </param>
    protected virtual void OnPropertyChanged( string? propertyName )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<TValue>( ref TValue backingField, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<TValue>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( ref TValue prop, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
        if ( EqualityComparer<TValue>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( TValue oldValue, TValue newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif

        if ( EqualityComparer<TValue>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion

    #region " file name properties "

    private string _defaultExtension;

    /// <summary> Gets or sets the default extension. </summary>
    /// <value> The default extension. </value>
    [Category( "Appearance" )]
    [Description( "Default extension" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( ".TXT" )]
    public string DefaultExtension
    {
        get => this._defaultExtension;
        set
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                value = string.Empty;
            }

            if ( !string.Equals( value, this.DefaultExtension, StringComparison.Ordinal ) )
            {
                this._defaultExtension = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    private string _dialogFilter;

    /// <summary>
    /// Gets or sets the filename filter string, which determines the choices of files that appear in
    /// the file type selection drop down list.
    /// </summary>
    /// <value> The dialog filter. </value>
    [Category( "Appearance" )]
    [Description( "Filename filter string determining the choices of files that appear in the file type selection drop down list." )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "Text files (*.txt; *.lst)|*.txt;*.lst|Batch files (*.bat)|*.bat|All Files (*.*)|*.*" )]
    public string DialogFilter
    {
        get => this._dialogFilter;
        set
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                value = string.Empty;
            }

            if ( !string.Equals( value, this.DialogFilter, StringComparison.Ordinal ) )
            {
                this._dialogFilter = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    private string _dialogTitle;

    /// <summary> Gets or sets the dialog title. </summary>
    /// <value> The dialog title. </value>
    [Category( "Appearance" )]
    [Description( "Dialog caption" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "SELECT A TEXT FILE" )]
    public string DialogTitle
    {
        get => this._dialogTitle;
        set
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                value = string.Empty;
            }

            if ( !string.Equals( value, this.DialogTitle, StringComparison.Ordinal ) )
            {
                this._dialogTitle = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    private string _fileExtension;

    /// <summary> Gets or sets the file extension. </summary>
    /// <value> The name of the extension. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public string FileExtension
    {
        get => this._fileExtension;

        protected set
        {
            this._fileExtension = value;
            this.FileTitle = ParseFileTitle( this.FileName, this.FileExtension );
        }
    }

    private string _fileName;

    /// <summary> Gets or sets the file name. </summary>
    /// <value> The name of the file. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public string FileName
    {
        get => this._fileName;
        set
        {
            this._fileName = value;
            this.FileTitle = ParseFileTitle( this.FileName, this.FileExtension );
        }
    }

    /// <summary> Parses file title. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="name">      The name. </param>
    /// <param name="extension"> The extension. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string ParseFileTitle( string name, string extension )
    {
        return string.IsNullOrWhiteSpace( name ) ? string.Empty : string.IsNullOrWhiteSpace( extension ) ? name : name[..name.LastIndexOf( extension, StringComparison.Ordinal )];
    }

    /// <summary> The text box contents. </summary>
    private string _textBoxContents;

    private string _filePath;

    /// <summary> Gets or sets a new file path. </summary>
    /// <value> The full pathname of the file. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public string FilePath
    {
        get => this._filePath;
        set
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                value = string.Empty;
            }

            value = value.Trim();
            this._filePath ??= string.Empty;

            if ( !(this._filePath.Equals( value, comparisonType: StringComparison.Ordinal ) && this._textBoxContents.Equals( value.Compact( this._filePathTextBox ), comparisonType: StringComparison.Ordinal )) )
            {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    this._textBoxContents = string.Empty;
                    this._filePathTextBox.Text = this._textBoxContents;
                    this._fileName = this._textBoxContents;
                    this._fileExtension = string.Empty;
                    this._toolTip.SetToolTip( this._filePathTextBox, "Enter or browse for a file name" );
                }
                else if ( System.IO.File.Exists( value ) )
                {
                    System.IO.FileInfo fileInfo = new( value );
                    value = fileInfo.FullName;
                    this._textBoxContents = value.Compact( this._filePathTextBox );
                    this._filePathTextBox.Text = this._textBoxContents;
                    this._toolTip.SetToolTip( this._filePathTextBox, value );
                    this._fileName = fileInfo.Name;
                    this._fileExtension = fileInfo.Extension;
                }
                else
                {
                    this._textBoxContents = "<file not found: " + value + ">";
                    this._filePathTextBox.Text = this._textBoxContents;
                    this._toolTip.SetToolTip( this._filePathTextBox, "Enter or browse for a file name" );
                    this._fileName = string.Empty;
                    this._fileExtension = string.Empty;
                }

                this.FileTitle = ParseFileTitle( this.FileName, this.FileExtension );
                this._filePath = value;
                this.OnSelectedChanged();
            }

            this._filePathTextBox.Refresh();
            this.NotifyPropertyChanged();
        }
    }

    /// <summary> Gets the file title. </summary>
    /// <value> The file title. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public string FileTitle { get; private set; }

    #endregion

    #region " refresh ; select file "

    /// <summary> Refreshes the control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public override void Refresh()
    {
        base.Refresh();
    }

    /// <summary> Selects a file. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> <c>true</c> if okay; otherwise, <c>false</c>. </returns>
    public bool SelectFile()
    {
        using OpenFileDialog dialog = new()
        {
            RestoreDirectory = true,
            Multiselect = false,
            CheckFileExists = true,
            CheckPathExists = true,
            ReadOnlyChecked = false,
            DefaultExt = this._defaultExtension,
            Title = this._dialogTitle,
            FilterIndex = 0,
            Filter = this._dialogFilter
        };

        if ( System.IO.File.Exists( this.FilePath ) )
            dialog.FileName = this.FilePath;

        System.IO.FileInfo fi = new( this.FilePath );

        dialog.InitialDirectory = fi.Directory is not null && fi.Directory.Exists ? fi.Directory.FullName : System.IO.Directory.GetCurrentDirectory();

        // Open the Open dialog
        if ( dialog.ShowDialog( this ) == DialogResult.OK )
        {
            // if file selected,
            this.FilePath = dialog.FileName;

            // return true for pass
            return true;
        }
        else
        {
            return false;
        }
    }

    #endregion

    #region " control event handlers "

    /// <summary>
    /// Event handler. Called by browseButton for click events. Browses for a file name.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="eventSender"> The event sender. </param>
    /// <param name="eventArgs">   Event information. </param>
    private void BrowseButton_Click( object eventSender, EventArgs eventArgs )
    {
        // Select a file
        _ = this.SelectFile();
    }

    /// <summary>
    /// Event handler. Called by filePathTextBox for validating events. Enters a file name.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Cancel event information. </param>
    private void FilePathTextBox_Validating( object? sender, CancelEventArgs e )
    {
        this.FilePath = this._filePathTextBox.Text;
    }

    /// <summary>
    /// Event handler. Called by UserControl for resize events. Resized the frame and sets the width
    /// of the text boxes.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="eventSender"> The event sender. </param>
    /// <param name="eventArgs">   Event information. </param>
    private void UserControl_Resize( object eventSender, EventArgs eventArgs )
    {
        this.Height = Math.Max( this._browseButton.Height, this._filePathTextBox.Height ) + 2;
        this._browseButton.Top = (this.Height - this._browseButton.Height) / 2;
        this._browseButton.Left = this.Width - this._browseButton.Width - 1;
        this._browseButton.Refresh();
        this._filePathTextBox.Top = (this.Height - this._filePathTextBox.Height) / 2;
        this._filePathTextBox.Width = this._browseButton.Left - 1;
        this._filePathTextBox.Refresh();
        this.Refresh();
    }

    #endregion

    #region " events "

    /// <summary> Occurs when a new file was selected. </summary>
    public event EventHandler<EventArgs>? SelectedChanged;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveSelectedChangedEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? ([]) : value.GetInvocationList() )
        {
            try
            {
                SelectedChanged -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Raises the selected changed event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void OnSelectedChanged()
    {
        this.SelectedChanged?.Invoke( this, EventArgs.Empty );
        Application.DoEvents();
    }

    #endregion
}
