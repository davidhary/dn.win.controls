using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> Defines a simple LED display with a caption. </summary>
/// <remarks>
/// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License. </para><para>
/// David, 2006-09-05, 2.0.2439 </para>
/// </remarks>
[Description( "Squared Led Control" )]
public partial class SquareLed : UserControl, INotifyPropertyChanged
{
    #region " construction and cleanup "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2021-03-16. </remarks>
    public SquareLed() : base() => this.InitializeComponent();

    /// <summary>
    /// Releases the unmanaged resources used by the cc.isr.WinControls.ModelViewBase and optionally
    /// releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.RemoveClickEvent( Click );
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <param name="propertyName"> Name of the property. </param>
    protected virtual void OnPropertyChanged( string? propertyName )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<TValue>( ref TValue backingField, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<TValue>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( ref TValue prop, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
        if ( EqualityComparer<TValue>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( TValue oldValue, TValue newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif

        if ( EqualityComparer<TValue>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion

    #region " appearance "

    /// <summary> The background color. </summary>
    private int _backgroundColor;

    /// <summary> Gets or sets the color of the background. </summary>
    /// <value> The color of the background. </value>
    [Browsable( true )]
    [Category( "Appearance" )]
    [Description( "Background Color." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int BackgroundColor
    {
        get => this._backgroundColor;
        set
        {
            if ( value != this.BackgroundColor )
            {
                this._backgroundColor = value;
                this.BackColor = ColorTranslator.FromOle( value );
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary> The caption. </summary>
    private string _caption = string.Empty;

    /// <summary> Gets or sets the caption. </summary>
    /// <value> The caption. </value>
    [Browsable( true )]
    [Category( "Appearance" )]
    [Description( "Caption." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string Caption
    {
        get => this._caption;
        set
        {
            if ( !string.Equals( value, this.Caption, StringComparison.Ordinal ) )
            {
                this._caption = value;
                this.CaptionLabel.Text = this.Caption;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary> The indicator color. </summary>
    private int _indicatorColor;

    /// <summary> Gets or sets the color of the indicator. </summary>
    /// <value> The color of the indicator. </value>
    [Browsable( true )]
    [Category( "Appearance" )]
    [Description( "Indicator color." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int IndicatorColor
    {
        get => this._indicatorColor;
        set
        {
            if ( value != this.IndicatorColor )
            {
                this._indicatorColor = value;
                this.IndicatorLabel.BackColor = ColorTranslator.FromOle( this._indicatorColor );
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary> The state. </summary>
    private IndicatorState _state;

    /// <summary> Gets or sets the is on. </summary>
    /// <value> The is on. </value>
    [Browsable( true )]
    [Category( "Appearance" )]
    [Description( "Is On." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool IsOn
    {
        get => this._state == IndicatorState.On;
        set
        {
            if ( value != this.IsOn )
            {
                this.State = value ? IndicatorState.On : IndicatorState.Off;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary> The no color. </summary>
    private int _noColor;

    /// <summary> Gets or sets the color of the no. </summary>
    /// <value> The color of the no. </value>
    [Browsable( true )]
    [Category( "Appearance" )]
    [Description( "No Color." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int NoColor
    {
        get => this._noColor;
        set
        {
            if ( value != this.NoColor )
            {
                this._noColor = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary> The off color. </summary>
    private int _offColor;

    /// <summary> Gets or sets the color of the off. </summary>
    /// <value> The color of the off. </value>
    [Browsable( true )]
    [Category( "Appearance" )]
    [Description( "Off Color." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int OffColor
    {
        get => this._offColor;
        set
        {
            if ( value != this.OffColor )
            {
                this._offColor = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary> The on color. </summary>
    private int _onColor;

    /// <summary> Gets or sets the color of the on. </summary>
    /// <value> The color of the on. </value>
    [Browsable( true )]
    [Category( "Appearance" )]
    [Description( "On Color." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int OnColor
    {
        get => this._onColor;
        set
        {
            if ( value != this.OnColor )
            {
                this._onColor = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary> Gets or sets the indicator color based on the state. </summary>
    /// <value> The state. </value>
    [Browsable( true )]
    [Category( "Appearance" )]
    [Description( "Current State." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public IndicatorState State
    {
        get => this._state;
        set
        {
            if ( value != this.State )
            {
                this._state = value;
                if ( this.Visible )
                {
                    switch ( true )
                    {
                        case object when value == IndicatorState.None:
                            {
                                this.IndicatorColor = this.NoColor;
                                break;
                            }

                        case object when value == IndicatorState.On:
                            {
                                this.IndicatorColor = this.OnColor;
                                break;
                            }

                        case object when value == IndicatorState.Off:
                            {
                                this.IndicatorColor = this.OffColor;
                                break;
                            }

                        default:
                            break;
                    }
                }

                this.NotifyPropertyChanged();
            }
        }
    }

    #endregion

    #region " events handlers "

    /// <summary>Occurs when the user clicks the indicator.
    /// </summary>
    public new event EventHandler<EventArgs>? Click;

    /// <summary> Removes click event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    private void RemoveClickEvent( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? [] : value.GetInvocationList() )
        {
            try
            {
                Click -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    /// <summary> Raises the click event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="eventSender"> The source of the event. </param>
    /// <param name="e">           The <see cref="EventArgs" /> instance containing the event
    /// data. </param>
    private void IndicatorLabel_Click( object eventSender, EventArgs e )
    {
        this.Click?.Invoke( this, e );
    }

    /// <summary> Position the controls. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="eventSender"> The source of the event. </param>
    /// <param name="eventArgs">   Event information. </param>
    private void SquareLed_Resize( object eventSender, EventArgs eventArgs )
    {
        this.CaptionLabel.SetBounds( (this.Width - this.CaptionLabel.Width) / 2, 0, this.Width, this.CaptionLabel.Height, BoundsSpecified.All );
        this.IndicatorLabel.SetBounds( (this.Width - this.IndicatorLabel.Width) / 2, this.Height - this.IndicatorLabel.Height, 0, 0, BoundsSpecified.X | BoundsSpecified.Y );
    }

    #endregion
}
/// <summary> Enumerates the indicator states. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum IndicatorState
{
    /// <summary>   An enum constant representing the none option. </summary>
    [Description( "None" )]
    None,

    /// <summary>   An enum constant representing the on option. </summary>
    [Description( "On" )]
    On,

    /// <summary>   An enum constant representing the off option. </summary>
    [Description( "Off" )]
    Off
}
