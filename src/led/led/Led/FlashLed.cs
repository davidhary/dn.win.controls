using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A flashing LED Control. </summary>
/// <remarks>
/// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License. </para><para>
/// David, 2007-09-04" by="HORIA TUDOSIE" revision="1.0.2803.x </para><para>
/// http://www.CodeProject.com/cs/MISCCTRL/FlashLED.asp. </para><para>
/// David, 2007-09-05, 1.0.2804 Added Flush Color property. </para>
/// </remarks>
[Description( "Flashing LED Control" )]
public partial class FlashLed : UserControl
{
    #region " construction and cleanup "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public FlashLed() : base()
    {
        // Initialize user components that might be affected by resize or paint actions

        // This call is required by the Windows Form Designer.
        this.InitializeComponent();

        // Add any initialization after the InitializeComponent() call
        this.SetStyle( ControlStyles.AllPaintingInWmPaint, true );
        this.SetStyle( ControlStyles.DoubleBuffer, true );
        this.SetStyle( ControlStyles.UserPaint, true );
        this.SetStyle( ControlStyles.ResizeRedraw, true );

        // These too lines are required to make the control transparent.
        this.SetStyle( ControlStyles.SupportsTransparentBackColor, true );
        this._flashColor = Color.Red;
        // _flash = False
        this._flashColorsArray = [Color.Red, Color.Empty, Color.Yellow, Color.Empty, Color.Blue];
        this._flashColors = "Red,,Yellow,,Blue";
        this._flashIntervalsArray = [500, 250, 500, 250, 500, 250];
        this._flashIntervals = "500,250,500,250,500,250";
        this._sparklePenWidth = 2f;
        this.Active = true;
        this.BackColor = Color.Transparent;
        this.OffColor = SystemColors.Control;
        this.OnColor = Color.Red;

        // Set the Width and Height to an odd number for the convenience of having a pixel in the center 
        // of the control. 17 is a good size for a perfect circle for a circular LED.
        // Width = 17
        // Height = 17
        try
        {
            this._tickTimer = new Timer();
        }
        catch
        {
            this._tickTimer?.Dispose();
            throw;
        }

        this._tickTimer.Enabled = false;
        this._tickTimer.Tick += this.TickTimerTick;
    }

    /// <summary>
    /// Releases the unmanaged resources used by the cc.isr.WinControls.ModelViewBase and optionally
    /// releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;

                if ( this._tickTimer is not null )
                {
                    this._tickTimer.Dispose();
                    this._tickTimer = null;
                }
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " properties "

    /// <summary> true to active. </summary>
    private bool _active;

    /// <summary>
    /// Turns on the <see cref="OnColor">on color</see> when True or the
    /// <see cref="OffColor">off color</see> when False.
    /// </summary>
    /// <value> The active. </value>
    [Category( "Behavior" )]
    [DefaultValue( true )]
    public bool Active
    {
        get => this._active;
        set
        {
            this._active = value;
            this.Invalidate();
        }
    }

    /// <summary> true to flash. </summary>
    private bool _flash;

    /// <summary> Gets or sets the Flash condition.  When True, the Led flashes. </summary>
    /// <value> The flash. </value>
    [Category( "Behavior" )]
    [DefaultValue( false )]
    public bool Flash
    {
        get => this._flash;
        set
        {
            this._flash = value; // AndAlso (Me._flashIntervalsArray.Length > 0)
            this._tickIndex = 0;
            this._tickTimer.Interval = this._flashIntervalsArray[this._tickIndex];
            this._tickTimer.Enabled = this._flash;
            this.Active = this._active;
        }
    }

    /// <summary> The flash color. </summary>
    private Color _flashColor = Color.Red;

    /// <summary> Gets or sets the Flash color. </summary>
    /// <value> The color of the flash. </value>
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color FlashColor
    {
        get => this._flashColor;
        set
        {
            this._flashColor = value;
            this.Invalidate();
        }
    }

    /// <summary>
    /// Holds the array of flash colors in case a set of colors is used.
    /// </summary>
    private Color[]? _flashColorsArray;

    /// <summary> List of colors of the flashes. </summary>
    private string _flashColors;

    /// <summary> Gets or sets the set of flash colors. </summary>
    /// <remarks>
    /// Accepts a delimited string that is converted to a color array for setting the flash color.
    /// Any reasonable delimiter will break the input string, and error items will default to
    /// Color.Empty for colors that will switch the LED Off.
    /// </remarks>
    /// <value> A list of colors of the flashes. </value>
    [Category( "Appearance" )]
    [DefaultValue( "Red,,Yellow,,Blue" )]
    public string FlashColors
    {
        get => this._flashColors;
        set
        {
            this._flashColors = string.IsNullOrWhiteSpace( value ) ? string.Empty : value;
            if ( string.IsNullOrWhiteSpace( this._flashColors ) )
            {
                this._flashColorsArray = null;
            }
            else
            {
                string[] fc = this._flashColors.Split( [',', '/', '|', ' ', Environment.NewLine.ToCharArray()[0], Environment.NewLine.ToCharArray()[1]] );
                this._flashColorsArray = new Color[fc.Length];
                for ( int i = 0, loopTo = fc.Length - 1; i <= loopTo; i++ )
                {
                    try
                    {
                        this._flashColorsArray[i] = string.IsNullOrEmpty( fc[i] ) ? Color.Empty : Color.FromName( fc[i] );
                    }
                    catch
                    {
                        this._flashColorsArray[i] = Color.Empty;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Holds the array of flash intervals.
    /// </summary>
    private int[] _flashIntervalsArray;

    /// <summary> The flash intervals. </summary>
    private string _flashIntervals;

    /// <summary> Gets or sets the set of flash intervals. </summary>
    /// <remarks>
    /// Accepts a delimited string that is converted to a interval array for setting the flash
    /// intervals. Any reasonable delimiter will break the input string, and error items will default
    /// to 25 ms for the interval.
    /// </remarks>
    /// <value> The flash intervals. </value>
    [Category( "Appearance" )]
    [DefaultValue( "500,250,500,250,500,250" )]
    public string FlashIntervals
    {
        get => this._flashIntervals;
        set
        {
            this._flashIntervals = string.IsNullOrWhiteSpace( value ) ? string.Empty : value;
            if ( string.IsNullOrWhiteSpace( this._flashIntervals ) )
            {
                this._flashIntervalsArray = [];
            }
            else
            {
                string[] fi = this._flashIntervals.Split( [',', '/', '|', ' ', Environment.NewLine.ToCharArray()[0], Environment.NewLine.ToCharArray()[1]] );
                this._flashIntervalsArray = new int[fi.Length];
                for ( int i = 0, loopTo = fi.Length - 1; i <= loopTo; i++ )
                {
                    try
                    {
                        this._flashIntervalsArray[i] = int.Parse( fi[i], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture );
                    }
                    catch
                    {
                        this._flashIntervalsArray[i] = 25;
                    }
                }
            }
        }
    }

    /// <summary> The off color. </summary>
    private Color _offColor;

    /// <summary> Gets or sets the OFF color. </summary>
    /// <value> The color of the off. </value>
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color OffColor
    {
        get => this._offColor;
        set
        {
            this._offColor = value;
            this.Invalidate();
        }
    }

    /// <summary> The on color. </summary>
    private Color _onColor;

    /// <summary> Gets or sets the ON color. </summary>
    /// <value> The color of the on. </value>
    [Category( "Appearance" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Color OnColor
    {
        get => this._onColor;
        set
        {
            this._onColor = value;
            this.Invalidate();
        }
    }

    #endregion

    #region " helper color functions "

    /// <summary>
    /// Blends the <para>firstColor</para> over the <para>secondColor</para> with a weighted blend of
    /// <para>firstWeight</para> and <para>secondWeight</para>.
    /// </summary>
    /// <remarks>
    /// The function works by splitting the two colors in R, G and B components, applying the ratio,
    /// and returning the recomposed color. This function blends the LED color in the margin of the
    /// bubble, and adds the sparkle that will give the bubble's volume. When the LED is On, the
    /// margin is enlightened with White, while - when Off is darkened with Black. The same for the
    /// sparkle, but with different ratios.
    /// </remarks>
    /// <param name="firstColor">   The first color. </param>
    /// <param name="secondColor">  The second color. </param>
    /// <param name="firstWeight">  The first weight. </param>
    /// <param name="secondWeight"> The second weight. </param>
    /// <returns> null if it fails, else a Color. </returns>
    public static Color FadeColor( Color firstColor, Color secondColor, int firstWeight, int secondWeight )
    {
        int r = ( int ) (((firstWeight * firstColor.R) + (secondWeight * secondColor.R)) / ( double ) (firstWeight + secondWeight));
        int g = ( int ) (((firstWeight * firstColor.G) + (secondWeight * secondColor.G)) / ( double ) (firstWeight + secondWeight));
        int b = ( int ) (((firstWeight * firstColor.B) + (secondWeight * secondColor.B)) / ( double ) (firstWeight + secondWeight));
        return Color.FromArgb( r, g, b );
    }

    /// <summary> Blends two colors with equal weights. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="firstColor">  The first color. </param>
    /// <param name="secondColor"> The second color. </param>
    /// <returns> A Color. </returns>
    public static Color FadeColor( Color firstColor, Color secondColor )
    {
        return FadeColor( firstColor, secondColor, 1, 1 );
    }

    /// <summary> Width of the sparkle pen. </summary>
    private readonly float _sparklePenWidth;

    /// <summary> Draws the LED. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="graphics"> The graphics. </param>
    /// <param name="color">    The color. </param>
    /// <param name="sparkle">  true to sparkle. </param>
    private void DrawLed( Graphics graphics, Color color, bool sparkle )
    {
        float halfPenWidth = this._sparklePenWidth / 2f;
        int imageWidth = this.Width - this.Margin.Left;
        int imageHeight = this.Height - this.Margin.Top;
        if ( sparkle )
        {
            using ( SolidBrush b = new( color ) )
            {
                graphics.FillEllipse( b, 1, 1, imageWidth, imageHeight );
            }

            using ( Pen p = new( FadeColor( color, Color.White, 1, 2 ), this._sparklePenWidth ) )
            {
                graphics.DrawArc( p, halfPenWidth + 2f, halfPenWidth + 2f, this.Width - 6 - halfPenWidth, this.Height - 6 - halfPenWidth, -90.0f, -90.0f );
            }

            using ( Pen p = new( FadeColor( color, Color.White ), 1f ) )
            {
                graphics.DrawEllipse( p, 1, 1, imageWidth, imageHeight );
            }
        }
        else
        {
            using ( SolidBrush b = new( color ) )
            {
                graphics.FillEllipse( b, 1, 1, imageWidth, imageHeight );
            }

            using ( Pen p = new( FadeColor( color, Color.Black, 2, 1 ), this._sparklePenWidth ) )
            {
                graphics.DrawArc( p, 3f, 3f, this.Width - 7, this.Height - 7, 0.0f, 90.0f );
            }

            using ( Pen p = new( FadeColor( color, Color.Black ), 1f ) )
            {
                graphics.DrawEllipse( p, 1, 1, imageWidth, imageHeight );
            }
        }
    }

    #endregion

    #region " event handlers "

    /// <summary>
    /// Event for adding external features.
    /// </summary>
    public new event PaintEventHandler? Paint;

    /// <summary> Draws the control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs e )
    {
        if ( e is null ) return;

        if ( Paint is not null )
        {
            Paint( this, e );
        }
        else
        {
            base.OnPaint( e );
            // e.Graphics.Clear(BackColor);
            if ( this.Enabled )
            {
                if ( this._flash )
                {
                    this.DrawLed( e.Graphics, this._flashColor, true );
                }
                // e.Graphics.FillEllipse(New SolidBrush(Me.FlashColor), 1, 1, Width - 3, Height - 3)
                // e.Graphics.DrawArc(New Pen(FadeColor(FlashColor, Color.White, 1, 2), 2), 3, 3, Width - 7, Height - 7, -90.0F, -90.0F)
                // e.Graphics.DrawEllipse(New Pen(FadeColor(FlashColor, Color.White), 1), 1, 1, Width - 3, Height - 3)
                // if non flash mode, set the on or off color.
                else if ( this.Active )
                {
                    this.DrawLed( e.Graphics, this._onColor, true );
                }
                // e.Graphics.FillEllipse(New SolidBrush(Me.OnColor), 1, 1, Width - 3, Height - 3)
                // e.Graphics.DrawArc(New Pen(FadeColor(OnColor, Color.White, 1, 2), 2), 3, 3, Width - 7, Height - 7, -90.0F, -90.0F)
                // e.Graphics.DrawEllipse(New Pen(FadeColor(OnColor, Color.White), 1), 1, 1, Width - 3, Height - 3)
                else
                {
                    this.DrawLed( e.Graphics, this._offColor, false );
                    // e.Graphics.FillEllipse(New SolidBrush(OffColor), 1, 1, Width - 3, Height - 3)
                    // e.Graphics.DrawArc(New Pen(FadeColor(OffColor, Color.Black, 2, 1), 2), 3, 3, Width - 7, Height - 7, 0.0F, 90.0F)
                    // e.Graphics.DrawEllipse(New Pen(FadeColor(OffColor, Color.Black), 1), 1, 1, Width - 3, Height - 3)
                }
            }
            else
            {
                using Pen p = new( SystemColors.ControlDark, 1f );
                e.Graphics.DrawEllipse( p, 1, 1, this.Width - 3, this.Height - 3 );
            }
        }
    }

    /// <summary> Zero-based index of the tick. </summary>
    private int _tickIndex;

    /// <summary> Handles the change of colors in flash mode. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void TickTimerTick( object? sender, EventArgs e )
    {
        this._tickIndex += 1;
        if ( this._tickIndex >= this._flashIntervalsArray.Length )
        {
            this._tickIndex = 0;
        }

        this._tickTimer.Interval = this._flashIntervalsArray[this._tickIndex];
        if ( this._flashColorsArray is null )
        {
            // if no color array defined or array color index out of bounds,
            // use the on/off colors to flash
            this.FlashColor = this._flashColor.Equals( this._onColor ) ? this._offColor : this._onColor;
        }
        else
        {
            this.FlashColor = this._flashColorsArray.Length <= this._tickIndex || this._flashColorsArray[this._tickIndex].Equals( Color.Empty )
                ? this._offColor
                : this._flashColorsArray[this._tickIndex];
        }
    }

    #endregion
}
