using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace cc.isr.WinControls;

public partial class SquareLed
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;
    private Label IndicatorLabel;
    private Label CaptionLabel;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        CaptionLabel = new Label();
        IndicatorLabel = new Label();
        IndicatorLabel.Click += new EventHandler(IndicatorLabel_Click);
        SuspendLayout();
        // 
        // captionLabel
        // 
        CaptionLabel.BackColor = Color.Transparent;
        CaptionLabel.Cursor = Cursors.Default;
        CaptionLabel.ForeColor = SystemColors.ControlText;
        CaptionLabel.Location = new Point(0, 0);
        CaptionLabel.Name = "captionLabel";
        CaptionLabel.RightToLeft = RightToLeft.No;
        CaptionLabel.Size = new Size(33, 13);
        CaptionLabel.TabIndex = 0;
        CaptionLabel.Text = "TITLE";
        CaptionLabel.TextAlign = ContentAlignment.MiddleCenter;
        // 
        // indicatorLabel
        // 
        IndicatorLabel.BackColor = Color.Green;
        IndicatorLabel.BorderStyle = BorderStyle.FixedSingle;
        IndicatorLabel.Cursor = Cursors.Default;
        IndicatorLabel.ForeColor = Color.Red;
        IndicatorLabel.Location = new Point(8, 16);
        IndicatorLabel.Name = "indicatorLabel";
        IndicatorLabel.RightToLeft = RightToLeft.No;
        IndicatorLabel.Size = new Size(13, 13);
        IndicatorLabel.TabIndex = 1;
        // 
        // SquareLed
        // 
        Controls.Add(IndicatorLabel);
        Controls.Add(CaptionLabel);
        Name = "SquareLed";
        Size = new Size(33, 33);
        Resize += new EventHandler(SquareLed_Resize);
        ResumeLayout(false);
    }
}
