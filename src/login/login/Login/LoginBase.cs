using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;

namespace cc.isr.WinControls;

/// <summary> Interfaces user log in/out functionality. </summary>
/// <remarks>
/// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2008-10-30, 1.00.3225 </para>
/// </remarks>
[System.Runtime.InteropServices.ComVisible( false )]
public abstract class LoginBase : IDisposable, INotifyPropertyChanged
{
    #region " construction and cleanup "

    /// <summary> Initializes a new instance of the <see cref="LoginBase" /> class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected LoginBase() : base()
    {
    }

    /// <summary> Calls <see cref="Dispose(bool)" /> to cleanup. </summary>
    /// <remarks>
    /// Do not make this method Overridable (virtual) because a derived class should not be able to
    /// override this method.
    /// </remarks>
    public void Dispose()
    {
        // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        // this disposes all child classes.
        this.Dispose( true );

        // Take this object off the finalization(Queue) and prevent finalization code 
        // from executing a second time.
        GC.SuppressFinalize( this );
    }

    /// <summary>
    /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
    /// class provided proper implementation.
    /// </summary>
    /// <value> <c>true</c> if disposed; otherwise, <c>false</c>. </value>
    protected bool IsDisposed { get; set; }

    /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    /// <remarks>
    /// Executes in two distinct scenarios as determined by its disposing parameter:<para>
    /// If True, the method has been called directly or indirectly by a user's code--managed and
    /// unmanaged resources can be disposed.</para><para>
    /// If False, the method has been called by the runtime from inside the finalizer and you should
    /// not reference other objects--only unmanaged resources can be disposed.</para>
    /// </remarks>
    /// <param name="disposing"> <c>true</c> if this method releases both managed and unmanaged
    /// resources;
    /// False if this method releases only unmanaged
    /// resources. </param>
    protected virtual void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.RemoveEventHandler( this.PropertyChanged );
            }
        }
        finally
        {
            // set the sentinel indicating that the class was disposed.
            this.IsDisposed = true;
        }
    }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary> Removes the event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    private void RemoveEventHandler( PropertyChangedEventHandler? value )
    {
        foreach ( Delegate d in value is null ? [] : value.GetInvocationList() )
        {
            try
            {
                PropertyChanged -= ( PropertyChangedEventHandler ) d;
            }
            catch ( Exception ex )
            {
                Trace.TraceError( $"exception occurred removing event handlers; {ex}" );
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    #endregion

    #region " validation methods "

    /// <summary>   Authenticates a user by user name and password. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userCredential">   The user credential. </param>
    /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    public abstract bool Authenticate( System.Net.NetworkCredential userCredential );

    /// <summary>   Authenticates a user by user name and password. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userCredential">   The user credential. </param>
    /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    ///                                 the name of enumeration flags. </param>
    /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    public virtual bool Authenticate( System.Net.NetworkCredential userCredential, ArrayList allowedUserRoles )
    {
        this.IsAuthenticated = this.TryFindUser( userCredential.UserName, allowedUserRoles ) && this.Authenticate( userCredential );
        return this.IsAuthenticated;
    }

    /// <summary> Tries to find a user in a group role. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="userName">         Specifies a user name. </param>
    /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    /// the name of enumeration flags. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    public abstract bool TryFindUser( string userName, ArrayList allowedUserRoles );

    /// <summary> Invalidates the last login user. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void Invalidate()
    {
        this.IsAuthenticated = false;
        this.Failed = false;
    }

    #endregion

    #region " validation outcomes "

    /// <summary> Name of the user. </summary>
    private string _userName = string.Empty;

    /// <summary> Gets or sets (protected) the name of the user. </summary>
    /// <value> The name of the user. </value>
    public string UserName
    {
        get => this._userName;

        protected set
        {
            this._userName = value;
            this.NotifyPropertyChanged();
        }
    }

    private ArrayList _userRoles = [];

    /// <summary> Gets or sets (protected) the roles of the user. </summary>
    /// <value> The Role of the user. </value>
    public ArrayList UserRoles
    {
        get => this._userRoles;

        protected set
        {
            this._userRoles = value;
            this.NotifyPropertyChanged( nameof( LoginBase.UserName ) );
        }
    }

    /// <summary> True if is authenticated, false if not. </summary>
    private bool _isAuthenticated;

    /// <summary>
    /// Gets or sets (protected) a value indicating whether the user is authenticated.
    /// </summary>
    /// <value> <c>true</c> if the user authenticated is valid; otherwise <c>false</c>. </value>
    public bool IsAuthenticated
    {
        get => this._isAuthenticated;

        protected set
        {
            this._isAuthenticated = value;
            this.NotifyPropertyChanged();
        }
    }

    /// <summary> The validation details. </summary>
    private string _validationDetails = string.Empty;

    /// <summary> Gets or sets (protected) the validation message. </summary>
    /// <value> A message describing the validation. </value>
    public string ValidationMessage
    {
        get => this._validationDetails;

        protected set
        {
            this._validationDetails = value;
            this.NotifyPropertyChanged();
        }
    }

    /// <summary> True if failed. </summary>
    private bool _failed;

    /// <summary> Gets or sets a value indicating whether the login failed. </summary>
    /// <value> <c>true</c> if failed; otherwise <c>false</c>. </value>
    public bool Failed
    {
        get => this._failed;

        protected set
        {
            this._failed = value;
            this.NotifyPropertyChanged();
        }
    }

    #endregion
}
