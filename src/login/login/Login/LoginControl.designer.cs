using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace cc.isr.WinControls;

public partial class LoginControl
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        _passwordTextBox = new System.Windows.Forms.TextBox();
        _passwordTextBoxLabel = new Label();
        _userNameTextBox = new System.Windows.Forms.TextBox();
        _userNameTextBoxLabel = new Label();
        _errorProvider = new ErrorProvider(components);
        _loginLinkLabel = new LinkLabel();
        _loginLinkLabel.LinkClicked += new LinkLabelLinkClickedEventHandler(LoginLinkLabel_LinkClicked);
        ((System.ComponentModel.ISupportInitialize)_errorProvider).BeginInit();
        SuspendLayout();
        // 
        // _passwordTextBox
        // 
        _passwordTextBox.Location = new Point(95, 31);
        _passwordTextBox.Margin = new Padding(3, 4, 3, 4);
        _passwordTextBox.Name = "_passwordTextBox";
        _passwordTextBox.PasswordChar = '*';
        _passwordTextBox.Size = new Size(81, 25);
        _passwordTextBox.TabIndex = 3;
        // 
        // _passwordTextBoxLabel
        // 
        _passwordTextBoxLabel.Location = new Point(5, 30);
        _passwordTextBoxLabel.Name = "_passwordTextBoxLabel";
        _passwordTextBoxLabel.Size = new Size(90, 26);
        _passwordTextBoxLabel.TabIndex = 2;
        _passwordTextBoxLabel.Text = "Password: ";
        _passwordTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
        // 
        // _userNameTextBox
        // 
        _userNameTextBox.Location = new Point(96, 4);
        _userNameTextBox.Margin = new Padding(3, 4, 3, 4);
        _userNameTextBox.Name = "_userNameTextBox";
        _userNameTextBox.Size = new Size(81, 25);
        _userNameTextBox.TabIndex = 1;
        _userNameTextBox.Text = "David";
        // 
        // _userNameTextBoxLabel
        // 
        _userNameTextBoxLabel.Location = new Point(6, 3);
        _userNameTextBoxLabel.Name = "_userNameTextBoxLabel";
        _userNameTextBoxLabel.Size = new Size(89, 26);
        _userNameTextBoxLabel.TabIndex = 0;
        _userNameTextBoxLabel.Text = "User Name: ";
        _userNameTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
        // 
        // _errorProvider
        // 
        _errorProvider.ContainerControl = this;
        // 
        // _loginLinkLabel
        // 
        _loginLinkLabel.Location = new Point(113, 60);
        _loginLinkLabel.Name = "_LoginLinkLabel";
        _loginLinkLabel.Size = new Size(59, 17);
        _loginLinkLabel.TabIndex = 4;
        _loginLinkLabel.TabStop = true;
        _loginLinkLabel.Text = "Log In";
        _loginLinkLabel.TextAlign = ContentAlignment.MiddleRight;
        // 
        // LoginControl
        // 
        AutoScaleDimensions = new SizeF(7.0f, 17.0f);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.Transparent;
        BorderStyle = BorderStyle.Fixed3D;
        Controls.Add(_loginLinkLabel);
        Controls.Add(_passwordTextBox);
        Controls.Add(_passwordTextBoxLabel);
        Controls.Add(_userNameTextBox);
        Controls.Add(_userNameTextBoxLabel);
        Margin = new Padding(3, 4, 3, 4);
        Name = "LoginControl";
        Size = new Size(179, 81);
        ((System.ComponentModel.ISupportInitialize)_errorProvider).EndInit();
        Load += new EventHandler(UserLoginControl_Load);
        VisibleChanged += new EventHandler(LoginControl_VisibleChanged);
        ResumeLayout(false);
        PerformLayout();
    }

    private System.Windows.Forms.TextBox _passwordTextBox;
    private Label _passwordTextBoxLabel;
    private System.Windows.Forms.TextBox _userNameTextBox;
    private Label _userNameTextBoxLabel;
    private ErrorProvider _errorProvider;
    private LinkLabel _loginLinkLabel;
}
