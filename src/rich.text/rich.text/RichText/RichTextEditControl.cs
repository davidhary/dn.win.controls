using System;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;
/// <summary>
/// Control for viewing and printing a <see cref="RichTextBox">rich text box</see>.
/// </summary>
/// <remarks>
/// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-01-08 </para>
/// </remarks>
public partial class RichTextEditControl : UserControl
{
    #region " construction and cleanup  "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public RichTextEditControl() : base()
    {
        // This call is required by the Windows Form Designer.
        this.InitializeComponent();

        // Add any initialization after the InitializeComponent() call
        this._formatFontSegoeUIMenu.Text = SystemFonts.MessageBoxFont!.Name;
    }

    /// <summary>
    /// Disposes of the resources (other than memory) used by the
    /// <see cref="Form" />.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " print settings "

    /// <summary> Gets a reference to the print document. </summary>
    /// <value> The print document. </value>
    public System.Drawing.Printing.PrintDocument PrintDocument => this._printDocument;

    /// <summary> Gets the reference to the rich text box. </summary>
    /// <value> The rich text box. </value>
    public RichTextBox RichTextBox => this._richTextBox;

    #endregion

    #region " print dialog "

    /// <summary> The first character on page. </summary>
    private int _firstCharOnPage;

    /// <summary> Print document begin print. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Print event information. </param>
    private void PrintDocument_BeginPrint( object? sender, System.Drawing.Printing.PrintEventArgs e )
    {
        // Start at the beginning of the text
        this._firstCharOnPage = 0;
    }

    /// <summary> Print document print page. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Print page event information. </param>
    private void PrintDocument_PrintPage( object? sender, System.Drawing.Printing.PrintPageEventArgs e )
    {
        if ( e is null || e.Graphics is null ) return;

        // To print the boundaries of the current page margins
        // uncomment the next line:
        e.Graphics.DrawRectangle( Pens.Blue, e.MarginBounds );

        // make the RichTextBoxEx calculate and render as much text as will
        // fit on the page and remember the last character printed for the
        // beginning of the next page
        this._firstCharOnPage = this.RichTextBox.FormatRange( false, e, this._firstCharOnPage, this.RichTextBox.TextLength );

        // check if there are more pages to print
        e.HasMorePages = this._firstCharOnPage < this.RichTextBox.TextLength;
    }

    /// <summary> Print document end print. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Print event information. </param>
    private void PrintDocument_EndPrint( object? sender, System.Drawing.Printing.PrintEventArgs e )
    {
        // Clean up cached information
        _ = this.RichTextBox.FormatRangeDone();
    }

    #endregion

    #region " file menu "

    /// <summary> File page setup menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FilePageSetupMenu_Click( object? sender, EventArgs e )
    {
        _ = this._pageSetupDialog.ShowDialog();
    }

    /// <summary> File print preview menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FilePrintPreviewMenu_Click( object? sender, EventArgs e )
    {
        if ( this._printPreviewDialog.ShowDialog() == DialogResult.OK )
        {
            this._printDocument.Print();
        }
    }

    /// <summary> File print menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FilePrintMenu_Click( object? sender, EventArgs e )
    {
        this._printDocument.Print();
    }

    /// <summary>   Event queue for all listeners interested in ExitRequested events. </summary>
    public event EventHandler? ExitRequested;

    /// <summary> File Exit menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FileExitMenu_Click( object? sender, EventArgs e )
    {
        this.ExitRequested?.Invoke( this, EventArgs.Empty );
    }

    #endregion

    #region " format menu "

    /// <summary> Format bold menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FormatBoldMenu_Click( object? sender, EventArgs e )
    {
        _ = this.RichTextBox.SetSelectionBold( true );
    }

    /// <summary> Format italic menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FormatItalicMenu_Click( object? sender, EventArgs e )
    {
        _ = this.RichTextBox.SetSelectionItalic( true );
    }

    /// <summary> Format underlined menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FormatUnderlinedMenu_Click( object? sender, EventArgs e )
    {
        _ = this.RichTextBox.SetSelectionUnderlined( true );
    }

    /// <summary> Format font size 8 menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FormatFontSize8Menu_Click( object? sender, EventArgs e )
    {
        _ = this.RichTextBox.SetSelectionSize( 8 );
    }

    /// <summary> Format font size 10 menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FormatFontSize10Menu_Click( object? sender, EventArgs e )
    {
        _ = this.RichTextBox.SetSelectionSize( 10 );
    }

    /// <summary> Font size 12 menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FontSize12Menu_Click( object? sender, EventArgs e )
    {
        _ = this.RichTextBox.SetSelectionSize( 12 );
    }

    /// <summary> Format font size 18 menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FormatFontSize18Menu_Click( object? sender, EventArgs e )
    {
        _ = this.RichTextBox.SetSelectionSize( 18 );
    }

    /// <summary> Format font size 24 menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FormatFontSize24Menu_Click( object? sender, EventArgs e )
    {
        _ = this.RichTextBox.SetSelectionSize( 24 );
    }

    /// <summary> Format font arial menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FormatFontArialMenu_Click( object? sender, EventArgs e )
    {
        _ = this.RichTextBox.SetSelectionFont( "Arial" );
    }

    /// <summary> Format font courier menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FormatFontCourierMenu_Click( object? sender, EventArgs e )
    {
        _ = this.RichTextBox.SetSelectionFont( "Courier New" );
    }

    /// <summary> Format font lucida menu item click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FormatFontLucidaMenuItem_Click( object? sender, EventArgs e )
    {
        _ = this.RichTextBox.SetSelectionFont( "Lucida Console" );
    }

    /// <summary> Format font Segoe menu item click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FormatFontSegoeMenuItem_Click( object? sender, EventArgs e )
    {
        _ = this.RichTextBox.SetSelectionFont( SystemFonts.MessageBoxFont!.Name );
    }

    /// <summary> Format font times menu click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FormatFontTimesMenu_Click( object? sender, EventArgs e )
    {
        _ = this.RichTextBox.SetSelectionFont( "Times New Roman" );
    }

    #endregion
}
