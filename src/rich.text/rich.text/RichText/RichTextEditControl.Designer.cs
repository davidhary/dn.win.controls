using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
namespace cc.isr.WinControls;

public partial class RichTextEditControl
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        var resources = new System.ComponentModel.ComponentResourceManager(typeof(RichTextEditControl));
        _richTextBox = new WinControls.RichTextBox();
        _mainMenu = new System.Windows.Forms.MenuStrip();
        _fileMenu = new System.Windows.Forms.ToolStripMenuItem();
        _filePageSetupMenu = new System.Windows.Forms.ToolStripMenuItem();
        _filePrintPreviewMenu = new System.Windows.Forms.ToolStripMenuItem();
        _filePrintMenu = new System.Windows.Forms.ToolStripMenuItem();
        _fileSeparatorMenuItem = new System.Windows.Forms.ToolStripSeparator();
        _fileExitMenu = new System.Windows.Forms.ToolStripMenuItem();
        _formatMenu = new System.Windows.Forms.ToolStripMenuItem();
        _formatFontStyleMenu = new System.Windows.Forms.ToolStripMenuItem();
        _formatBoldMenu = new System.Windows.Forms.ToolStripMenuItem();
        _formatItalicMenu = new System.Windows.Forms.ToolStripMenuItem();
        _formatUnderlinedMenu = new System.Windows.Forms.ToolStripMenuItem();
        _formatFontMenu = new System.Windows.Forms.ToolStripMenuItem();
        _formatFontArialMenu = new System.Windows.Forms.ToolStripMenuItem();
        _formatFontCourierMenu = new System.Windows.Forms.ToolStripMenuItem();
        _formatFontLucidaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        _formatFontSegoeUIMenu = new System.Windows.Forms.ToolStripMenuItem();
        _formatFontTimesMenu = new System.Windows.Forms.ToolStripMenuItem();
        _formatFontSizeMenu = new System.Windows.Forms.ToolStripMenuItem();
        _formatFontSize8Menu = new System.Windows.Forms.ToolStripMenuItem();
        _formatFontSize10Menu = new System.Windows.Forms.ToolStripMenuItem();
        _formatFontSize12Menu = new System.Windows.Forms.ToolStripMenuItem();
        _formatFontSize18Menu = new System.Windows.Forms.ToolStripMenuItem();
        _formatFontSize24Menu = new System.Windows.Forms.ToolStripMenuItem();
        _printDocument = new System.Drawing.Printing.PrintDocument();
        _pageSetupDialog = new System.Windows.Forms.PageSetupDialog();
        _printPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
        _printDialog = new System.Windows.Forms.PrintDialog();
        _mainMenu.SuspendLayout();
        SuspendLayout();
        // 
        // _richTextBox
        // 
        _richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
        _richTextBox.Location = new Point(0, 24);
        _richTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
        _richTextBox.Name = "_RichTextBox";
        _richTextBox.Size = new Size(560, 298);
        _richTextBox.TabIndex = 0;
        _richTextBox.Text = string.Empty;
        // 
        // _mainMenu
        // 
        _mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _fileMenu, _formatMenu });
        _mainMenu.Location = new Point(0, 0);
        _mainMenu.Name = "_MainMenu";
        _mainMenu.Size = new Size(560, 24);
        _mainMenu.TabIndex = 0;
        _mainMenu.Text = "MenuStrip1";
        // 
        // _fileMenu
        // 
        _fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _filePageSetupMenu, _filePrintPreviewMenu, _filePrintMenu, _fileSeparatorMenuItem, _fileExitMenu });
        _fileMenu.Name = "_FileMenu";
        _fileMenu.Size = new Size(37, 20);
        _fileMenu.Text = "&File";
        // 
        // _filePageSetupMenu
        // 
        _filePageSetupMenu.Name = "_FilePageSetupMenu";
        _filePageSetupMenu.Size = new Size(180, 22);
        _filePageSetupMenu.Text = "Page &Setup...";
        _filePageSetupMenu.Click += new EventHandler( FilePageSetupMenu_Click );
        // 
        // _filePrintPreviewMenu
        // 
        _filePrintPreviewMenu.Name = "_FilePrintPreviewMenu";
        _filePrintPreviewMenu.Size = new Size(180, 22);
        _filePrintPreviewMenu.Text = "Print Pre&view...";
        _filePrintPreviewMenu.Click += new EventHandler( FilePrintPreviewMenu_Click );
        // 
        // _filePrintMenu
        // 
        _filePrintMenu.Name = "_FilePrintMenu";
        _filePrintMenu.Size = new Size(180, 22);
        _filePrintMenu.Text = "&Print...";
        _filePrintMenu.Click += new EventHandler( FilePrintMenu_Click );
        // 
        // _fileSeparatorMenuItem
        // 
        _fileSeparatorMenuItem.Name = "_FileSeparatorMenuItem";
        _fileSeparatorMenuItem.Size = new Size(177, 6);
        // 
        // _fileExitMenu
        // 
        _fileExitMenu.Name = "_FileExitMenu";
        _fileExitMenu.Size = new Size(180, 22);
        _fileExitMenu.Text = "E&xit";
        _fileExitMenu.Click += new EventHandler( this.FileExitMenu_Click );
        // 
        // _formatMenu
        // 
        _formatMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _formatFontStyleMenu, _formatFontMenu, _formatFontSizeMenu });
        _formatMenu.Name = "_FormatMenu";
        _formatMenu.Size = new Size(57, 20);
        _formatMenu.Text = "Format".Insert(1,  "&");
        // 
        // _formatFontStyleMenu
        // 
        _formatFontStyleMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _formatBoldMenu, _formatItalicMenu, _formatUnderlinedMenu });
        _formatFontStyleMenu.Name = "_FormatFontStyleMenu";
        _formatFontStyleMenu.Size = new Size(180, 22);
        _formatFontStyleMenu.Text = "Font St&yle";
        // 
        // _formatBoldMenu
        // 
        _formatBoldMenu.Name = "_FormatBoldMenu";
        _formatBoldMenu.Size = new Size(132, 22);
        _formatBoldMenu.Text = "&Bold";
        _formatBoldMenu.Click += new EventHandler( FormatBoldMenu_Click );
        // 
        // _formatItalicMenu
        // 
        _formatItalicMenu.Name = "_FormatItalicMenu";
        _formatItalicMenu.Size = new Size(132, 22);
        _formatItalicMenu.Text = "&Italic";
        _formatItalicMenu.Click += new EventHandler( FormatItalicMenu_Click );
        // 
        // _formatUnderlinedMenu
        // 
        _formatUnderlinedMenu.Name = "_FormatUnderlinedMenu";
        _formatUnderlinedMenu.Size = new Size(132, 22);
        _formatUnderlinedMenu.Text = "&Underlined";
        _formatUnderlinedMenu.Click += new EventHandler( FormatUnderlinedMenu_Click );
        // 
        // _formatFontMenu
        // 
        _formatFontMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _formatFontArialMenu, _formatFontCourierMenu, _formatFontLucidaMenuItem, _formatFontSegoeUIMenu, _formatFontTimesMenu });
        _formatFontMenu.Name = "_FormatFontMenu";
        _formatFontMenu.Size = new Size(180, 22);
        _formatFontMenu.Text = "&Font";
        // 
        // _formatFontArialMenu
        // 
        _formatFontArialMenu.Name = "_FormatFontArialMenu";
        _formatFontArialMenu.Size = new Size(174, 22);
        _formatFontArialMenu.Text = "Arial";
        _formatFontArialMenu.Click += new EventHandler( FormatFontArialMenu_Click );
        // 
        // _formatFontCourierMenu
        // 
        _formatFontCourierMenu.Name = "_FormatFontCourierMenu";
        _formatFontCourierMenu.Size = new Size(174, 22);
        _formatFontCourierMenu.Text = "Courier";
        _formatFontCourierMenu.Click += new EventHandler( FormatFontCourierMenu_Click );
        // 
        // _formatFontLucidaMenuItem
        // 
        _formatFontLucidaMenuItem.Name = "_FormatFontLucidaMenuItem";
        _formatFontLucidaMenuItem.Size = new Size(174, 22);
        _formatFontLucidaMenuItem.Text = "Lucida Console";
        _formatFontLucidaMenuItem.Click += new EventHandler( FormatFontLucidaMenuItem_Click );
        // 
        // _formatFontSegoeUIMenu
        // 
        _formatFontSegoeUIMenu.Name = "_FormatFontSegoeUIMenu";
        _formatFontSegoeUIMenu.Size = new Size(174, 22);
        _formatFontSegoeUIMenu.Text = "Segoe UI";
        _formatFontSegoeUIMenu.Click += new EventHandler( FormatFontSegoeMenuItem_Click );
        // 
        // _formatFontTimesMenu
        // 
        _formatFontTimesMenu.Name = "_FormatFontTimesMenu";
        _formatFontTimesMenu.Size = new Size(174, 22);
        _formatFontTimesMenu.Text = "Times New Roman";
        _formatFontTimesMenu.Click += new EventHandler( FormatFontTimesMenu_Click );
        // 
        // _formatFontSizeMenu
        // 
        _formatFontSizeMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _formatFontSize8Menu, _formatFontSize10Menu, _formatFontSize12Menu, _formatFontSize18Menu, _formatFontSize24Menu });
        _formatFontSizeMenu.Name = "_FormatFontSizeMenu";
        _formatFontSizeMenu.Size = new Size(180, 22);
        _formatFontSizeMenu.Text = "Font &Size";
        // 
        // _formatFontSize8Menu
        // 
        _formatFontSize8Menu.Name = "_FormatFontSize8Menu";
        _formatFontSize8Menu.Size = new Size(86, 22);
        _formatFontSize8Menu.Text = "8";
        _formatFontSize8Menu.Click += new EventHandler( FormatFontSize8Menu_Click );
        // 
        // _formatFontSize10Menu
        // 
        _formatFontSize10Menu.Name = "_FormatFontSize10Menu";
        _formatFontSize10Menu.Size = new Size(86, 22);
        _formatFontSize10Menu.Text = "10";
        _formatFontSize10Menu.Click += new EventHandler( FormatFontSize10Menu_Click );
        // 
        // _formatFontSize12Menu
        // 
        _formatFontSize12Menu.Name = "_FormatFontSize12Menu";
        _formatFontSize12Menu.Size = new Size(86, 22);
        _formatFontSize12Menu.Text = "12";
        _formatFontSize12Menu.Click += new EventHandler( FontSize12Menu_Click );
        // 
        // _formatFontSize18Menu
        // 
        _formatFontSize18Menu.Name = "_FormatFontSize18Menu";
        _formatFontSize18Menu.Size = new Size(86, 22);
        _formatFontSize18Menu.Text = "18";
        _formatFontSize18Menu.Click += new EventHandler( FormatFontSize18Menu_Click );
        // 
        // _formatFontSize24Menu
        // 
        _formatFontSize24Menu.Name = "_FormatFontSize24Menu";
        _formatFontSize24Menu.Size = new Size(86, 22);
        _formatFontSize24Menu.Text = "24";
        _formatFontSize24Menu.Click += new EventHandler( FormatFontSize24Menu_Click );
        // 
        // _printDocument
        // 
        _printDocument.BeginPrint += new System.Drawing.Printing.PrintEventHandler( PrintDocument_BeginPrint );
        _printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler( PrintDocument_PrintPage );
        _printDocument.EndPrint += new System.Drawing.Printing.PrintEventHandler( PrintDocument_EndPrint );
        // 
        // _pageSetupDialog
        // 
        _pageSetupDialog.Document = _printDocument;
        // 
        // _printPreviewDialog
        // 
        _printPreviewDialog.AutoScrollMargin = new Size(0, 0);
        _printPreviewDialog.AutoScrollMinSize = new Size(0, 0);
        _printPreviewDialog.ClientSize = new Size(400, 300);
        _printPreviewDialog.Document = _printDocument;
        _printPreviewDialog.Enabled = true;
        _printPreviewDialog.Icon = (Icon)resources.GetObject("_PrintPreviewDialog.Icon");
        _printPreviewDialog.Name = "_PrintPreviewDialog";
        _printPreviewDialog.Visible = false;
        // 
        // _printDialog
        // 
        _printDialog.Document = _printDocument;
        // 
        // RichTextEditControl
        // 
        AutoScaleDimensions = new SizeF(7.0f, 17.0f);
        AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        Controls.Add(_richTextBox);
        Controls.Add(_mainMenu);
        Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
        Name = "RichTextEditControl";
        Size = new Size(560, 322);
        _mainMenu.ResumeLayout(false);
        _mainMenu.PerformLayout();
        ResumeLayout(false);
        PerformLayout();
    }

    private RichTextBox _richTextBox;
    private System.Drawing.Printing.PrintDocument _printDocument;
    private System.Windows.Forms.PageSetupDialog _pageSetupDialog;
    private System.Windows.Forms.PrintPreviewDialog _printPreviewDialog;
    private System.Windows.Forms.PrintDialog _printDialog;
    private System.Windows.Forms.MenuStrip _mainMenu;
    private System.Windows.Forms.ToolStripMenuItem _fileMenu;
    private System.Windows.Forms.ToolStripMenuItem _filePageSetupMenu;
    private System.Windows.Forms.ToolStripMenuItem _filePrintPreviewMenu;
    private System.Windows.Forms.ToolStripMenuItem _filePrintMenu;
    private System.Windows.Forms.ToolStripMenuItem _fileExitMenu;
    private System.Windows.Forms.ToolStripSeparator _fileSeparatorMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _formatMenu;
    private System.Windows.Forms.ToolStripMenuItem _formatFontStyleMenu;
    private System.Windows.Forms.ToolStripMenuItem _formatBoldMenu;
    private System.Windows.Forms.ToolStripMenuItem _formatItalicMenu;
    private System.Windows.Forms.ToolStripMenuItem _formatUnderlinedMenu;
    private System.Windows.Forms.ToolStripMenuItem _formatFontSizeMenu;
    private System.Windows.Forms.ToolStripMenuItem _formatFontSize8Menu;
    private System.Windows.Forms.ToolStripMenuItem _formatFontSize10Menu;
    private System.Windows.Forms.ToolStripMenuItem _formatFontSize12Menu;
    private System.Windows.Forms.ToolStripMenuItem _formatFontSize18Menu;
    private System.Windows.Forms.ToolStripMenuItem _formatFontSize24Menu;
    private System.Windows.Forms.ToolStripMenuItem _formatFontMenu;
    private System.Windows.Forms.ToolStripMenuItem _formatFontArialMenu;
    private System.Windows.Forms.ToolStripMenuItem _formatFontCourierMenu;
    private System.Windows.Forms.ToolStripMenuItem _formatFontLucidaMenuItem;
    private System.Windows.Forms.ToolStripMenuItem _formatFontSegoeUIMenu;
    private System.Windows.Forms.ToolStripMenuItem _formatFontTimesMenu;
}
