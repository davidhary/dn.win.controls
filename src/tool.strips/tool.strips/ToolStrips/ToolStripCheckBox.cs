using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace cc.isr.WinControls;

/// <summary> Tool strip check box. </summary>
/// <remarks>
/// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-01-23 </para>
/// </remarks>
[ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
public class ToolStripCheckBox : ToolStripControlHost
#if WINDOWS7_0_OR_GREATER
#else
    , IBindableComponent
#endif
{
    #region " construction and cleanup "

    /// <summary> Default constructor. </summary>
    /// <remarks> Call the base constructor passing in a CheckBox instance. </remarks>
    public ToolStripCheckBox() : base( new CheckBox() )
    {
    }

    #endregion

    #region " bindable "

#if WINDOWS7_0_OR_GREATER
#else

    private BindingContext? _context = null;

    /// <summary>
    /// Gets or sets the collection of currency managers for the
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The collection of <see cref="BindingManagerBase" /> objects for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [Browsable( false )]
    public BindingContext? BindingContext
    {
        get
        {
            this._context ??= new BindingContext();

            return this._context;
        }

        set => this._context = value;
    }

    private ControlBindingsCollection? _bindings;

    /// <summary>
    /// Gets the collection of data-binding objects for this
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The <see cref="ControlBindingsCollection" /> for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
    public ControlBindingsCollection? DataBindings
    {
        get
        {
            this._bindings ??= new ControlBindingsCollection( this );

            return this._bindings;
        }
    }

#endif

    #endregion

    #region " check box "

    /// <summary>   Gets the check box. </summary>
    /// <value> The check box. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
    public CheckBox CheckBox => ( CheckBox ) this.Control;

    /// <summary> Gets or sets the value. </summary>
    /// <value> The value. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool Checked
    {
        get => this.CheckBox.Checked;
        set => this.CheckBox.Checked = value;
    }

    #endregion
}
