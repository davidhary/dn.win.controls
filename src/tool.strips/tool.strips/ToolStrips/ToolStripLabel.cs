#if WINDOWS7_0_OR_GREATER
#else
using System.Windows.Forms;
using System.ComponentModel;
#endif
using System.Windows.Forms.Design;

namespace cc.isr.WinControls;

/// <summary> A tool strip Label. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-03-22 </para>
/// </remarks>
[ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
public class ToolStripLabel : System.Windows.Forms.ToolStripLabel
#if WINDOWS7_0_OR_GREATER
#else
    , IBindableComponent
#endif
{
    #region " bindable "

#if WINDOWS7_0_OR_GREATER
#else

    private BindingContext? _context = null;

    /// <summary>
    /// Gets or sets the collection of currency managers for the
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The collection of <see cref="BindingManagerBase" /> objects for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [Browsable( false )]
    public BindingContext? BindingContext
    {
        get
        {
            this._context ??= new BindingContext();

            return this._context;
        }

        set => this._context = value;
    }

    private ControlBindingsCollection? _bindings;

    /// <summary>
    /// Gets the collection of data-binding objects for this
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The <see cref="ControlBindingsCollection" /> for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
    public ControlBindingsCollection? DataBindings
    {
        get
        {
            this._bindings ??= new ControlBindingsCollection( this );

            return this._bindings;
        }
    }

#endif

    #endregion
}
