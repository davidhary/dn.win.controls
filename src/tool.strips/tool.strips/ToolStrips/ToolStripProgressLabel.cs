using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace cc.isr.WinControls;

/// <summary> A tool strip progress label. </summary>
/// <remarks>
/// (c) 2007 HyperCubed. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-09-19. http://www.codeproject.com/script/Membership/View.aspx?mid=722189
/// http://www.codeproject.com/Articles/21419/Label-with-ProgressBar-in-a-StatusStrip.
/// </para>
/// </remarks>
[DesignerCategory( "code" )]
[Description( "Status label with progress bar" )]
[ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.StatusStrip )]
public class ToolStripProgressLabel : System.Windows.Forms.ToolStripStatusLabel
#if WINDOWS7_0_OR_GREATER
#else
    , IBindableComponent
#endif
{
    #region " bindable "

#if WINDOWS7_0_OR_GREATER
#else

    private BindingContext? _context = null;

    /// <summary>
    /// Gets or sets the collection of currency managers for the
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The collection of <see cref="BindingManagerBase" /> objects for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [Browsable( false )]
    public BindingContext? BindingContext
    {
        get
        {
            this._context ??= new BindingContext();

            return this._context;
        }

        set => this._context = value;
    }

    private ControlBindingsCollection? _bindings;

    /// <summary>
    /// Gets the collection of data-binding objects for this
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The <see cref="ControlBindingsCollection" /> for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
    public ControlBindingsCollection? DataBindings
    {
        get
        {
            this._bindings ??= new ControlBindingsCollection( this );

            return this._bindings;
        }
    }

#endif

    #endregion

    #region " progress implementation "

    /// <summary> Paints this window. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="PaintEventArgs" />
    /// that contains the event data. </param>
    protected override void OnPaint( PaintEventArgs e )
    {
        if ( e is null ) return;

        double percent = this.Value / 100d;
        Rectangle rect = e.ClipRectangle;
        int height = this.BarHeight;
        if ( height == 0 )
        {
            height = rect.Height;
        }

        rect.Width = ( int ) (rect.Width * percent);
        rect.Y = ( int ) (0.5d * (rect.Height - height));
        rect.Height = height;
        using ( SolidBrush brush = new( this.BarColor ) )
        {
            // Draw bar
            Graphics g = e.Graphics;
            g.FillRectangle( brush, rect );
        }

        base.OnPaint( e );
    }

    /// <summary> The current progress value. </summary>
    private int _value;

    /// <summary> Progress Value. </summary>
    /// <value> The value. </value>
    [Category( "Behavior" )]
    [Description( "Progress Value" )]
    [DefaultValue( 0 )]
    public int Value
    {
        get => this._value;
        set
        {
            // Make sure that the value does not stray outside the valid range.
            value = value < 0 ? 0 : value > 100 ? 100 : value;
            if ( this.Value != value )
            {
                this._value = value;
                this.Invalidate();
            }
        }
    }

    private Color _barColor = Color.Blue;   // Color of progress meter

    /// <summary> Progress color. </summary>
    /// <value> The color of the bar. </value>
    [Category( "Behavior" )]
    [Description( "Progress color" )]
    [DefaultValue( typeof( Color ), "Blue" )]
    public Color BarColor
    {
        get => this._barColor;
        set
        {
            this._barColor = value;

            // Invalidate the control to get a repaint.
            this.Invalidate();
        }
    }

    private int _barHeight;

    /// <summary> Progress height. </summary>
    /// <value> The height of the bar. </value>
    [Category( "Behavior" )]
    [Description( "Progress height" )]
    [DefaultValue( 0 )]
    public int BarHeight
    {
        get => this._barHeight;
        set
        {
            switch ( value )
            {
                case var @case when @case > this.Size.Height:
                case var case1 when case1 < 0:
                    {
                        this._barHeight = this.Size.Height;
                        break;
                    }

                default:
                    {
                        this._barHeight = value;
                        break;
                    }
            }

            // Invalidate the control to get a repaint.
            this.Invalidate();
        }
    }

    private string? _captionFormat;

    /// <summary> Specifies the format of the overlay. </summary>
    /// <value> The caption format. </value>
    [Category( "Appearance" )]
    [DefaultValue( "{0} %" )]
    [Description( "Specifies the format of the overlay." )]
    public string CaptionFormat
    {
        get => string.IsNullOrEmpty( this._captionFormat ) ? "{0} %" : this._captionFormat!;
        set => this._captionFormat = value;
    }

    /// <summary> The default caption format. </summary>
#pragma warning disable IDE0079
#pragma warning disable CA1707
    public const string DEFAULT_CAPTION_FORMAT = "{0} %";
#pragma warning restore CA1707
#pragma warning restore IDE0079

    /// <summary> Updates the progress described by value. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    public void UpdateProgress( int value )
    {
        string format = this.CaptionFormat;
        if ( string.IsNullOrEmpty( format ) )
        {
            format = DEFAULT_CAPTION_FORMAT;
        }

        this.UpdateProgress( value, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, value ) );
    }

    /// <summary> Updates the progress described by arguments. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value">   The value. </param>
    /// <param name="caption"> The caption. </param>
    public void UpdateProgress( int value, string caption )
    {
        if ( value >= 0 ^ this.Visible )
        {
            this.Visible = value >= 0;
        }

        if ( this.Visible )
        {
            this.Text = caption;
            this.Value = value;
        }
    }
    #endregion
}
