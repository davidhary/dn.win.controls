#if WINDOWS7_0_OR_GREATER
#else
using System.Windows.Forms;
using System.ComponentModel;
#endif
using System.Windows.Forms.Design;

namespace cc.isr.WinControls;

/// <summary> A bindable tool strip drop down button. </summary>
/// <remarks>
/// <para>
/// David, 2018-09-27 </para><para>
/// http://forums.devx.com/showthread.php?153607-Making-ToolStripStatusLabel-data-bindable.
/// </para>
/// </remarks>
[ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
public class ToolStripDropDownButton : System.Windows.Forms.ToolStripDropDownButton
#if WINDOWS7_0_OR_GREATER
#else
    , IBindableComponent
#endif
{
    #region " bindable "

#if WINDOWS7_0_OR_GREATER
#else

    private BindingContext? _context = null;

    /// <summary>
    /// Gets or sets the collection of currency managers for the
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The collection of <see cref="BindingManagerBase" /> objects for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [Browsable( false )]
    public BindingContext? BindingContext
    {
        get
        {
            this._context ??= new BindingContext();

            return this._context;
        }

        set => this._context = value;
    }

    private ControlBindingsCollection? _bindings;

    /// <summary>
    /// Gets the collection of data-binding objects for this
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The <see cref="ControlBindingsCollection" /> for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
    public ControlBindingsCollection? DataBindings
    {
        get
        {
            this._bindings ??= new ControlBindingsCollection( this );

            return this._bindings;
        }
    }

#endif

    #endregion
}
