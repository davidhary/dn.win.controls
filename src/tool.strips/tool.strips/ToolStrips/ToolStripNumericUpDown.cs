using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace cc.isr.WinControls;

/// <summary> Tool strip numeric up down. </summary>
/// <remarks> David, 2014-04-16. </remarks>
[ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
public class ToolStripNumericUpDown : ToolStripControlHost
#if WINDOWS7_0_OR_GREATER
#else
    , IBindableComponent
#endif
{
    #region " construction and cleanup "

    /// <summary> Default constructor. </summary>
    /// <remarks> Call the base constructor passing in a NumericUpDown instance. </remarks>
    public ToolStripNumericUpDown() : base( new NumericUpDown() )
    {
    }

    #endregion

    #region " bindable "

#if WINDOWS7_0_OR_GREATER
#else

    private BindingContext? _context = null;

    /// <summary>
    /// Gets or sets the collection of currency managers for the
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The collection of <see cref="BindingManagerBase" /> objects for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [Browsable( false )]
    public BindingContext? BindingContext
    {
        get
        {
            this._context ??= new BindingContext();

            return this._context;
        }

        set => this._context = value;
    }

    private ControlBindingsCollection? _bindings;

    /// <summary>
    /// Gets the collection of data-binding objects for this
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The <see cref="ControlBindingsCollection" /> for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
    public ControlBindingsCollection? DataBindings
    {
        get
        {
            this._bindings ??= new ControlBindingsCollection( this );

            return this._bindings;
        }
    }

#endif

    #endregion

    #region " numeric up down "

    /// <summary> Gets the numeric up down control. </summary>
    /// <value> The numeric up down control. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
    public NumericUpDown NumericUpDown => ( NumericUpDown ) this.Control;

    /// <summary> Gets or sets the selected text. </summary>
    /// <value> The selected text. </value>
    [DefaultValue( "" )]
    [Description( "text" )]
    [Category( "Appearance" )]
    public override string Text
    {
        get => this.NumericUpDown.Text;

#pragma warning disable IDE0079
#pragma warning disable CS8765 // Nullability of type of parameter doesn't match overridden member (possibly because of nullability attributes).
        set => SafeSetter( this.NumericUpDown, () => this.NumericUpDown.Text = value );
#pragma warning restore CS8765 
#pragma warning restore IDE0079
    }

    /// <summary> Safe setter. </summary>
    /// <remarks> David, 2014-04-16. </remarks>
    /// <param name="control"> The control from which to subscribe events. </param>
    /// <param name="setter">  The setter. </param>
    private static void SafeSetter( Control control, Action setter )
    {
        if ( control is not null && control.Parent is not null )
        {
            if ( control.Parent.InvokeRequired )
            {
                _ = control.Parent.BeginInvoke( new Action<Control, Action>( SafeSetter ), [control, setter] );
            }
            else if ( control.Parent.IsHandleCreated )
            {
                setter.Invoke();
            }
        }
    }

    /// <summary> Gets or sets the value. </summary>
    /// <value> The value. </value>
    [DefaultValue( "" )]
    [Description( "Value" )]
    [Category( "Appearance" )]
    public decimal Value
    {
        get => this.NumericUpDown.Value;
        set => SafeSetter( this.NumericUpDown, () => this.NumericUpDown.Value = value );
    }

    /// <summary> Gets a value indicating whether this object has value. </summary>
    /// <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public bool HasValue => this.NumericUpDown.HasValue();

    #endregion
}
