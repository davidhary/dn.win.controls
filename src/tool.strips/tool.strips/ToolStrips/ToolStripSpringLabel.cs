using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace cc.isr.WinControls;

/// <summary> A tool strip spring Label. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-03-22 </para>
/// </remarks>
[ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
public class ToolStripSpringLabel : System.Windows.Forms.ToolStripLabel, ISpringable
#if WINDOWS7_0_OR_GREATER
#else
    , IBindableComponent
#endif
{
    #region " bindable "

#if WINDOWS7_0_OR_GREATER
#else

    private BindingContext? _context = null;

    /// <summary>
    /// Gets or sets the collection of currency managers for the
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The collection of <see cref="BindingManagerBase" /> objects for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [Browsable( false )]
    public BindingContext? BindingContext
    {
        get
        {
            this._context ??= new BindingContext();

            return this._context;
        }

        set => this._context = value;
    }

    private ControlBindingsCollection? _bindings;

    /// <summary>
    /// Gets the collection of data-binding objects for this
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The <see cref="ControlBindingsCollection" /> for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
    public ControlBindingsCollection? DataBindings
    {
        get
        {
            this._bindings ??= new ControlBindingsCollection( this );

            return this._bindings;
        }
    }

#endif

    #endregion

    #region " i springable "

    /// <summary> Gets the sentinel indicating if the tool strip item can spring. </summary>
    /// <value> The sentinel indicating if the tool strip item can spring. </value>
    public bool CanSpring => this.AutoSize && this.Spring;

    /// <summary> Gets or sets the spring. </summary>
    /// <value>
    /// <c>true</c> if the control stretches to fill the remaining space in the owner control.
    /// </value>
    [DefaultValue( false )]
    [Description( "Spring" )]
    [Category( "Appearance" )]
    public bool Spring { get; set; }

    #endregion

    #region " spring implementation "

    /// <summary> Retrieves the size of a rectangular area into which a control can be fit. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="constrainingSize"> The custom-sized area for a control. </param>
    /// <returns>
    /// A <see cref="Size" /> ordered pair, representing the width and height of a
    /// rectangle.
    /// </returns>
    public override Size GetPreferredSize( Size constrainingSize )
    {
        if ( this.Owner is null ) return new Size( 0, 0 );

        // Use the default size if the tool strip item is on the overflow menu,
        // is on a vertical ToolStrip, or cannot spring.
        if ( this.IsOnOverflow || this.Owner.Orientation == Orientation.Vertical || !this.CanSpring )
        {
            return this.DefaultSize;
        }

        // Declare a variable to store the total available width as 
        // it is calculated, starting with the display width of the 
        // owning ToolStrip.
        int width = this.Owner.DisplayRectangle.Width;

        // Subtract the width of the overflow button if it is displayed. 
        if ( this.Owner.OverflowButton.Visible )
        {
            width = width - this.Owner.OverflowButton.Width - this.Owner.OverflowButton.Margin.Horizontal;
        }

        // Declare a variable to maintain a count of Spring items
        // currently displayed in the owning ToolStrip. 
        int springItemCount = 0;
        foreach ( ToolStripItem item in this.Owner.Items )
        {
            // Ignore items on the overflow menu.
            if ( item.IsOnOverflow )
            {
                continue;
            }

            if ( item is ISpringable { CanSpring: true } )
            {
                // For Spring items, increment the count and 
                // subtract the margin width from the total available width.
                springItemCount += 1;
                width -= item.Margin.Horizontal;
            }
            else
            {
                // For all other items, subtract the full width from the total
                // available width.
                width = width - item.Width - item.Margin.Horizontal;
            }
        }

        // If there are multiple spring items in the owning
        // ToolStrip, divide the total available width between them. 
        if ( springItemCount > 1 )
        {
            width /= springItemCount;
        }

        // If the available width is less than the default width, use the
        // default width, forcing one or more items onto the overflow menu.
        if ( width < this.DefaultSize.Width )
        {
            width = this.DefaultSize.Width;
        }

        // Retrieve the preferred size from the base class, but change the
        // width to the calculated width. 
        Size preferredSize = base.GetPreferredSize( constrainingSize );
        preferredSize.Width = width;
        return preferredSize;
    }
    #endregion
}
