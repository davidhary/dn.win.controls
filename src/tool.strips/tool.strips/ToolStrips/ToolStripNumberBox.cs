using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace cc.isr.WinControls;

/// <summary> Tool strip number box. </summary>
/// <remarks>
/// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2020-04-08. </para>
/// </remarks>
[ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
public class ToolStripNumberBox : ToolStripControlHost
#if WINDOWS7_0_OR_GREATER
#else
    , IBindableComponent
#endif
{
    #region " construction and cleanup "

    /// <summary> Default constructor. </summary>
    /// <remarks> Call the base constructor passing in a NumberBox instance. </remarks>
    public ToolStripNumberBox() : base( new NumberBox() )
    {
    }

    #endregion

    #region " bindable "

#if WINDOWS7_0_OR_GREATER
#else

    private BindingContext? _context = null;

    /// <summary>
    /// Gets or sets the collection of currency managers for the
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The collection of <see cref="BindingManagerBase" /> objects for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [Browsable( false )]
    public BindingContext? BindingContext
    {
        get
        {
            this._context ??= new BindingContext();

            return this._context;
        }

        set => this._context = value;
    }

    private ControlBindingsCollection? _bindings;

    /// <summary>
    /// Gets the collection of data-binding objects for this
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The <see cref="ControlBindingsCollection" /> for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
    public ControlBindingsCollection? DataBindings
    {
        get
        {
            this._bindings ??= new ControlBindingsCollection( this );

            return this._bindings;
        }
    }

#endif

    #endregion

    #region " number box "

    /// <summary>   Gets the number box. </summary>
    /// <value> The number box. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
    public NumberBox NumberBox => ( NumberBox ) this.Control;

    /// <summary> Standard Numeric Format String D,E,F,N,X, B for binary. </summary>
    /// <value> The Standard numeric format string </value>
    [Category( "NumberBox" ), Description( "Standard Numeric Format String D,E,F,N,X, B for binary" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string NumberFormatString
    {
        get => this.NumberBox.NumberFormatString;
        set => this.NumberBox.NumberFormatString = value;
    }

    /// <summary>   Prefix for X and B formats. </summary>
    /// <value> The prefix. </value>
    [Category( "NumberBox" ), Description( "Prefix for X and B formats" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string? Prefix
    {
        get => this.NumberBox.Prefix;
        set => this.NumberBox.Prefix = value;
    }

    /// <summary>   Suffix for X and B formats. </summary>
    /// <value> The suffix. </value>
    [Category( "NumberBox" ), Description( "Suffix for X and B formats" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string? Suffix
    {
        get => this.NumberBox.Suffix;
        set => this.NumberBox.Suffix = value;
    }

    /// <summary>   Gets/sets Max Value...double. </summary>
    /// <value> The maximum value. </value>
    [Category( "NumberBox" ), Description( "gets/sets Max Value...double" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double MaxValue
    {
        get => this.NumberBox.MaxValue;
        set => this.NumberBox.MaxValue = value;
    }

    /// <summary>   Gets/sets Min Value...double. </summary>
    /// <value> The minimum value. </value>
    [Category( "NumberBox" ), Description( "gets/sets Min Value...double" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double MinValue
    {
        get => this.NumberBox.MinValue;
        set => this.NumberBox.MinValue = value;
    }

    /// <summary>   Gets/sets Value As Double. </summary>
    /// <value> The value as double. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As Double" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double ValueAsDouble
    {
        get => this.NumberBox.ValueAsDouble;
        set => this.NumberBox.ValueAsDouble = value;
    }

    /// <summary>   Gets/sets Value As Float. </summary>
    /// <value> The value as float. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As Float" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public float ValueAsFloat
    {
        get => this.NumberBox.ValueAsFloat;
        set => this.NumberBox.ValueAsFloat = value;
    }

    /// <summary>   Gets/sets Value As Byte. </summary>
    /// <value> The value as byte. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As Byte" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public byte ValueAsByte
    {
        get => this.NumberBox.ValueAsByte;
        set => this.NumberBox.ValueAsByte = value;
    }

    /// <summary>   Gets/sets Value As SByte. </summary>
    /// <value> The value as s byte. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As SByte" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public sbyte ValueAsSByte
    {
        get => this.NumberBox.ValueAsSByte;
        set => this.NumberBox.ValueAsSByte = value;
    }

    /// <summary>   Gets/sets Value As UInt64. </summary>
    /// <value> The value as u int 64. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As UInt64" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ulong ValueAsUInt64
    {
        get => this.NumberBox.ValueAsUInt64;
        set => this.NumberBox.ValueAsUInt64 = value;
    }

    /// <summary>   Gets/sets Value As UInt32. </summary>
    /// <value> The value as u int 32. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As UInt32" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public uint ValueAsUInt32
    {
        get => this.NumberBox.ValueAsUInt32;
        set => this.NumberBox.ValueAsUInt32 = value;
    }

    /// <summary>   Gets/sets Value As UInt16. </summary>
    /// <value> The value as u int 16. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As UInt16" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ushort ValueAsUInt16
    {
        get => this.NumberBox.ValueAsUInt16;
        set => this.NumberBox.ValueAsUInt16 = value;
    }

    /// <summary>   Gets/sets Value As Int64. </summary>
    /// <value> The value as int 64. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As Int64" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public long ValueAsInt64
    {
        get => this.NumberBox.ValueAsInt64;
        set => this.NumberBox.ValueAsInt64 = value;
    }

    /// <summary>   Gets/sets Value As Int32. </summary>
    /// <value> The value as int 32. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As Int32" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int ValueAsInt32
    {
        get => this.NumberBox.ValueAsInt32;
        set => this.NumberBox.ValueAsInt32 = value;
    }

    /// <summary>   Gets/sets Value As Int16. </summary>
    /// <value> The value as int 16. </value>
    [Category( "NumberBox" ), Description( "gets/sets Value As Int16" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public short ValueAsInt16
    {
        get => this.NumberBox.ValueAsInt16;
        set => this.NumberBox.ValueAsInt16 = value;
    }

    #endregion
}
