using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace cc.isr.WinControls;

/// <summary> Tool strip range number box. </summary>
/// <remarks>
/// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2020-04-08. </para>
/// </remarks>
[ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
public class ToolStripRangeNumberBox : ToolStripControlHost
#if WINDOWS7_0_OR_GREATER
#else
    , IBindableComponent
#endif
{
    #region " construction and cleanup "

    /// <summary> Default constructor. </summary>
    /// <remarks> Call the base constructor passing in a NumberBox instance. </remarks>
    public ToolStripRangeNumberBox() : base( new RangeNumberBox() )
    {
    }

    #endregion

    #region " bindable "

#if WINDOWS7_0_OR_GREATER
#else

    private BindingContext? _context = null;

    /// <summary>
    /// Gets or sets the collection of currency managers for the
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The collection of <see cref="BindingManagerBase" /> objects for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [Browsable( false )]
    public BindingContext? BindingContext
    {
        get
        {
            this._context ??= new BindingContext();

            return this._context;
        }

        set => this._context = value;
    }

    private ControlBindingsCollection? _bindings;

    /// <summary>
    /// Gets the collection of data-binding objects for this
    /// <see cref="IBindableComponent" />.
    /// </summary>
    /// <value>
    /// The <see cref="ControlBindingsCollection" /> for this
    /// <see cref="IBindableComponent" />.
    /// </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
    public ControlBindingsCollection? DataBindings
    {
        get
        {
            this._bindings ??= new ControlBindingsCollection( this );

            return this._bindings;
        }
    }

#endif

    #endregion

    #region " range number box "

    /// <summary> Gets the numeric up down control. </summary>
    /// <value> The numeric up down control. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
    public RangeNumberBox RangeNumberBox => ( RangeNumberBox ) this.Control;

    /// <summary> Standard Numeric Format String D,E,F,N,X, B for binary. </summary>
    /// <value> The Standard numeric format string </value>
    [Category( "RangeNumberBox" ), Description( "Standard Numeric Format String D,E,F,N,X, B for binary" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string NumberFormatString
    {
        get => this.RangeNumberBox.NumberFormatString;
        set => this.RangeNumberBox.NumberFormatString = value;
    }

    /// <summary>   Prefix for X and B formats. </summary>
    /// <value> The prefix. </value>
    [Category( "RangeNumberBox" ), Description( "Prefix for X and B formats" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string? Prefix
    {
        get => this.RangeNumberBox.Prefix;
        set => this.RangeNumberBox.Prefix = value;
    }

    /// <summary>   Suffix for X and B formats. </summary>
    /// <value> The suffix. </value>
    [Category( "RangeNumberBox" ), Description( "Suffix for X and B formats" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string? Suffix
    {
        get => this.RangeNumberBox.Suffix;
        set => this.RangeNumberBox.Suffix = value;
    }

    /// <summary>   Gets/sets Max Value...double. </summary>
    /// <value> The maximum value. </value>
    [Category( "RangeNumberBox" ), Description( "gets/sets Max Value...double" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double Maximum
    {
        get => this.RangeNumberBox.Maximum;
        set => this.RangeNumberBox.Maximum = value;
    }

    /// <summary>   Gets/sets Min Value...double. </summary>
    /// <value> The minimum value. </value>
    [Category( "RangeNumberBox" ), Description( "gets/sets Min Value...double" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double Minimum
    {
        get => this.RangeNumberBox.Minimum;
        set => this.RangeNumberBox.Minimum = value;
    }

    #endregion

    #region " lower range number box "

    /// <summary>   Gets/sets the Lower Value As Double. </summary>
    /// <value> The value as double. </value>
    [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As Double" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double? LowerValueAsDouble
    {
        get => this.RangeNumberBox.LowerNumberBox?.ValueAsDouble;
        set
        {
            if ( this.RangeNumberBox?.LowerNumberBox is not null )
                this.RangeNumberBox.LowerNumberBox.ValueAsDouble = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Lower Value As Float. </summary>
    /// <value> The value as float. </value>
    [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As Float" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public float? LowerValueAsFloat
    {
        get => this.RangeNumberBox.LowerNumberBox?.ValueAsFloat;
        set
        {
            if ( this.RangeNumberBox?.LowerNumberBox is not null )
                this.RangeNumberBox.LowerNumberBox.ValueAsFloat = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Lower Value As Byte. </summary>
    /// <value> The value as byte. </value>
    [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As Byte" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public byte? LowerValueAsByte
    {
        get => this.RangeNumberBox.LowerNumberBox?.ValueAsByte;
        set
        {
            if ( this.RangeNumberBox?.LowerNumberBox is not null )
                this.RangeNumberBox.LowerNumberBox.ValueAsByte = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Lower Value As Signed 8-bit integer. </summary>
    /// <value> The value as signed 8-bit integer. </value>
    [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As SByte" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public sbyte? LowerValueAsSByte
    {
        get => this.RangeNumberBox.LowerNumberBox?.ValueAsSByte;
        set
        {
            if ( this.RangeNumberBox?.LowerNumberBox is not null )
                this.RangeNumberBox.LowerNumberBox.ValueAsSByte = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Lower Value As UInt64. </summary>
    /// <value> The value as unsigned int 64. </value>
    [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As UInt64" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ulong? LowerValueAsUInt64
    {
        get => this.RangeNumberBox.LowerNumberBox?.ValueAsUInt64;
        set
        {
            if ( this.RangeNumberBox?.LowerNumberBox is not null )
                this.RangeNumberBox.LowerNumberBox.ValueAsUInt64 = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Lower Value As UInt32. </summary>
    /// <value> The value as unsigned int 32. </value>
    [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As UInt32" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public uint? LowerValueAsUInt32
    {
        get => this.RangeNumberBox.LowerNumberBox?.ValueAsUInt32;
        set
        {
            if ( this.RangeNumberBox?.LowerNumberBox is not null )
                this.RangeNumberBox.LowerNumberBox.ValueAsUInt32 = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Lower Value As UInt16. </summary>
    /// <value> The value as unsigned int 16. </value>
    [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As UInt16" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ushort? LowerValueAsUInt16
    {
        get => this.RangeNumberBox.LowerNumberBox?.ValueAsUInt16;
        set
        {
            if ( this.RangeNumberBox?.LowerNumberBox is not null )
                this.RangeNumberBox.LowerNumberBox.ValueAsUInt16 = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Lower Value As Int64. </summary>
    /// <value> The value as int 64. </value>
    [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As Int64" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public long? LowerValueAsInt64
    {
        get => this.RangeNumberBox.LowerNumberBox?.ValueAsInt64;
        set
        {
            if ( this.RangeNumberBox?.LowerNumberBox is not null )
                this.RangeNumberBox.LowerNumberBox.ValueAsInt64 = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Lower Value As Int32. </summary>
    /// <value> The value as int 32. </value>
    [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As Int32" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int? LowerValueAsInt32
    {
        get => this.RangeNumberBox.LowerNumberBox?.ValueAsInt32;
        set
        {
            if ( this.RangeNumberBox?.LowerNumberBox is not null )
                this.RangeNumberBox.LowerNumberBox.ValueAsInt32 = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Lower Value As Int16. </summary>
    /// <value> The value as int 16. </value>
    [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As Int16" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public short? LowerValueAsInt16
    {
        get => this.RangeNumberBox.LowerNumberBox?.ValueAsInt16;
        set
        {
            if ( this.RangeNumberBox?.LowerNumberBox is not null )
                this.RangeNumberBox.LowerNumberBox.ValueAsInt16 = value ?? 0;
        }
    }

    #endregion

    #region " upper range number box "

    /// <summary>   Gets/sets the Upper Value As Double. </summary>
    /// <value> The value as double. </value>
    [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As Double" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public double? UpperValueAsDouble
    {
        get => this.RangeNumberBox.UpperNumberBox?.ValueAsDouble;
        set
        {
            if ( this.RangeNumberBox?.UpperNumberBox is not null )
                this.RangeNumberBox.UpperNumberBox.ValueAsDouble = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Upper Value As Float. </summary>
    /// <value> The value as float. </value>
    [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As Float" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public float? UpperValueAsFloat
    {
        get => this.RangeNumberBox.UpperNumberBox?.ValueAsFloat;
        set
        {
            if ( this.RangeNumberBox?.UpperNumberBox is not null )
                this.RangeNumberBox.UpperNumberBox.ValueAsFloat = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Upper Value As Byte. </summary>
    /// <value> The value as byte. </value>
    [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As Byte" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public byte? UpperValueAsByte
    {
        get => this.RangeNumberBox.UpperNumberBox?.ValueAsByte;
        set
        {
            if ( this.RangeNumberBox?.UpperNumberBox is not null )
                this.RangeNumberBox.UpperNumberBox.ValueAsByte = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Upper Value As Signed 8-bit integer. </summary>
    /// <value> The value as signed 8-bit integer. </value>
    [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As SByte" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public sbyte? UpperValueAsSByte
    {
        get => this.RangeNumberBox.UpperNumberBox?.ValueAsSByte;
        set
        {
            if ( this.RangeNumberBox?.UpperNumberBox is not null )
                this.RangeNumberBox.UpperNumberBox.ValueAsSByte = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Upper Value As UInt64. </summary>
    /// <value> The value as unsigned int 64. </value>
    [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As UInt64" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ulong? UpperValueAsUInt64
    {
        get => this.RangeNumberBox.UpperNumberBox?.ValueAsUInt64;
        set
        {
            if ( this.RangeNumberBox?.UpperNumberBox is not null )
                this.RangeNumberBox.UpperNumberBox.ValueAsUInt64 = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Upper Value As UInt32. </summary>
    /// <value> The value as unsigned int 32. </value>
    [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As UInt32" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public uint? UpperValueAsUInt32
    {
        get => this.RangeNumberBox.UpperNumberBox?.ValueAsUInt32;
        set
        {
            if ( this.RangeNumberBox?.UpperNumberBox is not null )
                this.RangeNumberBox.UpperNumberBox.ValueAsUInt32 = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Upper Value As UInt16. </summary>
    /// <value> The value as unsigned int 16. </value>
    [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As UInt16" ), Browsable( true ), CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public ushort? UpperValueAsUInt16
    {
        get => this.RangeNumberBox.UpperNumberBox?.ValueAsUInt16;
        set
        {
            if ( this.RangeNumberBox?.UpperNumberBox is not null )
                this.RangeNumberBox.UpperNumberBox.ValueAsUInt16 = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Upper Value As Int64. </summary>
    /// <value> The value as int 64. </value>
    [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As Int64" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public long? UpperValueAsInt64
    {
        get => this.RangeNumberBox.UpperNumberBox?.ValueAsInt64;
        set
        {
            if ( this.RangeNumberBox?.UpperNumberBox is not null )
                this.RangeNumberBox.UpperNumberBox.ValueAsInt64 = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Upper Value As Int32. </summary>
    /// <value> The value as int 32. </value>
    [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As Int32" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public int? UpperValueAsInt32
    {
        get => this.RangeNumberBox.UpperNumberBox?.ValueAsInt32;
        set
        {
            if ( this.RangeNumberBox?.UpperNumberBox is not null )
                this.RangeNumberBox.UpperNumberBox.ValueAsInt32 = value ?? 0;
        }
    }

    /// <summary>   Gets/sets the Upper Value As Int16. </summary>
    /// <value> The value as int 16. </value>
    [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As Int16" ), Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public short? UpperValueAsInt16
    {
        get => this.RangeNumberBox.UpperNumberBox?.ValueAsInt16;
        set
        {
            if ( this.RangeNumberBox?.UpperNumberBox is not null )
                this.RangeNumberBox.UpperNumberBox.ValueAsInt16 = value ?? 0;
        }
    }

    #endregion
}
