using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace cc.isr.WinControls;
/// <summary>
/// A user control base. Supports property change notifications. Useful for a settings publisher.
/// </summary>
/// <remarks>
/// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License. </para><para>
/// David, 2010-11-02, 1.2.3988 </para>
/// </remarks>
public partial class ModelViewBase : UserControl, INotifyPropertyChanged
{
    #region " construction and cleanup "

    /// <summary> Gets the initializing components sentinel. </summary>
    /// <value> The initializing components sentinel. </value>
    protected bool InitializingComponents { get; set; }

    /// <summary>
    /// A private constructor for this class making it not publicly creatable. This ensure using the
    /// class as a singleton.
    /// </summary>
    protected ModelViewBase() : base()
    {
        this.InitializingComponents = true;
        this.InitializeComponent();
        this.InitializingComponents = false;
    }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="Control" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    [System.Diagnostics.DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " form load "

    /// <summary> Handles the <see cref="UserControl.Load" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnLoad( EventArgs e )
    {
        try
        {
            if ( !this.DesignMode )
            {
                this.InfoProvider?.Clear();
                cc.isr.WinControls.ControlCollectionExtensions.ControlCollectionExtensionMethods.ToolTipSetter( this.Controls, this.ToolTip );
            }
        }
        finally
        {
            base.OnLoad( e );
        }
    }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <param name="propertyName"> Name of the property. </param>
    protected virtual void OnPropertyChanged( string? propertyName )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<TValue>( ref TValue backingField, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<TValue>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( ref TValue prop, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
        if ( EqualityComparer<TValue>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( TValue oldValue, TValue newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif

        if ( EqualityComparer<TValue>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion

    #region " core properties  "

    /// <summary> Gets the information provider. </summary>
    /// <value> The information provider. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    protected InfoProvider? InfoProvider { get; private set; }

    /// <summary> Gets the tool tip. </summary>
    /// <value> The tool tip. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    protected ToolTip? ToolTip { get; private set; }

    private string? _statusPrompt;

    /// <summary> The status prompt. </summary>
    /// <value> The status prompt. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public string? StatusPrompt
    {
        get => this._statusPrompt;
        set
        {
            if ( !string.Equals( value, this.StatusPrompt, StringComparison.Ordinal ) )
            {
                this._statusPrompt = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    #endregion

    #region " exception event handler "

    /// <summary>   Event queue for all listeners interested in <see cref="Exception"/> events. </summary>
    public event EventHandler<System.Threading.ThreadExceptionEventArgs>? ExceptionEventHandler;

    /// <summary>   Raises the  <see cref="ExceptionEventHandler"/> event. </summary>
    /// <remarks>   David, 2021-05-03. </remarks>
    /// <param name="e">                                Event information to send to registered event
    ///                                                 handlers. </param>
    /// <param name="displayMessageBoxIfNoSubscribers"> (Optional) True to display message box if no
    ///                                                 subscribers. </param>
    protected virtual void OnEventHandlerError( System.Threading.ThreadExceptionEventArgs e, bool displayMessageBoxIfNoSubscribers = true )
    {
        this.ExceptionEventHandler?.Invoke( this, e );
        if ( this.ExceptionEventHandler is null && displayMessageBoxIfNoSubscribers )
            _ = MessageBox.Show( e.Exception.ToString() );
    }

    /// <summary>   Raises the  <see cref="ExceptionEventHandler"/> event. </summary>
    /// <remarks>   David, 2021-07-29. </remarks>
    /// <param name="exception">                        The exception. </param>
    /// <param name="displayMessageBoxIfNoSubscribers"> (Optional) True to display message box if no
    ///                                                 subscribers. </param>
    protected virtual void OnEventHandlerError( Exception? exception, bool displayMessageBoxIfNoSubscribers = true )
    {
        if ( exception is not null )
            this.OnEventHandlerError( new System.Threading.ThreadExceptionEventArgs( exception ), displayMessageBoxIfNoSubscribers );
    }

    #endregion
}
