using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace cc.isr.WinControls;

public partial class ModelViewBase
{
    /// <summary> Event queue for all listeners interested in FormClosing events. </summary>
    /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
    /// Using a custom Raise method lets you iterate through the delegate list.
    /// </remarks>
    public event EventHandler<CancelEventArgs>? FormClosing;

    /// <summary>
    /// Synchronously invokes the <see cref="FormClosing">FormClosing Event</see>. Not thread safe.
    /// </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="e">    The <see cref="EventArgs" /> instance containing the event data. </param>
    protected void NotifyFormClosing( CancelEventArgs e )
    {
        this.NotifyFormClosing( this, e );
    }

    /// <summary>
    /// Synchronously invokes the <see cref="FormClosing">FormClosing Event</see>. Not thread safe.
    /// </summary>
    /// <remarks>   David, 2021-03-03. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Cancel event information. </param>
    protected void NotifyFormClosing( object? sender, CancelEventArgs e )
    {
        this.FormClosing?.Invoke( sender, e );
    }

    /// <summary>
    /// Handles the container form <see cref="FormClosing" /> event.
    /// </summary>
    /// <remarks>   David, 2021-03-03. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information to send to registered event handlers. </param>
    private void OnFormClosing( object? sender, CancelEventArgs e )
    {
        this.NotifyFormClosing( sender, e );
    }

    /// <summary> Handles the container form closing when the parent form is closing. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Cancel event information. </param>
    private void HandleFormClosing( object? sender, CancelEventArgs e )
    {
        this.OnFormClosing( sender, e );
    }

    /// <summary>
    /// Handles the container form <see cref="FormClosing" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="CancelEventArgs" /> that contains the event
    /// data. </param>
    protected virtual void OnFormClosing( CancelEventArgs e )
    {
        this.NotifyFormClosing( e );
    }

    /// <summary> Gets the ancestor form. </summary>
    /// <value> The ancestor form. </value>
    private Form? AncestorForm { get; set; }

    /// <summary> Searches for the first ancestor form. </summary>
    /// <remarks>
    /// When called from the <see cref="Control.ParentChanged"/> event, the method returns the form
    /// associated with the top control.  The top control attends a form only after the form layout
    /// completes, so this method ends up being called a few times before the form is located.
    /// </remarks>
    /// <returns> The found ancestor form. </returns>
    private Form? FindAncestorForm()
    {
        Control? parentControl = this.Parent;
        Form? parentForm = parentControl as Form;
        while ( !(parentForm is not null) && parentControl is not null )
        {
            parentControl = parentControl.Parent;
            parentForm = parentControl as Form;
        }

        return parentForm;
    }

    /// <summary> Adds closing handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void AddClosingHandler()
    {
        Form? candidateAncestorForm = this.FindAncestorForm();
        if ( this.AncestorForm is not null )
        {
            this.AncestorForm.Closing -= this.HandleFormClosing;
        }

        this.AncestorForm = candidateAncestorForm;
        if ( this.AncestorForm is not null )
        {
            this.AncestorForm.Closing += this.HandleFormClosing;
        }
    }

    /// <summary>
    /// Raises the <see cref="Control.ParentChanged" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnParentChanged( EventArgs e )
    {
        base.OnParentChanged( e );
        this.AddClosingHandler();
    }

}
