namespace cc.isr.WinControls.Demo;

/// <summary>   Form for viewing the toggle switch. </summary>
/// <remarks>   David, 2021-03-12. </remarks>
public partial class ToggleSwitchForm : Form
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Form" /> class.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public ToggleSwitchForm()
    {
        this.InitializeComponent();
        this.SetPropertiesForStylesTabSwitches();
        this.SetPropertiesForPropertiesTabSwitches();
        this.SetPropertiesForCustomizationsTabSwitches();
    }

    /// <summary>   Sets properties for styles tab switches. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public void SetPropertiesForStylesTabSwitches()
    {
        // Set the properties for the ToggleSwitches on the "Styles" tab

        this.MetroStyleToggleSwitch.Style = ToggleSwitchStyle.Metro; // Default
        this.MetroStyleToggleSwitch.Size = new Size( 75, 23 );
        this.IOS5StyleToggleSwitch.Style = ToggleSwitchStyle.Ios5;
        this.IOS5StyleToggleSwitch.Size = new Size( 98, 42 );
        this.IOS5StyleToggleSwitch.OnText = "ON";
        this.IOS5StyleToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold );
        this.IOS5StyleToggleSwitch.OnForeColor = Color.White;
        this.IOS5StyleToggleSwitch.OffText = "OFF";
        this.IOS5StyleToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold );
        this.IOS5StyleToggleSwitch.OffForeColor = Color.FromArgb( 141, 123, 141 );
        this.AndroidStyleToggleSwitch.Style = ToggleSwitchStyle.Android;
        this.AndroidStyleToggleSwitch.Size = new Size( 78, 23 );
        this.AndroidStyleToggleSwitch.OnText = "ON";
        this.AndroidStyleToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 8f, FontStyle.Bold );
        this.AndroidStyleToggleSwitch.OnForeColor = Color.White;
        this.AndroidStyleToggleSwitch.OffText = "OFF";
        this.AndroidStyleToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 8f, FontStyle.Bold );
        this.AndroidStyleToggleSwitch.OffForeColor = Color.FromArgb( 141, 123, 141 );
        this.BrushedMetalStyleToggleSwitch.Style = ToggleSwitchStyle.BrushedMetal;
        this.BrushedMetalStyleToggleSwitch.Size = new Size( 93, 30 );
        this.BrushedMetalStyleToggleSwitch.OnText = "ON";
        this.BrushedMetalStyleToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.BrushedMetalStyleToggleSwitch.OnForeColor = Color.White;
        this.BrushedMetalStyleToggleSwitch.OffText = "OFF";
        this.BrushedMetalStyleToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.BrushedMetalStyleToggleSwitch.OffForeColor = Color.White;
        this.IPhoneStyleToggleSwitch.Style = ToggleSwitchStyle.IPhone;
        this.IPhoneStyleToggleSwitch.Size = new Size( 93, 30 );
        this.IPhoneStyleToggleSwitch.OnText = "ON";
        this.IPhoneStyleToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.IPhoneStyleToggleSwitch.OnForeColor = Color.White;
        this.IPhoneStyleToggleSwitch.OffText = "OFF";
        this.IPhoneStyleToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.IPhoneStyleToggleSwitch.OffForeColor = Color.FromArgb( 92, 92, 92 );
        this.ModernStyleToggleSwitch.Style = ToggleSwitchStyle.Modern;
        this.ModernStyleToggleSwitch.Size = new Size( 85, 32 );
        this.ModernStyleToggleSwitch.OnText = "ON";
        this.ModernStyleToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.ModernStyleToggleSwitch.OnForeColor = Color.White;
        this.ModernStyleToggleSwitch.OffText = "OFF";
        this.ModernStyleToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.ModernStyleToggleSwitch.OffForeColor = Color.FromArgb( 153, 153, 153 );
        this.CarbonStyleToggleSwitch.Style = ToggleSwitchStyle.Carbon;
        this.CarbonStyleToggleSwitch.Size = new Size( 93, 30 );
        this.CarbonStyleToggleSwitch.OnText = "On";
        this.CarbonStyleToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.CarbonStyleToggleSwitch.OnForeColor = Color.White;
        this.CarbonStyleToggleSwitch.OffText = "Off";
        this.CarbonStyleToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.CarbonStyleToggleSwitch.OffForeColor = Color.White;
        this.OSXStyleToggleSwitch.Style = ToggleSwitchStyle.OS10;
        this.OSXStyleToggleSwitch.Size = new Size( 93, 25 );
        this.FancyStyleToggleSwitch.Style = ToggleSwitchStyle.Fancy;
        this.FancyStyleToggleSwitch.Size = new Size( 100, 30 );
        this.FancyStyleToggleSwitch.OnText = "ON";
        this.FancyStyleToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.FancyStyleToggleSwitch.OnForeColor = Color.White;
        this.FancyStyleToggleSwitch.OffText = "OFF";
        this.FancyStyleToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.FancyStyleToggleSwitch.OffForeColor = Color.White;
        this.FancyStyleToggleSwitch.ButtonImage = Demo.Properties.Resources.handle;
    }

    /// <summary>   Sets properties for properties tab switches. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public void SetPropertiesForPropertiesTabSwitches()
    {
        // Set the properties for the ToggleSwitches on the "(Semi)-Important Properties" tab

        // AllowUserChange example:

        this.AllowUserChangeToggleSwitch1.AllowUserChange = false;
        this.AllowUserChangeToggleSwitch2.AllowUserChange = true;

        // Animation example:

        this.NoAnimationToggleSwitch.Style = ToggleSwitchStyle.Carbon; // Only to provide an interesting look
        this.NoAnimationToggleSwitch.Size = new Size( 93, 30 ); // Only to provide an interesting look
        this.NoAnimationToggleSwitch.OnText = "On"; // Only to provide an interesting look
        this.NoAnimationToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold ); // Only to provide an interesting look
        this.NoAnimationToggleSwitch.OnForeColor = Color.White; // Only to provide an interesting look
        this.NoAnimationToggleSwitch.OffText = "Off"; // Only to provide an interesting look
        this.NoAnimationToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold ); // Only to provide an interesting look
        this.NoAnimationToggleSwitch.OffForeColor = Color.White; // Only to provide an interesting look
        this.NoAnimationToggleSwitch.UseAnimation = false;
        this.FastAnimationToggleSwitch.Style = ToggleSwitchStyle.Carbon; // Only to provide an interesting look
        this.FastAnimationToggleSwitch.Size = new Size( 93, 30 ); // Only to provide an interesting look
        this.FastAnimationToggleSwitch.OnText = "On"; // Only to provide an interesting look
        this.FastAnimationToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold ); // Only to provide an interesting look
        this.FastAnimationToggleSwitch.OnForeColor = Color.White; // Only to provide an interesting look
        this.FastAnimationToggleSwitch.OffText = "Off"; // Only to provide an interesting look
        this.FastAnimationToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold ); // Only to provide an interesting look
        this.FastAnimationToggleSwitch.OffForeColor = Color.White; // Only to provide an interesting look
        this.FastAnimationToggleSwitch.UseAnimation = true; // Default
        this.FastAnimationToggleSwitch.AnimationInterval = 1; // Default
        this.FastAnimationToggleSwitch.AnimationStep = 10; // Default
        this.SlowAnimationToggleSwitch.Style = ToggleSwitchStyle.Carbon; // Only to provide an interesting look
        this.SlowAnimationToggleSwitch.Size = new Size( 93, 30 ); // Only to provide an interesting look
        this.SlowAnimationToggleSwitch.OnText = "On"; // Only to provide an interesting look
        this.SlowAnimationToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold ); // Only to provide an interesting look
        this.SlowAnimationToggleSwitch.OnForeColor = Color.White; // Only to provide an interesting look
        this.SlowAnimationToggleSwitch.OffText = "Off"; // Only to provide an interesting look
        this.SlowAnimationToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold ); // Only to provide an interesting look
        this.SlowAnimationToggleSwitch.OffForeColor = Color.White; // Only to provide an interesting look
        this.SlowAnimationToggleSwitch.UseAnimation = true; // Default
        this.SlowAnimationToggleSwitch.AnimationInterval = 10;
        this.SlowAnimationToggleSwitch.AnimationStep = 1;

        // GrayWhenDisabled example:

        this.GrayWhenDisabledToggleSwitch1.Style = ToggleSwitchStyle.Fancy; // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch1.Size = new Size( 100, 30 ); // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch1.OnText = "ON"; // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch1.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold ); // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch1.OnForeColor = Color.White; // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch1.OffText = "OFF"; // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch1.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold ); // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch1.OffForeColor = Color.White; // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch1.ButtonImage = Demo.Properties.Resources.arrowright; // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch1.GrayWhenDisabled = false;
        this.GrayWhenDisabledToggleSwitch2.Style = ToggleSwitchStyle.Fancy; // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch2.Size = new Size( 100, 30 ); // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch2.OnText = "ON"; // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch2.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold ); // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch2.OnForeColor = Color.White; // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch2.OffText = "OFF"; // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch2.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold ); // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch2.OffForeColor = Color.White; // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch2.ButtonImage = Demo.Properties.Resources.arrowright; // Only to provide an interesting look
        this.GrayWhenDisabledToggleSwitch2.GrayWhenDisabled = true; // Default

        // ThresholdPercentage example:

        this.ThresholdPercentageToggleSwitch.Style = ToggleSwitchStyle.Ios5; // Only to provide an interesting look
        this.ThresholdPercentageToggleSwitch.Size = new Size( 98, 42 ); // Only to provide an interesting look
        this.ThresholdPercentageToggleSwitch.OnText = "ON"; // Only to provide an interesting look
        this.ThresholdPercentageToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold ); // Only to provide an interesting look
        this.ThresholdPercentageToggleSwitch.OnForeColor = Color.White; // Only to provide an interesting look
        this.ThresholdPercentageToggleSwitch.OffText = "OFF"; // Only to provide an interesting look
        this.ThresholdPercentageToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold ); // Only to provide an interesting look
        this.ThresholdPercentageToggleSwitch.OffForeColor = Color.FromArgb( 141, 123, 141 ); // Only to provide an interesting look
        this.ThresholdPercentageToggleSwitch.ThresholdPercentage = 50; // Default

        // ToggleOnButtonClick & ToggleOnSideClick example:

        this.ToggleOnClickToggleSwitch.Style = ToggleSwitchStyle.BrushedMetal; // Only to provide an interesting look
        this.ToggleOnClickToggleSwitch.Size = new Size( 93, 30 ); // Only to provide an interesting look
        this.ToggleOnClickToggleSwitch.OnText = "ON"; // Only to provide an interesting look
        this.ToggleOnClickToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold ); // Only to provide an interesting look
        this.ToggleOnClickToggleSwitch.OnForeColor = Color.White; // Only to provide an interesting look
        this.ToggleOnClickToggleSwitch.OffText = "OFF"; // Only to provide an interesting look
        this.ToggleOnClickToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold ); // Only to provide an interesting look
        this.ToggleOnClickToggleSwitch.OffForeColor = Color.White; // Only to provide an interesting look
        this.ToggleOnClickToggleSwitch.ToggleOnButtonClick = true; // Default
        this.ToggleOnClickToggleSwitch.ToggleOnSideClick = true; // Default
    }

    /// <summary>   Sets properties for customizations tab switches. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public void SetPropertiesForCustomizationsTabSwitches()
    {
        // Set the properties for the ToggleSwitches on the "Special Customizations" tab

        // Color customization example, Metro Style ToggleSwitch:

        this.NormalMetroToggleSwitch.Style = ToggleSwitchStyle.Metro; // Default
        this.NormalMetroToggleSwitch.Size = new Size( 75, 23 );
        ToggleSwitchMetroRenderer customizedMetroRenderer = new()
        {
            LeftSideColor = Color.Red,
            LeftSideColorHovered = Color.FromArgb( 210, 0, 0 ),
            LeftSideColorPressed = Color.FromArgb( 190, 0, 0 ),
            RightSideColor = Color.Yellow,
            RightSideColorHovered = Color.FromArgb( 245, 245, 0 ),
            RightSideColorPressed = Color.FromArgb( 235, 235, 0 )
        };
        this.CustomizedMetroToggleSwitch.Style = ToggleSwitchStyle.Metro; // Default
        this.CustomizedMetroToggleSwitch.Size = new Size( 75, 23 );
        this.CustomizedMetroToggleSwitch.SetRenderer( customizedMetroRenderer );

        // Color customization example, IOS5 Style ToggleSwitch:

        this.NormalIOS5ToggleSwitch.Style = ToggleSwitchStyle.Ios5;
        this.NormalIOS5ToggleSwitch.Size = new Size( 98, 42 );
        this.NormalIOS5ToggleSwitch.OnText = "ON";
        this.NormalIOS5ToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold );
        this.NormalIOS5ToggleSwitch.OnForeColor = Color.White;
        this.NormalIOS5ToggleSwitch.OffText = "OFF";
        this.NormalIOS5ToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold );
        this.NormalIOS5ToggleSwitch.OffForeColor = Color.FromArgb( 141, 123, 141 );

        // Maybe not the prettiest color scheme in the world - It's just for demonstration :-)
        ToggleSwitchIos5Renderer customizedIos5Renderer = new()
        {
            LeftSideUpperColor1 = Color.FromArgb( 128, 0, 64 ),
            LeftSideUpperColor2 = Color.FromArgb( 180, 0, 90 ),
            LeftSideLowerColor1 = Color.FromArgb( 250, 0, 125 ),
            LeftSideLowerColor2 = Color.FromArgb( 255, 120, 190 ),
            RightSideUpperColor1 = Color.FromArgb( 0, 64, 128 ),
            RightSideUpperColor2 = Color.FromArgb( 0, 90, 180 ),
            RightSideLowerColor1 = Color.FromArgb( 0, 125, 250 ),
            RightSideLowerColor2 = Color.FromArgb( 120, 190, 255 ),
            ButtonNormalOuterBorderColor = Color.Green,
            ButtonNormalInnerBorderColor = Color.Green,
            ButtonNormalSurfaceColor1 = Color.Red,
            ButtonNormalSurfaceColor2 = Color.Red,
            ButtonHoverOuterBorderColor = Color.Green,
            ButtonHoverInnerBorderColor = Color.Green,
            ButtonHoverSurfaceColor1 = Color.Red,
            ButtonHoverSurfaceColor2 = Color.Red,
            ButtonPressedOuterBorderColor = Color.Green,
            ButtonPressedInnerBorderColor = Color.Green,
            ButtonPressedSurfaceColor1 = Color.Red,
            ButtonPressedSurfaceColor2 = Color.Red
        };
        this.CustomizedIOS5ToggleSwitch.Style = ToggleSwitchStyle.Ios5;
        this.CustomizedIOS5ToggleSwitch.Size = new Size( 98, 42 );
        this.CustomizedIOS5ToggleSwitch.OnText = "ON";
        this.CustomizedIOS5ToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold );
        this.CustomizedIOS5ToggleSwitch.OnForeColor = Color.White;
        this.CustomizedIOS5ToggleSwitch.OffText = "OFF";
        this.CustomizedIOS5ToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold );
        this.CustomizedIOS5ToggleSwitch.OffForeColor = Color.White; // OBS: Need to change this for text visibility
        this.CustomizedIOS5ToggleSwitch.SetRenderer( customizedIos5Renderer );

        // Image customization example, Fancy Style ToggleSwitch:

        this.NormalFancyToggleSwitch.Style = ToggleSwitchStyle.Fancy;
        this.NormalFancyToggleSwitch.Size = new Size( 100, 30 );
        this.NormalFancyToggleSwitch.OnText = "ON";
        this.NormalFancyToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.NormalFancyToggleSwitch.OnForeColor = Color.White;
        this.NormalFancyToggleSwitch.OffText = "OFF";
        this.NormalFancyToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.NormalFancyToggleSwitch.OffForeColor = Color.White;
        this.CustomizedFancyToggleSwitch.Style = ToggleSwitchStyle.Fancy;
        this.CustomizedFancyToggleSwitch.Size = new Size( 100, 30 );
        this.CustomizedFancyToggleSwitch.OffButtonImage = Demo.Properties.Resources.arrowright;
        this.CustomizedFancyToggleSwitch.OffSideImage = Demo.Properties.Resources.cross;
        this.CustomizedFancyToggleSwitch.OnButtonImage = Demo.Properties.Resources.arrowleft;
        this.CustomizedFancyToggleSwitch.OnSideImage = Demo.Properties.Resources.check;

        // Advanced behavior example, Fancy Style ToggleSwitch:

        Color tempColor;
        ToggleSwitchFancyRenderer customizedFancyRenderer = new();
        tempColor = customizedFancyRenderer.LeftSideBackColor1;
        customizedFancyRenderer.LeftSideBackColor1 = customizedFancyRenderer.RightSideBackColor1;
        customizedFancyRenderer.RightSideBackColor1 = tempColor;
        tempColor = customizedFancyRenderer.LeftSideBackColor2;
        customizedFancyRenderer.LeftSideBackColor2 = customizedFancyRenderer.RightSideBackColor2;
        customizedFancyRenderer.RightSideBackColor2 = tempColor;
        this.AdvancedBehaviorFancyToggleSwitch.Style = ToggleSwitchStyle.Fancy;
        this.AdvancedBehaviorFancyToggleSwitch.Size = new Size( 150, 30 );
        this.AdvancedBehaviorFancyToggleSwitch.OnText = "Restart";
        this.AdvancedBehaviorFancyToggleSwitch.OnFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.AdvancedBehaviorFancyToggleSwitch.OnForeColor = Color.White;
        this.AdvancedBehaviorFancyToggleSwitch.OffText = "Online";
        this.AdvancedBehaviorFancyToggleSwitch.OffFont = new Font( this.DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold );
        this.AdvancedBehaviorFancyToggleSwitch.OffForeColor = Color.White;
        this.AdvancedBehaviorFancyToggleSwitch.OffButtonImage = Demo.Properties.Resources.arrowright;
        this.AdvancedBehaviorFancyToggleSwitch.UseAnimation = false;
        this.AdvancedBehaviorFancyToggleSwitch.SetRenderer( customizedFancyRenderer );
        this.AdvancedBehaviorFancyToggleSwitch.CheckedChanged += this.AdvancedBehaviorFancyToggleSwitch_CheckedChanged;
        this.AnimatedGifPictureBox.Parent = this.AdvancedBehaviorFancyToggleSwitch; // Necessary to get the ToggleSwitch button to show through the picture box' transparent background
    }

    /// <summary>   Allow user change check box checked changed. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void AllowUserChangeCheckBox_CheckedChanged( object? sender, EventArgs e )
    {
        this.AllowUserChangeToggleSwitch1.Checked = this.AllowUserChangeCheckBox.Checked;
        this.AllowUserChangeToggleSwitch2.Checked = this.AllowUserChangeCheckBox.Checked;
    }

    /// <summary>   Gray when disabled check box checked changed. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void GrayWhenDisabledCheckBox_CheckedChanged( object? sender, EventArgs e )
    {
        this.GrayWhenDisabledToggleSwitch1.Enabled = this.GrayWhenDisabledCheckBox.Checked;
        this.GrayWhenDisabledToggleSwitch2.Enabled = this.GrayWhenDisabledCheckBox.Checked;
    }

    /// <summary>   Threshold percentage track bar scroll. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ThresholdPercentageTrackBar_Scroll( object? sender, EventArgs e )
    {
        this.label15.Text = $"Value = {this.ThresholdPercentageTrackBar.Value} (Default = 50)";
        this.ThresholdPercentageToggleSwitch.ThresholdPercentage = this.ThresholdPercentageTrackBar.Value;
    }

    /// <summary>   Toggle on button click check box checked changed. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ToggleOnButtonClickCheckBox_CheckedChanged( object? sender, EventArgs e )
    {
        this.ToggleOnClickToggleSwitch.ToggleOnButtonClick = this.ToggleOnButtonClickCheckBox.Checked;
    }

    /// <summary>   Toggle on side click check box checked changed. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void ToggleOnSideClickCheckBox_CheckedChanged( object? sender, EventArgs e )
    {
        this.ToggleOnClickToggleSwitch.ToggleOnSideClick = this.ToggleOnSideClickCheckBox.Checked;
    }

    /// <summary>
    /// Event handler. Called by AdvancedBehaviorFancyToggleSwitch for checked changed events.
    /// </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void AdvancedBehaviorFancyToggleSwitch_CheckedChanged( object? sender, EventArgs e )
    {
        if ( this.AdvancedBehaviorFancyToggleSwitch.Checked )
        {
            this.AdvancedBehaviorFancyToggleSwitch.AllowUserChange = false;
            this.AdvancedBehaviorFancyToggleSwitch.OnText = "Restarting...";
            this.PositionAniGifPictureBox();
            this.AnimatedGifPictureBox.Visible = true;
            if ( !this.SimulateRestartBackgroundWorker.IsBusy )
            {
                this.SimulateRestartBackgroundWorker.RunWorkerAsync();
            }
        }
        else
        {
            this.AdvancedBehaviorFancyToggleSwitch.AllowUserChange = true;
            this.AdvancedBehaviorFancyToggleSwitch.OnText = "Restart";
        }
    }

    /// <summary>   Position animation GIF picture box. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    private void PositionAniGifPictureBox()
    {
        // Position Ani gif picture box

        Rectangle buttonRectangle = this.AdvancedBehaviorFancyToggleSwitch.ButtonRectangle;
        this.AnimatedGifPictureBox.Height = buttonRectangle.Height - 2;
        this.AnimatedGifPictureBox.Width = this.AnimatedGifPictureBox.Height;
        this.AnimatedGifPictureBox.Left = buttonRectangle.X + ((buttonRectangle.Width - this.AnimatedGifPictureBox.Width) / 2);
        this.AnimatedGifPictureBox.Top = buttonRectangle.Y + ((buttonRectangle.Height - this.AnimatedGifPictureBox.Height) / 2);
    }

    /// <summary>   Simulate restart background worker do work. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Do work event information. </param>
    private void SimulateRestartBackgroundWorker_DoWork( object? sender, System.ComponentModel.DoWorkEventArgs e )
    {
        // Simulate restart delay
        Thread.Sleep( 1500 );
    }

    /// <summary>   Simulate restart background worker run worker completed. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Run worker completed event information. </param>
    private void SimulateRestartBackgroundWorker_RunWorkerCompleted( object? sender, System.ComponentModel.RunWorkerCompletedEventArgs e )
    {
        this.AnimatedGifPictureBox.Visible = false;
        this.AdvancedBehaviorFancyToggleSwitch.Checked = false;
    }
}
