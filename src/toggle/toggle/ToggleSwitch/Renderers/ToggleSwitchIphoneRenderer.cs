using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using cc.isr.WinControls.ColorExtensions;

namespace cc.isr.WinControls;

/// <summary> A toggle switch iPhone renderer. </summary>
/// <remarks>
/// (c) 2015 Johnny J.. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-12-04 </para>
/// </remarks>
public class ToggleSwitchIPhoneRenderer : ToggleSwitchRendererBase, IDisposable
{
    #region " constructor"

    /// <summary> Full pathname of the inner control file. </summary>
    private GraphicsPath? _innerControlPath;

    /// <summary> Initializes a new instance of the <see cref="object" /> class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public ToggleSwitchIPhoneRenderer()
    {
        this.OuterBorderColor = Color.FromArgb( 255, 205, 205, 207 );
        this.InnerBorderColor1 = Color.FromArgb( 200, 205, 205, 207 );
        this.InnerBorderColor2 = Color.FromArgb( 200, 205, 205, 207 );
        this.LeftSideBackColor1 = Color.FromArgb( 255, 50, 101, 161 );
        this.LeftSideBackColor2 = Color.FromArgb( 255, 123, 174, 229 );
        this.RightSideBackColor1 = Color.FromArgb( 255, 161, 161, 161 );
        this.RightSideBackColor2 = Color.FromArgb( 255, 250, 250, 250 );
        this.ButtonNormalBorderColor1 = Color.FromArgb( 255, 172, 172, 172 );
        this.ButtonNormalBorderColor2 = Color.FromArgb( 255, 196, 196, 196 );
        this.ButtonNormalSurfaceColor1 = Color.FromArgb( 255, 216, 215, 216 );
        this.ButtonNormalSurfaceColor2 = Color.FromArgb( 255, 251, 250, 251 );
        this.ButtonHoverBorderColor1 = Color.FromArgb( 255, 163, 163, 163 );
        this.ButtonHoverBorderColor2 = Color.FromArgb( 255, 185, 185, 185 );
        this.ButtonHoverSurfaceColor1 = Color.FromArgb( 255, 205, 204, 205 );
        this.ButtonHoverSurfaceColor2 = Color.FromArgb( 255, 239, 238, 239 );
        this.ButtonPressedBorderColor1 = Color.FromArgb( 255, 129, 129, 129 );
        this.ButtonPressedBorderColor2 = Color.FromArgb( 255, 146, 146, 146 );
        this.ButtonPressedSurfaceColor1 = Color.FromArgb( 255, 162, 161, 162 );
        this.ButtonPressedSurfaceColor2 = Color.FromArgb( 255, 188, 187, 188 );
        this.ButtonShadowColor1 = Color.FromArgb( 50, 0, 0, 0 );
        this.ButtonShadowColor2 = Color.FromArgb( 0, 0, 0, 0 );
        this.ButtonShadowWidth = 7;
        this.CornerRadius = 6;
        this.ButtonCornerRadius = 9;
    }

    #region " disposable support "

    /// <summary> Calls <see cref="Dispose(bool)" /> to cleanup. </summary>
    /// <remarks>
    /// Do not make this method Overridable (virtual) because a derived class should not be able to
    /// override this method.
    /// </remarks>
    public void Dispose()
    {
        // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        // this disposes all child classes.
        this.Dispose( true );

        // Take this object off the finalization(Queue) and prevent finalization code 
        // from executing a second time.
        GC.SuppressFinalize( this );
    }

    /// <summary>
    /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
    /// class provided proper implementation.
    /// </summary>
    /// <value> <c>true</c> if disposed; otherwise, <c>false</c>. </value>
    protected bool IsDisposed { get; set; }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="System.Windows.Forms.Control" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    [System.Diagnostics.DebuggerNonUserCode()]
    protected virtual void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this._innerControlPath?.Dispose();
                this._innerControlPath = null;
            }
        }
        finally
        {
            this.IsDisposed = true;
        }
    }

    #endregion

    #endregion ' Constructor

    #region " public properties"

    /// <summary> Gets or sets the color of the outer border. </summary>
    /// <value> The color of the outer border. </value>
    public Color OuterBorderColor { get; set; }

    /// <summary> Gets or sets the inner border color 1. </summary>
    /// <value> The inner border color 1. </value>
    public Color InnerBorderColor1 { get; set; }

    /// <summary> Gets or sets the inner border color 2. </summary>
    /// <value> The inner border color 2. </value>
    public Color InnerBorderColor2 { get; set; }

    /// <summary> Gets or sets the left side back color 1. </summary>
    /// <value> The left side back color 1. </value>
    public Color LeftSideBackColor1 { get; set; }

    /// <summary> Gets or sets the left side back color 2. </summary>
    /// <value> The left side back color 2. </value>
    public Color LeftSideBackColor2 { get; set; }

    /// <summary> Gets or sets the right side back color 1. </summary>
    /// <value> The right side back color 1. </value>
    public Color RightSideBackColor1 { get; set; }

    /// <summary> Gets or sets the right side back color 2. </summary>
    /// <value> The right side back color 2. </value>
    public Color RightSideBackColor2 { get; set; }

    /// <summary> Gets or sets the button normal border color 1. </summary>
    /// <value> The button normal border color 1. </value>
    public Color ButtonNormalBorderColor1 { get; set; }

    /// <summary> Gets or sets the button normal border color 2. </summary>
    /// <value> The button normal border color 2. </value>
    public Color ButtonNormalBorderColor2 { get; set; }

    /// <summary> Gets or sets the button normal surface color 1. </summary>
    /// <value> The button normal surface color 1. </value>
    public Color ButtonNormalSurfaceColor1 { get; set; }

    /// <summary> Gets or sets the button normal surface color 2. </summary>
    /// <value> The button normal surface color 2. </value>
    public Color ButtonNormalSurfaceColor2 { get; set; }

    /// <summary> Gets or sets the button hover border color 1. </summary>
    /// <value> The button hover border color 1. </value>
    public Color ButtonHoverBorderColor1 { get; set; }

    /// <summary> Gets or sets the button hover border color 2. </summary>
    /// <value> The button hover border color 2. </value>
    public Color ButtonHoverBorderColor2 { get; set; }

    /// <summary> Gets or sets the button hover surface color 1. </summary>
    /// <value> The button hover surface color 1. </value>
    public Color ButtonHoverSurfaceColor1 { get; set; }

    /// <summary> Gets or sets the button hover surface color 2. </summary>
    /// <value> The button hover surface color 2. </value>
    public Color ButtonHoverSurfaceColor2 { get; set; }

    /// <summary> Gets or sets the button pressed border color 1. </summary>
    /// <value> The button pressed border color 1. </value>
    public Color ButtonPressedBorderColor1 { get; set; }

    /// <summary> Gets or sets the button pressed border color 2. </summary>
    /// <value> The button pressed border color 2. </value>
    public Color ButtonPressedBorderColor2 { get; set; }

    /// <summary> Gets or sets the button pressed surface color 1. </summary>
    /// <value> The button pressed surface color 1. </value>
    public Color ButtonPressedSurfaceColor1 { get; set; }

    /// <summary> Gets or sets the button pressed surface color 2. </summary>
    /// <value> The button pressed surface color 2. </value>
    public Color ButtonPressedSurfaceColor2 { get; set; }

    /// <summary> Gets or sets the button shadow color 1. </summary>
    /// <value> The button shadow color 1. </value>
    public Color ButtonShadowColor1 { get; set; }

    /// <summary> Gets or sets the button shadow color 2. </summary>
    /// <value> The button shadow color 2. </value>
    public Color ButtonShadowColor2 { get; set; }

    /// <summary> Gets or sets the width of the button shadow. </summary>
    /// <value> The width of the button shadow. </value>
    public int ButtonShadowWidth { get; set; }

    /// <summary> Gets or sets the corner radius. </summary>
    /// <value> The corner radius. </value>
    public int CornerRadius { get; set; }

    /// <summary> Gets or sets the button corner radius. </summary>
    /// <value> The button corner radius. </value>
    public int ButtonCornerRadius { get; set; }

    #endregion ' Public Properties

    #region " render method implementations"

    /// <summary> Renders the border. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="g">               The Graphics to process. </param>
    /// <param name="borderRectangle"> The border rectangle. </param>
    public override void RenderBorder( Graphics g, Rectangle borderRectangle )
    {
        if ( g is null ) return;

        g.SmoothingMode = SmoothingMode.HighQuality;
        g.PixelOffsetMode = PixelOffsetMode.HighQuality;
        g.InterpolationMode = InterpolationMode.HighQualityBilinear;

        // Draw outer border
        using ( GraphicsPath outerBorderPath = this.GetRoundedRectanglePath( borderRectangle, this.CornerRadius ) )
        {
            g.SetClip( outerBorderPath );

            // INSTANT VB NOTE: The variable outerBorderColor was renamed since Visual Basic does not handle local variables named the same as class members well:
            Color outerBorderColor_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.OuterBorderColor.ToGrayscale() : this.OuterBorderColor;
            using ( Brush outerBorderBrush = new SolidBrush( outerBorderColor_Renamed ) )
            {
                g.FillPath( outerBorderBrush, outerBorderPath );
            }

            g.ResetClip();
        }

        // Draw inner border
        Rectangle innerBorderRectangle = new( borderRectangle.X + 1, borderRectangle.Y + 1, borderRectangle.Width - 2, borderRectangle.Height - 2 );
        using ( GraphicsPath innerBorderPath = this.GetRoundedRectanglePath( innerBorderRectangle, this.CornerRadius ) )
        {
            g.SetClip( innerBorderPath );
            Color borderColor1 = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.InnerBorderColor1.ToGrayscale() : this.InnerBorderColor1;
            Color borderColor2 = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.InnerBorderColor2.ToGrayscale() : this.InnerBorderColor2;
            using ( Brush borderBrush = new LinearGradientBrush( borderRectangle, borderColor1, borderColor2, LinearGradientMode.Vertical ) )
            {
                g.FillPath( borderBrush, innerBorderPath );
            }

            g.ResetClip();
        }

        Rectangle backgroundRectangle = new( borderRectangle.X + 2, borderRectangle.Y + 2, borderRectangle.Width - 4, borderRectangle.Height - 4 );
        this._innerControlPath = this.GetRoundedRectanglePath( backgroundRectangle, this.CornerRadius );
    }

    /// <summary> Renders the left toggle field. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="g">                     The Graphics to process. </param>
    /// <param name="leftRectangle">         The left rectangle. </param>
    /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    public override void RenderLeftToggleField( Graphics g, Rectangle leftRectangle, int totalToggleFieldWidth )
    {
        if ( g is null ) return;

        g.SmoothingMode = SmoothingMode.HighQuality;
        g.PixelOffsetMode = PixelOffsetMode.HighQuality;
        g.InterpolationMode = InterpolationMode.HighQualityBilinear;
        int buttonWidth = this.GetButtonWidth();

        // Draw inner background
        int gradientRectWidth = leftRectangle.Width + (buttonWidth / 2);
        Rectangle gradientRectangle = new( leftRectangle.X, leftRectangle.Y, gradientRectWidth, leftRectangle.Height );

        // INSTANT VB NOTE: The variable leftSideBackColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Color leftSideBackColor1_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.LeftSideBackColor1.ToGrayscale() : this.LeftSideBackColor1;
        // INSTANT VB NOTE: The variable leftSideBackColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Color leftSideBackColor2_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.LeftSideBackColor2.ToGrayscale() : this.LeftSideBackColor2;
        if ( this._innerControlPath is not null )
        {
            g.SetClip( this._innerControlPath );
            g.IntersectClip( gradientRectangle );
        }
        else
        {
            g.SetClip( gradientRectangle );
        }

        using ( Brush backgroundBrush = new LinearGradientBrush( gradientRectangle, leftSideBackColor1_Renamed, leftSideBackColor2_Renamed, LinearGradientMode.Vertical ) )
        {
            g.FillRectangle( backgroundBrush, gradientRectangle );
        }

        g.ResetClip();
        Rectangle leftShadowRectangle = new()
        {
            X = leftRectangle.X + leftRectangle.Width - this.ButtonShadowWidth,
            Y = leftRectangle.Y,
            Width = this.ButtonShadowWidth + this.CornerRadius,
            Height = leftRectangle.Height
        };
        if ( this._innerControlPath is not null )
        {
            g.SetClip( this._innerControlPath );
            g.IntersectClip( leftShadowRectangle );
        }
        else
        {
            g.SetClip( leftShadowRectangle );
        }

        using ( Brush buttonShadowBrush = new LinearGradientBrush( leftShadowRectangle, this.ButtonShadowColor2, this.ButtonShadowColor1, LinearGradientMode.Horizontal ) )
        {
            g.FillRectangle( buttonShadowBrush, leftShadowRectangle );
        }

        g.ResetClip();

        // Draw image or text
        if ( this.ToggleSwitch.OnSideImage is not null || !string.IsNullOrEmpty( this.ToggleSwitch.OnText ) )
        {
            RectangleF fullRectangle = new( leftRectangle.X + 1 - (totalToggleFieldWidth - leftRectangle.Width), 1f, totalToggleFieldWidth - 1, this.ToggleSwitch.Height - 2 );
            if ( this._innerControlPath is not null )
            {
                g.SetClip( this._innerControlPath );
                g.IntersectClip( fullRectangle );
            }
            else
            {
                g.SetClip( fullRectangle );
            }

            if ( this.ToggleSwitch.OnSideImage is not null )
            {
                Size imageSize = this.ToggleSwitch.OnSideImage.Size;
                Rectangle imageRectangle;
                int imageXPos = ( int ) Math.Truncate( fullRectangle.X );
                if ( this.ToggleSwitch.OnSideScaleImageToFit )
                {
                    Size canvasSize = new( ( int ) fullRectangle.Width, ( int ) fullRectangle.Height );
                    Size resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                    if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                    {
                        imageXPos = ( int ) Math.Truncate( fullRectangle.X + ((fullRectangle.Width - resizedImageSize.Width) / 2f) );
                    }
                    else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                    {
                        imageXPos = ( int ) Math.Truncate( fullRectangle.X + fullRectangle.Width - resizedImageSize.Width );
                    }

                    imageRectangle = new Rectangle( imageXPos, ( int ) Math.Truncate( fullRectangle.Y + ((fullRectangle.Height - resizedImageSize.Height) / 2f) ), resizedImageSize.Width, resizedImageSize.Height );
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                    }
                    else
                    {
                        g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle );
                    }
                }
                else
                {
                    if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                    {
                        imageXPos = ( int ) Math.Truncate( fullRectangle.X + ((fullRectangle.Width - imageSize.Width) / 2f) );
                    }
                    else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                    {
                        imageXPos = ( int ) Math.Truncate( fullRectangle.X + fullRectangle.Width - imageSize.Width );
                    }

                    imageRectangle = new Rectangle( imageXPos, ( int ) Math.Truncate( fullRectangle.Y + ((fullRectangle.Height - imageSize.Height) / 2f) ), imageSize.Width, imageSize.Height );
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                    }
                    else
                    {
                        g.DrawImageUnscaled( this.ToggleSwitch.OnSideImage, imageRectangle );
                    }
                }
            }
            else if ( !string.IsNullOrEmpty( this.ToggleSwitch.OnText ) )
            {
                SizeF textSize = g.MeasureString( this.ToggleSwitch.OnText, this.ToggleSwitch.OnFont );
                float textXPos = fullRectangle.X;
                if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                {
                    textXPos = fullRectangle.X + ((fullRectangle.Width - textSize.Width) / 2f);
                }
                else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                {
                    textXPos = fullRectangle.X + fullRectangle.Width - textSize.Width;
                }

                RectangleF textRectangle = new( textXPos, fullRectangle.Y + ((fullRectangle.Height - textSize.Height) / 2f), textSize.Width, textSize.Height );
                Color textForeColor = this.ToggleSwitch.OnForeColor;
                if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                {
                    textForeColor = textForeColor.ToGrayscale();
                }

                using Brush textBrush = new SolidBrush( textForeColor );
                g.DrawString( this.ToggleSwitch.OnText, this.ToggleSwitch.OnFont, textBrush, textRectangle );
            }

            g.ResetClip();
        }
    }

    /// <summary> Renders the right toggle field. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="g">                     The Graphics to process. </param>
    /// <param name="rightRectangle">        The right rectangle. </param>
    /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    public override void RenderRightToggleField( Graphics g, Rectangle rightRectangle, int totalToggleFieldWidth )
    {
        if ( g is null ) return;

        g.SmoothingMode = SmoothingMode.HighQuality;
        g.PixelOffsetMode = PixelOffsetMode.HighQuality;
        g.InterpolationMode = InterpolationMode.HighQualityBilinear;
        int buttonWidth = this.GetButtonWidth();

        // Draw inner background
        int gradientRectWidth = rightRectangle.Width + (buttonWidth / 2);
        Rectangle gradientRectangle = new( rightRectangle.X - (buttonWidth / 2), rightRectangle.Y, gradientRectWidth, rightRectangle.Height );

        // INSTANT VB NOTE: The variable rightSideBackColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Color rightSideBackColor1_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.RightSideBackColor1.ToGrayscale() : this.RightSideBackColor1;
        // INSTANT VB NOTE: The variable rightSideBackColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Color rightSideBackColor2_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.RightSideBackColor2.ToGrayscale() : this.RightSideBackColor2;
        if ( this._innerControlPath is not null )
        {
            g.SetClip( this._innerControlPath );
            g.IntersectClip( gradientRectangle );
        }
        else
        {
            g.SetClip( gradientRectangle );
        }

        using ( Brush backgroundBrush = new LinearGradientBrush( gradientRectangle, rightSideBackColor1_Renamed, rightSideBackColor2_Renamed, LinearGradientMode.Vertical ) )
        {
            g.FillRectangle( backgroundBrush, gradientRectangle );
        }

        g.ResetClip();
        Rectangle rightShadowRectangle = new()
        {
            X = rightRectangle.X - this.CornerRadius,
            Y = rightRectangle.Y,
            Width = this.ButtonShadowWidth + this.CornerRadius,
            Height = rightRectangle.Height
        };
        if ( this._innerControlPath is not null )
        {
            g.SetClip( this._innerControlPath );
            g.IntersectClip( rightShadowRectangle );
        }
        else
        {
            g.SetClip( rightShadowRectangle );
        }

        using ( Brush buttonShadowBrush = new LinearGradientBrush( rightShadowRectangle, this.ButtonShadowColor1, this.ButtonShadowColor2, LinearGradientMode.Horizontal ) )
        {
            g.FillRectangle( buttonShadowBrush, rightShadowRectangle );
        }

        g.ResetClip();

        // Draw image or text
        if ( this.ToggleSwitch.OffSideImage is not null || !string.IsNullOrEmpty( this.ToggleSwitch.OffText ) )
        {
            RectangleF fullRectangle = new( rightRectangle.X, 1f, totalToggleFieldWidth - 1, this.ToggleSwitch.Height - 2 );
            if ( this._innerControlPath is not null )
            {
                g.SetClip( this._innerControlPath );
                g.IntersectClip( fullRectangle );
            }
            else
            {
                g.SetClip( fullRectangle );
            }

            if ( this.ToggleSwitch.OffSideImage is not null )
            {
                Size imageSize = this.ToggleSwitch.OffSideImage.Size;
                Rectangle imageRectangle;
                int imageXPos = ( int ) Math.Truncate( fullRectangle.X );
                if ( this.ToggleSwitch.OffSideScaleImageToFit )
                {
                    Size canvasSize = new( ( int ) fullRectangle.Width, ( int ) fullRectangle.Height );
                    Size resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                    if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                    {
                        imageXPos = ( int ) Math.Truncate( fullRectangle.X + ((fullRectangle.Width - resizedImageSize.Width) / 2f) );
                    }
                    else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                    {
                        imageXPos = ( int ) Math.Truncate( fullRectangle.X + fullRectangle.Width - resizedImageSize.Width );
                    }

                    imageRectangle = new Rectangle( imageXPos, ( int ) Math.Truncate( fullRectangle.Y + ((fullRectangle.Height - resizedImageSize.Height) / 2f) ), resizedImageSize.Width, resizedImageSize.Height );
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                    }
                    else
                    {
                        g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle );
                    }
                }
                else
                {
                    if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                    {
                        imageXPos = ( int ) Math.Truncate( fullRectangle.X + ((fullRectangle.Width - imageSize.Width) / 2f) );
                    }
                    else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                    {
                        imageXPos = ( int ) Math.Truncate( fullRectangle.X + fullRectangle.Width - imageSize.Width );
                    }

                    imageRectangle = new Rectangle( imageXPos, ( int ) Math.Truncate( fullRectangle.Y + ((fullRectangle.Height - imageSize.Height) / 2f) ), imageSize.Width, imageSize.Height );
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                    }
                    else
                    {
                        g.DrawImageUnscaled( this.ToggleSwitch.OffSideImage, imageRectangle );
                    }
                }
            }
            else if ( !string.IsNullOrEmpty( this.ToggleSwitch.OffText ) )
            {
                SizeF textSize = g.MeasureString( this.ToggleSwitch.OffText, this.ToggleSwitch.OffFont );
                float textXPos = fullRectangle.X;
                if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                {
                    textXPos = fullRectangle.X + ((fullRectangle.Width - textSize.Width) / 2f);
                }
                else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                {
                    textXPos = fullRectangle.X + fullRectangle.Width - textSize.Width;
                }

                RectangleF textRectangle = new( textXPos, fullRectangle.Y + ((fullRectangle.Height - textSize.Height) / 2f), textSize.Width, textSize.Height );
                Color textForeColor = this.ToggleSwitch.OffForeColor;
                if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                {
                    textForeColor = textForeColor.ToGrayscale();
                }

                using Brush textBrush = new SolidBrush( textForeColor );
                g.DrawString( this.ToggleSwitch.OffText, this.ToggleSwitch.OffFont, textBrush, textRectangle );
            }

            g.ResetClip();
        }
    }

    /// <summary> Renders the button. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="g">               The Graphics to process. </param>
    /// <param name="buttonRectangle"> The button rectangle. </param>
    public override void RenderButton( Graphics g, Rectangle buttonRectangle )
    {
        if ( g is null ) return;

        g.SmoothingMode = SmoothingMode.HighQuality;
        g.PixelOffsetMode = PixelOffsetMode.HighQuality;
        g.InterpolationMode = InterpolationMode.HighQualityBilinear;
        using GraphicsPath buttonPath = this.GetRoundedRectanglePath( buttonRectangle, this.ButtonCornerRadius );
        if ( this._innerControlPath is not null )
        {
            g.SetClip( this._innerControlPath );
            g.IntersectClip( buttonRectangle );
        }
        else
        {
            g.SetClip( buttonRectangle );
        }

        // Draw button surface
        Color buttonSurfaceColor1 = this.ButtonNormalSurfaceColor1;
        Color buttonSurfaceColor2 = this.ButtonNormalSurfaceColor2;
        if ( this.ToggleSwitch.IsButtonPressed )
        {
            buttonSurfaceColor1 = this.ButtonPressedSurfaceColor1;
            buttonSurfaceColor2 = this.ButtonPressedSurfaceColor2;
        }
        else if ( this.ToggleSwitch.IsButtonHovered )
        {
            buttonSurfaceColor1 = this.ButtonHoverSurfaceColor1;
            buttonSurfaceColor2 = this.ButtonHoverSurfaceColor2;
        }

        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
        {
            buttonSurfaceColor1 = buttonSurfaceColor1.ToGrayscale();
            buttonSurfaceColor2 = buttonSurfaceColor2.ToGrayscale();
        }

        using ( Brush buttonSurfaceBrush = new LinearGradientBrush( buttonRectangle, buttonSurfaceColor1, buttonSurfaceColor2, LinearGradientMode.Vertical ) )
        {
            g.FillPath( buttonSurfaceBrush, buttonPath );
        }

        // Draw button border
        Color buttonBorderColor1 = this.ButtonNormalBorderColor1;
        Color buttonBorderColor2 = this.ButtonNormalBorderColor2;
        if ( this.ToggleSwitch.IsButtonPressed )
        {
            buttonBorderColor1 = this.ButtonPressedBorderColor1;
            buttonBorderColor2 = this.ButtonPressedBorderColor2;
        }
        else if ( this.ToggleSwitch.IsButtonHovered )
        {
            buttonBorderColor1 = this.ButtonHoverBorderColor1;
            buttonBorderColor2 = this.ButtonHoverBorderColor2;
        }

        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
        {
            buttonBorderColor1 = buttonBorderColor1.ToGrayscale();
            buttonBorderColor2 = buttonBorderColor2.ToGrayscale();
        }

        using ( Brush buttonBorderBrush = new LinearGradientBrush( buttonRectangle, buttonBorderColor1, buttonBorderColor2, LinearGradientMode.Vertical ) )
        {
            using Pen buttonBorderPen = new( buttonBorderBrush );
            g.DrawPath( buttonBorderPen, buttonPath );
        }

        g.ResetClip();

        // Draw button image
        Image? buttonImage = this.ToggleSwitch.ButtonImage ?? (this.ToggleSwitch.Checked ? this.ToggleSwitch.OnButtonImage : this.ToggleSwitch.OffButtonImage);
        if ( buttonImage is not null )
        {
            g.SetClip( buttonPath );
            ToggleSwitchButtonAlignment alignment = this.ToggleSwitch.ButtonImage is not null ? this.ToggleSwitch.ButtonAlignment : this.ToggleSwitch.Checked ? this.ToggleSwitch.OnButtonAlignment : this.ToggleSwitch.OffButtonAlignment;
            Size imageSize = buttonImage.Size;
            Rectangle imageRectangle;
            int imageXPos = buttonRectangle.X;
            bool scaleImage = this.ToggleSwitch.ButtonImage is not null ? this.ToggleSwitch.ButtonScaleImageToFit : this.ToggleSwitch.Checked ? this.ToggleSwitch.OnButtonScaleImageToFit : this.ToggleSwitch.OffButtonScaleImageToFit;
            if ( scaleImage )
            {
                Size canvasSize = buttonRectangle.Size;
                Size resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                if ( alignment == ToggleSwitchButtonAlignment.Center )
                {
                    imageXPos = ( int ) Math.Truncate( buttonRectangle.X + ((buttonRectangle.Width - ( float ) resizedImageSize.Width) / 2f) );
                }
                else if ( alignment == ToggleSwitchButtonAlignment.Right )
                {
                    imageXPos = ( int ) Math.Truncate( buttonRectangle.X + ( float ) buttonRectangle.Width - resizedImageSize.Width );
                }

                imageRectangle = new Rectangle( imageXPos, ( int ) Math.Truncate( buttonRectangle.Y + ((buttonRectangle.Height - ( float ) resizedImageSize.Height) / 2f) ), resizedImageSize.Width, resizedImageSize.Height );
                if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                {
                    g.DrawImage( buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                }
                else
                {
                    g.DrawImage( buttonImage, imageRectangle );
                }
            }
            else
            {
                if ( alignment == ToggleSwitchButtonAlignment.Center )
                {
                    imageXPos = ( int ) Math.Truncate( buttonRectangle.X + ((buttonRectangle.Width - ( float ) imageSize.Width) / 2f) );
                }
                else if ( alignment == ToggleSwitchButtonAlignment.Right )
                {
                    imageXPos = ( int ) Math.Truncate( buttonRectangle.X + ( float ) buttonRectangle.Width - imageSize.Width );
                }

                imageRectangle = new Rectangle( imageXPos, ( int ) Math.Truncate( buttonRectangle.Y + ((buttonRectangle.Height - ( float ) imageSize.Height) / 2f) ), imageSize.Width, imageSize.Height );
                if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                {
                    g.DrawImage( buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                }
                else
                {
                    g.DrawImageUnscaled( buttonImage, imageRectangle );
                }
            }

            g.ResetClip();
        }
    }

    #endregion ' Render Method Implementations

    #region " helper method implementations"

    /// <summary> Gets rounded rectangle path. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="rectangle"> The rectangle. </param>
    /// <param name="radius">    The radius. </param>
    /// <returns> The rounded rectangle path. </returns>
    public GraphicsPath GetRoundedRectanglePath( Rectangle rectangle, int radius )
    {
        GraphicsPath gp = new();
        int diameter = 2 * radius;
        if ( diameter > this.ToggleSwitch.Height )
        {
            diameter = this.ToggleSwitch.Height;
        }

        if ( diameter > this.ToggleSwitch.Width )
        {
            diameter = this.ToggleSwitch.Width;
        }

        gp.AddArc( rectangle.X, rectangle.Y, diameter, diameter, 180f, 90f );
        gp.AddArc( rectangle.X + rectangle.Width - diameter, rectangle.Y, diameter, diameter, 270f, 90f );
        gp.AddArc( rectangle.X + rectangle.Width - diameter, rectangle.Y + rectangle.Height - diameter, diameter, diameter, 0f, 90f );
        gp.AddArc( rectangle.X, rectangle.Y + rectangle.Height - diameter, diameter, diameter, 90f, 90f );
        gp.CloseFigure();
        return gp;
    }

    /// <summary> Gets button width. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The button width. </returns>
    public override int GetButtonWidth()
    {
        float buttonWidth = 1.57f * this.ToggleSwitch.Height;
        return ( int ) Math.Truncate( buttonWidth );
    }

    /// <summary> Gets button rectangle. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The button rectangle. </returns>
    public override Rectangle GetButtonRectangle()
    {
        int buttonWidth = this.GetButtonWidth();
        return this.GetButtonRectangle( buttonWidth );
    }

    /// <summary> Gets button rectangle. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="buttonWidth"> Width of the button. </param>
    /// <returns> The button rectangle. </returns>
    public override Rectangle GetButtonRectangle( int buttonWidth )
    {
        Rectangle buttonRect = new( this.ToggleSwitch.ButtonValue, 0, buttonWidth, this.ToggleSwitch.Height );
        return buttonRect;
    }

    #endregion ' Helper Method Implementations
}
