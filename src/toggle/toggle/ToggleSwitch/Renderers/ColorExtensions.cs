using System;

namespace cc.isr.WinControls.ColorExtensions;

/// <summary> Color extension Methods. </summary>
/// <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para> </remarks>
internal static class ColorExtensionMethods
{
    /// <summary> Converts a <see cref="System.Drawing.Color"/> to a gray scale. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="originalColor"> The original color. </param>
    /// <returns> originalColor as a Drawing.Color. </returns>
    public static System.Drawing.Color ToGrayscale( this System.Drawing.Color originalColor )
    {
        if ( originalColor.Equals( System.Drawing.Color.Transparent ) )
        {
            return originalColor;
        }

        int grayscale = ( int ) Math.Truncate( (originalColor.R * 0.299d) + (originalColor.G * 0.587d) + (originalColor.B * 0.114d) );
        return System.Drawing.Color.FromArgb( grayscale, grayscale, grayscale );
    }
}
