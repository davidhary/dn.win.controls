using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A toggle switch. </summary>
/// <remarks>
/// (c) 2015 Johnny J.. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-09-11 from Johnny J </para><para>
/// http://www.codeproject.com/Articles/1029499/ToggleSwitch-Winforms-Control. </para>
/// </remarks>
[DefaultValue( "Checked" )]
[DefaultEvent( "CheckedChanged" )]
[ToolboxBitmap( typeof( CheckBox ) )]
public class ToggleSwitch : Control
{
    #region " construction "

    /// <summary>
    /// Initializes a new instance of the <see cref="Control" /> class with
    /// default settings.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public ToggleSwitch() : base()
    {
        this.SetStyle( ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.DoubleBuffer, true );
        this._onFont = base.Font;
        this._offFont = base.Font;
        this.SetRendererThis( new ToggleSwitchMetroRenderer() );
        this._animationTimer.Enabled = false;
        this._animationTimer.Interval = this._animationInterval;
        this._animationTimer.Tick += this.AnimationTimer_Tick;
        this.SetRenderer( new ToggleSwitchMetroRenderer() );
        this._onButtonImage = new Bitmap( 16, 16 );
        this._onSideImage = new Bitmap( 16, 16 );
        this._offButtonImage = new Bitmap( 16, 16 );
        this._offSideImage = new Bitmap( 16, 16 );
    }

    /// <summary> Sets renderer this. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="renderer"> The renderer. </param>
    [MemberNotNull( nameof( _renderer ) )]
    private void SetRendererThis( ToggleSwitchRendererBase renderer )
    {
        renderer?.SetToggleSwitch( this );
        this._renderer = renderer ?? new ToggleSwitchMetroRenderer();
        this.Refresh();
    }

    /// <summary> Sets a renderer. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="renderer"> The renderer. </param>
    public void SetRenderer( ToggleSwitchRendererBase renderer )
    {
        this.SetRendererThis( renderer );
    }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="Control" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    [DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.RemoveCheckedChangedEventHandler( CheckedChanged );
                this._offFont?.Dispose();
                this._onFont?.Dispose();
                this._animationTimer?.Dispose();
                this._buttonImage?.Dispose();
                this._offButtonImage?.Dispose();
                this._offSideImage?.Dispose();
                this._onButtonImage?.Dispose();
                this._onSideImage?.Dispose();
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion ' Constructor Etc.

    #region " public properties "

    /// <summary> Gets or sets the style of the ToggleSwitch. </summary>
    /// <value> The style. </value>
    [Bindable( false )]
    [DefaultValue( typeof( ToggleSwitchStyle ), "Metro" )]
    [Category( "Appearance" )]
    [Description( "Gets or sets the style of the ToggleSwitch" )]
    public ToggleSwitchStyle Style
    {
        get => this._style;
        set
        {
            if ( value != this._style )
            {
                this._style = value;
                switch ( this._style )
                {
                    case ToggleSwitchStyle.Metro:
                        {
                            this.SetRenderer( new ToggleSwitchMetroRenderer() );
                            break;
                        }

                    case ToggleSwitchStyle.Android:
                        {
                            this.SetRenderer( new ToggleSwitchAndroidRenderer() );
                            break;
                        }

                    case ToggleSwitchStyle.Ios5:
                        {
                            this.SetRenderer( new ToggleSwitchIos5Renderer() );
                            break;
                        }

                    case ToggleSwitchStyle.BrushedMetal:
                        {
                            this.SetRenderer( new ToggleSwitchBrushedMetalRenderer() );
                            break;
                        }

                    case ToggleSwitchStyle.OS10:
                        {
                            this.SetRenderer( new ToggleSwitchOS10Renderer() );
                            break;
                        }

                    case ToggleSwitchStyle.Carbon:
                        {
                            this.SetRenderer( new ToggleSwitchCarbonRenderer() );
                            break;
                        }

                    case ToggleSwitchStyle.IPhone:
                        {
                            this.SetRenderer( new ToggleSwitchIPhoneRenderer() );
                            break;
                        }

                    case ToggleSwitchStyle.Fancy:
                        {
                            this.SetRenderer( new ToggleSwitchFancyRenderer() );
                            break;
                        }

                    case ToggleSwitchStyle.Modern:
                        {
                            this.SetRenderer( new ToggleSwitchModernRenderer() );
                            break;
                        }

                    default:
                        break;
                }
            }

            this.Refresh();
        }
    }

    /// <summary> True if checked. </summary>
    private bool _checked;

    /// <summary> Gets or sets the Checked value of the ToggleSwitch. </summary>
    /// <value> The checked. </value>
    [Bindable( true )]
    [DefaultValue( false )]
    [Category( "Data" )]
    [Description( "Gets or sets the Checked value of the ToggleSwitch" )]
    public bool Checked
    {
        get => this._checked;
        set
        {
            if ( value != this._checked )
            {
                while ( this._animating )
                {
                    Application.DoEvents();
                }

                if ( value )
                {
                    int buttonWidth = this._renderer.GetButtonWidth();
                    this._animationTarget = this.Width - buttonWidth;
                    this.BeginAnimation( true );
                }
                else
                {
                    this._animationTarget = 0;
                    this.BeginAnimation( false );
                }
            }
        }
    }

    /// <summary>
    /// Gets or sets whether the user can change the value of the button or not.
    /// </summary>
    /// <value> The allow user change. </value>
    [Bindable( true )]
    [DefaultValue( true )]
    [Category( "Behavior" )]
    [Description( "Gets or sets whether the user can change the value of the button or not" )]
    public bool AllowUserChange { get; set; } = true;

    /// <summary> Gets the checked string. </summary>
    /// <value> The checked string. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string CheckedString => this.Checked ? string.IsNullOrEmpty( this.OnText ) ? "ON" : this.OnText : string.IsNullOrEmpty( this.OffText ) ? "OFF" : this.OffText;

    /// <summary> Gets the button rectangle. </summary>
    /// <value> The button rectangle. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Rectangle ButtonRectangle => this._renderer.GetButtonRectangle();

    /// <summary> Gets or sets if the ToggleSwitch should be grayed out when disabled. </summary>
    /// <value> The gray when disabled. </value>
    [Bindable( false )]
    [DefaultValue( true )]
    [Category( "Appearance" )]
    [Description( "Gets or sets if the ToggleSwitch should be grayed out when disabled" )]
    public bool GrayWhenDisabled
    {
        get => this._grayWhenDisabled;
        set
        {
            if ( value != this._grayWhenDisabled )
            {
                this._grayWhenDisabled = value;
                if ( !this.Enabled )
                {
                    this.Refresh();
                }
            }
        }
    }

    /// <summary> Gets or sets if the ToggleSwitch should toggle when the button is clicked. </summary>
    /// <value> The toggle on button click. </value>
    [Bindable( false )]
    [DefaultValue( true )]
    [Category( "Behavior" )]
    [Description( "Gets or sets if the ToggleSwitch should toggle when the button is clicked" )]
    public bool ToggleOnButtonClick { get; set; } = true;

    /// <summary>
    /// Gets or sets if the ToggleSwitch should toggle when the track besides the button is clicked.
    /// </summary>
    /// <value> The toggle on side click. </value>
    [Bindable( false )]
    [DefaultValue( true )]
    [Category( "Behavior" )]
    [Description( "Gets or sets if the ToggleSwitch should toggle when the track besides the button is clicked" )]
    public bool ToggleOnSideClick { get; set; } = true;

    /// <summary>
    /// Gets or sets how much the button need to be on the other side (in percent) before it snaps.
    /// </summary>
    /// <value> The threshold percentage. </value>
    [Bindable( false )]
    [DefaultValue( 50 )]
    [Category( "Behavior" )]
    [Description( "Gets or sets how much the button need to be on the other side (in percent) before it snaps" )]
    public int ThresholdPercentage { get; set; } = 50;

    /// <summary> Gets or sets the fore-color of the text when Checked=false. </summary>
    /// <value> The color of the off foreground. </value>
    [Bindable( false )]
    [DefaultValue( typeof( Color ), "Black" )]
    [Category( "Appearance" )]
    [Description( "Gets or sets the fore-color of the text when Checked=false" )]
    public Color OffForeColor
    {
        get => this._offForeColor;
        set
        {
            if ( value != this._offForeColor )
            {
                this._offForeColor = value;
                this.Refresh();
            }
        }
    }

    /// <summary> Gets or sets the font of the text when Checked=false. </summary>
    /// <value> The off font. </value>
    [Bindable( false )]
    [DefaultValue( typeof( Font ), "Microsoft Sans Serif, 8.25pt" )]
    [Category( "Appearance" )]
    [Description( "Gets or sets the font of the text when Checked=false" )]
    public Font OffFont
    {
        get => this._offFont;
        set
        {
            if ( !Equals( value, this.OffFont ) )
            {
                this._offFont = value;
                this.Refresh();
            }
        }
    }

    /// <summary> Gets or sets the text when Checked=true. </summary>
    /// <value> The off text. </value>
    [Bindable( false )]
    [DefaultValue( "" )]
    [Category( "Appearance" )]
    [Description( "Gets or sets the text when Checked=true" )]
    public string OffText
    {
        get => this._offText;
        set
        {
            if ( !string.Equals( value, this._offText, StringComparison.Ordinal ) )
            {
                this._offText = value;
                this.Refresh();
            }
        }
    }

    /// <summary>
    /// Gets or sets the side image when Checked=false - Note: Settings the OffSideImage overrules
    /// the OffText property. Only the image will be shown.
    /// </summary>
    /// <value> The off side image. </value>
    [Bindable( false )]
    [Category( "Appearance" )]
    [Description( "Gets or sets the side image when Checked=false - Note: Settings the OffSideImage overrules the OffText property. Only the image will be shown" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image OffSideImage
    {
        get => this._offSideImage;
        set
        {
            if ( !Equals( value, this.OffSideImage ) )
            {
                this._offSideImage = value;
                this.Refresh();
            }
        }
    }

    /// <summary>
    /// Gets or sets whether the side image visible when Checked=false should be scaled to fit.
    /// </summary>
    /// <value> The off side scale image to fit. </value>
    [Bindable( false )]
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Gets or sets whether the side image visible when Checked=false should be scaled to fit" )]
    public bool OffSideScaleImageToFit
    {
        get => this._offSideScaleImage;
        set
        {
            if ( value != this._offSideScaleImage )
            {
                this._offSideScaleImage = value;
                this.Refresh();
            }
        }
    }

    /// <summary>
    /// Gets or sets how the text or side image visible when Checked=false should be aligned.
    /// </summary>
    /// <value> The off side alignment. </value>
    [Bindable( false )]
    [DefaultValue( typeof( ToggleSwitchAlignment ), "Center" )]
    [Category( "Appearance" )]
    [Description( "Gets or sets how the text or side image visible when Checked=false should be aligned" )]
    public ToggleSwitchAlignment OffSideAlignment
    {
        get => this._offSideAlignment;
        set
        {
            if ( value != this._offSideAlignment )
            {
                this._offSideAlignment = value;
                this.Refresh();
            }
        }
    }

    /// <summary>
    /// Gets or sets the button image when Checked=false and ButtonImage is not set.
    /// </summary>
    /// <value> The off button image. </value>
    [Bindable( false )]
    [Category( "Appearance" )]
    [Description( "Gets or sets the button image when Checked=false and ButtonImage is not set" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image OffButtonImage
    {
        get => this._offButtonImage;
        set
        {
            if ( !Equals( value, this.OffButtonImage ) )
            {
                this._offButtonImage = value;
                this.Refresh();
            }
        }
    }

    /// <summary>
    /// Gets or sets whether the button image visible when Checked=false should be scaled to fit.
    /// </summary>
    /// <value> The off button scale image to fit. </value>
    [Bindable( false )]
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Gets or sets whether the button image visible when Checked=false should be scaled to fit" )]
    public bool OffButtonScaleImageToFit
    {
        get => this._offButtonScaleImage;
        set
        {
            if ( value != this._offButtonScaleImage )
            {
                this._offButtonScaleImage = value;
                this.Refresh();
            }
        }
    }

    /// <summary>
    /// Gets or sets how the button image visible when Checked=false should be aligned.
    /// </summary>
    /// <value> The off button alignment. </value>
    [Bindable( false )]
    [DefaultValue( typeof( ToggleSwitchButtonAlignment ), "Center" )]
    [Category( "Appearance" )]
    [Description( "Gets or sets how the button image visible when Checked=false should be aligned" )]
    public ToggleSwitchButtonAlignment OffButtonAlignment
    {
        get => this._offButtonAlignment;
        set
        {
            if ( value != this._offButtonAlignment )
            {
                this._offButtonAlignment = value;
                this.Refresh();
            }
        }
    }

    /// <summary> Gets or sets the fore-color of the text when Checked=true. </summary>
    /// <value> The color of the on foreground. </value>
    [Bindable( false )]
    [DefaultValue( typeof( Color ), "Black" )]
    [Category( "Appearance" )]
    [Description( "Gets or sets the fore-color of the text when Checked=true" )]
    public Color OnForeColor
    {
        get => this._onForeColor;
        set
        {
            if ( value != this._onForeColor )
            {
                this._onForeColor = value;
                this.Refresh();
            }
        }
    }

    /// <summary> Gets or sets the font of the text when Checked=true. </summary>
    /// <value> The on font. </value>
    [Bindable( false )]
    [DefaultValue( typeof( Font ), "Microsoft Sans Serif, 8,25pt" )]
    [Category( "Appearance" )]
    [Description( "Gets or sets the font of the text when Checked=true" )]
    public Font OnFont
    {
        get => this._onFont;
        set
        {
            if ( !Equals( value, this.OnFont ) )
            {
                this._onFont = value;
                this.Refresh();
            }
        }
    }

    /// <summary> Gets or sets the text when Checked=true. </summary>
    /// <value> The on text. </value>
    [Bindable( false )]
    [DefaultValue( "" )]
    [Category( "Appearance" )]
    [Description( "Gets or sets the text when Checked=true" )]
    public string OnText
    {
        get => this._onText;
        set
        {
            if ( !string.Equals( value, this.OnText, StringComparison.Ordinal ) )
            {
                this._onText = value;
                this.Refresh();
            }
        }
    }

    /// <summary>
    /// Gets or sets the side image visible when Checked=true - Note: Settings the OnSideImage
    /// overrules the OnText property. Only the image will be shown.
    /// </summary>
    /// <value> The on side image. </value>
    [Bindable( false )]
    [Category( "Appearance" )]
    [Description( "Gets or sets the side image visible when Checked=true - Note: Settings the OnSideImage overrules the OnText property. Only the image will be shown." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image OnSideImage
    {
        get => this._onSideImage;
        set
        {
            if ( !ReferenceEquals( value, this._onSideImage ) )
            {
                this._onSideImage = value;
                this.Refresh();
            }
        }
    }

    /// <summary>
    /// Gets or sets whether the side image visible when Checked=true should be scaled to fit.
    /// </summary>
    /// <value> The on side scale image to fit. </value>
    [Bindable( false )]
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Gets or sets whether the side image visible when Checked=true should be scaled to fit" )]
    public bool OnSideScaleImageToFit
    {
        get => this._onSideScaleImage;
        set
        {
            if ( value != this._onSideScaleImage )
            {
                this._onSideScaleImage = value;
                this.Refresh();
            }
        }
    }

    private Image _buttonImage = new Bitmap( 16, 16 );

    /// <summary> Gets or sets the button image. </summary>
    /// <value> The button image. </value>
    [Bindable( false )]
    [Category( "Appearance" )]
    [Description( "Gets or sets the button image" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image ButtonImage
    {
        get => this._buttonImage;
        set
        {
            if ( !ReferenceEquals( value, this._buttonImage ) )
            {
                this._buttonImage = value;
                this.Refresh();
            }
        }
    }

    /// <summary> Gets or sets whether the button image should be scaled to fit. </summary>
    /// <value> The button scale image to fit. </value>
    [Bindable( false )]
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Gets or sets whether the button image should be scaled to fit" )]
    public bool ButtonScaleImageToFit
    {
        get => this._buttonScaleImage;
        set
        {
            if ( value != this._buttonScaleImage )
            {
                this._buttonScaleImage = value;
                this.Refresh();
            }
        }
    }

    /// <summary> Gets or sets how the button image should be aligned. </summary>
    /// <value> The button alignment. </value>
    [Bindable( false )]
    [DefaultValue( typeof( ToggleSwitchButtonAlignment ), "Center" )]
    [Category( "Appearance" )]
    [Description( "Gets or sets how the button image should be aligned" )]
    public ToggleSwitchButtonAlignment ButtonAlignment
    {
        get => this._buttonAlignment;
        set
        {
            if ( value != this._buttonAlignment )
            {
                this._buttonAlignment = value;
                this.Refresh();
            }
        }
    }

    /// <summary>
    /// Gets or sets how the text or side image visible when Checked=true should be aligned.
    /// </summary>
    /// <value> The on side alignment. </value>
    [Bindable( false )]
    [DefaultValue( typeof( ToggleSwitchAlignment ), "Center" )]
    [Category( "Appearance" )]
    [Description( "Gets or sets how the text or side image visible when Checked=true should be aligned" )]
    public ToggleSwitchAlignment OnSideAlignment
    {
        get => this._onSideAlignment;
        set
        {
            if ( value != this._onSideAlignment )
            {
                this._onSideAlignment = value;
                this.Refresh();
            }
        }
    }

    /// <summary>
    /// Gets or sets the button image visible when Checked=true and ButtonImage is not set.
    /// </summary>
    /// <value> The on button image. </value>
    [Bindable( false )]
    [Category( "Appearance" )]
    [Description( "Gets or sets the button image visible when Checked=true and ButtonImage is not set" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Image OnButtonImage
    {
        get => this._onButtonImage;
        set
        {
            if ( !ReferenceEquals( value, this._onButtonImage ) )
            {
                this._onButtonImage = value;
                this.Refresh();
            }
        }
    }

    /// <summary>
    /// Gets or sets whether the button image visible when Checked=true should be scaled to fit.
    /// </summary>
    /// <value> The on button scale image to fit. </value>
    [Bindable( false )]
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Gets or sets whether the button image visible when Checked=true should be scaled to fit" )]
    public bool OnButtonScaleImageToFit
    {
        get => this._onButtonScaleImage;
        set
        {
            if ( value != this._onButtonScaleImage )
            {
                this._onButtonScaleImage = value;
                this.Refresh();
            }
        }
    }

    /// <summary>
    /// Gets or sets how the button image visible when Checked=true should be aligned.
    /// </summary>
    /// <value> The on button alignment. </value>
    [Bindable( false )]
    [DefaultValue( typeof( ToggleSwitchButtonAlignment ), "Center" )]
    [Category( "Appearance" )]
    [Description( "Gets or sets how the button image visible when Checked=true should be aligned" )]
    public ToggleSwitchButtonAlignment OnButtonAlignment
    {
        get => this._onButtonAlignment;
        set
        {
            if ( value != this._onButtonAlignment )
            {
                this._onButtonAlignment = value;
                this.Refresh();
            }
        }
    }

    /// <summary> Gets or sets whether the toggle change should be animated or not. </summary>
    /// <value> The use animation. </value>
    [Bindable( false )]
    [DefaultValue( true )]
    [Category( "Behavior" )]
    [Description( "Gets or sets whether the toggle change should be animated or not" )]
    public bool UseAnimation { get; set; } = true;

    /// <summary> The animation interval. </summary>
    private int _animationInterval = 1;

    /// <summary> Gets or sets the interval in ms between animation frames. </summary>
    /// <value> The animation interval. </value>
    [Bindable( false )]
    [DefaultValue( 1 )]
    [Category( "Behavior" )]
    [Description( "Gets or sets the interval in ms between animation frames" )]
    public int AnimationInterval
    {
        get => this._animationInterval;
        set
        {
            if ( value != this.AnimationInterval && value > 0 )
            {
                this._animationInterval = value;
            }
        }
    }

    /// <summary> The animation step. </summary>
    private int _animationStep = 10;

    /// <summary>
    /// Gets or sets the step in pixels the button should be moved between each animation interval.
    /// </summary>
    /// <value> The animation step. </value>
    [Bindable( false )]
    [DefaultValue( 10 )]
    [Category( "Behavior" )]
    [Description( "Gets or sets the step in pixels the button should be moved between each animation interval" )]
    public int AnimationStep
    {
        get => this._animationStep;
        set
        {
            if ( value != this.AnimationStep && value > 0 )
            {
                this._animationStep = value;
            }
        }
    }

    #region " hidden base properties"

    /// <summary> Gets or sets the text. </summary>
    /// <value> The text. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new string Text
    {
        get => string.Empty;
        set => base.Text = string.Empty;
    }

    /// <summary> Gets or sets the color of the foreground. </summary>
    /// <value> The color of the foreground. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new Color ForeColor
    {
        get => Color.Black;
        set => base.ForeColor = Color.Black;
    }

    /// <summary> Gets or sets the font. </summary>
    /// <value> The font. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public new Font Font
    {
        get => base.Font;
        set => base.Font = new Font( base.Font, FontStyle.Regular );
    }

    #endregion ' Hidden Base Properties

    #endregion ' Public Properties

    #region " internal properties"

    /// <summary> True if is button hovered, false if not. </summary>
    private bool _isButtonHovered;

    /// <summary> Gets the is button hovered. </summary>
    /// <value> The is button hovered. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal bool IsButtonHovered => this._isButtonHovered && !this.IsButtonPressed;

    /// <summary> Gets the is button pressed. </summary>
    /// <value> The is button pressed. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal bool IsButtonPressed { get; private set; } = false;

    /// <summary> True if is left field hovered, false if not. </summary>
    private bool _isLeftFieldHovered;

    /// <summary> Gets the is left side hovered. </summary>
    /// <value> The is left side hovered. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal bool IsLeftSideHovered => this._isLeftFieldHovered && !this.IsLeftSidePressed;

    /// <summary> Gets the is left side pressed. </summary>
    /// <value> The is left side pressed. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal bool IsLeftSidePressed { get; private set; } = false;

    /// <summary> True if is right field hovered, false if not. </summary>
    private bool _isRightFieldHovered;

    /// <summary> Gets the is right side hovered. </summary>
    /// <value> The is right side hovered. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal bool IsRightSideHovered => this._isRightFieldHovered && !this.IsRightSidePressed;

    /// <summary> Gets the is right side pressed. </summary>
    /// <value> The is right side pressed. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal bool IsRightSidePressed { get; private set; } = false;

    /// <summary> The button value. </summary>
    private int _buttonValue;

    /// <summary> Gets the button value. </summary>
    /// <value> The button value. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal int ButtonValue
    {
        get => this._animating || this._moving ? this._buttonValue : this._checked ? this.Width - this._renderer.GetButtonWidth() : 0;
        set
        {
            if ( value != this._buttonValue )
            {
                this._buttonValue = value;
                this.Refresh();
            }
        }
    }

    /// <summary> Gets the is button left side. </summary>
    /// <value> The is button on left side. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal bool IsButtonOnLeftSide => this.ButtonValue <= 0;

    /// <summary> Gets the is button right side. </summary>
    /// <value> The is button on right side. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal bool IsButtonOnRightSide => this.ButtonValue >= this.Width - this._renderer.GetButtonWidth();

    /// <summary> Gets the is button moving left. </summary>
    /// <value> The is button moving left. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal bool IsButtonMovingLeft => this._animating && !this.IsButtonOnLeftSide && !this.AnimationResult;

    /// <summary> Gets the is button moving right. </summary>
    /// <value> The is button moving right. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal bool IsButtonMovingRight => this._animating && !this.IsButtonOnRightSide && this.AnimationResult;

    /// <summary> Gets the animation result. </summary>
    /// <value> The animation result. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal bool AnimationResult { get; private set; } = false;

    #endregion ' Private Properties

    #region " overridden control methods"

    /// <summary> Gets the default size of the control. </summary>
    /// <value> The default <see cref="Size" /> of the control. </value>
    protected override Size DefaultSize => new( 50, 19 );

    /// <summary> Paints the background of the control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="pevent"> A <see cref="PaintEventArgs" /> that contains
    /// information about the control to paint. </param>
    protected override void OnPaintBackground( PaintEventArgs pevent )
    {
        if ( pevent is null ) return;

        pevent.Graphics.ResetClip();
        base.OnPaintBackground( pevent );
        this._renderer?.RenderBackground( pevent );
    }

    /// <summary> Raises the <see cref="Control.Paint" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="PaintEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnPaint( PaintEventArgs e )
    {
        if ( e is null ) return;

        e.Graphics.ResetClip();
        base.OnPaint( e );
        this._renderer?.RenderControl( e );
    }

    /// <summary> Raises the <see cref="Control.MouseMove" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseMove( MouseEventArgs e )
    {
        if ( e is null ) return;

        this._lastMouseEventArgs = e;
        int buttonWidth = this._renderer.GetButtonWidth();
        Rectangle rectangle = this._renderer.GetButtonRectangle( buttonWidth );
        if ( this._moving )
        {
            int val = this._xValue + (e.Location.X - this._xOffset);
            if ( val < 0 )
            {
                val = 0;
            }

            if ( val > this.Width - buttonWidth )
            {
                val = this.Width - buttonWidth;
            }

            this.ButtonValue = val;
            this.Refresh();
            return;
        }

        if ( rectangle.Contains( e.Location ) )
        {
            this._isButtonHovered = true;
            this._isLeftFieldHovered = false;
            this._isRightFieldHovered = false;
        }
        else if ( e.Location.X > rectangle.X + rectangle.Width )
        {
            this._isButtonHovered = false;
            this._isLeftFieldHovered = false;
            this._isRightFieldHovered = true;
        }
        else if ( e.Location.X < rectangle.X )
        {
            this._isButtonHovered = false;
            this._isLeftFieldHovered = true;
            this._isRightFieldHovered = false;
        }

        this.Refresh();
    }

    /// <summary> Raises the <see cref="Control.MouseDown" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseDown( MouseEventArgs e )
    {
        if ( e is null ) return;

        if ( this._animating || !this.AllowUserChange ) return;

        int buttonWidth = this._renderer.GetButtonWidth();
        // INSTANT VB NOTE: The variable buttonRectangle was renamed since Visual Basic does not handle local variables named the same as class members well:
        Rectangle buttonRectangle_Renamed = this._renderer.GetButtonRectangle( buttonWidth );
        this._savedButtonValue = this.ButtonValue;
        if ( buttonRectangle_Renamed.Contains( e.Location ) )
        {
            this.IsButtonPressed = true;
            this.IsLeftSidePressed = false;
            this.IsRightSidePressed = false;
            this._moving = true;
            this._xOffset = e.Location.X;
            this._buttonValue = buttonRectangle_Renamed.X;
            this._xValue = this.ButtonValue;
        }
        else if ( e.Location.X > buttonRectangle_Renamed.X + buttonRectangle_Renamed.Width )
        {
            this.IsButtonPressed = false;
            this.IsLeftSidePressed = false;
            this.IsRightSidePressed = true;
        }
        else if ( e.Location.X < buttonRectangle_Renamed.X )
        {
            this.IsButtonPressed = false;
            this.IsLeftSidePressed = true;
            this.IsRightSidePressed = false;
        }

        this.Refresh();
    }

    /// <summary> Raises the <see cref="Control.MouseUp" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseUp( MouseEventArgs e )
    {
        if ( this._animating || !this.AllowUserChange ) return;

        int buttonWidth = this._renderer.GetButtonWidth();
        bool wasLeftSidePressed = this.IsLeftSidePressed;
        bool wasRightSidePressed = this.IsRightSidePressed;
        this.IsButtonPressed = false;
        this.IsLeftSidePressed = false;
        this.IsRightSidePressed = false;
        if ( this._moving )
        {
            int percentage = ( int ) Math.Truncate( 100d * this.ButtonValue / (this.Width - ( double ) buttonWidth) );
            if ( this._checked )
            {
                if ( percentage <= 100 - this.ThresholdPercentage )
                {
                    this._animationTarget = 0;
                    this.BeginAnimation( false );
                }
                else if ( this.ToggleOnButtonClick && this._savedButtonValue == this.ButtonValue )
                {
                    this._animationTarget = 0;
                    this.BeginAnimation( false );
                }
                else
                {
                    this._animationTarget = this.Width - buttonWidth;
                    this.BeginAnimation( true );
                }
            }
            else if ( percentage >= this.ThresholdPercentage )
            {
                this._animationTarget = this.Width - buttonWidth;
                this.BeginAnimation( true );
            }
            else if ( this.ToggleOnButtonClick && this._savedButtonValue == this.ButtonValue )
            {
                this._animationTarget = this.Width - buttonWidth;
                this.BeginAnimation( true );
            }
            else
            {
                this._animationTarget = 0;
                this.BeginAnimation( false );
            }

            this._moving = false;
            return;
        }

        if ( this.IsButtonOnRightSide )
        {
            this._buttonValue = this.Width - buttonWidth;
            this._animationTarget = 0;
        }
        else
        {
            this._buttonValue = 0;
            this._animationTarget = this.Width - buttonWidth;
        }

        if ( wasLeftSidePressed && this.ToggleOnSideClick )
        {
            this.SetValueInternal( false );
        }
        else if ( wasRightSidePressed && this.ToggleOnSideClick )
        {
            this.SetValueInternal( true );
        }

        this.Refresh();
    }

    /// <summary> Raises the <see cref="Control.MouseLeave" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnMouseLeave( EventArgs e )
    {
        this._isButtonHovered = false;
        this._isLeftFieldHovered = false;
        this._isRightFieldHovered = false;
        this.IsButtonPressed = false;
        this.IsLeftSidePressed = false;
        this.IsRightSidePressed = false;
        this.Refresh();
    }

    /// <summary>
    /// Raises the <see cref="Control.EnabledChanged" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnEnabledChanged( EventArgs e )
    {
        base.OnEnabledChanged( e );
        this.Refresh();
    }

    /// <summary>
    /// Raises the <see cref="Control.RegionChanged" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnRegionChanged( EventArgs e )
    {
        base.OnRegionChanged( e );
        this.Refresh();
    }

    /// <summary>
    /// Raises the <see cref="Control.SizeChanged" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnSizeChanged( EventArgs e )
    {
        if ( this._animationTarget > 0 )
        {
            int buttonWidth = this._renderer.GetButtonWidth();
            this._animationTarget = this.Width - buttonWidth;
        }

        base.OnSizeChanged( e );
    }

    #endregion ' Overridden Control Methods

    #region " private methods "

    /// <summary> Sets value internal. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="checkedValue"> True to checked value. </param>
    private void SetValueInternal( bool checkedValue )
    {
        if ( checkedValue == this._checked ) return;

        while ( this._animating )
        {
            Application.DoEvents();
        }

        this.BeginAnimation( checkedValue );
    }

    /// <summary> Begins an animation. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="checkedValue"> True to checked value. </param>
    private void BeginAnimation( bool checkedValue )
    {
        this._animating = true;
        this.AnimationResult = checkedValue;
        if ( this._animationTimer is not null && this.UseAnimation )
        {
            this._animationTimer.Interval = this._animationInterval;
            this._animationTimer.Enabled = true;
        }
        else
        {
            this.AnimationComplete();
        }
    }

    /// <summary> Event handler. Called by AnimationTimer for tick events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void AnimationTimer_Tick( object? sender, EventArgs e )
    {
        this._animationTimer.Enabled = false;
        int newButtonValue;
        bool animationDone;
        if ( this.IsButtonMovingRight )
        {
            newButtonValue = this.ButtonValue + this._animationStep;
            if ( newButtonValue > this._animationTarget )
            {
                newButtonValue = this._animationTarget;
            }

            this.ButtonValue = newButtonValue;
            animationDone = this.ButtonValue >= this._animationTarget;
        }
        else
        {
            newButtonValue = this.ButtonValue - this._animationStep;
            if ( newButtonValue < this._animationTarget )
            {
                newButtonValue = this._animationTarget;
            }

            this.ButtonValue = newButtonValue;
            animationDone = this.ButtonValue <= this._animationTarget;
        }

        if ( animationDone )
        {
            this.AnimationComplete();
        }
        else
        {
            this._animationTimer.Enabled = true;
        }
    }

    /// <summary> Animation complete. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void AnimationComplete()
    {
        this._animating = false;
        this._moving = false;
        this._checked = this.AnimationResult;
        this._isButtonHovered = false;
        this.IsButtonPressed = false;
        this._isLeftFieldHovered = false;
        this.IsLeftSidePressed = false;
        this._isRightFieldHovered = false;
        this.IsRightSidePressed = false;
        this.Refresh();
        CheckedChanged?.Invoke( this, new EventArgs() );
        if ( this._lastMouseEventArgs is not null )
        {
            this.OnMouseMove( this._lastMouseEventArgs );
        }

        this._lastMouseEventArgs = null;
    }

    #endregion ' Private Methods

    #region " events "

    /// <summary> Event queue for all listeners interested in CheckedChanged events. </summary>
    [Description( "Raised when the ToggleSwitch has changed state" )]
    public event EventHandler<EventArgs>? CheckedChanged;

    /// <summary> Removes event handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The handler. </param>
    private void RemoveCheckedChangedEventHandler( EventHandler<EventArgs>? value )
    {
        foreach ( Delegate d in value is null ? [] : value.GetInvocationList() )
        {
            try
            {
                CheckedChanged -= ( EventHandler<EventArgs> ) d;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }
    }

    #endregion

    #region " private members"

    /// <summary> The animation timer. </summary>
    private readonly Timer _animationTimer = new();

    /// <summary> The renderer. </summary>
    private ToggleSwitchRendererBase _renderer;

    /// <summary> The style. </summary>
    private ToggleSwitchStyle _style = ToggleSwitchStyle.Metro;

    /// <summary> True to moving. </summary>
    private bool _moving;

    /// <summary> True to animating. </summary>
    private bool _animating;

    /// <summary> The animation target. </summary>
    private int _animationTarget;

    /// <summary> The saved button value. </summary>
    private int _savedButtonValue;

    /// <summary> The offset. </summary>
    private int _xOffset;

    /// <summary> The value. </summary>
    private int _xValue;

    /// <summary> True to disable, false to enable the gray when. </summary>
    private bool _grayWhenDisabled = true;

    /// <summary> Mouse event information. </summary>
    private MouseEventArgs? _lastMouseEventArgs;

    /// <summary> True to button scale image. </summary>
    private bool _buttonScaleImage;

    /// <summary> The button alignment. </summary>
    private ToggleSwitchButtonAlignment _buttonAlignment = ToggleSwitchButtonAlignment.Center;

    /// <summary> The off text. </summary>
    private string _offText = string.Empty;

    /// <summary> The off foreground color. </summary>
    private Color _offForeColor = Color.Black;

    /// <summary> The off font. </summary>
    private Font _offFont;

    /// <summary> The off side image. </summary>
    private Image _offSideImage;

    /// <summary> True to off side scale image. </summary>
    private bool _offSideScaleImage;

    /// <summary> The off side alignment. </summary>
    private ToggleSwitchAlignment _offSideAlignment = ToggleSwitchAlignment.Center;

    /// <summary> The off button image. </summary>
    private Image _offButtonImage;

    /// <summary> True to off button scale image. </summary>
    private bool _offButtonScaleImage;

    /// <summary> The off button alignment. </summary>
    private ToggleSwitchButtonAlignment _offButtonAlignment = ToggleSwitchButtonAlignment.Center;

    /// <summary> The on text. </summary>
    private string _onText = string.Empty;

    /// <summary> The on foreground color. </summary>
    private Color _onForeColor = Color.Black;

    /// <summary> The on font. </summary>
    private Font _onFont;

    /// <summary> The on side image. </summary>
    private Image _onSideImage;

    /// <summary> True to on side scale image. </summary>
    private bool _onSideScaleImage;

    /// <summary> The on side alignment. </summary>
    private ToggleSwitchAlignment _onSideAlignment = ToggleSwitchAlignment.Center;

    /// <summary> The on button image. </summary>
    private Image _onButtonImage;

    /// <summary> True to on button scale image. </summary>
    private bool _onButtonScaleImage;

    /// <summary> The on button alignment. </summary>
    private ToggleSwitchButtonAlignment _onButtonAlignment = ToggleSwitchButtonAlignment.Center;

    #endregion ' Private Members

}

#region " enums"
/// <summary> Values that represent toggle switch styles. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum ToggleSwitchStyle
{
    /// <summary> An enum constant representing the metro option. </summary>
    Metro,

    /// <summary> An enum constant representing the android option. </summary>
    Android,

    /// <summary> An enum constant representing the ios 5 option. </summary>
    Ios5,

    /// <summary> An enum constant representing the brushed metal option. </summary>
    BrushedMetal,

    /// <summary> An enum constant representing the Operating system 10 option. </summary>
    OS10,

    /// <summary> An enum constant representing the carbon option. </summary>
    Carbon,

    /// <summary> An enum constant representing the iPhone option. </summary>
    IPhone,

    /// <summary> An enum constant representing the fancy option. </summary>
    Fancy,

    /// <summary> An enum constant representing the modern option. </summary>
    Modern
}
/// <summary> Values that represent toggle switch alignments. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum ToggleSwitchAlignment
{
    /// <summary> An enum constant representing the near option. </summary>
    Near,

    /// <summary> An enum constant representing the center option. </summary>
    Center,

    /// <summary> An enum constant representing the far option. </summary>
    Far
}
/// <summary> Values that represent toggle switch button alignments. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public enum ToggleSwitchButtonAlignment
{
    /// <summary> An enum constant representing the left option. </summary>
    Left,

    /// <summary> An enum constant representing the center option. </summary>
    Center,

    /// <summary> An enum constant representing the right option. </summary>
    Right
}

#endregion ' Enums

