using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace cc.isr.WinControls;

/// <summary> An image helper. </summary>
/// <remarks> David, 2020-09-24. </remarks>
public sealed class ImageHelper
{
    /// <summary> Initializes a new instance of the <see cref="object" /> class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private ImageHelper()
    {
    }

    /// <summary> The color matrix elements. </summary>
    private static readonly float[][] _colorMatrixElements = [ [0.299f, 0.299f, 0.299f, 0f, 0f],
                                                                            [0.587f, 0.587f, 0.587f, 0f, 0f],
                                                                            [0.114f, 0.114f, 0.114f, 0f, 0f],
                                                                            [0f, 0f, 0f, 1f, 0f],
                                                                            [0f, 0f, 0f, 0f, 1f] ];

    /// <summary> The gray scale color matrix. </summary>
    private static readonly ColorMatrix _grayscaleColorMatrix = new( _colorMatrixElements );

    /// <summary> Gets gray scale attributes. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The gray scale attributes. </returns>
    public static ImageAttributes GetGrayscaleAttributes()
    {
        ImageAttributes attr = new();
        attr.SetColorMatrix( _grayscaleColorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap );
        return attr;
    }

    /// <summary> Rescale image to fit. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <param name="imageSize">  Size of the image. </param>
    /// <param name="canvasSize"> Size of the canvas. </param>
    /// <returns> A Size. </returns>
    public static Size RescaleImageToFit( Size imageSize, Size canvasSize )
    {
        // Code "borrowed" from http://StackOverflow.com/questions/1940581/c-sharp-image-resizing-to-different-size-while-preserving-aspect-ratio
        // and the Math.Min improvement from http://StackOverflow.com/questions/6501797/resize-image-proportionally-with-maxheight-and-maxwidth-constraints

        // Figure out the ratio
        double ratioX = canvasSize.Width / ( double ) imageSize.Width;
        double ratioY = canvasSize.Height / ( double ) imageSize.Height;

        // use whichever multiplier is smaller
        double ratio = Math.Min( ratioX, ratioY );

        // now we can get the new height and width
        int newHeight = Convert.ToInt32( imageSize.Height * ratio );
        int newWidth = Convert.ToInt32( imageSize.Width * ratio );
        Size resizedSize = new( newWidth, newHeight );
        return resizedSize.Width > canvasSize.Width || resizedSize.Height > canvasSize.Height
            ? throw new InvalidOperationException( "Rescale image to fit - Resize failed!" )
            : resizedSize;
    }
}
