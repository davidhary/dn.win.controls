using System;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace cc.isr.WinControls.RichTextBoxExtensions;

public static partial class RichTextBoxExtensionMethods
{
    #region " printout functions "

    private static RichTextBox? _richTextBox;
    private static int _textLength;
    private static PrintDocument? _printDocument;
    private static int _currentPage, _fromPage, _toPage;
    private static int[]? _pageIndexes;
    private static int _pageCount;

    // private static read only Font CharFont = null;

    /// <summary> Hundredth inch to Twips. </summary>
    /// <param name="n"> An Integer to process. </param>
    /// <returns> An Int32. </returns>
    private static int HundredthInchToTwips( int n )
    {
        // convert units
        return Convert.ToInt32( n * 144 / 10 );
    }

    /// <summary> Gets page indexes.
    /// Determines indexes for page beginnings, based on printer info</summary>
    private static void GetPageIndexes()
    {
        if ( _richTextBox is null || _printDocument is null ) { return; }
        _pageCount = 0;
        int firstCharOnPage = 0;
        do
        {
            // store index for start of current page
            Array.Resize( ref _pageIndexes, _pageCount + 1 );
            _pageIndexes[_pageCount] = firstCharOnPage;
            // measure current page
            firstCharOnPage = RichTextBoxExtensionMethods.SafeNativeMethods.FormatRange( _richTextBox.Handle, _printDocument.PrinterSettings.CreateMeasurementGraphics(), _printDocument.DefaultPageSettings, firstCharOnPage, _textLength, false );
            // prepare for next page
            _pageCount += 1;
        }
        while ( firstCharOnPage < _textLength );
    }

    /// <summary> Gets page number.
    /// Searches for page containing a given caret Position</summary>
    /// <param name="position"> The position. </param>
    /// <returns> The page number. </returns>
    private static int GetPageNumber( int position )
    {
        if ( _pageIndexes is null ) { return 0; }
        int pageNumber = Array.BinarySearch( _pageIndexes, position );
        if ( pageNumber < 0 )
        {
            pageNumber ^= -1; // caret is inside of page
        }
        else
        {
            pageNumber += 1;
        }                  // caret is at beginning of page

        return pageNumber;
    }

    /// <summary>   Sets an up. Prepare for print job. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="richTextBox">      The rich text format control. </param>
    /// <param name="printDocument">    Instance of PrintDocument. </param>
    /// <param name="pageIndexesOnly">  True to page indexes only. </param>
    private static void SetUp( RichTextBox richTextBox, PrintDocument printDocument, bool pageIndexesOnly )
    {
        // prepare for print job
        _richTextBox = richTextBox;
        _textLength = richTextBox.TextLength;
        _printDocument = printDocument;
        GetPageIndexes();
        if ( pageIndexesOnly )
        {
            return; // leave with page indexes
        }
        // else prepare to preview/print
        {
            PrintDocument withBlock = _printDocument;
            // wire up events
            // (RemoveHandler is used before AddHandler to guard against double-firing
            // of events in the event this routine is called multiple times)
            withBlock.BeginPrint -= BeginPrint; // remove any pre-existing handler
            withBlock.BeginPrint += BeginPrint;    // add a new handler
            withBlock.PrintPage -= PrintPage;
            withBlock.PrintPage += PrintPage;
            withBlock.EndPrint -= EndPrint;
            withBlock.EndPrint += EndPrint;
            // determine which pages to print/preview
            {
                PrinterSettings withBlock1 = withBlock.PrinterSettings;
                switch ( withBlock1.PrintRange )
                {
                    case PrintRange.AllPages:
                        {
                            // all pages
                            _fromPage = 1;
                            _toPage = _pageCount;
                            break;
                        }

                    case PrintRange.SomePages:
                        {
                            // range of pages
                            _fromPage = withBlock1.FromPage - withBlock1.MinimumPage + 1;
                            _toPage = withBlock1.ToPage - withBlock1.MinimumPage + 1;
                            break;
                        }

                    case PrintRange.Selection:
                        {
                            // pages of selected text
                            _fromPage = GetPageNumber( _richTextBox.SelectionStart );
                            if ( _richTextBox.SelectionLength == 0 )
                            {
                                _toPage = _fromPage; // no selection
                            }
                            else
                            {
                                _toPage = GetPageNumber( _richTextBox.SelectionStart + _richTextBox.SelectionLength - 1 );
                            }

                            break;
                        }

                    case PrintRange.CurrentPage:
                        break;
                    default:
                        {
                            // page at caret position
                            _fromPage = GetPageNumber( _richTextBox.SelectionStart );
                            _toPage = _fromPage;
                            break;
                        }
                }
                // validate page range
                if ( _fromPage < 1 )
                {
                    _fromPage = 1; // page #'s are 1-based
                }
                else if ( _fromPage > _pageCount )
                {
                    _fromPage = _pageCount;
                }

                if ( _toPage < _fromPage )
                {
                    _toPage = _fromPage; // at least 1 page
                }
                else if ( _toPage > _pageCount )
                {
                    _toPage = _pageCount;
                }
            }
        }
    }

    #endregion

    #region " print document event procedures "

    /// <summary> Begins a print. </summary>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Print event information. </param>
    private static void BeginPrint( object? sender, PrintEventArgs e )
    {
        // prepare to start printing   
        _currentPage = _fromPage;
    }

    /// <summary> Print page. </summary>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Print page event information. </param>
    private static void PrintPage( object? sender, PrintPageEventArgs e )
    {
        if ( _richTextBox is null || _pageIndexes is null || e is null || e.Graphics is null ) return;

        // print current page
        int firstCharOnNextPage = RichTextBoxExtensionMethods.SafeNativeMethods.FormatRange( _richTextBox.Handle, e.Graphics, e.PageSettings, _pageIndexes[_currentPage - 1], _textLength, true );
        // prepare for next page; is it already the last page?
        _currentPage += 1;
        e.HasMorePages = _currentPage <= _toPage && firstCharOnNextPage < _textLength;
    }

    /// <summary> Ends a print. </summary>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Print event information. </param>
    private static void EndPrint( object? sender, PrintEventArgs e )
    {
        if ( _richTextBox is null ) { return; }
        // finish printing
        RichTextBoxExtensionMethods.SafeNativeMethods.FormatRangeDone( _richTextBox.Handle );
    }

    #endregion

    #region " print functions "

    /// <summary>
    /// Width of left-side "selection margin" for highlighting whole lines
    /// when a RichTextBox's ShowSelection property is True
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Naming", "CA1707:Identifiers should not contain underscores",
        Justification = "visual studio does not read the editor config file correctly for some projects or classes." )]
    public const float SELECTION_MARGIN = 8.0f;

    /// <summary>   Print RichTextBox contents or a range of pages thereof. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="richTextBox">      The rich text format control. </param>
    /// <param name="printDocument">    Instance of PrintDocument. </param>
    public static void Print( this RichTextBox richTextBox, PrintDocument printDocument )
    {
        // print document
        SetUp( richTextBox, printDocument, false );
        printDocument.Print();
    }

    /// <summary>   Preview RichTextBox contents or a range of pages thereof to be printed. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="richTextBox">          The rich text format control. </param>
    /// <param name="printPreviewDialog">   Instance of PrintPreviewDialog. </param>
    /// <returns>   Result of Print Preview dialog. </returns>
    public static DialogResult PrintPreview( this RichTextBox richTextBox, PrintPreviewDialog printPreviewDialog )
    {
        if ( richTextBox is null || printPreviewDialog is null || printPreviewDialog.Document is null ) return DialogResult.Cancel;
        SetUp( richTextBox, printPreviewDialog.Document, false );
        return printPreviewDialog.ShowDialog();
    }

    /// <summary>   Get array of indexes for beginnings of pages. </summary>
    /// <remarks>
    /// Pages are measured according to PrintDocument.DefaultPageSettings;
    /// no print job is performed. There is always at least one index (array element)
    /// returned, and the first index is always 0, representing the beginning of all text.
    /// </remarks>
    /// <param name="richTextBox">      The rich text format control. </param>
    /// <param name="printDocument">    Instance of PrintDocument. </param>
    /// <returns>   An int[]. </returns>
    public static int[]? PageIndexes( this RichTextBox richTextBox, PrintDocument printDocument )
    {
        SetUp( richTextBox, printDocument, true );
        return _pageIndexes;
    }

    /// <summary>
    /// Set RightMargin property of RichTextBox to width of printer page (within horizontal margins)
    /// so that text wraps at the same position in the text box as on the printer.
    /// </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="richTextBox">  The rich text format control. </param>
    /// <param name="pageSettings"> Instance of PageSettings. </param>
    public static void SetRightMarginToPrinterWidth( this RichTextBox richTextBox, PageSettings pageSettings )
    {
        int pageWidth;
        // get page width in 1/100's of inches 
        pageWidth = pageSettings.Bounds.Width - pageSettings.Margins.Left - pageSettings.Margins.Right;
        // set rich-text-boxes .RightMargin property
        richTextBox.RightMargin = ( int ) (pageWidth * richTextBox.CreateGraphics().DpiX / 100.0d);
    }

    /// <summary>
    /// Set PageSettings right margin to RichTextBox's RightMargin value so that text wraps at the
    /// same position on the printer as in the text box.
    /// </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="richTextBox">  The rich text format control. </param>
    /// <param name="pageSettings"> Instance of PageSettings. </param>
    public static void SetPrinterWidthToRightMargin( this RichTextBox richTextBox, PageSettings pageSettings )
    {
        int pageWidth;
        // get text box's.RightMargin property in 1/100's of inches 
        pageWidth = ( int ) (richTextBox.RightMargin * 100.0d / richTextBox.CreateGraphics().DpiX);
        // set left- and right-hand margin of printer page
        int difference = pageSettings.Bounds.Width - pageWidth;
        pageSettings.Margins.Left = difference / 2;
        pageSettings.Margins.Right = difference - pageSettings.Margins.Left;
    }

    #endregion
}
