using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls.Explorers;

/// <summary> File list box. </summary>
/// <remarks>
/// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2013-09-27 </para>
/// </remarks>
[DesignerCategory( "code" )]
[Description( "File List Box" )]
public class FileListBox : ListBox
{
    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public FileListBox() : base()
    {
        // _readOnlyBackColor = SystemColors.Control
        // _readOnlyForeColor = SystemColors.WindowText
    }

    /// <summary> Gets the selection enabled. </summary>
    /// <value> The selection enabled. </value>
    public bool SelectionEnabled => base.AllowSelection;

    /// <summary> A pattern specifying the search. </summary>
    private string _searchPattern = string.Empty;

    /// <summary>
    /// Gets the file name search pattern for files displayed in the file list box.
    /// </summary>
    /// <value> The pattern. </value>
    [Category( "Pattern" )]
    [Description( "Specifies the file name search pattern." )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string SearchPattern
    {
        get => this._searchPattern;
        set
        {
            if ( !string.Equals( value, this.SearchPattern, StringComparison.Ordinal ) )
                this._searchPattern = value;
        }
    }

    /// <summary> Updates the display. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void UpdateDisplay()
    {
        if ( !string.IsNullOrWhiteSpace( this.DirectoryPath ) )
        {
            this.Items.Clear();
            System.IO.DirectoryInfo di = new( this._directoryPath );
            if ( di.Exists )
                foreach ( System.IO.FileInfo fi in di.GetFiles( this.SearchPattern ) )
                    _ = this.Items.Add( fi.Name );
        }
    }

    /// <summary> Gets or sets the filename of the selected file. </summary>
    /// <value> The filename of the selected file. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public string SelectedFileName
    {
        get => this.SelectedItem is null ? string.Empty : ( string ) this.SelectedItem;
        set
        {
            if ( !string.Equals( value, this.SelectedFileName, StringComparison.Ordinal ) )
            {
                int selectableIndex = this.FindString( value );
                if ( selectableIndex >= 0 )
                    this.SelectedIndex = selectableIndex;
            }
        }
    }

    /// <summary> Gets or sets the full file name including path). </summary>
    /// <value> The full filename of the selected file. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public string SelectedFilePath
    {
        get => this.SelectedItem is null ? string.Empty : System.IO.Path.Combine( this.DirectoryPath, ( string ) this.SelectedItem );
        set
        {
            if ( !string.Equals( value, this.SelectedFilePath, StringComparison.Ordinal ) )
            {
                System.IO.FileInfo fi = new( value );
                this.DirectoryPath = fi.DirectoryName ?? System.IO.Directory.GetCurrentDirectory();
                this.SelectedFileName = fi.Name;
            }
        }
    }

    /// <summary> Gets a list of names of the files. </summary>
    /// <value> A list of names of the files. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public IList<string> SelectedFileNames
    {
        get
        {
            List<string> l = [];
            foreach ( object v in this.SelectedItems )
            {
                string? name = v as string;
                if ( !string.IsNullOrWhiteSpace( name ) )
                    l.Add( name! );
            }

            return l;
        }
    }

    /// <summary> Gets the selected file paths. </summary>
    /// <value> The selected file paths. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public IList<string> SelectedFilePaths
    {
        get
        {
            List<string> l = [];
            foreach ( object v in this.SelectedItems )
            {
                string? name = v as string;
                if ( !string.IsNullOrWhiteSpace( name ) )
                    l.Add( System.IO.Path.Combine( this.DirectoryPath, name! ) );
            }

            return l;
        }
    }

    /// <summary> Full pathname of the directory. </summary>
    private string _directoryPath = string.Empty;

    /// <summary> Gets or sets the current directory path. </summary>
    /// <value> The directory path. </value>
    [Category( "Behavior" )]
    [Description( "The current directory path" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string DirectoryPath
    {
        get => this._directoryPath;
        set
        {
            if ( string.IsNullOrWhiteSpace( value ) )
                value = string.Empty;

            if ( !string.Equals( value, this.DirectoryPath, StringComparison.Ordinal ) )
            {
                this._directoryPath = value;
                this.UpdateDisplay();
            }
        }
    }

    /// <summary> The read write context menu. </summary>
    private ContextMenuStrip? _readWriteContextMenu;

    /// <summary> true to read only. </summary>
    private bool _readOnly;

    /// <summary> Gets or sets the read only. </summary>
    /// <value> The read only. </value>
    [DefaultValue( false )]
    [Category( "Behavior" )]
    [Description( "Indicates whether the List Box is read only." )]
    public bool ReadOnly
    {
        get => this._readOnly;
        set
        {
            if ( this._readOnly != value )
            {
                this._readOnly = value;
                if ( value )
                {
                    this._readWriteContextMenu = base.ContextMenuStrip;
                    this.ContextMenuStrip = new ContextMenuStrip();
                    int h = this.Height;
                    this.Height = h + 3;
                    this.BackColor = this.ReadOnlyBackColor;
                    this.ForeColor = this.ReadOnlyForeColor;
                }
                else
                {
                    this.ContextMenuStrip = this._readWriteContextMenu;
                    this.BackColor = this.ReadWriteBackColor;
                    this.ForeColor = this.ReadWriteForeColor;
                }
            }
        }
    }

    /// <summary> The read only back color. </summary>
    private Color _readOnlyBackColor;

    /// <summary> Gets or sets the color of the read only back. </summary>
    /// <value> The color of the read only back. </value>
    [DefaultValue( typeof( Color ), "SystemColors.Control" )]
    [Description( "Back color when read only" )]
    [Category( "Appearance" )]
    public Color ReadOnlyBackColor
    {
        get
        {
            if ( this._readOnlyBackColor.IsEmpty )
                this._readOnlyBackColor = SystemColors.Control;

            return this._readOnlyBackColor;
        }

        set => this._readOnlyBackColor = value;
    }

    /// <summary> The read only foreground color. </summary>
    private Color _readOnlyForeColor;

    /// <summary> Gets or sets the color of the read only foreground. </summary>
    /// <value> The color of the read only foreground. </value>
    [DefaultValue( typeof( Color ), "SystemColors.WindowText" )]
    [Description( "Fore color when read only" )]
    [Category( "Appearance" )]
    public Color ReadOnlyForeColor
    {
        get
        {
            if ( this._readOnlyForeColor.IsEmpty )
                this._readOnlyForeColor = SystemColors.WindowText;

            return this._readOnlyForeColor;
        }

        set => this._readOnlyForeColor = value;
    }

    /// <summary> The read write back color. </summary>
    private Color _readWriteBackColor;

    /// <summary> Gets or sets the color of the read write back. </summary>
    /// <value> The color of the read write back. </value>
    [DefaultValue( typeof( Color ), "SystemColors.Window" )]
    [Description( "Back color when control is read/write" )]
    [Category( "Appearance" )]
    public Color ReadWriteBackColor
    {
        get
        {
            if ( this._readWriteBackColor.IsEmpty )
                this._readWriteBackColor = SystemColors.Window;

            return this._readWriteBackColor;
        }

        set => this._readWriteBackColor = value;
    }

    /// <summary> The read write foreground color. </summary>
    private Color _readWriteForeColor;

    /// <summary> Gets or sets the color of the read write foreground. </summary>
    /// <value> The color of the read write foreground. </value>
    [DefaultValue( typeof( Color ), "System.Drawing.SystemColors.ControlText" )]
    [Description( "Fore color when control is read/write" )]
    [Category( "Appearance" )]
    public Color ReadWriteForeColor
    {
        get
        {
            if ( this._readWriteForeColor.IsEmpty )
                this._readWriteForeColor = SystemColors.ControlText;

            return this._readWriteForeColor;
        }

        set => this._readWriteForeColor = value;
    }

    /// <summary> Raises the <see cref="Control.KeyDown" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="KeyEventArgs" /> that contains the event
    /// data. </param>
    protected override void OnKeyDown( KeyEventArgs e )
    {
        if ( e is null )
            return;

        if ( this.ReadOnly && (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down || e.KeyCode == Keys.Delete || e.KeyCode == Keys.F4 || e.KeyCode == Keys.PageDown || e.KeyCode == Keys.PageUp) )
            e.Handled = true;
        else
            base.OnKeyDown( e );
    }

    /// <summary> Raises the <see cref="Control.KeyPress" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="KeyPressEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnKeyPress( KeyPressEventArgs e )
    {
        if ( e is null )
            return;

        if ( this.ReadOnly )
            e.Handled = true;
        else
            base.OnKeyPress( e );
    }
}
