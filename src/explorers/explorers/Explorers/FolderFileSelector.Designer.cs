using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using cc.isr.WinControls.Explorers;

namespace cc.isr.WinControls;
public partial class FolderFileSelector
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        var resources = new System.ComponentModel.ComponentResourceManager(typeof(FolderFileSelector));
        _fileListBoxLabel = new Label();
        _fileNamePatternCheckBoxLabel = new Label();
        _folderTextBoxLabel = new Label();
        _fullFileNameTextBoxLabel = new Label();
        _fullFileNameTextBox = new System.Windows.Forms.TextBox();
        _fullFileNameTextBox.TextChanged += new EventHandler(FileTitleTextBox_TextChanged);
        _fullFileNameTextBox.Enter += new EventHandler(EditTextBox_Enter);
        _selectFolderButton = new Button();
        _selectFolderButton.Click += new EventHandler(SelectFolderButton_Click);
        _folderTextBox = new System.Windows.Forms.TextBox();
        _folderTextBox.Enter += new EventHandler(EditTextBox_Enter);
        _folderTextBox.TextChanged += new EventHandler(FolderTextBox_TextChanged);
        _filePatternComboBox = new System.Windows.Forms.ComboBox();
        _filePatternComboBox.SelectedIndexChanged += new EventHandler(FilePatternComboBox_SelectedIndexChanged);
        _toolTip = new ToolTip(components);
        _dataFileListBox = new FileListBox();
        _dataFileListBox.SelectedIndexChanged += new EventHandler(DataFileListBox_SelectedIndexChanged);
        _dataFileListBox.DoubleClick += new EventHandler(DataFileListBox_DoubleClick);
        _fileInfoTextBox = new RichTextBox();
        _fileInfoTextBoxLabel = new Label();
        _refreshButton = new Button();
        _refreshButton.Click += new EventHandler(RefreshButton_Click);
        _annunciator = new ErrorProvider(components);
        ((System.ComponentModel.ISupportInitialize)_annunciator).BeginInit();
        SuspendLayout();
        // 
        // _fileListBoxLabel
        // 
        _fileListBoxLabel.BackColor = Color.Transparent;
        _fileListBoxLabel.Cursor = Cursors.Default;
        _fileListBoxLabel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _fileListBoxLabel.ForeColor = SystemColors.WindowText;
        _fileListBoxLabel.Location = new Point(2, 94);
        _fileListBoxLabel.Name = "_FileListBoxLabel";
        _fileListBoxLabel.RightToLeft = RightToLeft.No;
        _fileListBoxLabel.Size = new Size(72, 18);
        _fileListBoxLabel.TabIndex = 5;
        _fileListBoxLabel.Text = "&Files:";
        _fileListBoxLabel.TextAlign = ContentAlignment.BottomLeft;
        // 
        // _fileNamePatternCheckBoxLabel
        // 
        _fileNamePatternCheckBoxLabel.BackColor = Color.Transparent;
        _fileNamePatternCheckBoxLabel.Cursor = Cursors.Default;
        _fileNamePatternCheckBoxLabel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _fileNamePatternCheckBoxLabel.ForeColor = SystemColors.WindowText;
        _fileNamePatternCheckBoxLabel.Location = new Point(3, 342);
        _fileNamePatternCheckBoxLabel.Name = "_FileNamePatternCheckBoxLabel";
        _fileNamePatternCheckBoxLabel.RightToLeft = RightToLeft.No;
        _fileNamePatternCheckBoxLabel.Size = new Size(72, 18);
        _fileNamePatternCheckBoxLabel.TabIndex = 7;
        _fileNamePatternCheckBoxLabel.Text = "File T&ype: ";
        _fileNamePatternCheckBoxLabel.TextAlign = ContentAlignment.BottomLeft;
        // 
        // _folderTextBoxLabel
        // 
        _folderTextBoxLabel.AutoSize = true;
        _folderTextBoxLabel.BackColor = Color.Transparent;
        _folderTextBoxLabel.Cursor = Cursors.Default;
        _folderTextBoxLabel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _folderTextBoxLabel.ForeColor = SystemColors.WindowText;
        _folderTextBoxLabel.Location = new Point(3, 3);
        _folderTextBoxLabel.Name = "_FolderTextBoxLabel";
        _folderTextBoxLabel.RightToLeft = RightToLeft.No;
        _folderTextBoxLabel.Size = new Size(48, 17);
        _folderTextBoxLabel.TabIndex = 0;
        _folderTextBoxLabel.Text = "F&older:";
        _folderTextBoxLabel.TextAlign = ContentAlignment.BottomLeft;
        // 
        // _fullFileNameTextBoxLabel
        // 
        _fullFileNameTextBoxLabel.AutoSize = true;
        _fullFileNameTextBoxLabel.BackColor = Color.Transparent;
        _fullFileNameTextBoxLabel.Cursor = Cursors.Default;
        _fullFileNameTextBoxLabel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _fullFileNameTextBoxLabel.ForeColor = SystemColors.WindowText;
        _fullFileNameTextBoxLabel.Location = new Point(3, 51);
        _fullFileNameTextBoxLabel.Name = "_FullFileNameTextBoxLabel";
        _fullFileNameTextBoxLabel.RightToLeft = RightToLeft.No;
        _fullFileNameTextBoxLabel.Size = new Size(30, 17);
        _fullFileNameTextBoxLabel.TabIndex = 3;
        _fullFileNameTextBoxLabel.Text = "F&ile:";
        _fullFileNameTextBoxLabel.TextAlign = ContentAlignment.BottomLeft;
        // 
        // _fullFileNameTextBox
        // 
        _fullFileNameTextBox.AcceptsReturn = true;
        _fullFileNameTextBox.BackColor = SystemColors.Window;
        _fullFileNameTextBox.Cursor = Cursors.IBeam;
        _fullFileNameTextBox.Font = new Font("Arial", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _fullFileNameTextBox.ForeColor = SystemColors.WindowText;
        _fullFileNameTextBox.Location = new Point(3, 70);
        _fullFileNameTextBox.MaxLength = 0;
        _fullFileNameTextBox.Name = "_FullFileNameTextBox";
        _fullFileNameTextBox.RightToLeft = RightToLeft.No;
        _fullFileNameTextBox.Size = new Size(246, 22);
        _fullFileNameTextBox.TabIndex = 4;
        _toolTip.SetToolTip(_fullFileNameTextBox, "The name of the file that was selected or entered");
        _fullFileNameTextBox.WordWrap = false;
        // 
        // _selectFolderButton
        // 
        _selectFolderButton.Font = new Font("Segoe UI Black", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _selectFolderButton.Location = new Point(609, 22);
        _selectFolderButton.Name = "_SelectFolderButton";
        _selectFolderButton.Size = new Size(36, 26);
        _selectFolderButton.TabIndex = 2;
        _selectFolderButton.Text = "...";
        _toolTip.SetToolTip(_selectFolderButton, "Click to select a folder");
        // 
        // _folderTextBox
        // 
        _folderTextBox.AcceptsReturn = true;
        _folderTextBox.BackColor = SystemColors.Window;
        _folderTextBox.Cursor = Cursors.IBeam;
        _folderTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _folderTextBox.ForeColor = SystemColors.WindowText;
        _folderTextBox.Location = new Point(3, 22);
        _folderTextBox.MaxLength = 0;
        _folderTextBox.Name = "_FolderTextBox";
        _folderTextBox.RightToLeft = RightToLeft.No;
        _folderTextBox.Size = new Size(604, 25);
        _folderTextBox.TabIndex = 1;
        _toolTip.SetToolTip(_folderTextBox, "The name of the file you have selected or entered");
        // 
        // _filePatternComboBox
        // 
        _filePatternComboBox.BackColor = SystemColors.Window;
        _filePatternComboBox.Cursor = Cursors.Default;
        _filePatternComboBox.Font = new Font("Arial", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _filePatternComboBox.ForeColor = SystemColors.WindowText;
        _filePatternComboBox.Items.AddRange(new object[] { "Data Files (*.dat)", "All Files (*.*)" });
        _filePatternComboBox.Location = new Point(3, 362);
        _filePatternComboBox.Name = "_FilePatternComboBox";
        _filePatternComboBox.RightToLeft = RightToLeft.No;
        _filePatternComboBox.Size = new Size(184, 24);
        _filePatternComboBox.TabIndex = 8;
        _filePatternComboBox.Text = "*.dat";
        _toolTip.SetToolTip(_filePatternComboBox, "Determines the type listed in the file list");
        // 
        // _dataFileListBox
        // 
        _dataFileListBox.BackColor = SystemColors.Window;
        _dataFileListBox.Cursor = Cursors.Default;
        _dataFileListBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _dataFileListBox.ForeColor = Color.Black;
        _dataFileListBox.FormattingEnabled = true;
        _dataFileListBox.ItemHeight = 17;
        _dataFileListBox.Location = new Point(3, 113);
        _dataFileListBox.Name = "_DataFileListBox";
        _dataFileListBox.DirectoryPath = string.Empty;
        _dataFileListBox.SearchPattern = "*.dat";
        _dataFileListBox.ReadOnlyBackColor = SystemColors.Control;
        _dataFileListBox.ReadOnlyForeColor = SystemColors.WindowText;
        _dataFileListBox.ReadWriteBackColor = SystemColors.Window;
        _dataFileListBox.ReadWriteForeColor = SystemColors.ControlText;
        _dataFileListBox.SelectionMode = SelectionMode.MultiExtended;
        _dataFileListBox.Size = new Size(248, 225);
        _dataFileListBox.TabIndex = 6;
        _toolTip.SetToolTip(_dataFileListBox, "Select file(s)");
        // 
        // _fileInfoTextBox
        // 
        _fileInfoTextBox.Location = new Point(273, 70);
        _fileInfoTextBox.Name = "_FileInfoTextBox";
        _fileInfoTextBox.Size = new Size(371, 316);
        _fileInfoTextBox.TabIndex = 10;
        _fileInfoTextBox.Text = string.Empty;
        _toolTip.SetToolTip(_fileInfoTextBox, "Displays file information if available");
        // 
        // _fileInfoTextBoxLabel
        // 
        _fileInfoTextBoxLabel.AutoSize = true;
        _fileInfoTextBoxLabel.Location = new Point(272, 51);
        _fileInfoTextBoxLabel.Name = "_FileInfoTextBoxLabel";
        _fileInfoTextBoxLabel.Size = new Size(56, 17);
        _fileInfoTextBoxLabel.TabIndex = 11;
        _fileInfoTextBoxLabel.Text = "File info:";
        // 
        // _refreshButton
        // 
        _refreshButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
        _refreshButton.Image = (Image)resources.GetObject("_RefreshButton.Image");
        _refreshButton.Location = new Point(214, 352);
        _refreshButton.Name = "_RefreshButton";
        _refreshButton.Size = new Size(35, 34);
        _refreshButton.TabIndex = 9;
        _toolTip.SetToolTip(_refreshButton, "Click to select a folder");
        _refreshButton.UseCompatibleTextRendering = true;
        // 
        // _annunciator
        // 
        _annunciator.ContainerControl = this;
        // 
        // FolderFileSelector
        // 
        AutoScaleMode = AutoScaleMode.Inherit;
        Controls.Add(_fileInfoTextBoxLabel);
        Controls.Add(_fileInfoTextBox);
        Controls.Add(_fileListBoxLabel);
        Controls.Add(_fileNamePatternCheckBoxLabel);
        Controls.Add(_folderTextBoxLabel);
        Controls.Add(_fullFileNameTextBoxLabel);
        Controls.Add(_fullFileNameTextBox);
        Controls.Add(_filePatternComboBox);
        Controls.Add(_refreshButton);
        Controls.Add(_selectFolderButton);
        Controls.Add(_folderTextBox);
        Controls.Add(_dataFileListBox);
        Name = "FolderFileSelector";
        Size = new Size(650, 390);
        ((System.ComponentModel.ISupportInitialize)_annunciator).EndInit();
        ResumeLayout(false);
        PerformLayout();
    }

    private Label _fileListBoxLabel;
    private Label _fileNamePatternCheckBoxLabel;
    private Label _folderTextBoxLabel;
    private ToolTip _toolTip;
    private Label _fullFileNameTextBoxLabel;
    private System.Windows.Forms.TextBox _fullFileNameTextBox;
    private Button _refreshButton;
    private Button _selectFolderButton;
    private System.Windows.Forms.TextBox _folderTextBox;
    private System.Windows.Forms.ComboBox _filePatternComboBox;
    private FileListBox _dataFileListBox;
    private ErrorProvider _annunciator;
    private Label _fileInfoTextBoxLabel;
    private RichTextBox _fileInfoTextBox;
}
