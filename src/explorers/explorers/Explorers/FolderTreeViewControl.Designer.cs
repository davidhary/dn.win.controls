using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls;

public partial class FolderTreeViewControl
{
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    [DebuggerStepThrough()]
    private partial void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        var resources = new System.ComponentModel.ComponentResourceManager(typeof(FolderTreeViewControl));
        _pathTextBox = new System.Windows.Forms.TextBox();
        _pathTextBox.KeyUp += new KeyEventHandler(PathTextBox_KeyUp);
        _goToDirectoryButton = new Button();
        _goToDirectoryButton.Click += new EventHandler(GoToDirectoryButtonClick);
        _imageList = new ImageList(components);
        _toolTip = new ToolTip(components);
        FolderTreeView = new FolderTreeView();
        FolderTreeView.PathChanged += new EventHandler<EventArgs>(FolderTreeView_PathChanged);
        _currentPathLayoutPanel = new TableLayoutPanel();
        _toolStrip = new ToolStrip();
        _homeToolStripButton = new System.Windows.Forms.ToolStripButton();
        _homeToolStripButton.Click += new EventHandler(HomeToolStripButton_Click);
        _folderUpButton = new System.Windows.Forms.ToolStripButton();
        _folderUpButton.Click += new EventHandler(FolderUpButton_Click);
        _backToolStripButton = new System.Windows.Forms.ToolStripButton();
        _backToolStripButton.Click += new EventHandler(BackToolStripButton_Click);
        _forwardToolStripButton = new System.Windows.Forms.ToolStripButton();
        _forwardToolStripButton.Click += new EventHandler(ForwardToolStripButton_Click);
        _addFolderButton = new System.Windows.Forms.ToolStripButton();
        _addFolderButton.Click += new EventHandler(AddFolderButton_Click);
        _refreshToolStripButton = new System.Windows.Forms.ToolStripButton();
        _refreshToolStripButton.Click += new EventHandler(RefreshButtonClick);
        _currentPathLayoutPanel.SuspendLayout();
        _toolStrip.SuspendLayout();
        SuspendLayout();
        // 
        // _pathTextBox
        // 
        _pathTextBox.Dock = DockStyle.Fill;
        _pathTextBox.Location = new Point(3, 3);
        _pathTextBox.Name = "_PathTextBox";
        _pathTextBox.Size = new Size(276, 23);
        _pathTextBox.TabIndex = 0;
        _toolTip.SetToolTip(_pathTextBox, "Current directory");
        // 
        // _goToDirectoryButton
        // 
        _goToDirectoryButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        _goToDirectoryButton.Cursor = Cursors.Hand;
        _goToDirectoryButton.FlatStyle = FlatStyle.Flat;
        _goToDirectoryButton.ForeColor = Color.White;
        _goToDirectoryButton.ImageIndex = 21;
        _goToDirectoryButton.ImageList = _imageList;
        _goToDirectoryButton.Location = new Point(285, 3);
        _goToDirectoryButton.Name = "_GoToDirectoryButton";
        _goToDirectoryButton.Size = new Size(20, 22);
        _goToDirectoryButton.TabIndex = 1;
        _toolTip.SetToolTip(_goToDirectoryButton, "Go to the directory");
        // 
        // _imageList
        // 
        _imageList.ImageStream = (ImageListStreamer)resources.GetObject("_ImageList.ImageStream");
        _imageList.TransparentColor = Color.Transparent;
        _imageList.Images.SetKeyName(0, "");
        _imageList.Images.SetKeyName(1, "");
        _imageList.Images.SetKeyName(2, "");
        _imageList.Images.SetKeyName(3, "");
        _imageList.Images.SetKeyName(4, "");
        _imageList.Images.SetKeyName(5, "");
        _imageList.Images.SetKeyName(6, "");
        _imageList.Images.SetKeyName(7, "");
        _imageList.Images.SetKeyName(8, "");
        _imageList.Images.SetKeyName(9, "");
        _imageList.Images.SetKeyName(10, "");
        _imageList.Images.SetKeyName(11, "");
        _imageList.Images.SetKeyName(12, "");
        _imageList.Images.SetKeyName(13, "");
        _imageList.Images.SetKeyName(14, "");
        _imageList.Images.SetKeyName(15, "");
        _imageList.Images.SetKeyName(16, "");
        _imageList.Images.SetKeyName(17, "");
        _imageList.Images.SetKeyName(18, "");
        _imageList.Images.SetKeyName(19, "");
        _imageList.Images.SetKeyName(20, "");
        _imageList.Images.SetKeyName(21, "");
        _imageList.Images.SetKeyName(22, "");
        _imageList.Images.SetKeyName(23, "");
        _imageList.Images.SetKeyName(24, "");
        _imageList.Images.SetKeyName(25, "");
        _imageList.Images.SetKeyName(26, "");
        _imageList.Images.SetKeyName(27, "");
        _imageList.Images.SetKeyName(28, "");
        // 
        // _folderTreeView
        // 
        FolderTreeView.Dock = DockStyle.Fill;
        FolderTreeView.ImageIndex = 0;
        FolderTreeView.Location = new Point(0, 57);
        FolderTreeView.Name = "_FolderTreeView";
        FolderTreeView.SelectedImageIndex = 2;
        FolderTreeView.ShowLines = false;
        FolderTreeView.ShowRootLines = false;
        FolderTreeView.Size = new Size(308, 382);
        FolderTreeView.TabIndex = 2;
        // 
        // _currentPathLayoutPanel
        // 
        _currentPathLayoutPanel.ColumnCount = 2;
        _currentPathLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
        _currentPathLayoutPanel.ColumnStyles.Add(new ColumnStyle());
        _currentPathLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
        _currentPathLayoutPanel.Controls.Add(_goToDirectoryButton, 1, 0);
        _currentPathLayoutPanel.Controls.Add(_pathTextBox, 0, 0);
        _currentPathLayoutPanel.Dock = DockStyle.Top;
        _currentPathLayoutPanel.Location = new Point(0, 25);
        _currentPathLayoutPanel.Name = "_CurrentPathLayoutPanel";
        _currentPathLayoutPanel.RowCount = 2;
        _currentPathLayoutPanel.RowStyles.Add(new RowStyle());
        _currentPathLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
        _currentPathLayoutPanel.Size = new Size(308, 32);
        _currentPathLayoutPanel.TabIndex = 1;
        // 
        // _toolStrip
        // 
        _toolStrip.GripMargin = new Padding(0);
        _toolStrip.GripStyle = ToolStripGripStyle.Hidden;
        _toolStrip.Items.AddRange(new ToolStripItem[] { _homeToolStripButton, _folderUpButton, _backToolStripButton, _forwardToolStripButton, _addFolderButton, _refreshToolStripButton });
        _toolStrip.Location = new Point(0, 0);
        _toolStrip.Name = "_ToolStrip";
        _toolStrip.Size = new Size(308, 25);
        _toolStrip.TabIndex = 0;
        _toolStrip.Text = "ToolStrip1";
        // 
        // _homeToolStripButton
        // 
        _homeToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _homeToolStripButton.Image = Properties.Resources.go_up_8;
        _homeToolStripButton.ImageTransparentColor = Color.Magenta;
        _homeToolStripButton.Name = "_HomeToolStripButton";
        _homeToolStripButton.Size = new Size(23, 22);
        _homeToolStripButton.Text = "Home";
        // 
        // _folderUpButton
        // 
        _folderUpButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _folderUpButton.Image = Properties.Resources.go_up_8;
        _folderUpButton.ImageTransparentColor = Color.Magenta;
        _folderUpButton.Name = "_FolderUpButton";
        _folderUpButton.Size = new Size(23, 22);
        _folderUpButton.Text = "Up";
        // 
        // _backToolStripButton
        // 
        _backToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _backToolStripButton.Image = Properties.Resources.go_up_8;
        _backToolStripButton.ImageTransparentColor = Color.Magenta;
        _backToolStripButton.Name = "_BackToolStripButton";
        _backToolStripButton.Size = new Size(23, 22);
        _backToolStripButton.Text = "Back";
        // 
        // _forwardToolStripButton
        // 
        _forwardToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _forwardToolStripButton.Image = Properties.Resources.go_up_8;
        _forwardToolStripButton.ImageTransparentColor = Color.Magenta;
        _forwardToolStripButton.Name = "_ForwardToolStripButton";
        _forwardToolStripButton.Size = new Size(23, 22);
        _forwardToolStripButton.Text = "Forward";
        // 
        // _addFolderButton
        // 
        _addFolderButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _addFolderButton.Image = Properties.Resources.go_up_8;
        _addFolderButton.ImageTransparentColor = Color.Magenta;
        _addFolderButton.Name = "_addFolderButton";
        _addFolderButton.Size = new Size(23, 22);
        _addFolderButton.Text = "Add";
        // 
        // _refreshToolStripButton
        // 
        _refreshToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _refreshToolStripButton.Image = Properties.Resources.go_up_8;
        _refreshToolStripButton.ImageTransparentColor = Color.Magenta;
        _refreshToolStripButton.Name = "_RefreshToolStripButton";
        _refreshToolStripButton.Size = new Size(23, 22);
        _refreshToolStripButton.Text = "Refresh";
        // 
        // FolderTreeViewControl
        // 
        BackColor = SystemColors.Control;
        Controls.Add(FolderTreeView);
        Controls.Add(_currentPathLayoutPanel);
        Controls.Add(_toolStrip);
        Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        Name = "FolderTreeViewControl";
        Size = new Size(308, 439);
        _currentPathLayoutPanel.ResumeLayout(false);
        _currentPathLayoutPanel.PerformLayout();
        _toolStrip.ResumeLayout(false);
        _toolStrip.PerformLayout();
        ResumeLayout(false);
        PerformLayout();
    }

    private Button _goToDirectoryButton;
    private ToolTip _toolTip;
    private ImageList _imageList;
    private System.Windows.Forms.TextBox _pathTextBox;
    private TableLayoutPanel _currentPathLayoutPanel;
    private System.Windows.Forms.ToolStripButton _folderUpButton;
    private ToolStrip _toolStrip;
    private System.Windows.Forms.ToolStripButton _homeToolStripButton;
    private System.Windows.Forms.ToolStripButton _backToolStripButton;
    private System.Windows.Forms.ToolStripButton _forwardToolStripButton;
    private System.Windows.Forms.ToolStripButton _addFolderButton;
    private System.Windows.Forms.ToolStripButton _refreshToolStripButton;
}
