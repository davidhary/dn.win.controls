using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls.Explorers;

/// <summary> Folder and file explorer view. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-07-04 </para>
/// </remarks>
[Description( "Folder and file explorer" )]
[ToolboxBitmap( typeof( FileExplorerView ), "FileExplorerView.gif" )]
public class FileExplorerView : ModelViewBase
{
    #region " construction "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public FileExplorerView() : base()
    {
        this.InitializingComponents = true;
        // This call is required by the Windows Form Designer.
        this.InitializeComponent();
        this.InitializingComponents = false;
        this._folderExplorer.FolderTreeView.PathChanged += this.FolderTreeViewControlPathChanged;
    }

    #endregion

    #region " designer "

    /// <summary> The splitter. </summary>
    private Splitter _splitter;

    /// <summary>   The folder explorer. </summary>
    private FolderTreeViewControl _folderExplorer;

    /// <summary>   Gets or sets the file list view control. </summary>
    /// <value> The file list view control. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    internal FileListViewControl FileListViewControl { get; set; }

    /// <summary>
    /// Required method for Designer support - do not modify the contents of this method with the
    /// code editor.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    [MemberNotNull( nameof( _splitter ) )]
    [MemberNotNull( nameof( _folderExplorer ) )]
    [MemberNotNull( nameof( FileListViewControl ) )]
    private void InitializeComponent()
    {
        this._folderExplorer = new FolderTreeViewControl();
        this._splitter = new Splitter();
        this.FileListViewControl = new FileListViewControl();
        this.SuspendLayout();
        // 
        // _folderExplorer
        // 
        this._folderExplorer.BackColor = SystemColors.Control;
        this._folderExplorer.Dock = DockStyle.Left;
        this._folderExplorer.Font = new Font( "Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0 );
        this._folderExplorer.Location = new Point( 0, 0 );
        this._folderExplorer.Name = "_FolderExplorer";
        this._folderExplorer.ShowCurrentPathTextBox = true;
        this._folderExplorer.ShowMyDocuments = true;
        this._folderExplorer.ShowMyFavorites = true;
        this._folderExplorer.ShowMyNetwork = true;
        this._folderExplorer.ShowToolBar = true;
        this._folderExplorer.Size = new Size( 168, 357 );
        this._folderExplorer.TabIndex = 0;
        // 
        // _splitter
        // 
        this._splitter.Location = new Point( 168, 0 );
        this._splitter.Name = "_Splitter";
        this._splitter.Size = new Size( 3, 357 );
        this._splitter.TabIndex = 3;
        this._splitter.TabStop = false;
        // 
        // _fileListViewControl
        // 
        this.FileListViewControl.Dock = DockStyle.Fill;
        this.FileListViewControl.Font = new Font( "Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0 );
        this.FileListViewControl.HideFileInfo = false;
        this.FileListViewControl.Location = new Point( 171, 0 );
        this.FileListViewControl.Name = "_FileListViewControl";
        this.FileListViewControl.DirectoryPath = System.IO.Directory.GetCurrentDirectory();
        this.FileListViewControl.SearchPatternDelimiter = ';';
        this.FileListViewControl.SearchPatterns = "*.*;*.bmp, *.jpg, *.png;*.csv, *.txt";
        this.FileListViewControl.Size = new Size( 429, 357 );
        this.FileListViewControl.TabIndex = 1;
        // 
        // FileExplorerView
        // 
        this.Controls.Add( this.FileListViewControl );
        this.Controls.Add( this._splitter );
        this.Controls.Add( this._folderExplorer );
        this.Name = "FileExplorerView";
        this.Size = new Size( 600, 357 );
        this.ResumeLayout( false );
    }

    #endregion

    #region " control events "

    /// <summary> Select home. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void SelectHome()
    {
        _ = this._folderExplorer.FolderTreeView.TrySelectHome();
    }

    /// <summary>
    /// Forces the control to invalidate its client area and immediately redraw itself and any child
    /// controls.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public override void Refresh()
    {
        base.Refresh();
        this._folderExplorer.Refresh();
    }

    /// <summary> Handles the path change. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Tree view event information. </param>
    private void FolderTreeViewControlPathChanged( object? sender, EventArgs e )
    {
        // Populate folders and files when a folder is selected
        this.Cursor = Cursors.WaitCursor;
        try
        {
            this.FileListViewControl.DirectoryPath = this._folderExplorer.SelectedPath;
            this.FileListViewControl.Refresh();
        }
        // Me._fileListViewControl.FileListView.PopulateFiles(Me._folderExplorer.SelectedPath)
        catch ( UnauthorizedAccessException )
        {
            _ = MessageBox.Show( $"Access denied to folder {this._folderExplorer.SelectedPath}", "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
        }
        catch ( Exception ex )
        {
            _ = MessageBox.Show( $"Error: {ex}" );
        }
        finally
        {
            this.Cursor = Cursors.Default;
        }
    }

    #endregion
}
