using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> A folder file selector. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-07-03 </para>
/// </remarks>
public partial class FolderFileSelector : ModelViewBase
{
    #region " construction and cleanup "

    /// <summary>
    /// Prevents a default instance of the <see cref="FolderFileSelector" /> class from being created.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private FolderFileSelector() : base()
    {
        // This method is required by the Windows Form Designer.
        this.InitializeComponent();
        this.ParentFolderPathName = System.IO.Directory.GetCurrentDirectory();
    }

    #endregion

    #region " methods  and  properties "

    /// <summary> Adds a file name pattern. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="pattern"> Specify the file type and pattern such as "Data Files (*.dat)" or
    /// "Pictures (*.bmp; *.jpg)". </param>
    public void AddFilePattern( string pattern )
    {
        if ( string.IsNullOrWhiteSpace( pattern ) )
        {
            this._filePatternComboBox.Items.Clear();
        }
        else
        {
            // Add an item to the combo box.
            _ = this._filePatternComboBox.Items.Add( pattern );
        }
    }

    /// <summary> Gets or sets the caption of the form. </summary>
    /// <value> The caption. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string Caption
    {
        get => this.Text;
        set => this.Text = value;
    }

    /// <summary> Gets or sets the data file list selection mode. </summary>
    /// <value> The selection mode. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public SelectionMode SelectionMode
    {
        get => this._dataFileListBox.SelectionMode;
        set => this._dataFileListBox.SelectionMode = value;
    }

    /// <summary> Returns the number of selected files. </summary>
    /// <value> The selected count. </value>
    public int SelectedCount => this._dataFileListBox.SelectedItems.Count;

    /// <summary> Gets the array of selected files. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> System.String[][]. </returns>
    public string[] SelectedFiles()
    {
        string[] selected = new string[this._dataFileListBox.SelectedItems.Count];
        this._dataFileListBox.SelectedItems.CopyTo( selected, 0 );
        return selected;
    }

    /// <summary> Returns the selected file names. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> System.String[][]. </returns>
    public string[] SelectedFullFileNames()
    {
        if ( this.SelectedFiles() is null || this.SelectedFiles().Length == 0 )
        {
            return [];
        }
        else
        {
            List<string> fullFileNames = [];
            foreach ( string fileName in this.SelectedFiles() )
            {
                fullFileNames.Add( System.IO.Path.Combine( this.SelectedPathName, fileName ) );
            }

            return [.. fullFileNames];
        }
    }

    /// <summary> Returns the path of the selected file. </summary>
    /// <value> The name of the selected path. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string SelectedPathName
    {
        get =>
            // Set file path.
            this._dataFileListBox.DirectoryPath;
        set
        {
            this._folderTextBox.Text = value;
            if ( System.IO.Directory.Exists( value ) )
            {
                // clear the previous path so that we get a refresh
                this._dataFileListBox.DirectoryPath = Environment.CurrentDirectory;
                this._dataFileListBox.Invalidate();
                System.Threading.Tasks.Task.Delay( 1 ).Wait();

                this._dataFileListBox.DirectoryPath = value;
                this._dataFileListBox.Invalidate();
            }
        }
    }

    /// <summary> Returns the name of the select file. </summary>
    /// <value> The selected filename. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string SelectedFileName
    {
        get =>
            // return the file title.
            this._fullFileNameTextBox.Text;
        set => this._fullFileNameTextBox.Text = value;
    }

    /// <summary> Returns the title of the select file and path. </summary>
    /// <value> The name of the selected file path. </value>
    public string SelectedFullFileName =>
            // return the file title.
            System.IO.Path.Combine( this.SelectedPathName, this.SelectedFileName );

    /// <summary> Returns the title of the select file. </summary>
    /// <value> The selected file title. </value>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1865:Use char overload", Justification = "<Pending>" )]
    public string SelectedFileTitle =>
            // return the file title.
            string.IsNullOrWhiteSpace( this.SelectedFileName ) ? string.Empty : this.SelectedFileName[..this.SelectedFileName.LastIndexOf( ".", StringComparison.Ordinal )];

    /// <summary> Returns the file type pattern. </summary>
    /// <value> The pattern. </value>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1865:Use char overload", Justification = "<Pending>" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string Pattern
    {
        get => this._filePatternComboBox.Text;
        set
        {
            this._filePatternComboBox.Text = value;
            if ( !string.IsNullOrWhiteSpace( value ) )
            {
                int firstLocation = value.IndexOf( "(", StringComparison.Ordinal ) + 1;
                int patternLength = value.IndexOf( ")", firstLocation, StringComparison.Ordinal ) - firstLocation;
                this._dataFileListBox.SearchPattern = value.Substring( firstLocation, patternLength );
            }
        }
    }

    /// <summary> Gets or sets the status message. </summary>
    /// <remarks> Use this property to get the status message generated by the object. </remarks>
    /// <value> A <see cref="string">String</see>. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string StatusMessage { get; set; } = string.Empty;

    #endregion

    #region " private  and  protected "

    /// <summary> Verifies the file name and provides alerts as necessary. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> <c>true</c> if okay, <c>false</c> otherwise. </returns>
    public bool VerifyFileName()
    {
        // Set file name for testing (faster than using the Text property over again.
        string fileTitle = this._fullFileNameTextBox.Text.Trim();

        // clear the Annunciator
        this._annunciator.SetError( this._fullFileNameTextBox, "" );

        // Check if file name was given.
        if ( fileTitle is null || fileTitle.Length < 2 )
        {
            // If file is not given, alert and exit:
            this.StatusMessage = "Please select or enter a file name or use Cancel to exit this dialog box.";
            this._annunciator.SetIconAlignment( this._fullFileNameTextBox, ErrorIconAlignment.TopRight );
            this._annunciator.SetError( this._fullFileNameTextBox, "" );
            this._annunciator.SetError( this._fullFileNameTextBox, this.StatusMessage );
            return false;
        }
        else
        {
            try
            {
                // Check for wild cards in the file name.
                if ( fileTitle.IndexOf( '*' ) + fileTitle.IndexOf( '?' ) > 0 )
                {
                    // If wild cards in file name, alert and exit:
                    this.StatusMessage = "Please enter a complete file name!";
                    this._annunciator.SetIconAlignment( this._fullFileNameTextBox, ErrorIconAlignment.TopLeft );
                    this._annunciator.SetError( this._fullFileNameTextBox, "" );
                    this._annunciator.SetError( this._fullFileNameTextBox, this.StatusMessage );
                    return false;
                }

                // This will create an error if the file name is bad.
                return System.IO.File.Exists( this.SelectedFullFileName );
            }
            catch ( System.IO.IOException )
            {
                // If error in file name, alert:
                this.StatusMessage = "Please enter a correct file name!";
                this._annunciator.SetIconAlignment( this._fullFileNameTextBox, ErrorIconAlignment.TopLeft );
                this._annunciator.SetError( this._fullFileNameTextBox, "" );
                this._annunciator.SetError( this._fullFileNameTextBox, this.StatusMessage );
                return false;
            }
        }
    }

    /// <summary> Event queue for all listeners interested in FileSelected events. </summary>
    public event EventHandler<System.IO.FileSystemEventArgs>? FileSelected;

    /// <summary> Update data for the specified file name. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="fullFileName"> Name of the file path. </param>
    private void UpdateData( string fullFileName )
    {
        // clear the Annunciator
        this._annunciator.SetError( this._fullFileNameTextBox, "" );
        if ( !System.IO.File.Exists( fullFileName ) )
        {
            this._annunciator.SetIconAlignment( this._fullFileNameTextBox, ErrorIconAlignment.TopLeft );
            this._annunciator.SetError( this._fullFileNameTextBox, "" );
            this.StatusMessage = "File not found";
            this._annunciator.SetError( this._fullFileNameTextBox, this.StatusMessage );
            return;
        }

        try
        {
            this.FileSelected?.Invoke( this, new System.IO.FileSystemEventArgs( System.IO.WatcherChangeTypes.Changed, this.SelectedPathName, this.SelectedFileName ) );
        }
        catch
        {
            throw;
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    /// <summary> Displays a file information described by value. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    public void DisplayFileInfo( string value )
    {
        this._fileInfoTextBox.Text = value;
    }

    /// <summary> Displays a file information rich text described by value. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    public void DisplayFileInfoRichText( string value )
    {
        this._fileInfoTextBox.Rtf = value;
    }

    #endregion

    #region " control event handlers "

    /// <summary> Handles the SelectedIndexChanged event of the Me._dataFileListBox control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> The source of the event. </param>
    /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    private void DataFileListBox_SelectedIndexChanged( object? sender, EventArgs e )
    {
        // Set the file name text box.
        this._fullFileNameTextBox.Text = this._dataFileListBox.SelectedFilePath;
        this.UpdateData( this.SelectedFullFileName );
    }

    /// <summary> Updates and exits. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> The source of the event. </param>
    /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    private void DataFileListBox_DoubleClick( object? sender, EventArgs e )
    {
        // Just update the file and try to exit.
        this.DataFileListBox_SelectedIndexChanged( this._dataFileListBox, new EventArgs() );
    }

    /// <summary>
    /// Handles the SelectedIndexChanged event of the Me._filePatternComboBox control.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> The source of the event. </param>
    /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    private void FilePatternComboBox_SelectedIndexChanged( object? sender, EventArgs e )
    {
        // clear the Annunciator
        this._annunciator.SetError( this._filePatternComboBox, "" );
        this._annunciator.SetError( this._fullFileNameTextBox, "" );

        // Reset the file pattern upon a click on the Pattern combo box.
        this.Pattern = this._filePatternComboBox.Text;
    }

    /// <summary> Handles the TextChanged event of the fileTitleTextBox control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> The source of the event. </param>
    /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    private void FileTitleTextBox_TextChanged( object? sender, EventArgs e )
    {
        // clear the Annunciator
        this._annunciator.SetError( this._fullFileNameTextBox, "" );
        string newFileName = this._fullFileNameTextBox.Text;

        // Check if wild cards in file name.
        if ( newFileName.IndexOf( "*", StringComparison.Ordinal ) + newFileName.IndexOf( "?", StringComparison.Ordinal ) > 0 )
        {
            // If so, reset the file pattern.
            this._dataFileListBox.SearchPattern = newFileName;
        }
    }

    /// <summary>
    /// Handles the Enter event of the edit text box controls. Selects the control text.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> The source of the event. </param>
    /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    private void EditTextBox_Enter( object? sender, EventArgs e )
    {
        if ( sender is TextBox editBox )
        {
            editBox.SelectionStart = 0;
            editBox.SelectionLength = editBox.Text.Length;
        }
    }

    /// <summary> Handles the TextChanged event of the Me._folderTextBox control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> The source of the event. </param>
    /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    private void FolderTextBox_TextChanged( object? sender, EventArgs e )
    {
        if ( System.IO.Directory.Exists( this._folderTextBox.Text ) )
        {
            this.SelectedPathName = this._folderTextBox.Text;
        }
    }

    /// <summary> Handles the Click event of the Me._refreshButton control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> The source of the event. </param>
    /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    private void RefreshButton_Click( object? sender, EventArgs e )
    {
        // clear the Annunciator
        this._annunciator.SetError( this._fullFileNameTextBox, "" );
        this.SelectedPathName = this._dataFileListBox.DirectoryPath;
    }

    /// <summary> Gets or sets the full pathname of the parent folder file. </summary>
    /// <value> The full pathname of the parent folder file. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string ParentFolderPathName { get; set; }

    /// <summary> Handles the Click event of the Me._selectFolderButton control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> The source of the event. </param>
    /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    private void SelectFolderButton_Click( object? sender, EventArgs e )
    {
        // clear the Annunciator
        this._annunciator.SetError( this._fullFileNameTextBox, "" );
        using FolderBrowserDialog browser = new()
        {
            // browser.Description = "Select folder for files to retrieve"
            RootFolder = Environment.SpecialFolder.MyComputer,
            // set the default path
            SelectedPath = System.IO.Directory.Exists( this.ParentFolderPathName ) ? this.ParentFolderPathName : this._dataFileListBox.DirectoryPath,
            ShowNewFolderButton = true
        };
        if ( browser.ShowDialog() == DialogResult.OK )
        {
            // Set the path label
            this.SelectedPathName = browser.SelectedPath;
        }
    }

    #endregion
}
