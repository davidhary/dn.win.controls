using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Windows.Forms;

namespace cc.isr.WinControls.Explorers;

/// <summary> A file explorer. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-07-04 </para>
/// </remarks>
[Description( "File explorer" )]
[ToolboxBitmap( typeof( FileExplorerPassive ), "FileExplorerPassive.gif" )]
public class FileExplorerPassive : ModelViewBase
{
    #region " construction "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public FileExplorerPassive() : base()
    {
        this.InitializingComponents = true;
        // This call is required by the Windows Form Designer.
        this.InitializeComponent();
        this.InitializingComponents = false;
    }

    /// <summary>
    /// Releases the unmanaged resources used by the cc.isr.WinControls.ModelViewBase and optionally
    /// releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
                this._components?.Dispose();
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " designer "

    private Splitter _splitter;
    private FolderTreeViewPassive _folderTreeView;
    private FileListView _fileListView;
    private Container _components;

    /// <summary>
    /// Required method for Designer support - do not modify the contents of this method with the
    /// code editor.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    [MemberNotNull( nameof( _splitter ) )]
    [MemberNotNull( nameof( _folderTreeView ) )]
    [MemberNotNull( nameof( _fileListView ) )]
    [MemberNotNull( nameof( _components ) )]
    private void InitializeComponent()
    {
        this._components = new Container();
        TreeNode treeNode1 = new( @"C:\", 6, 6 );
        TreeNode TreeNode2 = new( @"D:\", 7, 7 );
        TreeNode TreeNode3 = new( "My Computer", 0, 0, [treeNode1, TreeNode2] );
        this._folderTreeView = new FolderTreeViewPassive();
        this._folderTreeView.AfterSelect += new TreeViewEventHandler( this.FolderTreeViewAfterSelect );
        this._splitter = new Splitter();
        this._fileListView = new FileListView();
        this.SuspendLayout();
        // 
        // _folderTreeView
        // 
        this._folderTreeView.Cursor = Cursors.Default;
        this._folderTreeView.Dock = DockStyle.Left;
        this._folderTreeView.ImageIndex = 0;
        this._folderTreeView.Location = new Point( 0, 0 );
        this._folderTreeView.Name = "_FolderTreeView";
        treeNode1.ImageIndex = 6;
        treeNode1.Name = string.Empty;
        treeNode1.SelectedImageIndex = 6;
        treeNode1.Text = @"C:\";
        TreeNode2.ImageIndex = 7;
        TreeNode2.Name = string.Empty;
        TreeNode2.SelectedImageIndex = 7;
        TreeNode2.Text = @"D:\";
        TreeNode3.ImageIndex = 0;
        TreeNode3.Name = string.Empty;
        TreeNode3.SelectedImageIndex = 0;
        TreeNode3.Text = "My Computer";
        this._folderTreeView.Nodes.AddRange( [TreeNode3] );
        this._folderTreeView.Path = string.Empty;
        this._folderTreeView.SelectedImageIndex = 0;
        this._folderTreeView.Size = new Size( 168, 357 );
        this._folderTreeView.TabIndex = 0;
        // 
        // _splitter
        // 
        this._splitter.Location = new Point( 168, 0 );
        this._splitter.Name = "_Splitter";
        this._splitter.Size = new Size( 3, 357 );
        this._splitter.TabIndex = 3;
        this._splitter.TabStop = false;
        // 
        // _fileListView
        // 
        this._fileListView.Dock = DockStyle.Fill;
        this._fileListView.HideSelection = false;
        this._fileListView.Location = new Point( 171, 0 );
        this._fileListView.Name = "_FileListView";
        this._fileListView.Size = new Size( 429, 357 );
        this._fileListView.TabIndex = 1;
        this._fileListView.UseCompatibleStateImageBehavior = false;
        this._fileListView.View = View.Details;
        // 
        // FileExplorerPassive
        // 
        this.Controls.Add( this._fileListView );
        this.Controls.Add( this._splitter );
        this.Controls.Add( this._folderTreeView );
        this.Name = "FileExplorerPassive";
        this.Size = new Size( 600, 357 );
        this.ResumeLayout( false );
    }

    #endregion

    #region " control events "

    /// <summary> Handles the after select. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Tree view event information. </param>
    private void FolderTreeViewAfterSelect( object? sender, TreeViewEventArgs e )
    {
        if ( e.Node is null ) return;

        // Populate folders and files when a folder is selected
        this.Cursor = Cursors.WaitCursor;
        string fullPath = string.Empty;
        try
        {
            TreeNode? folderTreeNode = e.Node;
            fullPath = FolderTreeViewPassive.ParseFullPath( e.Node.FullPath );
            if ( folderTreeNode is null || folderTreeNode.SelectedImageIndex == 0 )
            {
            }
            // nothing to do: leave clear file list
            else if ( System.IO.Directory.Exists( fullPath ) )
                this._fileListView.PopulateFiles( fullPath, "*.*" );
            else
            {
            }
        }
        catch ( UnauthorizedAccessException )
        {
            _ = MessageBox.Show( $"Access denied to folder {fullPath}", "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
        }
        catch ( Exception ex )
        {
            _ = MessageBox.Show( $"Error: {ex}" );
        }
        finally
        {
            this.Cursor = Cursors.Default;
        }
    }

    #endregion
}
