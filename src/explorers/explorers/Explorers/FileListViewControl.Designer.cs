using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using cc.isr.WinControls.Explorers;

namespace cc.isr.WinControls;

public partial class FileListViewControl
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        _toolStrip = new ToolStrip();
        _searchPatternComboBox = new System.Windows.Forms.ToolStripComboBox();
        _refreshButton = new System.Windows.Forms.ToolStripButton();
        _refreshButton.Click += new EventHandler(RefreshButtonClick);
        FileListView = new FileListView();
        _split = new SplitContainer();
        _fileInfoRichTextBox = new System.Windows.Forms.RichTextBox();
        _toolStrip.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)_split).BeginInit();
        _split.Panel1.SuspendLayout();
        _split.Panel2.SuspendLayout();
        _split.SuspendLayout();
        SuspendLayout();
        // 
        // _toolStrip
        // 
        _toolStrip.GripMargin = new Padding(0);
        _toolStrip.GripStyle = ToolStripGripStyle.Hidden;
        _toolStrip.Items.AddRange(new ToolStripItem[] { _searchPatternComboBox, _refreshButton });
        _toolStrip.Location = new Point(0, 0);
        _toolStrip.Name = "_ToolStrip";
        _toolStrip.Size = new Size(301, 25);
        _toolStrip.Stretch = true;
        _toolStrip.TabIndex = 0;
        _toolStrip.Text = "Tool Strip";
        // 
        // _searchPatternComboBox
        // 
        _searchPatternComboBox.Items.AddRange(new object[] { "*.*", "*.bmp, *.jpg, *.png", "*.csv, *.txt" });
        _searchPatternComboBox.Name = "_SearchPatternComboBox";
        _searchPatternComboBox.Size = new Size(140, 25);
        _searchPatternComboBox.Text = "*.*";
        _searchPatternComboBox.ToolTipText = "Search patterns";
        // 
        // _refreshButton
        // 
        _refreshButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        _refreshButton.Image = Properties.Resources.arrow_refresh_small;
        _refreshButton.ImageScaling = ToolStripItemImageScaling.None;
        _refreshButton.ImageTransparentColor = Color.Magenta;
        _refreshButton.Name = "_RefreshButton";
        _refreshButton.Size = new Size(23, 22);
        _refreshButton.Text = "Refresh";
        // 
        // _fileListView
        // 
        FileListView.Dock = DockStyle.Fill;
        FileListView.HideSelection = false;
        FileListView.Location = new Point(0, 0);
        FileListView.Name = "_FileListView";
        FileListView.Size = new Size(301, 238);
        FileListView.TabIndex = 0;
        FileListView.UseCompatibleStateImageBehavior = false;
        FileListView.View = View.Details;
        // 
        // _split
        // 
        _split.Dock = DockStyle.Fill;
        _split.Location = new Point(0, 25);
        _split.Name = "_Split";
        _split.Orientation = Orientation.Horizontal;
        // 
        // _split.Panel1
        // 
        _split.Panel1.Controls.Add(FileListView);
        // 
        // _split.Panel2
        // 
        _split.Panel2.Controls.Add(_fileInfoRichTextBox);
        _split.Panel2MinSize = 5;
        _split.Size = new Size(301, 472);
        _split.SplitterDistance = 238;
        _split.SplitterWidth = 5;
        _split.TabIndex = 1;
        // 
        // _fileInfoRichTextBox
        // 
        _fileInfoRichTextBox.Dock = DockStyle.Fill;
        _fileInfoRichTextBox.Location = new Point(0, 0);
        _fileInfoRichTextBox.Name = "_FileInfoRichTextBox";
        _fileInfoRichTextBox.Size = new Size(301, 229);
        _fileInfoRichTextBox.TabIndex = 0;
        _fileInfoRichTextBox.Text = "file info";
        // 
        // FileListViewControl
        // 
        AutoScaleDimensions = new SizeF(7.0f, 15.0f);
        AutoScaleMode = AutoScaleMode.Font;
        Controls.Add(_split);
        Controls.Add(_toolStrip);
        Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
        Name = "FileListViewControl";
        Size = new Size(301, 497);
        _toolStrip.ResumeLayout(false);
        _toolStrip.PerformLayout();
        _split.Panel1.ResumeLayout(false);
        _split.Panel2.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)_split).EndInit();
        _split.ResumeLayout(false);
        ResumeLayout(false);
        PerformLayout();
    }

    private ToolStrip _toolStrip;
    private System.Windows.Forms.ToolStripComboBox _searchPatternComboBox;
    private System.Windows.Forms.ToolStripButton _refreshButton;
    private SplitContainer _split;
    private System.Windows.Forms.RichTextBox _fileInfoRichTextBox;
}
