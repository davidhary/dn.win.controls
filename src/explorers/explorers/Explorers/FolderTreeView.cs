using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> Implements a tree view for exploring folders. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-07-06 </para><para>
/// David, 2019-07-03, Uses tree view from
/// https://www.codeproject.com/Articles/14570/A-Windows-Explorer-in-a-user-control
/// </para>
/// </remarks>
[ToolboxBitmap( typeof( FolderTreeView ), "FolderTreeView.gif" )]
[DefaultEvent( "PathChanged" )]
public partial class FolderTreeView : TreeView
{
    #region " construction and cleanup "

    /// <summary> Gets or sets the initializing components. </summary>
    /// <value> The initializing components. </value>
    private bool InitializingComponents { get; set; }

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public FolderTreeView() : base()
    {
        this.InitializingComponents = true;
        this.ShowLines = false;
        this.ShowRootLines = false;
        ComponentResourceManager resources = new( typeof( FolderTreeView ) );
        this.ImageList = new ImageList()
        {
            ImageStream = resources.GetObject( "_ImageList.ImageStream", System.Globalization.CultureInfo.CurrentCulture ) as ImageListStreamer,
            TransparentColor = Color.Transparent
        };
        for ( int i = 0; i <= 28; i++ )
        {
            this.ImageList.Images.SetKeyName( i, string.Empty );
        }

        // List view managers the contiguous list of paths.
        this._pathColumnHeader = new ColumnHeader();
        this._statusColumnHeader = new ColumnHeader();
        this._listView = new System.Windows.Forms.ListView()
        {
            Name = "_ListView",
            HideSelection = false,
            UseCompatibleStateImageBehavior = false,
            View = View.Details,
            Visible = false
        };
        this._listView.Columns.AddRange( [this._pathColumnHeader, this._statusColumnHeader] );

        // Context menu
        base.ContextMenuStrip = this.CreateContextMenuStrip();
        this.ImageIndex = 0;
        this.SelectedImageIndex = 2;
        this.InitializingComponents = false;
        this._candidatePath = System.IO.Directory.GetCurrentDirectory();
        this._selectedPath = System.IO.Directory.GetCurrentDirectory();
        this._myComputerTreeNode = new TreeNode();
        this._rootTreeNode = new TreeNode();
        this._myContextMenuStrip = new ContextMenuStrip();
    }

    /// <summary> Clean up any resources being used. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    /// resources; <see langword="false" /> to release only unmanaged
    /// resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( disposing )
        {
            this.ContextMenuStrip = null;
            this._myContextMenuStrip?.Dispose();
            this._listView?.Dispose();
        }

        base.Dispose( disposing );
    }

    /// <summary> The list view control. </summary>
    private readonly System.Windows.Forms.ListView _listView;

    /// <summary> The path column header. </summary>
    private readonly ColumnHeader _pathColumnHeader;

    /// <summary> The status column header. </summary>
    private readonly ColumnHeader _statusColumnHeader;

    #endregion

    #region " appearance "

    /// <summary> True to show, false to hide my documents. </summary>
    private bool _showMyDocuments = true;

    /// <summary> Show my documents. </summary>
    /// <value> The show my documents. </value>
    [Category( "Appearance" )]
    [Description( "Show my documents" )]
    [DefaultValue( true )]
    public bool ShowMyDocuments
    {
        get => this._showMyDocuments;
        set
        {
            if ( value != this.ShowMyDocuments )
            {
                this._showMyDocuments = value;
                if ( this.IsInitialized )
                {
                    this.Refresh();
                }
            }
        }
    }

    /// <summary> True to show, false to hide my favorites. </summary>
    private bool _showMyFavorites = true;

    /// <summary> True to show, false to hide my favorites. </summary>
    /// <value> The show my favorites. </value>
    [Category( "Appearance" )]
    [Description( "Show my favorites" )]
    [DefaultValue( true )]
    public bool ShowMyFavorites
    {
        get => this._showMyFavorites;
        set
        {
            if ( value != this.ShowMyFavorites )
            {
                this._showMyFavorites = value;
                if ( this.IsInitialized )
                {
                    this.Refresh();
                }
            }
        }
    }

    /// <summary> True to show, false to hide my network. </summary>
    private bool _showMyNetwork = true;

    /// <summary> True to show, false to hide my network. </summary>
    /// <value> The show my network. </value>
    [Category( "Appearance" )]
    [Description( "Show my network" )]
    [DefaultValue( true )]
    public bool ShowMyNetwork
    {
        get => this._showMyNetwork;
        set
        {
            if ( value != this.ShowMyNetwork )
            {
                this._showMyNetwork = value;
                if ( this.IsInitialized )
                {
                    this.Refresh();
                }
            }
        }
    }

    #endregion

    #region " events "

    /// <summary> Event queue for all listeners interested in PathChanged events. </summary>
    public event EventHandler<EventArgs>? PathChanged;

    /// <summary> Notifies the path changed. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected void NotifyPathChanged()
    {
        if ( !this.InitializingComponents )
        {
            this.PathChanged?.Invoke( this, EventArgs.Empty );
        }
    }

    #endregion

    #region " behavior "

    /// <summary> Name of the 'Home' path </summary>
#pragma warning disable IDE0079
#pragma warning disable CA1707 // Identifiers should not contain underscores
    public const string HOME_PATH_NAME = "home";
#pragma warning restore CA1707 // Identifiers should not contain underscores
#pragma warning restore IDE0079

    /// <summary> Full pathname of the selected file. </summary>
    private string _selectedPath;

    /// <summary> Gets or sets the full pathname of the selected directory. </summary>
    /// <value> The full pathname of the selected directory. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string SelectedPath
    {
        get => this._selectedPath;

        protected set
        {
            if ( !string.Equals( value, this.SelectedPath, StringComparison.Ordinal ) )
            {
                this._candidatePath = value;
                this._selectedPath = value;
                this.NotifyPathChanged();
            }
        }
    }

    /// <summary> Select path. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="pathName"> Full pathname of the file. </param>
    public void SelectPath( string pathName )
    {
        this.SetCandidatePath( pathName );
        this.SelectMyComputerTreeNode( this.CandidatePath );
    }

    /// <summary> Full pathname of the candidate file. </summary>
    private string _candidatePath;

    /// <summary> Gets or sets the full pathname of the Candidate path. </summary>
    /// <value> The full pathname of the Candidate path. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string CandidatePath
    {
        get => this._candidatePath;

        protected set
        {
            if ( !string.Equals( value, this.CandidatePath, StringComparison.Ordinal ) )
            {
                this._candidatePath = value;
            }
        }
    }

    /// <summary> Sets candidate path. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="pathName"> Full pathname of the file. </param>
    public void SetCandidatePath( string pathName )
    {
        this.CandidatePath = string.Equals( pathName, HOME_PATH_NAME, StringComparison.Ordinal ) ? Application.StartupPath : Directory.Exists( pathName ) ? pathName : Application.StartupPath;
    }

    /// <summary> my computer tree node. </summary>
    private TreeNode _myComputerTreeNode;

    /// <summary> The root tree node. </summary>
    private TreeNode _rootTreeNode;

    /// <summary> Gets the is initialized. </summary>
    /// <value> The is initialized. </value>
    public bool IsInitialized => this._rootTreeNode is not null;

    /// <summary> Initializes the nodes and explores the desktop folder. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void InitializeKnownState()
    {
        this.Nodes.Clear();

        // Dim drives() As String = Environment.GetLogicalDrives()

        // Environment.UserDomainName .GetFolderPath( 
        // Environment.GetFolderPath (Environment.SystemDirectory);

        TreeNode desktopNode = new()
        {
            Tag = Environment.GetFolderPath( Environment.SpecialFolder.Desktop ),
            Text = "Desktop",
            ImageIndex = 10,
            SelectedImageIndex = 10
        };
        _ = this.Nodes.Add( desktopNode );
        this._rootTreeNode = desktopNode;
        if ( this.ShowMyDocuments )
        {
            // Add My Documents and Desktop folder outside
            TreeNode myDocumentsNode = new()
            {
                Tag = Environment.GetFolderPath( Environment.SpecialFolder.Personal ),
                Text = "My Documents",
                ImageIndex = 9,
                SelectedImageIndex = 9
            };
            _ = desktopNode.Nodes.Add( myDocumentsNode );
            _ = TryFillFilesAndDirectories( myDocumentsNode );
        }

        TreeNode myComputerNode = new()
        {
            Tag = "My Computer",
            Text = "My Computer",
            ImageIndex = 12,
            SelectedImageIndex = 12
        };
        _ = desktopNode.Nodes.Add( myComputerNode );
        myComputerNode.EnsureVisible();
        this._myComputerTreeNode = myComputerNode;
        TreeNode myNode = new()
        {
            Tag = "my Node",
            Text = "my Node",
            ImageIndex = 12,
            SelectedImageIndex = 12
        };
        _ = myComputerNode.Nodes.Add( myNode );
        if ( this.ShowMyNetwork )
        {
            TreeNode myNetworkPlacesNode = new()
            {
                Tag = "My Network Places",
                Text = "My Network Places",
                ImageIndex = 13,
                SelectedImageIndex = 13
            };
            _ = desktopNode.Nodes.Add( myNetworkPlacesNode );
            myNetworkPlacesNode.EnsureVisible();
            TreeNode entireNetworkNode = new()
            {
                Tag = "Entire Network",
                Text = "Entire Network",
                ImageIndex = 14,
                SelectedImageIndex = 14
            };
            _ = myNetworkPlacesNode.Nodes.Add( entireNetworkNode );
            TreeNode networkNode = new()
            {
                Tag = "Network Node",
                Text = "Network Node",
                ImageIndex = 15,
                SelectedImageIndex = 15
            };
            _ = entireNetworkNode.Nodes.Add( networkNode );
            entireNetworkNode.EnsureVisible();
        }

        if ( this.ShowMyFavorites )
        {
            TreeNode myFavoriteNode = new()
            {
                Tag = Environment.GetFolderPath( Environment.SpecialFolder.Favorites ),
                Text = "My Favorites",
                ImageIndex = 26,
                SelectedImageIndex = 26
            };
            _ = desktopNode.Nodes.Add( myFavoriteNode );
            _ = TryFillFilesAndDirectories( myFavoriteNode );
        }

        ExploreTreeNode( desktopNode );
    }

    /// <summary> Explore tree node. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="parentNode"> The node. </param>
    private static void ExploreTreeNode( TreeNode parentNode )
    {
        Cursor.Current = Cursors.WaitCursor;
        try
        {
            // get directories
            _ = TryFillFilesAndDirectories( parentNode );
            // get directories one more level deep in current directory so user can see there is
            // more directories underneath current directory 
            foreach ( TreeNode subNode in parentNode.Nodes )
            {
                if ( string.Equals( parentNode.Text, "Desktop", StringComparison.Ordinal ) )
                {
                    if ( string.Equals( subNode.Text, "My Documents", StringComparison.Ordinal ) || string.Equals( subNode.Text, "My Computer", StringComparison.Ordinal ) || string.Equals( subNode.Text, "Microsoft Windows Network", StringComparison.Ordinal ) || string.Equals( subNode.Text, "My Network Places", StringComparison.Ordinal ) )
                    {
                    }
                    else
                    {
                        _ = TryFillFilesAndDirectories( subNode );
                    }
                }
                else
                {
                    _ = TryFillFilesAndDirectories( subNode );
                }
            }
        }
        catch
        {
            throw;
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    /// <summary> Gets the directories. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="parentNode"> The parent node. </param>
    private static void GetDirectories( TreeNode parentNode )
    {
        if ( parentNode is null || parentNode.Tag is null ) return;
        string[] dirList = Directory.GetDirectories( parentNode.Tag.ToString()! );
        Array.Sort( dirList );

        // check if directory already exists in case click same directory twice
        if ( dirList.Length != parentNode.Nodes.Count )
        {
            // add each directory in selected director
            foreach ( string path in dirList )
            {
                _ = parentNode.Nodes.Add( new TreeNode()
                {
                    Tag = path, // store path in tag
                    Text = path[(path.LastIndexOf( @"\", StringComparison.Ordinal ) + 1)..],
                    ImageIndex = 1
                } );
            }
        }
    }

    /// <summary> Fill files and directories. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="communalNode"> The communal node. </param>
    /// <returns> The (Success As Boolean, Details As String) </returns>
    private static (bool Success, string Details) TryFillFilesAndDirectories( TreeNode communalNode )
    {
        try
        {
            GetDirectories( communalNode );
            return (true, string.Empty);
        }
        catch ( Exception ex )
        {
            return (false, ex.ToString());
        }
    }

    /// <summary> Refresh folders. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void RefreshFolders()
    {
        this._listView.Items.Clear();
        this.Nodes.Clear();
        // Me.SetCurrentPath(Environment.GetFolderPath(Environment.SpecialFolder.Personal))
        this.InitializeKnownState();
    }

    /// <summary> Select my computer tree node. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="searchPath"> Full pathname of the search file. </param>
    private void SelectMyComputerTreeNode( string searchPath )
    {
        if ( !this.IsInitialized )
        {
            this.InitializeKnownState();
        }

        int h = 1;
        TreeNode tn = this._myComputerTreeNode;
        if ( tn is null || tn.Nodes is null ) return;
        TreeNode? selectedNode = null;
        this.ExploreMyComputer();
    StartAgain:
        ;
        do
        {
            if ( tn is null || tn.Nodes is null ) continue;
            foreach ( TreeNode t in tn.Nodes )
            {
                string? myPath = t.Tag?.ToString();
                if ( myPath is null || string.IsNullOrWhiteSpace( myPath ) ) continue;
                string? fullPath = myPath;
                if ( !myPath.EndsWith( @"\", StringComparison.Ordinal ) )
                {
                    if ( searchPath.Length > fullPath.Length )
                    {
                        fullPath = $@"{myPath}\";
                    }
                }

                if ( searchPath.StartsWith( fullPath, StringComparison.Ordinal ) )
                {
                    _ = t.TreeView?.Focus();
                    // t.TreeView.SelectedNode = t
                    t.EnsureVisible();
                    t.Expand();
                    if ( t.Nodes.Count >= 1 )
                    {
                        t.Expand();
                        tn = t;
                    }
                    else if ( string.Equals( searchPath, myPath, StringComparison.Ordinal ) )
                    {
                        selectedNode = t;
                        h = -1;
                        break;
                    }
                    else
                    {
                        continue;
                    }

                    if ( fullPath.StartsWith( searchPath, StringComparison.Ordinal ) )
                    {
                        selectedNode = t;
                        h = -1;
                        break;
                    }
                    else
                    {
                        goto StartAgain;
                        // return;
                    }
                }
            }

            try
            {
                if ( tn.NextNode != null ) tn = tn.NextNode;
            }
            catch
            {
            }
        }
        while ( h >= 0 );
        if ( selectedNode is not null && selectedNode.TreeView is not null )
        {
            selectedNode.TreeView.SelectedNode = selectedNode;
        }
    }

    /// <summary> Adds a folder node to 'path'. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="name"> The name. </param>
    /// <param name="path"> Full pathname of the file. </param>
    private void AddFolderNode( string name, string path )
    {
        try
        {
            TreeNode myCNode = new()
            {
                Tag = path,
                Text = name,
                ImageIndex = 18,
                SelectedImageIndex = 18
            };
            _ = this._rootTreeNode.Nodes.Add( myCNode );
            try
            {
                // add directories under drive
                if ( Directory.Exists( path ) )
                {
                    foreach ( string dir in Directory.GetDirectories( path ) )
                    {
                        TreeNode newNode = new()
                        {
                            Tag = dir,
                            Text = dir[(dir.LastIndexOf( @"\", StringComparison.Ordinal ) + 1)..],
                            ImageIndex = 1
                        };
                        _ = myCNode.Nodes.Add( newNode );
                    }
                }
            }
            catch ( Exception ex ) // error just add blank directory
            {
                _ = MessageBox.Show( "Error while Filling the Explorer:" + ex.Message );
            }
        }
        catch ( Exception e )
        {
            _ = MessageBox.Show( e.Message );
        }
    }

    /// <summary> Explore my computer. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void ExploreMyComputer()
    {
        if ( !this.IsInitialized )
        {
            this.RefreshFolders();
        }

        string[] drives = Environment.GetLogicalDrives();
        TreeNode nodeDrive;
        if ( this._myComputerTreeNode.GetNodeCount( true ) < 2 )
        {
            this._myComputerTreeNode.FirstNode!.Remove();
            foreach ( string drive in drives )
            {
                nodeDrive = new TreeNode() { Tag = drive };
                UpdateDriveImage( drive, nodeDrive );
                _ = this._myComputerTreeNode.Nodes.Add( nodeDrive );
                try
                {
                    // add directories under drive
                    if ( Directory.Exists( drive ) )
                    {
                        foreach ( string dir in Directory.GetDirectories( drive ) )
                        {
                            _ = nodeDrive.Nodes.Add( new TreeNode()
                            {
                                Tag = dir,
                                Text = dir[(dir.LastIndexOf( @"\", StringComparison.Ordinal ) + 1)..],
                                ImageIndex = 1
                            } );
                        }
                    }
                }
                catch ( Exception ex )
                {
                    _ = MessageBox.Show( "Error while Filling the Explorer:" + ex.Message );
                }
            }
        }

        this._myComputerTreeNode.Expand();
    }

    #endregion

    #region " folder list "

    /// <summary> Updates the list add current. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void UpdateListAddCurrent()
    {
        int loopTo = this._listView.Items.Count - 2;
        int i;
        for ( i = 0; i <= loopTo; i++ )
        {
            if ( string.Equals( this._listView.Items[i].SubItems[1].Text, "Selected", StringComparison.Ordinal ) )
            {
                int loopTo1 = i + 2;
                int j;
                for ( j = this._listView.Items.Count - 1; j >= loopTo1; j -= 1 )
                {
                    this._listView.Items[j].Remove();
                }

                break;
            }
        }
    }

    /// <summary> Updates the list go back. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void UpdateListGoBack()
    {
        if ( this._listView.Items.Count > 0 && string.Equals( this._listView.Items[0].SubItems[1].Text, "Selected", StringComparison.Ordinal ) ) return;

        int loopTo = this._listView.Items.Count - 1;

        int i;
        for ( i = 0; i <= loopTo; i++ )
        {
            if ( string.Equals( this._listView.Items[i].SubItems[1].Text, "Selected", StringComparison.Ordinal ) )
            {
                if ( i != 0 )
                {
                    this._listView.Items[i - 1].SubItems[1].Text = "Selected";
                    this.CandidatePath = this._listView.Items[i - 1].Text;
                }
            }

            if ( i != 0 )
            {
                this._listView.Items[i].SubItems[1].Text = " -/- ";
            }
        }
    }

    /// <summary> Updates the list go forward. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void UpdateListGoFwd()
    {
        if ( this._listView.Items.Count > 0 && string.Equals( this._listView.Items[^1].SubItems[1].Text, "Selected", StringComparison.Ordinal ) ) return;

        int i;
        for ( i = this._listView.Items.Count - 1; i >= 0; i -= 1 )
        {
            if ( string.Equals( this._listView.Items[i].SubItems[1].Text, "Selected", StringComparison.Ordinal ) )
            {
                if ( i != this._listView.Items.Count )
                {
                    this._listView.Items[i + 1].SubItems[1].Text = "Selected";
                    this.CandidatePath = this._listView.Items[i + 1].Text;
                }
            }

            if ( i != this._listView.Items.Count - 1 )
            {
                this._listView.Items[i].SubItems[1].Text = " -/- ";
            }
        }
    }

    /// <summary> Updates the list described by f. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="f"> The format string. </param>
    private void UpdateList( string f )
    {
        ListViewItem listViewItem; // Used for creating list view items.
        this.UpdateListAddCurrent();
        try
        {
            if ( this._listView.Items.Count > 0 )
            {
                if ( string.Equals( this._listView.Items[^1].Text, f, StringComparison.Ordinal ) )
                {
                    return;
                }
            }

            int i;
            int loopTo = this._listView.Items.Count - 1;
            for ( i = 0; i <= loopTo; i++ )
            {
                this._listView.Items[i].SubItems[1].Text = " -/- ";
            }

            listViewItem = new ListViewItem( f );
            _ = listViewItem.SubItems.Add( "Selected" );
            listViewItem.Tag = f;
            _ = this._listView.Items.Add( listViewItem );
        }
        catch ( Exception e )
        {
            _ = MessageBox.Show( e.Message );
        }
    }

    /// <summary> Updates the drive image. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="drive">     The drive. </param>
    /// <param name="nodeDrive"> The node drive. </param>
    private static void UpdateDriveImage( string drive, TreeNode nodeDrive )
    {
        if ( nodeDrive is not null )
        {
            nodeDrive.Text = drive;
            DriveInfo di = new( drive );
            switch ( di.DriveType )
            {
                case DriveType.Removable: // 2
                    {
                        nodeDrive.ImageIndex = 17;
                        nodeDrive.SelectedImageIndex = 17;
                        break;
                    }

                case DriveType.Fixed: // 3
                    {
                        nodeDrive.ImageIndex = 0;
                        nodeDrive.SelectedImageIndex = 0;
                        break;
                    }

                case DriveType.Network: // 4
                    {
                        nodeDrive.ImageIndex = 8;
                        nodeDrive.SelectedImageIndex = 8;
                        break;
                    }

                case DriveType.CDRom: // 5
                    {
                        nodeDrive.ImageIndex = 7;
                        nodeDrive.SelectedImageIndex = 7;
                        break;
                    }

                case DriveType.Unknown:
                    break;
                case DriveType.NoRootDirectory:
                    break;
                case DriveType.Ram:
                    break;
                default:
                    {
                        nodeDrive.ImageIndex = 0;
                        nodeDrive.SelectedImageIndex = 0;
                        break;
                    }
            }
        }
    }

    #endregion

    #region " tree view control events "

    /// <summary> Expand my computer node. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> Tree view event information. </param>
    private void ExpandMyComputerNode( TreeViewEventArgs e )
    {
        if ( e?.Node is null ) return;

        string[] drives = Environment.GetLogicalDrives();
        TreeNode n;
        TreeNode nodeMyC;
        TreeNode nodeDrive;
        nodeMyC = e.Node;
        n = e.Node;
        if ( string.Equals( n.Text, "My Computer", StringComparison.Ordinal ) && nodeMyC.GetNodeCount( true ) < 2 )
        {
            // /////////
            // add each drive and files and directories
            nodeMyC.FirstNode!.Remove();
            foreach ( string drive in drives )
            {
                nodeDrive = new TreeNode() { Tag = drive };
                UpdateDriveImage( drive, nodeDrive );
                _ = nodeMyC.Nodes.Add( nodeDrive );
                nodeDrive.EnsureVisible();
                base.Refresh();
                try
                {
                    // add directories under drive
                    if ( Directory.Exists( drive ) )
                    {
                        foreach ( string dir in Directory.GetDirectories( drive ) )
                        {
                            _ = nodeDrive.Nodes.Add( new TreeNode()
                            {
                                Tag = dir,
                                Text = dir[(dir.LastIndexOf( @"\", StringComparison.Ordinal ) + 1)..],
                                ImageIndex = 1
                            } );
                        }
                    }
                }
                catch
                {
                }

                nodeMyC.Expand();
            }
        }
    }

    /// <summary>
    /// Raises the <see cref="TreeView.AfterExpand" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="TreeViewEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnAfterExpand( TreeViewEventArgs e )
    {
        base.OnAfterExpand( e );
        if ( e.Node is null ) return;

        Cursor.Current = Cursors.WaitCursor;
        try
        {
            TreeNode? n = e.Node;
            if ( n.Text.IndexOf( ":", 1, StringComparison.Ordinal ) > 0 )
            {
                ExploreTreeNode( n );
            }
            else if ( string.Equals( n.Text, "Desktop", StringComparison.Ordinal )
                      || string.Equals( n.Text, "Microsoft Windows Network", StringComparison.Ordinal )
                      || string.Equals( n.Text, "My Computer", StringComparison.Ordinal )
                      || string.Equals( n.Text, "My Network Places", StringComparison.Ordinal )
                      || string.Equals( n.Text, "Entire Network", StringComparison.Ordinal )
                      || (n.Parent is not null && string.Equals( n.Parent.Text, "Microsoft Windows Network", StringComparison.Ordinal )) )
            {
                this.ExpandMyComputerNode( e );
            }
            // Me.ExpandEntireNetworkNode(e) ' TO_DO: Fix
            // Me.ExpandMicrosoftWindowsNetworkNode(e)  TO_DO: Fix
            else
            {
                ExploreTreeNode( n );
            }
        }
        catch ( UnauthorizedAccessException )
        {
            _ = MessageBox.Show( $"Access denied to folder {e.Node?.Tag}", "Access denied",
                MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
        }
        catch
        {
            throw;
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    /// <summary>
    /// Raises the <see cref="TreeView.AfterSelect" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="TreeViewEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnAfterSelect( TreeViewEventArgs e )
    {
        base.OnAfterSelect( e );
        if ( e.Node is null ) return;

        TreeNode n;
        n = e.Node;
        try
        {
            if ( string.Equals( n.Text, "My Computer", StringComparison.Ordinal ) || string.Equals( n.Text, "My Network Places", StringComparison.Ordinal ) || string.Equals( n.Text, "Entire Network", StringComparison.Ordinal ) )
            {
            }
            else if ( n.Tag is not null )
            {
                this.SelectedPath = n.Tag.ToString()!;
            }
        }
        catch
        {
        }
    }

    /// <summary>
    /// Raises the <see cref="Control.DoubleClick" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> An <see cref="EventArgs" /> that contains the event data. </param>
    protected override void OnDoubleClick( EventArgs e )
    {
        base.OnDoubleClick( e );
        if ( e is null ) return;

        TreeNode n;
        if ( this.SelectedNode is not null )
        {
            n = this.SelectedNode;
            if ( !this.SelectedNode.IsExpanded )
            {
                this.SelectedNode.Collapse();
            }
            else
            {
                ExploreTreeNode( n );
            }
        }
    }

    /// <summary> Raises the <see cref="Control.MouseUp" /> event. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> A <see cref="MouseEventArgs" /> that contains the
    /// event data. </param>
    protected override void OnMouseUp( MouseEventArgs e )
    {
        this.UpdateList( this.SelectedPath );
        base.OnMouseUp( e );
    }

    /// <summary>
    /// Forces the control to invalidate its client area and immediately redraw itself and any child
    /// controls.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public override void Refresh()
    {
        base.Refresh();
        Cursor.Current = Cursors.WaitCursor;
        try
        {
            this.RefreshFolders();
            if ( string.IsNullOrWhiteSpace( this.CandidatePath ) )
            {
                this.SetCandidatePath( HOME_PATH_NAME );
            }

            this.SelectMyComputerTreeNode( this.CandidatePath );
        }
        catch
        {
            throw;
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    #endregion

    #region " tool bar "

    /// <summary> Try select directory. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="path"> Full pathname of the file. </param>
    /// <returns> The (Success As Boolean, Details As String) </returns>
    public (bool Success, string Details) TrySelectPath( string path )
    {
        Cursor.Current = Cursors.WaitCursor;
        try
        {
            this.SelectPath( path );
            return (false, string.Empty);
        }
        catch ( Exception ex )
        {
            return (false, ex.ToString());
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    /// <summary> Try select home. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The (Success As Boolean, Details As String) </returns>
    public (bool Success, string Details) TrySelectHome()
    {
        return this.TrySelectPath( HOME_PATH_NAME );
    }

    /// <summary> Try go forward. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The (Success As Boolean, Details As String) </returns>
    public (bool Success, string Details) TryGoingForward()
    {
        Cursor.Current = Cursors.WaitCursor;
        try
        {
            this.UpdateListGoFwd();
            if ( !string.Equals( this.SelectedPath, this.CandidatePath, StringComparison.Ordinal ) )
            {
                this.SelectMyComputerTreeNode( this.CandidatePath );
            }

            return (false, string.Empty);
        }
        catch ( Exception ex )
        {
            return (false, ex.ToString());
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    /// <summary> Try go back. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The (Success As Boolean, Details As String) </returns>
    public (bool Success, string Details) TryGoingBack()
    {
        Cursor.Current = Cursors.WaitCursor;
        try
        {
            this.UpdateListGoBack();
            if ( !string.Equals( this.SelectedPath, this.CandidatePath, StringComparison.Ordinal ) )
            {
                this.SelectMyComputerTreeNode( this.CandidatePath );
            }

            return (false, string.Empty);
        }
        catch ( Exception ex )
        {
            return (false, ex.ToString());
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    /// <summary> Try go up. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The (Success As Boolean, Details As String) </returns>
    public (bool Success, string Details) TryGoingUp()
    {
        Cursor.Current = Cursors.WaitCursor;
        try
        {
            DirectoryInfo di = new( this.SelectedPath );
            if ( di.Parent?.Exists ?? false )
            {
                this.CandidatePath = di.Parent.FullName;
                this.UpdateList( this.CandidatePath );
                this.SelectMyComputerTreeNode( this.CandidatePath );
            }

            return (false, string.Empty);
        }
        catch ( Exception ex )
        {
            return (false, ex.ToString());
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    /// <summary> Try add folder. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The (Success As Boolean, Details As String) </returns>
    public (bool Success, string Details) TryAddFolder()
    {
        try
        {
            using ( FolderBrowserDialog dialog = new()
            {
                Description = "Add Folder in Explorer Tree",
                ShowNewFolderButton = true,
                SelectedPath = this.SelectedPath
            } )
            {
                if ( dialog.ShowDialog() == DialogResult.OK )
                {
                    string newPath = dialog.SelectedPath;
                    string pathTitle = newPath[(newPath.LastIndexOf( @"\", StringComparison.Ordinal ) + 1)..];
                    this.AddFolderNode( pathTitle, newPath );
                }
            }

            return (false, string.Empty);
        }
        catch ( Exception ex )
        {
            return (false, ex.ToString());
        }
        finally
        {
        }
    }

    #endregion

    #region " context menu strip "

    /// <summary> my context menu strip. </summary>
    private ContextMenuStrip _myContextMenuStrip;

    /// <summary> Creates a context menu strip. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> The new context menu strip. </returns>
    private ContextMenuStrip CreateContextMenuStrip()
    {
        // Create a new ContextMenuStrip control.
        this._myContextMenuStrip = new ContextMenuStrip();

        // Attach an event handler for the 
        // ContextMenuStrip control's Opening event.
        this._myContextMenuStrip.Opening += this.ContextMenuOpeningHandler;
        return this._myContextMenuStrip;
    }

    /// <summary> Adds menu items. </summary>
    /// <remarks>
    /// This event handler is invoked when the <see cref="ContextMenuStrip"/> control's Opening event
    /// is raised.
    /// </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Cancel event information. </param>
    private void ContextMenuOpeningHandler( object? sender, CancelEventArgs e )
    {
        if ( sender is null ) return;
#pragma warning disable IDE0083 // Use pattern matching
        if ( !(sender is ContextMenuStrip) ) return;
#pragma warning restore IDE0083 // Use pattern matching
        this._myContextMenuStrip = (sender as ContextMenuStrip)!;

        // Clear the ContextMenuStrip control's Items collection.
        this._myContextMenuStrip.Items.Clear();

        // Populate the ContextMenuStrip control with its default items.
        // myContextMenuStrip.Items.Add("-")
        _ = this._myContextMenuStrip.Items.Add( new ToolStripMenuItem( "&Refresh", null, this.RefreshHandler, "Refresh" ) );
        if ( this.SelectedNode!.ImageIndex == 18 )
        {
            _ = this._myContextMenuStrip.Items.Add( new ToolStripMenuItem( "Remove &Shortcut", null, this.RemoveShortcutHandler, "RemoveShortcut" ) );
        }

        // Set Cancel to false. 
        // It is optimized to true based on empty entry.
        e.Cancel = false;
    }

    /// <summary> Removes the shortcut handler. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void RemoveShortcutHandler( object? sender, EventArgs e )
    {
        if ( this.SelectedNode!.ImageIndex == 18 )
        {
            this.SelectedNode.Remove();
        }
    }

    /// <summary> Handler, called when the refresh. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void RefreshHandler( object? sender, EventArgs e )
    {
        this.Refresh();
    }

    #endregion
}
