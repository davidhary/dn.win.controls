using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using cc.isr.WinControls.Explorers;

namespace cc.isr.WinControls;

/// <summary> A file list view control. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-07-06 </para>
/// </remarks>
[Description( "File list view control" )]
[ToolboxBitmap( typeof( FileListView ), "FileListViewControl.gif" )]
public partial class FileListViewControl : UserControl
{
    /// <summary>
    /// Initializes a new instance of the <see cref="UserControl" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public FileListViewControl() : base()
    {
        this.IsInitializingComponents = true;
        this.FileListView = new FileListView();
        this.InitializeComponent();
        this.IsInitializingComponents = false;
        this._refreshButton.Name = "_RefreshButton";
        this._directoryPath = string.Empty;
    }

    #region " construction and cleanup "

    /// <summary> Gets the is initializing components. </summary>
    /// <value> The is initializing components. </value>
    private bool IsInitializingComponents { get; set; }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="Control" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    /// resources; <see langword="false" /> to release only unmanaged
    /// resources. </param>
    [System.Diagnostics.DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " event source "

    /// <summary> Gets the file list view. </summary>
    /// <value> The file list view. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public FileListView FileListView { get; private set; }

    #endregion

    #region " appearance "

    /// <summary> Gets or sets information describing the hide file. </summary>
    /// <value> Information describing the hide file. </value>
    [Category( "Appearance" )]
    [Description( "Hide file info" )]
    [DefaultValue( true )]
    public bool HideFileInfo
    {
        get => this._split.Panel2Collapsed;
        set
        {
            if ( value != this.HideFileInfo )
            {
                this._split.Panel2Collapsed = value;
            }
        }
    }

    #endregion

    #region " behavior "

    /// <summary> Full pathname of the directory file. </summary>
    private string _directoryPath;

    /// <summary> The current directory path. </summary>
    /// <value> The full pathname of the folder tree view. </value>
    [Category( "Behavior" )]
    [Description( "Current directory path" )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string DirectoryPath
    {
        get => this._directoryPath;
        set
        {
            if ( !string.Equals( value, this.DirectoryPath, StringComparison.Ordinal ) )
            {
                this._directoryPath = value;
            }
        }
    }

    /// <summary> Search pattern. </summary>
    /// <value> The search pattern. </value>
    [Category( "Behavior" )]
    [Description( "Search pattern" )]
    [DefaultValue( "*.*" )]
    public string SearchPattern
    {
        get => this._searchPatternComboBox.Text;
        set
        {
            if ( !string.Equals( value, this.SearchPattern, StringComparison.Ordinal ) )
            {
                this._searchPatternComboBox.Text = this.SearchPattern;
            }
        }
    }

    /// <summary> Gets or sets the search pattern delimiter. </summary>
    /// <value> The search pattern delimiter. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public char SearchPatternDelimiter { get; set; } = ';';

    /// <summary> Gets or sets the search patterns. </summary>
    /// <value> The search patterns. </value>
    [Category( "Behavior" )]
    [Description( "Search patterns" )]
    [DefaultValue( "*.*;*.bmp;*.png;*.csv;*.txt" )]
    public string SearchPatterns
    {
        get
        {
            System.Text.StringBuilder values = new();
            foreach ( object item in this._searchPatternComboBox.Items )
            {
                _ = values.Append( $"{item}{this.SearchPatternDelimiter}" );
            }

            return values.ToString().TrimEnd( this.SearchPatternDelimiter );
        }

        set
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                this._searchPatternComboBox.Items.Clear();
                _ = this._searchPatternComboBox.Items.Add( "*.*" );
            }
            else
            {
                this._searchPatternComboBox.Items.Clear();
                foreach ( string pattern in value.Split( this.SearchPatternDelimiter ) )
                {
                    _ = this._searchPatternComboBox.Items.Add( pattern );
                }
            }

            this._searchPatternComboBox.SelectedIndex = 0;
        }
    }

    /// <summary>
    /// Forces the control to invalidate its client area and immediately redraw itself and any child
    /// controls.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public override void Refresh()
    {
        base.Refresh();
        if ( this.IsInitializingComponents ) return;

        this.FileListView.PopulateFiles( this.DirectoryPath, this._searchPatternComboBox.Text );
    }

    #endregion

    #region " control event handlers "

    /// <summary> Refresh button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void RefreshButtonClick( object? sender, EventArgs e )
    {
        this.Refresh();
    }

    #endregion
}
