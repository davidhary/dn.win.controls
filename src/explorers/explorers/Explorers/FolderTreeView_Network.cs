using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace cc.isr.WinControls;

public partial class FolderTreeView
{
    /// <summary> Network computers. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <returns> A list of. </returns>
    public static IList<string> NetworkComputers()
    {
        List<string> servers = [];
        // TO_DO: Use Win32_MyComputerSystem node instead of Win NT
        using ( System.DirectoryServices.DirectoryEntry root = new( "WinNT:" ) )
        {
            foreach ( System.DirectoryServices.DirectoryEntries entries in root.Children )
            {
                foreach ( System.DirectoryServices.DirectoryEntry entry in entries )
                {
                    if ( entry.SchemaClassName == "Computer" )
                    {
                        servers.Add( entry.Name );
                    }
                }
            }
        }

        return servers;
    }

    /// <summary>   Expand entire network node. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="e">    Tree view event information. </param>
    public static void ExpandEntireNetworkNode( TreeViewEventArgs e )
    {
        if ( e.Node is null ) return;
        TreeNode? n = e.Node;
        TreeNode nodeNN;
        TreeNode nodeMN;
        if ( string.Equals( n.Text, "Entire Network", StringComparison.Ordinal ) )
        {
            if ( n.FirstNode!.Text == "Network Node" )
            {
                n.FirstNode.Remove();

                // NETRESOURCE netRoot = new NETRESOURCE();
                using ( System.Management.ManagementClass shares = new() )
                {
                }

                ArrayList servers = [];
                using ( System.DirectoryServices.DirectoryEntry root = new( "WinNT:" ) )
                {
                    foreach ( System.DirectoryServices.DirectoryEntries entries in root.Children )
                    {
                        foreach ( System.DirectoryServices.DirectoryEntry entry in entries )
                        {
                            _ = servers.Add( entry.Name );
                        }
                    }
                }

                foreach ( string s1 in servers )
                {
                    string s2 = string.Empty;
                    s2 = s1[..s1.IndexOf( "|", 1, StringComparison.Ordinal )];
                    if ( s1.IndexOf( "NETWORK", 1, StringComparison.Ordinal ) > 0 )
                    {
                        nodeNN = new TreeNode()
                        {
                            Tag = s2,
                            Text = s2, // dir.Substring(dir.LastIndexOf(@"\") + 1);
                            ImageIndex = 15,
                            SelectedImageIndex = 15
                        };
                        _ = n.Nodes.Add( nodeNN );
                    }
                    else
                    {
                        // need to handle the VMWare situation where 
                        nodeMN = new TreeNode()
                        {
                            Tag = s2, // "my Node";
                            Text = s2, // "my Node";//dir.Substring(dir.LastIndexOf(@"\") + 1);
                            ImageIndex = 16,
                            SelectedImageIndex = 16
                        };
                        // DH: this does not work. there are initially no nodes here
                        // so last node is null.
                        _ = n.LastNode is null ? n.Nodes.Add( nodeMN ) : n.LastNode.Nodes.Add( nodeMN );

                        TreeNode nodeMNC;
                        nodeMNC = new TreeNode()
                        {
                            Tag = MY_NETWORK_NODE_NAME,
                            Text = MY_NETWORK_NODE_NAME, // dir.Substring(dir.LastIndexOf(@"\") + 1);
                            ImageIndex = 12,
                            SelectedImageIndex = 12
                        };
                        _ = nodeMN.Nodes.Add( nodeMNC );
                    }
                }
            }
        }
    }

    /// <summary> Name of my network node. </summary>
    private const string MY_NETWORK_NODE_NAME = "My Node";

    /// <summary> Expand Microsoft windows network node. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="e"> Tree view event information. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
    private static void ExpandMicrosoftWindowsNetworkNode( TreeViewEventArgs e )
    {
        if ( e.Node is null ) return;

        TreeNode? n = e.Node;
        TreeNode nodeNN;
        TreeNode nodeNNode;
        if ( n.Parent is not null && string.Equals( n.Parent.Text, "Microsoft Windows Network", StringComparison.Ordinal ) )
        {
            if ( string.Equals( n.FirstNode!.Text, MY_NETWORK_NODE_NAME, StringComparison.Ordinal ) )
            {
                n.FirstNode.Remove();

                // NETRESOURCE netRoot = new NETRESOURCE();
                // Dim servers As New ServerEnum(ResourceScope.RESOURCE_GLOBALNET, ResourceType.RESOURCETYPE_DISK, 
                // ResourceUsage.RESOURCEUSAGE_ALL, ResourceDisplayType.RESOURCEDISPLAYTYPE_SERVER, n.Text)
                string[] servers = []; // ServerEnum(n.Text)
                foreach ( string s1 in servers )
                {
                    if ( s1.Length < 6 || !string.Equals( s1.Substring( s1.Length - 6, 6 ), "-share", StringComparison.Ordinal ) )
                    {
                        string s2 = s1;
                        nodeNN = new TreeNode()
                        {
                            Tag = s2,
                            Text = s2[2..],
                            ImageIndex = 12,
                            SelectedImageIndex = 12
                        };
                        _ = n.Nodes.Add( nodeNN );
                        foreach ( string s1node in servers )
                        {
                            if ( s1node.Length > 6 )
                            {
                                if ( string.Equals( s1node.Substring( s1node.Length - 6, 6 ), "-share", StringComparison.Ordinal ) )
                                {
                                    if ( s2.Length <= s1node.Length )
                                    {
                                        try
                                        {
                                            if ( string.Equals( s1node[..(s2.Length + 1)], s2 + @"\", StringComparison.Ordinal ) )
                                            {
                                                nodeNNode = new TreeNode()
                                                {
                                                    Tag = s1node[0..^6],
                                                    Text = s1node.Substring( s2.Length + 1, s1node.Length - s2.Length - 7 ),
                                                    ImageIndex = 28,
                                                    SelectedImageIndex = 28
                                                };
                                                _ = nodeNN.Nodes.Add( nodeNNode );
                                            }
                                        }
                                        catch ( Exception )
                                        {
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
