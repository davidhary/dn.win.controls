using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace cc.isr.WinControls;

/// <summary> Summary description for ExplorerTree. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-07-06 </para>
/// </remarks>
[ToolboxBitmap( typeof( FolderTreeViewControl ), "FolderTreeViewControl.gif" )]
public partial class FolderTreeViewControl : UserControl
{
    /// <summary>
    /// Initializes a new instance of the <see cref="UserControl" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public FolderTreeViewControl()
    {
        this.IsInitializingComponents = true;
        // This call is required by the Windows.Forms Form Designer.
        this.InitializeComponent();
        this.IsInitializingComponents = false;
        this._homeToolStripButton.Image = this._imageList.Images[23];
        this._addFolderButton.Image = this._imageList.Images[27];
        this._backToolStripButton.Image = this._imageList.Images[20];
        this._forwardToolStripButton.Image = this._imageList.Images[19];
        this._folderUpButton.Image = this._imageList.Images[24];
        this._refreshToolStripButton.Image = this._imageList.Images[25];
    }

    [MemberNotNull( nameof( FolderTreeView ) )]
    private partial void InitializeComponent();

    #region " construction and cleanup "

    /// <summary> Gets the is initializing components. </summary>
    /// <value> The is initializing components. </value>
    private bool IsInitializingComponents { get; set; }

    /// <summary> Clean up any resources being used. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    /// resources; <see langword="false" /> to release only unmanaged
    /// resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( disposing )
        {
            this.components?.Dispose();
        }

        base.Dispose( disposing );
    }

    #endregion

    #region " appearance "

    /// <summary> Gets the folder tree view. </summary>
    /// <value> The folder tree view. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public FolderTreeView FolderTreeView { get; private set; }

    /// <summary> Gets or sets the show current path text box. </summary>
    /// <value> The show current path text box. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool ShowCurrentPathTextBox
    {
        get => this._currentPathLayoutPanel.Visible;
        set => this._currentPathLayoutPanel.Visible = value;
    }

    /// <summary> Gets or sets the show tool bar. </summary>
    /// <value> The show tool bar. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool ShowToolBar
    {
        get => this._toolStrip.Visible;
        set => this._toolStrip.Visible = value;
    }

    /// <summary> Gets or sets the show my documents. </summary>
    /// <value> The show my documents. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool ShowMyDocuments
    {
        get => this.FolderTreeView.ShowMyDocuments;
        set => this.FolderTreeView.ShowMyDocuments = value;
    }

    /// <summary> Gets or sets the show my favorites. </summary>
    /// <value> The show my favorites. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool ShowMyFavorites
    {
        get => this.FolderTreeView.ShowMyFavorites;
        set => this.FolderTreeView.ShowMyFavorites = value;
    }

    /// <summary> Gets or sets the show my network. </summary>
    /// <value> The show my network. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool ShowMyNetwork
    {
        get => this.FolderTreeView.ShowMyNetwork;
        set => this.FolderTreeView.ShowMyNetwork = value;
    }

    #endregion

    #region " behavior "

    /// <summary> The selected path. </summary>
    /// <value> The full pathname of the folder tree view. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string SelectedPath
    {
        get => this.FolderTreeView.SelectedPath;
        set
        {
            if ( !string.Equals( value, this.SelectedPath, StringComparison.Ordinal ) )
            {
                this.SelectCurrentPath( value );
            }
        }
    }

    /// <summary> Select current path. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    private void SelectCurrentPath( string value )
    {
        (bool success, string _) = this.FolderTreeView.TrySelectPath( value );
        if ( success )
        {
            this._pathTextBox.Text = value;
        }
    }

    /// <summary> Refresh view. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void RefreshView()
    {
        this.FolderTreeView.Refresh();
    }

    #endregion

    #region " tool bar "

    /// <summary> Event handler. Called by _refreshToolStripButton for click events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void RefreshButtonClick( object? sender, EventArgs e )
    {
        Cursor.Current = Cursors.WaitCursor;
        try
        {
            this.RefreshView();
        }
        catch ( Exception e1 )
        {
            _ = MessageBox.Show( "Error: " + e1.Message );
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    /// <summary> Event handler. Called by _goToDirectoryButton for click events. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void GoToDirectoryButtonClick( object? sender, EventArgs e )
    {
        Cursor.Current = Cursors.WaitCursor;
        try
        {
            this.SelectCurrentPath( this._pathTextBox.Text.Trim() );
        }
        catch ( Exception e1 )
        {
            _ = MessageBox.Show( "Error: " + e1.Message );
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    /// <summary> Home tool strip button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void HomeToolStripButton_Click( object? sender, EventArgs e )
    {
        _ = this.FolderTreeView.TrySelectHome();
    }

    /// <summary> Forward tool strip button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void ForwardToolStripButton_Click( object? sender, EventArgs e )
    {
        _ = this.FolderTreeView.TryGoingForward();
    }

    /// <summary> Back tool strip button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void BackToolStripButton_Click( object? sender, EventArgs e )
    {
        _ = this.FolderTreeView.TryGoingBack();
    }

    /// <summary> Folder up button click. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FolderUpButton_Click( object? sender, EventArgs e )
    {
        _ = this.FolderTreeView.TryGoingUp();
    }

    /// <summary> Adds a folder button click to 'e'. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void AddFolderButton_Click( object? sender, EventArgs e )
    {
        _ = this.FolderTreeView.TryAddFolder();
    }

    #endregion

    #region " path text box "

    /// <summary> Folder tree view path changed. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void FolderTreeView_PathChanged( object? sender, EventArgs e )
    {
        if ( this.IsInitializingComponents ) return;

        this._pathTextBox.Text = this.FolderTreeView.SelectedPath;
    }

    /// <summary> Path text box key up. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Key event information. </param>
    private void PathTextBox_KeyUp( object? sender, KeyEventArgs e )
    {
        if ( e.KeyValue == 13 )
        {
            string value = this._pathTextBox.Text.Trim();
            if ( Directory.Exists( value ) && !string.Equals( this.FolderTreeView.SelectedPath, value, StringComparison.Ordinal ) )
            {
                this.FolderTreeView.SelectPath( value );
            }

            _ = this._pathTextBox.Focus();
        }
    }

    #endregion
}
