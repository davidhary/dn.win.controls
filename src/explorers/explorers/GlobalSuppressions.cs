// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "namespace", Target = "~N:cc.isr" )]
[assembly: SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "namespace", Target = "~N:cc.isr.WinControls" )]
[assembly: SuppressMessage( "Performance", "CA1865:Use char overload", Justification = "<Pending>", Scope = "member", Target = "~M:cc.isr.WinControls.FolderFileSelector.FileTitleTextBox_TextChanged(System.Object,System.EventArgs)" )]
[assembly: SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "namespace", Target = "~N:cc.isr.WinControls.Explorers" )]
[assembly: SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "member", Target = "~M:cc.isr.WinControls.Explorers.FileExplorerPassive.InitializeComponent" )]
[assembly: SuppressMessage( "Performance", "CA1865:Use char overload", Justification = "<Pending>", Scope = "module" )]
[assembly: SuppressMessage( "CodeQuality", "IDE0079:Remove unnecessary suppression", Justification = "<Pending>", Scope = "module" )]
[assembly: SuppressMessage( "Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~P:cc.isr.WinControls.FileListViewControl.SearchPatterns" )]
