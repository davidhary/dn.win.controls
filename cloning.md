# Cloning

Support libraries for Windows Forms Controls
This is a fork of the [dn.core] repository.

* [Source Code](#Source-Code)
* [Repositories](#Repositories)
* [Packages](#Packages)
* [Facilitated By](#Facilitated-By)

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories] are required:
* [win.controls] - ISR windows controls libraries
* [enums] - ISR enum libraries
* [std] - ISR .NET standard libraries
* [json] - ISR json libraries
* [tracing] - ISR tracing libraries
* [logging] - ISR logging libraries
```
git clone git@bitbucket.org:davidhary/dn.win.controls.git
git clone git@bitbucket.org:davidhary/dn.enums.git
git clone git@bitbucket.org:davidhary/dn.std.git
git clone git@bitbucket.org:davidhary/dn.json.git
git clone git@bitbucket.org:davidhary/dn.tracing.git
git clone git@bitbucket.org:davidhary/dn.logging.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
%vslib%\core\win.controls
%vslib%\core\enums
%vslib%\core\std
%vslib%\core\json
%vslib%\core\tracing
%vslib%\core\logging
```
where %vslib% is the root folder of the .NET libraries, e.g., %my%\lib\vs 
and %my% is the root folder of the .NET solutions

### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

### Packages
Presently, packages are consumed from a _source feed_ residing in a local folder, e.g., _%my%\nuget\packages_. 
The packages are 'packed', using the _Pack_ command from each packable project,
into the _%my%\nuget_ folder as specified in the project file and then
added to the source feed. Alternatively, the packages can be downloaded from the 
private [MEGA packages folder].

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[win.controls]: https://www.bitbucket.org/davidhary/dn.win.controls
[enums]: https://www.bitbucket.org/davidhary/dn.enums
[json]: https://www.bitbucket.org/davidhary/dn.json
[tracing]: https://www.bitbucket.org/davidhary/dn.tracing
[logging]: https://www.bitbucket.org/davidhary/dn.logging
[std]: https://www.bitbucket.org/davidhary/dn.std
[Exception Extension]: https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
[Notification Window]: http://www.codeproject.com/KB/dialog/notificationwindow.aspx
[Safe Events1]: http://www.CodeProject.com/KB/cs/EventSafeTrigger.aspx
[Safe Events2]: http://www.DreamInCode.net/forums/user/334492-aeonhack
[Safe Events3]: http://www.DreamInCode.net/code/snippet5016.htm

[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
[external repositories]: ExternalReposCommits.csv
